package com.android.settings.security;

import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import com.android.settingslib.RestrictedPreference;
import android.security.KeyStore;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class ResetCredentialsPreferenceController extends RestrictedEncryptionPreferenceController implements LifecycleObserver, OnResume
{
    private final KeyStore mKeyStore;
    private RestrictedPreference mPreference;
    
    public ResetCredentialsPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "no_config_credentials");
        this.mKeyStore = KeyStore.getInstance();
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = (RestrictedPreference)preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public String getPreferenceKey() {
        return "credentials_reset";
    }
    
    @Override
    public void onResume() {
        if (this.mPreference != null && !this.mPreference.isDisabledByAdmin()) {
            this.mPreference.setEnabled(this.mKeyStore.isEmpty() ^ true);
        }
    }
}
