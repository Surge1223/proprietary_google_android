package com.android.settings.security;

import android.hardware.fingerprint.FingerprintManager;
import com.android.settings.Utils;
import android.support.v7.preference.Preference;
import android.content.Intent;
import com.android.settings.fingerprint.FingerprintProfileStatusPreferenceController;
import java.util.Collection;
import com.android.settings.security.screenlock.LockScreenPreferenceController;
import com.android.settings.fingerprint.FingerprintStatusPreferenceController;
import com.android.settings.widget.PreferenceCategoryController;
import com.android.settings.security.trustagent.TrustAgentListPreferenceController;
import com.android.settings.security.trustagent.ManageTrustAgentsPreferenceController;
import com.android.settings.enterprise.EnterprisePrivacyPreferenceController;
import com.android.settings.enterprise.ManageDeviceAdminPreferenceController;
import com.android.settings.location.LocationPreferenceController;
import android.app.Activity;
import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import com.android.settings.dashboard.SummaryLoader;
import com.android.settings.search.Indexable;
import com.android.settings.dashboard.DashboardFragment;

public class SecuritySettings extends DashboardFragment
{
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    public static final SummaryProviderFactory SUMMARY_PROVIDER_FACTORY;
    
    static {
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null);
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082824;
                list.add(searchIndexableResource);
                return list;
            }
        };
        SUMMARY_PROVIDER_FACTORY = new SummaryProviderFactory() {
            @Override
            public SummaryLoader.SummaryProvider createSummaryProvider(final Activity activity, final SummaryLoader summaryLoader) {
                return new SecuritySettings.SummaryProvider((Context)activity, summaryLoader);
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Lifecycle lifecycle, final SecuritySettings securitySettings) {
        final ArrayList<PreferenceCategoryController> list = (ArrayList<PreferenceCategoryController>)new ArrayList<AbstractPreferenceController>();
        list.add(new LocationPreferenceController(context, lifecycle));
        list.add(new ManageDeviceAdminPreferenceController(context));
        list.add(new EnterprisePrivacyPreferenceController(context));
        list.add(new ManageTrustAgentsPreferenceController(context));
        list.add(new ScreenPinningPreferenceController(context));
        list.add(new SimLockPreferenceController(context));
        list.add(new ShowPasswordPreferenceController(context));
        list.add(new EncryptionStatusPreferenceController(context, "encryption_and_credential"));
        list.add(new TrustAgentListPreferenceController(context, securitySettings, lifecycle));
        final ArrayList<PreferenceCategoryController> children = new ArrayList<PreferenceCategoryController>();
        children.add((ChangeScreenLockPreferenceController)new FingerprintStatusPreferenceController(context));
        children.add((ChangeScreenLockPreferenceController)new LockScreenPreferenceController(context, lifecycle));
        children.add(new ChangeScreenLockPreferenceController(context, securitySettings));
        list.add(new PreferenceCategoryController(context, "security_category").setChildren((List<AbstractPreferenceController>)children));
        list.addAll((Collection<? extends AbstractPreferenceController>)children);
        final ArrayList<LocationPreferenceController> children2 = new ArrayList<LocationPreferenceController>();
        children2.add((FingerprintProfileStatusPreferenceController)new ChangeProfileScreenLockPreferenceController(context, securitySettings));
        children2.add((FingerprintProfileStatusPreferenceController)new LockUnificationPreferenceController(context, securitySettings));
        children2.add((FingerprintProfileStatusPreferenceController)new VisiblePatternProfilePreferenceController(context, lifecycle));
        children2.add(new FingerprintProfileStatusPreferenceController(context));
        list.add(new PreferenceCategoryController(context, "security_category_profile").setChildren((List<AbstractPreferenceController>)children2));
        list.addAll((Collection<? extends AbstractPreferenceController>)children2);
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        return buildPreferenceControllers(context, this.getLifecycle(), this);
    }
    
    @Override
    public int getHelpResource() {
        return 2131887828;
    }
    
    @Override
    protected String getLogTag() {
        return "SecuritySettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 87;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082824;
    }
    
    void launchConfirmDeviceLockForUnification() {
        this.use(LockUnificationPreferenceController.class).launchConfirmDeviceLockForUnification();
    }
    
    public void onActivityResult(final int n, final int n2, final Intent intent) {
        if (this.use(TrustAgentListPreferenceController.class).handleActivityResult(n, n2)) {
            return;
        }
        if (this.use(LockUnificationPreferenceController.class).handleActivityResult(n, n2, intent)) {
            return;
        }
        super.onActivityResult(n, n2, intent);
    }
    
    void unifyUncompliantLocks() {
        this.use(LockUnificationPreferenceController.class).unifyUncompliantLocks();
    }
    
    void updateUnificationPreference() {
        this.use(LockUnificationPreferenceController.class).updateState(null);
    }
    
    static class SummaryProvider implements SummaryLoader.SummaryProvider
    {
        private final Context mContext;
        private final SummaryLoader mSummaryLoader;
        
        public SummaryProvider(final Context mContext, final SummaryLoader mSummaryLoader) {
            this.mContext = mContext;
            this.mSummaryLoader = mSummaryLoader;
        }
        
        @Override
        public void setListening(final boolean b) {
            if (b) {
                final FingerprintManager fingerprintManagerOrNull = Utils.getFingerprintManagerOrNull(this.mContext);
                if (fingerprintManagerOrNull != null && fingerprintManagerOrNull.isHardwareDetected()) {
                    this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getString(2131888973));
                }
                else {
                    this.mSummaryLoader.setSummary((SummaryLoader.SummaryProvider)this, this.mContext.getString(2131888974));
                }
            }
        }
    }
}
