package com.android.settings.security;

import java.util.concurrent.ExecutionException;
import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.Utils;
import com.android.settings.overlay.FeatureFactory;
import android.os.UserHandle;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import com.android.internal.widget.LockPatternUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.TogglePreferenceController;

public class VisiblePatternProfilePreferenceController extends TogglePreferenceController implements LifecycleObserver, OnResume
{
    private static final String KEY_VISIBLE_PATTERN_PROFILE = "visiblepattern_profile";
    private static final String TAG = "VisPtnProfPrefCtrl";
    private final LockPatternUtils mLockPatternUtils;
    private Preference mPreference;
    private final int mProfileChallengeUserId;
    private final UserManager mUm;
    private final int mUserId;
    
    public VisiblePatternProfilePreferenceController(final Context context) {
        this(context, (Lifecycle)null);
    }
    
    public VisiblePatternProfilePreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "visiblepattern_profile");
        this.mUserId = UserHandle.myUserId();
        this.mUm = (UserManager)context.getSystemService("user");
        this.mLockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
        this.mProfileChallengeUserId = Utils.getManagedProfileId(this.mUm, this.mUserId);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public int getAvailabilityStatus() {
        final FutureTask<Integer> futureTask = new FutureTask<Integer>(new _$$Lambda$VisiblePatternProfilePreferenceController$rwDeZ_aTyFGsJcFkBXrMF4RE1tM(this));
        try {
            futureTask.run();
            return futureTask.get();
        }
        catch (InterruptedException | ExecutionException ex) {
            Log.w("VisPtnProfPrefCtrl", "Error getting lock pattern state.");
            return 3;
        }
    }
    
    @Override
    public boolean isChecked() {
        return this.mLockPatternUtils.isVisiblePatternEnabled(this.mProfileChallengeUserId);
    }
    
    @Override
    public void onResume() {
        this.mPreference.setVisible(this.isAvailable());
    }
    
    @Override
    public boolean setChecked(final boolean b) {
        if (Utils.startQuietModeDialogIfNecessary(this.mContext, this.mUm, this.mProfileChallengeUserId)) {
            return false;
        }
        this.mLockPatternUtils.setVisiblePatternEnabled(b, this.mProfileChallengeUserId);
        return true;
    }
}
