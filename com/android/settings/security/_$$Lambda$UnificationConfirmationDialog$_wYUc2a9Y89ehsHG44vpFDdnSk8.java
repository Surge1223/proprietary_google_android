package com.android.settings.security;

import android.app.FragmentManager;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$UnificationConfirmationDialog$_wYUc2a9Y89ehsHG44vpFDdnSk8 implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        UnificationConfirmationDialog.lambda$onCreateDialog$0(this.f$0, this.f$1, dialogInterface, n);
    }
}
