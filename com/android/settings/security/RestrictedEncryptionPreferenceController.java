package com.android.settings.security;

import android.content.Context;
import android.os.UserManager;
import android.os.UserHandle;
import com.android.settings.core.PreferenceControllerMixin;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class RestrictedEncryptionPreferenceController extends AbstractPreferenceController implements PreferenceControllerMixin
{
    private final UserHandle mUserHandle;
    protected final UserManager mUserManager;
    private final String mUserRestriction;
    
    public RestrictedEncryptionPreferenceController(final Context context, final String mUserRestriction) {
        super(context);
        this.mUserHandle = UserHandle.of(UserHandle.myUserId());
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mUserRestriction = mUserRestriction;
    }
    
    @Override
    public boolean isAvailable() {
        return this.mUserManager.hasBaseUserRestriction(this.mUserRestriction, this.mUserHandle) ^ true;
    }
}
