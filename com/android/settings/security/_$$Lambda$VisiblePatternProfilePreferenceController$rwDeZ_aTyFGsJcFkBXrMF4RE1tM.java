package com.android.settings.security;

import java.util.concurrent.ExecutionException;
import android.util.Log;
import java.util.concurrent.FutureTask;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.Utils;
import com.android.settings.overlay.FeatureFactory;
import android.os.UserHandle;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserManager;
import android.support.v7.preference.Preference;
import com.android.internal.widget.LockPatternUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.TogglePreferenceController;
import java.util.concurrent.Callable;

public final class _$$Lambda$VisiblePatternProfilePreferenceController$rwDeZ_aTyFGsJcFkBXrMF4RE1tM implements Callable
{
    @Override
    public final Object call() {
        return VisiblePatternProfilePreferenceController.lambda$getAvailabilityStatus$0(this.f$0);
    }
}
