package com.android.settings.security;

import com.android.settings.password.ChooseLockGeneric;
import com.android.settings.core.SubSettingLauncher;
import android.os.Bundle;
import com.android.settings.Utils;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.content.Context;

public class ChangeProfileScreenLockPreferenceController extends ChangeScreenLockPreferenceController
{
    public ChangeProfileScreenLockPreferenceController(final Context context, final SecuritySettings securitySettings) {
        super(context, securitySettings);
    }
    
    @Override
    public String getPreferenceKey() {
        return "unlock_set_or_change_profile";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (!TextUtils.equals((CharSequence)preference.getKey(), (CharSequence)this.getPreferenceKey())) {
            return false;
        }
        if (Utils.startQuietModeDialogIfNecessary(this.mContext, this.mUm, this.mProfileChallengeUserId)) {
            return false;
        }
        final Bundle arguments = new Bundle();
        arguments.putInt("android.intent.extra.USER_ID", this.mProfileChallengeUserId);
        new SubSettingLauncher(this.mContext).setDestination(ChooseLockGeneric.ChooseLockGenericFragment.class.getName()).setTitle(2131888098).setSourceMetricsCategory(this.mHost.getMetricsCategory()).setArguments(arguments).launch();
        return true;
    }
    
    @Override
    public boolean isAvailable() {
        if (this.mProfileChallengeUserId == -10000 || !this.mLockPatternUtils.isSeparateProfileChallengeAllowed(this.mProfileChallengeUserId)) {
            return false;
        }
        if (!this.mLockPatternUtils.isSecure(this.mProfileChallengeUserId)) {
            return true;
        }
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mProfileChallengeUserId);
        return keyguardStoredPasswordQuality == 65536 || keyguardStoredPasswordQuality == 131072 || keyguardStoredPasswordQuality == 196608 || keyguardStoredPasswordQuality == 262144 || keyguardStoredPasswordQuality == 327680 || keyguardStoredPasswordQuality == 393216 || keyguardStoredPasswordQuality == 524288;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.updateSummary(preference, this.mProfileChallengeUserId);
        if (!this.mLockPatternUtils.isSeparateProfileChallengeEnabled(this.mProfileChallengeUserId)) {
            this.mPreference.setSummary(this.mContext.getString(2131888108));
            this.mPreference.setEnabled(false);
        }
        else {
            this.disableIfPasswordQualityManaged(this.mProfileChallengeUserId);
        }
    }
}
