package com.android.settings.security.screenlock;

import com.android.settings.notification.LockScreenNotificationPreferenceController;
import android.support.v7.preference.PreferenceScreen;
import com.android.settings.overlay.FeatureFactory;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.UserHandle;
import android.support.v7.preference.Preference;
import com.android.internal.widget.LockPatternUtils;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settings.core.BasePreferenceController;

public class LockScreenPreferenceController extends BasePreferenceController implements LifecycleObserver, OnResume
{
    static final String KEY_LOCKSCREEN_PREFERENCES = "lockscreen_preferences";
    private static final int MY_USER_ID;
    private final LockPatternUtils mLockPatternUtils;
    private Preference mPreference;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
    }
    
    public LockScreenPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, "lockscreen_preferences");
        this.mLockPatternUtils = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getLockPatternUtils(context);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public int getAvailabilityStatus() {
        final boolean secure = this.mLockPatternUtils.isSecure(LockScreenPreferenceController.MY_USER_ID);
        final boolean b = false;
        int n = 0;
        if (!secure) {
            if (this.mLockPatternUtils.isLockScreenDisabled(LockScreenPreferenceController.MY_USER_ID)) {
                n = 3;
            }
            return n;
        }
        int n2;
        if (this.mLockPatternUtils.getKeyguardStoredPasswordQuality(LockScreenPreferenceController.MY_USER_ID) == 0) {
            n2 = 3;
        }
        else {
            n2 = (b ? 1 : 0);
        }
        return n2;
    }
    
    @Override
    public void onResume() {
        this.mPreference.setVisible(this.isAvailable());
    }
    
    @Override
    public void updateState(final Preference preference) {
        preference.setSummary(LockScreenNotificationPreferenceController.getSummaryResource(this.mContext));
    }
}
