package com.android.settings.security.screenlock;

import android.support.v7.preference.TwoStatePreference;
import android.content.Context;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class PatternVisiblePreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final LockPatternUtils mLockPatternUtils;
    private final int mUserId;
    
    public PatternVisiblePreferenceController(final Context context, final int mUserId, final LockPatternUtils mLockPatternUtils) {
        super(context);
        this.mUserId = mUserId;
        this.mLockPatternUtils = mLockPatternUtils;
    }
    
    private boolean isPatternLock() {
        return this.mLockPatternUtils.isSecure(this.mUserId) && this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mUserId) == 65536;
    }
    
    @Override
    public String getPreferenceKey() {
        return "visiblepattern";
    }
    
    @Override
    public boolean isAvailable() {
        return this.isPatternLock();
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mLockPatternUtils.setVisiblePatternEnabled((boolean)o, this.mUserId);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((TwoStatePreference)preference).setChecked(this.mLockPatternUtils.isVisiblePatternEnabled(this.mUserId));
    }
}
