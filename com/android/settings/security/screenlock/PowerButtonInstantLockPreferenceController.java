package com.android.settings.security.screenlock;

import android.text.TextUtils;
import android.support.v7.preference.TwoStatePreference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.security.trustagent.TrustAgentManager;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class PowerButtonInstantLockPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final LockPatternUtils mLockPatternUtils;
    private final TrustAgentManager mTrustAgentManager;
    private final int mUserId;
    
    public PowerButtonInstantLockPreferenceController(final Context context, final int mUserId, final LockPatternUtils mLockPatternUtils) {
        super(context);
        this.mUserId = mUserId;
        this.mLockPatternUtils = mLockPatternUtils;
        this.mTrustAgentManager = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getTrustAgentManager();
    }
    
    @Override
    public String getPreferenceKey() {
        return "power_button_instantly_locks";
    }
    
    @Override
    public boolean isAvailable() {
        if (!this.mLockPatternUtils.isSecure(this.mUserId)) {
            return false;
        }
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mUserId);
        return keyguardStoredPasswordQuality == 65536 || keyguardStoredPasswordQuality == 131072 || keyguardStoredPasswordQuality == 196608 || keyguardStoredPasswordQuality == 262144 || keyguardStoredPasswordQuality == 327680 || keyguardStoredPasswordQuality == 393216 || keyguardStoredPasswordQuality == 524288;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        this.mLockPatternUtils.setPowerButtonInstantlyLocks((boolean)o, this.mUserId);
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((TwoStatePreference)preference).setChecked(this.mLockPatternUtils.getPowerButtonInstantlyLocks(this.mUserId));
        final CharSequence activeTrustAgentLabel = this.mTrustAgentManager.getActiveTrustAgentLabel(this.mContext, this.mLockPatternUtils);
        if (!TextUtils.isEmpty(activeTrustAgentLabel)) {
            preference.setSummary(this.mContext.getString(2131888193, new Object[] { activeTrustAgentLabel }));
        }
        else {
            preference.setSummary(2131889405);
        }
    }
}
