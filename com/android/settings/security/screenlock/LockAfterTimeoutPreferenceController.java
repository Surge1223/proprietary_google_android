package com.android.settings.security.screenlock;

import android.util.Log;
import android.text.TextUtils;
import com.android.settingslib.RestrictedLockUtils;
import android.provider.Settings;
import android.content.ComponentName;
import android.os.UserHandle;
import android.provider.Settings;
import com.android.settings.TimeoutListPreference;
import com.android.settings.overlay.FeatureFactory;
import android.content.Context;
import com.android.settings.security.trustagent.TrustAgentManager;
import com.android.internal.widget.LockPatternUtils;
import android.app.admin.DevicePolicyManager;
import com.android.settings.core.PreferenceControllerMixin;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public class LockAfterTimeoutPreferenceController extends AbstractPreferenceController implements OnPreferenceChangeListener, PreferenceControllerMixin
{
    private final DevicePolicyManager mDPM;
    private final LockPatternUtils mLockPatternUtils;
    private final TrustAgentManager mTrustAgentManager;
    private final int mUserId;
    
    public LockAfterTimeoutPreferenceController(final Context context, final int mUserId, final LockPatternUtils mLockPatternUtils) {
        super(context);
        this.mUserId = mUserId;
        this.mLockPatternUtils = mLockPatternUtils;
        this.mDPM = (DevicePolicyManager)context.getSystemService("device_policy");
        this.mTrustAgentManager = FeatureFactory.getFactory(context).getSecurityFeatureProvider().getTrustAgentManager();
    }
    
    private void setupLockAfterPreference(final TimeoutListPreference timeoutListPreference) {
        timeoutListPreference.setValue(String.valueOf(Settings.Secure.getLong(this.mContext.getContentResolver(), "lock_screen_lock_after_timeout", 5000L)));
        if (this.mDPM != null) {
            timeoutListPreference.removeUnusableTimeouts(Math.max(0L, this.mDPM.getMaximumTimeToLock((ComponentName)null, UserHandle.myUserId()) - Math.max(0, Settings.System.getInt(this.mContext.getContentResolver(), "screen_off_timeout", 0))), RestrictedLockUtils.checkIfMaximumTimeToLockIsSet(this.mContext));
        }
    }
    
    private void updateLockAfterPreferenceSummary(final TimeoutListPreference timeoutListPreference) {
        CharSequence summary;
        if (timeoutListPreference.isDisabledByAdmin()) {
            summary = this.mContext.getText(2131887427);
        }
        else {
            final long long1 = Settings.Secure.getLong(this.mContext.getContentResolver(), "lock_screen_lock_after_timeout", 5000L);
            final CharSequence[] entries = timeoutListPreference.getEntries();
            final CharSequence[] entryValues = timeoutListPreference.getEntryValues();
            int n = 0;
            for (int i = 0; i < entryValues.length; ++i) {
                if (long1 >= Long.valueOf(entryValues[i].toString())) {
                    n = i;
                }
            }
            final CharSequence activeTrustAgentLabel = this.mTrustAgentManager.getActiveTrustAgentLabel(this.mContext, this.mLockPatternUtils);
            if (!TextUtils.isEmpty(activeTrustAgentLabel)) {
                if (Long.valueOf(entryValues[n].toString()) == 0L) {
                    summary = this.mContext.getString(2131888071, new Object[] { activeTrustAgentLabel });
                }
                else {
                    summary = this.mContext.getString(2131888065, new Object[] { entries[n], activeTrustAgentLabel });
                }
            }
            else {
                summary = this.mContext.getString(2131888064, new Object[] { entries[n] });
            }
        }
        timeoutListPreference.setSummary(summary);
    }
    
    @Override
    public String getPreferenceKey() {
        return "lock_after_timeout";
    }
    
    @Override
    public boolean isAvailable() {
        if (!this.mLockPatternUtils.isSecure(this.mUserId)) {
            return false;
        }
        final int keyguardStoredPasswordQuality = this.mLockPatternUtils.getKeyguardStoredPasswordQuality(this.mUserId);
        return keyguardStoredPasswordQuality == 65536 || keyguardStoredPasswordQuality == 131072 || keyguardStoredPasswordQuality == 196608 || keyguardStoredPasswordQuality == 262144 || keyguardStoredPasswordQuality == 327680 || keyguardStoredPasswordQuality == 393216 || keyguardStoredPasswordQuality == 524288;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        try {
            Settings.Secure.putInt(this.mContext.getContentResolver(), "lock_screen_lock_after_timeout", Integer.parseInt((String)o));
            this.updateState(preference);
        }
        catch (NumberFormatException ex) {
            Log.e("PrefControllerMixin", "could not persist lockAfter timeout setting", (Throwable)ex);
        }
        return true;
    }
    
    @Override
    public void updateState(final Preference preference) {
        this.setupLockAfterPreference((TimeoutListPreference)preference);
        this.updateLockAfterPreferenceSummary((TimeoutListPreference)preference);
    }
}
