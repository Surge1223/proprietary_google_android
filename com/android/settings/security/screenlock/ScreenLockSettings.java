package com.android.settings.security.screenlock;

import java.util.ArrayList;
import android.provider.SearchIndexableResource;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.app.Fragment;
import com.android.settingslib.core.AbstractPreferenceController;
import java.util.List;
import android.content.Context;
import com.android.settings.search.BaseSearchIndexProvider;
import android.os.UserHandle;
import com.android.internal.widget.LockPatternUtils;
import com.android.settings.search.Indexable;
import com.android.settings.security.OwnerInfoPreferenceController;
import com.android.settings.dashboard.DashboardFragment;

public class ScreenLockSettings extends DashboardFragment implements OwnerInfoCallback
{
    private static final int MY_USER_ID;
    public static final SearchIndexProvider SEARCH_INDEX_DATA_PROVIDER;
    private LockPatternUtils mLockPatternUtils;
    
    static {
        MY_USER_ID = UserHandle.myUserId();
        SEARCH_INDEX_DATA_PROVIDER = new BaseSearchIndexProvider() {
            @Override
            public List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
                return buildPreferenceControllers(context, null, null, new LockPatternUtils(context));
            }
            
            @Override
            public List<String> getNonIndexableKeys(final Context context) {
                final List<String> nonIndexableKeys = super.getNonIndexableKeys(context);
                nonIndexableKeys.add("security_settings_password_sub_screen");
                return nonIndexableKeys;
            }
            
            @Override
            public List<SearchIndexableResource> getXmlResourcesToIndex(final Context context, final boolean b) {
                final ArrayList<SearchIndexableResource> list = new ArrayList<SearchIndexableResource>();
                final SearchIndexableResource searchIndexableResource = new SearchIndexableResource(context);
                searchIndexableResource.xmlResId = 2132082822;
                list.add(searchIndexableResource);
                return list;
            }
        };
    }
    
    private static List<AbstractPreferenceController> buildPreferenceControllers(final Context context, final Fragment fragment, final Lifecycle lifecycle, final LockPatternUtils lockPatternUtils) {
        final ArrayList<PowerButtonInstantLockPreferenceController> list = (ArrayList<PowerButtonInstantLockPreferenceController>)new ArrayList<OwnerInfoPreferenceController>();
        list.add((OwnerInfoPreferenceController)new PatternVisiblePreferenceController(context, ScreenLockSettings.MY_USER_ID, lockPatternUtils));
        list.add((OwnerInfoPreferenceController)new PowerButtonInstantLockPreferenceController(context, ScreenLockSettings.MY_USER_ID, lockPatternUtils));
        list.add((OwnerInfoPreferenceController)new LockAfterTimeoutPreferenceController(context, ScreenLockSettings.MY_USER_ID, lockPatternUtils));
        list.add(new OwnerInfoPreferenceController(context, fragment, lifecycle));
        return (List<AbstractPreferenceController>)list;
    }
    
    @Override
    protected List<AbstractPreferenceController> createPreferenceControllers(final Context context) {
        this.mLockPatternUtils = new LockPatternUtils(context);
        return buildPreferenceControllers(context, this, this.getLifecycle(), this.mLockPatternUtils);
    }
    
    @Override
    protected String getLogTag() {
        return "ScreenLockSettings";
    }
    
    @Override
    public int getMetricsCategory() {
        return 1265;
    }
    
    @Override
    protected int getPreferenceScreenResId() {
        return 2132082822;
    }
    
    @Override
    public void onOwnerInfoUpdated() {
        this.use(OwnerInfoPreferenceController.class).updateSummary();
    }
}
