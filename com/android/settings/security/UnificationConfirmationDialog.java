package com.android.settings.security;

import android.app.FragmentManager;
import android.content.DialogInterface$OnClickListener;
import android.content.Context;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.os.Bundle;
import android.content.DialogInterface;
import com.android.settings.core.instrumentation.InstrumentedDialogFragment;

public class UnificationConfirmationDialog extends InstrumentedDialogFragment
{
    public static UnificationConfirmationDialog newInstance(final boolean b) {
        final UnificationConfirmationDialog unificationConfirmationDialog = new UnificationConfirmationDialog();
        final Bundle arguments = new Bundle();
        arguments.putBoolean("compliant", b);
        unificationConfirmationDialog.setArguments(arguments);
        return unificationConfirmationDialog;
    }
    
    @Override
    public int getMetricsCategory() {
        return 532;
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final SecuritySettings securitySettings = (SecuritySettings)this.getParentFragment();
        final boolean boolean1 = this.getArguments().getBoolean("compliant");
        final AlertDialog$Builder setTitle = new AlertDialog$Builder((Context)this.getActivity()).setTitle(2131888103);
        int message;
        if (boolean1) {
            message = 2131888101;
        }
        else {
            message = 2131888104;
        }
        final AlertDialog$Builder setMessage = setTitle.setMessage(message);
        int n;
        if (boolean1) {
            n = 2131888102;
        }
        else {
            n = 2131888105;
        }
        return (Dialog)setMessage.setPositiveButton(n, (DialogInterface$OnClickListener)new _$$Lambda$UnificationConfirmationDialog$_wYUc2a9Y89ehsHG44vpFDdnSk8(boolean1, securitySettings)).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).create();
    }
    
    public void onDismiss(final DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        ((SecuritySettings)this.getParentFragment()).updateUnificationPreference();
    }
    
    public void show(final SecuritySettings securitySettings) {
        final FragmentManager childFragmentManager = securitySettings.getChildFragmentManager();
        if (childFragmentManager.findFragmentByTag("unification_dialog") == null) {
            this.show(childFragmentManager, "unification_dialog");
        }
    }
}
