package com.android.settings.security;

import android.provider.Settings;
import android.content.Context;
import com.android.settings.core.BasePreferenceController;

public class ScreenPinningPreferenceController extends BasePreferenceController
{
    private static final String KEY_SCREEN_PINNING = "screen_pinning_settings";
    
    public ScreenPinningPreferenceController(final Context context) {
        super(context, "screen_pinning_settings");
    }
    
    @Override
    public int getAvailabilityStatus() {
        int n;
        if (this.mContext.getResources().getBoolean(2131034162)) {
            n = 0;
        }
        else {
            n = 2;
        }
        return n;
    }
    
    @Override
    public CharSequence getSummary() {
        CharSequence charSequence;
        if (Settings.System.getInt(this.mContext.getContentResolver(), "lock_to_app_enabled", 0) != 0) {
            charSequence = this.mContext.getText(2131889422);
        }
        else {
            charSequence = this.mContext.getText(2131889421);
        }
        return charSequence;
    }
}
