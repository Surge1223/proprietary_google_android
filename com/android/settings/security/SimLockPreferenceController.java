package com.android.settings.security;

import android.os.PersistableBundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import java.util.Iterator;
import java.util.List;
import android.telephony.SubscriptionInfo;
import android.content.Context;
import android.os.UserManager;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.telephony.CarrierConfigManager;
import com.android.settings.core.BasePreferenceController;

public class SimLockPreferenceController extends BasePreferenceController
{
    private static final String KEY_SIM_LOCK = "sim_lock_settings";
    private final CarrierConfigManager mCarrierConfigManager;
    private final SubscriptionManager mSubscriptionManager;
    private final TelephonyManager mTelephonyManager;
    private final UserManager mUserManager;
    
    public SimLockPreferenceController(final Context context) {
        super(context, "sim_lock_settings");
        this.mUserManager = (UserManager)context.getSystemService("user");
        this.mCarrierConfigManager = (CarrierConfigManager)this.mContext.getSystemService("carrier_config");
        this.mSubscriptionManager = (SubscriptionManager)context.getSystemService("telephony_subscription_service");
        this.mTelephonyManager = (TelephonyManager)context.getSystemService("phone");
    }
    
    private boolean isSimIccReady() {
        final List activeSubscriptionInfoList = this.mSubscriptionManager.getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList != null) {
            final Iterator<SubscriptionInfo> iterator = activeSubscriptionInfoList.iterator();
            while (iterator.hasNext()) {
                if (this.mTelephonyManager.hasIccCard(iterator.next().getSimSlotIndex())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private boolean isSimReady() {
        final List activeSubscriptionInfoList = this.mSubscriptionManager.getActiveSubscriptionInfoList();
        if (activeSubscriptionInfoList != null) {
            final Iterator<SubscriptionInfo> iterator = activeSubscriptionInfoList.iterator();
            while (iterator.hasNext()) {
                final int simState = this.mTelephonyManager.getSimState(iterator.next().getSimSlotIndex());
                if (simState != 1 && simState != 0) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        final Preference preference = preferenceScreen.findPreference(this.getPreferenceKey());
        if (preference == null) {
            return;
        }
        preference.setEnabled(this.isSimReady());
    }
    
    @Override
    public int getAvailabilityStatus() {
        final PersistableBundle config = this.mCarrierConfigManager.getConfig();
        if (this.mUserManager.isAdminUser() && this.isSimIccReady() && !config.getBoolean("hide_sim_lock_settings_bool")) {
            return 0;
        }
        return 3;
    }
}
