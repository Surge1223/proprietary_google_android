package com.android.settings;

import android.support.v7.preference.PreferenceScreen;
import java.util.HashSet;
import android.os.UserManager;
import android.os.Bundle;
import java.util.List;
import android.content.Intent;
import android.content.ContentResolver;
import android.provider.Settings;
import android.util.Log;
import android.os.UserHandle;
import android.app.backup.IBackupManager$Stub;
import android.os.ServiceManager;
import java.util.Collection;
import android.content.Context;
import android.os.RemoteException;
import android.app.backup.IBackupManager;
import android.support.v7.preference.Preference;
import android.support.v14.preference.SwitchPreference;

public class PrivacySettings extends SettingsPreferenceFragment
{
    static final String AUTO_RESTORE = "auto_restore";
    static final String BACKUP_DATA = "backup_data";
    static final String CONFIGURE_ACCOUNT = "configure_account";
    static final String DATA_MANAGEMENT = "data_management";
    private SwitchPreference mAutoRestore;
    private Preference mBackup;
    private IBackupManager mBackupManager;
    private Preference mConfigure;
    private boolean mEnabled;
    private Preference mManageData;
    private OnPreferenceChangeListener preferenceChangeListener;
    
    public PrivacySettings() {
        this.preferenceChangeListener = new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(final Preference preference, final Object o) {
                if (!(preference instanceof SwitchPreference)) {
                    return true;
                }
                final boolean booleanValue = (boolean)o;
                boolean b = false;
                if (preference == PrivacySettings.this.mAutoRestore) {
                    try {
                        PrivacySettings.this.mBackupManager.setAutoRestore(booleanValue);
                        b = true;
                    }
                    catch (RemoteException ex) {
                        PrivacySettings.this.mAutoRestore.setChecked(booleanValue ^ true);
                        b = b;
                    }
                }
                return b;
            }
        };
    }
    
    private static void getNonVisibleKeys(final Context context, final Collection<String> collection) {
        final IBackupManager interface1 = IBackupManager$Stub.asInterface(ServiceManager.getService("backup"));
        boolean b = false;
        final boolean b2 = false;
        boolean backupServiceActive;
        try {
            backupServiceActive = interface1.isBackupServiceActive(UserHandle.myUserId());
        }
        catch (RemoteException ex) {
            Log.w("PrivacySettings", "Failed querying backup manager service activity status. Assuming it is inactive.");
            backupServiceActive = b2;
        }
        if (context.getPackageManager().resolveContentProvider("com.google.settings", 0) == null) {
            b = true;
        }
        if (b || backupServiceActive) {
            collection.add("backup_inactive");
        }
        if (b || !backupServiceActive) {
            collection.add("backup_data");
            collection.add("auto_restore");
            collection.add("configure_account");
        }
    }
    
    private void setConfigureSummary(final String summary) {
        if (summary != null) {
            this.mConfigure.setSummary(summary);
        }
        else {
            this.mConfigure.setSummary(2131886535);
        }
    }
    
    private void updateToggles() {
        final ContentResolver contentResolver = this.getContentResolver();
        boolean backupEnabled = false;
        final Intent intent = null;
        final String s = null;
        final Intent intent2 = null;
        final CharSequence charSequence = null;
        final boolean b = false;
        Intent intent3 = intent;
        String configureSummary = s;
        Intent intent4 = intent2;
        CharSequence title = charSequence;
        try {
            final boolean b2 = backupEnabled = this.mBackupManager.isBackupEnabled();
            intent3 = intent;
            configureSummary = s;
            intent4 = intent2;
            title = charSequence;
            final String currentTransport = this.mBackupManager.getCurrentTransport();
            backupEnabled = b2;
            intent3 = intent;
            configureSummary = s;
            intent4 = intent2;
            title = charSequence;
            final Intent validatedActivityIntent = this.validatedActivityIntent(this.mBackupManager.getConfigurationIntent(currentTransport), "config");
            backupEnabled = b2;
            intent3 = validatedActivityIntent;
            configureSummary = s;
            intent4 = intent2;
            title = charSequence;
            final String destinationString = this.mBackupManager.getDestinationString(currentTransport);
            backupEnabled = b2;
            intent3 = validatedActivityIntent;
            configureSummary = destinationString;
            intent4 = intent2;
            title = charSequence;
            final Intent validatedActivityIntent2 = this.validatedActivityIntent(this.mBackupManager.getDataManagementIntent(currentTransport), "management");
            backupEnabled = b2;
            intent3 = validatedActivityIntent;
            configureSummary = destinationString;
            intent4 = validatedActivityIntent2;
            title = charSequence;
            final String dataManagementLabel = this.mBackupManager.getDataManagementLabel(currentTransport);
            backupEnabled = b2;
            intent3 = validatedActivityIntent;
            configureSummary = destinationString;
            intent4 = validatedActivityIntent2;
            title = dataManagementLabel;
            final Preference mBackup = this.mBackup;
            int summary;
            if (b2) {
                summary = 2131886155;
            }
            else {
                summary = 2131886154;
            }
            backupEnabled = b2;
            intent3 = validatedActivityIntent;
            configureSummary = destinationString;
            intent4 = validatedActivityIntent2;
            title = dataManagementLabel;
            mBackup.setSummary(summary);
            backupEnabled = b2;
            intent3 = validatedActivityIntent;
            configureSummary = destinationString;
            intent4 = validatedActivityIntent2;
            title = dataManagementLabel;
        }
        catch (RemoteException ex) {
            this.mBackup.setEnabled(false);
        }
        this.mAutoRestore.setChecked(Settings.Secure.getInt(contentResolver, "backup_auto_restore", 1) == 1);
        this.mAutoRestore.setEnabled(backupEnabled);
        this.mConfigure.setEnabled(intent3 != null && backupEnabled);
        this.mConfigure.setIntent(intent3);
        this.setConfigureSummary(configureSummary);
        boolean b3 = b;
        if (intent4 != null) {
            b3 = b;
            if (backupEnabled) {
                b3 = true;
            }
        }
        if (b3) {
            this.mManageData.setIntent(intent4);
            if (title != null) {
                this.mManageData.setTitle(title);
            }
        }
        else {
            this.getPreferenceScreen().removePreference(this.mManageData);
        }
    }
    
    private Intent validatedActivityIntent(final Intent intent, final String s) {
        Intent intent2 = intent;
        if (intent != null) {
            final List queryIntentActivities = this.getPackageManager().queryIntentActivities(intent, 0);
            if (queryIntentActivities != null) {
                intent2 = intent;
                if (!queryIntentActivities.isEmpty()) {
                    return intent2;
                }
            }
            intent2 = null;
            final StringBuilder sb = new StringBuilder();
            sb.append("Backup ");
            sb.append(s);
            sb.append(" intent ");
            sb.append((Object)null);
            sb.append(" fails to resolve; ignoring");
            Log.e("PrivacySettings", sb.toString());
        }
        return intent2;
    }
    
    @Override
    public int getHelpResource() {
        return 2131887798;
    }
    
    @Override
    public int getMetricsCategory() {
        return 81;
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (!(this.mEnabled = UserManager.get((Context)this.getActivity()).isAdminUser())) {
            return;
        }
        this.addPreferencesFromResource(2132082816);
        final PreferenceScreen preferenceScreen = this.getPreferenceScreen();
        this.mBackupManager = IBackupManager$Stub.asInterface(ServiceManager.getService("backup"));
        this.setPreferenceReferences(preferenceScreen);
        final HashSet<String> set = new HashSet<String>();
        getNonVisibleKeys((Context)this.getActivity(), set);
        for (int i = preferenceScreen.getPreferenceCount() - 1; i >= 0; --i) {
            final Preference preference = preferenceScreen.getPreference(i);
            if (set.contains(preference.getKey())) {
                preferenceScreen.removePreference(preference);
            }
        }
        this.updateToggles();
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (this.mEnabled) {
            this.updateToggles();
        }
    }
    
    void setPreferenceReferences(final PreferenceScreen preferenceScreen) {
        this.mBackup = preferenceScreen.findPreference("backup_data");
        (this.mAutoRestore = (SwitchPreference)preferenceScreen.findPreference("auto_restore")).setOnPreferenceChangeListener(this.preferenceChangeListener);
        this.mConfigure = preferenceScreen.findPreference("configure_account");
        this.mManageData = preferenceScreen.findPreference("data_management");
    }
}
