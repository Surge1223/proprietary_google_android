package com.android.settings.overlay;

import com.android.settings.connecteddevice.dock.DockUpdater;
import com.android.settings.connecteddevice.DevicePreferenceCallback;
import android.content.Context;

public interface DockUpdaterFeatureProvider
{
    DockUpdater getConnectedDockUpdater(final Context p0, final DevicePreferenceCallback p1);
    
    DockUpdater getSavedDockUpdater(final Context p0, final DevicePreferenceCallback p1);
}
