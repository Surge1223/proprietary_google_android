package com.android.settings.overlay;

import android.app.Activity;
import android.content.Context;

public interface SupportFeatureProvider
{
    String getNewDeviceIntroUrl(final Context p0);
    
    void startSupportV2(final Activity p0);
}
