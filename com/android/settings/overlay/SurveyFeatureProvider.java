package com.android.settings.overlay;

import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.content.BroadcastReceiver;
import android.app.Activity;

public interface SurveyFeatureProvider
{
    default void unregisterReceiver(final Activity activity, final BroadcastReceiver broadcastReceiver) {
        if (activity != null) {
            LocalBroadcastManager.getInstance((Context)activity).unregisterReceiver(broadcastReceiver);
            return;
        }
        throw new IllegalStateException("Cannot unregister receiver if activity is null");
    }
    
    BroadcastReceiver createAndRegisterReceiver(final Activity p0);
    
    void downloadSurvey(final Activity p0, final String p1, final String p2);
    
    long getSurveyExpirationDate(final Context p0, final String p1);
    
    String getSurveyId(final Context p0, final String p1);
    
    boolean showSurveyIfAvailable(final Activity p0, final String p1);
}
