package com.android.settings.overlay;

import com.android.settings.users.UserFeatureProvider;
import com.android.settings.dashboard.suggestions.SuggestionFeatureProvider;
import com.android.settings.slices.SlicesFeatureProvider;
import com.android.settings.security.SecurityFeatureProvider;
import com.android.settings.search.SearchFeatureProvider;
import com.android.settings.fuelgauge.PowerUsageFeatureProvider;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.localepicker.LocaleFeatureProvider;
import com.android.settings.enterprise.EnterprisePrivacyFeatureProvider;
import com.android.settings.search.DeviceIndexFeatureProvider;
import com.android.settings.dashboard.DashboardFeatureProvider;
import com.android.settings.gestures.AssistGestureFeatureProvider;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.accounts.AccountFeatureProvider;
import android.text.TextUtils;
import android.content.Context;

public abstract class FeatureFactory
{
    protected static FeatureFactory sFactory;
    
    public static FeatureFactory getFactory(final Context context) {
        if (FeatureFactory.sFactory != null) {
            return FeatureFactory.sFactory;
        }
        final String string = context.getString(2131887097);
        if (!TextUtils.isEmpty((CharSequence)string)) {
            try {
                return FeatureFactory.sFactory = (FeatureFactory)context.getClassLoader().loadClass(string).newInstance();
            }
            catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                final Object o;
                throw new FactoryNotFoundException((Throwable)o);
            }
        }
        throw new UnsupportedOperationException("No feature factory configured");
    }
    
    public abstract AccountFeatureProvider getAccountFeatureProvider();
    
    public abstract ApplicationFeatureProvider getApplicationFeatureProvider(final Context p0);
    
    public abstract AssistGestureFeatureProvider getAssistGestureFeatureProvider();
    
    public abstract DashboardFeatureProvider getDashboardFeatureProvider(final Context p0);
    
    public abstract DeviceIndexFeatureProvider getDeviceIndexFeatureProvider();
    
    public abstract DockUpdaterFeatureProvider getDockUpdaterFeatureProvider();
    
    public abstract EnterprisePrivacyFeatureProvider getEnterprisePrivacyFeatureProvider(final Context p0);
    
    public abstract LocaleFeatureProvider getLocaleFeatureProvider();
    
    public abstract MetricsFeatureProvider getMetricsFeatureProvider();
    
    public abstract PowerUsageFeatureProvider getPowerUsageFeatureProvider(final Context p0);
    
    public abstract SearchFeatureProvider getSearchFeatureProvider();
    
    public abstract SecurityFeatureProvider getSecurityFeatureProvider();
    
    public abstract SlicesFeatureProvider getSlicesFeatureProvider();
    
    public abstract SuggestionFeatureProvider getSuggestionFeatureProvider(final Context p0);
    
    public abstract SupportFeatureProvider getSupportFeatureProvider(final Context p0);
    
    public abstract SurveyFeatureProvider getSurveyFeatureProvider(final Context p0);
    
    public abstract UserFeatureProvider getUserFeatureProvider(final Context p0);
    
    public static final class FactoryNotFoundException extends RuntimeException
    {
        public FactoryNotFoundException(final Throwable t) {
            super("Unable to create factory. Did you misconfigure Proguard?", t);
        }
    }
}
