package com.android.settings.overlay;

import com.android.settings.users.UserFeatureProviderImpl;
import com.android.settings.dashboard.suggestions.SuggestionFeatureProviderImpl;
import com.android.settings.slices.SlicesFeatureProviderImpl;
import com.android.settings.security.SecurityFeatureProviderImpl;
import com.android.settings.search.SearchFeatureProviderImpl;
import com.android.settings.fuelgauge.PowerUsageFeatureProviderImpl;
import com.android.settings.localepicker.LocaleFeatureProviderImpl;
import com.android.settings.enterprise.EnterprisePrivacyFeatureProviderImpl;
import android.net.ConnectivityManager;
import android.os.UserManager;
import com.android.settings.connecteddevice.dock.DockUpdaterFeatureProviderImpl;
import com.android.settings.search.DeviceIndexFeatureProvider;
import com.android.settings.dashboard.DashboardFeatureProvider;
import com.android.settings.gestures.AssistGestureFeatureProviderImpl;
import com.android.settings.applications.ApplicationFeatureProviderImpl;
import android.app.admin.DevicePolicyManager;
import android.app.AppGlobals;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;
import com.android.settings.accounts.AccountFeatureProviderImpl;
import com.android.settings.users.UserFeatureProvider;
import com.android.settings.dashboard.suggestions.SuggestionFeatureProvider;
import com.android.settings.slices.SlicesFeatureProvider;
import com.android.settings.security.SecurityFeatureProvider;
import com.android.settings.search.SearchFeatureProvider;
import com.android.settings.fuelgauge.PowerUsageFeatureProvider;
import com.android.settingslib.core.instrumentation.MetricsFeatureProvider;
import com.android.settings.localepicker.LocaleFeatureProvider;
import com.android.settings.enterprise.EnterprisePrivacyFeatureProvider;
import com.android.settings.search.DeviceIndexFeatureProviderImpl;
import com.android.settings.dashboard.DashboardFeatureProviderImpl;
import com.android.settings.gestures.AssistGestureFeatureProvider;
import com.android.settings.applications.ApplicationFeatureProvider;
import com.android.settings.accounts.AccountFeatureProvider;
import android.support.annotation.Keep;

@Keep
public class FeatureFactoryImpl extends FeatureFactory
{
    private AccountFeatureProvider mAccountFeatureProvider;
    private ApplicationFeatureProvider mApplicationFeatureProvider;
    private AssistGestureFeatureProvider mAssistGestureFeatureProvider;
    private DashboardFeatureProviderImpl mDashboardFeatureProvider;
    private DeviceIndexFeatureProviderImpl mDeviceIndexFeatureProvider;
    private DockUpdaterFeatureProvider mDockUpdaterFeatureProvider;
    private EnterprisePrivacyFeatureProvider mEnterprisePrivacyFeatureProvider;
    private LocaleFeatureProvider mLocaleFeatureProvider;
    private MetricsFeatureProvider mMetricsFeatureProvider;
    private PowerUsageFeatureProvider mPowerUsageFeatureProvider;
    private SearchFeatureProvider mSearchFeatureProvider;
    private SecurityFeatureProvider mSecurityFeatureProvider;
    private SlicesFeatureProvider mSlicesFeatureProvider;
    private SuggestionFeatureProvider mSuggestionFeatureProvider;
    private UserFeatureProvider mUserFeatureProvider;
    
    @Override
    public AccountFeatureProvider getAccountFeatureProvider() {
        if (this.mAccountFeatureProvider == null) {
            this.mAccountFeatureProvider = new AccountFeatureProviderImpl();
        }
        return this.mAccountFeatureProvider;
    }
    
    @Override
    public ApplicationFeatureProvider getApplicationFeatureProvider(Context applicationContext) {
        if (this.mApplicationFeatureProvider == null) {
            applicationContext = applicationContext.getApplicationContext();
            this.mApplicationFeatureProvider = new ApplicationFeatureProviderImpl(applicationContext, new PackageManagerWrapper(applicationContext.getPackageManager()), AppGlobals.getPackageManager(), (DevicePolicyManager)applicationContext.getSystemService("device_policy"));
        }
        return this.mApplicationFeatureProvider;
    }
    
    @Override
    public AssistGestureFeatureProvider getAssistGestureFeatureProvider() {
        if (this.mAssistGestureFeatureProvider == null) {
            this.mAssistGestureFeatureProvider = new AssistGestureFeatureProviderImpl();
        }
        return this.mAssistGestureFeatureProvider;
    }
    
    @Override
    public DashboardFeatureProvider getDashboardFeatureProvider(final Context context) {
        if (this.mDashboardFeatureProvider == null) {
            this.mDashboardFeatureProvider = new DashboardFeatureProviderImpl(context.getApplicationContext());
        }
        return this.mDashboardFeatureProvider;
    }
    
    @Override
    public DeviceIndexFeatureProvider getDeviceIndexFeatureProvider() {
        if (this.mDeviceIndexFeatureProvider == null) {
            this.mDeviceIndexFeatureProvider = new DeviceIndexFeatureProviderImpl();
        }
        return this.mDeviceIndexFeatureProvider;
    }
    
    @Override
    public DockUpdaterFeatureProvider getDockUpdaterFeatureProvider() {
        if (this.mDockUpdaterFeatureProvider == null) {
            this.mDockUpdaterFeatureProvider = new DockUpdaterFeatureProviderImpl();
        }
        return this.mDockUpdaterFeatureProvider;
    }
    
    @Override
    public EnterprisePrivacyFeatureProvider getEnterprisePrivacyFeatureProvider(Context applicationContext) {
        if (this.mEnterprisePrivacyFeatureProvider == null) {
            applicationContext = applicationContext.getApplicationContext();
            this.mEnterprisePrivacyFeatureProvider = new EnterprisePrivacyFeatureProviderImpl(applicationContext, (DevicePolicyManager)applicationContext.getSystemService("device_policy"), new PackageManagerWrapper(applicationContext.getPackageManager()), UserManager.get(applicationContext), (ConnectivityManager)applicationContext.getSystemService("connectivity"), applicationContext.getResources());
        }
        return this.mEnterprisePrivacyFeatureProvider;
    }
    
    @Override
    public LocaleFeatureProvider getLocaleFeatureProvider() {
        if (this.mLocaleFeatureProvider == null) {
            this.mLocaleFeatureProvider = new LocaleFeatureProviderImpl();
        }
        return this.mLocaleFeatureProvider;
    }
    
    @Override
    public MetricsFeatureProvider getMetricsFeatureProvider() {
        if (this.mMetricsFeatureProvider == null) {
            this.mMetricsFeatureProvider = new MetricsFeatureProvider();
        }
        return this.mMetricsFeatureProvider;
    }
    
    @Override
    public PowerUsageFeatureProvider getPowerUsageFeatureProvider(final Context context) {
        if (this.mPowerUsageFeatureProvider == null) {
            this.mPowerUsageFeatureProvider = new PowerUsageFeatureProviderImpl(context.getApplicationContext());
        }
        return this.mPowerUsageFeatureProvider;
    }
    
    @Override
    public SearchFeatureProvider getSearchFeatureProvider() {
        if (this.mSearchFeatureProvider == null) {
            this.mSearchFeatureProvider = new SearchFeatureProviderImpl();
        }
        return this.mSearchFeatureProvider;
    }
    
    @Override
    public SecurityFeatureProvider getSecurityFeatureProvider() {
        if (this.mSecurityFeatureProvider == null) {
            this.mSecurityFeatureProvider = new SecurityFeatureProviderImpl();
        }
        return this.mSecurityFeatureProvider;
    }
    
    @Override
    public SlicesFeatureProvider getSlicesFeatureProvider() {
        if (this.mSlicesFeatureProvider == null) {
            this.mSlicesFeatureProvider = new SlicesFeatureProviderImpl();
        }
        return this.mSlicesFeatureProvider;
    }
    
    @Override
    public SuggestionFeatureProvider getSuggestionFeatureProvider(final Context context) {
        if (this.mSuggestionFeatureProvider == null) {
            this.mSuggestionFeatureProvider = new SuggestionFeatureProviderImpl(context.getApplicationContext());
        }
        return this.mSuggestionFeatureProvider;
    }
    
    @Override
    public SupportFeatureProvider getSupportFeatureProvider(final Context context) {
        return null;
    }
    
    @Override
    public SurveyFeatureProvider getSurveyFeatureProvider(final Context context) {
        return null;
    }
    
    @Override
    public UserFeatureProvider getUserFeatureProvider(final Context context) {
        if (this.mUserFeatureProvider == null) {
            this.mUserFeatureProvider = new UserFeatureProviderImpl(context.getApplicationContext());
        }
        return this.mUserFeatureProvider;
    }
}
