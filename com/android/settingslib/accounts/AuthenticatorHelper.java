package com.android.settingslib.accounts;

import android.os.AsyncTask;
import android.content.Intent;
import android.accounts.AccountManager;
import android.os.Handler;
import android.content.IntentFilter;
import android.content.res.Resources$NotFoundException;
import android.content.pm.PackageManager;
import android.content.SyncAdapterType;
import android.util.Log;
import android.content.ContentResolver;
import android.accounts.Account;
import android.os.UserHandle;
import android.accounts.AuthenticatorDescription;
import android.content.Context;
import java.util.ArrayList;
import java.util.HashMap;
import android.graphics.drawable.Drawable;
import java.util.Map;
import android.content.BroadcastReceiver;

public final class AuthenticatorHelper extends BroadcastReceiver
{
    private final Map<String, Drawable> mAccTypeIconCache;
    private final HashMap<String, ArrayList<String>> mAccountTypeToAuthorities;
    private final Context mContext;
    private final ArrayList<String> mEnabledAccountTypes;
    private final OnAccountsUpdateListener mListener;
    private boolean mListeningToAccountUpdates;
    private final Map<String, AuthenticatorDescription> mTypeToAuthDescription;
    private final UserHandle mUserHandle;
    
    public AuthenticatorHelper(final Context mContext, final UserHandle mUserHandle, final OnAccountsUpdateListener mListener) {
        this.mTypeToAuthDescription = new HashMap<String, AuthenticatorDescription>();
        this.mEnabledAccountTypes = new ArrayList<String>();
        this.mAccTypeIconCache = new HashMap<String, Drawable>();
        this.mAccountTypeToAuthorities = new HashMap<String, ArrayList<String>>();
        this.mContext = mContext;
        this.mUserHandle = mUserHandle;
        this.mListener = mListener;
        this.onAccountsUpdated(null);
    }
    
    private void buildAccountTypeToAuthoritiesMap() {
        this.mAccountTypeToAuthorities.clear();
        final SyncAdapterType[] syncAdapterTypesAsUser = ContentResolver.getSyncAdapterTypesAsUser(this.mUserHandle.getIdentifier());
        for (int i = 0; i < syncAdapterTypesAsUser.length; ++i) {
            final SyncAdapterType syncAdapterType = syncAdapterTypesAsUser[i];
            ArrayList<String> list;
            if ((list = this.mAccountTypeToAuthorities.get(syncAdapterType.accountType)) == null) {
                list = new ArrayList<String>();
                this.mAccountTypeToAuthorities.put(syncAdapterType.accountType, list);
            }
            if (Log.isLoggable("AuthenticatorHelper", 2)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Added authority ");
                sb.append(syncAdapterType.authority);
                sb.append(" to accountType ");
                sb.append(syncAdapterType.accountType);
                Log.v("AuthenticatorHelper", sb.toString());
            }
            list.add(syncAdapterType.authority);
        }
    }
    
    public boolean containsAccountType(final String s) {
        return this.mTypeToAuthDescription.containsKey(s);
    }
    
    public AuthenticatorDescription getAccountTypeDescription(final String s) {
        return this.mTypeToAuthDescription.get(s);
    }
    
    public ArrayList<String> getAuthoritiesForAccountType(final String s) {
        return this.mAccountTypeToAuthorities.get(s);
    }
    
    public Drawable getDrawableForType(final Context context, final String s) {
        Drawable userBadgedIcon = null;
        final Drawable drawable = null;
        Object o = this.mAccTypeIconCache;
        synchronized (o) {
            if (this.mAccTypeIconCache.containsKey(s)) {
                return this.mAccTypeIconCache.get(s);
            }
            // monitorexit(o)
            if (this.mTypeToAuthDescription.containsKey(s)) {
                userBadgedIcon = drawable;
                try {
                    o = this.mTypeToAuthDescription.get(s);
                    userBadgedIcon = drawable;
                    final Context packageContextAsUser = context.createPackageContextAsUser(((AuthenticatorDescription)o).packageName, 0, this.mUserHandle);
                    userBadgedIcon = drawable;
                    final Drawable drawable2 = userBadgedIcon = this.mContext.getPackageManager().getUserBadgedIcon(packageContextAsUser.getDrawable(((AuthenticatorDescription)o).iconId), this.mUserHandle);
                    o = this.mAccTypeIconCache;
                    userBadgedIcon = drawable2;
                    // monitorenter(o)
                    try {
                        this.mAccTypeIconCache.put(s, drawable2);
                    }
                    finally {
                        // monitorexit(o)
                        userBadgedIcon = drawable2;
                    }
                }
                catch (PackageManager$NameNotFoundException ex) {}
                catch (Resources$NotFoundException ex2) {}
            }
            Drawable defaultActivityIcon;
            if ((defaultActivityIcon = userBadgedIcon) == null) {
                defaultActivityIcon = context.getPackageManager().getDefaultActivityIcon();
            }
            return defaultActivityIcon;
        }
    }
    
    public String[] getEnabledAccountTypes() {
        return this.mEnabledAccountTypes.toArray(new String[this.mEnabledAccountTypes.size()]);
    }
    
    public CharSequence getLabelForType(final Context context, final String s) {
        final CharSequence charSequence = null;
        final CharSequence charSequence2 = null;
        CharSequence charSequence3 = charSequence;
        if (this.mTypeToAuthDescription.containsKey(s)) {
            while (true) {
                try {
                    final AuthenticatorDescription authenticatorDescription = this.mTypeToAuthDescription.get(s);
                    final CharSequence text = context.createPackageContextAsUser(authenticatorDescription.packageName, 0, this.mUserHandle).getResources().getText(authenticatorDescription.labelId);
                    charSequence3 = text;
                }
                catch (Resources$NotFoundException ex) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("No label icon for account type ");
                    sb.append(s);
                    Log.w("AuthenticatorHelper", sb.toString());
                    charSequence3 = charSequence;
                }
                catch (PackageManager$NameNotFoundException ex2) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("No label name for account type ");
                    sb2.append(s);
                    Log.w("AuthenticatorHelper", sb2.toString());
                    final CharSequence text = charSequence2;
                    continue;
                }
                break;
            }
        }
        return charSequence3;
    }
    
    public int getLabelIdForType(final String s) {
        if (this.mTypeToAuthDescription.containsKey(s)) {
            return this.mTypeToAuthDescription.get(s).labelId;
        }
        return -1;
    }
    
    public String getPackageForType(final String s) {
        if (this.mTypeToAuthDescription.containsKey(s)) {
            return this.mTypeToAuthDescription.get(s).packageName;
        }
        return null;
    }
    
    public void listenToAccountUpdates() {
        if (!this.mListeningToAccountUpdates) {
            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.accounts.LOGIN_ACCOUNTS_CHANGED");
            intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
            this.mContext.registerReceiverAsUser((BroadcastReceiver)this, this.mUserHandle, intentFilter, (String)null, (Handler)null);
            this.mListeningToAccountUpdates = true;
        }
    }
    
    void onAccountsUpdated(final Account[] array) {
        this.updateAuthDescriptions(this.mContext);
        Account[] accountsAsUser = array;
        if (array == null) {
            accountsAsUser = AccountManager.get(this.mContext).getAccountsAsUser(this.mUserHandle.getIdentifier());
        }
        this.mEnabledAccountTypes.clear();
        this.mAccTypeIconCache.clear();
        for (int i = 0; i < accountsAsUser.length; ++i) {
            final Account account = accountsAsUser[i];
            if (!this.mEnabledAccountTypes.contains(account.type)) {
                this.mEnabledAccountTypes.add(account.type);
            }
        }
        this.buildAccountTypeToAuthoritiesMap();
        if (this.mListeningToAccountUpdates) {
            this.mListener.onAccountsUpdate(this.mUserHandle);
        }
    }
    
    public void onReceive(final Context context, final Intent intent) {
        this.onAccountsUpdated(AccountManager.get(this.mContext).getAccountsAsUser(this.mUserHandle.getIdentifier()));
    }
    
    public void preloadDrawableForType(final Context context, final String s) {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(final Void... array) {
                AuthenticatorHelper.this.getDrawableForType(context, s);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])null);
    }
    
    public void stopListeningToAccountUpdates() {
        if (this.mListeningToAccountUpdates) {
            this.mContext.unregisterReceiver((BroadcastReceiver)this);
            this.mListeningToAccountUpdates = false;
        }
    }
    
    public void updateAuthDescriptions(final Context context) {
        final AuthenticatorDescription[] authenticatorTypesAsUser = AccountManager.get(context).getAuthenticatorTypesAsUser(this.mUserHandle.getIdentifier());
        for (int i = 0; i < authenticatorTypesAsUser.length; ++i) {
            this.mTypeToAuthDescription.put(authenticatorTypesAsUser[i].type, authenticatorTypesAsUser[i]);
        }
    }
    
    public interface OnAccountsUpdateListener
    {
        void onAccountsUpdate(final UserHandle p0);
    }
}
