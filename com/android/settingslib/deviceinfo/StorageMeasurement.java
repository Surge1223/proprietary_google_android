package com.android.settingslib.deviceinfo;

import android.util.SparseArray;
import android.os.AsyncTask;
import android.app.usage.StorageStats;
import android.app.usage.ExternalStorageStats;
import java.util.Iterator;
import java.util.List;
import java.io.IOException;
import android.os.Environment;
import android.os.UserHandle;
import java.util.HashMap;
import android.content.pm.UserInfo;
import android.util.Log;
import android.os.SystemClock;
import android.util.SparseLongArray;
import android.os.UserManager;
import android.app.usage.StorageStatsManager;
import android.os.storage.VolumeInfo;
import java.lang.ref.WeakReference;
import android.content.Context;

public class StorageMeasurement
{
    private final Context mContext;
    private WeakReference<MeasurementReceiver> mReceiver;
    private final VolumeInfo mSharedVolume;
    private final StorageStatsManager mStats;
    private final UserManager mUser;
    private final VolumeInfo mVolume;
    
    public StorageMeasurement(final Context context, final VolumeInfo mVolume, final VolumeInfo mSharedVolume) {
        this.mContext = context.getApplicationContext();
        this.mUser = (UserManager)this.mContext.getSystemService((Class)UserManager.class);
        this.mStats = (StorageStatsManager)this.mContext.getSystemService((Class)StorageStatsManager.class);
        this.mVolume = mVolume;
        this.mSharedVolume = mSharedVolume;
    }
    
    private static void addValue(final SparseLongArray sparseLongArray, final int n, final long n2) {
        sparseLongArray.put(n, sparseLongArray.get(n) + n2);
    }
    
    private MeasurementDetails measureExactStorage() {
        final List users = this.mUser.getUsers();
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final MeasurementDetails measurementDetails = new MeasurementDetails();
        if (this.mVolume == null) {
            return measurementDetails;
        }
        if (this.mVolume.getType() == 0) {
            measurementDetails.totalSize = this.mVolume.getPath().getTotalSpace();
            measurementDetails.availSize = this.mVolume.getPath().getUsableSpace();
            return measurementDetails;
        }
        try {
            measurementDetails.totalSize = this.mStats.getTotalBytes(this.mVolume.fsUuid);
            measurementDetails.availSize = this.mStats.getFreeBytes(this.mVolume.fsUuid);
            final long elapsedRealtime2 = SystemClock.elapsedRealtime();
            final StringBuilder sb = new StringBuilder();
            sb.append("Measured total storage in ");
            sb.append(elapsedRealtime2 - elapsedRealtime);
            sb.append("ms");
            Log.d("StorageMeasurement", sb.toString());
            if (this.mSharedVolume != null && this.mSharedVolume.isMountedReadable()) {
                for (final UserInfo userInfo : users) {
                    final HashMap<String, Long> hashMap = new HashMap<String, Long>();
                    measurementDetails.mediaSize.put(userInfo.id, (Object)hashMap);
                    try {
                        final ExternalStorageStats queryExternalStatsForUser = this.mStats.queryExternalStatsForUser(this.mSharedVolume.fsUuid, UserHandle.of(userInfo.id));
                        addValue(measurementDetails.usersSize, userInfo.id, queryExternalStatsForUser.getTotalBytes());
                        hashMap.put(Environment.DIRECTORY_MUSIC, queryExternalStatsForUser.getAudioBytes());
                        hashMap.put(Environment.DIRECTORY_MOVIES, queryExternalStatsForUser.getVideoBytes());
                        hashMap.put(Environment.DIRECTORY_PICTURES, queryExternalStatsForUser.getImageBytes());
                        addValue(measurementDetails.miscSize, userInfo.id, queryExternalStatsForUser.getTotalBytes() - queryExternalStatsForUser.getAudioBytes() - queryExternalStatsForUser.getVideoBytes() - queryExternalStatsForUser.getImageBytes());
                    }
                    catch (IOException ex) {
                        Log.w("StorageMeasurement", (Throwable)ex);
                    }
                }
            }
            final long elapsedRealtime3 = SystemClock.elapsedRealtime();
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Measured shared storage in ");
            sb2.append(elapsedRealtime3 - elapsedRealtime2);
            sb2.append("ms");
            Log.d("StorageMeasurement", sb2.toString());
            if (this.mVolume.getType() == 1 && this.mVolume.isMountedReadable()) {
                for (final UserInfo userInfo2 : users) {
                    try {
                        final StorageStats queryStatsForUser = this.mStats.queryStatsForUser(this.mVolume.fsUuid, UserHandle.of(userInfo2.id));
                        if (userInfo2.id == UserHandle.myUserId()) {
                            addValue(measurementDetails.usersSize, userInfo2.id, queryStatsForUser.getCodeBytes());
                        }
                        addValue(measurementDetails.usersSize, userInfo2.id, queryStatsForUser.getDataBytes());
                        addValue(measurementDetails.appsSize, userInfo2.id, queryStatsForUser.getCodeBytes() + queryStatsForUser.getDataBytes());
                        measurementDetails.cacheSize += queryStatsForUser.getCacheBytes();
                    }
                    catch (IOException ex2) {
                        Log.w("StorageMeasurement", (Throwable)ex2);
                    }
                }
            }
            final long elapsedRealtime4 = SystemClock.elapsedRealtime();
            final StringBuilder sb3 = new StringBuilder();
            sb3.append("Measured private storage in ");
            sb3.append(elapsedRealtime4 - elapsedRealtime3);
            sb3.append("ms");
            Log.d("StorageMeasurement", sb3.toString());
            return measurementDetails;
        }
        catch (IOException ex3) {
            Log.w("StorageMeasurement", (Throwable)ex3);
            return measurementDetails;
        }
    }
    
    public void forceMeasure() {
        this.measure();
    }
    
    public void measure() {
        new MeasureTask().execute((Object[])new Void[0]);
    }
    
    public void onDestroy() {
        this.mReceiver = null;
    }
    
    public void setReceiver(final MeasurementReceiver measurementReceiver) {
        if (this.mReceiver == null || this.mReceiver.get() == null) {
            this.mReceiver = new WeakReference<MeasurementReceiver>(measurementReceiver);
        }
    }
    
    private class MeasureTask extends AsyncTask<Void, Void, MeasurementDetails>
    {
        protected MeasurementDetails doInBackground(final Void... array) {
            return StorageMeasurement.this.measureExactStorage();
        }
        
        protected void onPostExecute(final MeasurementDetails measurementDetails) {
            MeasurementReceiver measurementReceiver;
            if (StorageMeasurement.this.mReceiver != null) {
                measurementReceiver = (MeasurementReceiver)StorageMeasurement.this.mReceiver.get();
            }
            else {
                measurementReceiver = null;
            }
            if (measurementReceiver != null) {
                measurementReceiver.onDetailsChanged(measurementDetails);
            }
        }
    }
    
    public static class MeasurementDetails
    {
        public SparseLongArray appsSize;
        public long availSize;
        public long cacheSize;
        public SparseArray<HashMap<String, Long>> mediaSize;
        public SparseLongArray miscSize;
        public long totalSize;
        public SparseLongArray usersSize;
        
        public MeasurementDetails() {
            this.usersSize = new SparseLongArray();
            this.appsSize = new SparseLongArray();
            this.mediaSize = (SparseArray<HashMap<String, Long>>)new SparseArray();
            this.miscSize = new SparseLongArray();
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("MeasurementDetails: [totalSize: ");
            sb.append(this.totalSize);
            sb.append(" availSize: ");
            sb.append(this.availSize);
            sb.append(" cacheSize: ");
            sb.append(this.cacheSize);
            sb.append(" mediaSize: ");
            sb.append(this.mediaSize);
            sb.append(" miscSize: ");
            sb.append(this.miscSize);
            sb.append("usersSize: ");
            sb.append(this.usersSize);
            sb.append("]");
            return sb.toString();
        }
    }
    
    public interface MeasurementReceiver
    {
        void onDetailsChanged(final MeasurementDetails p0);
    }
}
