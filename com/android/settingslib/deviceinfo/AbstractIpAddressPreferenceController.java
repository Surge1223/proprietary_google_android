package com.android.settingslib.deviceinfo;

import com.android.settingslib.R;
import android.support.v7.preference.PreferenceScreen;
import java.util.Iterator;
import java.net.InetAddress;
import android.net.LinkProperties;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;
import android.net.ConnectivityManager;

public abstract class AbstractIpAddressPreferenceController extends AbstractConnectivityPreferenceController
{
    private static final String[] CONNECTIVITY_INTENTS;
    static final String KEY_IP_ADDRESS = "wifi_ip_address";
    private final ConnectivityManager mCM;
    private Preference mIpAddress;
    
    static {
        CONNECTIVITY_INTENTS = new String[] { "android.net.conn.CONNECTIVITY_CHANGE", "android.net.wifi.LINK_CONFIGURATION_CHANGED", "android.net.wifi.STATE_CHANGE" };
    }
    
    public AbstractIpAddressPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, lifecycle);
        this.mCM = (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class);
    }
    
    private static String formatIpAddresses(final LinkProperties linkProperties) {
        if (linkProperties == null) {
            return null;
        }
        final Iterator<InetAddress> iterator = (Iterator<InetAddress>)linkProperties.getAllAddresses().iterator();
        if (!iterator.hasNext()) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        while (iterator.hasNext()) {
            sb.append(iterator.next().getHostAddress());
            if (iterator.hasNext()) {
                sb.append("\n");
            }
        }
        return sb.toString();
    }
    
    private static String getDefaultIpAddresses(final ConnectivityManager connectivityManager) {
        return formatIpAddresses(connectivityManager.getActiveLinkProperties());
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mIpAddress = preferenceScreen.findPreference("wifi_ip_address");
        this.updateConnectivity();
    }
    
    @Override
    protected String[] getConnectivityIntents() {
        return AbstractIpAddressPreferenceController.CONNECTIVITY_INTENTS;
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_ip_address";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @Override
    protected void updateConnectivity() {
        final String defaultIpAddresses = getDefaultIpAddresses(this.mCM);
        if (defaultIpAddresses != null) {
            this.mIpAddress.setSummary(defaultIpAddresses);
        }
        else {
            this.mIpAddress.setSummary(R.string.status_unavailable);
        }
    }
}
