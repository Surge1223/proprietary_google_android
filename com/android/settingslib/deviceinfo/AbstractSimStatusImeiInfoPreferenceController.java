package com.android.settingslib.deviceinfo;

import com.android.settingslib.Utils;
import android.os.UserManager;
import android.content.Context;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class AbstractSimStatusImeiInfoPreferenceController extends AbstractPreferenceController
{
    public AbstractSimStatusImeiInfoPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public boolean isAvailable() {
        return ((UserManager)this.mContext.getSystemService((Class)UserManager.class)).isAdminUser() && !Utils.isWifiOnly(this.mContext);
    }
}
