package com.android.settingslib.deviceinfo;

import java.util.List;
import java.io.IOException;
import android.app.usage.StorageStatsManager;
import android.os.storage.VolumeInfo;

public interface StorageVolumeProvider
{
    VolumeInfo findEmulatedForPrivate(final VolumeInfo p0);
    
    long getFreeBytes(final StorageStatsManager p0, final VolumeInfo p1) throws IOException;
    
    long getTotalBytes(final StorageStatsManager p0, final VolumeInfo p1) throws IOException;
    
    List<VolumeInfo> getVolumes();
}
