package com.android.settingslib.deviceinfo;

import android.annotation.SuppressLint;
import com.android.settingslib.R;
import android.text.TextUtils;
import android.bluetooth.BluetoothAdapter;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.support.v7.preference.Preference;

public abstract class AbstractBluetoothAddressPreferenceController extends AbstractConnectivityPreferenceController
{
    private static final String[] CONNECTIVITY_INTENTS;
    static final String KEY_BT_ADDRESS = "bt_address";
    private Preference mBtAddress;
    
    static {
        CONNECTIVITY_INTENTS = new String[] { "android.bluetooth.adapter.action.STATE_CHANGED" };
    }
    
    public AbstractBluetoothAddressPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, lifecycle);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mBtAddress = preferenceScreen.findPreference("bt_address");
        this.updateConnectivity();
    }
    
    @Override
    protected String[] getConnectivityIntents() {
        return AbstractBluetoothAddressPreferenceController.CONNECTIVITY_INTENTS;
    }
    
    @Override
    public String getPreferenceKey() {
        return "bt_address";
    }
    
    @Override
    public boolean isAvailable() {
        return BluetoothAdapter.getDefaultAdapter() != null;
    }
    
    @SuppressLint({ "HardwareIds" })
    @Override
    protected void updateConnectivity() {
        final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter != null && this.mBtAddress != null) {
            String address;
            if (defaultAdapter.isEnabled()) {
                address = defaultAdapter.getAddress();
            }
            else {
                address = null;
            }
            if (!TextUtils.isEmpty((CharSequence)address)) {
                this.mBtAddress.setSummary(address.toLowerCase());
            }
            else {
                this.mBtAddress.setSummary(R.string.status_unavailable);
            }
        }
    }
}
