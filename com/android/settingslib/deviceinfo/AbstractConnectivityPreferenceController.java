package com.android.settingslib.deviceinfo;

import android.os.Message;
import java.lang.ref.WeakReference;
import android.content.IntentFilter;
import com.android.internal.util.ArrayUtils;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.os.Handler;
import android.content.BroadcastReceiver;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class AbstractConnectivityPreferenceController extends AbstractPreferenceController implements LifecycleObserver, OnStart, OnStop
{
    private final BroadcastReceiver mConnectivityReceiver;
    private Handler mHandler;
    
    public AbstractConnectivityPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mConnectivityReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                if (ArrayUtils.contains((Object[])AbstractConnectivityPreferenceController.this.getConnectivityIntents(), (Object)intent.getAction())) {
                    AbstractConnectivityPreferenceController.this.getHandler().sendEmptyMessage(600);
                }
            }
        };
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private Handler getHandler() {
        if (this.mHandler == null) {
            this.mHandler = new ConnectivityEventHandler(this);
        }
        return this.mHandler;
    }
    
    protected abstract String[] getConnectivityIntents();
    
    @Override
    public void onStart() {
        final IntentFilter intentFilter = new IntentFilter();
        final String[] connectivityIntents = this.getConnectivityIntents();
        for (int length = connectivityIntents.length, i = 0; i < length; ++i) {
            intentFilter.addAction(connectivityIntents[i]);
        }
        this.mContext.registerReceiver(this.mConnectivityReceiver, intentFilter, "android.permission.CHANGE_NETWORK_STATE", (Handler)null);
    }
    
    @Override
    public void onStop() {
        this.mContext.unregisterReceiver(this.mConnectivityReceiver);
    }
    
    protected abstract void updateConnectivity();
    
    private static class ConnectivityEventHandler extends Handler
    {
        private WeakReference<AbstractConnectivityPreferenceController> mPreferenceController;
        
        public ConnectivityEventHandler(final AbstractConnectivityPreferenceController abstractConnectivityPreferenceController) {
            this.mPreferenceController = new WeakReference<AbstractConnectivityPreferenceController>(abstractConnectivityPreferenceController);
        }
        
        public void handleMessage(final Message message) {
            final AbstractConnectivityPreferenceController abstractConnectivityPreferenceController = this.mPreferenceController.get();
            if (abstractConnectivityPreferenceController == null) {
                return;
            }
            if (message.what == 600) {
                abstractConnectivityPreferenceController.updateConnectivity();
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown message ");
            sb.append(message.what);
            throw new IllegalStateException(sb.toString());
        }
    }
}
