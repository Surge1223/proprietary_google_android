package com.android.settingslib.deviceinfo;

import com.android.settingslib.R;
import android.telephony.TelephonyManager;
import android.os.PersistableBundle;
import android.telephony.SubscriptionManager;
import android.telephony.CarrierConfigManager;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;

public abstract class AbstractImsStatusPreferenceController extends AbstractConnectivityPreferenceController
{
    private static final String[] CONNECTIVITY_INTENTS;
    static final String KEY_IMS_REGISTRATION_STATE = "ims_reg_state";
    private Preference mImsStatus;
    
    static {
        CONNECTIVITY_INTENTS = new String[] { "android.bluetooth.adapter.action.STATE_CHANGED", "android.net.conn.CONNECTIVITY_CHANGE", "android.net.wifi.LINK_CONFIGURATION_CHANGED", "android.net.wifi.STATE_CHANGE" };
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mImsStatus = preferenceScreen.findPreference("ims_reg_state");
        this.updateConnectivity();
    }
    
    @Override
    protected String[] getConnectivityIntents() {
        return AbstractImsStatusPreferenceController.CONNECTIVITY_INTENTS;
    }
    
    @Override
    public String getPreferenceKey() {
        return "ims_reg_state";
    }
    
    @Override
    public boolean isAvailable() {
        final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)this.mContext.getSystemService((Class)CarrierConfigManager.class);
        final int defaultDataSubscriptionId = SubscriptionManager.getDefaultDataSubscriptionId();
        PersistableBundle configForSubId = null;
        if (carrierConfigManager != null) {
            configForSubId = carrierConfigManager.getConfigForSubId(defaultDataSubscriptionId);
        }
        return configForSubId != null && configForSubId.getBoolean("show_ims_registration_status_bool");
    }
    
    @Override
    protected void updateConnectivity() {
        final int defaultDataSubscriptionId = SubscriptionManager.getDefaultDataSubscriptionId();
        if (this.mImsStatus != null) {
            final TelephonyManager telephonyManager = (TelephonyManager)this.mContext.getSystemService((Class)TelephonyManager.class);
            final Preference mImsStatus = this.mImsStatus;
            int summary;
            if (telephonyManager != null && telephonyManager.isImsRegistered(defaultDataSubscriptionId)) {
                summary = R.string.ims_reg_status_registered;
            }
            else {
                summary = R.string.ims_reg_status_not_registered;
            }
            mImsStatus.setSummary(summary);
        }
    }
}
