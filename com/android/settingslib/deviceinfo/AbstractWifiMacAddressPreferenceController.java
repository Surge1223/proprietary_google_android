package com.android.settingslib.deviceinfo;

import android.annotation.SuppressLint;
import android.net.wifi.WifiInfo;
import com.android.settingslib.R;
import android.text.TextUtils;
import android.provider.Settings;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v7.preference.Preference;

public abstract class AbstractWifiMacAddressPreferenceController extends AbstractConnectivityPreferenceController
{
    private static final String[] CONNECTIVITY_INTENTS;
    static final String KEY_WIFI_MAC_ADDRESS = "wifi_mac_address";
    private Preference mWifiMacAddress;
    private final WifiManager mWifiManager;
    
    static {
        CONNECTIVITY_INTENTS = new String[] { "android.net.conn.CONNECTIVITY_CHANGE", "android.net.wifi.LINK_CONFIGURATION_CHANGED", "android.net.wifi.STATE_CHANGE" };
    }
    
    public AbstractWifiMacAddressPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context, lifecycle);
        this.mWifiManager = (WifiManager)context.getSystemService((Class)WifiManager.class);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mWifiMacAddress = preferenceScreen.findPreference("wifi_mac_address");
        this.updateConnectivity();
    }
    
    @Override
    protected String[] getConnectivityIntents() {
        return AbstractWifiMacAddressPreferenceController.CONNECTIVITY_INTENTS;
    }
    
    @Override
    public String getPreferenceKey() {
        return "wifi_mac_address";
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    @SuppressLint({ "HardwareIds" })
    @Override
    protected void updateConnectivity() {
        final WifiInfo connectionInfo = this.mWifiManager.getConnectionInfo();
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "wifi_connected_mac_randomization_enabled", 0);
        CharSequence macAddress;
        if (connectionInfo == null) {
            macAddress = null;
        }
        else {
            macAddress = connectionInfo.getMacAddress();
        }
        if (TextUtils.isEmpty(macAddress)) {
            this.mWifiMacAddress.setSummary(R.string.status_unavailable);
        }
        else if (int1 == 1 && "02:00:00:00:00:00".equals(macAddress)) {
            this.mWifiMacAddress.setSummary(R.string.wifi_status_mac_randomized);
        }
        else {
            this.mWifiMacAddress.setSummary(macAddress);
        }
    }
}
