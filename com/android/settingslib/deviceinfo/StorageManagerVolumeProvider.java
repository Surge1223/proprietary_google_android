package com.android.settingslib.deviceinfo;

import java.util.List;
import java.io.IOException;
import android.app.usage.StorageStatsManager;
import android.os.storage.VolumeInfo;
import android.os.storage.StorageManager;

public class StorageManagerVolumeProvider implements StorageVolumeProvider
{
    private StorageManager mStorageManager;
    
    public StorageManagerVolumeProvider(final StorageManager mStorageManager) {
        this.mStorageManager = mStorageManager;
    }
    
    @Override
    public VolumeInfo findEmulatedForPrivate(final VolumeInfo volumeInfo) {
        return this.mStorageManager.findEmulatedForPrivate(volumeInfo);
    }
    
    @Override
    public long getFreeBytes(final StorageStatsManager storageStatsManager, final VolumeInfo volumeInfo) throws IOException {
        return storageStatsManager.getFreeBytes(volumeInfo.getFsUuid());
    }
    
    @Override
    public long getTotalBytes(final StorageStatsManager storageStatsManager, final VolumeInfo volumeInfo) throws IOException {
        return storageStatsManager.getTotalBytes(volumeInfo.getFsUuid());
    }
    
    @Override
    public List<VolumeInfo> getVolumes() {
        return (List<VolumeInfo>)this.mStorageManager.getVolumes();
    }
}
