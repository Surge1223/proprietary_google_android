package com.android.settingslib.deviceinfo;

import android.content.Context;
import java.util.Iterator;
import java.io.IOException;
import android.util.Log;
import android.os.storage.VolumeInfo;
import android.app.AppGlobals;
import android.app.usage.StorageStatsManager;

public class PrivateStorageInfo
{
    public final long freeBytes;
    public final long totalBytes;
    
    public PrivateStorageInfo(final long freeBytes, final long totalBytes) {
        this.freeBytes = freeBytes;
        this.totalBytes = totalBytes;
    }
    
    public static PrivateStorageInfo getPrivateStorageInfo(final StorageVolumeProvider storageVolumeProvider) {
        final StorageStatsManager storageStatsManager = (StorageStatsManager)((Context)AppGlobals.getInitialApplication()).getSystemService((Class)StorageStatsManager.class);
        long n = 0L;
        long n2 = 0L;
        for (final VolumeInfo volumeInfo : storageVolumeProvider.getVolumes()) {
            long n3 = n;
            long n4 = n2;
            if (volumeInfo.getType() == 1) {
                n3 = n;
                n4 = n2;
                if (volumeInfo.isMountedReadable()) {
                    n4 = n2;
                    try {
                        n4 = n2 + storageVolumeProvider.getTotalBytes(storageStatsManager, volumeInfo);
                        n3 = n + storageVolumeProvider.getFreeBytes(storageStatsManager, volumeInfo);
                        n4 = n4;
                    }
                    catch (IOException ex) {
                        Log.w("PrivateStorageInfo", (Throwable)ex);
                        n3 = n;
                    }
                }
            }
            n = n3;
            n2 = n4;
        }
        return new PrivateStorageInfo(n, n2);
    }
    
    public static long getTotalSize(final VolumeInfo volumeInfo, long totalBytes) {
        final StorageStatsManager storageStatsManager = (StorageStatsManager)((Context)AppGlobals.getInitialApplication()).getSystemService((Class)StorageStatsManager.class);
        try {
            totalBytes = storageStatsManager.getTotalBytes(volumeInfo.getFsUuid());
            return totalBytes;
        }
        catch (IOException ex) {
            Log.w("PrivateStorageInfo", (Throwable)ex);
            return 0L;
        }
    }
}
