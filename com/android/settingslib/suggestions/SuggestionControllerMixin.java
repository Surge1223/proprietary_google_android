package com.android.settingslib.suggestions;

import android.arch.lifecycle.OnLifecycleEvent;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.content.ComponentName;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.arch.lifecycle.LifecycleObserver;
import android.service.settings.suggestions.Suggestion;
import java.util.List;
import android.app.LoaderManager$LoaderCallbacks;

public class SuggestionControllerMixin implements LoaderManager$LoaderCallbacks<List<Suggestion>>, LifecycleObserver, ServiceConnectionListener
{
    private final Context mContext;
    private final SuggestionControllerHost mHost;
    private final SuggestionController mSuggestionController;
    private boolean mSuggestionLoaded;
    
    public SuggestionControllerMixin(final Context context, final SuggestionControllerHost mHost, final Lifecycle lifecycle, final ComponentName componentName) {
        this.mContext = context.getApplicationContext();
        this.mHost = mHost;
        this.mSuggestionController = new SuggestionController(this.mContext, componentName, (SuggestionController.ServiceConnectionListener)this);
        if (lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    public void dismissSuggestion(final Suggestion suggestion) {
        this.mSuggestionController.dismissSuggestions(suggestion);
    }
    
    public boolean isSuggestionLoaded() {
        return this.mSuggestionLoaded;
    }
    
    public void launchSuggestion(final Suggestion suggestion) {
        this.mSuggestionController.launchSuggestion(suggestion);
    }
    
    public Loader<List<Suggestion>> onCreateLoader(final int n, final Bundle bundle) {
        if (n == 42) {
            this.mSuggestionLoaded = false;
            return (Loader<List<Suggestion>>)new SuggestionLoader(this.mContext, this.mSuggestionController);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("This loader id is not supported ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public void onLoadFinished(final Loader<List<Suggestion>> loader, final List<Suggestion> list) {
        this.mSuggestionLoaded = true;
        this.mHost.onSuggestionReady(list);
    }
    
    public void onLoaderReset(final Loader<List<Suggestion>> loader) {
        this.mSuggestionLoaded = false;
    }
    
    public void onServiceConnected() {
        final LoaderManager loaderManager = this.mHost.getLoaderManager();
        if (loaderManager != null) {
            loaderManager.restartLoader(42, (Bundle)null, (LoaderManager$LoaderCallbacks)this);
        }
    }
    
    public void onServiceDisconnected() {
        final LoaderManager loaderManager = this.mHost.getLoaderManager();
        if (loaderManager != null) {
            loaderManager.destroyLoader(42);
        }
    }
    
    @OnLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_START)
    public void onStart() {
        this.mSuggestionController.start();
    }
    
    @OnLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_STOP)
    public void onStop() {
        this.mSuggestionController.stop();
    }
    
    public interface SuggestionControllerHost
    {
        LoaderManager getLoaderManager();
        
        void onSuggestionReady(final List<Suggestion> p0);
    }
}
