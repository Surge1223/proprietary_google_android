package com.android.settingslib.suggestions;

import android.os.Process;
import java.util.List;
import android.os.RemoteException;
import android.util.Log;
import android.service.settings.suggestions.Suggestion;
import android.service.settings.suggestions.ISuggestionService$Stub;
import android.os.IBinder;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.service.settings.suggestions.ISuggestionService;
import android.content.Context;

public class SuggestionController
{
    private ServiceConnectionListener mConnectionListener;
    private final Context mContext;
    private ISuggestionService mRemoteService;
    private ServiceConnection mServiceConnection;
    private final Intent mServiceIntent;
    
    public SuggestionController(final Context context, final ComponentName component, final ServiceConnectionListener mConnectionListener) {
        this.mContext = context.getApplicationContext();
        this.mConnectionListener = mConnectionListener;
        this.mServiceIntent = new Intent().setComponent(component);
        this.mServiceConnection = this.createServiceConnection();
    }
    
    private ServiceConnection createServiceConnection() {
        return (ServiceConnection)new ServiceConnection() {
            public void onServiceConnected(final ComponentName componentName, final IBinder binder) {
                SuggestionController.this.mRemoteService = ISuggestionService$Stub.asInterface(binder);
                if (SuggestionController.this.mConnectionListener != null) {
                    SuggestionController.this.mConnectionListener.onServiceConnected();
                }
            }
            
            public void onServiceDisconnected(final ComponentName componentName) {
                if (SuggestionController.this.mConnectionListener != null) {
                    SuggestionController.this.mRemoteService = null;
                    SuggestionController.this.mConnectionListener.onServiceDisconnected();
                }
            }
        };
    }
    
    private boolean isReady() {
        return this.mRemoteService != null;
    }
    
    public void dismissSuggestions(final Suggestion suggestion) {
        if (!this.isReady()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SuggestionController not ready, cannot dismiss ");
            sb.append(suggestion.getId());
            Log.w("SuggestionController", sb.toString());
            return;
        }
        try {
            this.mRemoteService.dismissSuggestion(suggestion);
        }
        catch (RemoteException | RuntimeException ex) {
            final Throwable t;
            Log.w("SuggestionController", "Error when calling dismissSuggestion()", t);
        }
    }
    
    public List<Suggestion> getSuggestions() {
        if (!this.isReady()) {
            return null;
        }
        try {
            return (List<Suggestion>)this.mRemoteService.getSuggestions();
        }
        catch (RemoteException | RuntimeException ex2) {
            final Throwable t;
            Log.w("SuggestionController", "Error when calling getSuggestion()", t);
            return null;
        }
        catch (NullPointerException ex) {
            Log.w("SuggestionController", "mRemote service detached before able to query", (Throwable)ex);
            return null;
        }
    }
    
    public void launchSuggestion(final Suggestion suggestion) {
        if (!this.isReady()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("SuggestionController not ready, cannot launch ");
            sb.append(suggestion.getId());
            Log.w("SuggestionController", sb.toString());
            return;
        }
        try {
            this.mRemoteService.launchSuggestion(suggestion);
        }
        catch (RemoteException | RuntimeException ex) {
            final Throwable t;
            Log.w("SuggestionController", "Error when calling launchSuggestion()", t);
        }
    }
    
    public void start() {
        this.mContext.bindServiceAsUser(this.mServiceIntent, this.mServiceConnection, 1, Process.myUserHandle());
    }
    
    public void stop() {
        if (this.mRemoteService != null) {
            this.mRemoteService = null;
            this.mContext.unbindService(this.mServiceConnection);
        }
    }
    
    public interface ServiceConnectionListener
    {
        void onServiceConnected();
        
        void onServiceDisconnected();
    }
}
