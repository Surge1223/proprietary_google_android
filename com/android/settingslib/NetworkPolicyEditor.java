package com.android.settingslib;

import android.os.AsyncTask;
import java.util.Iterator;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import android.util.RecurrenceRule;
import android.net.NetworkTemplate;
import com.android.internal.util.Preconditions;
import com.google.android.collect.Lists;
import android.net.NetworkPolicyManager;
import android.net.NetworkPolicy;
import java.util.ArrayList;

public class NetworkPolicyEditor
{
    private ArrayList<NetworkPolicy> mPolicies;
    private NetworkPolicyManager mPolicyManager;
    
    public NetworkPolicyEditor(final NetworkPolicyManager networkPolicyManager) {
        this.mPolicies = (ArrayList<NetworkPolicy>)Lists.newArrayList();
        this.mPolicyManager = (NetworkPolicyManager)Preconditions.checkNotNull((Object)networkPolicyManager);
    }
    
    @Deprecated
    private static NetworkPolicy buildDefaultPolicy(final NetworkTemplate networkTemplate) {
        RecurrenceRule recurrenceRule;
        boolean b;
        if (networkTemplate.getMatchRule() == 4) {
            recurrenceRule = RecurrenceRule.buildNever();
            b = false;
        }
        else {
            recurrenceRule = RecurrenceRule.buildRecurringMonthly(ZonedDateTime.now().getDayOfMonth(), ZoneId.systemDefault());
            b = true;
        }
        return new NetworkPolicy(networkTemplate, recurrenceRule, -1L, -1L, -1L, -1L, b, true);
    }
    
    private void setPolicyWarningBytesInner(final NetworkTemplate networkTemplate, final long warningBytes) {
        final NetworkPolicy orCreatePolicy = this.getOrCreatePolicy(networkTemplate);
        orCreatePolicy.warningBytes = warningBytes;
        orCreatePolicy.inferred = false;
        orCreatePolicy.clearSnooze();
        this.writeAsync();
    }
    
    public NetworkPolicy getOrCreatePolicy(final NetworkTemplate networkTemplate) {
        NetworkPolicy networkPolicy;
        if ((networkPolicy = this.getPolicy(networkTemplate)) == null) {
            networkPolicy = buildDefaultPolicy(networkTemplate);
            this.mPolicies.add(networkPolicy);
        }
        return networkPolicy;
    }
    
    public NetworkPolicy getPolicy(final NetworkTemplate networkTemplate) {
        for (final NetworkPolicy networkPolicy : this.mPolicies) {
            if (networkPolicy.template.equals((Object)networkTemplate)) {
                return networkPolicy;
            }
        }
        return null;
    }
    
    @Deprecated
    public int getPolicyCycleDay(final NetworkTemplate networkTemplate) {
        final NetworkPolicy policy = this.getPolicy(networkTemplate);
        if (policy != null && policy.cycleRule.isMonthly()) {
            return policy.cycleRule.start.getDayOfMonth();
        }
        return -1;
    }
    
    public long getPolicyLimitBytes(final NetworkTemplate networkTemplate) {
        final NetworkPolicy policy = this.getPolicy(networkTemplate);
        long limitBytes;
        if (policy != null) {
            limitBytes = policy.limitBytes;
        }
        else {
            limitBytes = -1L;
        }
        return limitBytes;
    }
    
    public long getPolicyWarningBytes(final NetworkTemplate networkTemplate) {
        final NetworkPolicy policy = this.getPolicy(networkTemplate);
        long warningBytes;
        if (policy != null) {
            warningBytes = policy.warningBytes;
        }
        else {
            warningBytes = -1L;
        }
        return warningBytes;
    }
    
    public void read() {
        final NetworkPolicy[] networkPolicies = this.mPolicyManager.getNetworkPolicies();
        boolean b = false;
        this.mPolicies.clear();
        for (final NetworkPolicy networkPolicy : networkPolicies) {
            if (networkPolicy.limitBytes < -1L) {
                networkPolicy.limitBytes = -1L;
                b = true;
            }
            if (networkPolicy.warningBytes < -1L) {
                networkPolicy.warningBytes = -1L;
                b = true;
            }
            this.mPolicies.add(networkPolicy);
        }
        if (b) {
            this.writeAsync();
        }
    }
    
    @Deprecated
    public void setPolicyCycleDay(final NetworkTemplate networkTemplate, final int n, final String s) {
        final NetworkPolicy orCreatePolicy = this.getOrCreatePolicy(networkTemplate);
        orCreatePolicy.cycleRule = NetworkPolicy.buildRule(n, ZoneId.of(s));
        orCreatePolicy.inferred = false;
        orCreatePolicy.clearSnooze();
        this.writeAsync();
    }
    
    public void setPolicyLimitBytes(final NetworkTemplate networkTemplate, final long limitBytes) {
        if (this.getPolicyWarningBytes(networkTemplate) > limitBytes && limitBytes != -1L) {
            this.setPolicyWarningBytesInner(networkTemplate, limitBytes);
        }
        final NetworkPolicy orCreatePolicy = this.getOrCreatePolicy(networkTemplate);
        orCreatePolicy.limitBytes = limitBytes;
        orCreatePolicy.inferred = false;
        orCreatePolicy.clearSnooze();
        this.writeAsync();
    }
    
    public void setPolicyWarningBytes(final NetworkTemplate networkTemplate, long min) {
        final long policyLimitBytes = this.getPolicyLimitBytes(networkTemplate);
        if (policyLimitBytes != -1L) {
            min = Math.min(min, policyLimitBytes);
        }
        this.setPolicyWarningBytesInner(networkTemplate, min);
    }
    
    public void write(final NetworkPolicy[] networkPolicies) {
        this.mPolicyManager.setNetworkPolicies(networkPolicies);
    }
    
    public void writeAsync() {
        new AsyncTask<Void, Void, Void>() {
            final /* synthetic */ NetworkPolicy[] val$policies = NetworkPolicyEditor.this.mPolicies.toArray(new NetworkPolicy[NetworkPolicyEditor.this.mPolicies.size()]);
            
            protected Void doInBackground(final Void... array) {
                NetworkPolicyEditor.this.write(this.val$policies);
                return null;
            }
        }.execute((Object[])new Void[0]);
    }
}
