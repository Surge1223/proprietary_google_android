package com.android.settingslib.users;

import android.graphics.drawable.Drawable;
import android.view.inputmethod.InputMethodManager;
import android.app.AppGlobals;
import android.content.pm.ParceledListSlice;
import android.text.TextUtils;
import java.util.Comparator;
import java.util.Collections;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Map;
import android.util.Log;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.content.res.Resources$NotFoundException;
import android.view.inputmethod.InputMethodInfo;
import android.content.pm.ApplicationInfo;
import java.util.Iterator;
import android.content.pm.ResolveInfo;
import java.util.Set;
import android.content.Intent;
import java.util.List;
import android.os.UserManager;
import android.os.UserHandle;
import java.util.HashMap;
import android.content.pm.PackageManager;
import android.content.pm.IPackageManager;
import android.content.Context;

public class AppRestrictionsHelper
{
    private final Context mContext;
    private final IPackageManager mIPm;
    private final Injector mInjector;
    private boolean mLeanback;
    private final PackageManager mPackageManager;
    private final boolean mRestrictedProfile;
    HashMap<String, Boolean> mSelectedPackages;
    private final UserHandle mUser;
    private final UserManager mUserManager;
    private List<SelectableAppInfo> mVisibleApps;
    
    public AppRestrictionsHelper(final Context context, final UserHandle userHandle) {
        this(new Injector(context, userHandle));
    }
    
    AppRestrictionsHelper(final Injector mInjector) {
        this.mSelectedPackages = new HashMap<String, Boolean>();
        this.mInjector = mInjector;
        this.mContext = this.mInjector.getContext();
        this.mPackageManager = this.mInjector.getPackageManager();
        this.mIPm = this.mInjector.getIPackageManager();
        this.mUser = this.mInjector.getUser();
        this.mUserManager = this.mInjector.getUserManager();
        this.mRestrictedProfile = this.mUserManager.getUserInfo(this.mUser.getIdentifier()).isRestricted();
    }
    
    private void addSystemApps(final List<SelectableAppInfo> list, final Intent intent, final Set<String> set) {
        final PackageManager mPackageManager = this.mPackageManager;
        for (final ResolveInfo resolveInfo : mPackageManager.queryIntentActivities(intent, 8704)) {
            if (resolveInfo.activityInfo != null && resolveInfo.activityInfo.applicationInfo != null) {
                final String packageName = resolveInfo.activityInfo.packageName;
                final int flags = resolveInfo.activityInfo.applicationInfo.flags;
                if ((flags & 0x1) == 0x0 && (flags & 0x80) == 0x0) {
                    continue;
                }
                if (set.contains(packageName)) {
                    continue;
                }
                final int applicationEnabledSetting = mPackageManager.getApplicationEnabledSetting(packageName);
                if (applicationEnabledSetting == 4 || applicationEnabledSetting == 2) {
                    final ApplicationInfo appInfoForUser = this.getAppInfoForUser(packageName, 0, this.mUser);
                    if (appInfoForUser == null) {
                        continue;
                    }
                    if ((appInfoForUser.flags & 0x800000) == 0x0) {
                        continue;
                    }
                }
                final SelectableAppInfo selectableAppInfo = new SelectableAppInfo();
                selectableAppInfo.packageName = resolveInfo.activityInfo.packageName;
                selectableAppInfo.appName = resolveInfo.activityInfo.applicationInfo.loadLabel(mPackageManager);
                selectableAppInfo.icon = resolveInfo.activityInfo.loadIcon(mPackageManager);
                selectableAppInfo.activityName = resolveInfo.activityInfo.loadLabel(mPackageManager);
                if (selectableAppInfo.activityName == null) {
                    selectableAppInfo.activityName = selectableAppInfo.appName;
                }
                list.add(selectableAppInfo);
            }
        }
    }
    
    private void addSystemImes(final Set<String> set) {
        for (final InputMethodInfo inputMethodInfo : this.mInjector.getInputMethodList()) {
            try {
                if (!inputMethodInfo.isDefault(this.mContext) || !this.isSystemPackage(inputMethodInfo.getPackageName())) {
                    continue;
                }
                set.add(inputMethodInfo.getPackageName());
            }
            catch (Resources$NotFoundException ex) {}
        }
    }
    
    private ApplicationInfo getAppInfoForUser(final String s, final int n, final UserHandle userHandle) {
        try {
            return this.mIPm.getApplicationInfo(s, n, userHandle.getIdentifier());
        }
        catch (RemoteException ex) {
            return null;
        }
    }
    
    private boolean isSystemPackage(final String s) {
        try {
            final PackageInfo packageInfo = this.mPackageManager.getPackageInfo(s, 0);
            if (packageInfo.applicationInfo == null) {
                return false;
            }
            final int flags = packageInfo.applicationInfo.flags;
            if ((flags & 0x1) != 0x0 || (flags & 0x80) != 0x0) {
                return true;
            }
        }
        catch (PackageManager$NameNotFoundException ex) {}
        return false;
    }
    
    public void applyUserAppState(final String s, final boolean b, final OnDisableUiForPackageListener onDisableUiForPackageListener) {
        final int identifier = this.mUser.getIdentifier();
        if (b) {
            try {
                final ApplicationInfo applicationInfo = this.mIPm.getApplicationInfo(s, 4194304, identifier);
                if (applicationInfo == null || !applicationInfo.enabled || (applicationInfo.flags & 0x800000) == 0x0) {
                    this.mIPm.installExistingPackageAsUser(s, this.mUser.getIdentifier(), 0, 0);
                }
                if (applicationInfo != null && (0x1 & applicationInfo.privateFlags) != 0x0 && (applicationInfo.flags & 0x800000) != 0x0) {
                    onDisableUiForPackageListener.onDisableUiForPackage(s);
                    this.mIPm.setApplicationHiddenSettingAsUser(s, false, identifier);
                }
            }
            catch (RemoteException ex) {}
        }
        else {
            try {
                if (this.mIPm.getApplicationInfo(s, 0, identifier) != null) {
                    if (this.mRestrictedProfile) {
                        this.mPackageManager.deletePackageAsUser(s, (IPackageDeleteObserver)null, 4, this.mUser.getIdentifier());
                    }
                    else {
                        onDisableUiForPackageListener.onDisableUiForPackage(s);
                        this.mIPm.setApplicationHiddenSettingAsUser(s, true, identifier);
                    }
                }
            }
            catch (RemoteException ex2) {}
        }
    }
    
    public void applyUserAppsStates(final OnDisableUiForPackageListener onDisableUiForPackageListener) {
        if (!this.mRestrictedProfile && this.mUser.getIdentifier() != UserHandle.myUserId()) {
            Log.e("AppRestrictionsHelper", "Cannot apply application restrictions on another user!");
            return;
        }
        for (final Map.Entry<String, Boolean> entry : this.mSelectedPackages.entrySet()) {
            this.applyUserAppState(entry.getKey(), entry.getValue(), onDisableUiForPackageListener);
        }
    }
    
    public void fetchAndMergeApps() {
        this.mVisibleApps = new ArrayList<SelectableAppInfo>();
        final PackageManager mPackageManager = this.mPackageManager;
        final IPackageManager miPm = this.mIPm;
        final HashSet<String> set = new HashSet<String>();
        this.addSystemImes(set);
        final Intent intent = new Intent("android.intent.action.MAIN");
        if (this.mLeanback) {
            intent.addCategory("android.intent.category.LEANBACK_LAUNCHER");
        }
        else {
            intent.addCategory("android.intent.category.LAUNCHER");
        }
        this.addSystemApps(this.mVisibleApps, intent, set);
        this.addSystemApps(this.mVisibleApps, new Intent("android.appwidget.action.APPWIDGET_UPDATE"), set);
        for (final ApplicationInfo applicationInfo : mPackageManager.getInstalledApplications(4194304)) {
            if ((0x800000 & applicationInfo.flags) == 0x0) {
                continue;
            }
            if ((applicationInfo.flags & 0x1) == 0x0 && (applicationInfo.flags & 0x80) == 0x0) {
                final SelectableAppInfo selectableAppInfo = new SelectableAppInfo();
                selectableAppInfo.packageName = applicationInfo.packageName;
                selectableAppInfo.appName = applicationInfo.loadLabel(mPackageManager);
                selectableAppInfo.activityName = selectableAppInfo.appName;
                selectableAppInfo.icon = applicationInfo.loadIcon(mPackageManager);
                this.mVisibleApps.add(selectableAppInfo);
            }
            else {
                try {
                    final PackageInfo packageInfo = mPackageManager.getPackageInfo(applicationInfo.packageName, 0);
                    if (!this.mRestrictedProfile || packageInfo.requiredAccountType == null || packageInfo.restrictedAccountType != null) {
                        continue;
                    }
                    this.mSelectedPackages.put(applicationInfo.packageName, false);
                }
                catch (PackageManager$NameNotFoundException ex) {}
            }
        }
        final List<ApplicationInfo> list = null;
        List<ApplicationInfo> list2 = null;
        try {
            final ParceledListSlice installedApplications = miPm.getInstalledApplications(8192, this.mUser.getIdentifier());
            if (installedApplications != null) {
                list2 = (List<ApplicationInfo>)installedApplications.getList();
            }
        }
        catch (RemoteException ex2) {
            list2 = list;
        }
        if (list2 != null) {
            for (final ApplicationInfo applicationInfo2 : list2) {
                if ((applicationInfo2.flags & 0x800000) == 0x0) {
                    continue;
                }
                if ((applicationInfo2.flags & 0x1) != 0x0 || (applicationInfo2.flags & 0x80) != 0x0) {
                    continue;
                }
                final SelectableAppInfo selectableAppInfo2 = new SelectableAppInfo();
                selectableAppInfo2.packageName = applicationInfo2.packageName;
                selectableAppInfo2.appName = applicationInfo2.loadLabel(mPackageManager);
                selectableAppInfo2.activityName = selectableAppInfo2.appName;
                selectableAppInfo2.icon = applicationInfo2.loadIcon(mPackageManager);
                this.mVisibleApps.add(selectableAppInfo2);
            }
        }
        Collections.sort(this.mVisibleApps, (Comparator<? super SelectableAppInfo>)new AppLabelComparator());
        final HashSet<String> set2 = new HashSet<String>();
        for (int i = this.mVisibleApps.size() - 1; i >= 0; --i) {
            final SelectableAppInfo selectableAppInfo3 = this.mVisibleApps.get(i);
            final StringBuilder sb = new StringBuilder();
            sb.append(selectableAppInfo3.packageName);
            sb.append("+");
            sb.append((Object)selectableAppInfo3.activityName);
            final String string = sb.toString();
            if (!TextUtils.isEmpty((CharSequence)selectableAppInfo3.packageName) && !TextUtils.isEmpty(selectableAppInfo3.activityName) && set2.contains(string)) {
                this.mVisibleApps.remove(i);
            }
            else {
                set2.add(string);
            }
        }
        final HashMap<String, SelectableAppInfo> hashMap = new HashMap<String, SelectableAppInfo>();
        for (final SelectableAppInfo selectableAppInfo4 : this.mVisibleApps) {
            if (hashMap.containsKey(selectableAppInfo4.packageName)) {
                selectableAppInfo4.masterEntry = hashMap.get(selectableAppInfo4.packageName);
            }
            else {
                hashMap.put(selectableAppInfo4.packageName, selectableAppInfo4);
            }
        }
    }
    
    public List<SelectableAppInfo> getVisibleApps() {
        return this.mVisibleApps;
    }
    
    public boolean isPackageSelected(final String s) {
        return this.mSelectedPackages.get(s);
    }
    
    public void setPackageSelected(final String s, final boolean b) {
        this.mSelectedPackages.put(s, b);
    }
    
    private static class AppLabelComparator implements Comparator<SelectableAppInfo>
    {
        @Override
        public int compare(final SelectableAppInfo selectableAppInfo, final SelectableAppInfo selectableAppInfo2) {
            return selectableAppInfo.activityName.toString().toLowerCase().compareTo(selectableAppInfo2.activityName.toString().toLowerCase());
        }
    }
    
    static class Injector
    {
        private Context mContext;
        private UserHandle mUser;
        
        Injector(final Context mContext, final UserHandle mUser) {
            this.mContext = mContext;
            this.mUser = mUser;
        }
        
        Context getContext() {
            return this.mContext;
        }
        
        IPackageManager getIPackageManager() {
            return AppGlobals.getPackageManager();
        }
        
        List<InputMethodInfo> getInputMethodList() {
            return (List<InputMethodInfo>)((InputMethodManager)this.getContext().getSystemService("input_method")).getInputMethodList();
        }
        
        PackageManager getPackageManager() {
            return this.mContext.getPackageManager();
        }
        
        UserHandle getUser() {
            return this.mUser;
        }
        
        UserManager getUserManager() {
            return (UserManager)this.mContext.getSystemService((Class)UserManager.class);
        }
    }
    
    public interface OnDisableUiForPackageListener
    {
        void onDisableUiForPackage(final String p0);
    }
    
    public static class SelectableAppInfo
    {
        public CharSequence activityName;
        public CharSequence appName;
        public Drawable icon;
        public SelectableAppInfo masterEntry;
        public String packageName;
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append(this.packageName);
            sb.append(": appName=");
            sb.append((Object)this.appName);
            sb.append("; activityName=");
            sb.append((Object)this.activityName);
            sb.append("; icon=");
            sb.append(this.icon);
            sb.append("; masterEntry=");
            sb.append(this.masterEntry);
            return sb.toString();
        }
    }
}
