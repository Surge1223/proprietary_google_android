package com.android.settingslib.notification;

import com.android.internal.logging.MetricsLogger;
import android.app.ActivityManager;
import android.widget.ScrollView;
import android.view.ViewGroup;
import com.android.internal.policy.PhoneWindow;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import com.android.settingslib.R;
import android.app.AlertDialog$Builder;
import android.provider.Settings;
import android.app.Dialog;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.RadioButton;
import android.view.View;
import java.util.Arrays;
import android.service.notification.ZenModeConfig;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.view.LayoutInflater;
import android.content.Context;

public class ZenDurationDialog
{
    protected static final int ALWAYS_ASK_CONDITION_INDEX = 2;
    protected static final int COUNTDOWN_CONDITION_INDEX = 1;
    private static final int DEFAULT_BUCKET_INDEX;
    protected static final int FOREVER_CONDITION_INDEX = 0;
    protected static final int MAX_BUCKET_MINUTES;
    private static final int[] MINUTE_BUCKETS;
    protected static final int MIN_BUCKET_MINUTES;
    private int MAX_MANUAL_DND_OPTIONS;
    protected int mBucketIndex;
    protected Context mContext;
    protected LayoutInflater mLayoutInflater;
    private RadioGroup mZenRadioGroup;
    protected LinearLayout mZenRadioGroupContent;
    
    static {
        MINUTE_BUCKETS = ZenModeConfig.MINUTE_BUCKETS;
        MIN_BUCKET_MINUTES = ZenDurationDialog.MINUTE_BUCKETS[0];
        MAX_BUCKET_MINUTES = ZenDurationDialog.MINUTE_BUCKETS[ZenDurationDialog.MINUTE_BUCKETS.length - 1];
        DEFAULT_BUCKET_INDEX = Arrays.binarySearch(ZenDurationDialog.MINUTE_BUCKETS, 60);
    }
    
    public ZenDurationDialog(final Context mContext) {
        this.mBucketIndex = -1;
        this.MAX_MANUAL_DND_OPTIONS = 3;
        this.mContext = mContext;
    }
    
    private void bindTag(final int countdownZenDuration, final View view, final int n) {
        ConditionTag tag;
        if (view.getTag() != null) {
            tag = (ConditionTag)view.getTag();
        }
        else {
            tag = new ConditionTag();
        }
        view.setTag((Object)tag);
        if (tag.rb == null) {
            tag.rb = (RadioButton)this.mZenRadioGroup.getChildAt(n);
        }
        if (countdownZenDuration <= 0) {
            tag.countdownZenDuration = ZenDurationDialog.MINUTE_BUCKETS[ZenDurationDialog.DEFAULT_BUCKET_INDEX];
        }
        else {
            tag.countdownZenDuration = countdownZenDuration;
        }
        tag.rb.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                if (b) {
                    tag.rb.setChecked(true);
                }
            }
        });
        this.updateUi(tag, view, n);
    }
    
    private void setupUi(final ConditionTag conditionTag, final View view) {
        if (conditionTag.lines == null) {
            (conditionTag.lines = view.findViewById(16908290)).setAccessibilityLiveRegion(1);
        }
        if (conditionTag.line1 == null) {
            conditionTag.line1 = (TextView)view.findViewById(16908308);
        }
        view.findViewById(16908309).setVisibility(8);
        conditionTag.lines.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                conditionTag.rb.setChecked(true);
            }
        });
    }
    
    private void updateButtons(final ConditionTag conditionTag, final View view, final int n) {
        final ImageView imageView = (ImageView)view.findViewById(16908313);
        imageView.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ZenDurationDialog.this.onClickTimeButton(view, conditionTag, false, n);
            }
        });
        final ImageView imageView2 = (ImageView)view.findViewById(16908314);
        imageView2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                ZenDurationDialog.this.onClickTimeButton(view, conditionTag, true, n);
            }
        });
        final long n2 = conditionTag.countdownZenDuration;
        final boolean b = true;
        if (n == 1) {
            imageView.setVisibility(0);
            imageView2.setVisibility(0);
            imageView.setEnabled(n2 > ZenDurationDialog.MIN_BUCKET_MINUTES);
            imageView2.setEnabled(conditionTag.countdownZenDuration != ZenDurationDialog.MAX_BUCKET_MINUTES && b);
            final boolean enabled = imageView.isEnabled();
            final float n3 = 0.5f;
            float alpha;
            if (enabled) {
                alpha = 1.0f;
            }
            else {
                alpha = 0.5f;
            }
            imageView.setAlpha(alpha);
            float alpha2 = n3;
            if (imageView2.isEnabled()) {
                alpha2 = 1.0f;
            }
            imageView2.setAlpha(alpha2);
        }
        else {
            imageView.setVisibility(8);
            imageView2.setVisibility(8);
        }
    }
    
    public Dialog createDialog() {
        final int int1 = Settings.Global.getInt(this.mContext.getContentResolver(), "zen_duration", 0);
        final AlertDialog$Builder setPositiveButton = new AlertDialog$Builder(this.mContext).setTitle(R.string.zen_mode_duration_settings_title).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setPositiveButton(R.string.okay, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, final int n) {
                ZenDurationDialog.this.updateZenDuration(int1);
            }
        });
        final View contentView = this.getContentView();
        this.setupRadioButtons(int1);
        setPositiveButton.setView(contentView);
        return (Dialog)setPositiveButton.create();
    }
    
    protected ConditionTag getConditionTagAt(final int n) {
        return (ConditionTag)this.mZenRadioGroupContent.getChildAt(n).getTag();
    }
    
    protected View getContentView() {
        if (this.mLayoutInflater == null) {
            this.mLayoutInflater = new PhoneWindow(this.mContext).getLayoutInflater();
        }
        final View inflate = this.mLayoutInflater.inflate(R.layout.zen_mode_duration_dialog, (ViewGroup)null);
        final ScrollView scrollView = (ScrollView)inflate.findViewById(R.id.zen_duration_container);
        this.mZenRadioGroup = (RadioGroup)scrollView.findViewById(R.id.zen_radio_buttons);
        this.mZenRadioGroupContent = (LinearLayout)scrollView.findViewById(R.id.zen_radio_buttons_content);
        for (int i = 0; i < this.MAX_MANUAL_DND_OPTIONS; ++i) {
            final View inflate2 = this.mLayoutInflater.inflate(R.layout.zen_mode_radio_button, (ViewGroup)this.mZenRadioGroup, false);
            this.mZenRadioGroup.addView(inflate2);
            inflate2.setId(i);
            final View inflate3 = this.mLayoutInflater.inflate(R.layout.zen_mode_condition, (ViewGroup)this.mZenRadioGroupContent, false);
            inflate3.setId(this.MAX_MANUAL_DND_OPTIONS + i);
            this.mZenRadioGroupContent.addView(inflate3);
        }
        return inflate;
    }
    
    protected void onClickTimeButton(final View view, final ConditionTag conditionTag, final boolean b, final int n) {
        final int n2 = -1;
        final int length = ZenDurationDialog.MINUTE_BUCKETS.length;
        final int mBucketIndex = this.mBucketIndex;
        final int n3 = 0;
        int n4 = -1;
        int countdownZenDuration;
        if (mBucketIndex == -1) {
            final long n5 = conditionTag.countdownZenDuration;
            int n6 = n3;
            int n7;
            while (true) {
                n7 = n2;
                if (n6 >= length) {
                    break;
                }
                int mBucketIndex2;
                if (b) {
                    mBucketIndex2 = n6;
                }
                else {
                    mBucketIndex2 = length - 1 - n6;
                }
                final int n8 = ZenDurationDialog.MINUTE_BUCKETS[mBucketIndex2];
                if ((b && n8 > n5) || (!b && n8 < n5)) {
                    this.mBucketIndex = mBucketIndex2;
                    n7 = n8;
                    break;
                }
                ++n6;
            }
            if ((countdownZenDuration = n7) == -1) {
                this.mBucketIndex = ZenDurationDialog.DEFAULT_BUCKET_INDEX;
                countdownZenDuration = ZenDurationDialog.MINUTE_BUCKETS[this.mBucketIndex];
            }
        }
        else {
            final int mBucketIndex3 = this.mBucketIndex;
            if (b) {
                n4 = 1;
            }
            this.mBucketIndex = Math.max(0, Math.min(length - 1, mBucketIndex3 + n4));
            countdownZenDuration = ZenDurationDialog.MINUTE_BUCKETS[this.mBucketIndex];
        }
        this.bindTag(conditionTag.countdownZenDuration = countdownZenDuration, view, n);
        conditionTag.rb.setChecked(true);
    }
    
    protected void setupRadioButtons(final int n) {
        int n2 = 2;
        if (n == 0) {
            n2 = 0;
        }
        else if (n > 0) {
            n2 = 1;
        }
        this.bindTag(n, this.mZenRadioGroupContent.getChildAt(0), 0);
        this.bindTag(n, this.mZenRadioGroupContent.getChildAt(1), 1);
        this.bindTag(n, this.mZenRadioGroupContent.getChildAt(2), 2);
        this.getConditionTagAt(n2).rb.setChecked(true);
    }
    
    protected void updateUi(final ConditionTag conditionTag, final View view, final int n) {
        if (conditionTag.lines == null) {
            this.setupUi(conditionTag, view);
        }
        this.updateButtons(conditionTag, view, n);
        String text = "";
        switch (n) {
            case 2: {
                text = this.mContext.getString(R.string.zen_mode_duration_always_prompt_title);
                break;
            }
            case 1: {
                text = ZenModeConfig.toTimeCondition(this.mContext, conditionTag.countdownZenDuration, ActivityManager.getCurrentUser(), false).line1;
                break;
            }
            case 0: {
                text = this.mContext.getString(17041151);
                break;
            }
        }
        conditionTag.line1.setText((CharSequence)text);
    }
    
    protected void updateZenDuration(final int n) {
        final int checkedRadioButtonId = this.mZenRadioGroup.getCheckedRadioButtonId();
        int n2 = Settings.Global.getInt(this.mContext.getContentResolver(), "zen_duration", 0);
        switch (checkedRadioButtonId) {
            case 2: {
                n2 = -1;
                MetricsLogger.action(this.mContext, 1344);
                break;
            }
            case 1: {
                n2 = this.getConditionTagAt(checkedRadioButtonId).countdownZenDuration;
                MetricsLogger.action(this.mContext, 1342, n2);
                break;
            }
            case 0: {
                n2 = 0;
                MetricsLogger.action(this.mContext, 1343);
                break;
            }
        }
        if (n != n2) {
            Settings.Global.putInt(this.mContext.getContentResolver(), "zen_duration", n2);
        }
    }
    
    protected static class ConditionTag
    {
        public int countdownZenDuration;
        public TextView line1;
        public View lines;
        public RadioButton rb;
    }
}
