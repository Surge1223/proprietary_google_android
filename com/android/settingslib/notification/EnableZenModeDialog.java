package com.android.settingslib.notification;

import java.util.GregorianCalendar;
import java.util.Locale;
import android.text.format.DateFormat;
import android.app.AlarmManager$AlarmClockInfo;
import android.widget.ScrollView;
import android.view.ViewGroup;
import com.android.internal.policy.PhoneWindow;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import com.android.settingslib.R;
import android.util.Slog;
import android.widget.CompoundButton;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.widget.RadioButton;
import java.util.Objects;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import android.text.TextUtils;
import java.util.Calendar;
import android.app.ActivityManager;
import com.android.internal.logging.MetricsLogger;
import android.view.View;
import android.service.notification.Condition;
import java.util.Arrays;
import android.service.notification.ZenModeConfig;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.app.NotificationManager;
import android.view.LayoutInflater;
import android.net.Uri;
import android.content.Context;
import android.app.AlarmManager;
import com.android.internal.annotations.VisibleForTesting;

public class EnableZenModeDialog
{
    @VisibleForTesting
    protected static final int COUNTDOWN_ALARM_CONDITION_INDEX = 2;
    @VisibleForTesting
    protected static final int COUNTDOWN_CONDITION_INDEX = 1;
    private static final boolean DEBUG;
    private static final int DEFAULT_BUCKET_INDEX;
    @VisibleForTesting
    protected static final int FOREVER_CONDITION_INDEX = 0;
    private static final int MAX_BUCKET_MINUTES;
    private static final int[] MINUTE_BUCKETS;
    private static final int MIN_BUCKET_MINUTES;
    private int MAX_MANUAL_DND_OPTIONS;
    private AlarmManager mAlarmManager;
    private boolean mAttached;
    private int mBucketIndex;
    @VisibleForTesting
    protected Context mContext;
    @VisibleForTesting
    protected Uri mForeverId;
    @VisibleForTesting
    protected LayoutInflater mLayoutInflater;
    @VisibleForTesting
    protected NotificationManager mNotificationManager;
    private int mUserId;
    @VisibleForTesting
    protected TextView mZenAlarmWarning;
    private RadioGroup mZenRadioGroup;
    @VisibleForTesting
    protected LinearLayout mZenRadioGroupContent;
    
    static {
        DEBUG = Log.isLoggable("EnableZenModeDialog", 3);
        MINUTE_BUCKETS = ZenModeConfig.MINUTE_BUCKETS;
        MIN_BUCKET_MINUTES = EnableZenModeDialog.MINUTE_BUCKETS[0];
        MAX_BUCKET_MINUTES = EnableZenModeDialog.MINUTE_BUCKETS[EnableZenModeDialog.MINUTE_BUCKETS.length - 1];
        DEFAULT_BUCKET_INDEX = Arrays.binarySearch(EnableZenModeDialog.MINUTE_BUCKETS, 60);
    }
    
    public EnableZenModeDialog(final Context mContext) {
        this.mBucketIndex = -1;
        this.MAX_MANUAL_DND_OPTIONS = 3;
        this.mContext = mContext;
    }
    
    private String foreverSummary(final Context context) {
        return context.getString(17041151);
    }
    
    public static Uri getConditionId(final Condition condition) {
        Uri id;
        if (condition != null) {
            id = condition.id;
        }
        else {
            id = null;
        }
        return id;
    }
    
    private Uri getRealConditionId(final Condition condition) {
        Uri conditionId;
        if (this.isForever(condition)) {
            conditionId = null;
        }
        else {
            conditionId = getConditionId(condition);
        }
        return conditionId;
    }
    
    private void hideAllConditions() {
        for (int childCount = this.mZenRadioGroupContent.getChildCount(), i = 0; i < childCount; ++i) {
            this.mZenRadioGroupContent.getChildAt(i).setVisibility(8);
        }
        this.mZenAlarmWarning.setVisibility(8);
    }
    
    private boolean isForever(final Condition condition) {
        return condition != null && this.mForeverId.equals((Object)condition.id);
    }
    
    private void onClickTimeButton(final View view, final ConditionTag conditionTag, final boolean b, final int n) {
        MetricsLogger.action(this.mContext, 163, b);
        final Condition condition = null;
        final int length = EnableZenModeDialog.MINUTE_BUCKETS.length;
        final int mBucketIndex = this.mBucketIndex;
        final int n2 = 0;
        int n3 = -1;
        Condition condition2;
        if (mBucketIndex == -1) {
            final long tryParseCountdownConditionId = ZenModeConfig.tryParseCountdownConditionId(getConditionId(conditionTag.condition));
            final long currentTimeMillis = System.currentTimeMillis();
            int n4 = n2;
            Condition timeCondition;
            while (true) {
                timeCondition = condition;
                if (n4 >= length) {
                    break;
                }
                int mBucketIndex2;
                if (b) {
                    mBucketIndex2 = n4;
                }
                else {
                    mBucketIndex2 = length - 1 - n4;
                }
                final int n5 = EnableZenModeDialog.MINUTE_BUCKETS[mBucketIndex2];
                final long n6 = currentTimeMillis + 60000 * n5;
                if ((b && n6 > tryParseCountdownConditionId) || (!b && n6 < tryParseCountdownConditionId)) {
                    this.mBucketIndex = mBucketIndex2;
                    timeCondition = ZenModeConfig.toTimeCondition(this.mContext, n6, n5, ActivityManager.getCurrentUser(), false);
                    break;
                }
                ++n4;
            }
            if ((condition2 = timeCondition) == null) {
                this.mBucketIndex = EnableZenModeDialog.DEFAULT_BUCKET_INDEX;
                condition2 = ZenModeConfig.toTimeCondition(this.mContext, EnableZenModeDialog.MINUTE_BUCKETS[this.mBucketIndex], ActivityManager.getCurrentUser());
            }
        }
        else {
            final int mBucketIndex3 = this.mBucketIndex;
            if (b) {
                n3 = 1;
            }
            this.mBucketIndex = Math.max(0, Math.min(length - 1, mBucketIndex3 + n3));
            condition2 = ZenModeConfig.toTimeCondition(this.mContext, EnableZenModeDialog.MINUTE_BUCKETS[this.mBucketIndex], ActivityManager.getCurrentUser());
        }
        this.bind(condition2, view, n);
        this.updateAlarmWarningText(conditionTag.condition);
        conditionTag.rb.setChecked(true);
    }
    
    private static void setToMidnight(final Calendar calendar) {
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
    }
    
    private void updateAlarmWarningText(final Condition condition) {
        final String computeAlarmWarningText = this.computeAlarmWarningText(condition);
        this.mZenAlarmWarning.setText((CharSequence)computeAlarmWarningText);
        final TextView mZenAlarmWarning = this.mZenAlarmWarning;
        int visibility;
        if (computeAlarmWarningText == null) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        mZenAlarmWarning.setVisibility(visibility);
    }
    
    private void updateUi(final ConditionTag conditionTag, final View view, final Condition condition, final boolean enabled, final int n, final Uri uri) {
        final View lines = conditionTag.lines;
        final boolean b = true;
        if (lines == null) {
            (conditionTag.lines = view.findViewById(16908290)).setAccessibilityLiveRegion(1);
        }
        if (conditionTag.line1 == null) {
            conditionTag.line1 = (TextView)view.findViewById(16908308);
        }
        if (conditionTag.line2 == null) {
            conditionTag.line2 = (TextView)view.findViewById(16908309);
        }
        String text;
        if (!TextUtils.isEmpty((CharSequence)condition.line1)) {
            text = condition.line1;
        }
        else {
            text = condition.summary;
        }
        final String line2 = condition.line2;
        conditionTag.line1.setText((CharSequence)text);
        if (TextUtils.isEmpty((CharSequence)line2)) {
            conditionTag.line2.setVisibility(8);
        }
        else {
            conditionTag.line2.setVisibility(0);
            conditionTag.line2.setText((CharSequence)line2);
        }
        conditionTag.lines.setEnabled(enabled);
        final View lines2 = conditionTag.lines;
        float alpha;
        if (enabled) {
            alpha = 1.0f;
        }
        else {
            alpha = 0.4f;
        }
        lines2.setAlpha(alpha);
        conditionTag.lines.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                conditionTag.rb.setChecked(true);
            }
        });
        final ImageView imageView = (ImageView)view.findViewById(16908313);
        imageView.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                EnableZenModeDialog.this.onClickTimeButton(view, conditionTag, false, n);
            }
        });
        final ImageView imageView2 = (ImageView)view.findViewById(16908314);
        imageView2.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                EnableZenModeDialog.this.onClickTimeButton(view, conditionTag, true, n);
            }
        });
        final long tryParseCountdownConditionId = ZenModeConfig.tryParseCountdownConditionId(uri);
        if (n == 1 && tryParseCountdownConditionId > 0L) {
            imageView.setVisibility(0);
            imageView2.setVisibility(0);
            if (this.mBucketIndex > -1) {
                imageView.setEnabled(this.mBucketIndex > 0);
                imageView2.setEnabled(this.mBucketIndex < EnableZenModeDialog.MINUTE_BUCKETS.length - 1 && b);
            }
            else {
                imageView.setEnabled(tryParseCountdownConditionId - System.currentTimeMillis() > EnableZenModeDialog.MIN_BUCKET_MINUTES * 60000);
                imageView2.setEnabled(Objects.equals(condition.summary, ZenModeConfig.toTimeCondition(this.mContext, EnableZenModeDialog.MAX_BUCKET_MINUTES, ActivityManager.getCurrentUser()).summary) ^ true);
            }
            final boolean enabled2 = imageView.isEnabled();
            final float n2 = 0.5f;
            float alpha2;
            if (enabled2) {
                alpha2 = 1.0f;
            }
            else {
                alpha2 = 0.5f;
            }
            imageView.setAlpha(alpha2);
            float alpha3 = n2;
            if (imageView2.isEnabled()) {
                alpha3 = 1.0f;
            }
            imageView2.setAlpha(alpha3);
        }
        else {
            imageView.setVisibility(8);
            imageView2.setVisibility(8);
        }
    }
    
    @VisibleForTesting
    protected void bind(final Condition condition, final View view, final int n) {
        if (condition != null) {
            final int state = condition.state;
            boolean b = true;
            final boolean enabled = state == 1;
            ConditionTag tag;
            if (view.getTag() != null) {
                tag = (ConditionTag)view.getTag();
            }
            else {
                tag = new ConditionTag();
            }
            view.setTag((Object)tag);
            if (tag.rb != null) {
                b = false;
            }
            if (tag.rb == null) {
                tag.rb = (RadioButton)this.mZenRadioGroup.getChildAt(n);
            }
            tag.condition = condition;
            final Uri conditionId = getConditionId(tag.condition);
            if (EnableZenModeDialog.DEBUG) {
                final StringBuilder sb = new StringBuilder();
                sb.append("bind i=");
                sb.append(this.mZenRadioGroupContent.indexOfChild(view));
                sb.append(" first=");
                sb.append(b);
                sb.append(" condition=");
                sb.append(conditionId);
                Log.d("EnableZenModeDialog", sb.toString());
            }
            tag.rb.setEnabled(enabled);
            tag.rb.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)new CompoundButton$OnCheckedChangeListener() {
                public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                    if (b) {
                        tag.rb.setChecked(true);
                        if (EnableZenModeDialog.DEBUG) {
                            final StringBuilder sb = new StringBuilder();
                            sb.append("onCheckedChanged ");
                            sb.append(conditionId);
                            Log.d("EnableZenModeDialog", sb.toString());
                        }
                        MetricsLogger.action(EnableZenModeDialog.this.mContext, 164);
                        EnableZenModeDialog.this.updateAlarmWarningText(tag.condition);
                    }
                }
            });
            this.updateUi(tag, view, condition, enabled, n, conditionId);
            view.setVisibility(0);
            return;
        }
        throw new IllegalArgumentException("condition must not be null");
    }
    
    @VisibleForTesting
    protected void bindConditions(final Condition condition) {
        this.bind(this.forever(), this.mZenRadioGroupContent.getChildAt(0), 0);
        if (condition == null) {
            this.bindGenericCountdown();
            this.bindNextAlarm(this.getTimeUntilNextAlarmCondition());
        }
        else if (this.isForever(condition)) {
            this.getConditionTagAt(0).rb.setChecked(true);
            this.bindGenericCountdown();
            this.bindNextAlarm(this.getTimeUntilNextAlarmCondition());
        }
        else if (this.isAlarm(condition)) {
            this.bindGenericCountdown();
            this.bindNextAlarm(condition);
            this.getConditionTagAt(2).rb.setChecked(true);
        }
        else if (this.isCountdown(condition)) {
            this.bindNextAlarm(this.getTimeUntilNextAlarmCondition());
            this.bind(condition, this.mZenRadioGroupContent.getChildAt(1), 1);
            this.getConditionTagAt(1).rb.setChecked(true);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid manual condition: ");
            sb.append(condition);
            Slog.d("EnableZenModeDialog", sb.toString());
        }
    }
    
    @VisibleForTesting
    protected void bindGenericCountdown() {
        this.mBucketIndex = EnableZenModeDialog.DEFAULT_BUCKET_INDEX;
        final Condition timeCondition = ZenModeConfig.toTimeCondition(this.mContext, EnableZenModeDialog.MINUTE_BUCKETS[this.mBucketIndex], ActivityManager.getCurrentUser());
        if (!this.mAttached || this.getConditionTagAt(1).condition == null) {
            this.bind(timeCondition, this.mZenRadioGroupContent.getChildAt(1), 1);
        }
    }
    
    @VisibleForTesting
    protected void bindNextAlarm(final Condition condition) {
        final View child = this.mZenRadioGroupContent.getChildAt(2);
        final ConditionTag conditionTag = (ConditionTag)child.getTag();
        if (condition != null && (!this.mAttached || conditionTag == null || conditionTag.condition == null)) {
            this.bind(condition, child, 2);
        }
        final ConditionTag conditionTag2 = (ConditionTag)child.getTag();
        final boolean b = false;
        final boolean b2 = conditionTag2 != null && conditionTag2.condition != null;
        final View child2 = this.mZenRadioGroup.getChildAt(2);
        int visibility;
        if (b2) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        child2.setVisibility(visibility);
        int visibility2;
        if (b2) {
            visibility2 = (b ? 1 : 0);
        }
        else {
            visibility2 = 8;
        }
        child.setVisibility(visibility2);
    }
    
    @VisibleForTesting
    protected String computeAlarmWarningText(final Condition condition) {
        if ((this.mNotificationManager.getNotificationPolicy().priorityCategories & 0x20) != 0x0) {
            return null;
        }
        final long currentTimeMillis = System.currentTimeMillis();
        final long nextAlarm = this.getNextAlarm();
        if (nextAlarm < currentTimeMillis) {
            return null;
        }
        final boolean b = false;
        int n;
        if (condition != null && !this.isForever(condition)) {
            final long tryParseCountdownConditionId = ZenModeConfig.tryParseCountdownConditionId(condition.id);
            n = (b ? 1 : 0);
            if (tryParseCountdownConditionId > currentTimeMillis) {
                n = (b ? 1 : 0);
                if (nextAlarm < tryParseCountdownConditionId) {
                    n = R.string.zen_alarm_warning;
                }
            }
        }
        else {
            n = R.string.zen_alarm_warning_indef;
        }
        if (n == 0) {
            return null;
        }
        return this.mContext.getResources().getString(n, new Object[] { this.getTime(nextAlarm, currentTimeMillis) });
    }
    
    public Dialog createDialog() {
        this.mNotificationManager = (NotificationManager)this.mContext.getSystemService("notification");
        this.mForeverId = Condition.newId(this.mContext).appendPath("forever").build();
        this.mAlarmManager = (AlarmManager)this.mContext.getSystemService("alarm");
        this.mUserId = this.mContext.getUserId();
        this.mAttached = false;
        final AlertDialog$Builder setPositiveButton = new AlertDialog$Builder(this.mContext).setTitle(R.string.zen_mode_settings_turn_on_dialog_title).setNegativeButton(R.string.cancel, (DialogInterface$OnClickListener)null).setPositiveButton(R.string.zen_mode_enable_dialog_turn_on, (DialogInterface$OnClickListener)new DialogInterface$OnClickListener() {
            public void onClick(final DialogInterface dialogInterface, int checkedRadioButtonId) {
                checkedRadioButtonId = EnableZenModeDialog.this.mZenRadioGroup.getCheckedRadioButtonId();
                final ConditionTag conditionTag = EnableZenModeDialog.this.getConditionTagAt(checkedRadioButtonId);
                if (EnableZenModeDialog.this.isForever(conditionTag.condition)) {
                    MetricsLogger.action(EnableZenModeDialog.this.mContext, 1259);
                }
                else if (EnableZenModeDialog.this.isAlarm(conditionTag.condition)) {
                    MetricsLogger.action(EnableZenModeDialog.this.mContext, 1261);
                }
                else if (EnableZenModeDialog.this.isCountdown(conditionTag.condition)) {
                    MetricsLogger.action(EnableZenModeDialog.this.mContext, 1260);
                }
                else {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Invalid manual condition: ");
                    sb.append(conditionTag.condition);
                    Slog.d("EnableZenModeDialog", sb.toString());
                }
                EnableZenModeDialog.this.mNotificationManager.setZenMode(1, EnableZenModeDialog.this.getRealConditionId(conditionTag.condition), "EnableZenModeDialog");
            }
        });
        final View contentView = this.getContentView();
        this.bindConditions(this.forever());
        setPositiveButton.setView(contentView);
        return (Dialog)setPositiveButton.create();
    }
    
    public Condition forever() {
        return new Condition(Condition.newId(this.mContext).appendPath("forever").build(), this.foreverSummary(this.mContext), "", "", 0, 1, 0);
    }
    
    @VisibleForTesting
    protected ConditionTag getConditionTagAt(final int n) {
        return (ConditionTag)this.mZenRadioGroupContent.getChildAt(n).getTag();
    }
    
    protected View getContentView() {
        if (this.mLayoutInflater == null) {
            this.mLayoutInflater = new PhoneWindow(this.mContext).getLayoutInflater();
        }
        final View inflate = this.mLayoutInflater.inflate(R.layout.zen_mode_turn_on_dialog_container, (ViewGroup)null);
        final ScrollView scrollView = (ScrollView)inflate.findViewById(R.id.container);
        this.mZenRadioGroup = (RadioGroup)scrollView.findViewById(R.id.zen_radio_buttons);
        this.mZenRadioGroupContent = (LinearLayout)scrollView.findViewById(R.id.zen_radio_buttons_content);
        this.mZenAlarmWarning = (TextView)scrollView.findViewById(R.id.zen_alarm_warning);
        for (int i = 0; i < this.MAX_MANUAL_DND_OPTIONS; ++i) {
            final View inflate2 = this.mLayoutInflater.inflate(R.layout.zen_mode_radio_button, (ViewGroup)this.mZenRadioGroup, false);
            this.mZenRadioGroup.addView(inflate2);
            inflate2.setId(i);
            final View inflate3 = this.mLayoutInflater.inflate(R.layout.zen_mode_condition, (ViewGroup)this.mZenRadioGroupContent, false);
            inflate3.setId(this.MAX_MANUAL_DND_OPTIONS + i);
            this.mZenRadioGroupContent.addView(inflate3);
        }
        this.hideAllConditions();
        return inflate;
    }
    
    public long getNextAlarm() {
        final AlarmManager$AlarmClockInfo nextAlarmClock = this.mAlarmManager.getNextAlarmClock(this.mUserId);
        long triggerTime;
        if (nextAlarmClock != null) {
            triggerTime = nextAlarmClock.getTriggerTime();
        }
        else {
            triggerTime = 0L;
        }
        return triggerTime;
    }
    
    @VisibleForTesting
    protected String getTime(final long n, final long n2) {
        final boolean b = n - n2 < 86400000L;
        final boolean is24HourFormat = DateFormat.is24HourFormat(this.mContext, ActivityManager.getCurrentUser());
        String s;
        if (b) {
            if (is24HourFormat) {
                s = "Hm";
            }
            else {
                s = "hma";
            }
        }
        else if (is24HourFormat) {
            s = "EEEHm";
        }
        else {
            s = "EEEhma";
        }
        final CharSequence format = DateFormat.format((CharSequence)DateFormat.getBestDateTimePattern(Locale.getDefault(), s), n);
        int n3;
        if (b) {
            n3 = R.string.alarm_template;
        }
        else {
            n3 = R.string.alarm_template_far;
        }
        return this.mContext.getResources().getString(n3, new Object[] { format });
    }
    
    @VisibleForTesting
    protected Condition getTimeUntilNextAlarmCondition() {
        final GregorianCalendar toMidnight = new GregorianCalendar();
        setToMidnight(toMidnight);
        toMidnight.add(5, 6);
        final long nextAlarm = this.getNextAlarm();
        if (nextAlarm > 0L) {
            final GregorianCalendar toMidnight2 = new GregorianCalendar();
            toMidnight2.setTimeInMillis(nextAlarm);
            setToMidnight(toMidnight2);
            if (toMidnight.compareTo((Calendar)toMidnight2) >= 0) {
                return ZenModeConfig.toNextAlarmCondition(this.mContext, nextAlarm, ActivityManager.getCurrentUser());
            }
        }
        return null;
    }
    
    @VisibleForTesting
    protected boolean isAlarm(final Condition condition) {
        return condition != null && ZenModeConfig.isValidCountdownToAlarmConditionId(condition.id);
    }
    
    @VisibleForTesting
    protected boolean isCountdown(final Condition condition) {
        return condition != null && ZenModeConfig.isValidCountdownConditionId(condition.id);
    }
    
    @VisibleForTesting
    protected static class ConditionTag
    {
        public Condition condition;
        public TextView line1;
        public TextView line2;
        public View lines;
        public RadioButton rb;
    }
}
