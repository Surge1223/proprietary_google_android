package com.android.settingslib.development;

import android.os.RemoteException;
import android.os.Parcel;
import android.util.Log;
import android.os.ServiceManager;
import android.os.IBinder;
import android.os.AsyncTask;

public class SystemPropPoker
{
    private static final SystemPropPoker sInstance;
    private boolean mBlockPokes;
    
    static {
        sInstance = new SystemPropPoker();
    }
    
    private SystemPropPoker() {
        this.mBlockPokes = false;
    }
    
    public static SystemPropPoker getInstance() {
        return SystemPropPoker.sInstance;
    }
    
    public void blockPokes() {
        this.mBlockPokes = true;
    }
    
    PokerTask createPokerTask() {
        return new PokerTask();
    }
    
    public void poke() {
        if (!this.mBlockPokes) {
            this.createPokerTask().execute((Object[])new Void[0]);
        }
    }
    
    public void unblockPokes() {
        this.mBlockPokes = false;
    }
    
    public static class PokerTask extends AsyncTask<Void, Void, Void>
    {
        IBinder checkService(final String s) {
            return ServiceManager.checkService(s);
        }
        
        protected Void doInBackground(Void... listServices) {
            listServices = (Void[])this.listServices();
            if (listServices == null) {
                Log.e("SystemPropPoker", "There are no services, how odd");
                return null;
            }
            for (final Void void1 : listServices) {
                final IBinder checkService = this.checkService((String)void1);
                if (checkService != null) {
                    final Parcel obtain = Parcel.obtain();
                    try {
                        checkService.transact(1599295570, obtain, (Parcel)null, 0);
                    }
                    catch (Exception ex) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Someone wrote a bad service '");
                        sb.append((String)void1);
                        sb.append("' that doesn't like to be poked");
                        Log.i("SystemPropPoker", sb.toString(), (Throwable)ex);
                    }
                    catch (RemoteException void1) {}
                    obtain.recycle();
                }
            }
            return null;
        }
        
        String[] listServices() {
            return ServiceManager.listServices();
        }
    }
}
