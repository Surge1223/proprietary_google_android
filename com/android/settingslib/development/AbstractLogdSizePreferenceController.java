package com.android.settingslib.development;

import com.android.settingslib.R;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.preference.PreferenceScreen;
import android.os.SystemProperties;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;

public abstract class AbstractLogdSizePreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener
{
    static final String DEFAULT_SNET_TAG = "I";
    static final String LOW_RAM_CONFIG_PROPERTY_KEY = "ro.config.low_ram";
    static final String SELECT_LOGD_DEFAULT_SIZE_VALUE = "262144";
    static final String SELECT_LOGD_MINIMUM_SIZE_VALUE = "65536";
    static final String SELECT_LOGD_SIZE_PROPERTY = "persist.logd.size";
    static final String SELECT_LOGD_SNET_TAG_PROPERTY = "persist.log.tag.snet_event_log";
    private ListPreference mLogdSize;
    
    public AbstractLogdSizePreferenceController(final Context context) {
        super(context);
    }
    
    private String defaultLogdSizeValue() {
        final String value = SystemProperties.get("ro.logd.size");
        if (value != null) {
            final String s = value;
            if (value.length() != 0) {
                return s;
            }
        }
        String s;
        if (SystemProperties.get("ro.config.low_ram").equals("true")) {
            s = "65536";
        }
        else {
            s = "262144";
        }
        return s;
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mLogdSize = (ListPreference)preferenceScreen.findPreference("select_logd_size");
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "select_logd_size";
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mLogdSize) {
            this.writeLogdSizeOption(o);
            return true;
        }
        return false;
    }
    
    public void updateLogdSizeValues() {
        if (this.mLogdSize != null) {
            final String value = SystemProperties.get("persist.log.tag");
            String value2;
            final String s = value2 = SystemProperties.get("persist.logd.size");
            if (value != null) {
                value2 = s;
                if (value.startsWith("Settings")) {
                    value2 = "32768";
                }
            }
            LocalBroadcastManager.getInstance(this.mContext).sendBroadcastSync(new Intent("com.android.settingslib.development.AbstractLogdSizePreferenceController.LOGD_SIZE_UPDATED").putExtra("CURRENT_LOGD_VALUE", value2));
            String defaultLogdSizeValue = null;
            Label_0082: {
                if (value2 != null) {
                    defaultLogdSizeValue = value2;
                    if (value2.length() != 0) {
                        break Label_0082;
                    }
                }
                defaultLogdSizeValue = this.defaultLogdSizeValue();
            }
            final String[] stringArray = this.mContext.getResources().getStringArray(R.array.select_logd_size_values);
            String[] array = this.mContext.getResources().getStringArray(R.array.select_logd_size_titles);
            int n = 2;
            if (SystemProperties.get("ro.config.low_ram").equals("true")) {
                this.mLogdSize.setEntries(R.array.select_logd_size_lowram_titles);
                array = this.mContext.getResources().getStringArray(R.array.select_logd_size_lowram_titles);
                n = 1;
            }
            final String[] stringArray2 = this.mContext.getResources().getStringArray(R.array.select_logd_size_summaries);
            int n2 = 0;
            int n3;
            while (true) {
                n3 = n;
                if (n2 >= array.length) {
                    break;
                }
                if (defaultLogdSizeValue.equals(stringArray[n2]) || defaultLogdSizeValue.equals(array[n2])) {
                    n3 = n2;
                    break;
                }
                ++n2;
            }
            this.mLogdSize.setValue(stringArray[n3]);
            this.mLogdSize.setSummary(stringArray2[n3]);
        }
    }
    
    public void writeLogdSizeOption(Object o) {
        final boolean b = o != null && o.toString().equals("32768");
        String value;
        if ((value = SystemProperties.get("persist.log.tag")) == null) {
            value = "";
        }
        String replaceFirst;
        final String s = replaceFirst = value.replaceAll(",+Settings", "").replaceFirst("^Settings,*", "").replaceAll(",+", ",").replaceFirst(",+$", "");
        if (b) {
            final String s2 = "65536";
            final String value2 = SystemProperties.get("persist.log.tag.snet_event_log");
            if (value2 == null || value2.length() == 0) {
                final String value3 = SystemProperties.get("log.tag.snet_event_log");
                if (value3 == null || value3.length() == 0) {
                    SystemProperties.set("persist.log.tag.snet_event_log", "I");
                }
            }
            String string = s;
            if (s.length() != 0) {
                o = new StringBuilder();
                ((StringBuilder)o).append(",");
                ((StringBuilder)o).append(s);
                string = ((StringBuilder)o).toString();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Settings");
            sb.append(string);
            final String string2 = sb.toString();
            o = s2;
            replaceFirst = string2;
        }
        if (!replaceFirst.equals(value)) {
            SystemProperties.set("persist.log.tag", replaceFirst);
        }
        final String defaultLogdSizeValue = this.defaultLogdSizeValue();
        String string3;
        if (o != null && o.toString().length() != 0) {
            string3 = o.toString();
        }
        else {
            string3 = defaultLogdSizeValue;
        }
        if (defaultLogdSizeValue.equals(string3)) {
            string3 = "";
        }
        SystemProperties.set("persist.logd.size", string3);
        SystemProperties.set("ctl.start", "logd-reinit");
        SystemPropPoker.getInstance().poke();
        this.updateLogdSizeValues();
    }
}
