package com.android.settingslib.development;

import com.android.settingslib.R;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.support.v7.preference.PreferenceScreen;
import android.os.SystemProperties;
import android.content.Intent;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;
import android.content.BroadcastReceiver;
import android.support.v7.preference.ListPreference;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import com.android.settingslib.core.lifecycle.LifecycleObserver;
import com.android.settingslib.core.ConfirmationDialogController;
import android.support.v7.preference.Preference;

public abstract class AbstractLogpersistPreferenceController extends DeveloperOptionsPreferenceController implements OnPreferenceChangeListener, ConfirmationDialogController, LifecycleObserver, OnCreate, OnDestroy
{
    static final String ACTUAL_LOGPERSIST_PROPERTY = "logd.logpersistd";
    static final String ACTUAL_LOGPERSIST_PROPERTY_BUFFER = "logd.logpersistd.buffer";
    static final String SELECT_LOGPERSIST_PROPERTY_SERVICE = "logcatd";
    private ListPreference mLogpersist;
    private boolean mLogpersistCleared;
    private final BroadcastReceiver mReceiver;
    
    public AbstractLogpersistPreferenceController(final Context context, final Lifecycle lifecycle) {
        super(context);
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                AbstractLogpersistPreferenceController.this.onLogdSizeSettingUpdate(intent.getStringExtra("CURRENT_LOGD_VALUE"));
            }
        };
        if (this.isAvailable() && lifecycle != null) {
            lifecycle.addObserver(this);
        }
    }
    
    private void onLogdSizeSettingUpdate(final String s) {
        if (this.mLogpersist != null) {
            final String value = SystemProperties.get("logd.logpersistd.enable");
            if (value != null && value.equals("true") && !s.equals("32768")) {
                if (DevelopmentSettingsEnabler.isDevelopmentSettingsEnabled(this.mContext)) {
                    this.mLogpersist.setEnabled(true);
                }
            }
            else {
                this.writeLogpersistOption(null, true);
                this.mLogpersist.setEnabled(false);
            }
        }
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mLogpersist = (ListPreference)preferenceScreen.findPreference("select_logpersist");
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "select_logpersist";
    }
    
    @Override
    public boolean isAvailable() {
        return TextUtils.equals((CharSequence)SystemProperties.get("ro.debuggable", "0"), (CharSequence)"1");
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        LocalBroadcastManager.getInstance(this.mContext).registerReceiver(this.mReceiver, new IntentFilter("com.android.settingslib.development.AbstractLogdSizePreferenceController.LOGD_SIZE_UPDATED"));
    }
    
    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(this.mContext).unregisterReceiver(this.mReceiver);
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (preference == this.mLogpersist) {
            this.writeLogpersistOption(o, false);
            return true;
        }
        return false;
    }
    
    protected void setLogpersistOff(final boolean b) {
        SystemProperties.set("persist.logd.logpersistd.buffer", "");
        SystemProperties.set("logd.logpersistd.buffer", "");
        SystemProperties.set("persist.logd.logpersistd", "");
        String s;
        if (b) {
            s = "";
        }
        else {
            s = "stop";
        }
        SystemProperties.set("logd.logpersistd", s);
        SystemPropPoker.getInstance().poke();
        if (b) {
            this.updateLogpersistValues();
        }
        else {
            for (int i = 0; i < 3; ++i) {
                final String value = SystemProperties.get("logd.logpersistd");
                if (value == null) {
                    break;
                }
                if (value.equals("")) {
                    break;
                }
                try {
                    Thread.sleep(100L);
                }
                catch (InterruptedException ex) {}
            }
        }
    }
    
    public void updateLogpersistValues() {
        if (this.mLogpersist == null) {
            return;
        }
        String value;
        if ((value = SystemProperties.get("logd.logpersistd")) == null) {
            value = "";
        }
        final String value2 = SystemProperties.get("logd.logpersistd.buffer");
        String s = null;
        Label_0045: {
            if (value2 != null) {
                s = value2;
                if (value2.length() != 0) {
                    break Label_0045;
                }
            }
            s = "all";
        }
        int n = 0;
        if (value.equals("logcatd")) {
            final int n2 = 1;
            if (s.equals("kernel")) {
                n = 3;
            }
            else {
                n = n2;
                if (!s.equals("all")) {
                    n = n2;
                    if (!s.contains("radio")) {
                        n = n2;
                        if (s.contains("security")) {
                            n = n2;
                            if (s.contains("kernel")) {
                                final int n3 = n = 2;
                                if (!s.contains("default")) {
                                    final String[] array = { "main", "events", "system", "crash" };
                                    final int length = array.length;
                                    int n4 = 0;
                                    while (true) {
                                        n = n3;
                                        if (n4 >= length) {
                                            break;
                                        }
                                        if (!s.contains(array[n4])) {
                                            n = 1;
                                            break;
                                        }
                                        ++n4;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.mLogpersist.setValue(this.mContext.getResources().getStringArray(R.array.select_logpersist_values)[n]);
        this.mLogpersist.setSummary(this.mContext.getResources().getStringArray(R.array.select_logpersist_summaries)[n]);
        if (n != 0) {
            this.mLogpersistCleared = false;
        }
        else if (!this.mLogpersistCleared) {
            SystemProperties.set("logd.logpersistd", "clear");
            SystemPropPoker.getInstance().poke();
            this.mLogpersistCleared = true;
        }
    }
    
    public void writeLogpersistOption(final Object o, final boolean b) {
        if (this.mLogpersist == null) {
            return;
        }
        final String value = SystemProperties.get("persist.log.tag");
        Object o2 = o;
        boolean b2 = b;
        if (value != null) {
            o2 = o;
            b2 = b;
            if (value.startsWith("Settings")) {
                o2 = null;
                b2 = true;
            }
        }
        int i = 0;
        if (o2 != null && !o2.toString().equals("")) {
            final String value2 = SystemProperties.get("logd.logpersistd.buffer");
            if (value2 != null && !value2.equals(o2.toString())) {
                this.setLogpersistOff(false);
            }
            SystemProperties.set("persist.logd.logpersistd.buffer", o2.toString());
            SystemProperties.set("persist.logd.logpersistd", "logcatd");
            SystemPropPoker.getInstance().poke();
            while (i < 3) {
                final String value3 = SystemProperties.get("logd.logpersistd");
                if (value3 != null && value3.equals("logcatd")) {
                    break;
                }
                try {
                    Thread.sleep(100L);
                }
                catch (InterruptedException ex) {}
                ++i;
            }
            this.updateLogpersistValues();
            return;
        }
        if (b2) {
            this.mLogpersistCleared = false;
        }
        else if (!this.mLogpersistCleared) {
            final String value4 = SystemProperties.get("logd.logpersistd");
            if (value4 != null && value4.equals("logcatd")) {
                this.showConfirmationDialog(this.mLogpersist);
                return;
            }
        }
        this.setLogpersistOff(true);
    }
}
