package com.android.settingslib.development;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.provider.Settings;
import android.os.Build;
import android.os.UserManager;
import android.content.Context;

public class DevelopmentSettingsEnabler
{
    public static boolean isDevelopmentSettingsEnabled(final Context context) {
        final UserManager userManager = (UserManager)context.getSystemService("user");
        final int int1 = Settings.Global.getInt(context.getContentResolver(), "development_settings_enabled", (int)(Build.TYPE.equals("eng") ? 1 : 0));
        final boolean b = false;
        final boolean b2 = int1 != 0;
        final boolean hasUserRestriction = userManager.hasUserRestriction("no_debugging_features");
        final boolean b3 = userManager.isAdminUser() || userManager.isDemoUser();
        boolean b4 = b;
        if (b3) {
            b4 = b;
            if (!hasUserRestriction) {
                b4 = b;
                if (b2) {
                    b4 = true;
                }
            }
        }
        return b4;
    }
    
    public static void setDevelopmentSettingsEnabled(final Context context, final boolean b) {
        Settings.Global.putInt(context.getContentResolver(), "development_settings_enabled", (int)(b ? 1 : 0));
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent("com.android.settingslib.development.DevelopmentSettingsEnabler.SETTINGS_CHANGED"));
    }
}
