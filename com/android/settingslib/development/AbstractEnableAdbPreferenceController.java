package com.android.settingslib.development;

import android.support.v7.preference.TwoStatePreference;
import android.app.ActivityManager;
import android.os.UserManager;
import android.text.TextUtils;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Context;
import android.support.v14.preference.SwitchPreference;
import com.android.settingslib.core.ConfirmationDialogController;

public abstract class AbstractEnableAdbPreferenceController extends DeveloperOptionsPreferenceController implements ConfirmationDialogController
{
    protected SwitchPreference mPreference;
    
    public AbstractEnableAdbPreferenceController(final Context context) {
        super(context);
    }
    
    private boolean isAdbEnabled() {
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "adb_enabled", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    private void notifyStateChanged() {
        LocalBroadcastManager.getInstance(this.mContext).sendBroadcast(new Intent("com.android.settingslib.development.AbstractEnableAdbController.ENABLE_ADB_STATE_CHANGED"));
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        if (this.isAvailable()) {
            this.mPreference = (SwitchPreference)preferenceScreen.findPreference("enable_adb");
        }
    }
    
    @Override
    public String getPreferenceKey() {
        return "enable_adb";
    }
    
    @Override
    public boolean handlePreferenceTreeClick(final Preference preference) {
        if (this.isUserAMonkey()) {
            return false;
        }
        if (TextUtils.equals((CharSequence)"enable_adb", (CharSequence)preference.getKey())) {
            if (!this.isAdbEnabled()) {
                this.showConfirmationDialog(preference);
            }
            else {
                this.writeAdbSetting(false);
            }
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isAvailable() {
        final UserManager userManager = (UserManager)this.mContext.getSystemService((Class)UserManager.class);
        return userManager != null && (userManager.isAdminUser() || userManager.isDemoUser());
    }
    
    boolean isUserAMonkey() {
        return ActivityManager.isUserAMonkey();
    }
    
    @Override
    public void updateState(final Preference preference) {
        ((TwoStatePreference)preference).setChecked(this.isAdbEnabled());
    }
    
    protected void writeAdbSetting(final boolean b) {
        Settings.Global.putInt(this.mContext.getContentResolver(), "adb_enabled", (int)(b ? 1 : 0));
        this.notifyStateChanged();
    }
}
