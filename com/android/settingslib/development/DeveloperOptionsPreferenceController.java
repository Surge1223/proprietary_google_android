package com.android.settingslib.development;

import android.support.v7.preference.PreferenceScreen;
import android.content.Context;
import android.support.v7.preference.Preference;
import com.android.settingslib.core.AbstractPreferenceController;

public abstract class DeveloperOptionsPreferenceController extends AbstractPreferenceController
{
    protected Preference mPreference;
    
    public DeveloperOptionsPreferenceController(final Context context) {
        super(context);
    }
    
    @Override
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        super.displayPreference(preferenceScreen);
        this.mPreference = preferenceScreen.findPreference(this.getPreferenceKey());
    }
    
    @Override
    public boolean isAvailable() {
        return true;
    }
    
    public void onDeveloperOptionsDisabled() {
        if (this.isAvailable()) {
            this.onDeveloperOptionsSwitchDisabled();
        }
    }
    
    public void onDeveloperOptionsEnabled() {
        if (this.isAvailable()) {
            this.onDeveloperOptionsSwitchEnabled();
        }
    }
    
    protected void onDeveloperOptionsSwitchDisabled() {
        this.mPreference.setEnabled(false);
    }
    
    protected void onDeveloperOptionsSwitchEnabled() {
        this.mPreference.setEnabled(true);
    }
}
