package com.android.settingslib;

import android.content.pm.PackageManager;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import android.text.format.DateFormat;
import java.util.Locale;
import android.os.Build.VERSION;
import java.io.IOException;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionInfo;
import android.system.Os;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.text.TextUtils;
import java.util.regex.Matcher;
import android.util.Log;
import java.util.regex.Pattern;
import android.system.StructUtsname;
import android.content.Context;

public class DeviceInfoUtils
{
    static String formatKernelVersion(final Context context, final StructUtsname structUtsname) {
        if (structUtsname == null) {
            return context.getString(R.string.status_unavailable);
        }
        final Matcher matcher = Pattern.compile("(#\\d+) (?:.*?)?((Sun|Mon|Tue|Wed|Thu|Fri|Sat).+)").matcher(structUtsname.version);
        if (!matcher.matches()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Regex did not match on uname version ");
            sb.append(structUtsname.version);
            Log.e("DeviceInfoUtils", sb.toString());
            return context.getString(R.string.status_unavailable);
        }
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(structUtsname.release);
        sb2.append("\n");
        sb2.append(matcher.group(1));
        sb2.append(" ");
        sb2.append(matcher.group(2));
        return sb2.toString();
    }
    
    public static String getFeedbackReporterPackage(Context packageManager) {
        final String string = packageManager.getResources().getString(R.string.oem_preferred_feedback_reporter);
        if (TextUtils.isEmpty((CharSequence)string)) {
            return string;
        }
        final Intent intent = new Intent("android.intent.action.BUG_REPORT");
        packageManager = (Context)packageManager.getPackageManager();
        for (final ResolveInfo resolveInfo : ((PackageManager)packageManager).queryIntentActivities(intent, 64)) {
            if (resolveInfo.activityInfo != null && !TextUtils.isEmpty((CharSequence)resolveInfo.activityInfo.packageName)) {
                try {
                    if ((((PackageManager)packageManager).getApplicationInfo(resolveInfo.activityInfo.packageName, 0).flags & 0x1) != 0x0 && TextUtils.equals((CharSequence)resolveInfo.activityInfo.packageName, (CharSequence)string)) {
                        return string;
                    }
                    continue;
                }
                catch (PackageManager$NameNotFoundException ex) {}
            }
        }
        return null;
    }
    
    public static String getFormattedKernelVersion(final Context context) {
        return formatKernelVersion(context, Os.uname());
    }
    
    public static String getFormattedPhoneNumber(final Context context, final SubscriptionInfo subscriptionInfo) {
        String formatNumber = null;
        if (subscriptionInfo != null) {
            final String line1Number = ((TelephonyManager)context.getSystemService("phone")).getLine1Number(subscriptionInfo.getSubscriptionId());
            formatNumber = formatNumber;
            if (!TextUtils.isEmpty((CharSequence)line1Number)) {
                formatNumber = PhoneNumberUtils.formatNumber(line1Number);
            }
        }
        return formatNumber;
    }
    
    public static String getMsvSuffix() {
        try {
            if (Long.parseLong(readLine("/sys/board_properties/soc/msv"), 16) == 0L) {
                return " (ENGINEERING)";
            }
        }
        catch (IOException ex) {}
        catch (NumberFormatException ex2) {}
        return "";
    }
    
    public static String getSecurityPatch() {
        String s = Build.VERSION.SECURITY_PATCH;
        if (!"".equals(s)) {
            try {
                s = DateFormat.format((CharSequence)DateFormat.getBestDateTimePattern(Locale.getDefault(), "dMMMMyyyy"), new SimpleDateFormat("yyyy-MM-dd").parse(s)).toString();
            }
            catch (ParseException ex) {}
            return s;
        }
        return null;
    }
    
    private static String readLine(String s) throws IOException {
        s = (String)new BufferedReader(new FileReader(s), 256);
        try {
            return ((BufferedReader)s).readLine();
        }
        finally {
            ((BufferedReader)s).close();
        }
    }
}
