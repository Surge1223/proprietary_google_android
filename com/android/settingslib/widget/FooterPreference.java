package com.android.settingslib.widget;

import android.text.method.MovementMethod;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v4.content.res.TypedArrayUtils;
import com.android.settingslib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class FooterPreference extends Preference
{
    public FooterPreference(final Context context) {
        this(context, null);
    }
    
    public FooterPreference(final Context context, final AttributeSet set) {
        super(context, set, TypedArrayUtils.getAttr(context, R.attr.footerPreferenceStyle, 16842894));
        this.init();
    }
    
    private void init() {
        this.setIcon(R.drawable.ic_info_outline_24dp);
        this.setKey("footer_preference");
        this.setOrder(2147483646);
        this.setSelectable(false);
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final TextView textView = (TextView)preferenceViewHolder.itemView.findViewById(16908310);
        textView.setMovementMethod((MovementMethod)new LinkMovementMethod());
        textView.setClickable(false);
        textView.setLongClickable(false);
    }
}
