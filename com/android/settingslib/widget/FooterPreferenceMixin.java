package com.android.settingslib.widget;

import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.Preference;
import android.content.Context;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.support.v14.preference.PreferenceFragment;
import com.android.settingslib.core.lifecycle.events.SetPreferenceScreen;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class FooterPreferenceMixin implements LifecycleObserver, SetPreferenceScreen
{
    private FooterPreference mFooterPreference;
    private final PreferenceFragment mFragment;
    
    public FooterPreferenceMixin(final PreferenceFragment mFragment, final Lifecycle lifecycle) {
        this.mFragment = mFragment;
        lifecycle.addObserver(this);
    }
    
    private Context getPrefContext() {
        return this.mFragment.getPreferenceManager().getContext();
    }
    
    public FooterPreference createFooterPreference() {
        final PreferenceScreen preferenceScreen = this.mFragment.getPreferenceScreen();
        if (this.mFooterPreference != null && preferenceScreen != null) {
            preferenceScreen.removePreference(this.mFooterPreference);
        }
        this.mFooterPreference = new FooterPreference(this.getPrefContext());
        if (preferenceScreen != null) {
            preferenceScreen.addPreference(this.mFooterPreference);
        }
        return this.mFooterPreference;
    }
    
    public boolean hasFooter() {
        return this.mFooterPreference != null;
    }
    
    @Override
    public void setPreferenceScreen(final PreferenceScreen preferenceScreen) {
        if (this.mFooterPreference != null) {
            preferenceScreen.addPreference(this.mFooterPreference);
        }
    }
}
