package com.android.settingslib;

import android.graphics.Paint$FontMetricsInt;
import android.graphics.Paint;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.content.Context;
import android.text.style.ImageSpan;

public class RestrictedLockImageSpan extends ImageSpan
{
    private Context mContext;
    private final float mExtraPadding;
    private final Drawable mRestrictedPadlock;
    
    public RestrictedLockImageSpan(final Context mContext) {
        super((Drawable)null);
        this.mContext = mContext;
        this.mExtraPadding = this.mContext.getResources().getDimensionPixelSize(R.dimen.restricted_icon_padding);
        this.mRestrictedPadlock = RestrictedLockUtils.getRestrictedPadlock(this.mContext);
    }
    
    public void draw(final Canvas canvas, final CharSequence charSequence, final int n, final int n2, final float n3, final int n4, final int n5, final int n6, final Paint paint) {
        final Drawable drawable = this.getDrawable();
        canvas.save();
        canvas.translate(this.mExtraPadding + n3, (n6 - drawable.getBounds().bottom) / 2.0f);
        drawable.draw(canvas);
        canvas.restore();
    }
    
    public Drawable getDrawable() {
        return this.mRestrictedPadlock;
    }
    
    public int getSize(final Paint paint, final CharSequence charSequence, final int n, final int n2, final Paint$FontMetricsInt paint$FontMetricsInt) {
        return (int)(super.getSize(paint, charSequence, n, n2, paint$FontMetricsInt) + 2.0f * this.mExtraPadding);
    }
}
