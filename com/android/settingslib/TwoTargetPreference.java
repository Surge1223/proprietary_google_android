package com.android.settingslib;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.ImageView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.Preference;

public class TwoTargetPreference extends Preference
{
    private int mIconSize;
    private int mMediumIconSize;
    private int mSmallIconSize;
    
    public TwoTargetPreference(final Context context) {
        super(context);
        this.init(context);
    }
    
    public TwoTargetPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(context);
    }
    
    public TwoTargetPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(context);
    }
    
    public TwoTargetPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.init(context);
    }
    
    private void init(final Context context) {
        this.setLayoutResource(R.layout.preference_two_target);
        this.mSmallIconSize = context.getResources().getDimensionPixelSize(R.dimen.two_target_pref_small_icon_size);
        this.mMediumIconSize = context.getResources().getDimensionPixelSize(R.dimen.two_target_pref_medium_icon_size);
        final int secondTargetResId = this.getSecondTargetResId();
        if (secondTargetResId != 0) {
            this.setWidgetLayoutResource(secondTargetResId);
        }
    }
    
    protected int getSecondTargetResId() {
        return 0;
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        final ImageView imageView = (ImageView)preferenceViewHolder.itemView.findViewById(16908294);
        switch (this.mIconSize) {
            case 2: {
                imageView.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout$LayoutParams(this.mSmallIconSize, this.mSmallIconSize));
                break;
            }
            case 1: {
                imageView.setLayoutParams((ViewGroup.LayoutParams)new LinearLayout$LayoutParams(this.mMediumIconSize, this.mMediumIconSize));
                break;
            }
        }
        final View viewById = preferenceViewHolder.findViewById(R.id.two_target_divider);
        final View viewById2 = preferenceViewHolder.findViewById(16908312);
        final boolean shouldHideSecondTarget = this.shouldHideSecondTarget();
        final boolean b = false;
        if (viewById != null) {
            int visibility;
            if (shouldHideSecondTarget) {
                visibility = 8;
            }
            else {
                visibility = 0;
            }
            viewById.setVisibility(visibility);
        }
        if (viewById2 != null) {
            int visibility2 = b ? 1 : 0;
            if (shouldHideSecondTarget) {
                visibility2 = 8;
            }
            viewById2.setVisibility(visibility2);
        }
    }
    
    public void setIconSize(final int mIconSize) {
        this.mIconSize = mIconSize;
    }
    
    protected boolean shouldHideSecondTarget() {
        return this.getSecondTargetResId() == 0;
    }
}
