package com.android.settingslib.datetime;

import libcore.util.TimeZoneFinder;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.util.Log;
import com.android.settingslib.R;
import java.util.ArrayList;
import android.icu.text.TimeZoneNames$NameType;
import android.icu.text.TimeZoneNames;
import android.support.v4.text.TextDirectionHeuristicCompat;
import android.support.v4.text.TextDirectionHeuristicsCompat;
import android.text.TextUtils;
import android.support.v4.text.BidiFormatter;
import android.text.SpannableString;
import android.text.style.TtsSpan$VerbatimBuilder;
import android.text.style.TtsSpan$MeasureBuilder;
import android.icu.text.TimeZoneFormat$GMTOffsetPatternType;
import android.text.style.TtsSpan$TextBuilder;
import java.util.Date;
import java.util.Locale;
import android.icu.text.TimeZoneFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import android.text.style.TtsSpan;
import android.text.SpannableStringBuilder;
import java.util.List;
import android.content.Context;

public class ZoneGetter
{
    private static void appendWithTtsSpan(final SpannableStringBuilder spannableStringBuilder, final CharSequence charSequence, final TtsSpan ttsSpan) {
        final int length = spannableStringBuilder.length();
        spannableStringBuilder.append(charSequence);
        spannableStringBuilder.setSpan((Object)ttsSpan, length, spannableStringBuilder.length(), 0);
    }
    
    private static Map<String, Object> createDisplayEntry(final TimeZone timeZone, final CharSequence charSequence, final CharSequence charSequence2, final int n) {
        final HashMap<String, String> hashMap = (HashMap<String, String>)new HashMap<String, Integer>();
        hashMap.put("id", (Integer)timeZone.getID());
        hashMap.put("name", (Integer)charSequence2.toString());
        hashMap.put("display_label", (Integer)charSequence2);
        hashMap.put("gmt", (Integer)charSequence.toString());
        hashMap.put("offset_label", (Integer)charSequence);
        hashMap.put("offset", n);
        return (Map<String, Object>)hashMap;
    }
    
    private static String formatDigits(final int n, final int n2, final String s) {
        final int n3 = n / 10;
        final StringBuilder sb = new StringBuilder(n2);
        if (n >= 10 || n2 == 2) {
            sb.append(s.charAt(n3));
        }
        sb.append(s.charAt(n % 10));
        return sb.toString();
    }
    
    public static CharSequence getGmtOffsetText(final TimeZoneFormat timeZoneFormat, final Locale locale, final TimeZone timeZone, final Date date) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        final String gmtPattern = timeZoneFormat.getGMTPattern();
        final int index = gmtPattern.indexOf("{0}");
        String substring;
        String substring2;
        if (index == -1) {
            substring = "GMT";
            substring2 = "";
        }
        else {
            substring = gmtPattern.substring(0, index);
            substring2 = gmtPattern.substring(index + 3);
        }
        if (!substring.isEmpty()) {
            appendWithTtsSpan(spannableStringBuilder, substring, new TtsSpan$TextBuilder(substring).build());
        }
        int offset = timeZone.getOffset(date.getTime());
        TimeZoneFormat$GMTOffsetPatternType timeZoneFormat$GMTOffsetPatternType;
        if (offset < 0) {
            offset = -offset;
            timeZoneFormat$GMTOffsetPatternType = TimeZoneFormat$GMTOffsetPatternType.NEGATIVE_HM;
        }
        else {
            timeZoneFormat$GMTOffsetPatternType = TimeZoneFormat$GMTOffsetPatternType.POSITIVE_HM;
        }
        final String gmtOffsetPattern = timeZoneFormat.getGMTOffsetPattern(timeZoneFormat$GMTOffsetPatternType);
        final String gmtOffsetDigits = timeZoneFormat.getGMTOffsetDigits();
        final int n = (int)(offset / 3600000L);
        final int abs = Math.abs((int)(offset / 60000L));
        for (int i = 0; i < gmtOffsetPattern.length(); ++i) {
            final char char1 = gmtOffsetPattern.charAt(i);
            if (char1 != '+' && char1 != '-' && char1 != '\u2212') {
                if (char1 != 'H' && char1 != 'm') {
                    spannableStringBuilder.append(char1);
                }
                else {
                    int n2;
                    if (i + 1 < gmtOffsetPattern.length() && gmtOffsetPattern.charAt(i + 1) == char1) {
                        n2 = 2;
                        ++i;
                    }
                    else {
                        n2 = 1;
                    }
                    int n3;
                    String unit;
                    if (char1 == 'H') {
                        n3 = n;
                        unit = "hour";
                    }
                    else {
                        n3 = abs % 60;
                        unit = "minute";
                    }
                    appendWithTtsSpan(spannableStringBuilder, formatDigits(n3, n2, gmtOffsetDigits), new TtsSpan$MeasureBuilder().setNumber((long)n3).setUnit(unit).build());
                }
            }
            else {
                final String value = String.valueOf(char1);
                appendWithTtsSpan(spannableStringBuilder, value, new TtsSpan$VerbatimBuilder(value).build());
            }
        }
        if (!substring2.isEmpty()) {
            appendWithTtsSpan(spannableStringBuilder, substring2, new TtsSpan$TextBuilder(substring2).build());
        }
        final SpannableString spannableString = new SpannableString((CharSequence)spannableStringBuilder);
        final BidiFormatter instance = BidiFormatter.getInstance();
        TextDirectionHeuristicCompat textDirectionHeuristicCompat;
        if (TextUtils.getLayoutDirectionFromLocale(locale) == 1) {
            textDirectionHeuristicCompat = TextDirectionHeuristicsCompat.RTL;
        }
        else {
            textDirectionHeuristicCompat = TextDirectionHeuristicsCompat.LTR;
        }
        return instance.unicodeWrap((CharSequence)spannableString, textDirectionHeuristicCompat);
    }
    
    private static CharSequence getTimeZoneDisplayName(final ZoneGetterData zoneGetterData, final TimeZoneNames timeZoneNames, final boolean b, final TimeZone timeZone, String canonicalID) {
        final Date date = new Date();
        String s;
        if (zoneGetterData.localZoneIds.contains(canonicalID) && !b) {
            s = getZoneLongName(timeZoneNames, timeZone, date);
        }
        else {
            canonicalID = android.icu.util.TimeZone.getCanonicalID(timeZone.getID());
            String id;
            if ((id = canonicalID) == null) {
                id = timeZone.getID();
            }
            s = timeZoneNames.getExemplarLocationName(id);
            if (s == null || s.isEmpty()) {
                s = getZoneLongName(timeZoneNames, timeZone, date);
            }
        }
        return s;
    }
    
    public static CharSequence getTimeZoneOffsetAndName(final Context context, final TimeZone timeZone, final Date date) {
        final Locale locale = context.getResources().getConfiguration().locale;
        final CharSequence gmtOffsetText = getGmtOffsetText(TimeZoneFormat.getInstance(locale), locale, timeZone, date);
        final String zoneLongName = getZoneLongName(TimeZoneNames.getInstance(locale), timeZone, date);
        if (zoneLongName == null) {
            return gmtOffsetText;
        }
        return TextUtils.concat(new CharSequence[] { gmtOffsetText, " ", zoneLongName });
    }
    
    private static String getZoneLongName(final TimeZoneNames timeZoneNames, final TimeZone timeZone, final Date date) {
        TimeZoneNames$NameType timeZoneNames$NameType;
        if (timeZone.inDaylightTime(date)) {
            timeZoneNames$NameType = TimeZoneNames$NameType.LONG_DAYLIGHT;
        }
        else {
            timeZoneNames$NameType = TimeZoneNames$NameType.LONG_STANDARD;
        }
        return timeZoneNames.getDisplayName(timeZone.getID(), timeZoneNames$NameType, date.getTime());
    }
    
    public static List<Map<String, Object>> getZonesList(final Context context) {
        final Locale locale = context.getResources().getConfiguration().locale;
        final Date date = new Date();
        final TimeZoneNames instance = TimeZoneNames.getInstance(locale);
        final ZoneGetterData zoneGetterData = new ZoneGetterData(context);
        final boolean shouldUseExemplarLocationForLocalNames = shouldUseExemplarLocationForLocalNames(zoneGetterData, instance);
        final ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < zoneGetterData.zoneCount; ++i) {
            final TimeZone timeZone = zoneGetterData.timeZones[i];
            final CharSequence charSequence = zoneGetterData.gmtOffsetTexts[i];
            CharSequence timeZoneDisplayName;
            if (TextUtils.isEmpty(timeZoneDisplayName = getTimeZoneDisplayName(zoneGetterData, instance, shouldUseExemplarLocationForLocalNames, timeZone, zoneGetterData.olsonIdsToDisplay[i]))) {
                timeZoneDisplayName = charSequence;
            }
            list.add(createDisplayEntry(timeZone, charSequence, timeZoneDisplayName, timeZone.getOffset(date.getTime())));
        }
        return list;
    }
    
    private static List<String> readTimezonesToDisplay(Context context) {
        final ArrayList<String> list = new ArrayList<String>();
        Label_0259: {
            try {
                final XmlResourceParser xml = context.getResources().getXml(R.xml.timezones);
                final Context context2 = null;
                while (true) {
                    context = context2;
                    try {
                        try {
                            if (xml.next() != 2) {
                                continue;
                            }
                            context = context2;
                            xml.next();
                            while (true) {
                                context = context2;
                                if (xml.getEventType() != 3) {
                                    while (true) {
                                        context = context2;
                                        if (xml.getEventType() == 2) {
                                            context = context2;
                                            if (xml.getName().equals("timezone")) {
                                                context = context2;
                                                list.add(xml.getAttributeValue(0));
                                            }
                                            while (true) {
                                                context = context2;
                                                if (xml.getEventType() == 3) {
                                                    break;
                                                }
                                                context = context2;
                                                xml.next();
                                            }
                                            context = context2;
                                            xml.next();
                                            break;
                                        }
                                        context = context2;
                                        if (xml.getEventType() == 1) {
                                            if (xml != null) {
                                                xml.close();
                                            }
                                            return list;
                                        }
                                        context = context2;
                                        xml.next();
                                    }
                                }
                                else {
                                    if (xml != null) {
                                        xml.close();
                                        break Label_0259;
                                    }
                                    break Label_0259;
                                }
                            }
                        }
                        finally {
                            if (xml != null) {
                                if (context != null) {
                                    final XmlResourceParser xmlResourceParser = xml;
                                    xmlResourceParser.close();
                                }
                                else {
                                    xml.close();
                                }
                            }
                        }
                    }
                    catch (Throwable t) {}
                    try {
                        final XmlResourceParser xmlResourceParser = xml;
                        xmlResourceParser.close();
                    }
                    catch (Throwable t2) {}
                }
            }
            catch (IOException ex) {
                Log.e("ZoneGetter", "Unable to read timezones.xml file");
            }
            catch (XmlPullParserException ex2) {
                Log.e("ZoneGetter", "Ill-formatted timezones.xml file");
            }
        }
        return list;
    }
    
    private static boolean shouldUseExemplarLocationForLocalNames(final ZoneGetterData zoneGetterData, final TimeZoneNames timeZoneNames) {
        final HashSet<String> set = new HashSet<String>();
        final Date date = new Date();
        for (int i = 0; i < zoneGetterData.zoneCount; ++i) {
            if (zoneGetterData.localZoneIds.contains(zoneGetterData.olsonIdsToDisplay[i])) {
                CharSequence zoneLongName;
                if ((zoneLongName = getZoneLongName(timeZoneNames, zoneGetterData.timeZones[i], date)) == null) {
                    zoneLongName = zoneGetterData.gmtOffsetTexts[i];
                }
                if (!set.add((String)zoneLongName)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static final class ZoneGetterData
    {
        public final CharSequence[] gmtOffsetTexts;
        public final Set<String> localZoneIds;
        public final String[] olsonIdsToDisplay;
        public final TimeZone[] timeZones;
        public final int zoneCount;
        
        public ZoneGetterData(final Context context) {
            final Locale locale = context.getResources().getConfiguration().locale;
            final TimeZoneFormat instance = TimeZoneFormat.getInstance(locale);
            final Date date = new Date();
            final List access$000 = readTimezonesToDisplay(context);
            this.zoneCount = access$000.size();
            this.olsonIdsToDisplay = new String[this.zoneCount];
            this.timeZones = new TimeZone[this.zoneCount];
            this.gmtOffsetTexts = new CharSequence[this.zoneCount];
            for (int i = 0; i < this.zoneCount; ++i) {
                final String s = access$000.get(i);
                this.olsonIdsToDisplay[i] = s;
                final TimeZone timeZone = TimeZone.getTimeZone(s);
                this.timeZones[i] = timeZone;
                this.gmtOffsetTexts[i] = ZoneGetter.getGmtOffsetText(instance, locale, timeZone, date);
            }
            this.localZoneIds = new HashSet<String>(this.lookupTimeZoneIdsByCountry(locale.getCountry()));
        }
        
        public List<String> lookupTimeZoneIdsByCountry(final String s) {
            return (List<String>)TimeZoneFinder.getInstance().lookupTimeZoneIdsByCountry(s);
        }
    }
}
