package com.android.settingslib.utils;

import java.util.concurrent.Executors;
import android.os.Looper;
import java.util.concurrent.ExecutorService;
import android.os.Handler;

public class ThreadUtils
{
    private static volatile Thread sMainThread;
    private static volatile Handler sMainThreadHandler;
    private static volatile ExecutorService sSingleThreadExecutor;
    
    public static void ensureMainThread() {
        if (isMainThread()) {
            return;
        }
        throw new RuntimeException("Must be called on the UI thread");
    }
    
    public static Handler getUiThreadHandler() {
        if (ThreadUtils.sMainThreadHandler == null) {
            ThreadUtils.sMainThreadHandler = new Handler(Looper.getMainLooper());
        }
        return ThreadUtils.sMainThreadHandler;
    }
    
    public static boolean isMainThread() {
        if (ThreadUtils.sMainThread == null) {
            ThreadUtils.sMainThread = Looper.getMainLooper().getThread();
        }
        return Thread.currentThread() == ThreadUtils.sMainThread;
    }
    
    public static void postOnBackgroundThread(final Runnable runnable) {
        if (ThreadUtils.sSingleThreadExecutor == null) {
            ThreadUtils.sSingleThreadExecutor = Executors.newSingleThreadExecutor();
        }
        ThreadUtils.sSingleThreadExecutor.execute(runnable);
    }
    
    public static void postOnMainThread(final Runnable runnable) {
        getUiThreadHandler().post(runnable);
    }
}
