package com.android.settingslib.utils;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.support.v4.util.ArrayMap;
import android.content.Context;

public class IconCache
{
    private final Context mContext;
    final ArrayMap<Icon, Drawable> mMap;
    
    public IconCache(final Context mContext) {
        this.mMap = new ArrayMap<Icon, Drawable>();
        this.mContext = mContext;
    }
    
    public Drawable getIcon(final Icon icon) {
        if (icon == null) {
            return null;
        }
        Drawable loadDrawable;
        if ((loadDrawable = this.mMap.get(icon)) == null) {
            loadDrawable = icon.loadDrawable(this.mContext);
            this.updateIcon(icon, loadDrawable);
        }
        return loadDrawable;
    }
    
    public void updateIcon(final Icon icon, final Drawable drawable) {
        this.mMap.put(icon, drawable);
    }
}
