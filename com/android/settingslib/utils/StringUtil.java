package com.android.settingslib.utils;

import android.icu.text.RelativeDateTimeFormatter$Direction;
import android.icu.text.NumberFormat;
import android.icu.text.RelativeDateTimeFormatter;
import android.icu.text.DisplayContext;
import android.icu.text.RelativeDateTimeFormatter$Style;
import android.icu.util.ULocale;
import android.icu.text.RelativeDateTimeFormatter$RelativeUnit;
import com.android.settingslib.R;
import android.icu.util.TimeUnit;
import android.text.style.TtsSpan$MeasureBuilder;
import android.icu.text.MeasureFormat;
import android.icu.text.MeasureFormat$FormatWidth;
import android.icu.util.MeasureUnit;
import android.icu.util.Measure;
import java.util.ArrayList;
import android.text.SpannableStringBuilder;
import android.content.Context;

public class StringUtil
{
    public static CharSequence formatElapsedTime(final Context context, final double n, final boolean b) {
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        int n3;
        final int n2 = n3 = (int)Math.floor(n / 1000.0);
        if (!b) {
            n3 = n2 + 30;
        }
        int n4 = 0;
        int n5 = 0;
        final int n6 = 0;
        int n7;
        if ((n7 = n3) >= 86400) {
            n4 = n3 / 86400;
            n7 = n3 - 86400 * n4;
        }
        int n8;
        if ((n8 = n7) >= 3600) {
            n5 = n7 / 3600;
            n8 = n7 - n5 * 3600;
        }
        int n9 = n8;
        int n10 = n6;
        if (n8 >= 60) {
            n10 = n8 / 60;
            n9 = n8 - n10 * 60;
        }
        final ArrayList<Measure> list = new ArrayList<Measure>(4);
        if (n4 > 0) {
            list.add(new Measure((Number)n4, (MeasureUnit)MeasureUnit.DAY));
        }
        if (n5 > 0) {
            list.add(new Measure((Number)n5, (MeasureUnit)MeasureUnit.HOUR));
        }
        if (n10 > 0) {
            list.add(new Measure((Number)n10, (MeasureUnit)MeasureUnit.MINUTE));
        }
        if (b && n9 > 0) {
            list.add(new Measure((Number)n9, (MeasureUnit)MeasureUnit.SECOND));
        }
        if (list.size() == 0) {
            TimeUnit timeUnit;
            if (b) {
                timeUnit = MeasureUnit.SECOND;
            }
            else {
                timeUnit = MeasureUnit.MINUTE;
            }
            list.add(new Measure((Number)0, (MeasureUnit)timeUnit));
        }
        final Measure[] array = list.toArray(new Measure[list.size()]);
        spannableStringBuilder.append((CharSequence)MeasureFormat.getInstance(context.getResources().getConfiguration().locale, MeasureFormat$FormatWidth.SHORT).formatMeasures(array));
        if (array.length == 1 && MeasureUnit.MINUTE.equals((Object)array[0].getUnit())) {
            spannableStringBuilder.setSpan((Object)new TtsSpan$MeasureBuilder().setNumber((long)n10).setUnit("minute").build(), 0, spannableStringBuilder.length(), 33);
        }
        return (CharSequence)spannableStringBuilder;
    }
    
    public static CharSequence formatRelativeTime(final Context context, final double n, final boolean b) {
        final int n2 = (int)Math.floor(n / 1000.0);
        if (b && n2 < 120) {
            return context.getResources().getString(R.string.time_unit_just_now);
        }
        RelativeDateTimeFormatter$RelativeUnit relativeDateTimeFormatter$RelativeUnit;
        int n3;
        if (n2 < 7200) {
            relativeDateTimeFormatter$RelativeUnit = RelativeDateTimeFormatter$RelativeUnit.MINUTES;
            n3 = (n2 + 30) / 60;
        }
        else if (n2 < 172800) {
            relativeDateTimeFormatter$RelativeUnit = RelativeDateTimeFormatter$RelativeUnit.HOURS;
            n3 = (n2 + 1800) / 3600;
        }
        else {
            relativeDateTimeFormatter$RelativeUnit = RelativeDateTimeFormatter$RelativeUnit.DAYS;
            n3 = (43200 + n2) / 86400;
        }
        return RelativeDateTimeFormatter.getInstance(ULocale.forLocale(context.getResources().getConfiguration().locale), (NumberFormat)null, RelativeDateTimeFormatter$Style.LONG, DisplayContext.CAPITALIZATION_FOR_MIDDLE_OF_SENTENCE).format((double)n3, RelativeDateTimeFormatter$Direction.LAST, relativeDateTimeFormatter$RelativeUnit);
    }
}
