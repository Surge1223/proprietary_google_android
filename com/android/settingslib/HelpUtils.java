package com.android.settingslib;

import android.net.Uri.Builder;
import android.content.pm.PackageManager;
import java.util.Locale;
import android.content.ActivityNotFoundException;
import android.util.Log;
import com.android.internal.logging.MetricsLogger;
import android.view.MenuItem$OnMenuItemClickListener;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.Menu;
import android.app.Activity;
import java.net.URISyntaxException;
import android.net.Uri;
import android.provider.Settings;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.content.Intent;
import android.content.Context;

public class HelpUtils
{
    private static final String TAG;
    private static String sCachedVersionCode;
    
    static {
        TAG = HelpUtils.class.getSimpleName();
        HelpUtils.sCachedVersionCode = null;
    }
    
    public static void addIntentParameters(final Context context, final Intent intent, String string, final boolean b) {
        if (!intent.hasExtra("EXTRA_CONTEXT")) {
            intent.putExtra("EXTRA_CONTEXT", string);
        }
        final Resources resources = context.getResources();
        final boolean boolean1 = resources.getBoolean(17957018);
        if (b && boolean1) {
            final String[] array = { resources.getString(17039686) };
            final String[] array2 = { resources.getString(17039687) };
            final String string2 = resources.getString(17039684);
            final String string3 = resources.getString(17039685);
            string = resources.getString(17039673);
            final String string4 = resources.getString(17039674);
            intent.putExtra(string2, array);
            intent.putExtra(string3, array2);
            intent.putExtra(string, array);
            intent.putExtra(string4, array2);
        }
        intent.putExtra("EXTRA_THEME", 0);
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { 16843827 });
        intent.putExtra("EXTRA_PRIMARY_COLOR", obtainStyledAttributes.getColor(0, 0));
        obtainStyledAttributes.recycle();
    }
    
    public static Intent getHelpIntent(final Context context, final String s, final String s2) {
        if (Settings.Global.getInt(context.getContentResolver(), "device_provisioned", 0) == 0) {
            return null;
        }
        try {
            final Intent uri = Intent.parseUri(s, 3);
            addIntentParameters(context, uri, s2, true);
            if (uri.resolveActivity(context.getPackageManager()) != null) {
                return uri;
            }
            if (uri.hasExtra("EXTRA_BACKUP_URI")) {
                return getHelpIntent(context, uri.getStringExtra("EXTRA_BACKUP_URI"), s2);
            }
            return null;
        }
        catch (URISyntaxException ex) {
            final Intent intent = new Intent("android.intent.action.VIEW", uriWithAddedParameters(context, Uri.parse(s)));
            intent.setFlags(276824064);
            return intent;
        }
    }
    
    public static boolean prepareHelpMenuItem(final Activity activity, final Menu menu, final int n, final String s) {
        final MenuItem add = menu.add(0, 101, 0, R.string.help_feedback_label);
        add.setIcon(R.drawable.ic_help_actionbar);
        return prepareHelpMenuItem(activity, add, activity.getString(n), s);
    }
    
    public static boolean prepareHelpMenuItem(final Activity activity, final Menu menu, final String s, final String s2) {
        final MenuItem add = menu.add(0, 101, 0, R.string.help_feedback_label);
        add.setIcon(R.drawable.ic_help_actionbar);
        return prepareHelpMenuItem(activity, add, s, s2);
    }
    
    public static boolean prepareHelpMenuItem(final Activity activity, final MenuItem menuItem, final String s, final String s2) {
        if (Settings.Global.getInt(activity.getContentResolver(), "device_provisioned", 0) == 0) {
            return false;
        }
        if (TextUtils.isEmpty((CharSequence)s)) {
            menuItem.setVisible(false);
            return false;
        }
        final Intent helpIntent = getHelpIntent((Context)activity, s, s2);
        if (helpIntent != null) {
            menuItem.setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
                public boolean onMenuItemClick(final MenuItem menuItem) {
                    MetricsLogger.action((Context)activity, 513, helpIntent.getStringExtra("EXTRA_CONTEXT"));
                    try {
                        activity.startActivityForResult(helpIntent, 0);
                    }
                    catch (ActivityNotFoundException ex) {
                        final String access$000 = HelpUtils.TAG;
                        final StringBuilder sb = new StringBuilder();
                        sb.append("No activity found for intent: ");
                        sb.append(helpIntent);
                        Log.e(access$000, sb.toString());
                    }
                    return true;
                }
            });
            menuItem.setShowAsAction(2);
            menuItem.setVisible(true);
            return true;
        }
        menuItem.setVisible(false);
        return false;
    }
    
    private static Uri uriWithAddedParameters(final Context context, Uri buildUpon) {
        buildUpon = (Uri)buildUpon.buildUpon();
        ((Uri.Builder)buildUpon).appendQueryParameter("hl", Locale.getDefault().toString());
        if (HelpUtils.sCachedVersionCode == null) {
            try {
                ((Uri.Builder)buildUpon).appendQueryParameter("version", HelpUtils.sCachedVersionCode = Long.toString(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).getLongVersionCode()));
            }
            catch (PackageManager$NameNotFoundException ex) {
                Log.wtf(HelpUtils.TAG, "Invalid package name for context", (Throwable)ex);
            }
        }
        else {
            ((Uri.Builder)buildUpon).appendQueryParameter("version", HelpUtils.sCachedVersionCode);
        }
        return ((Uri.Builder)buildUpon).build();
    }
}
