package com.android.settingslib;

import android.text.TextUtils;
import android.widget.TextView;
import android.support.v7.preference.PreferenceViewHolder;
import android.util.TypedValue;
import android.content.res.TypedArray;
import android.os.UserHandle;
import android.util.AttributeSet;
import android.support.v7.preference.Preference;
import android.content.Context;

public class RestrictedPreferenceHelper
{
    private String mAttrUserRestriction;
    private final Context mContext;
    private boolean mDisabledByAdmin;
    private RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin;
    private final Preference mPreference;
    private boolean mUseAdminDisabledSummary;
    
    public RestrictedPreferenceHelper(final Context mContext, final Preference mPreference, final AttributeSet set) {
        this.mAttrUserRestriction = null;
        final boolean b = false;
        this.mUseAdminDisabledSummary = false;
        this.mContext = mContext;
        this.mPreference = mPreference;
        if (set != null) {
            final TypedArray obtainStyledAttributes = mContext.obtainStyledAttributes(set, R.styleable.RestrictedPreference);
            final TypedValue peekValue = obtainStyledAttributes.peekValue(R.styleable.RestrictedPreference_userRestriction);
            CharSequence charSequence2;
            final CharSequence charSequence = charSequence2 = null;
            if (peekValue != null) {
                charSequence2 = charSequence;
                if (peekValue.type == 3) {
                    if (peekValue.resourceId != 0) {
                        charSequence2 = mContext.getText(peekValue.resourceId);
                    }
                    else {
                        charSequence2 = peekValue.string;
                    }
                }
            }
            String string;
            if (charSequence2 == null) {
                string = null;
            }
            else {
                string = charSequence2.toString();
            }
            this.mAttrUserRestriction = string;
            if (RestrictedLockUtils.hasBaseUserRestriction(this.mContext, this.mAttrUserRestriction, UserHandle.myUserId())) {
                this.mAttrUserRestriction = null;
                return;
            }
            final TypedValue peekValue2 = obtainStyledAttributes.peekValue(R.styleable.RestrictedPreference_useAdminDisabledSummary);
            if (peekValue2 != null) {
                boolean mUseAdminDisabledSummary = b;
                if (peekValue2.type == 18) {
                    mUseAdminDisabledSummary = b;
                    if (peekValue2.data != 0) {
                        mUseAdminDisabledSummary = true;
                    }
                }
                this.mUseAdminDisabledSummary = mUseAdminDisabledSummary;
            }
        }
    }
    
    public void checkRestrictionAndSetDisabled(final String s, final int n) {
        this.setDisabledByAdmin(RestrictedLockUtils.checkIfRestrictionEnforced(this.mContext, s, n));
    }
    
    public boolean isDisabledByAdmin() {
        return this.mDisabledByAdmin;
    }
    
    public void onAttachedToHierarchy() {
        if (this.mAttrUserRestriction != null) {
            this.checkRestrictionAndSetDisabled(this.mAttrUserRestriction, UserHandle.myUserId());
        }
    }
    
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        if (this.mDisabledByAdmin) {
            preferenceViewHolder.itemView.setEnabled(true);
        }
        if (this.mUseAdminDisabledSummary) {
            final TextView textView = (TextView)preferenceViewHolder.findViewById(16908304);
            if (textView != null) {
                final CharSequence text = textView.getContext().getText(R.string.disabled_by_admin_summary_text);
                if (this.mDisabledByAdmin) {
                    textView.setText(text);
                }
                else if (TextUtils.equals(text, textView.getText())) {
                    textView.setText((CharSequence)null);
                }
            }
        }
    }
    
    public boolean performClick() {
        if (this.mDisabledByAdmin) {
            RestrictedLockUtils.sendShowAdminSupportDetailsIntent(this.mContext, this.mEnforcedAdmin);
            return true;
        }
        return false;
    }
    
    public boolean setDisabledByAdmin(final RestrictedLockUtils.EnforcedAdmin mEnforcedAdmin) {
        boolean enabled = false;
        final boolean mDisabledByAdmin = mEnforcedAdmin != null;
        this.mEnforcedAdmin = mEnforcedAdmin;
        boolean b = false;
        if (this.mDisabledByAdmin != mDisabledByAdmin) {
            this.mDisabledByAdmin = mDisabledByAdmin;
            b = true;
        }
        final Preference mPreference = this.mPreference;
        if (!mDisabledByAdmin) {
            enabled = true;
        }
        mPreference.setEnabled(enabled);
        return b;
    }
    
    public void useAdminDisabledSummary(final boolean mUseAdminDisabledSummary) {
        this.mUseAdminDisabledSummary = mUseAdminDisabledSummary;
    }
}
