package com.android.settingslib.accessibility;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.util.ArraySet;
import android.content.ContentResolver;
import android.content.pm.ServiceInfo;
import android.text.TextUtils;
import android.content.res.Configuration;
import java.util.Locale;
import android.content.pm.ResolveInfo;
import java.util.Iterator;
import java.util.List;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.view.accessibility.AccessibilityManager;
import java.util.HashSet;
import java.util.Collections;
import android.provider.Settings;
import android.os.UserHandle;
import android.content.ComponentName;
import java.util.Set;
import android.content.Context;
import android.text.TextUtils$SimpleStringSplitter;

public class AccessibilityUtils
{
    static final TextUtils$SimpleStringSplitter sStringColonSplitter;
    
    static {
        sStringColonSplitter = new TextUtils$SimpleStringSplitter(':');
    }
    
    public static Set<ComponentName> getEnabledServicesFromSettings(final Context context) {
        return getEnabledServicesFromSettings(context, UserHandle.myUserId());
    }
    
    public static Set<ComponentName> getEnabledServicesFromSettings(final Context context, final int n) {
        final String stringForUser = Settings.Secure.getStringForUser(context.getContentResolver(), "enabled_accessibility_services", n);
        if (stringForUser == null) {
            return Collections.emptySet();
        }
        final HashSet<ComponentName> set = new HashSet<ComponentName>();
        final TextUtils$SimpleStringSplitter sStringColonSplitter = AccessibilityUtils.sStringColonSplitter;
        sStringColonSplitter.setString(stringForUser);
        while (sStringColonSplitter.hasNext()) {
            final ComponentName unflattenFromString = ComponentName.unflattenFromString(sStringColonSplitter.next());
            if (unflattenFromString != null) {
                set.add(unflattenFromString);
            }
        }
        return set;
    }
    
    private static Set<ComponentName> getInstalledServices(final Context context) {
        final HashSet<ComponentName> set = new HashSet<ComponentName>();
        set.clear();
        final List installedAccessibilityServiceList = AccessibilityManager.getInstance(context).getInstalledAccessibilityServiceList();
        if (installedAccessibilityServiceList == null) {
            return set;
        }
        final Iterator<AccessibilityServiceInfo> iterator = installedAccessibilityServiceList.iterator();
        while (iterator.hasNext()) {
            final ResolveInfo resolveInfo = iterator.next().getResolveInfo();
            set.add(new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name));
        }
        return set;
    }
    
    public static String getShortcutTargetServiceComponentNameString(final Context context, final int n) {
        final String stringForUser = Settings.Secure.getStringForUser(context.getContentResolver(), "accessibility_shortcut_target_service", n);
        if (stringForUser != null) {
            return stringForUser;
        }
        return context.getString(17039649);
    }
    
    public static CharSequence getTextForLocale(final Context context, final Locale locale, final int n) {
        final Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration).getText(n);
    }
    
    public static boolean hasServiceCrashed(final String s, final String s2, final List<AccessibilityServiceInfo> list) {
        for (int i = 0; i < list.size(); ++i) {
            final AccessibilityServiceInfo accessibilityServiceInfo = list.get(i);
            final ServiceInfo serviceInfo = list.get(i).getResolveInfo().serviceInfo;
            if (TextUtils.equals((CharSequence)serviceInfo.packageName, (CharSequence)s) && TextUtils.equals((CharSequence)serviceInfo.name, (CharSequence)s2)) {
                return accessibilityServiceInfo.crashed;
            }
        }
        return false;
    }
    
    public static boolean isShortcutEnabled(final Context context, final int n) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = true;
        if (Settings.Secure.getIntForUser(contentResolver, "accessibility_shortcut_enabled", 1, n) != 1) {
            b = false;
        }
        return b;
    }
    
    public static void setAccessibilityServiceState(final Context context, final ComponentName componentName, final boolean b) {
        setAccessibilityServiceState(context, componentName, b, UserHandle.myUserId());
    }
    
    public static void setAccessibilityServiceState(final Context context, final ComponentName componentName, final boolean b, final int n) {
        Object enabledServicesFromSettings;
        if (((Set)(enabledServicesFromSettings = getEnabledServicesFromSettings(context, n))).isEmpty()) {
            enabledServicesFromSettings = new ArraySet(1);
        }
        if (b) {
            ((Set<ComponentName>)enabledServicesFromSettings).add(componentName);
        }
        else {
            ((Set)enabledServicesFromSettings).remove(componentName);
            final Set<ComponentName> installedServices = getInstalledServices(context);
            final Iterator<ComponentName> iterator = ((Set<ComponentName>)enabledServicesFromSettings).iterator();
            while (iterator.hasNext()) {
                if (installedServices.contains(iterator.next())) {
                    break;
                }
            }
        }
        final StringBuilder sb = new StringBuilder();
        final Iterator<ComponentName> iterator2 = ((Set<ComponentName>)enabledServicesFromSettings).iterator();
        while (iterator2.hasNext()) {
            sb.append(iterator2.next().flattenToString());
            sb.append(':');
        }
        final int length = sb.length();
        if (length > 0) {
            sb.deleteCharAt(length - 1);
        }
        Settings.Secure.putStringForUser(context.getContentResolver(), "enabled_accessibility_services", sb.toString(), n);
    }
}
