package com.android.settingslib.drawer;

import android.app.Activity;
import android.widget.ListAdapter;
import java.util.List;
import com.android.settingslib.R;
import android.app.AlertDialog$Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import java.util.ArrayList;
import android.os.UserHandle;
import android.os.UserManager;
import android.content.Context;
import android.os.Parcelable;
import android.os.Bundle;
import android.app.FragmentManager;
import android.util.Log;
import android.content.DialogInterface$OnClickListener;
import android.app.DialogFragment;

public class ProfileSelectDialog extends DialogFragment implements DialogInterface$OnClickListener
{
    private static final boolean DEBUG;
    private Tile mSelectedTile;
    
    static {
        DEBUG = Log.isLoggable("ProfileSelectDialog", 3);
    }
    
    public static void show(final FragmentManager fragmentManager, final Tile tile) {
        final ProfileSelectDialog profileSelectDialog = new ProfileSelectDialog();
        final Bundle arguments = new Bundle();
        arguments.putParcelable("selectedTile", (Parcelable)tile);
        profileSelectDialog.setArguments(arguments);
        profileSelectDialog.show(fragmentManager, "select_profile");
    }
    
    public static void updateUserHandlesIfNeeded(final Context context, final Tile tile) {
        final ArrayList<UserHandle> userHandle = tile.userHandle;
        if (tile.userHandle != null && tile.userHandle.size() > 1) {
            final UserManager value = UserManager.get(context);
            for (int i = userHandle.size() - 1; i >= 0; --i) {
                if (value.getUserInfo(((UserHandle)userHandle.get(i)).getIdentifier()) == null) {
                    if (ProfileSelectDialog.DEBUG) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Delete the user: ");
                        sb.append(userHandle.get(i).getIdentifier());
                        Log.d("ProfileSelectDialog", sb.toString());
                    }
                    userHandle.remove(i);
                }
            }
        }
    }
    
    public void onClick(final DialogInterface dialogInterface, final int n) {
        final UserHandle userHandle = this.mSelectedTile.userHandle.get(n);
        this.mSelectedTile.intent.addFlags(32768);
        this.getActivity().startActivityAsUser(this.mSelectedTile.intent, userHandle);
    }
    
    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        this.mSelectedTile = (Tile)this.getArguments().getParcelable("selectedTile");
    }
    
    public Dialog onCreateDialog(final Bundle bundle) {
        final Activity activity = this.getActivity();
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)activity);
        alertDialog$Builder.setTitle(R.string.choose_profile).setAdapter((ListAdapter)UserAdapter.createUserAdapter(UserManager.get((Context)activity), (Context)activity, this.mSelectedTile.userHandle), (DialogInterface$OnClickListener)this);
        return (Dialog)alertDialog$Builder.create();
    }
}
