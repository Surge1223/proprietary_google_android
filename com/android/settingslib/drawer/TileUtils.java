package com.android.settingslib.drawer;

import android.content.res.Resources;
import android.graphics.drawable.Icon;
import android.widget.RemoteViews;
import android.content.res.Resources$NotFoundException;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.ActivityInfo;
import android.content.ContentResolver;
import java.util.Collections;
import java.util.Collection;
import android.util.Log;
import java.util.HashMap;
import android.app.ActivityManager;
import android.os.UserHandle;
import android.os.UserManager;
import java.util.ArrayList;
import android.provider.Settings;
import android.util.Pair;
import android.os.RemoteException;
import android.net.Uri;
import android.text.TextUtils;
import android.os.Bundle;
import android.content.IContentProvider;
import java.util.Map;
import java.util.Iterator;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.Intent;
import android.content.Context;
import java.util.Comparator;

public class TileUtils
{
    private static final Comparator<DashboardCategory> CATEGORY_COMPARATOR;
    
    static {
        CATEGORY_COMPARATOR = new Comparator<DashboardCategory>() {
            @Override
            public int compare(final DashboardCategory dashboardCategory, final DashboardCategory dashboardCategory2) {
                return dashboardCategory2.priority - dashboardCategory.priority;
            }
        };
    }
    
    private static DashboardCategory createCategory(final Context context, final String key, final boolean b) {
        final DashboardCategory dashboardCategory = new DashboardCategory();
        dashboardCategory.key = key;
        if (!b) {
            return dashboardCategory;
        }
        final PackageManager packageManager = context.getPackageManager();
        final List queryIntentActivities = packageManager.queryIntentActivities(new Intent(key), 0);
        if (queryIntentActivities.size() == 0) {
            return null;
        }
        for (final ResolveInfo resolveInfo : queryIntentActivities) {
            if (!resolveInfo.system) {
                continue;
            }
            dashboardCategory.title = resolveInfo.activityInfo.loadLabel(packageManager);
            int priority;
            if ("com.android.settings".equals(resolveInfo.activityInfo.applicationInfo.packageName)) {
                priority = resolveInfo.priority;
            }
            else {
                priority = 0;
            }
            dashboardCategory.priority = priority;
        }
        return dashboardCategory;
    }
    
    private static Bundle getBundleFromUri(final Context context, final String s, final Map<String, IContentProvider> map) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return null;
        }
        final Uri parse = Uri.parse(s);
        final String methodFromUri = getMethodFromUri(parse);
        if (TextUtils.isEmpty((CharSequence)methodFromUri)) {
            return null;
        }
        final IContentProvider providerFromUri = getProviderFromUri(context, parse, map);
        if (providerFromUri == null) {
            return null;
        }
        try {
            return providerFromUri.call(context.getPackageName(), methodFromUri, s, (Bundle)null);
        }
        catch (RemoteException ex) {
            return null;
        }
    }
    
    public static List<DashboardCategory> getCategories(final Context context, final Map<Pair<String, String>, Tile> map, final boolean b, final String s, final String s2) {
        System.currentTimeMillis();
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b2 = false;
        if (Settings.Global.getInt(contentResolver, "device_provisioned", 0) != 0) {
            b2 = true;
        }
        final ArrayList<Tile> list = new ArrayList<Tile>();
        for (final UserHandle userHandle : ((UserManager)context.getSystemService("user")).getUserProfiles()) {
            if (userHandle.getIdentifier() == ActivityManager.getCurrentUser()) {
                final UserHandle userHandle2 = userHandle;
                getTilesForAction(context, userHandle, "com.android.settings.action.SETTINGS", map, null, list, true, s2);
                getTilesForAction(context, userHandle2, "com.android.settings.OPERATOR_APPLICATION_SETTING", map, "com.android.settings.category.wireless", list, false, true, s2);
                getTilesForAction(context, userHandle2, "com.android.settings.MANUFACTURER_APPLICATION_SETTING", map, "com.android.settings.category.device", list, false, true, s2);
            }
            if (b2) {
                getTilesForAction(context, userHandle, "com.android.settings.action.EXTRA_SETTINGS", map, null, list, false, s2);
                if (b) {
                    continue;
                }
                getTilesForAction(context, userHandle, "com.android.settings.action.IA_SETTINGS", map, null, list, false, s2);
                if (s == null) {
                    continue;
                }
                getTilesForAction(context, userHandle, s, map, null, list, false, s2);
            }
        }
        final HashMap<String, DashboardCategory> hashMap = new HashMap<String, DashboardCategory>();
        for (final Tile tile : list) {
            DashboardCategory category;
            if ((category = hashMap.get(tile.category)) == null) {
                category = createCategory(context, tile.category, b);
                if (category == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Couldn't find category ");
                    sb.append(tile.category);
                    Log.w("TileUtils", sb.toString());
                    continue;
                }
                hashMap.put(category.key, category);
            }
            category.addTile(tile);
        }
        final ArrayList list2 = new ArrayList<Object>(hashMap.values());
        final Iterator<DashboardCategory> iterator3 = list2.iterator();
        while (iterator3.hasNext()) {
            iterator3.next().sortTiles();
        }
        Collections.sort((List<Object>)list2, (Comparator<? super Object>)TileUtils.CATEGORY_COMPARATOR);
        return (List<DashboardCategory>)list2;
    }
    
    public static Pair<String, Integer> getIconFromUri(final Context context, final String s, final String s2, final Map<String, IContentProvider> map) {
        final Bundle bundleFromUri = getBundleFromUri(context, s2, map);
        if (bundleFromUri == null) {
            return null;
        }
        final String string = bundleFromUri.getString("com.android.settings.icon_package");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return null;
        }
        if (bundleFromUri.getInt("com.android.settings.icon", 0) == 0) {
            return null;
        }
        if (!string.equals(s) && !string.equals(context.getPackageName())) {
            return null;
        }
        return (Pair<String, Integer>)Pair.create((Object)string, (Object)bundleFromUri.getInt("com.android.settings.icon", 0));
    }
    
    static String getMethodFromUri(final Uri uri) {
        if (uri == null) {
            return null;
        }
        final List pathSegments = uri.getPathSegments();
        if (pathSegments != null && !pathSegments.isEmpty()) {
            return pathSegments.get(0);
        }
        return null;
    }
    
    private static IContentProvider getProviderFromUri(final Context context, final Uri uri, final Map<String, IContentProvider> map) {
        if (uri == null) {
            return null;
        }
        final String authority = uri.getAuthority();
        if (TextUtils.isEmpty((CharSequence)authority)) {
            return null;
        }
        if (!map.containsKey(authority)) {
            map.put(authority, context.getContentResolver().acquireUnstableProvider(uri));
        }
        return map.get(authority);
    }
    
    private static String getString(final Bundle bundle, final String s) {
        String string;
        if (bundle == null) {
            string = null;
        }
        else {
            string = bundle.getString(s);
        }
        return string;
    }
    
    public static String getTextFromUri(final Context context, final String s, final Map<String, IContentProvider> map, final String s2) {
        final Bundle bundleFromUri = getBundleFromUri(context, s, map);
        String string;
        if (bundleFromUri != null) {
            string = bundleFromUri.getString(s2);
        }
        else {
            string = null;
        }
        return string;
    }
    
    private static void getTilesForAction(final Context context, final UserHandle userHandle, final String s, final Map<Pair<String, String>, Tile> map, final String s2, final ArrayList<Tile> list, final boolean b, final String s3) {
        getTilesForAction(context, userHandle, s, map, s2, list, b, b, s3);
    }
    
    private static void getTilesForAction(final Context context, final UserHandle userHandle, final String s, final Map<Pair<String, String>, Tile> map, final String s2, final ArrayList<Tile> list, final boolean b, final boolean b2, final String package1) {
        final Intent intent = new Intent(s);
        if (b) {
            intent.setPackage(package1);
        }
        getTilesForIntent(context, userHandle, intent, map, s2, list, b2, true, true);
    }
    
    public static void getTilesForIntent(final Context context, final UserHandle userHandle, final Intent intent, final Map<Pair<String, String>, Tile> map, final String s, final List<Tile> list, final boolean b, final boolean b2, final boolean b3) {
        getTilesForIntent(context, userHandle, intent, map, s, list, b, b2, b3, false);
    }
    
    public static void getTilesForIntent(final Context context, final UserHandle userHandle, final Intent intent, final Map<Pair<String, String>, Tile> map, final String s, final List<Tile> list, final boolean b, final boolean b2, final boolean b3, final boolean b4) {
        final PackageManager packageManager = context.getPackageManager();
        final List queryIntentActivitiesAsUser = packageManager.queryIntentActivitiesAsUser(intent, 128, userHandle.getIdentifier());
        final HashMap<String, IContentProvider> hashMap = new HashMap<String, IContentProvider>();
        for (final ResolveInfo resolveInfo : queryIntentActivitiesAsUser) {
            if (!resolveInfo.system) {
                continue;
            }
            final ActivityInfo activityInfo = resolveInfo.activityInfo;
            final Bundle metaData = activityInfo.metaData;
            if (b2 && (metaData == null || !metaData.containsKey("com.android.settings.category")) && s == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Found ");
                sb.append(resolveInfo.activityInfo.name);
                sb.append(" for intent ");
                sb.append(intent);
                sb.append(" missing metadata ");
                String s2;
                if (metaData == null) {
                    s2 = "";
                }
                else {
                    s2 = "com.android.settings.category";
                }
                sb.append(s2);
                Log.w("TileUtils", sb.toString());
            }
            else {
                final String string = metaData.getString("com.android.settings.category");
                final Pair pair = new Pair((Object)activityInfo.packageName, (Object)activityInfo.name);
                Tile tile = map.get(pair);
                if (tile == null) {
                    tile = new Tile();
                    tile.intent = new Intent().setClassName(activityInfo.packageName, activityInfo.name);
                    tile.category = string;
                    int priority;
                    if (b) {
                        priority = resolveInfo.priority;
                    }
                    else {
                        priority = 0;
                    }
                    tile.priority = priority;
                    tile.metaData = activityInfo.metaData;
                    updateTileData(context, tile, activityInfo, activityInfo.applicationInfo, packageManager, hashMap, b3);
                    map.put((Pair<String, String>)pair, tile);
                }
                else if (b4) {
                    updateSummaryAndTitle(context, hashMap, tile);
                }
                if (!tile.userHandle.contains(userHandle)) {
                    tile.userHandle.add(userHandle);
                }
                if (list.contains(tile)) {
                    continue;
                }
                list.add(tile);
            }
        }
    }
    
    private static void updateSummaryAndTitle(final Context context, final Map<String, IContentProvider> map, final Tile tile) {
        if (tile != null && tile.metaData != null && tile.metaData.containsKey("com.android.settings.summary_uri")) {
            final Bundle bundleFromUri = getBundleFromUri(context, tile.metaData.getString("com.android.settings.summary_uri"), map);
            final String string = getString(bundleFromUri, "com.android.settings.summary");
            final String string2 = getString(bundleFromUri, "com.android.settings.title");
            if (string != null) {
                tile.remoteViews.setTextViewText(16908304, (CharSequence)string);
            }
            if (string2 != null) {
                tile.remoteViews.setTextViewText(16908310, (CharSequence)string2);
            }
        }
    }
    
    private static boolean updateTileData(final Context context, final Tile tile, final ActivityInfo activityInfo, final ApplicationInfo applicationInfo, final PackageManager packageManager, final Map<String, IContentProvider> map, boolean boolean1) {
        final boolean systemApp = applicationInfo.isSystemApp();
        final boolean b = false;
        final boolean b2 = false;
        if (systemApp) {
            final boolean b3 = false;
            int int1 = 0;
            final int n = 0;
            final int n2 = 0;
            int int2 = 0;
            final int n3 = 0;
            final String s = null;
            final String s2 = null;
            final String s3 = null;
            final String s4 = null;
            String s5 = null;
            final String s6 = null;
            final String s7 = null;
            String summary = null;
            final String s8 = null;
            final String s9 = null;
            String s10 = null;
            final String s11 = null;
            final String s12 = null;
            final String s13 = null;
            Object remoteViews = null;
            final String s14 = null;
            Object key = null;
            Label_0902: {
                try {
                    final Resources resourcesForApplication = packageManager.getResourcesForApplication(applicationInfo.packageName);
                    final Bundle metaData = activityInfo.metaData;
                    boolean b4 = b;
                    boolean b5 = b3;
                    while (true) {
                        Label_0184: {
                            if (!boolean1) {
                                break Label_0184;
                            }
                            int2 = n3;
                            final String s15 = s6;
                            final String s16 = s11;
                            try {
                                final boolean equals = context.getPackageName().equals(applicationInfo.packageName);
                                b4 = b;
                                b5 = b3;
                                if (!equals) {
                                    b4 = true;
                                    b5 = true;
                                }
                                break Label_0184;
                            }
                            catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex) {
                                boolean1 = b2;
                                summary = s16;
                                s5 = s15;
                                int1 = int2;
                            }
                            key = remoteViews;
                            break Label_0902;
                        }
                        if (resourcesForApplication != null && metaData != null) {
                            int2 = n;
                            Label_0848: {
                                try {
                                    if (metaData.containsKey("com.android.settings.icon")) {
                                        int2 = n3;
                                        int1 = metaData.getInt("com.android.settings.icon");
                                    }
                                    int2 = int1;
                                    Label_0340: {
                                        Label_0336: {
                                            if (metaData.containsKey("com.android.settings.icon_tintable")) {
                                                Label_0307: {
                                                    if (!b5) {
                                                        break Label_0307;
                                                    }
                                                    StringBuilder sb;
                                                    try {
                                                        final StringBuilder sb2;
                                                        sb = (sb2 = new StringBuilder());
                                                        final String s17 = "Ignoring icon tintable for ";
                                                        sb2.append(s17);
                                                        final StringBuilder sb3 = sb;
                                                        final ActivityInfo activityInfo2 = activityInfo;
                                                        sb3.append(activityInfo2);
                                                        final String s18 = "TileUtils";
                                                        final StringBuilder sb4 = sb;
                                                        final String s19 = sb4.toString();
                                                        Log.w(s18, s19);
                                                        break Label_0336;
                                                    }
                                                    catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex2) {
                                                        break Label_0848;
                                                    }
                                                    try {
                                                        final StringBuilder sb2 = sb;
                                                        final String s17 = "Ignoring icon tintable for ";
                                                        sb2.append(s17);
                                                        final StringBuilder sb3 = sb;
                                                        final ActivityInfo activityInfo2 = activityInfo;
                                                        sb3.append(activityInfo2);
                                                        final String s18 = "TileUtils";
                                                        final StringBuilder sb4 = sb;
                                                        final String s19 = sb4.toString();
                                                        Log.w(s18, s19);
                                                        break Label_0336;
                                                        boolean1 = metaData.getBoolean("com.android.settings.icon_tintable");
                                                        break Label_0340;
                                                    }
                                                    catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex3) {
                                                        boolean1 = b4;
                                                        s5 = s;
                                                        summary = s7;
                                                        continue;
                                                    }
                                                }
                                            }
                                        }
                                        boolean1 = b4;
                                    }
                                    String s20 = s3;
                                    s10 = s8;
                                    String s21 = s12;
                                    try {
                                        b4 = metaData.containsKey("com.android.settings.title");
                                        s5 = s2;
                                        if (b4) {
                                            int2 = int1;
                                            s21 = s11;
                                            if (metaData.get("com.android.settings.title") instanceof Integer) {
                                                int2 = int1;
                                                s21 = s11;
                                                s5 = resourcesForApplication.getString(metaData.getInt("com.android.settings.title"));
                                            }
                                            else {
                                                int2 = int1;
                                                s21 = s11;
                                                s5 = metaData.getString("com.android.settings.title");
                                            }
                                        }
                                        s20 = s5;
                                        s10 = s8;
                                        s21 = s12;
                                        b4 = metaData.containsKey("com.android.settings.summary");
                                        if (b4) {
                                            int2 = int1;
                                            s21 = s11;
                                            if (metaData.get("com.android.settings.summary") instanceof Integer) {
                                                int2 = int1;
                                                s21 = s11;
                                                summary = resourcesForApplication.getString(metaData.getInt("com.android.settings.summary"));
                                            }
                                            else {
                                                int2 = int1;
                                                s21 = s11;
                                                summary = metaData.getString("com.android.settings.summary");
                                            }
                                        }
                                        s20 = s5;
                                        s10 = summary;
                                        s21 = s12;
                                        b4 = metaData.containsKey("com.android.settings.keyhint");
                                        key = s14;
                                        if (b4) {
                                            int2 = int1;
                                            s21 = summary;
                                            if (metaData.get("com.android.settings.keyhint") instanceof Integer) {
                                                int2 = int1;
                                                s21 = summary;
                                                s10 = (String)(key = resourcesForApplication.getString(metaData.getInt("com.android.settings.keyhint")));
                                            }
                                            else {
                                                int2 = int1;
                                                s21 = summary;
                                                s10 = (String)(key = metaData.getString("com.android.settings.keyhint"));
                                            }
                                        }
                                        s20 = s5;
                                        s10 = summary;
                                        s21 = (String)key;
                                        if (metaData.containsKey("com.android.settings.custom_view")) {
                                            s20 = s5;
                                            s10 = summary;
                                            s21 = (String)key;
                                            int2 = metaData.getInt("com.android.settings.custom_view");
                                            s20 = s5;
                                            s10 = summary;
                                            s21 = (String)key;
                                            s20 = s5;
                                            s10 = summary;
                                            s21 = (String)key;
                                            remoteViews = new RemoteViews(applicationInfo.packageName, int2);
                                            s20 = s5;
                                            s10 = summary;
                                            s21 = (String)key;
                                            tile.remoteViews = (RemoteViews)remoteViews;
                                            try {
                                                updateSummaryAndTitle(context, map, tile);
                                            }
                                            catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex4) {
                                                break Label_0902;
                                            }
                                        }
                                    }
                                    catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex5) {
                                        s5 = s20;
                                        summary = s10;
                                        key = s21;
                                    }
                                }
                                catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex6) {
                                    int1 = int2;
                                }
                            }
                            boolean1 = b4;
                            summary = s10;
                            key = remoteViews;
                        }
                        else {
                            boolean1 = b4;
                            key = s13;
                            summary = s9;
                            s5 = s4;
                            int1 = n2;
                        }
                        break;
                    }
                }
                catch (PackageManager$NameNotFoundException | Resources$NotFoundException ex7) {
                    boolean1 = false;
                    key = remoteViews;
                    summary = s10;
                    int1 = int2;
                }
            }
            String string = s5;
            if (TextUtils.isEmpty((CharSequence)s5)) {
                string = activityInfo.loadLabel(packageManager).toString();
            }
            int icon;
            if ((icon = int1) == 0) {
                icon = int1;
                if (!tile.metaData.containsKey("com.android.settings.icon_uri")) {
                    icon = activityInfo.icon;
                }
            }
            if (icon != 0) {
                tile.icon = Icon.createWithResource(activityInfo.packageName, icon);
            }
            tile.title = string;
            tile.summary = summary;
            tile.intent = new Intent().setClassName(activityInfo.packageName, activityInfo.name);
            tile.key = (String)key;
            tile.isIconTintable = boolean1;
            return true;
        }
        return false;
    }
}
