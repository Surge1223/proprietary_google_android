package com.android.settingslib.drawer;

import com.android.settingslib.drawable.UserIconDrawable;
import android.content.pm.UserInfo;
import com.android.internal.util.UserIcons;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.database.DataSetObserver;
import android.widget.TextView;
import android.widget.ImageView;
import android.app.ActivityManager;
import android.os.UserHandle;
import java.util.List;
import android.os.UserManager;
import com.android.settingslib.R;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.view.LayoutInflater;
import java.util.ArrayList;
import android.widget.SpinnerAdapter;
import android.widget.ListAdapter;

public class UserAdapter implements ListAdapter, SpinnerAdapter
{
    private ArrayList<UserDetails> data;
    private final LayoutInflater mInflater;
    
    public UserAdapter(final Context context, final ArrayList<UserDetails> data) {
        if (data != null) {
            this.data = data;
            this.mInflater = (LayoutInflater)context.getSystemService("layout_inflater");
            return;
        }
        throw new IllegalArgumentException("A list of user details must be provided");
    }
    
    private View createUser(final ViewGroup viewGroup) {
        return this.mInflater.inflate(R.layout.user_preference, viewGroup, false);
    }
    
    public static UserAdapter createUserAdapter(final UserManager userManager, final Context context, final List<UserHandle> list) {
        final ArrayList list2 = new ArrayList(list.size());
        for (int size = list.size(), i = 0; i < size; ++i) {
            list2.add(new UserDetails(list.get(i), userManager, context));
        }
        return new UserAdapter(context, list2);
    }
    
    public static UserAdapter createUserSpinnerAdapter(final UserManager userManager, final Context context) {
        final List userProfiles = userManager.getUserProfiles();
        if (userProfiles.size() < 2) {
            return null;
        }
        final UserHandle userHandle = new UserHandle(UserHandle.myUserId());
        userProfiles.remove(userHandle);
        userProfiles.add(0, userHandle);
        return createUserAdapter(userManager, context, userProfiles);
    }
    
    private int getTitle(final UserDetails userDetails) {
        final int identifier = userDetails.mUserHandle.getIdentifier();
        if (identifier != -2 && identifier != ActivityManager.getCurrentUser()) {
            return R.string.category_work;
        }
        return R.string.category_personal;
    }
    
    public boolean areAllItemsEnabled() {
        return true;
    }
    
    public int getCount() {
        return this.data.size();
    }
    
    public View getDropDownView(final int n, View user, final ViewGroup viewGroup) {
        if (user == null) {
            user = this.createUser(viewGroup);
        }
        final UserDetails userDetails = this.data.get(n);
        ((ImageView)user.findViewById(16908294)).setImageDrawable(userDetails.mIcon);
        ((TextView)user.findViewById(16908310)).setText(this.getTitle(userDetails));
        return user;
    }
    
    public UserDetails getItem(final int n) {
        return this.data.get(n);
    }
    
    public long getItemId(final int n) {
        return this.data.get(n).mUserHandle.getIdentifier();
    }
    
    public int getItemViewType(final int n) {
        return 0;
    }
    
    public UserHandle getUserHandle(final int n) {
        if (n >= 0 && n < this.data.size()) {
            return this.data.get(n).mUserHandle;
        }
        return null;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        return this.getDropDownView(n, view, viewGroup);
    }
    
    public int getViewTypeCount() {
        return 1;
    }
    
    public boolean hasStableIds() {
        return false;
    }
    
    public boolean isEmpty() {
        return this.data.isEmpty();
    }
    
    public boolean isEnabled(final int n) {
        return true;
    }
    
    public void registerDataSetObserver(final DataSetObserver dataSetObserver) {
    }
    
    public void unregisterDataSetObserver(final DataSetObserver dataSetObserver) {
    }
    
    public static class UserDetails
    {
        private final Drawable mIcon;
        private final String mName;
        private final UserHandle mUserHandle;
        
        public UserDetails(final UserHandle mUserHandle, final UserManager userManager, final Context context) {
            this.mUserHandle = mUserHandle;
            final UserInfo userInfo = userManager.getUserInfo(this.mUserHandle.getIdentifier());
            Object o;
            if (userInfo.isManagedProfile()) {
                this.mName = context.getString(R.string.managed_user_title);
                o = context.getDrawable(17302325);
            }
            else {
                this.mName = userInfo.name;
                final int id = userInfo.id;
                if (userManager.getUserIcon(id) != null) {
                    o = new BitmapDrawable(context.getResources(), userManager.getUserIcon(id));
                }
                else {
                    o = UserIcons.getDefaultUserIcon(context.getResources(), id, false);
                }
            }
            this.mIcon = encircle(context, (Drawable)o);
        }
        
        private static Drawable encircle(final Context context, final Drawable iconDrawable) {
            return new UserIconDrawable(UserIconDrawable.getSizeForList(context)).setIconDrawable(iconDrawable).bake();
        }
    }
}
