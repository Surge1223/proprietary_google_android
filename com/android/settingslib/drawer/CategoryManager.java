package com.android.settingslib.drawer;

import android.util.Log;
import java.util.Set;
import android.content.ComponentName;
import android.util.ArraySet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import android.util.ArrayMap;
import android.content.Context;
import android.util.Pair;
import com.android.settingslib.applications.InterestingConfigChanges;
import java.util.Map;
import java.util.List;

public class CategoryManager
{
    private static CategoryManager sInstance;
    private List<DashboardCategory> mCategories;
    private final Map<String, DashboardCategory> mCategoryByKeyMap;
    private String mExtraAction;
    private final InterestingConfigChanges mInterestingConfigChanges;
    private final Map<Pair<String, String>, Tile> mTileByComponentCache;
    
    CategoryManager(final Context context, final String mExtraAction) {
        this.mTileByComponentCache = (Map<Pair<String, String>, Tile>)new ArrayMap();
        this.mCategoryByKeyMap = (Map<String, DashboardCategory>)new ArrayMap();
        (this.mInterestingConfigChanges = new InterestingConfigChanges()).applyNewConfig(context.getResources());
        this.mExtraAction = mExtraAction;
    }
    
    public static CategoryManager get(final Context context) {
        return get(context, null);
    }
    
    public static CategoryManager get(final Context context, final String s) {
        if (CategoryManager.sInstance == null) {
            CategoryManager.sInstance = new CategoryManager(context, s);
        }
        return CategoryManager.sInstance;
    }
    
    private void tryInitCategories(final Context context, final String s) {
        synchronized (this) {
            this.tryInitCategories(context, false, s);
        }
    }
    
    private void tryInitCategories(final Context context, final boolean b, final String s) {
        synchronized (this) {
            if (this.mCategories == null) {
                if (b) {
                    this.mTileByComponentCache.clear();
                }
                this.mCategoryByKeyMap.clear();
                this.mCategories = TileUtils.getCategories(context, this.mTileByComponentCache, false, this.mExtraAction, s);
                for (final DashboardCategory dashboardCategory : this.mCategories) {
                    this.mCategoryByKeyMap.put(dashboardCategory.key, dashboardCategory);
                }
                this.backwardCompatCleanupForCategory(this.mTileByComponentCache, this.mCategoryByKeyMap);
                this.sortCategories(context, this.mCategoryByKeyMap);
                this.filterDuplicateTiles(this.mCategoryByKeyMap);
            }
        }
    }
    
    void backwardCompatCleanupForCategory(final Map<Pair<String, String>, Tile> map, final Map<String, DashboardCategory> map2) {
        synchronized (this) {
            final HashMap<Object, List<Tile>> hashMap = new HashMap<Object, List<Tile>>();
            for (final Map.Entry<Pair<String, String>, Tile> entry : map.entrySet()) {
                final String s = (String)entry.getKey().first;
                List<Tile> list;
                if ((list = hashMap.get(s)) == null) {
                    list = new ArrayList<Tile>();
                    hashMap.put(s, list);
                }
                list.add(entry.getValue());
            }
            final Iterator<Map.Entry<String, List<Tile>>> iterator2 = hashMap.entrySet().iterator();
            while (iterator2.hasNext()) {
                final List<Tile> list2 = iterator2.next().getValue();
                final boolean b = false;
                boolean b2 = false;
                final Iterator<Tile> iterator3 = list2.iterator();
                boolean b3;
                while (true) {
                    b3 = b;
                    if (!iterator3.hasNext()) {
                        break;
                    }
                    if (!CategoryKey.KEY_COMPAT_MAP.containsKey(iterator3.next().category)) {
                        b3 = true;
                        break;
                    }
                    b2 = true;
                }
                if (b2 && !b3) {
                    for (final Tile tile : list2) {
                        final String category = CategoryKey.KEY_COMPAT_MAP.get(tile.category);
                        tile.category = category;
                        DashboardCategory dashboardCategory;
                        if ((dashboardCategory = map2.get(category)) == null) {
                            dashboardCategory = new DashboardCategory();
                            map2.put(category, dashboardCategory);
                        }
                        dashboardCategory.addTile(tile);
                    }
                }
            }
        }
    }
    
    void filterDuplicateTiles(final Map<String, DashboardCategory> map) {
        synchronized (this) {
            final Iterator<Map.Entry<String, DashboardCategory>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                final DashboardCategory dashboardCategory = iterator.next().getValue();
                int i = dashboardCategory.getTilesCount();
                final ArraySet set = new ArraySet();
                --i;
                while (i >= 0) {
                    final Tile tile = dashboardCategory.getTile(i);
                    if (tile.intent != null) {
                        final ComponentName component = tile.intent.getComponent();
                        if (((Set)set).contains(component)) {
                            dashboardCategory.removeTile(i);
                        }
                        else {
                            ((Set<ComponentName>)set).add(component);
                        }
                    }
                    --i;
                }
            }
        }
    }
    
    public List<DashboardCategory> getCategories(final Context context) {
        synchronized (this) {
            return this.getCategories(context, "com.android.settings");
        }
    }
    
    public List<DashboardCategory> getCategories(final Context context, final String s) {
        synchronized (this) {
            this.tryInitCategories(context, s);
            return this.mCategories;
        }
    }
    
    public DashboardCategory getTilesByCategory(final Context context, final String s) {
        synchronized (this) {
            return this.getTilesByCategory(context, s, "com.android.settings");
        }
    }
    
    public DashboardCategory getTilesByCategory(final Context context, final String s, final String s2) {
        synchronized (this) {
            this.tryInitCategories(context, s2);
            return this.mCategoryByKeyMap.get(s);
        }
    }
    
    public void reloadAllCategories(final Context context, final String s) {
        synchronized (this) {
            final boolean applyNewConfig = this.mInterestingConfigChanges.applyNewConfig(context.getResources());
            this.mCategories = null;
            this.tryInitCategories(context, applyNewConfig, s);
        }
    }
    
    void sortCategories(final Context context, final Map<String, DashboardCategory> map) {
        synchronized (this) {
            final Iterator<Map.Entry<String, DashboardCategory>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                iterator.next().getValue().sortTiles(context.getPackageName());
            }
        }
    }
    
    public void updateCategoryFromBlacklist(final Set<ComponentName> set) {
        synchronized (this) {
            if (this.mCategories == null) {
                Log.w("CategoryManager", "Category is null, skipping blacklist update");
            }
            for (int i = 0; i < this.mCategories.size(); ++i) {
                final DashboardCategory dashboardCategory = this.mCategories.get(i);
                int n;
                for (int j = 0; j < dashboardCategory.getTilesCount(); j = n + 1) {
                    n = j;
                    if (set.contains(dashboardCategory.getTile(j).intent.getComponent())) {
                        dashboardCategory.removeTile(j);
                        n = j - 1;
                    }
                }
            }
        }
    }
}
