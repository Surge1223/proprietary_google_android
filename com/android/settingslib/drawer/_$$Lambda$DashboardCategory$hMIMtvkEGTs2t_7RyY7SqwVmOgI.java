package com.android.settingslib.drawer;

import java.util.Collections;
import java.util.Iterator;
import android.text.TextUtils;
import java.util.ArrayList;
import android.os.Parcel;
import android.util.Log;
import java.util.List;
import android.os.Parcelable.Creator;
import android.os.Parcelable;
import java.util.Comparator;

public final class _$$Lambda$DashboardCategory$hMIMtvkEGTs2t_7RyY7SqwVmOgI implements Comparator
{
    @Override
    public final int compare(final Object o, final Object o2) {
        return DashboardCategory.lambda$sortTiles$0(this.f$0, (Tile)o, (Tile)o2);
    }
}
