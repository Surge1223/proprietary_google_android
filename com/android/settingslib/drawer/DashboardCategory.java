package com.android.settingslib.drawer;

import java.util.Collections;
import java.util.Iterator;
import android.text.TextUtils;
import java.util.ArrayList;
import android.os.Parcel;
import android.util.Log;
import java.util.List;
import java.util.Comparator;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class DashboardCategory implements Parcelable
{
    public static final Parcelable.Creator<DashboardCategory> CREATOR;
    private static final boolean DEBUG;
    public static final Comparator<Tile> TILE_COMPARATOR;
    public String key;
    private List<Tile> mTiles;
    public int priority;
    public CharSequence title;
    
    static {
        DEBUG = Log.isLoggable("DashboardCategory", 3);
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<DashboardCategory>() {
            public DashboardCategory createFromParcel(final Parcel parcel) {
                return new DashboardCategory(parcel);
            }
            
            public DashboardCategory[] newArray(final int n) {
                return new DashboardCategory[n];
            }
        };
        TILE_COMPARATOR = new Comparator<Tile>() {
            @Override
            public int compare(final Tile tile, final Tile tile2) {
                return tile2.priority - tile.priority;
            }
        };
    }
    
    public DashboardCategory() {
        this.mTiles = new ArrayList<Tile>();
    }
    
    DashboardCategory(final Parcel parcel) {
        this.mTiles = new ArrayList<Tile>();
        this.readFromParcel(parcel);
    }
    
    public void addTile(final Tile tile) {
        synchronized (this) {
            this.mTiles.add(tile);
        }
    }
    
    public int describeContents() {
        return 0;
    }
    
    public Tile getTile(final int n) {
        return this.mTiles.get(n);
    }
    
    public List<Tile> getTiles() {
        synchronized (this) {
            final ArrayList<Tile> list = new ArrayList<Tile>(this.mTiles.size());
            final Iterator<Tile> iterator = this.mTiles.iterator();
            while (iterator.hasNext()) {
                list.add(iterator.next());
            }
            return list;
        }
    }
    
    public int getTilesCount() {
        return this.mTiles.size();
    }
    
    public void readFromParcel(final Parcel parcel) {
        this.title = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.key = parcel.readString();
        this.priority = parcel.readInt();
        for (int int1 = parcel.readInt(), i = 0; i < int1; ++i) {
            this.mTiles.add((Tile)Tile.CREATOR.createFromParcel(parcel));
        }
    }
    
    public void removeTile(final int n) {
        synchronized (this) {
            this.mTiles.remove(n);
        }
    }
    
    public void sortTiles() {
        Collections.sort(this.mTiles, DashboardCategory.TILE_COMPARATOR);
    }
    
    public void sortTiles(final String s) {
        synchronized (this) {
            Collections.sort(this.mTiles, new _$$Lambda$DashboardCategory$hMIMtvkEGTs2t_7RyY7SqwVmOgI(s));
        }
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        TextUtils.writeToParcel(this.title, parcel, n);
        parcel.writeString(this.key);
        parcel.writeInt(this.priority);
        final int size = this.mTiles.size();
        parcel.writeInt(size);
        for (int i = 0; i < size; ++i) {
            this.mTiles.get(i).writeToParcel(parcel, n);
        }
    }
}
