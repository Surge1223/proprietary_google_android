package com.android.settingslib.drawer;

import android.text.TextUtils;
import android.os.Parcel;
import android.os.UserHandle;
import java.util.ArrayList;
import android.widget.RemoteViews;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.os.Parcelable.Creator;
import android.os.Parcelable;

public class Tile implements Parcelable
{
    public static final Parcelable.Creator<Tile> CREATOR;
    public String category;
    public Bundle extras;
    public Icon icon;
    public Intent intent;
    public boolean isIconTintable;
    public String key;
    public Bundle metaData;
    public int priority;
    public RemoteViews remoteViews;
    public CharSequence summary;
    public CharSequence title;
    public ArrayList<UserHandle> userHandle;
    
    static {
        CREATOR = (Parcelable.Creator)new Parcelable.Creator<Tile>() {
            public Tile createFromParcel(final Parcel parcel) {
                return new Tile(parcel);
            }
            
            public Tile[] newArray(final int n) {
                return new Tile[n];
            }
        };
    }
    
    public Tile() {
        this.userHandle = new ArrayList<UserHandle>();
    }
    
    Tile(final Parcel parcel) {
        this.userHandle = new ArrayList<UserHandle>();
        this.readFromParcel(parcel);
    }
    
    public int describeContents() {
        return 0;
    }
    
    public void readFromParcel(final Parcel parcel) {
        this.title = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.summary = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        if (parcel.readByte() != 0) {
            this.icon = (Icon)Icon.CREATOR.createFromParcel(parcel);
        }
        if (parcel.readByte() != 0) {
            this.intent = (Intent)Intent.CREATOR.createFromParcel(parcel);
        }
        for (int int1 = parcel.readInt(), i = 0; i < int1; ++i) {
            this.userHandle.add((UserHandle)UserHandle.CREATOR.createFromParcel(parcel));
        }
        this.extras = parcel.readBundle();
        this.category = parcel.readString();
        this.priority = parcel.readInt();
        this.metaData = parcel.readBundle();
        this.key = parcel.readString();
        this.remoteViews = (RemoteViews)parcel.readParcelable(RemoteViews.class.getClassLoader());
        this.isIconTintable = parcel.readBoolean();
    }
    
    public void writeToParcel(final Parcel parcel, final int n) {
        TextUtils.writeToParcel(this.title, parcel, n);
        TextUtils.writeToParcel(this.summary, parcel, n);
        final Icon icon = this.icon;
        int i = 0;
        if (icon != null) {
            parcel.writeByte((byte)1);
            this.icon.writeToParcel(parcel, n);
        }
        else {
            parcel.writeByte((byte)0);
        }
        if (this.intent != null) {
            parcel.writeByte((byte)1);
            this.intent.writeToParcel(parcel, n);
        }
        else {
            parcel.writeByte((byte)0);
        }
        final int size = this.userHandle.size();
        parcel.writeInt(size);
        while (i < size) {
            this.userHandle.get(i).writeToParcel(parcel, n);
            ++i;
        }
        parcel.writeBundle(this.extras);
        parcel.writeString(this.category);
        parcel.writeInt(this.priority);
        parcel.writeBundle(this.metaData);
        parcel.writeString(this.key);
        parcel.writeParcelable((Parcelable)this.remoteViews, n);
        parcel.writeBoolean(this.isIconTintable);
    }
}
