package com.android.settingslib.drawer;

import android.content.Intent;
import java.util.Set;
import android.os.AsyncTask;
import android.content.pm.PackageManager;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.content.res.TypedArray;
import android.widget.Toolbar;
import com.android.settingslib.R;
import android.R$styleable;
import android.os.Bundle;
import java.util.ArrayList;
import android.util.Log;
import android.widget.FrameLayout;
import java.util.List;
import android.content.ComponentName;
import android.util.ArraySet;
import android.app.Activity;

public class SettingsDrawerActivity extends Activity
{
    private static final boolean DEBUG;
    private static ArraySet<ComponentName> sTileBlacklist;
    private final List<CategoryListener> mCategoryListeners;
    private FrameLayout mContentHeaderContainer;
    private final PackageReceiver mPackageReceiver;
    
    static {
        DEBUG = Log.isLoggable("SettingsDrawerActivity", 3);
        SettingsDrawerActivity.sTileBlacklist = (ArraySet<ComponentName>)new ArraySet();
    }
    
    public SettingsDrawerActivity() {
        this.mPackageReceiver = new PackageReceiver();
        this.mCategoryListeners = new ArrayList<CategoryListener>();
    }
    
    private void onCategoriesChanged() {
        for (int size = this.mCategoryListeners.size(), i = 0; i < size; ++i) {
            this.mCategoryListeners.get(i).onCategoriesChanged();
        }
    }
    
    public void addCategoryListener(final CategoryListener categoryListener) {
        this.mCategoryListeners.add(categoryListener);
    }
    
    public String getSettingPkg() {
        return "com.android.settings";
    }
    
    protected void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        System.currentTimeMillis();
        final TypedArray obtainStyledAttributes = this.getTheme().obtainStyledAttributes(R$styleable.Theme);
        if (!obtainStyledAttributes.getBoolean(38, false)) {
            this.getWindow().addFlags(Integer.MIN_VALUE);
            this.requestWindowFeature(1);
        }
        super.setContentView(R.layout.settings_with_drawer);
        this.mContentHeaderContainer = (FrameLayout)this.findViewById(R.id.content_header_container);
        final Toolbar actionBar = (Toolbar)this.findViewById(R.id.action_bar);
        if (obtainStyledAttributes.getBoolean(38, false)) {
            actionBar.setVisibility(8);
            return;
        }
        this.setActionBar(actionBar);
    }
    
    public boolean onNavigateUp() {
        if (!super.onNavigateUp()) {
            this.finish();
        }
        return true;
    }
    
    protected void onPause() {
        this.unregisterReceiver((BroadcastReceiver)this.mPackageReceiver);
        super.onPause();
    }
    
    protected void onResume() {
        super.onResume();
        final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme("package");
        this.registerReceiver((BroadcastReceiver)this.mPackageReceiver, intentFilter);
        new CategoriesUpdateTask().execute((Object[])new Void[0]);
    }
    
    public void remCategoryListener(final CategoryListener categoryListener) {
        this.mCategoryListeners.remove(categoryListener);
    }
    
    public void setContentView(final int n) {
        final ViewGroup viewGroup = (ViewGroup)this.findViewById(R.id.content_frame);
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
        LayoutInflater.from((Context)this).inflate(n, viewGroup);
    }
    
    public void setContentView(final View view) {
        ((ViewGroup)this.findViewById(R.id.content_frame)).addView(view);
    }
    
    public void setContentView(final View view, final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        ((ViewGroup)this.findViewById(R.id.content_frame)).addView(view, viewGroup.LayoutParams);
    }
    
    public boolean setTileEnabled(final ComponentName componentName, final boolean b) {
        final PackageManager packageManager = this.getPackageManager();
        final int componentEnabledSetting = packageManager.getComponentEnabledSetting(componentName);
        if (componentEnabledSetting == 1 == b && componentEnabledSetting != 0) {
            return false;
        }
        if (b) {
            SettingsDrawerActivity.sTileBlacklist.remove((Object)componentName);
        }
        else {
            SettingsDrawerActivity.sTileBlacklist.add((Object)componentName);
        }
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 2;
        }
        packageManager.setComponentEnabledSetting(componentName, n, 1);
        return true;
    }
    
    public void updateCategories() {
        new CategoriesUpdateTask().execute((Object[])new Void[0]);
    }
    
    private class CategoriesUpdateTask extends AsyncTask<Void, Void, Void>
    {
        private final CategoryManager mCategoryManager;
        
        public CategoriesUpdateTask() {
            this.mCategoryManager = CategoryManager.get((Context)SettingsDrawerActivity.this);
        }
        
        protected Void doInBackground(final Void... array) {
            this.mCategoryManager.reloadAllCategories((Context)SettingsDrawerActivity.this, SettingsDrawerActivity.this.getSettingPkg());
            return null;
        }
        
        protected void onPostExecute(final Void void1) {
            this.mCategoryManager.updateCategoryFromBlacklist((Set<ComponentName>)SettingsDrawerActivity.sTileBlacklist);
            SettingsDrawerActivity.this.onCategoriesChanged();
        }
    }
    
    public interface CategoryListener
    {
        void onCategoriesChanged();
    }
    
    private class PackageReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            new CategoriesUpdateTask().execute((Object[])new Void[0]);
        }
    }
}
