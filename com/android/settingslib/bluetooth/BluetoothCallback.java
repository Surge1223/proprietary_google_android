package com.android.settingslib.bluetooth;

public interface BluetoothCallback
{
    void onActiveDeviceChanged(final CachedBluetoothDevice p0, final int p1);
    
    void onAudioModeChanged();
    
    void onBluetoothStateChanged(final int p0);
    
    void onConnectionStateChanged(final CachedBluetoothDevice p0, final int p1);
    
    void onDeviceAdded(final CachedBluetoothDevice p0);
    
    void onDeviceBondStateChanged(final CachedBluetoothDevice p0, final int p1);
    
    void onDeviceDeleted(final CachedBluetoothDevice p0);
    
    default void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
    }
    
    void onScanningStateChanged(final boolean p0);
}
