package com.android.settingslib.bluetooth;

import android.content.Intent;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothUuid;
import java.util.Iterator;
import android.os.ParcelUuid;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;
import java.util.Map;
import android.content.Context;

public class LocalBluetoothProfileManager
{
    private A2dpProfile mA2dpProfile;
    private A2dpSinkProfile mA2dpSinkProfile;
    private final Context mContext;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private final BluetoothEventManager mEventManager;
    private HeadsetProfile mHeadsetProfile;
    private HearingAidProfile mHearingAidProfile;
    private HfpClientProfile mHfpClientProfile;
    private HidDeviceProfile mHidDeviceProfile;
    private final HidProfile mHidProfile;
    private final LocalBluetoothAdapter mLocalAdapter;
    private MapClientProfile mMapClientProfile;
    private MapProfile mMapProfile;
    private OppProfile mOppProfile;
    private final PanProfile mPanProfile;
    private PbapClientProfile mPbapClientProfile;
    private final PbapServerProfile mPbapProfile;
    private final Map<String, LocalBluetoothProfile> mProfileNameMap;
    private final Collection<ServiceListener> mServiceListeners;
    private final boolean mUseMapClient;
    private final boolean mUsePbapPce;
    
    LocalBluetoothProfileManager(final Context mContext, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final BluetoothEventManager mEventManager) {
        this.mProfileNameMap = new HashMap<String, LocalBluetoothProfile>();
        this.mServiceListeners = new ArrayList<ServiceListener>();
        this.mContext = mContext;
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mEventManager = mEventManager;
        this.mUsePbapPce = this.mContext.getResources().getBoolean(17957104);
        this.mUseMapClient = this.mContext.getResources().getBoolean(17957104);
        this.mLocalAdapter.setProfileManager(this);
        this.mEventManager.setProfileManager(this);
        final ParcelUuid[] uuids = mLocalAdapter.getUuids();
        if (this.mLocalAdapter.getSupportedProfiles().contains(21)) {
            this.addProfile(this.mHearingAidProfile = new HearingAidProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "HearingAid", "android.bluetooth.hearingaid.profile.action.CONNECTION_STATE_CHANGED");
        }
        if (uuids != null) {
            this.updateLocalProfiles(uuids);
        }
        this.addProfile(this.mHidProfile = new HidProfile(mContext, this.mLocalAdapter, this.mDeviceManager, this), "HID", "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED");
        this.addPanProfile(this.mPanProfile = new PanProfile(mContext, this.mLocalAdapter), "PAN", "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED");
        this.addProfile(this.mHidDeviceProfile = new HidDeviceProfile(mContext, this.mLocalAdapter, this.mDeviceManager, this), "HID DEVICE", "android.bluetooth.hiddevice.profile.action.CONNECTION_STATE_CHANGED");
        Log.d("LocalBluetoothProfileManager", "Adding local MAP profile");
        if (this.mUseMapClient) {
            this.addProfile(this.mMapClientProfile = new MapClientProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "MAP Client", "android.bluetooth.mapmce.profile.action.CONNECTION_STATE_CHANGED");
        }
        else {
            this.addProfile(this.mMapProfile = new MapProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "MAP", "android.bluetooth.map.profile.action.CONNECTION_STATE_CHANGED");
        }
        Log.d("LocalBluetoothProfileManager", "Adding local PBAP profile");
        this.addProfile(this.mPbapProfile = new PbapServerProfile(mContext), "PBAP Server", "android.bluetooth.pbap.profile.action.CONNECTION_STATE_CHANGED");
        Log.d("LocalBluetoothProfileManager", "LocalBluetoothProfileManager construction complete");
    }
    
    private void addHeadsetProfile(final LocalBluetoothProfile localBluetoothProfile, final String s, final String s2, final String s3, final int n) {
        final HeadsetStateChangeHandler headsetStateChangeHandler = new HeadsetStateChangeHandler(localBluetoothProfile, s3, n);
        this.mEventManager.addProfileHandler(s2, (BluetoothEventManager.Handler)headsetStateChangeHandler);
        this.mEventManager.addProfileHandler(s3, (BluetoothEventManager.Handler)headsetStateChangeHandler);
        this.mProfileNameMap.put(s, localBluetoothProfile);
    }
    
    private void addPanProfile(final LocalBluetoothProfile localBluetoothProfile, final String s, final String s2) {
        this.mEventManager.addProfileHandler(s2, (BluetoothEventManager.Handler)new PanStateChangedHandler(localBluetoothProfile));
        this.mProfileNameMap.put(s, localBluetoothProfile);
    }
    
    private void addProfile(final LocalBluetoothProfile localBluetoothProfile, final String s, final String s2) {
        this.mEventManager.addProfileHandler(s2, (BluetoothEventManager.Handler)new StateChangedHandler(localBluetoothProfile));
        this.mProfileNameMap.put(s, localBluetoothProfile);
    }
    
    public void addServiceListener(final ServiceListener serviceListener) {
        this.mServiceListeners.add(serviceListener);
    }
    
    void callServiceConnectedListeners() {
        final Iterator<ServiceListener> iterator = this.mServiceListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onServiceConnected();
        }
    }
    
    void callServiceDisconnectedListeners() {
        final Iterator<ServiceListener> iterator = this.mServiceListeners.iterator();
        while (iterator.hasNext()) {
            iterator.next().onServiceDisconnected();
        }
    }
    
    public A2dpProfile getA2dpProfile() {
        return this.mA2dpProfile;
    }
    
    public A2dpSinkProfile getA2dpSinkProfile() {
        if (this.mA2dpSinkProfile != null && this.mA2dpSinkProfile.isProfileReady()) {
            return this.mA2dpSinkProfile;
        }
        return null;
    }
    
    public HeadsetProfile getHeadsetProfile() {
        return this.mHeadsetProfile;
    }
    
    public HearingAidProfile getHearingAidProfile() {
        return this.mHearingAidProfile;
    }
    
    HidDeviceProfile getHidDeviceProfile() {
        return this.mHidDeviceProfile;
    }
    
    HidProfile getHidProfile() {
        return this.mHidProfile;
    }
    
    public MapProfile getMapProfile() {
        return this.mMapProfile;
    }
    
    public PbapClientProfile getPbapClientProfile() {
        return this.mPbapClientProfile;
    }
    
    public PbapServerProfile getPbapProfile() {
        return this.mPbapProfile;
    }
    
    public LocalBluetoothProfile getProfileByName(final String s) {
        return this.mProfileNameMap.get(s);
    }
    
    public void removeServiceListener(final ServiceListener serviceListener) {
        this.mServiceListeners.remove(serviceListener);
    }
    
    void setBluetoothStateOn() {
        final ParcelUuid[] uuids = this.mLocalAdapter.getUuids();
        if (uuids != null) {
            this.updateLocalProfiles(uuids);
        }
        this.mEventManager.readPairedDevices();
    }
    
    void updateLocalProfiles(final ParcelUuid[] array) {
        if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.AudioSource)) {
            if (this.mA2dpProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local A2DP SRC profile");
                this.addProfile(this.mA2dpProfile = new A2dpProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "A2DP", "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED");
            }
        }
        else if (this.mA2dpProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: A2DP profile was previously added but the UUID is now missing.");
        }
        if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.AudioSink)) {
            if (this.mA2dpSinkProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local A2DP Sink profile");
                this.addProfile(this.mA2dpSinkProfile = new A2dpSinkProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "A2DPSink", "android.bluetooth.a2dp-sink.profile.action.CONNECTION_STATE_CHANGED");
            }
        }
        else if (this.mA2dpSinkProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: A2DP Sink profile was previously added but the UUID is now missing.");
        }
        if (!BluetoothUuid.isUuidPresent(array, BluetoothUuid.Handsfree_AG) && !BluetoothUuid.isUuidPresent(array, BluetoothUuid.HSP_AG)) {
            if (this.mHeadsetProfile != null) {
                Log.w("LocalBluetoothProfileManager", "Warning: HEADSET profile was previously added but the UUID is now missing.");
            }
        }
        else if (this.mHeadsetProfile == null) {
            Log.d("LocalBluetoothProfileManager", "Adding local HEADSET profile");
            this.addHeadsetProfile(this.mHeadsetProfile = new HeadsetProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "HEADSET", "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED", "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED", 10);
        }
        if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.Handsfree)) {
            if (this.mHfpClientProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local HfpClient profile");
                this.addHeadsetProfile(this.mHfpClientProfile = new HfpClientProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "HEADSET_CLIENT", "android.bluetooth.headsetclient.profile.action.CONNECTION_STATE_CHANGED", "android.bluetooth.headsetclient.profile.action.AUDIO_STATE_CHANGED", 0);
            }
        }
        else if (this.mHfpClientProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: Hfp Client profile was previously added but the UUID is now missing.");
        }
        else {
            Log.d("LocalBluetoothProfileManager", "Handsfree Uuid not found.");
        }
        if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.MNS)) {
            if (this.mMapClientProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local Map Client profile");
                this.addProfile(this.mMapClientProfile = new MapClientProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "MAP Client", "android.bluetooth.mapmce.profile.action.CONNECTION_STATE_CHANGED");
            }
        }
        else if (this.mMapClientProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: MAP Client profile was previously added but the UUID is now missing.");
        }
        else {
            Log.d("LocalBluetoothProfileManager", "MAP Client Uuid not found.");
        }
        if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.ObexObjectPush)) {
            if (this.mOppProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local OPP profile");
                this.mOppProfile = new OppProfile();
                this.mProfileNameMap.put("OPP", this.mOppProfile);
            }
        }
        else if (this.mOppProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: OPP profile was previously added but the UUID is now missing.");
        }
        if (this.mUsePbapPce) {
            if (this.mPbapClientProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local PBAP Client profile");
                this.addProfile(this.mPbapClientProfile = new PbapClientProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "PbapClient", "android.bluetooth.pbapclient.profile.action.CONNECTION_STATE_CHANGED");
            }
        }
        else if (this.mPbapClientProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: PBAP Client profile was previously added but the UUID is now missing.");
        }
        if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.HearingAid)) {
            if (this.mHearingAidProfile == null) {
                Log.d("LocalBluetoothProfileManager", "Adding local Hearing Aid profile");
                this.addProfile(this.mHearingAidProfile = new HearingAidProfile(this.mContext, this.mLocalAdapter, this.mDeviceManager, this), "HearingAid", "android.bluetooth.hearingaid.profile.action.CONNECTION_STATE_CHANGED");
            }
        }
        else if (this.mHearingAidProfile != null) {
            Log.w("LocalBluetoothProfileManager", "Warning: Hearing Aid profile was previously added but the UUID is now missing.");
        }
        this.mEventManager.registerProfileIntentReceiver();
    }
    
    void updateProfiles(final ParcelUuid[] array, final ParcelUuid[] array2, final Collection<LocalBluetoothProfile> collection, final Collection<LocalBluetoothProfile> collection2, final boolean b, final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            collection2.clear();
            collection2.addAll(collection);
            final StringBuilder sb = new StringBuilder();
            sb.append("Current Profiles");
            sb.append(collection.toString());
            Log.d("LocalBluetoothProfileManager", sb.toString());
            collection.clear();
            if (array == null) {
                return;
            }
            if (this.mHeadsetProfile != null && ((BluetoothUuid.isUuidPresent(array2, BluetoothUuid.HSP_AG) && BluetoothUuid.isUuidPresent(array, BluetoothUuid.HSP)) || (BluetoothUuid.isUuidPresent(array2, BluetoothUuid.Handsfree_AG) && BluetoothUuid.isUuidPresent(array, BluetoothUuid.Handsfree)))) {
                collection.add(this.mHeadsetProfile);
                collection2.remove(this.mHeadsetProfile);
            }
            if (this.mHfpClientProfile != null && BluetoothUuid.isUuidPresent(array, BluetoothUuid.Handsfree_AG) && BluetoothUuid.isUuidPresent(array2, BluetoothUuid.Handsfree)) {
                collection.add(this.mHfpClientProfile);
                collection2.remove(this.mHfpClientProfile);
            }
            if (BluetoothUuid.containsAnyUuid(array, A2dpProfile.SINK_UUIDS) && this.mA2dpProfile != null) {
                collection.add(this.mA2dpProfile);
                collection2.remove(this.mA2dpProfile);
            }
            if (BluetoothUuid.containsAnyUuid(array, A2dpSinkProfile.SRC_UUIDS) && this.mA2dpSinkProfile != null) {
                collection.add(this.mA2dpSinkProfile);
                collection2.remove(this.mA2dpSinkProfile);
            }
            if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.ObexObjectPush) && this.mOppProfile != null) {
                collection.add(this.mOppProfile);
                collection2.remove(this.mOppProfile);
            }
            if ((BluetoothUuid.isUuidPresent(array, BluetoothUuid.Hid) || BluetoothUuid.isUuidPresent(array, BluetoothUuid.Hogp)) && this.mHidProfile != null) {
                collection.add(this.mHidProfile);
                collection2.remove(this.mHidProfile);
            }
            if (this.mHidDeviceProfile != null && this.mHidDeviceProfile.getConnectionStatus(bluetoothDevice) != 0) {
                collection.add(this.mHidDeviceProfile);
                collection2.remove(this.mHidDeviceProfile);
            }
            if (b) {
                Log.d("LocalBluetoothProfileManager", "Valid PAN-NAP connection exists.");
            }
            if ((BluetoothUuid.isUuidPresent(array, BluetoothUuid.NAP) && this.mPanProfile != null) || b) {
                collection.add(this.mPanProfile);
                collection2.remove(this.mPanProfile);
            }
            if (this.mMapProfile != null && this.mMapProfile.getConnectionStatus(bluetoothDevice) == 2) {
                collection.add(this.mMapProfile);
                collection2.remove(this.mMapProfile);
                this.mMapProfile.setPreferred(bluetoothDevice, true);
            }
            if (this.mPbapProfile != null && this.mPbapProfile.getConnectionStatus(bluetoothDevice) == 2) {
                collection.add(this.mPbapProfile);
                collection2.remove(this.mPbapProfile);
                this.mPbapProfile.setPreferred(bluetoothDevice, true);
            }
            if (this.mMapClientProfile != null) {
                collection.add(this.mMapClientProfile);
                collection2.remove(this.mMapClientProfile);
            }
            if (this.mUsePbapPce) {
                collection.add(this.mPbapClientProfile);
                collection2.remove(this.mPbapClientProfile);
            }
            if (BluetoothUuid.isUuidPresent(array, BluetoothUuid.HearingAid) && this.mHearingAidProfile != null) {
                collection.add(this.mHearingAidProfile);
                collection2.remove(this.mHearingAidProfile);
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("New Profiles");
            sb2.append(collection.toString());
            Log.d("LocalBluetoothProfileManager", sb2.toString());
        }
    }
    
    private class HeadsetStateChangeHandler extends StateChangedHandler
    {
        private final String mAudioChangeAction;
        private final int mAudioDisconnectedState;
        
        HeadsetStateChangeHandler(final LocalBluetoothProfile localBluetoothProfile, final String mAudioChangeAction, final int mAudioDisconnectedState) {
            super(localBluetoothProfile);
            this.mAudioChangeAction = mAudioChangeAction;
            this.mAudioDisconnectedState = mAudioDisconnectedState;
        }
        
        public void onReceiveInternal(final Intent intent, final CachedBluetoothDevice cachedBluetoothDevice) {
            if (this.mAudioChangeAction.equals(intent.getAction())) {
                if (intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0) != this.mAudioDisconnectedState) {
                    cachedBluetoothDevice.onProfileStateChanged(this.mProfile, 2);
                }
                cachedBluetoothDevice.refresh();
            }
            else {
                super.onReceiveInternal(intent, cachedBluetoothDevice);
            }
        }
    }
    
    private class PanStateChangedHandler extends StateChangedHandler
    {
        PanStateChangedHandler(final LocalBluetoothProfile localBluetoothProfile) {
            super(localBluetoothProfile);
        }
        
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            ((PanProfile)this.mProfile).setLocalRole(bluetoothDevice, intent.getIntExtra("android.bluetooth.pan.extra.LOCAL_ROLE", 0));
            super.onReceive(context, intent, bluetoothDevice);
        }
    }
    
    public interface ServiceListener
    {
        void onServiceConnected();
        
        void onServiceDisconnected();
    }
    
    private class StateChangedHandler implements Handler
    {
        final LocalBluetoothProfile mProfile;
        
        StateChangedHandler(final LocalBluetoothProfile mProfile) {
            this.mProfile = mProfile;
        }
        
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            CachedBluetoothDevice cachedBluetoothDevice;
            if ((cachedBluetoothDevice = LocalBluetoothProfileManager.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("StateChangedHandler found new device: ");
                sb.append(bluetoothDevice);
                Log.w("LocalBluetoothProfileManager", sb.toString());
                cachedBluetoothDevice = LocalBluetoothProfileManager.this.mDeviceManager.addDevice(LocalBluetoothProfileManager.this.mLocalAdapter, LocalBluetoothProfileManager.this, bluetoothDevice);
            }
            this.onReceiveInternal(intent, cachedBluetoothDevice);
        }
        
        protected void onReceiveInternal(final Intent intent, final CachedBluetoothDevice cachedBluetoothDevice) {
            final boolean b = false;
            final int intExtra = intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0);
            final int intExtra2 = intent.getIntExtra("android.bluetooth.profile.extra.PREVIOUS_STATE", 0);
            if (intExtra == 0 && intExtra2 == 1) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to connect ");
                sb.append(this.mProfile);
                sb.append(" device");
                Log.i("LocalBluetoothProfileManager", sb.toString());
            }
            boolean b2 = b;
            if (LocalBluetoothProfileManager.this.getHearingAidProfile() != null) {
                b2 = b;
                if (this.mProfile instanceof HearingAidProfile) {
                    b2 = true;
                }
            }
            if (b2 && intExtra == 2 && cachedBluetoothDevice.getHiSyncId() == 0L) {
                final long hiSyncId = LocalBluetoothProfileManager.this.getHearingAidProfile().getHiSyncId(cachedBluetoothDevice.getDevice());
                if (hiSyncId != 0L) {
                    cachedBluetoothDevice.setHiSyncId(hiSyncId);
                    LocalBluetoothProfileManager.this.mDeviceManager.onHiSyncIdChanged(hiSyncId);
                }
            }
            cachedBluetoothDevice.onProfileStateChanged(this.mProfile, intExtra);
            cachedBluetoothDevice.refresh();
            if (b2) {
                final CachedBluetoothDevice hearingAidOtherDevice = LocalBluetoothProfileManager.this.mDeviceManager.getHearingAidOtherDevice(cachedBluetoothDevice, cachedBluetoothDevice.getHiSyncId());
                if (hearingAidOtherDevice != null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Refreshing other hearing aid=");
                    sb2.append(hearingAidOtherDevice);
                    sb2.append(", newState=");
                    sb2.append(intExtra);
                    Log.d("LocalBluetoothProfileManager", sb2.toString());
                    hearingAidOtherDevice.refresh();
                }
            }
            LocalBluetoothProfileManager.this.mEventManager.dispatchProfileConnectionStateChanged(cachedBluetoothDevice, intExtra, this.mProfile.getProfileId());
        }
    }
}
