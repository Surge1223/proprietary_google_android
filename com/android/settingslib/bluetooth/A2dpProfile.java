package com.android.settingslib.bluetooth;

import com.android.internal.annotations.VisibleForTesting;
import java.util.Comparator;
import java.util.Arrays;
import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.Iterator;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothCodecConfig;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.bluetooth.BluetoothUuid;
import com.android.settingslib.wrapper.BluetoothA2dpWrapper;
import android.bluetooth.BluetoothA2dp;
import android.content.Context;
import android.os.ParcelUuid;

public class A2dpProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] SINK_UUIDS;
    private static boolean V;
    private Context mContext;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothA2dp mService;
    private BluetoothA2dpWrapper mServiceWrapper;
    
    static {
        A2dpProfile.V = false;
        SINK_UUIDS = new ParcelUuid[] { BluetoothUuid.AudioSink, BluetoothUuid.AdvAudioDist };
    }
    
    A2dpProfile(final Context mContext, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mContext = mContext;
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(mContext, (BluetoothProfile$ServiceListener)new A2dpServiceListener(), 2);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        if (this.mLocalAdapter.getMaxConnectedAudioDevices() == 1) {
            final List<BluetoothDevice> connectedDevices = this.getConnectedDevices();
            if (connectedDevices != null) {
                for (final BluetoothDevice bluetoothDevice2 : connectedDevices) {
                    if (bluetoothDevice2.equals((Object)bluetoothDevice)) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Connecting to device ");
                        sb.append(bluetoothDevice);
                        sb.append(" : disconnect skipped");
                        Log.w("A2dpProfile", sb.toString());
                    }
                    else {
                        this.mService.disconnect(bluetoothDevice2);
                    }
                }
            }
        }
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 100) {
            this.mService.setPriority(bluetoothDevice, 100);
        }
        return this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (A2dpProfile.V) {
            Log.d("A2dpProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(2, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("A2dpProfile", "Error cleaning up A2DP proxy", t);
            }
        }
    }
    
    public BluetoothDevice getActiveDevice() {
        if (this.mService == null) {
            return null;
        }
        return this.mService.getActiveDevice();
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_headphones_a2dp;
    }
    
    public String getHighQualityAudioOptionLabel(final BluetoothDevice bluetoothDevice) {
        final int bluetooth_profile_a2dp_high_quality_unknown_codec = R.string.bluetooth_profile_a2dp_high_quality_unknown_codec;
        if (!this.supportsHighQualityAudio(bluetoothDevice) || this.getConnectionStatus(bluetoothDevice) != 2) {
            return this.mContext.getString(bluetooth_profile_a2dp_high_quality_unknown_codec);
        }
        BluetoothCodecConfig[] codecsSelectableCapabilities = null;
        if (this.mServiceWrapper.getCodecStatus(bluetoothDevice) != null) {
            codecsSelectableCapabilities = this.mServiceWrapper.getCodecStatus(bluetoothDevice).getCodecsSelectableCapabilities();
            Arrays.sort(codecsSelectableCapabilities, (Comparator<? super BluetoothCodecConfig>)_$$Lambda$A2dpProfile$exPXCssgW4cryyr_RqCY5K_rQFI.INSTANCE);
        }
        BluetoothCodecConfig bluetoothCodecConfig;
        if (codecsSelectableCapabilities != null && codecsSelectableCapabilities.length >= 1) {
            bluetoothCodecConfig = codecsSelectableCapabilities[0];
        }
        else {
            bluetoothCodecConfig = null;
        }
        int codecType;
        if (bluetoothCodecConfig != null && !bluetoothCodecConfig.isMandatoryCodec()) {
            codecType = bluetoothCodecConfig.getCodecType();
        }
        else {
            codecType = 1000000;
        }
        final int n = -1;
        int n2 = 0;
        switch (codecType) {
            default: {
                n2 = n;
                break;
            }
            case 4: {
                n2 = 5;
                break;
            }
            case 3: {
                n2 = 4;
                break;
            }
            case 2: {
                n2 = 3;
                break;
            }
            case 1: {
                n2 = 2;
                break;
            }
            case 0: {
                n2 = 1;
                break;
            }
        }
        if (n2 < 0) {
            return this.mContext.getString(bluetooth_profile_a2dp_high_quality_unknown_codec);
        }
        return this.mContext.getString(R.string.bluetooth_profile_a2dp_high_quality, new Object[] { this.mContext.getResources().getStringArray(R.array.bluetooth_a2dp_codec_titles)[n2] });
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_a2dp;
    }
    
    @Override
    public int getProfileId() {
        return 2;
    }
    
    boolean isA2dpPlaying() {
        if (this.mService == null) {
            return false;
        }
        final Iterator<BluetoothDevice> iterator = this.mService.getConnectedDevices().iterator();
        while (iterator.hasNext()) {
            if (this.mService.isA2dpPlaying((BluetoothDevice)iterator.next())) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    public boolean isHighQualityAudioEnabled(final BluetoothDevice bluetoothDevice) {
        final int optionalCodecsEnabled = this.mServiceWrapper.getOptionalCodecsEnabled(bluetoothDevice);
        boolean b = false;
        if (optionalCodecsEnabled != -1) {
            if (optionalCodecsEnabled == 1) {
                b = true;
            }
            return b;
        }
        if (this.getConnectionStatus(bluetoothDevice) != 2 && this.supportsHighQualityAudio(bluetoothDevice)) {
            return true;
        }
        BluetoothCodecConfig codecConfig = null;
        if (this.mServiceWrapper.getCodecStatus(bluetoothDevice) != null) {
            codecConfig = this.mServiceWrapper.getCodecStatus(bluetoothDevice).getCodecConfig();
        }
        return codecConfig != null && (codecConfig.isMandatoryCodec() ^ true);
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothA2dp mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    public boolean setActiveDevice(final BluetoothDevice activeDevice) {
        return this.mService != null && this.mService.setActiveDevice(activeDevice);
    }
    
    @VisibleForTesting
    void setBluetoothA2dpWrapper(final BluetoothA2dpWrapper mServiceWrapper) {
        this.mServiceWrapper = mServiceWrapper;
    }
    
    public void setHighQualityAudioEnabled(final BluetoothDevice bluetoothDevice, final boolean b) {
        int n;
        if (b) {
            n = 1;
        }
        else {
            n = 0;
        }
        this.mServiceWrapper.setOptionalCodecsEnabled(bluetoothDevice, n);
        if (this.getConnectionStatus(bluetoothDevice) != 2) {
            return;
        }
        if (b) {
            this.mService.enableOptionalCodecs(bluetoothDevice);
        }
        else {
            this.mService.disableOptionalCodecs(bluetoothDevice);
        }
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    public boolean supportsHighQualityAudio(final BluetoothDevice bluetoothDevice) {
        final int supportsOptionalCodecs = this.mServiceWrapper.supportsOptionalCodecs(bluetoothDevice);
        boolean b = true;
        if (supportsOptionalCodecs != 1) {
            b = false;
        }
        return b;
    }
    
    @Override
    public String toString() {
        return "A2DP";
    }
    
    private final class A2dpServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (A2dpProfile.V) {
                Log.d("A2dpProfile", "Bluetooth service connected");
            }
            A2dpProfile.this.mService = (BluetoothA2dp)bluetoothProfile;
            A2dpProfile.this.mServiceWrapper = new BluetoothA2dpWrapper(A2dpProfile.this.mService);
            final List connectedDevices = A2dpProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = A2dpProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("A2dpProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("A2dpProfile", sb.toString());
                    cachedBluetoothDevice = A2dpProfile.this.mDeviceManager.addDevice(A2dpProfile.this.mLocalAdapter, A2dpProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(A2dpProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            A2dpProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (A2dpProfile.V) {
                Log.d("A2dpProfile", "Bluetooth service disconnected");
            }
            A2dpProfile.this.mIsProfileReady = false;
        }
    }
}
