package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import java.util.List;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.bluetooth.BluetoothHearingAid;
import android.content.Context;

public class HearingAidProfile implements LocalBluetoothProfile
{
    private static boolean V;
    private Context mContext;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothHearingAid mService;
    
    static {
        HearingAidProfile.V = true;
    }
    
    HearingAidProfile(final Context mContext, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mContext = mContext;
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(mContext, (BluetoothProfile$ServiceListener)new HearingAidServiceListener(), 21);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        return this.mService != null && this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 100) {
            this.mService.setPriority(bluetoothDevice, 100);
        }
        return this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (HearingAidProfile.V) {
            Log.d("HearingAidProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(21, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("HearingAidProfile", "Error cleaning up Hearing Aid proxy", t);
            }
        }
    }
    
    public List<BluetoothDevice> getActiveDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>();
        }
        return (List<BluetoothDevice>)this.mService.getActiveDevices();
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_hearing_aid;
    }
    
    public long getHiSyncId(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0L;
        }
        return this.mService.getHiSyncId(bluetoothDevice);
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_hearing_aid;
    }
    
    @Override
    public int getProfileId() {
        return 21;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return false;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothHearingAid mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    public boolean setActiveDevice(final BluetoothDevice activeDevice) {
        return this.mService != null && this.mService.setActiveDevice(activeDevice);
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "HearingAid";
    }
    
    private final class HearingAidServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (HearingAidProfile.V) {
                Log.d("HearingAidProfile", "Bluetooth service connected");
            }
            HearingAidProfile.this.mService = (BluetoothHearingAid)bluetoothProfile;
            final List connectedDevices = HearingAidProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = HearingAidProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    if (HearingAidProfile.V) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("HearingAidProfile found new device: ");
                        sb.append(bluetoothDevice);
                        Log.d("HearingAidProfile", sb.toString());
                    }
                    cachedBluetoothDevice = HearingAidProfile.this.mDeviceManager.addDevice(HearingAidProfile.this.mLocalAdapter, HearingAidProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(HearingAidProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            HearingAidProfile.this.mDeviceManager.updateHearingAidsDevices(HearingAidProfile.this.mProfileManager);
            HearingAidProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (HearingAidProfile.V) {
                Log.d("HearingAidProfile", "Bluetooth service disconnected");
            }
            HearingAidProfile.this.mIsProfileReady = false;
        }
    }
}
