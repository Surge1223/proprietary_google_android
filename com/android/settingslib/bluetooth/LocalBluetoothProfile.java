package com.android.settingslib.bluetooth;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;

public interface LocalBluetoothProfile
{
    boolean connect(final BluetoothDevice p0);
    
    boolean disconnect(final BluetoothDevice p0);
    
    int getConnectionStatus(final BluetoothDevice p0);
    
    int getDrawableResource(final BluetoothClass p0);
    
    int getNameResource(final BluetoothDevice p0);
    
    int getProfileId();
    
    boolean isAutoConnectable();
    
    boolean isConnectable();
    
    boolean isPreferred(final BluetoothDevice p0);
    
    boolean isProfileReady();
    
    void setPreferred(final BluetoothDevice p0, final boolean p1);
}
