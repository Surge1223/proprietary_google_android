package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothMapClient;
import android.os.ParcelUuid;

public final class MapClientProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] UUIDS;
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothMapClient mService;
    
    static {
        MapClientProfile.V = false;
        UUIDS = new ParcelUuid[] { BluetoothUuid.MAP, BluetoothUuid.MNS, BluetoothUuid.MAS };
    }
    
    MapClientProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new MapClientServiceListener(), 18);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List<BluetoothDevice> connectedDevices = this.getConnectedDevices();
        if (connectedDevices != null && connectedDevices.contains(bluetoothDevice)) {
            Log.d("MapClientProfile", "Ignoring Connect");
            return true;
        }
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 100) {
            this.mService.setPriority(bluetoothDevice, 100);
        }
        return this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (MapClientProfile.V) {
            Log.d("MapClientProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(18, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("MapClientProfile", "Error cleaning up MAP Client proxy", t);
            }
        }
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_cellphone;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_map;
    }
    
    @Override
    public int getProfileId() {
        return 18;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothMapClient mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        if (MapClientProfile.V) {
            final StringBuilder sb = new StringBuilder();
            sb.append("isProfileReady(): ");
            sb.append(this.mIsProfileReady);
            Log.d("MapClientProfile", sb.toString());
        }
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "MAP Client";
    }
    
    private final class MapClientServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (MapClientProfile.V) {
                Log.d("MapClientProfile", "Bluetooth service connected");
            }
            MapClientProfile.this.mService = (BluetoothMapClient)bluetoothProfile;
            final List connectedDevices = MapClientProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = MapClientProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("MapProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("MapClientProfile", sb.toString());
                    cachedBluetoothDevice = MapClientProfile.this.mDeviceManager.addDevice(MapClientProfile.this.mLocalAdapter, MapClientProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(MapClientProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            MapClientProfile.this.mProfileManager.callServiceConnectedListeners();
            MapClientProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (MapClientProfile.V) {
                Log.d("MapClientProfile", "Bluetooth service disconnected");
            }
            MapClientProfile.this.mProfileManager.callServiceDisconnectedListeners();
            MapClientProfile.this.mIsProfileReady = false;
        }
    }
}
