package com.android.settingslib.bluetooth;

import java.util.HashSet;
import java.util.Objects;
import java.util.Collection;
import java.util.Iterator;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import java.util.HashMap;
import java.util.ArrayList;
import android.content.Context;
import java.util.Map;
import com.android.internal.annotations.VisibleForTesting;
import java.util.List;
import java.util.function.Predicate;

public final class _$$Lambda$CachedBluetoothDeviceManager$RiRiNHhShon_xX_yLpIJ83GKKko implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return CachedBluetoothDeviceManager.lambda$clearNonBondedDevices$2((CachedBluetoothDevice)o);
    }
}
