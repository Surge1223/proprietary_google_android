package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothPan;
import android.bluetooth.BluetoothDevice;
import java.util.HashMap;

public class PanProfile implements LocalBluetoothProfile
{
    private static boolean V;
    private final HashMap<BluetoothDevice, Integer> mDeviceRoleMap;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private BluetoothPan mService;
    
    static {
        PanProfile.V = true;
    }
    
    PanProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter) {
        this.mDeviceRoleMap = new HashMap<BluetoothDevice, Integer>();
        (this.mLocalAdapter = mLocalAdapter).getProfileProxy(context, (BluetoothProfile$ServiceListener)new PanServiceListener(), 5);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (connectedDevices != null) {
            final Iterator<BluetoothDevice> iterator = connectedDevices.iterator();
            while (iterator.hasNext()) {
                this.mService.disconnect((BluetoothDevice)iterator.next());
            }
        }
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        return this.mService != null && this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (PanProfile.V) {
            Log.d("PanProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(5, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("PanProfile", "Error cleaning up PAN proxy", t);
            }
        }
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_network_pan;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        if (this.isLocalRoleNap(bluetoothDevice)) {
            return R.string.bluetooth_profile_pan_nap;
        }
        return R.string.bluetooth_profile_pan;
    }
    
    @Override
    public int getProfileId() {
        return 5;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return false;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    boolean isLocalRoleNap(final BluetoothDevice bluetoothDevice) {
        final boolean containsKey = this.mDeviceRoleMap.containsKey(bluetoothDevice);
        boolean b = false;
        if (containsKey) {
            if (this.mDeviceRoleMap.get(bluetoothDevice) == 1) {
                b = true;
            }
            return b;
        }
        return false;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        return true;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    void setLocalRole(final BluetoothDevice bluetoothDevice, final int n) {
        this.mDeviceRoleMap.put(bluetoothDevice, n);
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
    }
    
    @Override
    public String toString() {
        return "PAN";
    }
    
    private final class PanServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (PanProfile.V) {
                Log.d("PanProfile", "Bluetooth service connected");
            }
            PanProfile.this.mService = (BluetoothPan)bluetoothProfile;
            PanProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (PanProfile.V) {
                Log.d("PanProfile", "Bluetooth service disconnected");
            }
            PanProfile.this.mIsProfileReady = false;
        }
    }
}
