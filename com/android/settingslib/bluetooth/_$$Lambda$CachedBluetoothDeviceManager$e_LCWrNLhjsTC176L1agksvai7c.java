package com.android.settingslib.bluetooth;

import java.util.HashSet;
import java.util.Objects;
import java.util.Collection;
import java.util.Iterator;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import java.util.HashMap;
import java.util.ArrayList;
import android.content.Context;
import com.android.internal.annotations.VisibleForTesting;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public final class _$$Lambda$CachedBluetoothDeviceManager$e_LCWrNLhjsTC176L1agksvai7c implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return CachedBluetoothDeviceManager.lambda$clearNonBondedDevices$0((Map.Entry)o);
    }
}
