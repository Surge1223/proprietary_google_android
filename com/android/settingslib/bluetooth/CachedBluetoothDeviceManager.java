package com.android.settingslib.bluetooth;

import java.util.HashSet;
import java.util.Objects;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import java.util.HashMap;
import java.util.ArrayList;
import android.content.Context;
import java.util.Map;
import com.android.internal.annotations.VisibleForTesting;
import java.util.List;

public class CachedBluetoothDeviceManager
{
    private final LocalBluetoothManager mBtManager;
    @VisibleForTesting
    final List<CachedBluetoothDevice> mCachedDevices;
    @VisibleForTesting
    final Map<Long, CachedBluetoothDevice> mCachedDevicesMapForHearingAids;
    private Context mContext;
    @VisibleForTesting
    final List<CachedBluetoothDevice> mHearingAidDevicesNotAddedInCache;
    
    CachedBluetoothDeviceManager(final Context mContext, final LocalBluetoothManager mBtManager) {
        this.mCachedDevices = new ArrayList<CachedBluetoothDevice>();
        this.mHearingAidDevicesNotAddedInCache = new ArrayList<CachedBluetoothDevice>();
        this.mCachedDevicesMapForHearingAids = new HashMap<Long, CachedBluetoothDevice>();
        this.mContext = mContext;
        this.mBtManager = mBtManager;
    }
    
    private void hearingAidSwitchDisplayDevice(final CachedBluetoothDevice cachedBluetoothDevice, final CachedBluetoothDevice cachedBluetoothDevice2, final long n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("hearingAidSwitchDisplayDevice: toDisplayDevice=");
        sb.append(cachedBluetoothDevice);
        sb.append(", toHideDevice=");
        sb.append(cachedBluetoothDevice2);
        this.log(sb.toString());
        this.mHearingAidDevicesNotAddedInCache.add(cachedBluetoothDevice2);
        this.mCachedDevices.remove(cachedBluetoothDevice2);
        this.mBtManager.getEventManager().dispatchDeviceRemoved(cachedBluetoothDevice2);
        this.mHearingAidDevicesNotAddedInCache.remove(cachedBluetoothDevice);
        this.mCachedDevices.add(cachedBluetoothDevice);
        this.mCachedDevicesMapForHearingAids.put(n, cachedBluetoothDevice);
        this.mBtManager.getEventManager().dispatchDeviceAdded(cachedBluetoothDevice);
    }
    
    private boolean isPairAddedInCache(final long n) {
        // monitorenter(this)
        if (n == 0L) {
            // monitorexit(this)
            return false;
        }
        try {
            return this.mCachedDevicesMapForHearingAids.containsKey(n);
        }
        finally {
        }
        // monitorexit(this)
    }
    
    private void log(final String s) {
        Log.d("CachedBluetoothDeviceManager", s);
    }
    
    public static boolean onDeviceDisappeared(final CachedBluetoothDevice cachedBluetoothDevice) {
        boolean b = false;
        cachedBluetoothDevice.setJustDiscovered(false);
        if (cachedBluetoothDevice.getBondState() == 10) {
            b = true;
        }
        return b;
    }
    
    public CachedBluetoothDevice addDevice(final LocalBluetoothAdapter localBluetoothAdapter, final LocalBluetoothProfileManager localBluetoothProfileManager, final BluetoothDevice bluetoothDevice) {
        final CachedBluetoothDevice cachedBluetoothDevice = new CachedBluetoothDevice(this.mContext, localBluetoothAdapter, localBluetoothProfileManager, bluetoothDevice);
        if (localBluetoothProfileManager.getHearingAidProfile() != null && localBluetoothProfileManager.getHearingAidProfile().getHiSyncId(cachedBluetoothDevice.getDevice()) != 0L) {
            cachedBluetoothDevice.setHiSyncId(localBluetoothProfileManager.getHearingAidProfile().getHiSyncId(cachedBluetoothDevice.getDevice()));
        }
        if (this.isPairAddedInCache(cachedBluetoothDevice.getHiSyncId())) {
            synchronized (this) {
                this.mHearingAidDevicesNotAddedInCache.add(cachedBluetoothDevice);
                return cachedBluetoothDevice;
            }
        }
        synchronized (this) {
            this.mCachedDevices.add(cachedBluetoothDevice);
            if (cachedBluetoothDevice.getHiSyncId() != 0L && !this.mCachedDevicesMapForHearingAids.containsKey(cachedBluetoothDevice.getHiSyncId())) {
                this.mCachedDevicesMapForHearingAids.put(cachedBluetoothDevice.getHiSyncId(), cachedBluetoothDevice);
            }
            this.mBtManager.getEventManager().dispatchDeviceAdded(cachedBluetoothDevice);
            return cachedBluetoothDevice;
        }
    }
    
    public void clearNonBondedDevices() {
        synchronized (this) {
            this.mCachedDevicesMapForHearingAids.entrySet().removeIf((Predicate<? super Object>)_$$Lambda$CachedBluetoothDeviceManager$e_LCWrNLhjsTC176L1agksvai7c.INSTANCE);
            this.mCachedDevices.removeIf((Predicate<? super Object>)_$$Lambda$CachedBluetoothDeviceManager$kt27ylP2LAAkpFyw4Jk0DP1n8j4.INSTANCE);
            this.mHearingAidDevicesNotAddedInCache.removeIf((Predicate<? super Object>)_$$Lambda$CachedBluetoothDeviceManager$RiRiNHhShon_xX_yLpIJ83GKKko.INSTANCE);
        }
    }
    
    public void dispatchAudioModeChanged() {
        synchronized (this) {
            final Iterator<CachedBluetoothDevice> iterator = this.mCachedDevices.iterator();
            while (iterator.hasNext()) {
                iterator.next().onAudioModeChanged();
            }
        }
    }
    
    public CachedBluetoothDevice findDevice(final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            for (final CachedBluetoothDevice cachedBluetoothDevice : this.mCachedDevices) {
                if (cachedBluetoothDevice.getDevice().equals((Object)bluetoothDevice)) {
                    return cachedBluetoothDevice;
                }
            }
            for (final CachedBluetoothDevice cachedBluetoothDevice2 : this.mHearingAidDevicesNotAddedInCache) {
                if (cachedBluetoothDevice2.getDevice().equals((Object)bluetoothDevice)) {
                    return cachedBluetoothDevice2;
                }
            }
            return null;
        }
    }
    
    public Collection<CachedBluetoothDevice> getCachedDevicesCopy() {
        synchronized (this) {
            return new ArrayList<CachedBluetoothDevice>(this.mCachedDevices);
        }
    }
    
    public CachedBluetoothDevice getHearingAidOtherDevice(final CachedBluetoothDevice cachedBluetoothDevice, final long n) {
        if (n == 0L) {
            return null;
        }
        for (final CachedBluetoothDevice cachedBluetoothDevice2 : this.mHearingAidDevicesNotAddedInCache) {
            if (n == cachedBluetoothDevice2.getHiSyncId() && !Objects.equals(cachedBluetoothDevice2, cachedBluetoothDevice)) {
                return cachedBluetoothDevice2;
            }
        }
        final CachedBluetoothDevice cachedBluetoothDevice3 = this.mCachedDevicesMapForHearingAids.get(n);
        if (!Objects.equals(cachedBluetoothDevice3, cachedBluetoothDevice)) {
            return cachedBluetoothDevice3;
        }
        return null;
    }
    
    public String getHearingAidPairDeviceSummary(final CachedBluetoothDevice cachedBluetoothDevice) {
        // monitorenter(this)
        final String s = null;
        try {
            final CachedBluetoothDevice hearingAidOtherDevice = this.getHearingAidOtherDevice(cachedBluetoothDevice, cachedBluetoothDevice.getHiSyncId());
            String connectionSummary = s;
            if (hearingAidOtherDevice != null) {
                connectionSummary = hearingAidOtherDevice.getConnectionSummary();
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("getHearingAidPairDeviceSummary: pairDeviceSummary=");
            sb.append(connectionSummary);
            sb.append(", otherHearingAidDevice=");
            sb.append(hearingAidOtherDevice);
            this.log(sb.toString());
            return connectionSummary;
        }
        finally {
        }
        // monitorexit(this)
    }
    
    public String getName(final BluetoothDevice bluetoothDevice) {
        final CachedBluetoothDevice device = this.findDevice(bluetoothDevice);
        if (device != null && device.getName() != null) {
            return device.getName();
        }
        final String aliasName = bluetoothDevice.getAliasName();
        if (aliasName != null) {
            return aliasName;
        }
        return bluetoothDevice.getAddress();
    }
    
    public void onActiveDeviceChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        synchronized (this) {
            for (final CachedBluetoothDevice cachedBluetoothDevice2 : this.mCachedDevices) {
                cachedBluetoothDevice2.onActiveDeviceChanged(Objects.equals(cachedBluetoothDevice2, cachedBluetoothDevice), n);
            }
        }
    }
    
    public void onBluetoothStateChanged(int i) {
        // monitorenter(this)
        if (i == 13) {
            try {
                CachedBluetoothDevice cachedBluetoothDevice;
                for (i = this.mCachedDevices.size() - 1; i >= 0; --i) {
                    cachedBluetoothDevice = this.mCachedDevices.get(i);
                    if (cachedBluetoothDevice.getBondState() != 12) {
                        cachedBluetoothDevice.setJustDiscovered(false);
                        this.mCachedDevices.remove(i);
                        if (cachedBluetoothDevice.getHiSyncId() != 0L && this.mCachedDevicesMapForHearingAids.containsKey(cachedBluetoothDevice.getHiSyncId())) {
                            this.mCachedDevicesMapForHearingAids.remove(cachedBluetoothDevice.getHiSyncId());
                        }
                    }
                    else {
                        cachedBluetoothDevice.clearProfileConnectionState();
                    }
                }
                CachedBluetoothDevice cachedBluetoothDevice2;
                for (i = this.mHearingAidDevicesNotAddedInCache.size() - 1; i >= 0; --i) {
                    cachedBluetoothDevice2 = this.mHearingAidDevicesNotAddedInCache.get(i);
                    if (cachedBluetoothDevice2.getBondState() != 12) {
                        cachedBluetoothDevice2.setJustDiscovered(false);
                        this.mHearingAidDevicesNotAddedInCache.remove(i);
                    }
                    else {
                        cachedBluetoothDevice2.clearProfileConnectionState();
                    }
                }
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    // monitorexit(this)
    
    public void onBtClassChanged(final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            final CachedBluetoothDevice device = this.findDevice(bluetoothDevice);
            if (device != null) {
                device.refreshBtClass();
            }
        }
    }
    
    public void onDeviceNameUpdated(final BluetoothDevice bluetoothDevice) {
        final CachedBluetoothDevice device = this.findDevice(bluetoothDevice);
        if (device != null) {
            device.refreshName();
        }
    }
    
    public void onDeviceUnpaired(final CachedBluetoothDevice cachedBluetoothDevice) {
        synchronized (this) {
            final long hiSyncId = cachedBluetoothDevice.getHiSyncId();
            if (hiSyncId == 0L) {
                return;
            }
            for (int i = this.mHearingAidDevicesNotAddedInCache.size() - 1; i >= 0; --i) {
                final CachedBluetoothDevice cachedBluetoothDevice2 = this.mHearingAidDevicesNotAddedInCache.get(i);
                if (cachedBluetoothDevice2.getHiSyncId() == hiSyncId) {
                    this.mHearingAidDevicesNotAddedInCache.remove(i);
                    if (cachedBluetoothDevice != cachedBluetoothDevice2) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("onDeviceUnpaired: Unpair device=");
                        sb.append(cachedBluetoothDevice2);
                        this.log(sb.toString());
                        cachedBluetoothDevice2.unpair();
                    }
                }
            }
            final CachedBluetoothDevice cachedBluetoothDevice3 = this.mCachedDevicesMapForHearingAids.get(hiSyncId);
            if (cachedBluetoothDevice3 != null && !Objects.equals(cachedBluetoothDevice, cachedBluetoothDevice3)) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("onDeviceUnpaired: Unpair mapped device=");
                sb2.append(cachedBluetoothDevice3);
                this.log(sb2.toString());
                cachedBluetoothDevice3.unpair();
            }
        }
    }
    
    public void onHiSyncIdChanged(final long n) {
        // monitorenter(this)
        int n2 = -1;
        try {
            int n3;
            for (int i = this.mCachedDevices.size() - 1; i >= 0; --i, n2 = n3) {
                CachedBluetoothDevice cachedBluetoothDevice = this.mCachedDevices.get(i);
                n3 = n2;
                if (cachedBluetoothDevice.getHiSyncId() == n) {
                    if (n2 != -1) {
                        if (cachedBluetoothDevice.isConnected()) {
                            i = n2;
                            final CachedBluetoothDevice cachedBluetoothDevice2 = this.mCachedDevices.get(n2);
                            this.mCachedDevicesMapForHearingAids.put(n, cachedBluetoothDevice);
                            cachedBluetoothDevice = cachedBluetoothDevice2;
                        }
                        else {
                            this.mCachedDevicesMapForHearingAids.put(n, this.mCachedDevices.get(n2));
                        }
                        this.mCachedDevices.remove(i);
                        this.mHearingAidDevicesNotAddedInCache.add(cachedBluetoothDevice);
                        final StringBuilder sb = new StringBuilder();
                        sb.append("onHiSyncIdChanged: removed from UI device=");
                        sb.append(cachedBluetoothDevice);
                        sb.append(", with hiSyncId=");
                        sb.append(n);
                        this.log(sb.toString());
                        this.mBtManager.getEventManager().dispatchDeviceRemoved(cachedBluetoothDevice);
                        break;
                    }
                    this.mCachedDevicesMapForHearingAids.put(n, cachedBluetoothDevice);
                    n3 = i;
                }
            }
        }
        finally {
        }
        // monitorexit(this)
    }
    
    public void onProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
        // monitorenter(this)
        if (n2 == 21) {
            try {
                if (cachedBluetoothDevice.getHiSyncId() != 0L && cachedBluetoothDevice.getBondState() == 12) {
                    final long hiSyncId = cachedBluetoothDevice.getHiSyncId();
                    final CachedBluetoothDevice hearingAidOtherDevice = this.getHearingAidOtherDevice(cachedBluetoothDevice, hiSyncId);
                    if (hearingAidOtherDevice == null) {
                        return;
                    }
                    if (n == 2 && this.mHearingAidDevicesNotAddedInCache.contains(cachedBluetoothDevice)) {
                        this.hearingAidSwitchDisplayDevice(cachedBluetoothDevice, hearingAidOtherDevice, hiSyncId);
                    }
                    else if (n == 0 && hearingAidOtherDevice.isConnected()) {
                        final CachedBluetoothDevice cachedBluetoothDevice2 = this.mCachedDevicesMapForHearingAids.get(hiSyncId);
                        if (cachedBluetoothDevice2 != null && Objects.equals(cachedBluetoothDevice, cachedBluetoothDevice2)) {
                            this.hearingAidSwitchDisplayDevice(hearingAidOtherDevice, cachedBluetoothDevice, hiSyncId);
                        }
                    }
                }
            }
            finally {
            }
            // monitorexit(this)
        }
    }
    // monitorexit(this)
    
    public void onScanningStateChanged(final boolean b) {
        // monitorenter(this)
        if (!b) {
            // monitorexit(this)
            return;
        }
        try {
            for (int i = this.mCachedDevices.size() - 1; i >= 0; --i) {
                this.mCachedDevices.get(i).setJustDiscovered(false);
            }
            for (int j = this.mHearingAidDevicesNotAddedInCache.size() - 1; j >= 0; --j) {
                this.mHearingAidDevicesNotAddedInCache.get(j).setJustDiscovered(false);
            }
        }
        finally {
        }
        // monitorexit(this)
    }
    
    public void onUuidChanged(final BluetoothDevice bluetoothDevice) {
        synchronized (this) {
            final CachedBluetoothDevice device = this.findDevice(bluetoothDevice);
            if (device != null) {
                device.onUuidChanged();
            }
        }
    }
    
    public void updateHearingAidsDevices(final LocalBluetoothProfileManager localBluetoothProfileManager) {
        synchronized (this) {
            final HearingAidProfile hearingAidProfile = localBluetoothProfileManager.getHearingAidProfile();
            if (hearingAidProfile == null) {
                this.log("updateHearingAidsDevices: getHearingAidProfile() is null");
                return;
            }
            final HashSet<Long> set = new HashSet<Long>();
            for (final CachedBluetoothDevice cachedBluetoothDevice : this.mCachedDevices) {
                if (cachedBluetoothDevice.getHiSyncId() != 0L) {
                    continue;
                }
                final long hiSyncId = hearingAidProfile.getHiSyncId(cachedBluetoothDevice.getDevice());
                if (hiSyncId == 0L) {
                    continue;
                }
                cachedBluetoothDevice.setHiSyncId(hiSyncId);
                set.add(hiSyncId);
            }
            final Iterator<Object> iterator2 = set.iterator();
            while (iterator2.hasNext()) {
                this.onHiSyncIdChanged(iterator2.next());
            }
        }
    }
}
