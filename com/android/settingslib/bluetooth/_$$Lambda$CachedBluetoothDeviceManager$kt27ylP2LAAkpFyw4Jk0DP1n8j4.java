package com.android.settingslib.bluetooth;

import java.util.HashSet;
import java.util.Objects;
import java.util.Collection;
import java.util.Iterator;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import java.util.HashMap;
import java.util.ArrayList;
import android.content.Context;
import java.util.Map;
import com.android.internal.annotations.VisibleForTesting;
import java.util.List;
import java.util.function.Predicate;

public final class _$$Lambda$CachedBluetoothDeviceManager$kt27ylP2LAAkpFyw4Jk0DP1n8j4 implements Predicate
{
    @Override
    public final boolean test(final Object o) {
        return CachedBluetoothDeviceManager.lambda$clearNonBondedDevices$1((CachedBluetoothDevice)o);
    }
}
