package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.Iterator;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothHeadset;
import android.os.ParcelUuid;

public class HeadsetProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] UUIDS;
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothHeadset mService;
    
    static {
        HeadsetProfile.V = true;
        UUIDS = new ParcelUuid[] { BluetoothUuid.HSP, BluetoothUuid.Handsfree };
    }
    
    HeadsetProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new HeadsetServiceListener(), 1);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (connectedDevices != null) {
            for (final BluetoothDevice bluetoothDevice2 : connectedDevices) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Not disconnecting device = ");
                sb.append(bluetoothDevice2);
                Log.d("HeadsetProfile", sb.toString());
            }
        }
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (!connectedDevices.isEmpty()) {
            final Iterator<BluetoothDevice> iterator = connectedDevices.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals((Object)bluetoothDevice)) {
                    if (HeadsetProfile.V) {
                        Log.d("HeadsetProfile", "Downgrade priority as useris disconnecting the headset");
                    }
                    if (this.mService.getPriority(bluetoothDevice) > 100) {
                        this.mService.setPriority(bluetoothDevice, 100);
                    }
                    return this.mService.disconnect(bluetoothDevice);
                }
            }
        }
        return false;
    }
    
    @Override
    protected void finalize() {
        if (HeadsetProfile.V) {
            Log.d("HeadsetProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(1, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("HeadsetProfile", "Error cleaning up HID proxy", t);
            }
        }
    }
    
    public BluetoothDevice getActiveDevice() {
        if (this.mService == null) {
            return null;
        }
        return this.mService.getActiveDevice();
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (!connectedDevices.isEmpty()) {
            final Iterator<BluetoothDevice> iterator = connectedDevices.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals((Object)bluetoothDevice)) {
                    return this.mService.getConnectionState(bluetoothDevice);
                }
            }
        }
        return 0;
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_headset_hfp;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_headset;
    }
    
    @Override
    public int getProfileId() {
        return 1;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothHeadset mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    public boolean setActiveDevice(final BluetoothDevice activeDevice) {
        return this.mService != null && this.mService.setActiveDevice(activeDevice);
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "HEADSET";
    }
    
    private final class HeadsetServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (HeadsetProfile.V) {
                Log.d("HeadsetProfile", "Bluetooth service connected");
            }
            HeadsetProfile.this.mService = (BluetoothHeadset)bluetoothProfile;
            final List connectedDevices = HeadsetProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = HeadsetProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("HeadsetProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("HeadsetProfile", sb.toString());
                    cachedBluetoothDevice = HeadsetProfile.this.mDeviceManager.addDevice(HeadsetProfile.this.mLocalAdapter, HeadsetProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(HeadsetProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            HeadsetProfile.this.mProfileManager.callServiceConnectedListeners();
            HeadsetProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (HeadsetProfile.V) {
                Log.d("HeadsetProfile", "Bluetooth service disconnected");
            }
            HeadsetProfile.this.mProfileManager.callServiceDisconnectedListeners();
            HeadsetProfile.this.mIsProfileReady = false;
        }
    }
}
