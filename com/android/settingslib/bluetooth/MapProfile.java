package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothMap;
import android.os.ParcelUuid;

public class MapProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] UUIDS;
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothMap mService;
    
    static {
        MapProfile.V = true;
        UUIDS = new ParcelUuid[] { BluetoothUuid.MAP, BluetoothUuid.MNS, BluetoothUuid.MAS };
    }
    
    MapProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new MapServiceListener(), 9);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (MapProfile.V) {
            Log.d("MapProfile", "connect() - should not get called");
        }
        return false;
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (!connectedDevices.isEmpty() && connectedDevices.get(0).equals((Object)bluetoothDevice)) {
            if (this.mService.getPriority(bluetoothDevice) > 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
            return this.mService.disconnect(bluetoothDevice);
        }
        return false;
    }
    
    @Override
    protected void finalize() {
        if (MapProfile.V) {
            Log.d("MapProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(9, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("MapProfile", "Error cleaning up MAP proxy", t);
            }
        }
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        final BluetoothMap mService = this.mService;
        int connectionState = 0;
        if (mService == null) {
            return 0;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (MapProfile.V) {
            final StringBuilder sb = new StringBuilder();
            sb.append("getConnectionStatus: status is: ");
            sb.append(this.mService.getConnectionState(bluetoothDevice));
            Log.d("MapProfile", sb.toString());
        }
        if (!connectedDevices.isEmpty() && connectedDevices.get(0).equals((Object)bluetoothDevice)) {
            connectionState = this.mService.getConnectionState(bluetoothDevice);
        }
        return connectionState;
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_cellphone;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_map;
    }
    
    @Override
    public int getProfileId() {
        return 9;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothMap mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        if (MapProfile.V) {
            final StringBuilder sb = new StringBuilder();
            sb.append("isProfileReady(): ");
            sb.append(this.mIsProfileReady);
            Log.d("MapProfile", sb.toString());
        }
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "MAP";
    }
    
    private final class MapServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (MapProfile.V) {
                Log.d("MapProfile", "Bluetooth service connected");
            }
            MapProfile.this.mService = (BluetoothMap)bluetoothProfile;
            final List connectedDevices = MapProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = MapProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("MapProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("MapProfile", sb.toString());
                    cachedBluetoothDevice = MapProfile.this.mDeviceManager.addDevice(MapProfile.this.mLocalAdapter, MapProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(MapProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            MapProfile.this.mProfileManager.callServiceConnectedListeners();
            MapProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (MapProfile.V) {
                Log.d("MapProfile", "Bluetooth service disconnected");
            }
            MapProfile.this.mProfileManager.callServiceDisconnectedListeners();
            MapProfile.this.mIsProfileReady = false;
        }
    }
}
