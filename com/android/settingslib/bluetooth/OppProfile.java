package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;

final class OppProfile implements LocalBluetoothProfile
{
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        return false;
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        return false;
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        return 0;
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return 0;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_opp;
    }
    
    @Override
    public int getProfileId() {
        return 20;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return false;
    }
    
    @Override
    public boolean isConnectable() {
        return false;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        return false;
    }
    
    @Override
    public boolean isProfileReady() {
        return true;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
    }
    
    @Override
    public String toString() {
        return "OPP";
    }
}
