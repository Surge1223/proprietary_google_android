package com.android.settingslib.bluetooth;

import android.os.ParcelUuid;
import java.util.List;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothDevice;
import java.util.Set;
import android.bluetooth.BluetoothAdapter;

public class LocalBluetoothAdapter
{
    private static LocalBluetoothAdapter sInstance;
    private final BluetoothAdapter mAdapter;
    private long mLastScan;
    private LocalBluetoothProfileManager mProfileManager;
    private int mState;
    
    private LocalBluetoothAdapter(final BluetoothAdapter mAdapter) {
        this.mState = Integer.MIN_VALUE;
        this.mAdapter = mAdapter;
    }
    
    static LocalBluetoothAdapter getInstance() {
        synchronized (LocalBluetoothAdapter.class) {
            if (LocalBluetoothAdapter.sInstance == null) {
                final BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                if (defaultAdapter != null) {
                    LocalBluetoothAdapter.sInstance = new LocalBluetoothAdapter(defaultAdapter);
                }
            }
            return LocalBluetoothAdapter.sInstance;
        }
    }
    
    public void cancelDiscovery() {
        this.mAdapter.cancelDiscovery();
    }
    
    public boolean disable() {
        return this.mAdapter.disable();
    }
    
    public boolean enable() {
        return this.mAdapter.enable();
    }
    
    public String getAddress() {
        return this.mAdapter.getAddress();
    }
    
    public int getBluetoothState() {
        synchronized (this) {
            this.syncBluetoothState();
            return this.mState;
        }
    }
    
    public Set<BluetoothDevice> getBondedDevices() {
        return (Set<BluetoothDevice>)this.mAdapter.getBondedDevices();
    }
    
    public long getDiscoveryEndMillis() {
        return this.mAdapter.getDiscoveryEndMillis();
    }
    
    public int getMaxConnectedAudioDevices() {
        return this.mAdapter.getMaxConnectedAudioDevices();
    }
    
    public String getName() {
        return this.mAdapter.getName();
    }
    
    void getProfileProxy(final Context context, final BluetoothProfile$ServiceListener bluetoothProfile$ServiceListener, final int n) {
        this.mAdapter.getProfileProxy(context, bluetoothProfile$ServiceListener, n);
    }
    
    public BluetoothDevice getRemoteDevice(final String s) {
        return this.mAdapter.getRemoteDevice(s);
    }
    
    public int getScanMode() {
        return this.mAdapter.getScanMode();
    }
    
    public int getState() {
        return this.mAdapter.getState();
    }
    
    public List<Integer> getSupportedProfiles() {
        return (List<Integer>)this.mAdapter.getSupportedProfiles();
    }
    
    public ParcelUuid[] getUuids() {
        return this.mAdapter.getUuids();
    }
    
    public boolean isDiscovering() {
        return this.mAdapter.isDiscovering();
    }
    
    public boolean isEnabled() {
        return this.mAdapter.isEnabled();
    }
    
    public boolean setBluetoothEnabled(final boolean b) {
        boolean b2;
        if (b) {
            b2 = this.mAdapter.enable();
        }
        else {
            b2 = this.mAdapter.disable();
        }
        if (b2) {
            int bluetoothStateInt;
            if (b) {
                bluetoothStateInt = 11;
            }
            else {
                bluetoothStateInt = 13;
            }
            this.setBluetoothStateInt(bluetoothStateInt);
        }
        else {
            this.syncBluetoothState();
        }
        return b2;
    }
    
    void setBluetoothStateInt(final int mState) {
        synchronized (this) {
            if (this.mState == mState) {
                return;
            }
            this.mState = mState;
            // monitorexit(this)
            if (mState == 12 && this.mProfileManager != null) {
                this.mProfileManager.setBluetoothStateOn();
            }
        }
    }
    
    public void setName(final String name) {
        this.mAdapter.setName(name);
    }
    
    void setProfileManager(final LocalBluetoothProfileManager mProfileManager) {
        this.mProfileManager = mProfileManager;
    }
    
    public void setScanMode(final int scanMode) {
        this.mAdapter.setScanMode(scanMode);
    }
    
    public boolean setScanMode(final int n, final int n2) {
        return this.mAdapter.setScanMode(n, n2);
    }
    
    public void startScanning(final boolean b) {
        if (!this.mAdapter.isDiscovering()) {
            if (!b) {
                if (this.mLastScan + 300000L > System.currentTimeMillis()) {
                    return;
                }
                final A2dpProfile a2dpProfile = this.mProfileManager.getA2dpProfile();
                if (a2dpProfile != null && a2dpProfile.isA2dpPlaying()) {
                    return;
                }
                final A2dpSinkProfile a2dpSinkProfile = this.mProfileManager.getA2dpSinkProfile();
                if (a2dpSinkProfile != null && a2dpSinkProfile.isA2dpPlaying()) {
                    return;
                }
            }
            if (this.mAdapter.startDiscovery()) {
                this.mLastScan = System.currentTimeMillis();
            }
        }
    }
    
    public void stopScanning() {
        if (this.mAdapter.isDiscovering()) {
            this.mAdapter.cancelDiscovery();
        }
    }
    
    boolean syncBluetoothState() {
        if (this.mAdapter.getState() != this.mState) {
            this.setBluetoothStateInt(this.mAdapter.getState());
            return true;
        }
        return false;
    }
}
