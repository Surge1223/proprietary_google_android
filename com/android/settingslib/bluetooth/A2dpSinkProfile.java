package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.Iterator;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothA2dpSink;
import android.os.ParcelUuid;

final class A2dpSinkProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] SRC_UUIDS;
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothA2dpSink mService;
    
    static {
        A2dpSinkProfile.V = true;
        SRC_UUIDS = new ParcelUuid[] { BluetoothUuid.AudioSource, BluetoothUuid.AdvAudioDist };
    }
    
    A2dpSinkProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new A2dpSinkServiceListener(), 11);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List<BluetoothDevice> connectedDevices = this.getConnectedDevices();
        if (connectedDevices != null) {
            final Iterator<BluetoothDevice> iterator = connectedDevices.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals((Object)bluetoothDevice)) {
                    Log.d("A2dpSinkProfile", "Ignoring Connect");
                    return true;
                }
            }
        }
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 100) {
            this.mService.setPriority(bluetoothDevice, 100);
        }
        return this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (A2dpSinkProfile.V) {
            Log.d("A2dpSinkProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(11, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("A2dpSinkProfile", "Error cleaning up A2DP proxy", t);
            }
        }
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_headphones_a2dp;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_a2dp;
    }
    
    @Override
    public int getProfileId() {
        return 11;
    }
    
    boolean isA2dpPlaying() {
        if (this.mService == null) {
            return false;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        return !connectedDevices.isEmpty() && this.mService.isA2dpPlaying((BluetoothDevice)connectedDevices.get(0));
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothA2dpSink mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "A2DPSink";
    }
    
    private final class A2dpSinkServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (A2dpSinkProfile.V) {
                Log.d("A2dpSinkProfile", "Bluetooth service connected");
            }
            A2dpSinkProfile.this.mService = (BluetoothA2dpSink)bluetoothProfile;
            final List connectedDevices = A2dpSinkProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = A2dpSinkProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("A2dpSinkProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("A2dpSinkProfile", sb.toString());
                    cachedBluetoothDevice = A2dpSinkProfile.this.mDeviceManager.addDevice(A2dpSinkProfile.this.mLocalAdapter, A2dpSinkProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(A2dpSinkProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            A2dpSinkProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (A2dpSinkProfile.V) {
                Log.d("A2dpSinkProfile", "Bluetooth service disconnected");
            }
            A2dpSinkProfile.this.mIsProfileReady = false;
        }
    }
}
