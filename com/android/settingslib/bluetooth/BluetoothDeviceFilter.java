package com.android.settingslib.bluetooth;

import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothClass;
import android.os.ParcelUuid;
import android.bluetooth.BluetoothDevice;
import android.util.Log;

public final class BluetoothDeviceFilter
{
    public static final Filter ALL_FILTER;
    public static final Filter BONDED_DEVICE_FILTER;
    private static final Filter[] FILTERS;
    public static final Filter UNBONDED_DEVICE_FILTER;
    
    static {
        ALL_FILTER = (Filter)new AllFilter();
        BONDED_DEVICE_FILTER = (Filter)new BondedDeviceFilter();
        UNBONDED_DEVICE_FILTER = (Filter)new UnbondedDeviceFilter();
        FILTERS = new Filter[] { BluetoothDeviceFilter.ALL_FILTER, new AudioFilter(), new TransferFilter(), new PanuFilter(), new NapFilter() };
    }
    
    public static Filter getFilter(final int n) {
        if (n >= 0 && n < BluetoothDeviceFilter.FILTERS.length) {
            return BluetoothDeviceFilter.FILTERS[n];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Invalid filter type ");
        sb.append(n);
        sb.append(" for device picker");
        Log.w("BluetoothDeviceFilter", sb.toString());
        return BluetoothDeviceFilter.ALL_FILTER;
    }
    
    private static final class AllFilter implements Filter
    {
        @Override
        public boolean matches(final BluetoothDevice bluetoothDevice) {
            return true;
        }
    }
    
    private static final class AudioFilter extends ClassUuidFilter
    {
        @Override
        boolean matches(final ParcelUuid[] array, final BluetoothClass bluetoothClass) {
            if (array != null) {
                if (BluetoothUuid.containsAnyUuid(array, A2dpProfile.SINK_UUIDS)) {
                    return true;
                }
                if (BluetoothUuid.containsAnyUuid(array, HeadsetProfile.UUIDS)) {
                    return true;
                }
            }
            else if (bluetoothClass != null && (bluetoothClass.doesClassMatch(1) || bluetoothClass.doesClassMatch(0))) {
                return true;
            }
            return false;
        }
    }
    
    private static final class BondedDeviceFilter implements Filter
    {
        @Override
        public boolean matches(final BluetoothDevice bluetoothDevice) {
            return bluetoothDevice.getBondState() == 12;
        }
    }
    
    private abstract static class ClassUuidFilter implements Filter
    {
        @Override
        public boolean matches(final BluetoothDevice bluetoothDevice) {
            return this.matches(bluetoothDevice.getUuids(), bluetoothDevice.getBluetoothClass());
        }
        
        abstract boolean matches(final ParcelUuid[] p0, final BluetoothClass p1);
    }
    
    public interface Filter
    {
        boolean matches(final BluetoothDevice p0);
    }
    
    private static final class NapFilter extends ClassUuidFilter
    {
        @Override
        boolean matches(final ParcelUuid[] array, final BluetoothClass bluetoothClass) {
            boolean b = true;
            if (array != null && BluetoothUuid.isUuidPresent(array, BluetoothUuid.NAP)) {
                return true;
            }
            if (bluetoothClass == null || !bluetoothClass.doesClassMatch(5)) {
                b = false;
            }
            return b;
        }
    }
    
    private static final class PanuFilter extends ClassUuidFilter
    {
        @Override
        boolean matches(final ParcelUuid[] array, final BluetoothClass bluetoothClass) {
            boolean b = true;
            if (array != null && BluetoothUuid.isUuidPresent(array, BluetoothUuid.PANU)) {
                return true;
            }
            if (bluetoothClass == null || !bluetoothClass.doesClassMatch(4)) {
                b = false;
            }
            return b;
        }
    }
    
    private static final class TransferFilter extends ClassUuidFilter
    {
        @Override
        boolean matches(final ParcelUuid[] array, final BluetoothClass bluetoothClass) {
            boolean b = true;
            if (array != null && BluetoothUuid.isUuidPresent(array, BluetoothUuid.ObexObjectPush)) {
                return true;
            }
            if (bluetoothClass == null || !bluetoothClass.doesClassMatch(2)) {
                b = false;
            }
            return b;
        }
    }
    
    private static final class UnbondedDeviceFilter implements Filter
    {
        @Override
        public boolean matches(final BluetoothDevice bluetoothDevice) {
            return bluetoothDevice.getBondState() != 12;
        }
    }
}
