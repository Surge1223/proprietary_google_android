package com.android.settingslib.bluetooth;

import java.util.List;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothHidHost;

public class HidProfile implements LocalBluetoothProfile
{
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothHidHost mService;
    
    static {
        HidProfile.V = true;
    }
    
    HidProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new HidHostServiceListener(), 4);
    }
    
    public static int getHidClassDrawable(final BluetoothClass bluetoothClass) {
        final int deviceClass = bluetoothClass.getDeviceClass();
        if (deviceClass != 1344) {
            if (deviceClass == 1408) {
                return R.drawable.ic_bt_pointing_hid;
            }
            if (deviceClass != 1472) {
                return R.drawable.ic_bt_misc_hid;
            }
        }
        return R.drawable.ic_lockscreen_ime;
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        return this.mService != null && this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        return this.mService != null && this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (HidProfile.V) {
            Log.d("HidProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(4, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("HidProfile", "Error cleaning up HID proxy", t);
            }
        }
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        final BluetoothHidHost mService = this.mService;
        int connectionState = 0;
        if (mService == null) {
            return 0;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (!connectedDevices.isEmpty() && connectedDevices.get(0).equals((Object)bluetoothDevice)) {
            connectionState = this.mService.getConnectionState(bluetoothDevice);
        }
        return connectionState;
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        if (bluetoothClass == null) {
            return R.drawable.ic_lockscreen_ime;
        }
        return getHidClassDrawable(bluetoothClass);
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_hid;
    }
    
    @Override
    public int getProfileId() {
        return 4;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothHidHost mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "HID";
    }
    
    private final class HidHostServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (HidProfile.V) {
                Log.d("HidProfile", "Bluetooth service connected");
            }
            HidProfile.this.mService = (BluetoothHidHost)bluetoothProfile;
            final List connectedDevices = HidProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = HidProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("HidProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("HidProfile", sb.toString());
                    cachedBluetoothDevice = HidProfile.this.mDeviceManager.addDevice(HidProfile.this.mLocalAdapter, HidProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(HidProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            HidProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (HidProfile.V) {
                Log.d("HidProfile", "Bluetooth service disconnected");
            }
            HidProfile.this.mIsProfileReady = false;
        }
    }
}
