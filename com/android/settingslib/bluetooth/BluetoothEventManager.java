package com.android.settingslib.bluetooth;

import android.bluetooth.BluetoothClass;
import com.android.settingslib.R;
import java.util.Objects;
import android.util.Log;
import java.util.Set;
import java.util.Iterator;
import java.util.HashMap;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import java.util.ArrayList;
import android.os.Handler;
import java.util.Map;
import android.content.Context;
import java.util.Collection;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;

public class BluetoothEventManager
{
    private final IntentFilter mAdapterIntentFilter;
    private final BroadcastReceiver mBroadcastReceiver;
    private final Collection<BluetoothCallback> mCallbacks;
    private Context mContext;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private final Map<String, Handler> mHandlerMap;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final BroadcastReceiver mProfileBroadcastReceiver;
    private final IntentFilter mProfileIntentFilter;
    private LocalBluetoothProfileManager mProfileManager;
    private android.os.Handler mReceiverHandler;
    
    BluetoothEventManager(final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final Context mContext) {
        this.mCallbacks = new ArrayList<BluetoothCallback>();
        this.mBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                final Handler handler = BluetoothEventManager.this.mHandlerMap.get(action);
                if (handler != null) {
                    handler.onReceive(context, intent, bluetoothDevice);
                }
            }
        };
        this.mProfileBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                final BluetoothDevice bluetoothDevice = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                final Handler handler = BluetoothEventManager.this.mHandlerMap.get(action);
                if (handler != null) {
                    handler.onReceive(context, intent, bluetoothDevice);
                }
            }
        };
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mAdapterIntentFilter = new IntentFilter();
        this.mProfileIntentFilter = new IntentFilter();
        this.mHandlerMap = new HashMap<String, Handler>();
        this.mContext = mContext;
        this.addHandler("android.bluetooth.adapter.action.STATE_CHANGED", (Handler)new AdapterStateChangedHandler());
        this.addHandler("android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED", (Handler)new ConnectionStateChangedHandler());
        this.addHandler("android.bluetooth.adapter.action.DISCOVERY_STARTED", (Handler)new ScanningStateChangedHandler(true));
        this.addHandler("android.bluetooth.adapter.action.DISCOVERY_FINISHED", (Handler)new ScanningStateChangedHandler(false));
        this.addHandler("android.bluetooth.device.action.FOUND", (Handler)new DeviceFoundHandler());
        this.addHandler("android.bluetooth.device.action.DISAPPEARED", (Handler)new DeviceDisappearedHandler());
        this.addHandler("android.bluetooth.device.action.NAME_CHANGED", (Handler)new NameChangedHandler());
        this.addHandler("android.bluetooth.device.action.ALIAS_CHANGED", (Handler)new NameChangedHandler());
        this.addHandler("android.bluetooth.device.action.BOND_STATE_CHANGED", (Handler)new BondStateChangedHandler());
        this.addHandler("android.bluetooth.device.action.CLASS_CHANGED", (Handler)new ClassChangedHandler());
        this.addHandler("android.bluetooth.device.action.UUID", (Handler)new UuidChangedHandler());
        this.addHandler("android.bluetooth.device.action.BATTERY_LEVEL_CHANGED", (Handler)new BatteryLevelChangedHandler());
        this.addHandler("android.intent.action.DOCK_EVENT", (Handler)new DockEventHandler());
        this.addHandler("android.bluetooth.a2dp.profile.action.ACTIVE_DEVICE_CHANGED", (Handler)new ActiveDeviceChangedHandler());
        this.addHandler("android.bluetooth.headset.profile.action.ACTIVE_DEVICE_CHANGED", (Handler)new ActiveDeviceChangedHandler());
        this.addHandler("android.bluetooth.hearingaid.profile.action.ACTIVE_DEVICE_CHANGED", (Handler)new ActiveDeviceChangedHandler());
        this.addHandler("android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED", (Handler)new AudioModeChangedHandler());
        this.addHandler("android.intent.action.PHONE_STATE", (Handler)new AudioModeChangedHandler());
        this.mContext.registerReceiver(this.mBroadcastReceiver, this.mAdapterIntentFilter, (String)null, this.mReceiverHandler);
        this.mContext.registerReceiver(this.mProfileBroadcastReceiver, this.mProfileIntentFilter, (String)null, this.mReceiverHandler);
    }
    
    private void addHandler(final String s, final Handler handler) {
        this.mHandlerMap.put(s, handler);
        this.mAdapterIntentFilter.addAction(s);
    }
    
    private void dispatchActiveDeviceChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        this.mDeviceManager.onActiveDeviceChanged(cachedBluetoothDevice, n);
        synchronized (this.mCallbacks) {
            final Iterator<BluetoothCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onActiveDeviceChanged(cachedBluetoothDevice, n);
            }
        }
    }
    
    private void dispatchAudioModeChanged() {
        this.mDeviceManager.dispatchAudioModeChanged();
        synchronized (this.mCallbacks) {
            final Iterator<BluetoothCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onAudioModeChanged();
            }
        }
    }
    
    private void dispatchConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n) {
        synchronized (this.mCallbacks) {
            final Iterator<BluetoothCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onConnectionStateChanged(cachedBluetoothDevice, n);
            }
        }
    }
    
    void addProfileHandler(final String s, final Handler handler) {
        this.mHandlerMap.put(s, handler);
        this.mProfileIntentFilter.addAction(s);
    }
    
    void dispatchDeviceAdded(final CachedBluetoothDevice cachedBluetoothDevice) {
        synchronized (this.mCallbacks) {
            final Iterator<BluetoothCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDeviceAdded(cachedBluetoothDevice);
            }
        }
    }
    
    void dispatchDeviceRemoved(final CachedBluetoothDevice cachedBluetoothDevice) {
        synchronized (this.mCallbacks) {
            final Iterator<BluetoothCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDeviceDeleted(cachedBluetoothDevice);
            }
        }
    }
    
    void dispatchProfileConnectionStateChanged(final CachedBluetoothDevice cachedBluetoothDevice, final int n, final int n2) {
        synchronized (this.mCallbacks) {
            final Iterator<BluetoothCallback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onProfileConnectionStateChanged(cachedBluetoothDevice, n, n2);
            }
            // monitorexit(this.mCallbacks)
            this.mDeviceManager.onProfileConnectionStateChanged(cachedBluetoothDevice, n, n2);
        }
    }
    
    boolean readPairedDevices() {
        final Set<BluetoothDevice> bondedDevices = this.mLocalAdapter.getBondedDevices();
        if (bondedDevices == null) {
            return false;
        }
        boolean b = false;
        for (final BluetoothDevice bluetoothDevice : bondedDevices) {
            if (this.mDeviceManager.findDevice(bluetoothDevice) == null) {
                this.dispatchDeviceAdded(this.mDeviceManager.addDevice(this.mLocalAdapter, this.mProfileManager, bluetoothDevice));
                b = true;
            }
        }
        return b;
    }
    
    public void registerCallback(final BluetoothCallback bluetoothCallback) {
        synchronized (this.mCallbacks) {
            this.mCallbacks.add(bluetoothCallback);
        }
    }
    
    void registerProfileIntentReceiver() {
        this.mContext.registerReceiver(this.mProfileBroadcastReceiver, this.mProfileIntentFilter, (String)null, this.mReceiverHandler);
    }
    
    void setProfileManager(final LocalBluetoothProfileManager mProfileManager) {
        this.mProfileManager = mProfileManager;
    }
    
    public void unregisterCallback(final BluetoothCallback bluetoothCallback) {
        synchronized (this.mCallbacks) {
            this.mCallbacks.remove(bluetoothCallback);
        }
    }
    
    private class ActiveDeviceChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            final String action = intent.getAction();
            if (action == null) {
                Log.w("BluetoothEventManager", "ActiveDeviceChangedHandler: action is null");
                return;
            }
            final CachedBluetoothDevice device = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice);
            int n;
            if (Objects.equals(action, "android.bluetooth.a2dp.profile.action.ACTIVE_DEVICE_CHANGED")) {
                n = 2;
            }
            else if (Objects.equals(action, "android.bluetooth.headset.profile.action.ACTIVE_DEVICE_CHANGED")) {
                n = 1;
            }
            else {
                if (!Objects.equals(action, "android.bluetooth.hearingaid.profile.action.ACTIVE_DEVICE_CHANGED")) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("ActiveDeviceChangedHandler: unknown action ");
                    sb.append(action);
                    Log.w("BluetoothEventManager", sb.toString());
                    return;
                }
                n = 21;
            }
            BluetoothEventManager.this.dispatchActiveDeviceChanged(device, n);
        }
    }
    
    private class AdapterStateChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            final int intExtra = intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE);
            if (intExtra == 10) {
                context.unregisterReceiver(BluetoothEventManager.this.mProfileBroadcastReceiver);
                BluetoothEventManager.this.registerProfileIntentReceiver();
            }
            BluetoothEventManager.this.mLocalAdapter.setBluetoothStateInt(intExtra);
            synchronized (BluetoothEventManager.this.mCallbacks) {
                final Iterator<BluetoothCallback> iterator = BluetoothEventManager.this.mCallbacks.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onBluetoothStateChanged(intExtra);
                }
                // monitorexit(BluetoothEventManager.access$1500(this.this$0))
                BluetoothEventManager.this.mDeviceManager.onBluetoothStateChanged(intExtra);
            }
        }
    }
    
    private class AudioModeChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            if (intent.getAction() == null) {
                Log.w("BluetoothEventManager", "AudioModeChangedHandler() action is null");
                return;
            }
            BluetoothEventManager.this.dispatchAudioModeChanged();
        }
    }
    
    private class BatteryLevelChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            final CachedBluetoothDevice device = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice);
            if (device != null) {
                device.refresh();
            }
        }
    }
    
    private class BondStateChangedHandler implements Handler
    {
        private void showUnbondMessage(final Context context, final String s, int n) {
            switch (n) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("showUnbondMessage: Not displaying any message for reason: ");
                    sb.append(n);
                    Log.w("BluetoothEventManager", sb.toString());
                    return;
                }
                case 5:
                case 6:
                case 7:
                case 8: {
                    n = R.string.bluetooth_pairing_error_message;
                    break;
                }
                case 4: {
                    n = R.string.bluetooth_pairing_device_down_error_message;
                    break;
                }
                case 2: {
                    n = R.string.bluetooth_pairing_rejected_error_message;
                    break;
                }
                case 1: {
                    n = R.string.bluetooth_pairing_pin_error_message;
                    break;
                }
            }
            Utils.showError(context, s, n);
        }
        
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            if (bluetoothDevice == null) {
                Log.e("BluetoothEventManager", "ACTION_BOND_STATE_CHANGED with no EXTRA_DEVICE");
                return;
            }
            final int intExtra = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", Integer.MIN_VALUE);
            CachedBluetoothDevice cachedBluetoothDevice = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice);
            CachedBluetoothDevice addDevice;
            if ((addDevice = cachedBluetoothDevice) == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("CachedBluetoothDevice for device ");
                sb.append(bluetoothDevice);
                sb.append(" not found, calling readPairedDevices().");
                Log.w("BluetoothEventManager", sb.toString());
                if (BluetoothEventManager.this.readPairedDevices()) {
                    cachedBluetoothDevice = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice);
                }
                if ((addDevice = cachedBluetoothDevice) == null) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("Got bonding state changed for ");
                    sb2.append(bluetoothDevice);
                    sb2.append(", but we have no record of that device.");
                    Log.w("BluetoothEventManager", sb2.toString());
                    addDevice = BluetoothEventManager.this.mDeviceManager.addDevice(BluetoothEventManager.this.mLocalAdapter, BluetoothEventManager.this.mProfileManager, bluetoothDevice);
                    BluetoothEventManager.this.dispatchDeviceAdded(addDevice);
                }
            }
            synchronized (BluetoothEventManager.this.mCallbacks) {
                final Iterator<BluetoothCallback> iterator = BluetoothEventManager.this.mCallbacks.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onDeviceBondStateChanged(addDevice, intExtra);
                }
                // monitorexit(BluetoothEventManager.access$1500(this.this$0))
                addDevice.onBondingStateChanged(intExtra);
                if (intExtra == 10) {
                    if (addDevice.getHiSyncId() != 0L) {
                        BluetoothEventManager.this.mDeviceManager.onDeviceUnpaired(addDevice);
                    }
                    this.showUnbondMessage(context, addDevice.getName(), intent.getIntExtra("android.bluetooth.device.extra.REASON", Integer.MIN_VALUE));
                }
            }
        }
    }
    
    private class ClassChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            BluetoothEventManager.this.mDeviceManager.onBtClassChanged(bluetoothDevice);
        }
    }
    
    private class ConnectionStateChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            BluetoothEventManager.this.dispatchConnectionStateChanged(BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice), intent.getIntExtra("android.bluetooth.adapter.extra.CONNECTION_STATE", Integer.MIN_VALUE));
        }
    }
    
    private class DeviceDisappearedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            final CachedBluetoothDevice device = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice);
            if (device == null) {
                final StringBuilder sb = new StringBuilder();
                sb.append("received ACTION_DISAPPEARED for an unknown device: ");
                sb.append(bluetoothDevice);
                Log.w("BluetoothEventManager", sb.toString());
                return;
            }
            if (CachedBluetoothDeviceManager.onDeviceDisappeared(device)) {
                synchronized (BluetoothEventManager.this.mCallbacks) {
                    final Iterator<BluetoothCallback> iterator = BluetoothEventManager.this.mCallbacks.iterator();
                    while (iterator.hasNext()) {
                        iterator.next().onDeviceDeleted(device);
                    }
                }
            }
        }
    }
    
    private class DeviceFoundHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            final short shortExtra = intent.getShortExtra("android.bluetooth.device.extra.RSSI", (short)(-32768));
            final BluetoothClass btClass = (BluetoothClass)intent.getParcelableExtra("android.bluetooth.device.extra.CLASS");
            final String stringExtra = intent.getStringExtra("android.bluetooth.device.extra.NAME");
            CachedBluetoothDevice cachedBluetoothDevice;
            if ((cachedBluetoothDevice = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                cachedBluetoothDevice = BluetoothEventManager.this.mDeviceManager.addDevice(BluetoothEventManager.this.mLocalAdapter, BluetoothEventManager.this.mProfileManager, bluetoothDevice);
                final StringBuilder sb = new StringBuilder();
                sb.append("DeviceFoundHandler created new CachedBluetoothDevice: ");
                sb.append(cachedBluetoothDevice);
                Log.d("BluetoothEventManager", sb.toString());
            }
            cachedBluetoothDevice.setRssi(shortExtra);
            cachedBluetoothDevice.setBtClass(btClass);
            cachedBluetoothDevice.setNewName(stringExtra);
            cachedBluetoothDevice.setJustDiscovered(true);
        }
    }
    
    private class DockEventHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            if (intent.getIntExtra("android.intent.extra.DOCK_STATE", 1) == 0 && bluetoothDevice != null && bluetoothDevice.getBondState() == 10) {
                final CachedBluetoothDevice device = BluetoothEventManager.this.mDeviceManager.findDevice(bluetoothDevice);
                if (device != null) {
                    device.setJustDiscovered(false);
                }
            }
        }
    }
    
    interface Handler
    {
        void onReceive(final Context p0, final Intent p1, final BluetoothDevice p2);
    }
    
    private class NameChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            BluetoothEventManager.this.mDeviceManager.onDeviceNameUpdated(bluetoothDevice);
        }
    }
    
    private class ScanningStateChangedHandler implements Handler
    {
        private final boolean mStarted;
        
        ScanningStateChangedHandler(final boolean mStarted) {
            this.mStarted = mStarted;
        }
        
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            synchronized (BluetoothEventManager.this.mCallbacks) {
                final Iterator<BluetoothCallback> iterator = BluetoothEventManager.this.mCallbacks.iterator();
                while (iterator.hasNext()) {
                    iterator.next().onScanningStateChanged(this.mStarted);
                }
                // monitorexit(BluetoothEventManager.access$1500(this.this$0))
                BluetoothEventManager.this.mDeviceManager.onScanningStateChanged(this.mStarted);
            }
        }
    }
    
    private class UuidChangedHandler implements Handler
    {
        @Override
        public void onReceive(final Context context, final Intent intent, final BluetoothDevice bluetoothDevice) {
            BluetoothEventManager.this.mDeviceManager.onUuidChanged(bluetoothDevice);
        }
    }
}
