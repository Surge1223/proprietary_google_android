package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.Iterator;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothHeadsetClient;
import android.os.ParcelUuid;

final class HfpClientProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] SRC_UUIDS;
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothHeadsetClient mService;
    
    static {
        HfpClientProfile.V = false;
        SRC_UUIDS = new ParcelUuid[] { BluetoothUuid.HSP_AG, BluetoothUuid.Handsfree_AG };
    }
    
    HfpClientProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new HfpClientServiceListener(), 16);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        final List<BluetoothDevice> connectedDevices = this.getConnectedDevices();
        if (connectedDevices != null) {
            final Iterator<BluetoothDevice> iterator = connectedDevices.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals((Object)bluetoothDevice)) {
                    Log.d("HfpClientProfile", "Ignoring Connect");
                    return true;
                }
            }
        }
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 100) {
            this.mService.setPriority(bluetoothDevice, 100);
        }
        return this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (HfpClientProfile.V) {
            Log.d("HfpClientProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(16, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("HfpClientProfile", "Error cleaning up HfpClient proxy", t);
            }
        }
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_headset_hfp;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_headset;
    }
    
    @Override
    public int getProfileId() {
        return 16;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothHeadsetClient mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "HEADSET_CLIENT";
    }
    
    private final class HfpClientServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (HfpClientProfile.V) {
                Log.d("HfpClientProfile", "Bluetooth service connected");
            }
            HfpClientProfile.this.mService = (BluetoothHeadsetClient)bluetoothProfile;
            final List connectedDevices = HfpClientProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = HfpClientProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("HfpClient profile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("HfpClientProfile", sb.toString());
                    cachedBluetoothDevice = HfpClientProfile.this.mDeviceManager.addDevice(HfpClientProfile.this.mLocalAdapter, HfpClientProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(HfpClientProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            HfpClientProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (HfpClientProfile.V) {
                Log.d("HfpClientProfile", "Bluetooth service disconnected");
            }
            HfpClientProfile.this.mIsProfileReady = false;
        }
    }
}
