package com.android.settingslib.bluetooth;

import com.android.internal.annotations.VisibleForTesting;
import java.util.Arrays;
import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.Iterator;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.bluetooth.BluetoothUuid;
import com.android.settingslib.wrapper.BluetoothA2dpWrapper;
import android.bluetooth.BluetoothA2dp;
import android.content.Context;
import android.os.ParcelUuid;
import android.bluetooth.BluetoothCodecConfig;
import java.util.Comparator;

public final class _$$Lambda$A2dpProfile$exPXCssgW4cryyr_RqCY5K_rQFI implements Comparator
{
    @Override
    public final int compare(final Object o, final Object o2) {
        return A2dpProfile.lambda$getHighQualityAudioOptionLabel$0((BluetoothCodecConfig)o, (BluetoothCodecConfig)o2);
    }
}
