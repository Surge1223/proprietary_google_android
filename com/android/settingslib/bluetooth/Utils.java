package com.android.settingslib.bluetooth;

import java.util.Iterator;
import android.bluetooth.BluetoothClass;
import com.android.settingslib.R;
import android.util.Pair;
import com.android.settingslib.graph.BluetoothDeviceLayerDrawable;
import android.graphics.drawable.Drawable;
import android.content.Context;

public class Utils
{
    private static ErrorListener sErrorListener;
    
    public static Drawable getBluetoothDrawable(final Context context, final int n, final int n2, final float n3) {
        if (n2 != -1) {
            return (Drawable)BluetoothDeviceLayerDrawable.createLayerDrawable(context, n, n2, n3);
        }
        return context.getDrawable(n);
    }
    
    public static Pair<Drawable, String> getBtClassDrawableWithDescription(final Context context, final CachedBluetoothDevice cachedBluetoothDevice) {
        return getBtClassDrawableWithDescription(context, cachedBluetoothDevice, 1.0f);
    }
    
    public static Pair<Drawable, String> getBtClassDrawableWithDescription(final Context context, final CachedBluetoothDevice cachedBluetoothDevice, final float n) {
        final BluetoothClass btClass = cachedBluetoothDevice.getBtClass();
        final int batteryLevel = cachedBluetoothDevice.getBatteryLevel();
        if (btClass != null) {
            final int majorDeviceClass = btClass.getMajorDeviceClass();
            if (majorDeviceClass == 256) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, R.drawable.ic_bt_laptop, batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_computer));
            }
            if (majorDeviceClass == 512) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, R.drawable.ic_bt_cellphone, batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_phone));
            }
            if (majorDeviceClass == 1280) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, HidProfile.getHidClassDrawable(btClass), batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_input_peripheral));
            }
            if (majorDeviceClass == 1536) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, R.drawable.ic_settings_print, batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_imaging));
            }
        }
        final Iterator<LocalBluetoothProfile> iterator = cachedBluetoothDevice.getProfiles().iterator();
        while (iterator.hasNext()) {
            final int drawableResource = iterator.next().getDrawableResource(btClass);
            if (drawableResource != 0) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, drawableResource, batteryLevel, n), (Object)null);
            }
        }
        if (btClass != null) {
            if (btClass.doesClassMatch(0)) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, R.drawable.ic_bt_headset_hfp, batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_headset));
            }
            if (btClass.doesClassMatch(1)) {
                return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, R.drawable.ic_bt_headphones_a2dp, batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_headphone));
            }
        }
        return (Pair<Drawable, String>)new Pair((Object)getBluetoothDrawable(context, R.drawable.ic_settings_bluetooth, batteryLevel, n), (Object)context.getString(R.string.bluetooth_talkback_bluetooth));
    }
    
    public static int getConnectionStateSummary(final int n) {
        switch (n) {
            default: {
                return 0;
            }
            case 3: {
                return R.string.bluetooth_disconnecting;
            }
            case 2: {
                return R.string.bluetooth_connected;
            }
            case 1: {
                return R.string.bluetooth_connecting;
            }
            case 0: {
                return R.string.bluetooth_disconnected;
            }
        }
    }
    
    public static void setErrorListener(final ErrorListener sErrorListener) {
        Utils.sErrorListener = sErrorListener;
    }
    
    static void showError(final Context context, final String s, final int n) {
        if (Utils.sErrorListener != null) {
            Utils.sErrorListener.onShowError(context, s, n);
        }
    }
    
    public interface ErrorListener
    {
        void onShowError(final Context p0, final String p1, final int p2);
    }
}
