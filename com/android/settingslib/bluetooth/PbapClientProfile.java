package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.ArrayList;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import java.util.Iterator;
import java.util.List;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothPbapClient;
import android.os.ParcelUuid;

public final class PbapClientProfile implements LocalBluetoothProfile
{
    static final ParcelUuid[] SRC_UUIDS;
    private static boolean V;
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothPbapClient mService;
    
    static {
        PbapClientProfile.V = false;
        SRC_UUIDS = new ParcelUuid[] { BluetoothUuid.PBAP_PSE };
    }
    
    PbapClientProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        this.mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new PbapClientServiceListener(), 17);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        if (PbapClientProfile.V) {
            Log.d("PbapClientProfile", "PBAPClientProfile got connect request");
        }
        if (this.mService == null) {
            return false;
        }
        final List<BluetoothDevice> connectedDevices = this.getConnectedDevices();
        if (connectedDevices != null) {
            final Iterator<BluetoothDevice> iterator = connectedDevices.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().equals((Object)bluetoothDevice)) {
                    Log.d("PbapClientProfile", "Ignoring Connect");
                    return true;
                }
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("PBAPClientProfile attempting to connect to ");
        sb.append(bluetoothDevice.getAddress());
        Log.d("PbapClientProfile", sb.toString());
        return this.mService.connect(bluetoothDevice);
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        if (PbapClientProfile.V) {
            Log.d("PbapClientProfile", "PBAPClientProfile got disconnect request");
        }
        return this.mService != null && this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (PbapClientProfile.V) {
            Log.d("PbapClientProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(17, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("PbapClientProfile", "Error cleaning up PBAP Client proxy", t);
            }
        }
    }
    
    public List<BluetoothDevice> getConnectedDevices() {
        if (this.mService == null) {
            return new ArrayList<BluetoothDevice>(0);
        }
        return (List<BluetoothDevice>)this.mService.getDevicesMatchingConnectionStates(new int[] { 2, 1, 3 });
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        return this.mService.getConnectionState(bluetoothDevice);
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_cellphone;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_pbap;
    }
    
    @Override
    public int getProfileId() {
        return 17;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return true;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        final BluetoothPbapClient mService = this.mService;
        boolean b = false;
        if (mService == null) {
            return false;
        }
        if (this.mService.getPriority(bluetoothDevice) > 0) {
            b = true;
        }
        return b;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (this.mService == null) {
            return;
        }
        if (b) {
            if (this.mService.getPriority(bluetoothDevice) < 100) {
                this.mService.setPriority(bluetoothDevice, 100);
            }
        }
        else {
            this.mService.setPriority(bluetoothDevice, 0);
        }
    }
    
    @Override
    public String toString() {
        return "PbapClient";
    }
    
    private final class PbapClientServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            if (PbapClientProfile.V) {
                Log.d("PbapClientProfile", "Bluetooth service connected");
            }
            PbapClientProfile.this.mService = (BluetoothPbapClient)bluetoothProfile;
            final List connectedDevices = PbapClientProfile.this.mService.getConnectedDevices();
            while (!connectedDevices.isEmpty()) {
                final BluetoothDevice bluetoothDevice = connectedDevices.remove(0);
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = PbapClientProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("PbapClientProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("PbapClientProfile", sb.toString());
                    cachedBluetoothDevice = PbapClientProfile.this.mDeviceManager.addDevice(PbapClientProfile.this.mLocalAdapter, PbapClientProfile.this.mProfileManager, bluetoothDevice);
                }
                cachedBluetoothDevice.onProfileStateChanged(PbapClientProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            PbapClientProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            if (PbapClientProfile.V) {
                Log.d("PbapClientProfile", "Bluetooth service disconnected");
            }
            PbapClientProfile.this.mIsProfileReady = false;
        }
    }
}
