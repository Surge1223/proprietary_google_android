package com.android.settingslib.bluetooth;

import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothPbap$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothUuid;
import android.bluetooth.BluetoothPbap;
import android.os.ParcelUuid;
import com.android.internal.annotations.VisibleForTesting;

public class PbapServerProfile implements LocalBluetoothProfile
{
    @VisibleForTesting
    public static final String NAME = "PBAP Server";
    static final ParcelUuid[] PBAB_CLIENT_UUIDS;
    private static boolean V;
    private boolean mIsProfileReady;
    private BluetoothPbap mService;
    
    static {
        PbapServerProfile.V = true;
        PBAB_CLIENT_UUIDS = new ParcelUuid[] { BluetoothUuid.HSP, BluetoothUuid.Handsfree, BluetoothUuid.PBAP_PCE };
    }
    
    PbapServerProfile(final Context context) {
        new BluetoothPbap(context, (BluetoothPbap$ServiceListener)new PbapServiceListener());
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        return false;
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        return this.mService != null && this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        if (PbapServerProfile.V) {
            Log.d("PbapServerProfile", "finalize()");
        }
        if (this.mService != null) {
            try {
                this.mService.close();
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("PbapServerProfile", "Error cleaning up PBAP proxy", t);
            }
        }
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        if (this.mService == null) {
            return 0;
        }
        if (this.mService.isConnected(bluetoothDevice)) {
            return 2;
        }
        return 0;
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_cellphone;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_pbap;
    }
    
    @Override
    public int getProfileId() {
        return 6;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return false;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        return false;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
    }
    
    @Override
    public String toString() {
        return "PBAP Server";
    }
    
    private final class PbapServiceListener implements BluetoothPbap$ServiceListener
    {
        public void onServiceConnected(final BluetoothPbap bluetoothPbap) {
            if (PbapServerProfile.V) {
                Log.d("PbapServerProfile", "Bluetooth service connected");
            }
            PbapServerProfile.this.mService = bluetoothPbap;
            PbapServerProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected() {
            if (PbapServerProfile.V) {
                Log.d("PbapServerProfile", "Bluetooth service disconnected");
            }
            PbapServerProfile.this.mIsProfileReady = false;
        }
    }
}
