package com.android.settingslib.bluetooth;

import java.util.Collections;
import com.android.settingslib.R;
import android.os.SystemClock;
import android.os.ParcelUuid;
import android.bluetooth.BluetoothUuid;
import android.content.SharedPreferences$Editor;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import java.util.Collection;
import android.bluetooth.BluetoothClass;
import android.media.AudioManager;

public class CachedBluetoothDevice implements Comparable<CachedBluetoothDevice>
{
    private final AudioManager mAudioManager;
    private BluetoothClass mBtClass;
    private final Collection<Callback> mCallbacks;
    private long mConnectAttempted;
    private final Context mContext;
    private final BluetoothDevice mDevice;
    private long mHiSyncId;
    private boolean mIsActiveDeviceA2dp;
    private boolean mIsActiveDeviceHeadset;
    private boolean mIsActiveDeviceHearingAid;
    private boolean mIsConnectingErrorPossible;
    private boolean mJustDiscovered;
    private final LocalBluetoothAdapter mLocalAdapter;
    private boolean mLocalNapRoleConnected;
    private int mMessageRejectionCount;
    private String mName;
    private HashMap<LocalBluetoothProfile, Integer> mProfileConnectionState;
    private final LocalBluetoothProfileManager mProfileManager;
    private final List<LocalBluetoothProfile> mProfiles;
    private final List<LocalBluetoothProfile> mRemovedProfiles;
    private short mRssi;
    
    CachedBluetoothDevice(final Context mContext, final LocalBluetoothAdapter mLocalAdapter, final LocalBluetoothProfileManager mProfileManager, final BluetoothDevice mDevice) {
        this.mProfiles = new ArrayList<LocalBluetoothProfile>();
        this.mRemovedProfiles = new ArrayList<LocalBluetoothProfile>();
        this.mCallbacks = new ArrayList<Callback>();
        this.mIsActiveDeviceA2dp = false;
        this.mIsActiveDeviceHeadset = false;
        this.mIsActiveDeviceHearingAid = false;
        this.mContext = mContext;
        this.mLocalAdapter = mLocalAdapter;
        this.mProfileManager = mProfileManager;
        this.mAudioManager = (AudioManager)mContext.getSystemService((Class)AudioManager.class);
        this.mDevice = mDevice;
        this.mProfileConnectionState = new HashMap<LocalBluetoothProfile, Integer>();
        this.fillData();
        this.mHiSyncId = 0L;
    }
    
    private void connectAutoConnectableProfiles() {
        if (!this.ensurePaired()) {
            return;
        }
        this.mIsConnectingErrorPossible = true;
        for (final LocalBluetoothProfile localBluetoothProfile : this.mProfiles) {
            if (localBluetoothProfile.isAutoConnectable()) {
                localBluetoothProfile.setPreferred(this.mDevice, true);
                this.connectInt(localBluetoothProfile);
            }
        }
    }
    
    private void connectWithoutResettingTimer(final boolean b) {
        if (this.mProfiles.isEmpty()) {
            Log.d("CachedBluetoothDevice", "No profiles. Maybe we will connect later");
            return;
        }
        this.mIsConnectingErrorPossible = true;
        int n = 0;
        for (final LocalBluetoothProfile localBluetoothProfile : this.mProfiles) {
            int n2 = 0;
            Label_0119: {
                if (b) {
                    n2 = n;
                    if (!localBluetoothProfile.isConnectable()) {
                        break Label_0119;
                    }
                }
                else {
                    n2 = n;
                    if (!localBluetoothProfile.isAutoConnectable()) {
                        break Label_0119;
                    }
                }
                n2 = n;
                if (localBluetoothProfile.isPreferred(this.mDevice)) {
                    n2 = n + 1;
                    this.connectInt(localBluetoothProfile);
                }
            }
            n = n2;
        }
        if (n == 0) {
            this.connectAutoConnectableProfiles();
        }
    }
    
    private String describe(final LocalBluetoothProfile localBluetoothProfile) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Address:");
        sb.append(this.mDevice);
        if (localBluetoothProfile != null) {
            sb.append(" Profile:");
            sb.append(localBluetoothProfile);
        }
        return sb.toString();
    }
    
    private void dispatchAttributesChanged() {
        synchronized (this.mCallbacks) {
            final Iterator<Callback> iterator = this.mCallbacks.iterator();
            while (iterator.hasNext()) {
                iterator.next().onDeviceAttributesChanged();
            }
        }
    }
    
    private boolean ensurePaired() {
        if (this.getBondState() == 10) {
            this.startPairing();
            return false;
        }
        return true;
    }
    
    private void fetchActiveDevices() {
        final A2dpProfile a2dpProfile = this.mProfileManager.getA2dpProfile();
        if (a2dpProfile != null) {
            this.mIsActiveDeviceA2dp = this.mDevice.equals((Object)a2dpProfile.getActiveDevice());
        }
        final HeadsetProfile headsetProfile = this.mProfileManager.getHeadsetProfile();
        if (headsetProfile != null) {
            this.mIsActiveDeviceHeadset = this.mDevice.equals((Object)headsetProfile.getActiveDevice());
        }
        final HearingAidProfile hearingAidProfile = this.mProfileManager.getHearingAidProfile();
        if (hearingAidProfile != null) {
            this.mIsActiveDeviceHearingAid = hearingAidProfile.getActiveDevices().contains(this.mDevice);
        }
    }
    
    private void fetchBtClass() {
        this.mBtClass = this.mDevice.getBluetoothClass();
    }
    
    private void fetchMessageRejectionCount() {
        this.mMessageRejectionCount = this.mContext.getSharedPreferences("bluetooth_message_reject", 0).getInt(this.mDevice.getAddress(), 0);
    }
    
    private void fetchName() {
        this.mName = this.mDevice.getAliasName();
        if (TextUtils.isEmpty((CharSequence)this.mName)) {
            this.mName = this.mDevice.getAddress();
        }
    }
    
    private void fillData() {
        this.fetchName();
        this.fetchBtClass();
        this.updateProfiles();
        this.fetchActiveDevices();
        this.migratePhonebookPermissionChoice();
        this.migrateMessagePermissionChoice();
        this.fetchMessageRejectionCount();
        this.dispatchAttributesChanged();
    }
    
    private void migrateMessagePermissionChoice() {
        final SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("bluetooth_message_permission", 0);
        if (!sharedPreferences.contains(this.mDevice.getAddress())) {
            return;
        }
        if (this.mDevice.getMessageAccessPermission() == 0) {
            final int int1 = sharedPreferences.getInt(this.mDevice.getAddress(), 0);
            if (int1 == 1) {
                this.mDevice.setMessageAccessPermission(1);
            }
            else if (int1 == 2) {
                this.mDevice.setMessageAccessPermission(2);
            }
        }
        final SharedPreferences$Editor edit = sharedPreferences.edit();
        edit.remove(this.mDevice.getAddress());
        edit.commit();
    }
    
    private void migratePhonebookPermissionChoice() {
        final SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("bluetooth_phonebook_permission", 0);
        if (!sharedPreferences.contains(this.mDevice.getAddress())) {
            return;
        }
        if (this.mDevice.getPhonebookAccessPermission() == 0) {
            final int int1 = sharedPreferences.getInt(this.mDevice.getAddress(), 0);
            if (int1 == 1) {
                this.mDevice.setPhonebookAccessPermission(1);
            }
            else if (int1 == 2) {
                this.mDevice.setPhonebookAccessPermission(2);
            }
        }
        final SharedPreferences$Editor edit = sharedPreferences.edit();
        edit.remove(this.mDevice.getAddress());
        edit.commit();
    }
    
    private void processPhonebookAccess() {
        if (this.mDevice.getBondState() != 12) {
            return;
        }
        if (BluetoothUuid.containsAnyUuid(this.mDevice.getUuids(), PbapServerProfile.PBAB_CLIENT_UUIDS) && this.getPhonebookPermissionChoice() == 0) {
            if (this.mDevice.getBluetoothClass().getDeviceClass() != 1032 && this.mDevice.getBluetoothClass().getDeviceClass() != 1028) {
                this.setPhonebookPermissionChoice(2);
            }
            else {
                this.setPhonebookPermissionChoice(1);
            }
        }
    }
    
    private void saveMessageRejectionCount() {
        final SharedPreferences$Editor edit = this.mContext.getSharedPreferences("bluetooth_message_reject", 0).edit();
        if (this.mMessageRejectionCount == 0) {
            edit.remove(this.mDevice.getAddress());
        }
        else {
            edit.putInt(this.mDevice.getAddress(), this.mMessageRejectionCount);
        }
        edit.commit();
    }
    
    private boolean updateProfiles() {
        final ParcelUuid[] uuids = this.mDevice.getUuids();
        if (uuids == null) {
            return false;
        }
        final ParcelUuid[] uuids2 = this.mLocalAdapter.getUuids();
        if (uuids2 == null) {
            return false;
        }
        this.processPhonebookAccess();
        this.mProfileManager.updateProfiles(uuids, uuids2, this.mProfiles, this.mRemovedProfiles, this.mLocalNapRoleConnected, this.mDevice);
        return true;
    }
    
    public boolean checkAndIncreaseMessageRejectionCount() {
        final int mMessageRejectionCount = this.mMessageRejectionCount;
        boolean b = true;
        if (mMessageRejectionCount < 2) {
            ++this.mMessageRejectionCount;
            this.saveMessageRejectionCount();
        }
        if (this.mMessageRejectionCount < 2) {
            b = false;
        }
        return b;
    }
    
    public void clearProfileConnectionState() {
        final StringBuilder sb = new StringBuilder();
        sb.append(" Clearing all connection state for dev:");
        sb.append(this.mDevice.getName());
        Log.d("CachedBluetoothDevice", sb.toString());
        final Iterator<LocalBluetoothProfile> iterator = this.getProfiles().iterator();
        while (iterator.hasNext()) {
            this.mProfileConnectionState.put(iterator.next(), 0);
        }
    }
    
    @Override
    public int compareTo(final CachedBluetoothDevice cachedBluetoothDevice) {
        final boolean b = cachedBluetoothDevice.isConnected() - this.isConnected();
        if (b) {
            return b ? 1 : 0;
        }
        final int bondState = cachedBluetoothDevice.getBondState();
        int n = 0;
        int n2;
        if (bondState == 12) {
            n2 = 1;
        }
        else {
            n2 = 0;
        }
        if (this.getBondState() == 12) {
            n = 1;
        }
        final int n3 = n2 - n;
        if (n3 != 0) {
            return n3;
        }
        final boolean b2 = cachedBluetoothDevice.mJustDiscovered - this.mJustDiscovered;
        if (b2) {
            return b2 ? 1 : 0;
        }
        final short n4 = (short)(cachedBluetoothDevice.mRssi - this.mRssi);
        if (n4 != 0) {
            return n4;
        }
        return this.mName.compareTo(cachedBluetoothDevice.mName);
    }
    
    public void connect(final boolean b) {
        if (!this.ensurePaired()) {
            return;
        }
        this.mConnectAttempted = SystemClock.elapsedRealtime();
        this.connectWithoutResettingTimer(b);
    }
    
    void connectInt(final LocalBluetoothProfile localBluetoothProfile) {
        synchronized (this) {
            if (!this.ensurePaired()) {
                return;
            }
            if (localBluetoothProfile.connect(this.mDevice)) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Command sent successfully:CONNECT ");
                sb.append(this.describe(localBluetoothProfile));
                Log.d("CachedBluetoothDevice", sb.toString());
                return;
            }
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Failed to connect ");
            sb2.append(localBluetoothProfile.toString());
            sb2.append(" to ");
            sb2.append(this.mName);
            Log.i("CachedBluetoothDevice", sb2.toString());
        }
    }
    
    public void connectProfile(final LocalBluetoothProfile localBluetoothProfile) {
        this.mConnectAttempted = SystemClock.elapsedRealtime();
        this.mIsConnectingErrorPossible = true;
        this.connectInt(localBluetoothProfile);
        this.refresh();
    }
    
    public void disconnect() {
        final Iterator<LocalBluetoothProfile> iterator = this.mProfiles.iterator();
        while (iterator.hasNext()) {
            this.disconnect(iterator.next());
        }
        final PbapServerProfile pbapProfile = this.mProfileManager.getPbapProfile();
        if (pbapProfile.getConnectionStatus(this.mDevice) == 2) {
            pbapProfile.disconnect(this.mDevice);
        }
    }
    
    public void disconnect(final LocalBluetoothProfile localBluetoothProfile) {
        if (localBluetoothProfile.disconnect(this.mDevice)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Command sent successfully:DISCONNECT ");
            sb.append(this.describe(localBluetoothProfile));
            Log.d("CachedBluetoothDevice", sb.toString());
        }
    }
    
    @Override
    public boolean equals(final Object o) {
        return o != null && o instanceof CachedBluetoothDevice && this.mDevice.equals((Object)((CachedBluetoothDevice)o).mDevice);
    }
    
    public String getAddress() {
        return this.mDevice.getAddress();
    }
    
    public int getBatteryLevel() {
        return this.mDevice.getBatteryLevel();
    }
    
    public int getBondState() {
        return this.mDevice.getBondState();
    }
    
    public BluetoothClass getBtClass() {
        return this.mBtClass;
    }
    
    public List<LocalBluetoothProfile> getConnectableProfiles() {
        final ArrayList<LocalBluetoothProfile> list = new ArrayList<LocalBluetoothProfile>();
        for (final LocalBluetoothProfile localBluetoothProfile : this.mProfiles) {
            if (localBluetoothProfile.isConnectable()) {
                list.add(localBluetoothProfile);
            }
        }
        return list;
    }
    
    public String getConnectionSummary() {
        int n = 0;
        int n2 = 1;
        int n3 = 1;
        int n4 = 1;
        for (final LocalBluetoothProfile localBluetoothProfile : this.getProfiles()) {
            final int profileConnectionState = this.getProfileConnectionState(localBluetoothProfile);
            int n5 = 0;
            int n6 = 0;
            int n7 = 0;
            int n8 = 0;
            switch (profileConnectionState) {
                default: {
                    n5 = n;
                    n6 = n2;
                    n7 = n3;
                    n8 = n4;
                    break;
                }
                case 2: {
                    n5 = 1;
                    n6 = n2;
                    n7 = n3;
                    n8 = n4;
                    break;
                }
                case 1:
                case 3: {
                    return this.mContext.getString(Utils.getConnectionStateSummary(profileConnectionState));
                }
                case 0: {
                    n5 = n;
                    n6 = n2;
                    n7 = n3;
                    n8 = n4;
                    if (!localBluetoothProfile.isProfileReady()) {
                        break;
                    }
                    if (localBluetoothProfile instanceof A2dpProfile || localBluetoothProfile instanceof A2dpSinkProfile) {
                        n6 = 0;
                        n8 = n4;
                        n7 = n3;
                        n5 = n;
                        break;
                    }
                    if (localBluetoothProfile instanceof HeadsetProfile || localBluetoothProfile instanceof HfpClientProfile) {
                        n7 = 0;
                        n5 = n;
                        n6 = n2;
                        n8 = n4;
                        break;
                    }
                    n5 = n;
                    n6 = n2;
                    n7 = n3;
                    n8 = n4;
                    if (localBluetoothProfile instanceof HearingAidProfile) {
                        n8 = 0;
                        n5 = n;
                        n6 = n2;
                        n7 = n3;
                        break;
                    }
                    break;
                }
            }
            n = n5;
            n2 = n6;
            n3 = n7;
            n4 = n8;
        }
        Object formatPercentage = null;
        final int batteryLevel = this.getBatteryLevel();
        if (batteryLevel != -1) {
            formatPercentage = com.android.settingslib.Utils.formatPercentage(batteryLevel);
        }
        int n10;
        final int n9 = n10 = R.string.bluetooth_pairing;
        Label_0462: {
            if (n != 0) {
                if (n2 == 0 && n3 == 0 && n4 == 0) {
                    n10 = n9;
                    if (formatPercentage != null) {
                        n10 = R.string.bluetooth_battery_level;
                    }
                }
                else if (formatPercentage != null) {
                    if (com.android.settingslib.Utils.isAudioModeOngoingCall(this.mContext)) {
                        if (this.mIsActiveDeviceHeadset) {
                            n10 = R.string.bluetooth_active_battery_level;
                        }
                        else {
                            n10 = R.string.bluetooth_battery_level;
                        }
                    }
                    else if (!this.mIsActiveDeviceHearingAid && !this.mIsActiveDeviceA2dp) {
                        n10 = R.string.bluetooth_battery_level;
                    }
                    else {
                        n10 = R.string.bluetooth_active_battery_level;
                    }
                }
                else if (com.android.settingslib.Utils.isAudioModeOngoingCall(this.mContext)) {
                    n10 = n9;
                    if (this.mIsActiveDeviceHeadset) {
                        n10 = R.string.bluetooth_active_no_battery_level;
                    }
                }
                else {
                    if (!this.mIsActiveDeviceHearingAid) {
                        n10 = n9;
                        if (!this.mIsActiveDeviceA2dp) {
                            break Label_0462;
                        }
                    }
                    n10 = R.string.bluetooth_active_no_battery_level;
                }
            }
        }
        String string;
        if (n10 == R.string.bluetooth_pairing && this.getBondState() != 11) {
            string = null;
        }
        else {
            string = this.mContext.getString(n10, new Object[] { formatPercentage });
        }
        return string;
    }
    
    public BluetoothDevice getDevice() {
        return this.mDevice;
    }
    
    public long getHiSyncId() {
        return this.mHiSyncId;
    }
    
    public int getMessagePermissionChoice() {
        final int messageAccessPermission = this.mDevice.getMessageAccessPermission();
        if (messageAccessPermission == 1) {
            return 1;
        }
        if (messageAccessPermission == 2) {
            return 2;
        }
        return 0;
    }
    
    public String getName() {
        return this.mName;
    }
    
    public int getPhonebookPermissionChoice() {
        final int phonebookAccessPermission = this.mDevice.getPhonebookAccessPermission();
        if (phonebookAccessPermission == 1) {
            return 1;
        }
        if (phonebookAccessPermission == 2) {
            return 2;
        }
        return 0;
    }
    
    public int getProfileConnectionState(final LocalBluetoothProfile localBluetoothProfile) {
        if (this.mProfileConnectionState.get(localBluetoothProfile) == null) {
            this.mProfileConnectionState.put(localBluetoothProfile, localBluetoothProfile.getConnectionStatus(this.mDevice));
        }
        return this.mProfileConnectionState.get(localBluetoothProfile);
    }
    
    public List<LocalBluetoothProfile> getProfiles() {
        return Collections.unmodifiableList((List<? extends LocalBluetoothProfile>)this.mProfiles);
    }
    
    public List<LocalBluetoothProfile> getRemovedProfiles() {
        return this.mRemovedProfiles;
    }
    
    public int getSimPermissionChoice() {
        final int simAccessPermission = this.mDevice.getSimAccessPermission();
        if (simAccessPermission == 1) {
            return 1;
        }
        if (simAccessPermission == 2) {
            return 2;
        }
        return 0;
    }
    
    public boolean hasHumanReadableName() {
        return TextUtils.isEmpty((CharSequence)this.mDevice.getAliasName()) ^ true;
    }
    
    @Override
    public int hashCode() {
        return this.mDevice.getAddress().hashCode();
    }
    
    public boolean isA2dpDevice() {
        final A2dpProfile a2dpProfile = this.mProfileManager.getA2dpProfile();
        return a2dpProfile != null && a2dpProfile.getConnectionStatus(this.mDevice) == 2;
    }
    
    public boolean isActiveDevice(final int n) {
        if (n == 21) {
            return this.mIsActiveDeviceHearingAid;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("getActiveDevice: unknown profile ");
                sb.append(n);
                Log.w("CachedBluetoothDevice", sb.toString());
                return false;
            }
            case 2: {
                return this.mIsActiveDeviceA2dp;
            }
            case 1: {
                return this.mIsActiveDeviceHeadset;
            }
        }
    }
    
    public boolean isBusy() {
        final Iterator<LocalBluetoothProfile> iterator = this.mProfiles.iterator();
        while (true) {
            final boolean hasNext = iterator.hasNext();
            boolean b = true;
            if (!hasNext) {
                if (this.getBondState() != 11) {
                    b = false;
                }
                return b;
            }
            final int profileConnectionState = this.getProfileConnectionState(iterator.next());
            if (profileConnectionState != 1 && profileConnectionState != 3) {
                continue;
            }
            return true;
        }
    }
    
    public boolean isConnected() {
        final Iterator<LocalBluetoothProfile> iterator = this.mProfiles.iterator();
        while (iterator.hasNext()) {
            if (this.getProfileConnectionState(iterator.next()) == 2) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isConnectedHearingAidDevice() {
        final HearingAidProfile hearingAidProfile = this.mProfileManager.getHearingAidProfile();
        return hearingAidProfile != null && hearingAidProfile.getConnectionStatus(this.mDevice) == 2;
    }
    
    public boolean isConnectedProfile(final LocalBluetoothProfile localBluetoothProfile) {
        return this.getProfileConnectionState(localBluetoothProfile) == 2;
    }
    
    public boolean isHearingAidDevice() {
        return this.mHiSyncId != 0L;
    }
    
    public boolean isHfpDevice() {
        final HeadsetProfile headsetProfile = this.mProfileManager.getHeadsetProfile();
        return headsetProfile != null && headsetProfile.getConnectionStatus(this.mDevice) == 2;
    }
    
    public void onActiveDeviceChanged(final boolean mIsActiveDeviceHearingAid, int n) {
        final int n2 = 0;
        final int n3 = 0;
        final int n4 = 0;
        final int n5 = 0;
        if (n != 21) {
            switch (n) {
                default: {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("onActiveDeviceChanged: unknown profile ");
                    sb.append(n);
                    sb.append(" isActive ");
                    sb.append(mIsActiveDeviceHearingAid);
                    Log.w("CachedBluetoothDevice", sb.toString());
                    n = n2;
                    break;
                }
                case 2: {
                    n = n5;
                    if (this.mIsActiveDeviceA2dp != mIsActiveDeviceHearingAid) {
                        n = 1;
                    }
                    this.mIsActiveDeviceA2dp = mIsActiveDeviceHearingAid;
                    break;
                }
                case 1: {
                    n = n3;
                    if (this.mIsActiveDeviceHeadset != mIsActiveDeviceHearingAid) {
                        n = 1;
                    }
                    this.mIsActiveDeviceHeadset = mIsActiveDeviceHearingAid;
                    break;
                }
            }
        }
        else {
            n = n4;
            if (this.mIsActiveDeviceHearingAid != mIsActiveDeviceHearingAid) {
                n = 1;
            }
            this.mIsActiveDeviceHearingAid = mIsActiveDeviceHearingAid;
        }
        if (n != 0) {
            this.dispatchAttributesChanged();
        }
    }
    
    void onAudioModeChanged() {
        this.dispatchAttributesChanged();
    }
    
    void onBondingDockConnect() {
        this.connect(false);
    }
    
    void onBondingStateChanged(final int n) {
        if (n == 10) {
            this.mProfiles.clear();
            this.setPhonebookPermissionChoice(0);
            this.setMessagePermissionChoice(0);
            this.setSimPermissionChoice(0);
            this.mMessageRejectionCount = 0;
            this.saveMessageRejectionCount();
        }
        this.refresh();
        if (n == 12) {
            if (this.mDevice.isBluetoothDock()) {
                this.onBondingDockConnect();
            }
            else if (this.mDevice.isBondingInitiatedLocally()) {
                this.connect(false);
            }
        }
    }
    
    void onProfileStateChanged(final LocalBluetoothProfile localBluetoothProfile, final int n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("onProfileStateChanged: profile ");
        sb.append(localBluetoothProfile);
        sb.append(", device=");
        sb.append(this.mDevice);
        sb.append(", newProfileState ");
        sb.append(n);
        Log.d("CachedBluetoothDevice", sb.toString());
        if (this.mLocalAdapter.getBluetoothState() == 13) {
            Log.d("CachedBluetoothDevice", " BT Turninig Off...Profile conn state change ignored...");
            return;
        }
        this.mProfileConnectionState.put(localBluetoothProfile, n);
        if (n == 2) {
            if (localBluetoothProfile instanceof MapProfile) {
                localBluetoothProfile.setPreferred(this.mDevice, true);
            }
            if (!this.mProfiles.contains(localBluetoothProfile)) {
                this.mRemovedProfiles.remove(localBluetoothProfile);
                this.mProfiles.add(localBluetoothProfile);
                if (localBluetoothProfile instanceof PanProfile && ((PanProfile)localBluetoothProfile).isLocalRoleNap(this.mDevice)) {
                    this.mLocalNapRoleConnected = true;
                }
            }
        }
        else if (localBluetoothProfile instanceof MapProfile && n == 0) {
            localBluetoothProfile.setPreferred(this.mDevice, false);
        }
        else if (this.mLocalNapRoleConnected && localBluetoothProfile instanceof PanProfile && ((PanProfile)localBluetoothProfile).isLocalRoleNap(this.mDevice) && n == 0) {
            Log.d("CachedBluetoothDevice", "Removing PanProfile from device after NAP disconnect");
            this.mProfiles.remove(localBluetoothProfile);
            this.mRemovedProfiles.add(localBluetoothProfile);
            this.mLocalNapRoleConnected = false;
        }
        this.fetchActiveDevices();
    }
    
    void onUuidChanged() {
        this.updateProfiles();
        final ParcelUuid[] uuids = this.mDevice.getUuids();
        long n = 5000L;
        if (BluetoothUuid.isUuidPresent(uuids, BluetoothUuid.Hogp)) {
            n = 30000L;
        }
        if (!this.mProfiles.isEmpty() && this.mConnectAttempted + n > SystemClock.elapsedRealtime()) {
            this.connectWithoutResettingTimer(false);
        }
        this.dispatchAttributesChanged();
    }
    
    void refresh() {
        this.dispatchAttributesChanged();
    }
    
    void refreshBtClass() {
        this.fetchBtClass();
        this.dispatchAttributesChanged();
    }
    
    void refreshName() {
        this.fetchName();
        this.dispatchAttributesChanged();
    }
    
    public void registerCallback(final Callback callback) {
        synchronized (this.mCallbacks) {
            this.mCallbacks.add(callback);
        }
    }
    
    public boolean setActive() {
        final boolean b = false;
        final A2dpProfile a2dpProfile = this.mProfileManager.getA2dpProfile();
        boolean b2 = b;
        if (a2dpProfile != null) {
            b2 = b;
            if (this.isConnectedProfile(a2dpProfile)) {
                b2 = b;
                if (a2dpProfile.setActiveDevice(this.getDevice())) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("OnPreferenceClickListener: A2DP active device=");
                    sb.append(this);
                    Log.i("CachedBluetoothDevice", sb.toString());
                    b2 = true;
                }
            }
        }
        final HeadsetProfile headsetProfile = this.mProfileManager.getHeadsetProfile();
        boolean b3 = b2;
        if (headsetProfile != null) {
            b3 = b2;
            if (this.isConnectedProfile(headsetProfile)) {
                b3 = b2;
                if (headsetProfile.setActiveDevice(this.getDevice())) {
                    final StringBuilder sb2 = new StringBuilder();
                    sb2.append("OnPreferenceClickListener: Headset active device=");
                    sb2.append(this);
                    Log.i("CachedBluetoothDevice", sb2.toString());
                    b3 = true;
                }
            }
        }
        final HearingAidProfile hearingAidProfile = this.mProfileManager.getHearingAidProfile();
        boolean b4 = b3;
        if (hearingAidProfile != null) {
            b4 = b3;
            if (this.isConnectedProfile(hearingAidProfile)) {
                b4 = b3;
                if (hearingAidProfile.setActiveDevice(this.getDevice())) {
                    final StringBuilder sb3 = new StringBuilder();
                    sb3.append("OnPreferenceClickListener: Hearing Aid active device=");
                    sb3.append(this);
                    Log.i("CachedBluetoothDevice", sb3.toString());
                    b4 = true;
                }
            }
        }
        return b4;
    }
    
    void setBtClass(final BluetoothClass mBtClass) {
        if (mBtClass != null && this.mBtClass != mBtClass) {
            this.mBtClass = mBtClass;
            this.dispatchAttributesChanged();
        }
    }
    
    public void setHiSyncId(final long mHiSyncId) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setHiSyncId: mDevice ");
        sb.append(this.mDevice);
        sb.append(", id ");
        sb.append(mHiSyncId);
        Log.d("CachedBluetoothDevice", sb.toString());
        this.mHiSyncId = mHiSyncId;
    }
    
    public void setJustDiscovered(final boolean mJustDiscovered) {
        if (this.mJustDiscovered != mJustDiscovered) {
            this.mJustDiscovered = mJustDiscovered;
            this.dispatchAttributesChanged();
        }
    }
    
    public void setMessagePermissionChoice(final int n) {
        int messageAccessPermission = 0;
        if (n == 1) {
            messageAccessPermission = 1;
        }
        else if (n == 2) {
            messageAccessPermission = 2;
        }
        this.mDevice.setMessageAccessPermission(messageAccessPermission);
    }
    
    public void setName(final String s) {
        if (s != null && !TextUtils.equals((CharSequence)s, (CharSequence)this.mName)) {
            this.mName = s;
            this.mDevice.setAlias(s);
            this.dispatchAttributesChanged();
        }
    }
    
    void setNewName(final String mName) {
        if (this.mName == null) {
            this.mName = mName;
            if (this.mName == null || TextUtils.isEmpty((CharSequence)this.mName)) {
                this.mName = this.mDevice.getAddress();
            }
            this.dispatchAttributesChanged();
        }
    }
    
    public void setPhonebookPermissionChoice(final int n) {
        int phonebookAccessPermission = 0;
        if (n == 1) {
            phonebookAccessPermission = 1;
        }
        else if (n == 2) {
            phonebookAccessPermission = 2;
        }
        this.mDevice.setPhonebookAccessPermission(phonebookAccessPermission);
    }
    
    void setRssi(final short n) {
        if (this.mRssi != n) {
            this.mRssi = n;
            this.dispatchAttributesChanged();
        }
    }
    
    void setSimPermissionChoice(final int n) {
        int simAccessPermission = 0;
        if (n == 1) {
            simAccessPermission = 1;
        }
        else if (n == 2) {
            simAccessPermission = 2;
        }
        this.mDevice.setSimAccessPermission(simAccessPermission);
    }
    
    public boolean startPairing() {
        if (this.mLocalAdapter.isDiscovering()) {
            this.mLocalAdapter.cancelDiscovery();
        }
        return this.mDevice.createBond();
    }
    
    @Override
    public String toString() {
        return this.mDevice.toString();
    }
    
    public void unpair() {
        final int bondState = this.getBondState();
        if (bondState == 11) {
            this.mDevice.cancelBondProcess();
        }
        if (bondState != 10) {
            final BluetoothDevice mDevice = this.mDevice;
            if (mDevice != null && mDevice.removeBond()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Command sent successfully:REMOVE_BOND ");
                sb.append(this.describe(null));
                Log.d("CachedBluetoothDevice", sb.toString());
            }
        }
    }
    
    public void unregisterCallback(final Callback callback) {
        synchronized (this.mCallbacks) {
            this.mCallbacks.remove(callback);
        }
    }
    
    public interface Callback
    {
        void onDeviceAttributesChanged();
    }
}
