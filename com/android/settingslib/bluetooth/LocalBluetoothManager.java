package com.android.settingslib.bluetooth;

import android.util.Log;
import java.lang.ref.WeakReference;
import android.content.Context;

public class LocalBluetoothManager
{
    private static LocalBluetoothManager sInstance;
    private final CachedBluetoothDeviceManager mCachedDeviceManager;
    private final Context mContext;
    private final BluetoothEventManager mEventManager;
    private WeakReference<Context> mForegroundActivity;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    
    private LocalBluetoothManager(final LocalBluetoothAdapter mLocalAdapter, final Context mContext) {
        this.mContext = mContext;
        this.mLocalAdapter = mLocalAdapter;
        this.mCachedDeviceManager = new CachedBluetoothDeviceManager(mContext, this);
        this.mEventManager = new BluetoothEventManager(this.mLocalAdapter, this.mCachedDeviceManager, mContext);
        this.mProfileManager = new LocalBluetoothProfileManager(mContext, this.mLocalAdapter, this.mCachedDeviceManager, this.mEventManager);
        this.mEventManager.readPairedDevices();
    }
    
    public static LocalBluetoothManager getInstance(Context applicationContext, final BluetoothManagerCallback bluetoothManagerCallback) {
        synchronized (LocalBluetoothManager.class) {
            if (LocalBluetoothManager.sInstance == null) {
                final LocalBluetoothAdapter instance = LocalBluetoothAdapter.getInstance();
                if (instance == null) {
                    return null;
                }
                applicationContext = applicationContext.getApplicationContext();
                LocalBluetoothManager.sInstance = new LocalBluetoothManager(instance, applicationContext);
                if (bluetoothManagerCallback != null) {
                    bluetoothManagerCallback.onBluetoothManagerInitialized(applicationContext, LocalBluetoothManager.sInstance);
                }
            }
            return LocalBluetoothManager.sInstance;
        }
    }
    
    public LocalBluetoothAdapter getBluetoothAdapter() {
        return this.mLocalAdapter;
    }
    
    public CachedBluetoothDeviceManager getCachedDeviceManager() {
        return this.mCachedDeviceManager;
    }
    
    public BluetoothEventManager getEventManager() {
        return this.mEventManager;
    }
    
    public Context getForegroundActivity() {
        Context context;
        if (this.mForegroundActivity == null) {
            context = null;
        }
        else {
            context = this.mForegroundActivity.get();
        }
        return context;
    }
    
    public LocalBluetoothProfileManager getProfileManager() {
        return this.mProfileManager;
    }
    
    public boolean isForegroundActivity() {
        return this.mForegroundActivity != null && this.mForegroundActivity.get() != null;
    }
    
    public void setForegroundActivity(final Context context) {
        // monitorenter(this)
        Label_0055: {
            Label_0035: {
                if (context != null) {
                    Label_0058: {
                        try {
                            Log.d("LocalBluetoothManager", "setting foreground activity to non-null context");
                            this.mForegroundActivity = new WeakReference<Context>(context);
                            break Label_0055;
                        }
                        finally {
                            break Label_0058;
                        }
                        break Label_0035;
                    }
                }
                // monitorexit(this)
            }
            if (this.mForegroundActivity != null) {
                Log.d("LocalBluetoothManager", "setting foreground activity to null");
                this.mForegroundActivity = null;
            }
        }
    }
    // monitorexit(this)
    
    public interface BluetoothManagerCallback
    {
        void onBluetoothManagerInitialized(final Context p0, final LocalBluetoothManager p1);
    }
}
