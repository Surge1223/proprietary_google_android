package com.android.settingslib.bluetooth;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;
import android.content.Context;
import android.content.BroadcastReceiver;

public class BluetoothDiscoverableTimeoutReceiver extends BroadcastReceiver
{
    public static void setDiscoverableAlarm(final Context context, final long n) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setDiscoverableAlarm(): alarmTime = ");
        sb.append(n);
        Log.d("BluetoothDiscoverableTimeoutReceiver", sb.toString());
        final Intent intent = new Intent("android.bluetooth.intent.DISCOVERABLE_TIMEOUT");
        intent.setClass(context, (Class)BluetoothDiscoverableTimeoutReceiver.class);
        final PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, intent, 0);
        final AlarmManager alarmManager = (AlarmManager)context.getSystemService("alarm");
        if (broadcast != null) {
            alarmManager.cancel(broadcast);
            Log.d("BluetoothDiscoverableTimeoutReceiver", "setDiscoverableAlarm(): cancel prev alarm");
        }
        alarmManager.set(0, n, PendingIntent.getBroadcast(context, 0, intent, 0));
    }
    
    public void onReceive(final Context context, final Intent intent) {
        if (intent.getAction() != null && intent.getAction().equals("android.bluetooth.intent.DISCOVERABLE_TIMEOUT")) {
            final LocalBluetoothAdapter instance = LocalBluetoothAdapter.getInstance();
            if (instance != null && instance.getState() == 12) {
                Log.d("BluetoothDiscoverableTimeoutReceiver", "Disable discoverable...");
                instance.setScanMode(21);
            }
            else {
                Log.e("BluetoothDiscoverableTimeoutReceiver", "localBluetoothAdapter is NULL!!");
            }
        }
    }
}
