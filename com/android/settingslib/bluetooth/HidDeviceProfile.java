package com.android.settingslib.bluetooth;

import java.util.Iterator;
import com.android.settingslib.R;
import android.bluetooth.BluetoothClass;
import java.util.List;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile$ServiceListener;
import android.content.Context;
import android.bluetooth.BluetoothHidDevice;

public class HidDeviceProfile implements LocalBluetoothProfile
{
    private final CachedBluetoothDeviceManager mDeviceManager;
    private boolean mIsProfileReady;
    private final LocalBluetoothAdapter mLocalAdapter;
    private final LocalBluetoothProfileManager mProfileManager;
    private BluetoothHidDevice mService;
    
    HidDeviceProfile(final Context context, final LocalBluetoothAdapter mLocalAdapter, final CachedBluetoothDeviceManager mDeviceManager, final LocalBluetoothProfileManager mProfileManager) {
        this.mLocalAdapter = mLocalAdapter;
        this.mDeviceManager = mDeviceManager;
        this.mProfileManager = mProfileManager;
        mLocalAdapter.getProfileProxy(context, (BluetoothProfile$ServiceListener)new HidDeviceServiceListener(), 19);
    }
    
    @Override
    public boolean connect(final BluetoothDevice bluetoothDevice) {
        return false;
    }
    
    @Override
    public boolean disconnect(final BluetoothDevice bluetoothDevice) {
        return this.mService != null && this.mService.disconnect(bluetoothDevice);
    }
    
    @Override
    protected void finalize() {
        Log.d("HidDeviceProfile", "finalize()");
        if (this.mService != null) {
            try {
                BluetoothAdapter.getDefaultAdapter().closeProfileProxy(19, (BluetoothProfile)this.mService);
                this.mService = null;
            }
            catch (Throwable t) {
                Log.w("HidDeviceProfile", "Error cleaning up HID proxy", t);
            }
        }
    }
    
    @Override
    public int getConnectionStatus(final BluetoothDevice bluetoothDevice) {
        final BluetoothHidDevice mService = this.mService;
        int connectionState = 0;
        if (mService == null) {
            return 0;
        }
        final List connectedDevices = this.mService.getConnectedDevices();
        if (!connectedDevices.isEmpty() && connectedDevices.contains(bluetoothDevice)) {
            connectionState = this.mService.getConnectionState(bluetoothDevice);
        }
        return connectionState;
    }
    
    @Override
    public int getDrawableResource(final BluetoothClass bluetoothClass) {
        return R.drawable.ic_bt_misc_hid;
    }
    
    @Override
    public int getNameResource(final BluetoothDevice bluetoothDevice) {
        return R.string.bluetooth_profile_hid;
    }
    
    @Override
    public int getProfileId() {
        return 19;
    }
    
    @Override
    public boolean isAutoConnectable() {
        return false;
    }
    
    @Override
    public boolean isConnectable() {
        return true;
    }
    
    @Override
    public boolean isPreferred(final BluetoothDevice bluetoothDevice) {
        return this.getConnectionStatus(bluetoothDevice) != 0;
    }
    
    @Override
    public boolean isProfileReady() {
        return this.mIsProfileReady;
    }
    
    @Override
    public void setPreferred(final BluetoothDevice bluetoothDevice, final boolean b) {
        if (!b) {
            this.mService.disconnect(bluetoothDevice);
        }
    }
    
    @Override
    public String toString() {
        return "HID DEVICE";
    }
    
    private final class HidDeviceServiceListener implements BluetoothProfile$ServiceListener
    {
        public void onServiceConnected(final int n, final BluetoothProfile bluetoothProfile) {
            Log.d("HidDeviceProfile", "Bluetooth service connected :-)");
            HidDeviceProfile.this.mService = (BluetoothHidDevice)bluetoothProfile;
            for (final BluetoothDevice bluetoothDevice : HidDeviceProfile.this.mService.getConnectedDevices()) {
                CachedBluetoothDevice cachedBluetoothDevice;
                if ((cachedBluetoothDevice = HidDeviceProfile.this.mDeviceManager.findDevice(bluetoothDevice)) == null) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("HidProfile found new device: ");
                    sb.append(bluetoothDevice);
                    Log.w("HidDeviceProfile", sb.toString());
                    cachedBluetoothDevice = HidDeviceProfile.this.mDeviceManager.addDevice(HidDeviceProfile.this.mLocalAdapter, HidDeviceProfile.this.mProfileManager, bluetoothDevice);
                }
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("Connection status changed: ");
                sb2.append(cachedBluetoothDevice);
                Log.d("HidDeviceProfile", sb2.toString());
                cachedBluetoothDevice.onProfileStateChanged(HidDeviceProfile.this, 2);
                cachedBluetoothDevice.refresh();
            }
            HidDeviceProfile.this.mIsProfileReady = true;
        }
        
        public void onServiceDisconnected(final int n) {
            Log.d("HidDeviceProfile", "Bluetooth service disconnected");
            HidDeviceProfile.this.mIsProfileReady = false;
        }
    }
}
