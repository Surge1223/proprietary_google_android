package com.android.settingslib.inputmethod;

import java.util.Map;
import java.util.Iterator;
import android.view.inputmethod.InputMethodSubtype;
import android.content.Context;
import android.text.TextUtils;
import java.util.Comparator;
import java.util.ArrayList;
import com.android.settingslib.R;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.view.inputmethod.InputMethodInfo;
import java.util.List;
import android.view.inputmethod.InputMethodManager;
import android.support.v14.preference.PreferenceFragment;
import java.text.Collator;
import android.support.v7.preference.TwoStatePreference;
import java.util.HashMap;
import android.support.v7.preference.Preference;

public class InputMethodAndSubtypeEnablerManager implements OnPreferenceChangeListener
{
    private final HashMap<String, TwoStatePreference> mAutoSelectionPrefsMap;
    private final Collator mCollator;
    private final PreferenceFragment mFragment;
    private boolean mHaveHardKeyboard;
    private InputMethodManager mImm;
    private final HashMap<String, List<Preference>> mInputMethodAndSubtypePrefsMap;
    private List<InputMethodInfo> mInputMethodInfoList;
    
    public InputMethodAndSubtypeEnablerManager(final PreferenceFragment mFragment) {
        this.mInputMethodAndSubtypePrefsMap = new HashMap<String, List<Preference>>();
        this.mAutoSelectionPrefsMap = new HashMap<String, TwoStatePreference>();
        this.mCollator = Collator.getInstance();
        this.mFragment = mFragment;
        this.mImm = (InputMethodManager)mFragment.getContext().getSystemService((Class)InputMethodManager.class);
        this.mInputMethodInfoList = (List<InputMethodInfo>)this.mImm.getInputMethodList();
    }
    
    private void addInputMethodSubtypePreferences(final PreferenceFragment preferenceFragment, final InputMethodInfo inputMethodInfo, final PreferenceScreen preferenceScreen) {
        final Context context = preferenceFragment.getPreferenceManager().getContext();
        final int subtypeCount = inputMethodInfo.getSubtypeCount();
        if (subtypeCount <= 1) {
            return;
        }
        final String id = inputMethodInfo.getId();
        final PreferenceCategory preferenceCategory = new PreferenceCategory(context);
        preferenceScreen.addPreference(preferenceCategory);
        preferenceCategory.setTitle(inputMethodInfo.loadLabel(context.getPackageManager()));
        preferenceCategory.setKey(id);
        final SwitchWithNoTextPreference switchWithNoTextPreference = new SwitchWithNoTextPreference(context);
        this.mAutoSelectionPrefsMap.put(id, switchWithNoTextPreference);
        preferenceCategory.addPreference(switchWithNoTextPreference);
        switchWithNoTextPreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
        final PreferenceCategory preferenceCategory2 = new PreferenceCategory(context);
        preferenceCategory2.setTitle(R.string.active_input_method_subtypes);
        preferenceScreen.addPreference(preferenceCategory2);
        String title = null;
        final ArrayList<Object> list = new ArrayList<Object>();
        String subtypeLocaleNameAsSentence;
        for (int i = 0; i < subtypeCount; ++i, title = subtypeLocaleNameAsSentence) {
            final InputMethodSubtype subtype = inputMethodInfo.getSubtypeAt(i);
            if (subtype.overridesImplicitlyEnabledSubtype()) {
                if ((subtypeLocaleNameAsSentence = title) == null) {
                    subtypeLocaleNameAsSentence = InputMethodAndSubtypeUtil.getSubtypeLocaleNameAsSentence(subtype, context, inputMethodInfo);
                }
            }
            else {
                list.add(new InputMethodSubtypePreference(context, subtype, inputMethodInfo));
                subtypeLocaleNameAsSentence = title;
            }
        }
        list.sort(new _$$Lambda$InputMethodAndSubtypeEnablerManager$dNefE8o88NKQTk3_894EfBqAP3w(this));
        for (final InputMethodSubtypePreference inputMethodSubtypePreference : list) {
            preferenceCategory2.addPreference(inputMethodSubtypePreference);
            inputMethodSubtypePreference.setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            InputMethodAndSubtypeUtil.removeUnnecessaryNonPersistentPreference(inputMethodSubtypePreference);
        }
        this.mInputMethodAndSubtypePrefsMap.put(id, (List<Preference>)list);
        if (TextUtils.isEmpty((CharSequence)title)) {
            switchWithNoTextPreference.setTitle(R.string.use_system_language_to_select_input_method_subtypes);
        }
        else {
            switchWithNoTextPreference.setTitle(title);
        }
    }
    
    private boolean isNoSubtypesExplicitlySelected(final String s) {
        for (final Preference preference : this.mInputMethodAndSubtypePrefsMap.get(s)) {
            if (preference instanceof TwoStatePreference && ((TwoStatePreference)preference).isChecked()) {
                return false;
            }
        }
        return true;
    }
    
    private void setAutoSelectionSubtypesEnabled(final String s, final boolean checked) {
        final TwoStatePreference twoStatePreference = this.mAutoSelectionPrefsMap.get(s);
        if (twoStatePreference == null) {
            return;
        }
        twoStatePreference.setChecked(checked);
        for (final Preference preference : this.mInputMethodAndSubtypePrefsMap.get(s)) {
            if (preference instanceof TwoStatePreference) {
                preference.setEnabled(checked ^ true);
                if (!checked) {
                    continue;
                }
                ((TwoStatePreference)preference).setChecked(false);
            }
        }
        if (checked) {
            InputMethodAndSubtypeUtil.saveInputMethodSubtypeList(this.mFragment, this.mFragment.getContext().getContentResolver(), this.mInputMethodInfoList, this.mHaveHardKeyboard);
            this.updateImplicitlyEnabledSubtypes(s);
        }
    }
    
    private void updateAutoSelectionPreferences() {
        for (final String s : this.mInputMethodAndSubtypePrefsMap.keySet()) {
            this.setAutoSelectionSubtypesEnabled(s, this.isNoSubtypesExplicitlySelected(s));
        }
        this.updateImplicitlyEnabledSubtypes(null);
    }
    
    private void updateImplicitlyEnabledSubtypes(final String s) {
        for (final InputMethodInfo inputMethodInfo : this.mInputMethodInfoList) {
            final String id = inputMethodInfo.getId();
            final TwoStatePreference twoStatePreference = this.mAutoSelectionPrefsMap.get(id);
            if (twoStatePreference != null) {
                if (!twoStatePreference.isChecked()) {
                    continue;
                }
                if (!id.equals(s) && s != null) {
                    continue;
                }
                this.updateImplicitlyEnabledSubtypesOf(inputMethodInfo);
            }
        }
    }
    
    private void updateImplicitlyEnabledSubtypesOf(final InputMethodInfo inputMethodInfo) {
        final String id = inputMethodInfo.getId();
        final List<Preference> list = this.mInputMethodAndSubtypePrefsMap.get(id);
        final List enabledInputMethodSubtypeList = this.mImm.getEnabledInputMethodSubtypeList(inputMethodInfo, true);
        if (list != null && enabledInputMethodSubtypeList != null) {
            for (final Preference preference : list) {
                if (!(preference instanceof TwoStatePreference)) {
                    continue;
                }
                final TwoStatePreference twoStatePreference = (TwoStatePreference)preference;
                twoStatePreference.setChecked(false);
                for (final InputMethodSubtype inputMethodSubtype : enabledInputMethodSubtypeList) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append(id);
                    sb.append(inputMethodSubtype.hashCode());
                    if (twoStatePreference.getKey().equals(sb.toString())) {
                        twoStatePreference.setChecked(true);
                        break;
                    }
                }
            }
        }
    }
    
    public void init(final PreferenceFragment preferenceFragment, final String s, final PreferenceScreen preferenceScreen) {
        this.mHaveHardKeyboard = (preferenceFragment.getResources().getConfiguration().keyboard == 2);
        for (final InputMethodInfo inputMethodInfo : this.mInputMethodInfoList) {
            if (inputMethodInfo.getId().equals(s) || TextUtils.isEmpty((CharSequence)s)) {
                this.addInputMethodSubtypePreferences(preferenceFragment, inputMethodInfo, preferenceScreen);
            }
        }
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (!(o instanceof Boolean)) {
            return true;
        }
        final boolean booleanValue = (boolean)o;
        for (final String s : this.mAutoSelectionPrefsMap.keySet()) {
            if (this.mAutoSelectionPrefsMap.get(s) == preference) {
                final TwoStatePreference twoStatePreference = (TwoStatePreference)preference;
                twoStatePreference.setChecked(booleanValue);
                this.setAutoSelectionSubtypesEnabled(s, twoStatePreference.isChecked());
                return false;
            }
        }
        if (preference instanceof InputMethodSubtypePreference) {
            final InputMethodSubtypePreference inputMethodSubtypePreference = (InputMethodSubtypePreference)preference;
            inputMethodSubtypePreference.setChecked(booleanValue);
            if (!inputMethodSubtypePreference.isChecked()) {
                this.updateAutoSelectionPreferences();
            }
            return false;
        }
        return true;
    }
    
    public void refresh(final Context context, final PreferenceFragment preferenceFragment) {
        InputMethodSettingValuesWrapper.getInstance(context).refreshAllInputMethodAndSubtypes();
        InputMethodAndSubtypeUtil.loadInputMethodSubtypeList(preferenceFragment, context.getContentResolver(), this.mInputMethodInfoList, this.mInputMethodAndSubtypePrefsMap);
        this.updateAutoSelectionPreferences();
    }
    
    public void save(final Context context, final PreferenceFragment preferenceFragment) {
        InputMethodAndSubtypeUtil.saveInputMethodSubtypeList(preferenceFragment, context.getContentResolver(), this.mInputMethodInfoList, this.mHaveHardKeyboard);
    }
}
