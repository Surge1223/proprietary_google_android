package com.android.settingslib.inputmethod;

import java.util.Map;
import java.util.Iterator;
import android.view.inputmethod.InputMethodSubtype;
import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import com.android.settingslib.R;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.view.inputmethod.InputMethodInfo;
import java.util.List;
import android.view.inputmethod.InputMethodManager;
import android.support.v14.preference.PreferenceFragment;
import java.text.Collator;
import android.support.v7.preference.TwoStatePreference;
import java.util.HashMap;
import android.support.v7.preference.Preference;
import java.util.Comparator;

public final class _$$Lambda$InputMethodAndSubtypeEnablerManager$dNefE8o88NKQTk3_894EfBqAP3w implements Comparator
{
    @Override
    public final int compare(final Object o, final Object o2) {
        return InputMethodAndSubtypeEnablerManager.lambda$addInputMethodSubtypePreferences$0(this.f$0, (Preference)o, (Preference)o2);
    }
}
