package com.android.settingslib.inputmethod;

import java.util.Collection;
import java.util.Locale;
import com.android.internal.inputmethod.InputMethodUtils;
import java.util.List;
import android.view.inputmethod.InputMethodSubtype;
import java.util.Iterator;
import android.util.Log;
import android.os.RemoteException;
import android.util.Slog;
import android.app.ActivityManager;
import android.content.Context;
import com.android.internal.inputmethod.InputMethodUtils$InputMethodSettings;
import java.util.HashMap;
import java.util.ArrayList;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodInfo;
import java.util.HashSet;

public class InputMethodSettingValuesWrapper
{
    private static final String TAG;
    private static volatile InputMethodSettingValuesWrapper sInstance;
    private final HashSet<InputMethodInfo> mAsciiCapableEnabledImis;
    private final InputMethodManager mImm;
    private final ArrayList<InputMethodInfo> mMethodList;
    private final HashMap<String, InputMethodInfo> mMethodMap;
    private final InputMethodUtils$InputMethodSettings mSettings;
    
    static {
        TAG = InputMethodSettingValuesWrapper.class.getSimpleName();
    }
    
    private InputMethodSettingValuesWrapper(final Context context) {
        this.mMethodList = new ArrayList<InputMethodInfo>();
        this.mMethodMap = new HashMap<String, InputMethodInfo>();
        this.mAsciiCapableEnabledImis = new HashSet<InputMethodInfo>();
        this.mSettings = new InputMethodUtils$InputMethodSettings(context.getResources(), context.getContentResolver(), (HashMap)this.mMethodMap, (ArrayList)this.mMethodList, getDefaultCurrentUserId(), false);
        this.mImm = (InputMethodManager)context.getSystemService("input_method");
        this.refreshAllInputMethodAndSubtypes();
    }
    
    private static int getDefaultCurrentUserId() {
        try {
            return ActivityManager.getService().getCurrentUser().id;
        }
        catch (RemoteException ex) {
            Slog.w(InputMethodSettingValuesWrapper.TAG, "Couldn't get current user ID; guessing it's 0", (Throwable)ex);
            return 0;
        }
    }
    
    private int getEnabledValidSystemNonAuxAsciiCapableImeCount(final Context context) {
        int n = 0;
        Object o = this.mMethodMap;
        synchronized (o) {
            final ArrayList enabledInputMethodListLocked = this.mSettings.getEnabledInputMethodListLocked();
            // monitorexit(o)
            o = enabledInputMethodListLocked.iterator();
            while (((Iterator)o).hasNext()) {
                int n2 = n;
                if (this.isValidSystemNonAuxAsciiCapableIme(((Iterator<InputMethodInfo>)o).next(), context)) {
                    n2 = n + 1;
                }
                n = n2;
            }
            if (n == 0) {
                Log.w(InputMethodSettingValuesWrapper.TAG, "No \"enabledValidSystemNonAuxAsciiCapableIme\"s found.");
            }
            return n;
        }
    }
    
    public static InputMethodSettingValuesWrapper getInstance(final Context context) {
        if (InputMethodSettingValuesWrapper.sInstance == null) {
            synchronized (InputMethodSettingValuesWrapper.TAG) {
                if (InputMethodSettingValuesWrapper.sInstance == null) {
                    InputMethodSettingValuesWrapper.sInstance = new InputMethodSettingValuesWrapper(context);
                }
            }
        }
        return InputMethodSettingValuesWrapper.sInstance;
    }
    
    private void updateAsciiCapableEnabledImis() {
        synchronized (this.mMethodMap) {
            this.mAsciiCapableEnabledImis.clear();
            for (final InputMethodInfo inputMethodInfo : this.mSettings.getEnabledInputMethodListLocked()) {
                for (int subtypeCount = inputMethodInfo.getSubtypeCount(), i = 0; i < subtypeCount; ++i) {
                    final InputMethodSubtype subtype = inputMethodInfo.getSubtypeAt(i);
                    if ("keyboard".equalsIgnoreCase(subtype.getMode()) && subtype.isAsciiCapable()) {
                        this.mAsciiCapableEnabledImis.add(inputMethodInfo);
                        break;
                    }
                }
            }
        }
    }
    
    public List<InputMethodInfo> getInputMethodList() {
        synchronized (this.mMethodMap) {
            return this.mMethodList;
        }
    }
    
    public boolean isAlwaysCheckedIme(final InputMethodInfo inputMethodInfo, final Context context) {
        final boolean enabledImi = this.isEnabledImi(inputMethodInfo);
        synchronized (this.mMethodMap) {
            final int size = this.mSettings.getEnabledInputMethodListLocked().size();
            boolean b = true;
            if (size <= 1 && enabledImi) {
                return true;
            }
            // monitorexit(this.mMethodMap)
            final int enabledValidSystemNonAuxAsciiCapableImeCount = this.getEnabledValidSystemNonAuxAsciiCapableImeCount(context);
            if (enabledValidSystemNonAuxAsciiCapableImeCount > 1 || (enabledValidSystemNonAuxAsciiCapableImeCount == 1 && !enabledImi) || !InputMethodUtils.isSystemIme(inputMethodInfo) || !this.isValidSystemNonAuxAsciiCapableIme(inputMethodInfo, context)) {
                b = false;
            }
            return b;
        }
    }
    
    public boolean isEnabledImi(final InputMethodInfo inputMethodInfo) {
        Object o = this.mMethodMap;
        synchronized (o) {
            final ArrayList enabledInputMethodListLocked = this.mSettings.getEnabledInputMethodListLocked();
            // monitorexit(o)
            o = enabledInputMethodListLocked.iterator();
            while (((Iterator)o).hasNext()) {
                if (((Iterator<InputMethodInfo>)o).next().getId().equals(inputMethodInfo.getId())) {
                    return true;
                }
            }
            return false;
        }
    }
    
    public boolean isValidSystemNonAuxAsciiCapableIme(final InputMethodInfo inputMethodInfo, final Context context) {
        if (inputMethodInfo.isAuxiliaryIme()) {
            return false;
        }
        if (InputMethodUtils.isSystemImeThatHasSubtypeOf(inputMethodInfo, context, true, context.getResources().getConfiguration().locale, false, InputMethodUtils.SUBTYPE_MODE_ANY)) {
            return true;
        }
        if (this.mAsciiCapableEnabledImis.isEmpty()) {
            Log.w(InputMethodSettingValuesWrapper.TAG, "ascii capable subtype enabled imi not found. Fall back to English Keyboard subtype.");
            return InputMethodUtils.containsSubtypeOf(inputMethodInfo, Locale.ENGLISH, false, "keyboard");
        }
        return this.mAsciiCapableEnabledImis.contains(inputMethodInfo);
    }
    
    public void refreshAllInputMethodAndSubtypes() {
        synchronized (this.mMethodMap) {
            this.mMethodList.clear();
            this.mMethodMap.clear();
            final List inputMethodList = this.mImm.getInputMethodList();
            this.mMethodList.addAll(inputMethodList);
            for (final InputMethodInfo inputMethodInfo : inputMethodList) {
                this.mMethodMap.put(inputMethodInfo.getId(), inputMethodInfo);
            }
            this.updateAsciiCapableEnabledImis();
        }
    }
}
