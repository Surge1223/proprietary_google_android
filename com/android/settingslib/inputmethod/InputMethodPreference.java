package com.android.settingslib.inputmethod;

import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.content.ActivityNotFoundException;
import android.widget.Toast;
import android.util.Log;
import java.text.Collator;
import android.content.DialogInterface$OnClickListener;
import com.android.settingslib.R;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface;
import android.view.inputmethod.InputMethodSubtype;
import java.util.List;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.inputmethod.InputMethodUtils;
import android.content.Intent;
import android.text.TextUtils;
import android.content.Context;
import android.view.inputmethod.InputMethodInfo;
import android.app.AlertDialog;
import android.support.v7.preference.Preference;
import com.android.settingslib.RestrictedSwitchPreference;

public class InputMethodPreference extends RestrictedSwitchPreference implements OnPreferenceChangeListener, OnPreferenceClickListener
{
    private static final String TAG;
    private AlertDialog mDialog;
    private final boolean mHasPriorityInSorting;
    private final InputMethodInfo mImi;
    private final InputMethodSettingValuesWrapper mInputMethodSettingValues;
    private final boolean mIsAllowedByOrganization;
    private final OnSavePreferenceListener mOnSaveListener;
    
    static {
        TAG = InputMethodPreference.class.getSimpleName();
    }
    
    InputMethodPreference(final Context context, final InputMethodInfo mImi, final CharSequence title, final boolean mIsAllowedByOrganization, final OnSavePreferenceListener mOnSaveListener) {
        super(context);
        this.mDialog = null;
        final boolean b = false;
        this.setPersistent(false);
        this.mImi = mImi;
        this.mIsAllowedByOrganization = mIsAllowedByOrganization;
        this.mOnSaveListener = mOnSaveListener;
        this.setSwitchTextOn("");
        this.setSwitchTextOff("");
        this.setKey(mImi.getId());
        this.setTitle(title);
        final String settingsActivity = mImi.getSettingsActivity();
        if (TextUtils.isEmpty((CharSequence)settingsActivity)) {
            this.setIntent(null);
        }
        else {
            final Intent intent = new Intent("android.intent.action.MAIN");
            intent.setClassName(mImi.getPackageName(), settingsActivity);
            this.setIntent(intent);
        }
        this.mInputMethodSettingValues = InputMethodSettingValuesWrapper.getInstance(context);
        boolean mHasPriorityInSorting = b;
        if (InputMethodUtils.isSystemIme(mImi)) {
            mHasPriorityInSorting = b;
            if (this.mInputMethodSettingValues.isValidSystemNonAuxAsciiCapableIme(mImi, context)) {
                mHasPriorityInSorting = true;
            }
        }
        this.mHasPriorityInSorting = mHasPriorityInSorting;
        this.setOnPreferenceClickListener((OnPreferenceClickListener)this);
        this.setOnPreferenceChangeListener((OnPreferenceChangeListener)this);
    }
    
    public InputMethodPreference(final Context context, final InputMethodInfo inputMethodInfo, final boolean b, final boolean b2, final OnSavePreferenceListener onSavePreferenceListener) {
        this(context, inputMethodInfo, inputMethodInfo.loadLabel(context.getPackageManager()), b2, onSavePreferenceListener);
        if (!b) {
            this.setWidgetLayoutResource(0);
        }
    }
    
    private InputMethodManager getInputMethodManager() {
        return (InputMethodManager)this.getContext().getSystemService("input_method");
    }
    
    private String getSummaryString() {
        return InputMethodAndSubtypeUtil.getSubtypeLocaleNameListAsSentence(this.getInputMethodManager().getEnabledInputMethodSubtypeList(this.mImi, true), this.getContext(), this.mImi);
    }
    
    private boolean isImeEnabler() {
        return this.getWidgetLayoutResource() != 0;
    }
    
    private boolean isTv() {
        return (this.getContext().getResources().getConfiguration().uiMode & 0xF) == 0x4;
    }
    
    private void setCheckedInternal(final boolean checked) {
        super.setChecked(checked);
        this.mOnSaveListener.onSaveInputMethodPreference(this);
        this.notifyChanged();
    }
    
    private void showDirectBootWarnDialog() {
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        final Context context = this.getContext();
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(context);
        alertDialog$Builder.setCancelable(true);
        alertDialog$Builder.setMessage(context.getText(R.string.direct_boot_unaware_dialog_message));
        alertDialog$Builder.setPositiveButton(17039370, (DialogInterface$OnClickListener)new _$$Lambda$InputMethodPreference$FTfMqDGTv2yWgiGfPYaiYBCHriY(this));
        alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)new _$$Lambda$InputMethodPreference$_R1WCgG1LabBNKieYWiJs9NnYv4(this));
        (this.mDialog = alertDialog$Builder.create()).show();
    }
    
    private void showSecurityWarnDialog() {
        if (this.mDialog != null && this.mDialog.isShowing()) {
            this.mDialog.dismiss();
        }
        final Context context = this.getContext();
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder(context);
        alertDialog$Builder.setCancelable(true);
        alertDialog$Builder.setTitle(17039380);
        alertDialog$Builder.setMessage((CharSequence)context.getString(R.string.ime_security_warning, new Object[] { this.mImi.getServiceInfo().applicationInfo.loadLabel(context.getPackageManager()) }));
        alertDialog$Builder.setPositiveButton(17039370, (DialogInterface$OnClickListener)new _$$Lambda$InputMethodPreference$pHt4_6FWRQ9Ts6PuJy_AB14MhJc(this));
        alertDialog$Builder.setNegativeButton(17039360, (DialogInterface$OnClickListener)new _$$Lambda$InputMethodPreference$HH5dtwzFZv06UNDXJAO6Cyx4kxo(this));
        (this.mDialog = alertDialog$Builder.create()).show();
    }
    
    public int compareTo(final InputMethodPreference inputMethodPreference, final Collator collator) {
        int n = 0;
        if (this == inputMethodPreference) {
            return 0;
        }
        final boolean mHasPriorityInSorting = this.mHasPriorityInSorting;
        final boolean mHasPriorityInSorting2 = inputMethodPreference.mHasPriorityInSorting;
        int n2 = -1;
        if (mHasPriorityInSorting != mHasPriorityInSorting2) {
            if (!this.mHasPriorityInSorting) {
                n2 = 1;
            }
            return n2;
        }
        final CharSequence title = this.getTitle();
        final CharSequence title2 = inputMethodPreference.getTitle();
        final boolean empty = TextUtils.isEmpty(title);
        final boolean empty2 = TextUtils.isEmpty(title2);
        if (!empty && !empty2) {
            return collator.compare(title.toString(), title2.toString());
        }
        int n3;
        if (empty) {
            n3 = -1;
        }
        else {
            n3 = 0;
        }
        if (empty2) {
            n = -1;
        }
        return n3 - n;
    }
    
    @Override
    public boolean onPreferenceChange(final Preference preference, final Object o) {
        if (!this.isImeEnabler()) {
            return false;
        }
        if (this.isChecked()) {
            this.setCheckedInternal(false);
            return false;
        }
        if (InputMethodUtils.isSystemIme(this.mImi)) {
            if (!this.mImi.getServiceInfo().directBootAware && !this.isTv()) {
                if (!this.isTv()) {
                    this.showDirectBootWarnDialog();
                }
            }
            else {
                this.setCheckedInternal(true);
            }
        }
        else {
            this.showSecurityWarnDialog();
        }
        return false;
    }
    
    @Override
    public boolean onPreferenceClick(Preference context) {
        if (this.isImeEnabler()) {
            return true;
        }
        context = (Preference)this.getContext();
        try {
            final Intent intent = this.getIntent();
            if (intent != null) {
                ((Context)context).startActivity(intent);
            }
        }
        catch (ActivityNotFoundException ex) {
            Log.d(InputMethodPreference.TAG, "IME's Settings Activity Not Found", (Throwable)ex);
            Toast.makeText((Context)context, (CharSequence)((Context)context).getString(R.string.failed_to_open_app_settings_toast, new Object[] { this.mImi.loadLabel(((Context)context).getPackageManager()) }), 1).show();
        }
        return true;
    }
    
    public void updatePreferenceViews() {
        if (this.mInputMethodSettingValues.isAlwaysCheckedIme(this.mImi, this.getContext()) && this.isImeEnabler()) {
            this.setDisabledByAdmin(null);
            this.setEnabled(false);
        }
        else if (!this.mIsAllowedByOrganization) {
            this.setDisabledByAdmin(RestrictedLockUtils.checkIfInputMethodDisallowed(this.getContext(), this.mImi.getPackageName(), UserHandle.myUserId()));
        }
        else {
            this.setEnabled(true);
        }
        this.setChecked(this.mInputMethodSettingValues.isEnabledImi(this.mImi));
        if (!this.isDisabledByAdmin()) {
            this.setSummary(this.getSummaryString());
        }
    }
    
    public interface OnSavePreferenceListener
    {
        void onSaveInputMethodPreference(final InputMethodPreference p0);
    }
}
