package com.android.settingslib.inputmethod;

import android.os.UserHandle;
import com.android.settingslib.RestrictedLockUtils;
import android.content.ActivityNotFoundException;
import android.widget.Toast;
import android.util.Log;
import java.text.Collator;
import com.android.settingslib.R;
import android.app.AlertDialog$Builder;
import android.view.inputmethod.InputMethodSubtype;
import java.util.List;
import android.view.inputmethod.InputMethodManager;
import com.android.internal.inputmethod.InputMethodUtils;
import android.content.Intent;
import android.text.TextUtils;
import android.content.Context;
import android.view.inputmethod.InputMethodInfo;
import android.app.AlertDialog;
import android.support.v7.preference.Preference;
import com.android.settingslib.RestrictedSwitchPreference;
import android.content.DialogInterface;
import android.content.DialogInterface$OnClickListener;

public final class _$$Lambda$InputMethodPreference$FTfMqDGTv2yWgiGfPYaiYBCHriY implements DialogInterface$OnClickListener
{
    public final void onClick(final DialogInterface dialogInterface, final int n) {
        InputMethodPreference.lambda$showDirectBootWarnDialog$2(this.f$0, dialogInterface, n);
    }
}
