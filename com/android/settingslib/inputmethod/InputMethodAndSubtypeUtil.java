package com.android.settingslib.inputmethod;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.support.v7.preference.PreferenceScreen;
import com.android.internal.inputmethod.InputMethodUtils;
import android.content.SharedPreferences;
import android.support.v7.preference.TwoStatePreference;
import android.support.v7.preference.Preference;
import java.util.Map;
import android.support.v14.preference.PreferenceFragment;
import android.icu.text.ListFormatter;
import java.util.List;
import com.android.internal.app.LocaleHelper;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodSubtype;
import android.provider.Settings$SettingNotFoundException;
import android.content.res.Configuration;
import java.util.Locale;
import android.content.Context;
import android.text.TextUtils;
import android.provider.Settings;
import android.content.ContentResolver;
import java.util.Iterator;
import java.util.HashSet;
import java.util.HashMap;
import android.text.TextUtils$SimpleStringSplitter;

public class InputMethodAndSubtypeUtil
{
    private static final TextUtils$SimpleStringSplitter sStringInputMethodSplitter;
    private static final TextUtils$SimpleStringSplitter sStringInputMethodSubtypeSplitter;
    
    static {
        sStringInputMethodSplitter = new TextUtils$SimpleStringSplitter(':');
        sStringInputMethodSubtypeSplitter = new TextUtils$SimpleStringSplitter(';');
    }
    
    private static String buildInputMethodsAndSubtypesString(final HashMap<String, HashSet<String>> hashMap) {
        final StringBuilder sb = new StringBuilder();
        for (final String s : hashMap.keySet()) {
            if (sb.length() > 0) {
                sb.append(':');
            }
            final HashSet<String> set = hashMap.get(s);
            sb.append(s);
            for (final String s2 : set) {
                sb.append(';');
                sb.append(s2);
            }
        }
        return sb.toString();
    }
    
    private static String buildInputMethodsString(final HashSet<String> set) {
        final StringBuilder sb = new StringBuilder();
        for (final String s : set) {
            if (sb.length() > 0) {
                sb.append(':');
            }
            sb.append(s);
        }
        return sb.toString();
    }
    
    private static HashSet<String> getDisabledSystemIMEs(final ContentResolver contentResolver) {
        final HashSet<String> set = new HashSet<String>();
        final String string = Settings.Secure.getString(contentResolver, "disabled_system_input_methods");
        if (TextUtils.isEmpty((CharSequence)string)) {
            return set;
        }
        InputMethodAndSubtypeUtil.sStringInputMethodSplitter.setString(string);
        while (InputMethodAndSubtypeUtil.sStringInputMethodSplitter.hasNext()) {
            set.add(InputMethodAndSubtypeUtil.sStringInputMethodSplitter.next());
        }
        return set;
    }
    
    private static Locale getDisplayLocale(final Context context) {
        if (context == null) {
            return Locale.getDefault();
        }
        if (context.getResources() == null) {
            return Locale.getDefault();
        }
        final Configuration configuration = context.getResources().getConfiguration();
        if (configuration == null) {
            return Locale.getDefault();
        }
        final Locale value = configuration.getLocales().get(0);
        if (value == null) {
            return Locale.getDefault();
        }
        return value;
    }
    
    private static HashMap<String, HashSet<String>> getEnabledInputMethodsAndSubtypeList(final ContentResolver contentResolver) {
        return parseInputMethodsAndSubtypesString(Settings.Secure.getString(contentResolver, "enabled_input_methods"));
    }
    
    private static int getInputMethodSubtypeSelected(final ContentResolver contentResolver) {
        try {
            return Settings.Secure.getInt(contentResolver, "selected_input_method_subtype");
        }
        catch (Settings$SettingNotFoundException ex) {
            return -1;
        }
    }
    
    public static String getSubtypeLocaleNameAsSentence(final InputMethodSubtype inputMethodSubtype, final Context context, final InputMethodInfo inputMethodInfo) {
        if (inputMethodSubtype == null) {
            return "";
        }
        return LocaleHelper.toSentenceCase(inputMethodSubtype.getDisplayName(context, inputMethodInfo.getPackageName(), inputMethodInfo.getServiceInfo().applicationInfo).toString(), getDisplayLocale(context));
    }
    
    public static String getSubtypeLocaleNameListAsSentence(final List<InputMethodSubtype> list, final Context context, final InputMethodInfo inputMethodInfo) {
        if (list.isEmpty()) {
            return "";
        }
        final Locale displayLocale = getDisplayLocale(context);
        final int size = list.size();
        final CharSequence[] array = new CharSequence[size];
        for (int i = 0; i < size; ++i) {
            array[i] = list.get(i).getDisplayName(context, inputMethodInfo.getPackageName(), inputMethodInfo.getServiceInfo().applicationInfo);
        }
        return LocaleHelper.toSentenceCase(ListFormatter.getInstance(displayLocale).format((Object[])array), displayLocale);
    }
    
    private static boolean isInputMethodSubtypeSelected(final ContentResolver contentResolver) {
        return getInputMethodSubtypeSelected(contentResolver) != -1;
    }
    
    public static void loadInputMethodSubtypeList(final PreferenceFragment preferenceFragment, final ContentResolver contentResolver, final List<InputMethodInfo> list, final Map<String, List<Preference>> map) {
        final HashMap<String, HashSet<String>> enabledInputMethodsAndSubtypeList = getEnabledInputMethodsAndSubtypeList(contentResolver);
        final Iterator<InputMethodInfo> iterator = list.iterator();
        while (iterator.hasNext()) {
            final String id = iterator.next().getId();
            final Preference preference = preferenceFragment.findPreference(id);
            if (preference instanceof TwoStatePreference) {
                final TwoStatePreference twoStatePreference = (TwoStatePreference)preference;
                final boolean containsKey = enabledInputMethodsAndSubtypeList.containsKey(id);
                twoStatePreference.setChecked(containsKey);
                if (map != null) {
                    final Iterator<Preference> iterator2 = map.get(id).iterator();
                    while (iterator2.hasNext()) {
                        iterator2.next().setEnabled(containsKey);
                    }
                }
                setSubtypesPreferenceEnabled(preferenceFragment, list, id, containsKey);
            }
        }
        updateSubtypesPreferenceChecked(preferenceFragment, list, enabledInputMethodsAndSubtypeList);
    }
    
    private static HashMap<String, HashSet<String>> parseInputMethodsAndSubtypesString(String next) {
        final HashMap<String, HashSet<String>> hashMap = new HashMap<String, HashSet<String>>();
        if (TextUtils.isEmpty((CharSequence)next)) {
            return hashMap;
        }
        InputMethodAndSubtypeUtil.sStringInputMethodSplitter.setString(next);
        while (InputMethodAndSubtypeUtil.sStringInputMethodSplitter.hasNext()) {
            next = InputMethodAndSubtypeUtil.sStringInputMethodSplitter.next();
            InputMethodAndSubtypeUtil.sStringInputMethodSubtypeSplitter.setString(next);
            if (InputMethodAndSubtypeUtil.sStringInputMethodSubtypeSplitter.hasNext()) {
                final HashSet<String> set = new HashSet<String>();
                final String next2 = InputMethodAndSubtypeUtil.sStringInputMethodSubtypeSplitter.next();
                while (InputMethodAndSubtypeUtil.sStringInputMethodSubtypeSplitter.hasNext()) {
                    set.add(InputMethodAndSubtypeUtil.sStringInputMethodSubtypeSplitter.next());
                }
                hashMap.put(next2, set);
            }
        }
        return hashMap;
    }
    
    private static void putSelectedInputMethodSubtype(final ContentResolver contentResolver, final int n) {
        Settings.Secure.putInt(contentResolver, "selected_input_method_subtype", n);
    }
    
    public static void removeUnnecessaryNonPersistentPreference(final Preference preference) {
        final String key = preference.getKey();
        if (!preference.isPersistent() && key != null) {
            final SharedPreferences sharedPreferences = preference.getSharedPreferences();
            if (sharedPreferences != null && sharedPreferences.contains(key)) {
                sharedPreferences.edit().remove(key).apply();
            }
        }
    }
    
    public static void saveInputMethodSubtypeList(final PreferenceFragment preferenceFragment, final ContentResolver contentResolver, final List<InputMethodInfo> list, final boolean b) {
        final String string = Settings.Secure.getString(contentResolver, "default_input_method");
        final int inputMethodSubtypeSelected = getInputMethodSubtypeSelected(contentResolver);
        final HashMap<String, HashSet<String>> enabledInputMethodsAndSubtypeList = getEnabledInputMethodsAndSubtypeList(contentResolver);
        final HashSet<String> disabledSystemIMEs = getDisabledSystemIMEs(contentResolver);
        boolean b2 = false;
        final Iterator<InputMethodInfo> iterator = list.iterator();
        String s = string;
        while (iterator.hasNext()) {
            final InputMethodInfo inputMethodInfo = iterator.next();
            final String id = inputMethodInfo.getId();
            final Preference preference = preferenceFragment.findPreference(id);
            if (preference == null) {
                continue;
            }
            boolean b3;
            if (preference instanceof TwoStatePreference) {
                b3 = ((TwoStatePreference)preference).isChecked();
            }
            else {
                b3 = enabledInputMethodsAndSubtypeList.containsKey(id);
            }
            final boolean equals = id.equals(s);
            final boolean systemIme = InputMethodUtils.isSystemIme(inputMethodInfo);
            if ((!b && InputMethodSettingValuesWrapper.getInstance((Context)preferenceFragment.getActivity()).isAlwaysCheckedIme(inputMethodInfo, (Context)preferenceFragment.getActivity())) || b3) {
                if (!enabledInputMethodsAndSubtypeList.containsKey(id)) {
                    enabledInputMethodsAndSubtypeList.put(id, new HashSet<String>());
                }
                final HashSet<String> set = enabledInputMethodsAndSubtypeList.get(id);
                int n = 0;
                for (int subtypeCount = inputMethodInfo.getSubtypeCount(), i = 0; i < subtypeCount; ++i) {
                    final InputMethodSubtype subtype = inputMethodInfo.getSubtypeAt(i);
                    final String value = String.valueOf(subtype.hashCode());
                    final StringBuilder sb = new StringBuilder();
                    sb.append(id);
                    sb.append(value);
                    final TwoStatePreference twoStatePreference = (TwoStatePreference)preferenceFragment.findPreference(sb.toString());
                    if (twoStatePreference != null) {
                        if (n == 0) {
                            set.clear();
                            b2 = true;
                            n = 1;
                        }
                        if (twoStatePreference.isEnabled() && twoStatePreference.isChecked()) {
                            set.add(value);
                            if (equals && inputMethodSubtypeSelected == subtype.hashCode()) {
                                b2 = false;
                            }
                        }
                        else {
                            set.remove(value);
                        }
                    }
                }
            }
            else {
                enabledInputMethodsAndSubtypeList.remove(id);
                if (equals) {
                    s = null;
                }
            }
            if (!systemIme || !b) {
                continue;
            }
            if (disabledSystemIMEs.contains(id)) {
                if (!b3) {
                    continue;
                }
                disabledSystemIMEs.remove(id);
            }
            else {
                if (b3) {
                    continue;
                }
                disabledSystemIMEs.add(id);
            }
        }
        final String buildInputMethodsAndSubtypesString = buildInputMethodsAndSubtypesString(enabledInputMethodsAndSubtypeList);
        final String buildInputMethodsString = buildInputMethodsString(disabledSystemIMEs);
        if (b2 || !isInputMethodSubtypeSelected(contentResolver)) {
            putSelectedInputMethodSubtype(contentResolver, -1);
        }
        Settings.Secure.putString(contentResolver, "enabled_input_methods", buildInputMethodsAndSubtypesString);
        if (buildInputMethodsString.length() > 0) {
            Settings.Secure.putString(contentResolver, "disabled_system_input_methods", buildInputMethodsString);
        }
        if (s == null) {
            s = "";
        }
        Settings.Secure.putString(contentResolver, "default_input_method", s);
    }
    
    private static void setSubtypesPreferenceEnabled(final PreferenceFragment preferenceFragment, final List<InputMethodInfo> list, final String s, final boolean enabled) {
        final PreferenceScreen preferenceScreen = preferenceFragment.getPreferenceScreen();
        for (final InputMethodInfo inputMethodInfo : list) {
            if (s.equals(inputMethodInfo.getId())) {
                for (int subtypeCount = inputMethodInfo.getSubtypeCount(), i = 0; i < subtypeCount; ++i) {
                    final InputMethodSubtype subtype = inputMethodInfo.getSubtypeAt(i);
                    final StringBuilder sb = new StringBuilder();
                    sb.append(s);
                    sb.append(subtype.hashCode());
                    final TwoStatePreference twoStatePreference = (TwoStatePreference)preferenceScreen.findPreference(sb.toString());
                    if (twoStatePreference != null) {
                        twoStatePreference.setEnabled(enabled);
                    }
                }
            }
        }
    }
    
    private static void updateSubtypesPreferenceChecked(final PreferenceFragment preferenceFragment, final List<InputMethodInfo> list, final HashMap<String, HashSet<String>> hashMap) {
        final PreferenceScreen preferenceScreen = preferenceFragment.getPreferenceScreen();
        for (final InputMethodInfo inputMethodInfo : list) {
            final String id = inputMethodInfo.getId();
            if (!hashMap.containsKey(id)) {
                continue;
            }
            final HashSet<String> set = hashMap.get(id);
            for (int subtypeCount = inputMethodInfo.getSubtypeCount(), i = 0; i < subtypeCount; ++i) {
                final String value = String.valueOf(inputMethodInfo.getSubtypeAt(i).hashCode());
                final StringBuilder sb = new StringBuilder();
                sb.append(id);
                sb.append(value);
                final TwoStatePreference twoStatePreference = (TwoStatePreference)preferenceScreen.findPreference(sb.toString());
                if (twoStatePreference != null) {
                    twoStatePreference.setChecked(set.contains(value));
                }
            }
        }
    }
}
