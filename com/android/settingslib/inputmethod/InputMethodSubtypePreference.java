package com.android.settingslib.inputmethod;

import java.text.Collator;
import android.support.v7.preference.Preference;
import com.android.internal.inputmethod.InputMethodUtils;
import android.text.TextUtils;
import java.util.Locale;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodSubtype;
import android.content.Context;

public class InputMethodSubtypePreference extends SwitchWithNoTextPreference
{
    private final boolean mIsSystemLanguage;
    private final boolean mIsSystemLocale;
    
    public InputMethodSubtypePreference(final Context context, final InputMethodSubtype inputMethodSubtype, final InputMethodInfo inputMethodInfo) {
        final StringBuilder sb = new StringBuilder();
        sb.append(inputMethodInfo.getId());
        sb.append(inputMethodSubtype.hashCode());
        this(context, sb.toString(), InputMethodAndSubtypeUtil.getSubtypeLocaleNameAsSentence(inputMethodSubtype, context, inputMethodInfo), inputMethodSubtype.getLocale(), context.getResources().getConfiguration().locale);
    }
    
    InputMethodSubtypePreference(final Context context, final String key, final CharSequence title, final String s, final Locale locale) {
        super(context);
        boolean mIsSystemLanguage = false;
        this.setPersistent(false);
        this.setKey(key);
        this.setTitle(title);
        if (TextUtils.isEmpty((CharSequence)s)) {
            this.mIsSystemLocale = false;
            this.mIsSystemLanguage = false;
        }
        else {
            this.mIsSystemLocale = s.equals(locale.toString());
            if (this.mIsSystemLocale || InputMethodUtils.getLanguageFromLocaleString(s).equals(locale.getLanguage())) {
                mIsSystemLanguage = true;
            }
            this.mIsSystemLanguage = mIsSystemLanguage;
        }
    }
    
    public int compareTo(final Preference preference, final Collator collator) {
        int n = 0;
        if (this == preference) {
            return 0;
        }
        if (!(preference instanceof InputMethodSubtypePreference)) {
            return super.compareTo(preference);
        }
        final InputMethodSubtypePreference inputMethodSubtypePreference = (InputMethodSubtypePreference)preference;
        if (this.mIsSystemLocale && !inputMethodSubtypePreference.mIsSystemLocale) {
            return -1;
        }
        if (!this.mIsSystemLocale && inputMethodSubtypePreference.mIsSystemLocale) {
            return 1;
        }
        if (this.mIsSystemLanguage && !inputMethodSubtypePreference.mIsSystemLanguage) {
            return -1;
        }
        if (!this.mIsSystemLanguage && inputMethodSubtypePreference.mIsSystemLanguage) {
            return 1;
        }
        final CharSequence title = this.getTitle();
        final CharSequence title2 = preference.getTitle();
        final boolean empty = TextUtils.isEmpty(title);
        final boolean empty2 = TextUtils.isEmpty(title2);
        if (!empty && !empty2) {
            return collator.compare(title.toString(), title2.toString());
        }
        int n2;
        if (empty) {
            n2 = -1;
        }
        else {
            n2 = 0;
        }
        if (empty2) {
            n = -1;
        }
        return n2 - n;
    }
}
