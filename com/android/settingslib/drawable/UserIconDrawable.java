package com.android.settingslib.drawable;

import android.graphics.BitmapShader;
import android.graphics.Shader$TileMode;
import android.graphics.Shader;
import android.graphics.Matrix$ScaleToFit;
import android.graphics.RectF;
import android.graphics.Bitmap$Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable$ConstantState;
import android.graphics.PorterDuffColorFilter;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.Canvas;
import com.android.settingslib.R;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.Matrix;
import android.content.res.ColorStateList;
import android.graphics.Paint;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable$Callback;
import android.graphics.drawable.Drawable;

public class UserIconDrawable extends Drawable implements Drawable$Callback
{
    private Drawable mBadge;
    private float mBadgeMargin;
    private float mBadgeRadius;
    private Bitmap mBitmap;
    private Paint mClearPaint;
    private float mDisplayRadius;
    private ColorStateList mFrameColor;
    private float mFramePadding;
    private Paint mFramePaint;
    private float mFrameWidth;
    private final Matrix mIconMatrix;
    private final Paint mIconPaint;
    private float mIntrinsicRadius;
    private boolean mInvalidated;
    private float mPadding;
    private final Paint mPaint;
    private int mSize;
    private ColorStateList mTintColor;
    private PorterDuff.Mode mTintMode;
    private Drawable mUserDrawable;
    private Bitmap mUserIcon;
    
    public UserIconDrawable() {
        this(0);
    }
    
    public UserIconDrawable(final int intrinsicSize) {
        this.mIconPaint = new Paint();
        this.mPaint = new Paint();
        this.mIconMatrix = new Matrix();
        this.mPadding = 0.0f;
        this.mSize = 0;
        this.mInvalidated = true;
        this.mTintColor = null;
        this.mTintMode = PorterDuff.Mode.SRC_ATOP;
        this.mFrameColor = null;
        this.mIconPaint.setAntiAlias(true);
        this.mIconPaint.setFilterBitmap(true);
        this.mPaint.setFilterBitmap(true);
        this.mPaint.setAntiAlias(true);
        if (intrinsicSize > 0) {
            this.setBounds(0, 0, intrinsicSize, intrinsicSize);
            this.setIntrinsicSize(intrinsicSize);
        }
        this.setIcon(null);
    }
    
    private static Drawable getDrawableForDisplayDensity(final Context context, final int n) {
        return context.getResources().getDrawableForDensity(n, context.getResources().getDisplayMetrics().densityDpi, context.getTheme());
    }
    
    public static Drawable getManagedUserDrawable(final Context context) {
        return getDrawableForDisplayDensity(context, 17302335);
    }
    
    public static int getSizeForList(final Context context) {
        return (int)context.getResources().getDimension(R.dimen.circle_avatar_size);
    }
    
    private void rebake() {
        this.mInvalidated = false;
        if (this.mBitmap != null && (this.mUserDrawable != null || this.mUserIcon != null)) {
            final Canvas canvas = new Canvas(this.mBitmap);
            canvas.drawColor(0, PorterDuff.Mode.CLEAR);
            if (this.mUserDrawable != null) {
                this.mUserDrawable.draw(canvas);
            }
            else if (this.mUserIcon != null) {
                final int save = canvas.save();
                canvas.concat(this.mIconMatrix);
                canvas.drawCircle(this.mUserIcon.getWidth() * 0.5f, this.mUserIcon.getHeight() * 0.5f, this.mIntrinsicRadius, this.mIconPaint);
                canvas.restoreToCount(save);
            }
            if (this.mFrameColor != null) {
                this.mFramePaint.setColor(this.mFrameColor.getColorForState(this.getState(), 0));
            }
            if (this.mFrameWidth + this.mFramePadding > 0.001f) {
                canvas.drawCircle(this.getBounds().exactCenterX(), this.getBounds().exactCenterY(), this.mDisplayRadius - this.mPadding - this.mFrameWidth * 0.5f, this.mFramePaint);
            }
            if (this.mBadge != null && this.mBadgeRadius > 0.001f) {
                final float n = this.mBadgeRadius * 2.0f;
                final float n2 = this.mBitmap.getHeight() - n;
                final float n3 = this.mBitmap.getWidth() - n;
                this.mBadge.setBounds((int)n3, (int)n2, (int)(n3 + n), (int)(n2 + n));
                canvas.drawCircle(this.mBadgeRadius + n3, this.mBadgeRadius + n2, this.mBadge.getBounds().width() * 0.5f + this.mBadgeMargin, this.mClearPaint);
                this.mBadge.draw(canvas);
            }
        }
    }
    
    public UserIconDrawable bake() {
        if (this.mSize > 0) {
            this.onBoundsChange(new Rect(0, 0, this.mSize, this.mSize));
            this.rebake();
            this.mFrameColor = null;
            this.mFramePaint = null;
            this.mClearPaint = null;
            if (this.mUserDrawable != null) {
                this.mUserDrawable.setCallback((Drawable$Callback)null);
                this.mUserDrawable = null;
            }
            else if (this.mUserIcon != null) {
                this.mUserIcon.recycle();
                this.mUserIcon = null;
            }
            return this;
        }
        throw new IllegalStateException("Baking requires an explicit intrinsic size");
    }
    
    public void draw(final Canvas canvas) {
        if (this.mInvalidated) {
            this.rebake();
        }
        if (this.mBitmap != null) {
            if (this.mTintColor == null) {
                this.mPaint.setColorFilter((ColorFilter)null);
            }
            else {
                final int colorForState = this.mTintColor.getColorForState(this.getState(), this.mTintColor.getDefaultColor());
                if (this.mPaint.getColorFilter() == null) {
                    this.mPaint.setColorFilter((ColorFilter)new PorterDuffColorFilter(colorForState, this.mTintMode));
                }
                else {
                    ((PorterDuffColorFilter)this.mPaint.getColorFilter()).setMode(this.mTintMode);
                    ((PorterDuffColorFilter)this.mPaint.getColorFilter()).setColor(colorForState);
                }
            }
            canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, this.mPaint);
        }
    }
    
    public Drawable$ConstantState getConstantState() {
        return new BitmapDrawable(this.mBitmap).getConstantState();
    }
    
    public int getIntrinsicHeight() {
        return this.getIntrinsicWidth();
    }
    
    public int getIntrinsicWidth() {
        int mSize;
        if (this.mSize <= 0) {
            mSize = (int)this.mIntrinsicRadius * 2;
        }
        else {
            mSize = this.mSize;
        }
        return mSize;
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void invalidateDrawable(final Drawable drawable) {
        this.invalidateSelf();
    }
    
    public void invalidateSelf() {
        super.invalidateSelf();
        this.mInvalidated = true;
    }
    
    public boolean isStateful() {
        return this.mFrameColor != null && this.mFrameColor.isStateful();
    }
    
    protected void onBoundsChange(final Rect rect) {
        if (!rect.isEmpty() && (this.mUserIcon != null || this.mUserDrawable != null)) {
            final float mDisplayRadius = Math.min(rect.width(), rect.height()) * 0.5f;
            final int n = (int)(mDisplayRadius * 2.0f);
            if (this.mBitmap == null || n != (int)(this.mDisplayRadius * 2.0f)) {
                this.mDisplayRadius = mDisplayRadius;
                if (this.mBitmap != null) {
                    this.mBitmap.recycle();
                }
                this.mBitmap = Bitmap.createBitmap(n, n, Bitmap$Config.ARGB_8888);
            }
            this.mDisplayRadius = Math.min(rect.width(), rect.height()) * 0.5f;
            final float n2 = this.mDisplayRadius - this.mFrameWidth - this.mFramePadding - this.mPadding;
            final RectF rectF = new RectF(rect.exactCenterX() - n2, rect.exactCenterY() - n2, rect.exactCenterX() + n2, rect.exactCenterY() + n2);
            if (this.mUserDrawable != null) {
                final Rect bounds = new Rect();
                rectF.round(bounds);
                this.mIntrinsicRadius = Math.min(this.mUserDrawable.getIntrinsicWidth(), this.mUserDrawable.getIntrinsicHeight()) * 0.5f;
                this.mUserDrawable.setBounds(bounds);
            }
            else if (this.mUserIcon != null) {
                final float n3 = this.mUserIcon.getWidth() * 0.5f;
                final float n4 = this.mUserIcon.getHeight() * 0.5f;
                this.mIntrinsicRadius = Math.min(n3, n4);
                this.mIconMatrix.setRectToRect(new RectF(n3 - this.mIntrinsicRadius, n4 - this.mIntrinsicRadius, this.mIntrinsicRadius + n3, this.mIntrinsicRadius + n4), rectF, Matrix$ScaleToFit.FILL);
            }
            this.invalidateSelf();
        }
    }
    
    public void scheduleDrawable(final Drawable drawable, final Runnable runnable, final long n) {
        this.scheduleSelf(runnable, n);
    }
    
    public void setAlpha(final int alpha) {
        this.mPaint.setAlpha(alpha);
        super.invalidateSelf();
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
    }
    
    public UserIconDrawable setIcon(final Bitmap mUserIcon) {
        if (this.mUserDrawable != null) {
            this.mUserDrawable.setCallback((Drawable$Callback)null);
            this.mUserDrawable = null;
        }
        this.mUserIcon = mUserIcon;
        if (this.mUserIcon == null) {
            this.mIconPaint.setShader((Shader)null);
            this.mBitmap = null;
        }
        else {
            this.mIconPaint.setShader((Shader)new BitmapShader(mUserIcon, Shader$TileMode.CLAMP, Shader$TileMode.CLAMP));
        }
        this.onBoundsChange(this.getBounds());
        return this;
    }
    
    public UserIconDrawable setIconDrawable(final Drawable mUserDrawable) {
        if (this.mUserDrawable != null) {
            this.mUserDrawable.setCallback((Drawable$Callback)null);
        }
        this.mUserIcon = null;
        this.mUserDrawable = mUserDrawable;
        if (this.mUserDrawable == null) {
            this.mBitmap = null;
        }
        else {
            this.mUserDrawable.setCallback((Drawable$Callback)this);
        }
        this.onBoundsChange(this.getBounds());
        return this;
    }
    
    public void setIntrinsicSize(final int mSize) {
        this.mSize = mSize;
    }
    
    public void setTintList(final ColorStateList mTintColor) {
        this.mTintColor = mTintColor;
        super.invalidateSelf();
    }
    
    public void setTintMode(final PorterDuff.Mode mTintMode) {
        this.mTintMode = mTintMode;
        super.invalidateSelf();
    }
    
    public void unscheduleDrawable(final Drawable drawable, final Runnable runnable) {
        this.unscheduleSelf(runnable);
    }
}
