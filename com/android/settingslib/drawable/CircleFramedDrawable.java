package com.android.settingslib.drawable;

import android.graphics.ColorFilter;
import com.android.settingslib.R;
import android.content.Context;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.Path;
import android.graphics.Canvas;
import android.graphics.Bitmap$Config;
import android.graphics.Rect;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class CircleFramedDrawable extends Drawable
{
    private final Bitmap mBitmap;
    private RectF mDstRect;
    private final Paint mPaint;
    private float mScale;
    private final int mSize;
    private Rect mSrcRect;
    
    public CircleFramedDrawable(final Bitmap bitmap, int height) {
        this.mSize = height;
        this.mBitmap = Bitmap.createBitmap(this.mSize, this.mSize, Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(this.mBitmap);
        final int width = bitmap.getWidth();
        height = bitmap.getHeight();
        final int min = Math.min(width, height);
        final Rect rect = new Rect((width - min) / 2, (height - min) / 2, min, min);
        final RectF rectF = new RectF(0.0f, 0.0f, (float)this.mSize, (float)this.mSize);
        final Path path = new Path();
        path.addArc(rectF, 0.0f, 360.0f);
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        (this.mPaint = new Paint()).setAntiAlias(true);
        this.mPaint.setColor(-16777216);
        this.mPaint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, this.mPaint);
        this.mPaint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rectF, this.mPaint);
        this.mPaint.setXfermode((Xfermode)null);
        this.mScale = 1.0f;
        this.mSrcRect = new Rect(0, 0, this.mSize, this.mSize);
        this.mDstRect = new RectF(0.0f, 0.0f, (float)this.mSize, (float)this.mSize);
    }
    
    public static CircleFramedDrawable getInstance(final Context context, final Bitmap bitmap) {
        return new CircleFramedDrawable(bitmap, (int)context.getResources().getDimension(R.dimen.circle_avatar_size));
    }
    
    public void draw(final Canvas canvas) {
        final float n = (this.mSize - this.mScale * this.mSize) / 2.0f;
        this.mDstRect.set(n, n, this.mSize - n, this.mSize - n);
        canvas.drawBitmap(this.mBitmap, this.mSrcRect, this.mDstRect, (Paint)null);
    }
    
    public int getIntrinsicHeight() {
        return this.mSize;
    }
    
    public int getIntrinsicWidth() {
        return this.mSize;
    }
    
    public int getOpacity() {
        return -3;
    }
    
    public void setAlpha(final int n) {
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
    }
}
