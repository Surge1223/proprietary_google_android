package com.android.settingslib.display;

import android.util.Log;
import android.os.RemoteException;
import android.view.WindowManagerGlobal;
import android.os.AsyncTask;
import android.os.UserHandle;
import android.content.res.Resources;
import java.util.Arrays;
import android.util.MathUtils;
import android.util.DisplayMetrics;
import android.content.Context;
import com.android.settingslib.R;

public class DisplayDensityUtils
{
    private static final int[] SUMMARIES_LARGER;
    private static final int[] SUMMARIES_SMALLER;
    private static final int SUMMARY_CUSTOM;
    public static final int SUMMARY_DEFAULT;
    private final int mCurrentIndex;
    private final int mDefaultDensity;
    private final String[] mEntries;
    private final int[] mValues;
    
    static {
        SUMMARY_DEFAULT = R.string.screen_zoom_summary_default;
        SUMMARY_CUSTOM = R.string.screen_zoom_summary_custom;
        SUMMARIES_SMALLER = new int[] { R.string.screen_zoom_summary_small };
        SUMMARIES_LARGER = new int[] { R.string.screen_zoom_summary_large, R.string.screen_zoom_summary_very_large, R.string.screen_zoom_summary_extremely_large };
    }
    
    public DisplayDensityUtils(final Context context) {
        final int defaultDisplayDensity = getDefaultDisplayDensity(0);
        if (defaultDisplayDensity <= 0) {
            this.mEntries = null;
            this.mValues = null;
            this.mDefaultDensity = 0;
            this.mCurrentIndex = -1;
            return;
        }
        final Resources resources = context.getResources();
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getDisplay().getRealMetrics(displayMetrics);
        final int densityDpi = displayMetrics.densityDpi;
        int n = -1;
        final float min = Math.min(1.5f, 160 * Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels) / 320 / defaultDisplayDensity);
        final int n2 = (int)MathUtils.constrain((min - 1.0f) / 0.09f, 0.0f, (float)DisplayDensityUtils.SUMMARIES_LARGER.length);
        int n3 = (int)MathUtils.constrain(1.6666664f, 0.0f, (float)DisplayDensityUtils.SUMMARIES_SMALLER.length);
        final String[] array = new String[1 + n3 + n2];
        final int[] array2 = new int[array.length];
        int n4 = 0;
        int n5 = 0;
        int n6 = n;
        if (n3 > 0) {
            final float n7 = 0.14999998f / n3;
            --n3;
            while (true) {
                n6 = n;
                n4 = n5;
                if (n3 < 0) {
                    break;
                }
                final int n8 = (int)(defaultDisplayDensity * (1.0f - (n3 + 1) * n7)) & 0xFFFFFFFE;
                if (densityDpi == n8) {
                    n = n5;
                }
                array[n5] = resources.getString(DisplayDensityUtils.SUMMARIES_SMALLER[n3]);
                array2[n5] = n8;
                ++n5;
                --n3;
            }
        }
        if (densityDpi == defaultDisplayDensity) {
            n6 = n4;
        }
        array2[n4] = defaultDisplayDensity;
        array[n4] = resources.getString(DisplayDensityUtils.SUMMARY_DEFAULT);
        final int n9 = n4 + 1;
        int n10 = n6;
        int mCurrentIndex = n9;
        if (n2 > 0) {
            final float n11 = (min - 1.0f) / n2;
            int i = 0;
            mCurrentIndex = n9;
            while (i < n2) {
                final int n12 = (int)(defaultDisplayDensity * (1.0f + (i + 1) * n11)) & 0xFFFFFFFE;
                if (densityDpi == n12) {
                    n6 = mCurrentIndex;
                }
                array2[mCurrentIndex] = n12;
                array[mCurrentIndex] = resources.getString(DisplayDensityUtils.SUMMARIES_LARGER[i]);
                ++mCurrentIndex;
                ++i;
            }
            n10 = n6;
        }
        int[] copy;
        String[] mEntries;
        if (n10 >= 0) {
            mCurrentIndex = n10;
            copy = array2;
            mEntries = array;
        }
        else {
            final int n13 = array2.length + 1;
            copy = Arrays.copyOf(array2, n13);
            copy[mCurrentIndex] = densityDpi;
            mEntries = Arrays.copyOf(array, n13);
            mEntries[mCurrentIndex] = resources.getString(DisplayDensityUtils.SUMMARY_CUSTOM, new Object[] { densityDpi });
        }
        this.mDefaultDensity = defaultDisplayDensity;
        this.mCurrentIndex = mCurrentIndex;
        this.mEntries = mEntries;
        this.mValues = copy;
    }
    
    public static void clearForcedDisplayDensity(final int n) {
        AsyncTask.execute((Runnable)new _$$Lambda$DisplayDensityUtils$FjSo_v2dJihYeklLmCubVRPf_nw(n, UserHandle.myUserId()));
    }
    
    private static int getDefaultDisplayDensity(int initialDisplayDensity) {
        try {
            initialDisplayDensity = WindowManagerGlobal.getWindowManagerService().getInitialDisplayDensity(initialDisplayDensity);
            return initialDisplayDensity;
        }
        catch (RemoteException ex) {
            return -1;
        }
    }
    
    public static void setForcedDisplayDensity(final int n, final int n2) {
        AsyncTask.execute((Runnable)new _$$Lambda$DisplayDensityUtils$jbnNZEy3zYf8rJTNV5wQSa3Z5eQ(n, n2, UserHandle.myUserId()));
    }
    
    public int getCurrentIndex() {
        return this.mCurrentIndex;
    }
    
    public int getDefaultDensity() {
        return this.mDefaultDensity;
    }
    
    public String[] getEntries() {
        return this.mEntries;
    }
    
    public int[] getValues() {
        return this.mValues;
    }
}
