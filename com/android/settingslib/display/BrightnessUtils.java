package com.android.settingslib.display;

import android.util.MathUtils;

public class BrightnessUtils
{
    public static final int convertLinearToGamma(final int n, final int n2, final int n3) {
        final float n4 = MathUtils.norm((float)n2, (float)n3, (float)n) * 12.0f;
        float n5;
        if (n4 <= 1.0f) {
            n5 = MathUtils.sqrt(n4) * 0.5f;
        }
        else {
            n5 = 0.17883277f * MathUtils.log(n4 - 0.28466892f) + 0.5599107f;
        }
        return Math.round(MathUtils.lerp(0.0f, 1023.0f, n5));
    }
}
