package com.android.settingslib.display;

import android.util.Log;
import android.os.RemoteException;
import android.view.WindowManagerGlobal;
import android.os.AsyncTask;
import android.os.UserHandle;
import android.content.res.Resources;
import java.util.Arrays;
import android.util.MathUtils;
import android.util.DisplayMetrics;
import android.content.Context;
import com.android.settingslib.R;

public final class _$$Lambda$DisplayDensityUtils$jbnNZEy3zYf8rJTNV5wQSa3Z5eQ implements Runnable
{
    @Override
    public final void run() {
        DisplayDensityUtils.lambda$setForcedDisplayDensity$1(this.f$0, this.f$1, this.f$2);
    }
}
