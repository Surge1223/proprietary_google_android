package com.android.settingslib.display;

import android.util.Log;
import android.os.RemoteException;
import android.view.WindowManagerGlobal;
import android.os.AsyncTask;
import android.os.UserHandle;
import android.content.res.Resources;
import java.util.Arrays;
import android.util.MathUtils;
import android.util.DisplayMetrics;
import android.content.Context;
import com.android.settingslib.R;

public final class _$$Lambda$DisplayDensityUtils$FjSo_v2dJihYeklLmCubVRPf_nw implements Runnable
{
    @Override
    public final void run() {
        DisplayDensityUtils.lambda$clearForcedDisplayDensity$0(this.f$0, this.f$1);
    }
}
