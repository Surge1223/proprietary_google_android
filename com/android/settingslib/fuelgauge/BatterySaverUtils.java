package com.android.settingslib.fuelgauge;

import android.util.Slog;
import android.util.KeyValueListParser;
import android.content.ContentResolver;
import android.os.PowerManager;
import android.provider.Settings;
import android.provider.Settings;
import android.content.Context;
import android.content.Intent;

public class BatterySaverUtils
{
    private static Intent getSystemUiBroadcast(final String s) {
        final Intent intent = new Intent(s);
        intent.setFlags(268435456);
        intent.setPackage("com.android.systemui");
        return intent;
    }
    
    private static boolean maybeShowBatterySaverConfirmation(final Context context) {
        if (Settings.Secure.getInt(context.getContentResolver(), "low_power_warning_acknowledged", 0) != 0) {
            return false;
        }
        context.sendBroadcast(getSystemUiBroadcast("PNW.startSaverConfirmation"));
        return true;
    }
    
    public static void setAutoBatterySaverTriggerLevel(final Context context, final int n) {
        if (n > 0) {
            suppressAutoBatterySaver(context);
        }
        Settings.Global.putInt(context.getContentResolver(), "low_power_trigger_level", n);
    }
    
    private static void setBatterySaverConfirmationAcknowledged(final Context context) {
        Settings.Secure.putInt(context.getContentResolver(), "low_power_warning_acknowledged", 1);
    }
    
    public static boolean setPowerSaveMode(final Context batterySaverConfirmationAcknowledged, final boolean powerSaveMode, final boolean b) {
        synchronized (BatterySaverUtils.class) {
            final ContentResolver contentResolver = batterySaverConfirmationAcknowledged.getContentResolver();
            if (powerSaveMode && b && maybeShowBatterySaverConfirmation(batterySaverConfirmationAcknowledged)) {
                return false;
            }
            if (powerSaveMode && !b) {
                setBatterySaverConfirmationAcknowledged(batterySaverConfirmationAcknowledged);
            }
            if (((PowerManager)batterySaverConfirmationAcknowledged.getSystemService((Class)PowerManager.class)).setPowerSaveMode(powerSaveMode)) {
                if (powerSaveMode) {
                    final int n = Settings.Secure.getInt(contentResolver, "low_power_manual_activation_count", 0) + 1;
                    Settings.Secure.putInt(contentResolver, "low_power_manual_activation_count", n);
                    final Parameters parameters = new Parameters(batterySaverConfirmationAcknowledged);
                    if (n >= parameters.startNth && n <= parameters.endNth && Settings.Global.getInt(contentResolver, "low_power_trigger_level", 0) == 0 && Settings.Secure.getInt(contentResolver, "suppress_auto_battery_saver_suggestion", 0) == 0) {
                        showAutoBatterySaverSuggestion(batterySaverConfirmationAcknowledged);
                    }
                }
                return true;
            }
            return false;
        }
    }
    
    private static void showAutoBatterySaverSuggestion(final Context context) {
        context.sendBroadcast(getSystemUiBroadcast("PNW.autoSaverSuggestion"));
    }
    
    public static void suppressAutoBatterySaver(final Context context) {
        Settings.Secure.putInt(context.getContentResolver(), "suppress_auto_battery_saver_suggestion", 1);
    }
    
    private static class Parameters
    {
        public final int endNth;
        private final Context mContext;
        public final int startNth;
        
        public Parameters(Context string) {
            this.mContext = string;
            string = (Context)Settings.Global.getString(this.mContext.getContentResolver(), "low_power_mode_suggestion_params");
            final KeyValueListParser keyValueListParser = new KeyValueListParser(',');
            try {
                keyValueListParser.setString((String)string);
            }
            catch (IllegalArgumentException ex) {
                final StringBuilder sb = new StringBuilder();
                sb.append("Bad constants: ");
                sb.append((String)string);
                Slog.wtf("BatterySaverUtils", sb.toString());
            }
            this.startNth = keyValueListParser.getInt("start_nth", 4);
            this.endNth = keyValueListParser.getInt("end_nth", 8);
        }
    }
}
