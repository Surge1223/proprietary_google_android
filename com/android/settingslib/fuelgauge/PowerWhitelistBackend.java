package com.android.settingslib.fuelgauge;

import com.android.internal.util.ArrayUtils;
import android.content.ComponentName;
import android.app.admin.DevicePolicyManager;
import android.telecom.DefaultDialerManager;
import android.text.TextUtils;
import com.android.internal.telephony.SmsApplication;
import android.os.RemoteException;
import android.util.Log;
import android.os.IDeviceIdleController$Stub;
import android.os.ServiceManager;
import android.util.ArraySet;
import android.os.IDeviceIdleController;
import android.content.Context;

public class PowerWhitelistBackend
{
    private static PowerWhitelistBackend sInstance;
    private final Context mAppContext;
    private final IDeviceIdleController mDeviceIdleService;
    private final ArraySet<String> mSysWhitelistedApps;
    private final ArraySet<String> mSysWhitelistedAppsExceptIdle;
    private final ArraySet<String> mWhitelistedApps;
    
    public PowerWhitelistBackend(final Context context) {
        this(context, IDeviceIdleController$Stub.asInterface(ServiceManager.getService("deviceidle")));
    }
    
    PowerWhitelistBackend(final Context context, final IDeviceIdleController mDeviceIdleService) {
        this.mWhitelistedApps = (ArraySet<String>)new ArraySet();
        this.mSysWhitelistedApps = (ArraySet<String>)new ArraySet();
        this.mSysWhitelistedAppsExceptIdle = (ArraySet<String>)new ArraySet();
        this.mAppContext = context.getApplicationContext();
        this.mDeviceIdleService = mDeviceIdleService;
        this.refreshList();
    }
    
    public static PowerWhitelistBackend getInstance(final Context context) {
        if (PowerWhitelistBackend.sInstance == null) {
            PowerWhitelistBackend.sInstance = new PowerWhitelistBackend(context);
        }
        return PowerWhitelistBackend.sInstance;
    }
    
    public void addApp(final String s) {
        try {
            this.mDeviceIdleService.addPowerSaveWhitelistApp(s);
            this.mWhitelistedApps.add((Object)s);
        }
        catch (RemoteException ex) {
            Log.w("PowerWhitelistBackend", "Unable to reach IDeviceIdleController", (Throwable)ex);
        }
    }
    
    public boolean isSysWhitelisted(final String s) {
        return this.mSysWhitelistedApps.contains((Object)s);
    }
    
    public boolean isWhitelisted(final String s) {
        if (this.mWhitelistedApps.contains((Object)s)) {
            return true;
        }
        if (!this.mAppContext.getPackageManager().hasSystemFeature("android.hardware.telephony")) {
            return false;
        }
        final ComponentName defaultSmsApplication = SmsApplication.getDefaultSmsApplication(this.mAppContext, true);
        return (defaultSmsApplication != null && TextUtils.equals((CharSequence)s, (CharSequence)defaultSmsApplication.getPackageName())) || TextUtils.equals((CharSequence)s, (CharSequence)DefaultDialerManager.getDefaultDialerApplication(this.mAppContext)) || ((DevicePolicyManager)this.mAppContext.getSystemService((Class)DevicePolicyManager.class)).packageHasActiveAdmins(s);
    }
    
    public boolean isWhitelisted(final String[] array) {
        if (ArrayUtils.isEmpty((Object[])array)) {
            return false;
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            if (this.isWhitelisted(array[i])) {
                return true;
            }
        }
        return false;
    }
    
    public void refreshList() {
        this.mSysWhitelistedApps.clear();
        this.mSysWhitelistedAppsExceptIdle.clear();
        this.mWhitelistedApps.clear();
        if (this.mDeviceIdleService == null) {
            return;
        }
        try {
            final String[] fullPowerWhitelist = this.mDeviceIdleService.getFullPowerWhitelist();
            final int length = fullPowerWhitelist.length;
            final int n = 0;
            for (int i = 0; i < length; ++i) {
                this.mWhitelistedApps.add((Object)fullPowerWhitelist[i]);
            }
            final String[] systemPowerWhitelist = this.mDeviceIdleService.getSystemPowerWhitelist();
            for (int length2 = systemPowerWhitelist.length, j = 0; j < length2; ++j) {
                this.mSysWhitelistedApps.add((Object)systemPowerWhitelist[j]);
            }
            final String[] systemPowerWhitelistExceptIdle = this.mDeviceIdleService.getSystemPowerWhitelistExceptIdle();
            for (int length3 = systemPowerWhitelistExceptIdle.length, k = n; k < length3; ++k) {
                this.mSysWhitelistedAppsExceptIdle.add((Object)systemPowerWhitelistExceptIdle[k]);
            }
        }
        catch (RemoteException ex) {
            Log.w("PowerWhitelistBackend", "Unable to reach IDeviceIdleController", (Throwable)ex);
        }
    }
    
    public void removeApp(final String s) {
        try {
            this.mDeviceIdleService.removePowerSaveWhitelistApp(s);
            this.mWhitelistedApps.remove((Object)s);
        }
        catch (RemoteException ex) {
            Log.w("PowerWhitelistBackend", "Unable to reach IDeviceIdleController", (Throwable)ex);
        }
    }
}
