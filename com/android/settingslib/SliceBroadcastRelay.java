package com.android.settingslib;

import android.os.UserHandle;
import android.content.ComponentName;
import android.os.Parcelable;
import android.content.ContentProvider;
import android.os.Process;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.net.Uri;
import android.content.Context;

public class SliceBroadcastRelay
{
    public static void registerReceiver(final Context context, final Uri uri, final Class<? extends BroadcastReceiver> clazz, final IntentFilter intentFilter) {
        final Intent intent = new Intent("com.android.settingslib.action.REGISTER_SLICE_RECEIVER");
        intent.setPackage("com.android.systemui");
        intent.putExtra("uri", (Parcelable)ContentProvider.maybeAddUserId(uri, Process.myUserHandle().getIdentifier()));
        intent.putExtra("receiver", (Parcelable)new ComponentName(context.getPackageName(), clazz.getName()));
        intent.putExtra("filter", (Parcelable)intentFilter);
        context.sendBroadcastAsUser(intent, UserHandle.SYSTEM);
    }
    
    public static void unregisterReceivers(final Context context, final Uri uri) {
        final Intent intent = new Intent("com.android.settingslib.action.UNREGISTER_SLICE_RECEIVER");
        intent.setPackage("com.android.systemui");
        intent.putExtra("uri", (Parcelable)ContentProvider.maybeAddUserId(uri, Process.myUserHandle().getIdentifier()));
        context.sendBroadcastAsUser(intent, UserHandle.SYSTEM);
    }
}
