package com.android.settingslib;

import android.os.Bundle;
import android.support.v14.preference.EditTextPreferenceDialogFragment;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.app.Dialog;
import android.util.AttributeSet;
import android.content.Context;
import android.support.v7.preference.EditTextPreference;

public class CustomEditTextPreference extends EditTextPreference
{
    private CustomPreferenceDialogFragment mFragment;
    
    public CustomEditTextPreference(final Context context) {
        super(context);
    }
    
    public CustomEditTextPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public CustomEditTextPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public CustomEditTextPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    private void setFragment(final CustomPreferenceDialogFragment mFragment) {
        this.mFragment = mFragment;
    }
    
    public Dialog getDialog() {
        Dialog dialog;
        if (this.mFragment != null) {
            dialog = this.mFragment.getDialog();
        }
        else {
            dialog = null;
        }
        return dialog;
    }
    
    public EditText getEditText() {
        if (this.mFragment != null) {
            final Dialog dialog = this.mFragment.getDialog();
            if (dialog != null) {
                return (EditText)dialog.findViewById(16908291);
            }
        }
        return null;
    }
    
    public boolean isDialogOpen() {
        return this.getDialog() != null && this.getDialog().isShowing();
    }
    
    protected void onBindDialogView(final View view) {
        final EditText editText = (EditText)view.findViewById(16908291);
        if (editText != null) {
            editText.setInputType(16385);
            editText.requestFocus();
        }
    }
    
    protected void onClick(final DialogInterface dialogInterface, final int n) {
    }
    
    protected void onDialogClosed(final boolean b) {
    }
    
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
    }
    
    public static class CustomPreferenceDialogFragment extends EditTextPreferenceDialogFragment
    {
        private CustomEditTextPreference getCustomizablePreference() {
            return (CustomEditTextPreference)this.getPreference();
        }
        
        public static CustomPreferenceDialogFragment newInstance(final String s) {
            final CustomPreferenceDialogFragment customPreferenceDialogFragment = new CustomPreferenceDialogFragment();
            final Bundle arguments = new Bundle(1);
            arguments.putString("key", s);
            customPreferenceDialogFragment.setArguments(arguments);
            return customPreferenceDialogFragment;
        }
        
        @Override
        protected void onBindDialogView(final View view) {
            super.onBindDialogView(view);
            this.getCustomizablePreference().onBindDialogView(view);
        }
        
        @Override
        public void onClick(final DialogInterface dialogInterface, final int n) {
            super.onClick(dialogInterface, n);
            this.getCustomizablePreference().onClick(dialogInterface, n);
        }
        
        @Override
        public void onDialogClosed(final boolean b) {
            super.onDialogClosed(b);
            this.getCustomizablePreference().onDialogClosed(b);
        }
        
        @Override
        protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder) {
            super.onPrepareDialogBuilder(alertDialog$Builder);
            this.getCustomizablePreference().setFragment(this);
            this.getCustomizablePreference().onPrepareDialogBuilder(alertDialog$Builder, (DialogInterface$OnClickListener)this);
        }
    }
}
