package com.android.settingslib.wifi;

import java.util.Iterator;
import android.net.wifi.hotspot2.PasspointConfiguration;
import android.net.wifi.WifiConfiguration;
import java.util.ArrayList;
import java.util.List;
import android.net.wifi.WifiManager;
import android.content.Context;

public class WifiSavedConfigUtils
{
    public static List<AccessPoint> getAllConfigs(final Context context, final WifiManager wifiManager) {
        final ArrayList<AccessPoint> list = new ArrayList<AccessPoint>();
        for (final WifiConfiguration wifiConfiguration : wifiManager.getConfiguredNetworks()) {
            if (wifiConfiguration.isPasspoint()) {
                continue;
            }
            if (wifiConfiguration.isEphemeral()) {
                continue;
            }
            list.add(new AccessPoint(context, wifiConfiguration));
        }
        try {
            final Iterator<PasspointConfiguration> iterator2 = (Iterator<PasspointConfiguration>)wifiManager.getPasspointConfigurations().iterator();
            while (iterator2.hasNext()) {
                list.add(new AccessPoint(context, iterator2.next()));
            }
        }
        catch (UnsupportedOperationException ex) {}
        return list;
    }
}
