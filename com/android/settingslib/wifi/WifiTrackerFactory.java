package com.android.settingslib.wifi;

import android.support.annotation.Keep;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.content.Context;

public class WifiTrackerFactory
{
    private static WifiTracker sTestingWifiTracker;
    
    public static WifiTracker create(final Context context, final WifiTracker.WifiListener wifiListener, final Lifecycle lifecycle, final boolean b, final boolean b2) {
        if (WifiTrackerFactory.sTestingWifiTracker != null) {
            return WifiTrackerFactory.sTestingWifiTracker;
        }
        return new WifiTracker(context, wifiListener, lifecycle, b, b2);
    }
    
    @Keep
    public static void setTestingWifiTracker(final WifiTracker sTestingWifiTracker) {
        WifiTrackerFactory.sTestingWifiTracker = sTestingWifiTracker;
    }
}
