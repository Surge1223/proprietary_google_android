package com.android.settingslib.wifi;

import com.android.settingslib.utils.ThreadUtils;
import java.util.ArrayList;
import com.android.internal.annotations.VisibleForTesting;
import android.net.NetworkInfo.State;
import android.text.style.TtsSpan$TelephoneBuilder;
import android.text.SpannableString;
import java.util.Set;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.app.AppGlobals;
import android.os.UserHandle;
import android.net.wifi.WifiManager;
import java.util.function.Consumer;
import android.os.SystemClock;
import android.net.NetworkKey;
import android.net.wifi.WifiNetworkScoreCache;
import android.content.res.Resources;
import android.net.wifi.IWifiManager;
import android.net.NetworkScorerAppData;
import android.os.RemoteException;
import android.net.wifi.IWifiManager$Stub;
import android.os.ServiceManager;
import android.net.ConnectivityManager;
import android.net.NetworkScoreManager;
import android.net.ScoredNetwork;
import com.android.settingslib.R;
import android.net.NetworkInfo$DetailedState;
import java.util.BitSet;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.Iterator;
import android.os.Parcelable;
import android.os.Bundle;
import android.net.wifi.hotspot2.PasspointConfiguration;
import java.util.HashMap;
import java.util.Map;
import android.net.wifi.ScanResult;
import android.util.ArraySet;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import java.util.concurrent.atomic.AtomicInteger;

public final class _$$Lambda$AccessPoint$MkkIS1nUbezHicDMmYnviyiBJyo implements Runnable
{
    @Override
    public final void run() {
        AccessPoint.lambda$setScanResults$1(this.f$0);
    }
}
