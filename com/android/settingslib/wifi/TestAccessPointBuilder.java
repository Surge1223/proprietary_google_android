package com.android.settingslib.wifi;

import android.os.Parcelable;
import android.net.wifi.WifiConfiguration;
import android.os.Bundle;
import android.net.wifi.WifiInfo;
import android.net.wifi.ScanResult;
import java.util.ArrayList;
import android.net.NetworkInfo;
import android.content.Context;
import android.support.annotation.Keep;

@Keep
public class TestAccessPointBuilder
{
    private String mBssid;
    private String mCarrierName;
    Context mContext;
    private String mFqdn;
    private boolean mIsCarrierAp;
    private int mNetworkId;
    private NetworkInfo mNetworkInfo;
    private String mProviderFriendlyName;
    private int mRssi;
    private ArrayList<ScanResult> mScanResults;
    private ArrayList<TimestampedScoredNetwork> mScoredNetworkCache;
    private int mSecurity;
    private int mSpeed;
    private WifiInfo mWifiInfo;
    private String ssid;
    
    public TestAccessPointBuilder(final Context mContext) {
        this.mBssid = null;
        this.mSpeed = 0;
        this.mRssi = Integer.MIN_VALUE;
        this.mNetworkId = -1;
        this.ssid = "TestSsid";
        this.mNetworkInfo = null;
        this.mFqdn = null;
        this.mProviderFriendlyName = null;
        this.mSecurity = 0;
        this.mIsCarrierAp = false;
        this.mCarrierName = null;
        this.mContext = mContext;
    }
    
    @Keep
    public AccessPoint build() {
        final Bundle bundle = new Bundle();
        final WifiConfiguration wifiConfiguration = new WifiConfiguration();
        wifiConfiguration.networkId = this.mNetworkId;
        wifiConfiguration.BSSID = this.mBssid;
        bundle.putString("key_ssid", this.ssid);
        bundle.putParcelable("key_config", (Parcelable)wifiConfiguration);
        bundle.putParcelable("key_networkinfo", (Parcelable)this.mNetworkInfo);
        bundle.putParcelable("key_wifiinfo", (Parcelable)this.mWifiInfo);
        if (this.mFqdn != null) {
            bundle.putString("key_fqdn", this.mFqdn);
        }
        if (this.mProviderFriendlyName != null) {
            bundle.putString("key_provider_friendly_name", this.mProviderFriendlyName);
        }
        if (this.mScanResults != null) {
            bundle.putParcelableArray("key_scanresults", (Parcelable[])this.mScanResults.toArray(new Parcelable[this.mScanResults.size()]));
        }
        if (this.mScoredNetworkCache != null) {
            bundle.putParcelableArrayList("key_scorednetworkcache", (ArrayList)this.mScoredNetworkCache);
        }
        bundle.putInt("key_security", this.mSecurity);
        bundle.putInt("key_speed", this.mSpeed);
        bundle.putBoolean("key_is_carrier_ap", this.mIsCarrierAp);
        if (this.mCarrierName != null) {
            bundle.putString("key_carrier_name", this.mCarrierName);
        }
        final AccessPoint accessPoint = new AccessPoint(this.mContext, bundle);
        accessPoint.setRssi(this.mRssi);
        return accessPoint;
    }
    
    @Keep
    public TestAccessPointBuilder setActive(final boolean b) {
        if (b) {
            this.mNetworkInfo = new NetworkInfo(8, 8, "TestNetwork", "TestNetwork");
        }
        else {
            this.mNetworkInfo = null;
        }
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setFqdn(final String mFqdn) {
        this.mFqdn = mFqdn;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setLevel(final int n) {
        if (n == 0) {
            this.mRssi = -100;
        }
        else if (n >= 5) {
            this.mRssi = -55;
        }
        else {
            this.mRssi = (int)(n * 45.0f / 4.0f - 100.0f);
        }
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setNetworkId(final int mNetworkId) {
        this.mNetworkId = mNetworkId;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setNetworkInfo(final NetworkInfo mNetworkInfo) {
        this.mNetworkInfo = mNetworkInfo;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setProviderFriendlyName(final String mProviderFriendlyName) {
        this.mProviderFriendlyName = mProviderFriendlyName;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setReachable(final boolean b) {
        if (b) {
            if (this.mRssi == Integer.MIN_VALUE) {
                this.mRssi = -100;
            }
        }
        else {
            this.mRssi = Integer.MIN_VALUE;
        }
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setRssi(final int mRssi) {
        this.mRssi = mRssi;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setSaved(final boolean b) {
        if (b) {
            this.mNetworkId = 1;
        }
        else {
            this.mNetworkId = -1;
        }
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setSecurity(final int mSecurity) {
        this.mSecurity = mSecurity;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setSsid(final String ssid) {
        this.ssid = ssid;
        return this;
    }
    
    @Keep
    public TestAccessPointBuilder setWifiInfo(final WifiInfo mWifiInfo) {
        this.mWifiInfo = mWifiInfo;
        return this;
    }
}
