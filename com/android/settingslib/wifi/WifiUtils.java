package com.android.settingslib.wifi;

import java.util.Iterator;
import android.os.SystemClock;
import java.util.Map;
import android.net.wifi.ScanResult;
import com.android.settingslib.R;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiConfiguration$NetworkSelectionStatus;
import android.net.wifi.WifiConfiguration;

public class WifiUtils
{
    public static String buildLoggingSummary(final AccessPoint accessPoint, final WifiConfiguration wifiConfiguration) {
        final StringBuilder sb = new StringBuilder();
        final WifiInfo info = accessPoint.getInfo();
        if (accessPoint.isActive() && info != null) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(" f=");
            sb2.append(Integer.toString(info.getFrequency()));
            sb.append(sb2.toString());
        }
        final StringBuilder sb3 = new StringBuilder();
        sb3.append(" ");
        sb3.append(getVisibilityStatus(accessPoint));
        sb.append(sb3.toString());
        if (wifiConfiguration != null && !wifiConfiguration.getNetworkSelectionStatus().isNetworkEnabled()) {
            final StringBuilder sb4 = new StringBuilder();
            sb4.append(" (");
            sb4.append(wifiConfiguration.getNetworkSelectionStatus().getNetworkStatusString());
            sb.append(sb4.toString());
            if (wifiConfiguration.getNetworkSelectionStatus().getDisableTime() > 0L) {
                final long n = (System.currentTimeMillis() - wifiConfiguration.getNetworkSelectionStatus().getDisableTime()) / 1000L;
                final long n2 = n / 60L % 60L;
                final long n3 = n2 / 60L % 60L;
                sb.append(", ");
                if (n3 > 0L) {
                    final StringBuilder sb5 = new StringBuilder();
                    sb5.append(Long.toString(n3));
                    sb5.append("h ");
                    sb.append(sb5.toString());
                }
                final StringBuilder sb6 = new StringBuilder();
                sb6.append(Long.toString(n2));
                sb6.append("m ");
                sb.append(sb6.toString());
                final StringBuilder sb7 = new StringBuilder();
                sb7.append(Long.toString(n % 60L));
                sb7.append("s ");
                sb.append(sb7.toString());
            }
            sb.append(")");
        }
        if (wifiConfiguration != null) {
            final WifiConfiguration$NetworkSelectionStatus networkSelectionStatus = wifiConfiguration.getNetworkSelectionStatus();
            for (int i = 0; i < 14; ++i) {
                if (networkSelectionStatus.getDisableReasonCounter(i) != 0) {
                    final StringBuilder sb8 = new StringBuilder();
                    sb8.append(" ");
                    sb8.append(WifiConfiguration$NetworkSelectionStatus.getNetworkDisableReasonString(i));
                    sb8.append("=");
                    sb8.append(networkSelectionStatus.getDisableReasonCounter(i));
                    sb.append(sb8.toString());
                }
            }
        }
        return sb.toString();
    }
    
    public static String getMeteredLabel(final Context context, final WifiConfiguration wifiConfiguration) {
        if (wifiConfiguration.meteredOverride != 1 && (!wifiConfiguration.meteredHint || isMeteredOverridden(wifiConfiguration))) {
            return context.getString(R.string.wifi_unmetered_label);
        }
        return context.getString(R.string.wifi_metered_label);
    }
    
    private static int getSpecificApSpeed(final ScanResult scanResult, final Map<String, TimestampedScoredNetwork> map) {
        final TimestampedScoredNetwork timestampedScoredNetwork = map.get(scanResult.BSSID);
        if (timestampedScoredNetwork == null) {
            return 0;
        }
        return timestampedScoredNetwork.getScore().calculateBadge(scanResult.level);
    }
    
    static String getVisibilityStatus(final AccessPoint accessPoint) {
        final WifiInfo info = accessPoint.getInfo();
        final StringBuilder sb = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();
        final StringBuilder sb3 = new StringBuilder();
        String bssid;
        final String s = bssid = null;
        if (accessPoint.isActive()) {
            bssid = s;
            if (info != null) {
                bssid = info.getBSSID();
                if (bssid != null) {
                    sb.append(" ");
                    sb.append(bssid);
                }
                sb.append(" rssi=");
                sb.append(info.getRssi());
                sb.append(" ");
                sb.append(" score=");
                sb.append(info.score);
                if (accessPoint.getSpeed() != 0) {
                    sb.append(" speed=");
                    sb.append(accessPoint.getSpeedLabel());
                }
                sb.append(String.format(" tx=%.1f,", info.txSuccessRate));
                sb.append(String.format("%.1f,", info.txRetriesRate));
                sb.append(String.format("%.1f ", info.txBadRate));
                sb.append(String.format("rx=%.1f", info.rxSuccessRate));
            }
        }
        int invalid_RSSI = WifiConfiguration.INVALID_RSSI;
        int invalid_RSSI2 = WifiConfiguration.INVALID_RSSI;
        int n = 0;
        int n2 = 0;
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        for (final ScanResult scanResult : accessPoint.getScanResults()) {
            if (scanResult == null) {
                continue;
            }
            int n3;
            int n4;
            int n5;
            int n6;
            if (scanResult.frequency >= 4900 && scanResult.frequency <= 5900) {
                ++n;
                int level;
                if (scanResult.level > (level = invalid_RSSI)) {
                    level = scanResult.level;
                }
                n3 = level;
                n4 = invalid_RSSI2;
                n5 = n;
                n6 = n2;
                if (n <= 4) {
                    sb3.append(verboseScanResultSummary(accessPoint, scanResult, bssid, elapsedRealtime));
                    n3 = level;
                    n4 = invalid_RSSI2;
                    n5 = n;
                    n6 = n2;
                }
            }
            else {
                n3 = invalid_RSSI;
                n4 = invalid_RSSI2;
                n5 = n;
                n6 = n2;
                if (scanResult.frequency >= 2400) {
                    n3 = invalid_RSSI;
                    n4 = invalid_RSSI2;
                    n5 = n;
                    n6 = n2;
                    if (scanResult.frequency <= 2500) {
                        ++n2;
                        int level2;
                        if (scanResult.level > (level2 = invalid_RSSI2)) {
                            level2 = scanResult.level;
                        }
                        n3 = invalid_RSSI;
                        n4 = level2;
                        n5 = n;
                        if ((n6 = n2) <= 4) {
                            sb2.append(verboseScanResultSummary(accessPoint, scanResult, bssid, elapsedRealtime));
                            n6 = n2;
                            n5 = n;
                            n4 = level2;
                            n3 = invalid_RSSI;
                        }
                    }
                }
            }
            invalid_RSSI = n3;
            invalid_RSSI2 = n4;
            n = n5;
            n2 = n6;
        }
        sb.append(" [");
        if (n2 > 0) {
            sb.append("(");
            sb.append(n2);
            sb.append(")");
            if (n2 > 4) {
                sb.append("max=");
                sb.append(invalid_RSSI2);
                sb.append(",");
            }
            sb.append(sb2.toString());
        }
        sb.append(";");
        if (n > 0) {
            sb.append("(");
            sb.append(n);
            sb.append(")");
            if (n > 4) {
                sb.append("max=");
                sb.append(invalid_RSSI);
                sb.append(",");
            }
            sb.append(sb3.toString());
        }
        if (0 > 0) {
            sb.append("!");
            sb.append(0);
        }
        sb.append("]");
        return sb.toString();
    }
    
    public static boolean isMeteredOverridden(final WifiConfiguration wifiConfiguration) {
        return wifiConfiguration.meteredOverride != 0;
    }
    
    static String verboseScanResultSummary(final AccessPoint accessPoint, final ScanResult scanResult, final String s, final long n) {
        final StringBuilder sb = new StringBuilder();
        sb.append(" \n{");
        sb.append(scanResult.BSSID);
        if (scanResult.BSSID.equals(s)) {
            sb.append("*");
        }
        sb.append("=");
        sb.append(scanResult.frequency);
        sb.append(",");
        sb.append(scanResult.level);
        final int specificApSpeed = getSpecificApSpeed(scanResult, accessPoint.getScoredNetworkCache());
        if (specificApSpeed != 0) {
            sb.append(",");
            sb.append(accessPoint.getSpeedLabel(specificApSpeed));
        }
        final int n2 = (int)(n - scanResult.timestamp / 1000L) / 1000;
        sb.append(",");
        sb.append(n2);
        sb.append("s");
        sb.append("}");
        return sb.toString();
    }
}
