package com.android.settingslib.wifi;

import android.net.INetworkScoreCache;
import android.net.NetworkInfo;
import android.content.Intent;
import com.android.settingslib.R;
import android.net.NetworkKey;
import android.net.wifi.WifiConfiguration;
import android.net.NetworkCapabilities;
import android.net.Network;
import android.net.NetworkRequest$Builder;
import android.net.ScoredNetwork;
import java.util.List;
import android.os.Looper;
import android.net.wifi.WifiNetworkScoreCache;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import android.net.NetworkScoreManager;
import android.net.NetworkRequest;
import android.os.Handler;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiNetworkScoreCache$CacheListener;
import android.net.ConnectivityManager$NetworkCallback;

public class WifiStatusTracker extends ConnectivityManager$NetworkCallback
{
    public boolean connected;
    public boolean enabled;
    public int level;
    private final WifiNetworkScoreCache$CacheListener mCacheListener;
    private final Runnable mCallback;
    private final ConnectivityManager mConnectivityManager;
    private final Context mContext;
    private final Handler mHandler;
    private final ConnectivityManager$NetworkCallback mNetworkCallback;
    private final NetworkRequest mNetworkRequest;
    private final NetworkScoreManager mNetworkScoreManager;
    private WifiInfo mWifiInfo;
    private final WifiManager mWifiManager;
    private final WifiNetworkScoreCache mWifiNetworkScoreCache;
    public int rssi;
    public String ssid;
    public int state;
    public String statusLabel;
    
    public WifiStatusTracker(final Context mContext, final WifiManager mWifiManager, final NetworkScoreManager mNetworkScoreManager, final ConnectivityManager mConnectivityManager, final Runnable mCallback) {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mCacheListener = new WifiNetworkScoreCache$CacheListener(this.mHandler) {
            public void networkCacheUpdated(final List<ScoredNetwork> list) {
                WifiStatusTracker.this.updateStatusLabel();
                WifiStatusTracker.this.mCallback.run();
            }
        };
        this.mNetworkRequest = new NetworkRequest$Builder().clearCapabilities().addCapability(15).addTransportType(1).build();
        this.mNetworkCallback = new ConnectivityManager$NetworkCallback() {
            public void onCapabilitiesChanged(final Network network, final NetworkCapabilities networkCapabilities) {
                WifiStatusTracker.this.updateStatusLabel();
                WifiStatusTracker.this.mCallback.run();
            }
        };
        this.mContext = mContext;
        this.mWifiManager = mWifiManager;
        this.mWifiNetworkScoreCache = new WifiNetworkScoreCache(mContext);
        this.mNetworkScoreManager = mNetworkScoreManager;
        this.mConnectivityManager = mConnectivityManager;
        this.mCallback = mCallback;
    }
    
    private String getValidSsid(final WifiInfo wifiInfo) {
        final String ssid = wifiInfo.getSSID();
        if (ssid != null && !"<unknown ssid>".equals(ssid)) {
            return ssid;
        }
        final List configuredNetworks = this.mWifiManager.getConfiguredNetworks();
        for (int size = configuredNetworks.size(), i = 0; i < size; ++i) {
            if (configuredNetworks.get(i).networkId == wifiInfo.getNetworkId()) {
                return configuredNetworks.get(i).SSID;
            }
        }
        return null;
    }
    
    private void maybeRequestNetworkScore() {
        final NetworkKey fromWifiInfo = NetworkKey.createFromWifiInfo(this.mWifiInfo);
        if (this.mWifiNetworkScoreCache.getScoredNetwork(fromWifiInfo) == null) {
            this.mNetworkScoreManager.requestScores(new NetworkKey[] { fromWifiInfo });
        }
    }
    
    private void updateRssi(final int rssi) {
        this.rssi = rssi;
        this.level = WifiManager.calculateSignalLevel(this.rssi, 5);
    }
    
    private void updateStatusLabel() {
        final NetworkCapabilities networkCapabilities = this.mConnectivityManager.getNetworkCapabilities(this.mWifiManager.getCurrentNetwork());
        if (networkCapabilities != null) {
            if (networkCapabilities.hasCapability(17)) {
                this.statusLabel = this.mContext.getString(R.string.wifi_status_sign_in_required);
                return;
            }
            if (!networkCapabilities.hasCapability(16)) {
                this.statusLabel = this.mContext.getString(R.string.wifi_status_no_internet);
                return;
            }
        }
        final ScoredNetwork scoredNetwork = this.mWifiNetworkScoreCache.getScoredNetwork(NetworkKey.createFromWifiInfo(this.mWifiInfo));
        String speedLabel;
        if (scoredNetwork == null) {
            speedLabel = null;
        }
        else {
            speedLabel = AccessPoint.getSpeedLabel(this.mContext, scoredNetwork, this.rssi);
        }
        this.statusLabel = speedLabel;
    }
    
    private void updateWifiState() {
        this.state = this.mWifiManager.getWifiState();
        this.enabled = (this.state == 3);
    }
    
    public void handleBroadcast(final Intent intent) {
        final String action = intent.getAction();
        if (action.equals("android.net.wifi.WIFI_STATE_CHANGED")) {
            this.updateWifiState();
        }
        else if (action.equals("android.net.wifi.STATE_CHANGE")) {
            this.updateWifiState();
            final NetworkInfo networkInfo = (NetworkInfo)intent.getParcelableExtra("networkInfo");
            this.connected = (networkInfo != null && networkInfo.isConnected());
            this.mWifiInfo = null;
            this.ssid = null;
            if (this.connected) {
                this.mWifiInfo = this.mWifiManager.getConnectionInfo();
                if (this.mWifiInfo != null) {
                    this.ssid = this.getValidSsid(this.mWifiInfo);
                    this.updateRssi(this.mWifiInfo.getRssi());
                    this.maybeRequestNetworkScore();
                }
            }
            this.updateStatusLabel();
        }
        else if (action.equals("android.net.wifi.RSSI_CHANGED")) {
            this.updateRssi(intent.getIntExtra("newRssi", -200));
            this.updateStatusLabel();
        }
    }
    
    public void setListening(final boolean b) {
        if (b) {
            this.mNetworkScoreManager.registerNetworkScoreCache(1, (INetworkScoreCache)this.mWifiNetworkScoreCache, 1);
            this.mWifiNetworkScoreCache.registerListener(this.mCacheListener);
            this.mConnectivityManager.registerNetworkCallback(this.mNetworkRequest, this.mNetworkCallback, this.mHandler);
        }
        else {
            this.mNetworkScoreManager.unregisterNetworkScoreCache(1, (INetworkScoreCache)this.mWifiNetworkScoreCache);
            this.mWifiNetworkScoreCache.unregisterListener();
            this.mConnectivityManager.unregisterNetworkCallback(this.mNetworkCallback);
        }
    }
}
