package com.android.settingslib.wifi;

import android.os.UserHandle;
import android.content.pm.PackageManager;
import android.util.SparseArray;
import com.android.settingslib.Utils;
import com.android.settingslib.TronUtils;
import android.net.wifi.WifiConfiguration;
import android.view.View;
import android.support.v7.preference.PreferenceViewHolder;
import android.os.Looper;
import android.content.res.TypedArray;
import android.content.res.Resources$NotFoundException;
import android.text.TextUtils;
import android.widget.ImageView;
import android.util.AttributeSet;
import android.content.Context;
import com.android.settingslib.R;
import android.widget.TextView;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.preference.Preference;

public class AccessPointPreference extends Preference
{
    private static final int[] FRICTION_ATTRS;
    private static final int[] STATE_METERED;
    private static final int[] STATE_SECURED;
    private static final int[] WIFI_CONNECTION_STRENGTH;
    private AccessPoint mAccessPoint;
    private Drawable mBadge;
    private final UserBadgeCache mBadgeCache;
    private final int mBadgePadding;
    private CharSequence mContentDescription;
    private int mDefaultIconResId;
    private boolean mForSavedNetworks;
    private final StateListDrawable mFrictionSld;
    private final IconInjector mIconInjector;
    private int mLevel;
    private final Runnable mNotifyChanged;
    private boolean mShowDivider;
    private TextView mTitleView;
    private int mWifiSpeed;
    
    static {
        STATE_SECURED = new int[] { R.attr.state_encrypted };
        STATE_METERED = new int[] { R.attr.state_metered };
        FRICTION_ATTRS = new int[] { R.attr.wifi_friction };
        WIFI_CONNECTION_STRENGTH = new int[] { R.string.accessibility_no_wifi, R.string.accessibility_wifi_one_bar, R.string.accessibility_wifi_two_bars, R.string.accessibility_wifi_three_bars, R.string.accessibility_wifi_signal_full };
    }
    
    public AccessPointPreference(final Context context, final AttributeSet set) {
        super(context, set);
        this.mForSavedNetworks = false;
        this.mWifiSpeed = 0;
        this.mNotifyChanged = new Runnable() {
            @Override
            public void run() {
                AccessPointPreference.this.notifyChanged();
            }
        };
        this.mFrictionSld = null;
        this.mBadgePadding = 0;
        this.mBadgeCache = null;
        this.mIconInjector = new IconInjector(context);
    }
    
    public AccessPointPreference(final AccessPoint accessPoint, final Context context, final UserBadgeCache userBadgeCache, final int n, final boolean b) {
        this(accessPoint, context, userBadgeCache, n, b, getFrictionStateListDrawable(context), -1, new IconInjector(context));
    }
    
    AccessPointPreference(final AccessPoint mAccessPoint, final Context context, final UserBadgeCache mBadgeCache, final int mDefaultIconResId, final boolean mForSavedNetworks, final StateListDrawable mFrictionSld, final int mLevel, final IconInjector mIconInjector) {
        super(context);
        this.mForSavedNetworks = false;
        this.mWifiSpeed = 0;
        this.mNotifyChanged = new Runnable() {
            @Override
            public void run() {
                AccessPointPreference.this.notifyChanged();
            }
        };
        this.setLayoutResource(R.layout.preference_access_point);
        this.setWidgetLayoutResource(this.getWidgetLayoutResourceId());
        this.mBadgeCache = mBadgeCache;
        this.mAccessPoint = mAccessPoint;
        this.mForSavedNetworks = mForSavedNetworks;
        this.mAccessPoint.setTag(this);
        this.mLevel = mLevel;
        this.mDefaultIconResId = mDefaultIconResId;
        this.mFrictionSld = mFrictionSld;
        this.mIconInjector = mIconInjector;
        this.mBadgePadding = context.getResources().getDimensionPixelSize(R.dimen.wifi_preference_badge_padding);
    }
    
    public AccessPointPreference(final AccessPoint accessPoint, final Context context, final UserBadgeCache userBadgeCache, final boolean b) {
        this(accessPoint, context, userBadgeCache, 0, b);
        this.refresh();
    }
    
    private void bindFrictionImage(final ImageView imageView) {
        if (imageView != null && this.mFrictionSld != null) {
            if (this.mAccessPoint.getSecurity() != 0) {
                this.mFrictionSld.setState(AccessPointPreference.STATE_SECURED);
            }
            else if (this.mAccessPoint.isMetered()) {
                this.mFrictionSld.setState(AccessPointPreference.STATE_METERED);
            }
            imageView.setImageDrawable(this.mFrictionSld.getCurrent());
        }
    }
    
    static CharSequence buildContentDescription(final Context context, final Preference preference, final AccessPoint accessPoint) {
        final CharSequence title = preference.getTitle();
        final CharSequence summary = preference.getSummary();
        CharSequence concat = title;
        if (!TextUtils.isEmpty(summary)) {
            concat = TextUtils.concat(new CharSequence[] { title, ",", summary });
        }
        final int level = accessPoint.getLevel();
        CharSequence concat2 = concat;
        if (level >= 0) {
            concat2 = concat;
            if (level < AccessPointPreference.WIFI_CONNECTION_STRENGTH.length) {
                concat2 = TextUtils.concat(new CharSequence[] { concat, ",", context.getString(AccessPointPreference.WIFI_CONNECTION_STRENGTH[level]) });
            }
        }
        String s;
        if (accessPoint.getSecurity() == 0) {
            s = context.getString(R.string.accessibility_wifi_security_type_none);
        }
        else {
            s = context.getString(R.string.accessibility_wifi_security_type_secured);
        }
        return TextUtils.concat(new CharSequence[] { concat2, ",", s });
    }
    
    private static StateListDrawable getFrictionStateListDrawable(final Context context) {
        StateListDrawable stateListDrawable = null;
        TypedArray obtainStyledAttributes;
        try {
            obtainStyledAttributes = context.getTheme().obtainStyledAttributes(AccessPointPreference.FRICTION_ATTRS);
        }
        catch (Resources$NotFoundException ex) {
            obtainStyledAttributes = null;
        }
        if (obtainStyledAttributes != null) {
            stateListDrawable = (StateListDrawable)obtainStyledAttributes.getDrawable(0);
        }
        return stateListDrawable;
    }
    
    private void postNotifyChanged() {
        if (this.mTitleView != null) {
            this.mTitleView.post(this.mNotifyChanged);
        }
    }
    
    private void safeSetDefaultIcon() {
        if (this.mDefaultIconResId != 0) {
            this.setIcon(this.mDefaultIconResId);
        }
        else {
            this.setIcon(null);
        }
    }
    
    static void setTitle(final AccessPointPreference accessPointPreference, final AccessPoint accessPoint, final boolean b) {
        if (b) {
            accessPointPreference.setTitle(accessPoint.getConfigName());
        }
        else {
            accessPointPreference.setTitle(accessPoint.getSsidStr());
        }
    }
    
    public AccessPoint getAccessPoint() {
        return this.mAccessPoint;
    }
    
    protected int getWidgetLayoutResourceId() {
        return R.layout.access_point_friction_widget;
    }
    
    @Override
    protected void notifyChanged() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            this.postNotifyChanged();
        }
        else {
            super.notifyChanged();
        }
    }
    
    @Override
    public void onBindViewHolder(final PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        if (this.mAccessPoint == null) {
            return;
        }
        final Drawable icon = this.getIcon();
        if (icon != null) {
            icon.setLevel(this.mLevel);
        }
        this.mTitleView = (TextView)preferenceViewHolder.findViewById(16908310);
        if (this.mTitleView != null) {
            this.mTitleView.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable)null, (Drawable)null, this.mBadge, (Drawable)null);
            this.mTitleView.setCompoundDrawablePadding(this.mBadgePadding);
        }
        preferenceViewHolder.itemView.setContentDescription(this.mContentDescription);
        this.bindFrictionImage((ImageView)preferenceViewHolder.findViewById(R.id.friction_icon));
        final View viewById = preferenceViewHolder.findViewById(R.id.two_target_divider);
        int visibility;
        if (this.shouldShowDivider()) {
            visibility = 0;
        }
        else {
            visibility = 4;
        }
        viewById.setVisibility(visibility);
    }
    
    public void onLevelChanged() {
        this.postNotifyChanged();
    }
    
    public void refresh() {
        setTitle(this, this.mAccessPoint, this.mForSavedNetworks);
        final Context context = this.getContext();
        final int level = this.mAccessPoint.getLevel();
        final int speed = this.mAccessPoint.getSpeed();
        if (level != this.mLevel || speed != this.mWifiSpeed) {
            this.mLevel = level;
            this.mWifiSpeed = speed;
            this.updateIcon(this.mLevel, context);
            this.notifyChanged();
        }
        this.updateBadge(context);
        String summary;
        if (this.mForSavedNetworks) {
            summary = this.mAccessPoint.getSavedNetworkSummary();
        }
        else {
            summary = this.mAccessPoint.getSettingsSummary();
        }
        this.setSummary(summary);
        this.mContentDescription = buildContentDescription(this.getContext(), this, this.mAccessPoint);
    }
    
    public void setShowDivider(final boolean mShowDivider) {
        this.mShowDivider = mShowDivider;
        this.notifyChanged();
    }
    
    public boolean shouldShowDivider() {
        return this.mShowDivider;
    }
    
    protected void updateBadge(final Context context) {
        final WifiConfiguration config = this.mAccessPoint.getConfig();
        if (config != null) {
            this.mBadge = this.mBadgeCache.getUserBadge(config.creatorUid);
        }
    }
    
    protected void updateIcon(final int n, final Context context) {
        if (n == -1) {
            this.safeSetDefaultIcon();
            return;
        }
        TronUtils.logWifiSettingsSpeed(context, this.mWifiSpeed);
        final Drawable icon = this.mIconInjector.getIcon(n);
        if (!this.mForSavedNetworks && icon != null) {
            icon.setTint(Utils.getColorAttr(context, 16843817));
            this.setIcon(icon);
        }
        else {
            this.safeSetDefaultIcon();
        }
    }
    
    static class IconInjector
    {
        private final Context mContext;
        
        public IconInjector(final Context mContext) {
            this.mContext = mContext;
        }
        
        public Drawable getIcon(final int n) {
            return this.mContext.getDrawable(Utils.getWifiIconResource(n));
        }
    }
    
    public static class UserBadgeCache
    {
        private final SparseArray<Drawable> mBadges;
        private final PackageManager mPm;
        
        public UserBadgeCache(final PackageManager mPm) {
            this.mBadges = (SparseArray<Drawable>)new SparseArray();
            this.mPm = mPm;
        }
        
        private Drawable getUserBadge(final int n) {
            final int indexOfKey = this.mBadges.indexOfKey(n);
            if (indexOfKey < 0) {
                final Drawable userBadgeForDensity = this.mPm.getUserBadgeForDensity(new UserHandle(n), 0);
                this.mBadges.put(n, (Object)userBadgeForDensity);
                return userBadgeForDensity;
            }
            return (Drawable)this.mBadges.valueAt(indexOfKey);
        }
    }
}
