package com.android.settingslib.wifi;

import android.net.NetworkCapabilities;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import com.android.settingslib.utils.ThreadUtils;
import java.util.ArrayList;
import com.android.internal.annotations.VisibleForTesting;
import android.net.NetworkInfo.State;
import android.text.style.TtsSpan$TelephoneBuilder;
import android.text.SpannableString;
import java.util.Set;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.app.AppGlobals;
import android.os.UserHandle;
import android.net.wifi.WifiManager;
import java.util.function.Consumer;
import android.os.SystemClock;
import android.net.NetworkKey;
import android.net.wifi.WifiNetworkScoreCache;
import android.content.res.Resources;
import android.net.wifi.IWifiManager;
import android.net.NetworkScorerAppData;
import android.os.RemoteException;
import android.net.wifi.IWifiManager$Stub;
import android.os.ServiceManager;
import android.net.ConnectivityManager;
import android.net.NetworkScoreManager;
import android.net.ScoredNetwork;
import com.android.settingslib.R;
import android.net.NetworkInfo$DetailedState;
import java.util.BitSet;
import android.text.TextUtils;
import android.util.Log;
import java.util.Collection;
import java.util.Iterator;
import android.os.Parcelable;
import android.os.Bundle;
import android.net.wifi.hotspot2.PasspointConfiguration;
import java.util.HashMap;
import java.util.Map;
import android.net.wifi.ScanResult;
import android.util.ArraySet;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.content.Context;
import android.net.wifi.WifiConfiguration;
import java.util.concurrent.atomic.AtomicInteger;

public class AccessPoint implements Comparable<AccessPoint>
{
    static final AtomicInteger sLastId;
    private String bssid;
    AccessPointListener mAccessPointListener;
    private int mCarrierApEapType;
    private String mCarrierName;
    private WifiConfiguration mConfig;
    private final Context mContext;
    private String mFqdn;
    int mId;
    private WifiInfo mInfo;
    private boolean mIsCarrierAp;
    private boolean mIsScoredNetworkMetered;
    private String mKey;
    private NetworkInfo mNetworkInfo;
    private String mProviderFriendlyName;
    private int mRssi;
    private final ArraySet<ScanResult> mScanResults;
    private final Map<String, TimestampedScoredNetwork> mScoredNetworkCache;
    private int mSpeed;
    private Object mTag;
    private int networkId;
    private int pskType;
    private int security;
    private String ssid;
    
    static {
        sLastId = new AtomicInteger(0);
    }
    
    public AccessPoint(final Context mContext, final WifiConfiguration wifiConfiguration) {
        this.mScanResults = (ArraySet<ScanResult>)new ArraySet();
        this.mScoredNetworkCache = new HashMap<String, TimestampedScoredNetwork>();
        this.networkId = -1;
        this.pskType = 0;
        this.mRssi = Integer.MIN_VALUE;
        this.mSpeed = 0;
        this.mIsScoredNetworkMetered = false;
        this.mIsCarrierAp = false;
        this.mCarrierApEapType = -1;
        this.mCarrierName = null;
        this.mContext = mContext;
        this.loadConfig(wifiConfiguration);
        this.mId = AccessPoint.sLastId.incrementAndGet();
    }
    
    public AccessPoint(final Context mContext, final PasspointConfiguration passpointConfiguration) {
        this.mScanResults = (ArraySet<ScanResult>)new ArraySet();
        this.mScoredNetworkCache = new HashMap<String, TimestampedScoredNetwork>();
        this.networkId = -1;
        this.pskType = 0;
        this.mRssi = Integer.MIN_VALUE;
        this.mSpeed = 0;
        this.mIsScoredNetworkMetered = false;
        this.mIsCarrierAp = false;
        this.mCarrierApEapType = -1;
        this.mCarrierName = null;
        this.mContext = mContext;
        this.mFqdn = passpointConfiguration.getHomeSp().getFqdn();
        this.mProviderFriendlyName = passpointConfiguration.getHomeSp().getFriendlyName();
        this.mId = AccessPoint.sLastId.incrementAndGet();
    }
    
    public AccessPoint(final Context mContext, final Bundle bundle) {
        this.mScanResults = (ArraySet<ScanResult>)new ArraySet();
        this.mScoredNetworkCache = new HashMap<String, TimestampedScoredNetwork>();
        this.networkId = -1;
        int i = 0;
        this.pskType = 0;
        this.mRssi = Integer.MIN_VALUE;
        this.mSpeed = 0;
        this.mIsScoredNetworkMetered = false;
        this.mIsCarrierAp = false;
        this.mCarrierApEapType = -1;
        this.mCarrierName = null;
        this.mContext = mContext;
        if (bundle.containsKey("key_config")) {
            this.mConfig = (WifiConfiguration)bundle.getParcelable("key_config");
        }
        if (this.mConfig != null) {
            this.loadConfig(this.mConfig);
        }
        if (bundle.containsKey("key_ssid")) {
            this.ssid = bundle.getString("key_ssid");
        }
        if (bundle.containsKey("key_security")) {
            this.security = bundle.getInt("key_security");
        }
        if (bundle.containsKey("key_speed")) {
            this.mSpeed = bundle.getInt("key_speed");
        }
        if (bundle.containsKey("key_psktype")) {
            this.pskType = bundle.getInt("key_psktype");
        }
        this.mInfo = (WifiInfo)bundle.getParcelable("key_wifiinfo");
        if (bundle.containsKey("key_networkinfo")) {
            this.mNetworkInfo = (NetworkInfo)bundle.getParcelable("key_networkinfo");
        }
        if (bundle.containsKey("key_scanresults")) {
            final Parcelable[] parcelableArray = bundle.getParcelableArray("key_scanresults");
            this.mScanResults.clear();
            while (i < parcelableArray.length) {
                this.mScanResults.add((Object)parcelableArray[i]);
                ++i;
            }
        }
        if (bundle.containsKey("key_scorednetworkcache")) {
            for (final TimestampedScoredNetwork timestampedScoredNetwork : bundle.getParcelableArrayList("key_scorednetworkcache")) {
                this.mScoredNetworkCache.put(timestampedScoredNetwork.getScore().networkKey.wifiKey.bssid, timestampedScoredNetwork);
            }
        }
        if (bundle.containsKey("key_fqdn")) {
            this.mFqdn = bundle.getString("key_fqdn");
        }
        if (bundle.containsKey("key_provider_friendly_name")) {
            this.mProviderFriendlyName = bundle.getString("key_provider_friendly_name");
        }
        if (bundle.containsKey("key_is_carrier_ap")) {
            this.mIsCarrierAp = bundle.getBoolean("key_is_carrier_ap");
        }
        if (bundle.containsKey("key_carrier_ap_eap_type")) {
            this.mCarrierApEapType = bundle.getInt("key_carrier_ap_eap_type");
        }
        if (bundle.containsKey("key_carrier_name")) {
            this.mCarrierName = bundle.getString("key_carrier_name");
        }
        this.update(this.mConfig, this.mInfo, this.mNetworkInfo);
        this.updateKey();
        this.updateRssi();
        this.mId = AccessPoint.sLastId.incrementAndGet();
    }
    
    AccessPoint(final Context mContext, final Collection<ScanResult> collection) {
        this.mScanResults = (ArraySet<ScanResult>)new ArraySet();
        this.mScoredNetworkCache = new HashMap<String, TimestampedScoredNetwork>();
        this.networkId = -1;
        this.pskType = 0;
        this.mRssi = Integer.MIN_VALUE;
        this.mSpeed = 0;
        this.mIsScoredNetworkMetered = false;
        this.mIsCarrierAp = false;
        this.mCarrierApEapType = -1;
        this.mCarrierName = null;
        this.mContext = mContext;
        if (!collection.isEmpty()) {
            this.mScanResults.addAll((Collection)collection);
            final ScanResult scanResult = collection.iterator().next();
            this.ssid = scanResult.SSID;
            this.bssid = scanResult.BSSID;
            this.security = getSecurity(scanResult);
            if (this.security == 2) {
                this.pskType = getPskType(scanResult);
            }
            this.updateKey();
            this.updateRssi();
            this.mIsCarrierAp = scanResult.isCarrierAp;
            this.mCarrierApEapType = scanResult.carrierApEapType;
            this.mCarrierName = scanResult.carrierName;
            this.mId = AccessPoint.sLastId.incrementAndGet();
            return;
        }
        throw new IllegalArgumentException("Cannot construct with an empty ScanResult list");
    }
    
    public static String convertToQuotedString(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("\"");
        sb.append(s);
        sb.append("\"");
        return sb.toString();
    }
    
    private int generateAverageSpeedForSsid() {
        if (this.mScoredNetworkCache.isEmpty()) {
            return 0;
        }
        if (Log.isLoggable("SettingsLib.AccessPoint", 3)) {
            Log.d("SettingsLib.AccessPoint", String.format("Generating fallbackspeed for %s using cache: %s", this.getSsidStr(), this.mScoredNetworkCache));
        }
        int n = 0;
        int n2 = 0;
        final Iterator<TimestampedScoredNetwork> iterator = this.mScoredNetworkCache.values().iterator();
        while (iterator.hasNext()) {
            final int calculateBadge = iterator.next().getScore().calculateBadge(this.mRssi);
            int n3 = n;
            int n4 = n2;
            if (calculateBadge != 0) {
                n3 = n + 1;
                n4 = n2 + calculateBadge;
            }
            n = n3;
            n2 = n4;
        }
        int n5;
        if (n == 0) {
            n5 = 0;
        }
        else {
            n5 = n2 / n;
        }
        if (isVerboseLoggingEnabled()) {
            Log.i("SettingsLib.AccessPoint", String.format("%s generated fallback speed is: %d", this.getSsidStr(), n5));
        }
        return roundToClosestSpeedEnum(n5);
    }
    
    public static String getKey(final ScanResult scanResult) {
        final StringBuilder sb = new StringBuilder();
        if (TextUtils.isEmpty((CharSequence)scanResult.SSID)) {
            sb.append(scanResult.BSSID);
        }
        else {
            sb.append(scanResult.SSID);
        }
        sb.append(',');
        sb.append(getSecurity(scanResult));
        return sb.toString();
    }
    
    public static String getKey(final WifiConfiguration wifiConfiguration) {
        final StringBuilder sb = new StringBuilder();
        if (TextUtils.isEmpty((CharSequence)wifiConfiguration.SSID)) {
            sb.append(wifiConfiguration.BSSID);
        }
        else {
            sb.append(removeDoubleQuotes(wifiConfiguration.SSID));
        }
        sb.append(',');
        sb.append(getSecurity(wifiConfiguration));
        return sb.toString();
    }
    
    private static int getPskType(final ScanResult scanResult) {
        final boolean contains = scanResult.capabilities.contains("WPA-PSK");
        final boolean contains2 = scanResult.capabilities.contains("WPA2-PSK");
        if (contains2 && contains) {
            return 3;
        }
        if (contains2) {
            return 2;
        }
        if (contains) {
            return 1;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Received abnormal flag string: ");
        sb.append(scanResult.capabilities);
        Log.w("SettingsLib.AccessPoint", sb.toString());
        return 0;
    }
    
    private static int getSecurity(final ScanResult scanResult) {
        if (scanResult.capabilities.contains("WEP")) {
            return 1;
        }
        if (scanResult.capabilities.contains("PSK")) {
            return 2;
        }
        if (scanResult.capabilities.contains("EAP")) {
            return 3;
        }
        return 0;
    }
    
    static int getSecurity(final WifiConfiguration wifiConfiguration) {
        final BitSet allowedKeyManagement = wifiConfiguration.allowedKeyManagement;
        int n = 1;
        if (allowedKeyManagement.get(1)) {
            return 2;
        }
        if (!wifiConfiguration.allowedKeyManagement.get(2) && !wifiConfiguration.allowedKeyManagement.get(3)) {
            if (wifiConfiguration.wepKeys[0] == null) {
                n = 0;
            }
            return n;
        }
        return 3;
    }
    
    private String getSettingsSummary(final WifiConfiguration wifiConfiguration) {
        final StringBuilder sb = new StringBuilder();
        if (this.isActive() && wifiConfiguration != null && wifiConfiguration.isPasspoint()) {
            sb.append(getSummary(this.mContext, this.getDetailedState(), false, wifiConfiguration.providerFriendlyName));
        }
        else if (this.isActive() && wifiConfiguration != null && this.getDetailedState() == NetworkInfo$DetailedState.CONNECTED && this.mIsCarrierAp) {
            sb.append(String.format(this.mContext.getString(R.string.connected_via_carrier), this.mCarrierName));
        }
        else if (this.isActive()) {
            sb.append(getSummary(this.mContext, this.getDetailedState(), this.mInfo != null && this.mInfo.isEphemeral()));
        }
        else if (wifiConfiguration != null && wifiConfiguration.isPasspoint() && wifiConfiguration.getNetworkSelectionStatus().isNetworkEnabled()) {
            sb.append(String.format(this.mContext.getString(R.string.available_via_passpoint), wifiConfiguration.providerFriendlyName));
        }
        else if (wifiConfiguration != null && wifiConfiguration.hasNoInternetAccess()) {
            int n;
            if (wifiConfiguration.getNetworkSelectionStatus().isNetworkPermanentlyDisabled()) {
                n = R.string.wifi_no_internet_no_reconnect;
            }
            else {
                n = R.string.wifi_no_internet;
            }
            sb.append(this.mContext.getString(n));
        }
        else if (wifiConfiguration != null && !wifiConfiguration.getNetworkSelectionStatus().isNetworkEnabled()) {
            final int networkSelectionDisableReason = wifiConfiguration.getNetworkSelectionStatus().getNetworkSelectionDisableReason();
            if (networkSelectionDisableReason != 13) {
                switch (networkSelectionDisableReason) {
                    case 4:
                    case 5: {
                        sb.append(this.mContext.getString(R.string.wifi_disabled_network_failure));
                        break;
                    }
                    case 3: {
                        sb.append(this.mContext.getString(R.string.wifi_disabled_password_failure));
                        break;
                    }
                    case 2: {
                        sb.append(this.mContext.getString(R.string.wifi_disabled_generic));
                        break;
                    }
                }
            }
            else {
                sb.append(this.mContext.getString(R.string.wifi_check_password_try_again));
            }
        }
        else if (wifiConfiguration != null && wifiConfiguration.getNetworkSelectionStatus().isNotRecommended()) {
            sb.append(this.mContext.getString(R.string.wifi_disabled_by_recommendation_provider));
        }
        else if (this.mIsCarrierAp) {
            sb.append(String.format(this.mContext.getString(R.string.available_via_carrier), this.mCarrierName));
        }
        else if (!this.isReachable()) {
            sb.append(this.mContext.getString(R.string.wifi_not_in_range));
        }
        else if (wifiConfiguration != null) {
            if (wifiConfiguration.recentFailure.getAssociationStatus() != 17) {
                sb.append(this.mContext.getString(R.string.wifi_remembered));
            }
            else {
                sb.append(this.mContext.getString(R.string.wifi_ap_unable_to_handle_new_sta));
            }
        }
        if (isVerboseLoggingEnabled()) {
            sb.append(WifiUtils.buildLoggingSummary(this, wifiConfiguration));
        }
        if (wifiConfiguration != null && (WifiUtils.isMeteredOverridden(wifiConfiguration) || wifiConfiguration.meteredHint)) {
            return this.mContext.getResources().getString(R.string.preference_summary_default_combination, new Object[] { WifiUtils.getMeteredLabel(this.mContext, wifiConfiguration), sb.toString() });
        }
        if (this.getSpeedLabel() != null && sb.length() != 0) {
            return this.mContext.getResources().getString(R.string.preference_summary_default_combination, new Object[] { this.getSpeedLabel(), sb.toString() });
        }
        if (this.getSpeedLabel() != null) {
            return this.getSpeedLabel();
        }
        return sb.toString();
    }
    
    private static String getSpeedLabel(final Context context, final int n) {
        if (n == 5) {
            return context.getString(R.string.speed_label_slow);
        }
        if (n == 10) {
            return context.getString(R.string.speed_label_okay);
        }
        if (n == 20) {
            return context.getString(R.string.speed_label_fast);
        }
        if (n != 30) {
            return null;
        }
        return context.getString(R.string.speed_label_very_fast);
    }
    
    public static String getSpeedLabel(final Context context, final ScoredNetwork scoredNetwork, final int n) {
        return getSpeedLabel(context, roundToClosestSpeedEnum(scoredNetwork.calculateBadge(n)));
    }
    
    public static String getSummary(final Context context, final NetworkInfo$DetailedState networkInfo$DetailedState, final boolean b) {
        return getSummary(context, null, networkInfo$DetailedState, b, null);
    }
    
    public static String getSummary(final Context context, final NetworkInfo$DetailedState networkInfo$DetailedState, final boolean b, final String s) {
        return getSummary(context, null, networkInfo$DetailedState, b, s);
    }
    
    public static String getSummary(final Context context, final String s, final NetworkInfo$DetailedState networkInfo$DetailedState, final boolean b, String networkCapabilities) {
        if (networkInfo$DetailedState == NetworkInfo$DetailedState.CONNECTED && s == null) {
            if (!TextUtils.isEmpty((CharSequence)networkCapabilities)) {
                return String.format(context.getString(R.string.connected_via_passpoint), networkCapabilities);
            }
            if (b) {
                final NetworkScorerAppData activeScorer = ((NetworkScoreManager)context.getSystemService((Class)NetworkScoreManager.class)).getActiveScorer();
                if (activeScorer != null && activeScorer.getRecommendationServiceLabel() != null) {
                    return String.format(context.getString(R.string.connected_via_network_scorer), activeScorer.getRecommendationServiceLabel());
                }
                return context.getString(R.string.connected_via_network_scorer_default);
            }
        }
        final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService("connectivity");
        if (networkInfo$DetailedState == NetworkInfo$DetailedState.CONNECTED) {
            final IWifiManager interface1 = IWifiManager$Stub.asInterface(ServiceManager.getService("wifi"));
            networkCapabilities = null;
            try {
                networkCapabilities = (String)connectivityManager.getNetworkCapabilities(interface1.getCurrentNetwork());
            }
            catch (RemoteException ex) {}
            if (networkCapabilities != null) {
                if (((NetworkCapabilities)networkCapabilities).hasCapability(17)) {
                    return context.getString(context.getResources().getIdentifier("network_available_sign_in", "string", "android"));
                }
                if (!((NetworkCapabilities)networkCapabilities).hasCapability(16)) {
                    return context.getString(R.string.wifi_connected_no_internet);
                }
            }
        }
        if (networkInfo$DetailedState == null) {
            Log.w("SettingsLib.AccessPoint", "state is null, returning empty summary");
            return "";
        }
        final Resources resources = context.getResources();
        int n;
        if (s == null) {
            n = R.array.wifi_status;
        }
        else {
            n = R.array.wifi_status_with_ssid;
        }
        final String[] stringArray = resources.getStringArray(n);
        final int ordinal = networkInfo$DetailedState.ordinal();
        if (ordinal < stringArray.length && stringArray[ordinal].length() != 0) {
            return String.format(stringArray[ordinal], s);
        }
        return "";
    }
    
    private boolean isInfoForThisAccessPoint(final WifiConfiguration wifiConfiguration, final WifiInfo wifiInfo) {
        if (!this.isPasspoint() && this.networkId != -1) {
            return this.networkId == wifiInfo.getNetworkId();
        }
        if (wifiConfiguration != null) {
            return this.matches(wifiConfiguration);
        }
        return this.ssid.equals(removeDoubleQuotes(wifiInfo.getSSID()));
    }
    
    private static boolean isVerboseLoggingEnabled() {
        return WifiTracker.sVerboseLogging || Log.isLoggable("SettingsLib.AccessPoint", 2);
    }
    
    static String removeDoubleQuotes(final String s) {
        if (TextUtils.isEmpty((CharSequence)s)) {
            return "";
        }
        final int length = s.length();
        if (length > 1 && s.charAt(0) == '\"' && s.charAt(length - 1) == '\"') {
            return s.substring(1, length - 1);
        }
        return s;
    }
    
    private static int roundToClosestSpeedEnum(final int n) {
        if (n < 5) {
            return 0;
        }
        if (n < 7) {
            return 5;
        }
        if (n < 15) {
            return 10;
        }
        if (n < 25) {
            return 20;
        }
        return 30;
    }
    
    public static String securityToString(final int n, final int n2) {
        if (n == 1) {
            return "WEP";
        }
        if (n == 2) {
            if (n2 == 1) {
                return "WPA";
            }
            if (n2 == 2) {
                return "WPA2";
            }
            if (n2 == 3) {
                return "WPA_WPA2";
            }
            return "PSK";
        }
        else {
            if (n == 3) {
                return "EAP";
            }
            return "NONE";
        }
    }
    
    private void updateKey() {
        final StringBuilder sb = new StringBuilder();
        if (TextUtils.isEmpty((CharSequence)this.getSsidStr())) {
            sb.append(this.getBssid());
        }
        else {
            sb.append(this.getSsidStr());
        }
        sb.append(',');
        sb.append(this.getSecurity());
        this.mKey = sb.toString();
    }
    
    private boolean updateMetered(final WifiNetworkScoreCache wifiNetworkScoreCache) {
        final boolean mIsScoredNetworkMetered = this.mIsScoredNetworkMetered;
        boolean b = false;
        this.mIsScoredNetworkMetered = false;
        if (this.isActive() && this.mInfo != null) {
            final ScoredNetwork scoredNetwork = wifiNetworkScoreCache.getScoredNetwork(NetworkKey.createFromWifiInfo(this.mInfo));
            if (scoredNetwork != null) {
                this.mIsScoredNetworkMetered |= scoredNetwork.meteredHint;
            }
        }
        else {
            final Iterator iterator = this.mScanResults.iterator();
            while (iterator.hasNext()) {
                final ScoredNetwork scoredNetwork2 = wifiNetworkScoreCache.getScoredNetwork((ScanResult)iterator.next());
                if (scoredNetwork2 == null) {
                    continue;
                }
                this.mIsScoredNetworkMetered |= scoredNetwork2.meteredHint;
            }
        }
        if (mIsScoredNetworkMetered == this.mIsScoredNetworkMetered) {
            b = true;
        }
        return b;
    }
    
    private void updateRssi() {
        if (this.isActive()) {
            return;
        }
        int mRssi = Integer.MIN_VALUE;
        for (final ScanResult scanResult : this.mScanResults) {
            int level;
            if (scanResult.level > (level = mRssi)) {
                level = scanResult.level;
            }
            mRssi = level;
        }
        if (mRssi != Integer.MIN_VALUE && this.mRssi != Integer.MIN_VALUE) {
            this.mRssi = (this.mRssi + mRssi) / 2;
        }
        else {
            this.mRssi = mRssi;
        }
    }
    
    private boolean updateScores(final WifiNetworkScoreCache wifiNetworkScoreCache, final long n) {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        for (final ScanResult scanResult : this.mScanResults) {
            final ScoredNetwork scoredNetwork = wifiNetworkScoreCache.getScoredNetwork(scanResult);
            if (scoredNetwork == null) {
                continue;
            }
            final TimestampedScoredNetwork timestampedScoredNetwork = this.mScoredNetworkCache.get(scanResult.BSSID);
            if (timestampedScoredNetwork == null) {
                this.mScoredNetworkCache.put(scanResult.BSSID, new TimestampedScoredNetwork(scoredNetwork, elapsedRealtime));
            }
            else {
                timestampedScoredNetwork.update(scoredNetwork, elapsedRealtime);
            }
        }
        final Iterator<TimestampedScoredNetwork> iterator2 = this.mScoredNetworkCache.values().iterator();
        iterator2.forEachRemaining(new _$$Lambda$AccessPoint$OIXfUc7y1PqI_zmQ3STe_086YzY(elapsedRealtime - n, iterator2));
        return this.updateSpeed();
    }
    
    private boolean updateSpeed() {
        final int mSpeed = this.mSpeed;
        this.mSpeed = this.generateAverageSpeedForSsid();
        final boolean b = mSpeed != this.mSpeed;
        if (isVerboseLoggingEnabled() && b) {
            Log.i("SettingsLib.AccessPoint", String.format("%s: Set speed to %d", this.ssid, this.mSpeed));
        }
        return b;
    }
    
    @Override
    public int compareTo(final AccessPoint accessPoint) {
        if (this.isActive() && !accessPoint.isActive()) {
            return -1;
        }
        if (!this.isActive() && accessPoint.isActive()) {
            return 1;
        }
        if (this.isReachable() && !accessPoint.isReachable()) {
            return -1;
        }
        if (!this.isReachable() && accessPoint.isReachable()) {
            return 1;
        }
        if (this.isSaved() && !accessPoint.isSaved()) {
            return -1;
        }
        if (!this.isSaved() && accessPoint.isSaved()) {
            return 1;
        }
        if (this.getSpeed() != accessPoint.getSpeed()) {
            return accessPoint.getSpeed() - this.getSpeed();
        }
        final int n = WifiManager.calculateSignalLevel(accessPoint.mRssi, 5) - WifiManager.calculateSignalLevel(this.mRssi, 5);
        if (n != 0) {
            return n;
        }
        final int compareToIgnoreCase = this.getSsidStr().compareToIgnoreCase(accessPoint.getSsidStr());
        if (compareToIgnoreCase != 0) {
            return compareToIgnoreCase;
        }
        return this.getSsidStr().compareTo(accessPoint.getSsidStr());
    }
    
    @Override
    public boolean equals(final Object o) {
        final boolean b = o instanceof AccessPoint;
        boolean b2 = false;
        if (!b) {
            return false;
        }
        if (this.compareTo((AccessPoint)o) == 0) {
            b2 = true;
        }
        return b2;
    }
    
    public void generateOpenNetworkConfig() {
        if (this.security != 0) {
            throw new IllegalStateException();
        }
        if (this.mConfig != null) {
            return;
        }
        this.mConfig = new WifiConfiguration();
        this.mConfig.SSID = convertToQuotedString(this.ssid);
        this.mConfig.allowedKeyManagement.set(0);
    }
    
    public String getBssid() {
        return this.bssid;
    }
    
    public int getCarrierApEapType() {
        return this.mCarrierApEapType;
    }
    
    public String getCarrierName() {
        return this.mCarrierName;
    }
    
    public WifiConfiguration getConfig() {
        return this.mConfig;
    }
    
    public String getConfigName() {
        if (this.mConfig != null && this.mConfig.isPasspoint()) {
            return this.mConfig.providerFriendlyName;
        }
        if (this.mFqdn != null) {
            return this.mProviderFriendlyName;
        }
        return this.ssid;
    }
    
    public NetworkInfo$DetailedState getDetailedState() {
        if (this.mNetworkInfo != null) {
            return this.mNetworkInfo.getDetailedState();
        }
        Log.w("SettingsLib.AccessPoint", "NetworkInfo is null, cannot return detailed state");
        return null;
    }
    
    public WifiInfo getInfo() {
        return this.mInfo;
    }
    
    public String getKey() {
        return this.mKey;
    }
    
    public int getLevel() {
        return WifiManager.calculateSignalLevel(this.mRssi, 5);
    }
    
    public NetworkInfo getNetworkInfo() {
        return this.mNetworkInfo;
    }
    
    public String getPasspointFqdn() {
        return this.mFqdn;
    }
    
    public String getSavedNetworkSummary() {
        final WifiConfiguration mConfig = this.mConfig;
        if (mConfig != null) {
            final PackageManager packageManager = this.mContext.getPackageManager();
            final String nameForUid = packageManager.getNameForUid(1000);
            final int userId = UserHandle.getUserId(mConfig.creatorUid);
            ApplicationInfo applicationInfo = null;
            if (mConfig.creatorName != null && mConfig.creatorName.equals(nameForUid)) {
                applicationInfo = this.mContext.getApplicationInfo();
            }
            else {
                try {
                    applicationInfo = AppGlobals.getPackageManager().getApplicationInfo(mConfig.creatorName, 0, userId);
                }
                catch (RemoteException ex) {}
            }
            if (applicationInfo != null && !applicationInfo.packageName.equals(this.mContext.getString(R.string.settings_package)) && !applicationInfo.packageName.equals(this.mContext.getString(R.string.certinstaller_package))) {
                return this.mContext.getString(R.string.saved_network, new Object[] { applicationInfo.loadLabel(packageManager) });
            }
        }
        return "";
    }
    
    public Set<ScanResult> getScanResults() {
        return (Set<ScanResult>)this.mScanResults;
    }
    
    public Map<String, TimestampedScoredNetwork> getScoredNetworkCache() {
        return this.mScoredNetworkCache;
    }
    
    public int getSecurity() {
        return this.security;
    }
    
    public String getSecurityString(final boolean b) {
        final Context mContext = this.mContext;
        if (this.isPasspoint() || this.isPasspointConfig()) {
            String s;
            if (b) {
                s = mContext.getString(R.string.wifi_security_short_eap);
            }
            else {
                s = mContext.getString(R.string.wifi_security_eap);
            }
            return s;
        }
        switch (this.security) {
            default: {
                String string;
                if (b) {
                    string = "";
                }
                else {
                    string = mContext.getString(R.string.wifi_security_none);
                }
                return string;
            }
            case 3: {
                String s2;
                if (b) {
                    s2 = mContext.getString(R.string.wifi_security_short_eap);
                }
                else {
                    s2 = mContext.getString(R.string.wifi_security_eap);
                }
                return s2;
            }
            case 2: {
                switch (this.pskType) {
                    default: {
                        String s3;
                        if (b) {
                            s3 = mContext.getString(R.string.wifi_security_short_psk_generic);
                        }
                        else {
                            s3 = mContext.getString(R.string.wifi_security_psk_generic);
                        }
                        return s3;
                    }
                    case 3: {
                        String s4;
                        if (b) {
                            s4 = mContext.getString(R.string.wifi_security_short_wpa_wpa2);
                        }
                        else {
                            s4 = mContext.getString(R.string.wifi_security_wpa_wpa2);
                        }
                        return s4;
                    }
                    case 2: {
                        String s5;
                        if (b) {
                            s5 = mContext.getString(R.string.wifi_security_short_wpa2);
                        }
                        else {
                            s5 = mContext.getString(R.string.wifi_security_wpa2);
                        }
                        return s5;
                    }
                    case 1: {
                        String s6;
                        if (b) {
                            s6 = mContext.getString(R.string.wifi_security_short_wpa);
                        }
                        else {
                            s6 = mContext.getString(R.string.wifi_security_wpa);
                        }
                        return s6;
                    }
                }
                break;
            }
            case 1: {
                String s7;
                if (b) {
                    s7 = mContext.getString(R.string.wifi_security_short_wep);
                }
                else {
                    s7 = mContext.getString(R.string.wifi_security_wep);
                }
                return s7;
            }
        }
    }
    
    public String getSettingsSummary() {
        return this.getSettingsSummary(this.mConfig);
    }
    
    int getSpeed() {
        return this.mSpeed;
    }
    
    String getSpeedLabel() {
        return this.getSpeedLabel(this.mSpeed);
    }
    
    String getSpeedLabel(final int n) {
        return getSpeedLabel(this.mContext, n);
    }
    
    public CharSequence getSsid() {
        final SpannableString spannableString = new SpannableString((CharSequence)this.ssid);
        spannableString.setSpan((Object)new TtsSpan$TelephoneBuilder(this.ssid).build(), 0, this.ssid.length(), 18);
        return (CharSequence)spannableString;
    }
    
    public String getSsidStr() {
        return this.ssid;
    }
    
    public Object getTag() {
        return this.mTag;
    }
    
    @Override
    public int hashCode() {
        int n = 0;
        if (this.mInfo != null) {
            n = 0 + 13 * this.mInfo.hashCode();
        }
        return n + 19 * this.mRssi + 23 * this.networkId + 29 * this.ssid.hashCode();
    }
    
    public boolean isActive() {
        return this.mNetworkInfo != null && (this.networkId != -1 || this.mNetworkInfo.getState() != NetworkInfo.State.DISCONNECTED);
    }
    
    public boolean isCarrierAp() {
        return this.mIsCarrierAp;
    }
    
    public boolean isConnectable() {
        return this.getLevel() != -1 && this.getDetailedState() == null;
    }
    
    public boolean isEphemeral() {
        return this.mInfo != null && this.mInfo.isEphemeral() && this.mNetworkInfo != null && this.mNetworkInfo.getState() != NetworkInfo.State.DISCONNECTED;
    }
    
    public boolean isMetered() {
        return this.mIsScoredNetworkMetered || WifiConfiguration.isMetered(this.mConfig, this.mInfo);
    }
    
    public boolean isPasspoint() {
        return this.mConfig != null && this.mConfig.isPasspoint();
    }
    
    public boolean isPasspointConfig() {
        return this.mFqdn != null;
    }
    
    public boolean isReachable() {
        return this.mRssi != Integer.MIN_VALUE;
    }
    
    public boolean isSaved() {
        return this.networkId != -1;
    }
    
    @VisibleForTesting
    void loadConfig(final WifiConfiguration mConfig) {
        String removeDoubleQuotes;
        if (mConfig.SSID == null) {
            removeDoubleQuotes = "";
        }
        else {
            removeDoubleQuotes = removeDoubleQuotes(mConfig.SSID);
        }
        this.ssid = removeDoubleQuotes;
        this.bssid = mConfig.BSSID;
        this.security = getSecurity(mConfig);
        this.updateKey();
        this.networkId = mConfig.networkId;
        this.mConfig = mConfig;
    }
    
    public boolean matches(final WifiConfiguration wifiConfiguration) {
        final boolean passpoint = wifiConfiguration.isPasspoint();
        boolean b = false;
        final boolean b2 = false;
        if (passpoint && this.mConfig != null && this.mConfig.isPasspoint()) {
            boolean b3 = b2;
            if (this.ssid.equals(removeDoubleQuotes(wifiConfiguration.SSID))) {
                b3 = b2;
                if (wifiConfiguration.FQDN.equals(this.mConfig.FQDN)) {
                    b3 = true;
                }
            }
            return b3;
        }
        if (this.ssid.equals(removeDoubleQuotes(wifiConfiguration.SSID)) && this.security == getSecurity(wifiConfiguration) && (this.mConfig == null || this.mConfig.shared == wifiConfiguration.shared)) {
            b = true;
        }
        return b;
    }
    
    public void saveWifiState(final Bundle bundle) {
        if (this.ssid != null) {
            bundle.putString("key_ssid", this.getSsidStr());
        }
        bundle.putInt("key_security", this.security);
        bundle.putInt("key_speed", this.mSpeed);
        bundle.putInt("key_psktype", this.pskType);
        if (this.mConfig != null) {
            bundle.putParcelable("key_config", (Parcelable)this.mConfig);
        }
        bundle.putParcelable("key_wifiinfo", (Parcelable)this.mInfo);
        bundle.putParcelableArray("key_scanresults", (Parcelable[])this.mScanResults.toArray((Object[])new Parcelable[this.mScanResults.size()]));
        bundle.putParcelableArrayList("key_scorednetworkcache", new ArrayList((Collection<? extends E>)this.mScoredNetworkCache.values()));
        if (this.mNetworkInfo != null) {
            bundle.putParcelable("key_networkinfo", (Parcelable)this.mNetworkInfo);
        }
        if (this.mFqdn != null) {
            bundle.putString("key_fqdn", this.mFqdn);
        }
        if (this.mProviderFriendlyName != null) {
            bundle.putString("key_provider_friendly_name", this.mProviderFriendlyName);
        }
        bundle.putBoolean("key_is_carrier_ap", this.mIsCarrierAp);
        bundle.putInt("key_carrier_ap_eap_type", this.mCarrierApEapType);
        bundle.putString("key_carrier_name", this.mCarrierName);
    }
    
    public void setListener(final AccessPointListener mAccessPointListener) {
        this.mAccessPointListener = mAccessPointListener;
    }
    
    @VisibleForTesting
    void setRssi(final int mRssi) {
        this.mRssi = mRssi;
    }
    
    void setScanResults(final Collection<ScanResult> collection) {
        final String key = this.getKey();
        for (final ScanResult scanResult : collection) {
            final String key2 = getKey(scanResult);
            if (this.mKey.equals(key2)) {
                continue;
            }
            throw new IllegalArgumentException(String.format("ScanResult %s\nkey of %s did not match current AP key %s", scanResult, key2, key));
        }
        final int level = this.getLevel();
        this.mScanResults.clear();
        this.mScanResults.addAll((Collection)collection);
        this.updateRssi();
        final int level2 = this.getLevel();
        if (level2 > 0 && level2 != level) {
            this.updateSpeed();
            ThreadUtils.postOnMainThread(new _$$Lambda$AccessPoint$MkkIS1nUbezHicDMmYnviyiBJyo(this));
        }
        ThreadUtils.postOnMainThread(new _$$Lambda$AccessPoint$0Yq14aFJZLjPMzFGAvglLaxsblI(this));
        if (!collection.isEmpty()) {
            final ScanResult scanResult2 = collection.iterator().next();
            if (this.security == 2) {
                this.pskType = getPskType(scanResult2);
            }
            this.mIsCarrierAp = scanResult2.isCarrierAp;
            this.mCarrierApEapType = scanResult2.carrierApEapType;
            this.mCarrierName = scanResult2.carrierName;
        }
    }
    
    public void setTag(final Object mTag) {
        this.mTag = mTag;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("AccessPoint(");
        final StringBuilder append = sb.append(this.ssid);
        if (this.bssid != null) {
            append.append(":");
            append.append(this.bssid);
        }
        if (this.isSaved()) {
            append.append(',');
            append.append("saved");
        }
        if (this.isActive()) {
            append.append(',');
            append.append("active");
        }
        if (this.isEphemeral()) {
            append.append(',');
            append.append("ephemeral");
        }
        if (this.isConnectable()) {
            append.append(',');
            append.append("connectable");
        }
        if (this.security != 0) {
            append.append(',');
            append.append(securityToString(this.security, this.pskType));
        }
        append.append(",level=");
        append.append(this.getLevel());
        if (this.mSpeed != 0) {
            append.append(",speed=");
            append.append(this.mSpeed);
        }
        append.append(",metered=");
        append.append(this.isMetered());
        if (isVerboseLoggingEnabled()) {
            append.append(",rssi=");
            append.append(this.mRssi);
            append.append(",scan cache size=");
            append.append(this.mScanResults.size());
        }
        append.append(')');
        return append.toString();
    }
    
    void update(final WifiConfiguration mConfig) {
        this.mConfig = mConfig;
        int networkId;
        if (mConfig != null) {
            networkId = mConfig.networkId;
        }
        else {
            networkId = -1;
        }
        this.networkId = networkId;
        ThreadUtils.postOnMainThread(new _$$Lambda$AccessPoint$QyP0aXhFuWtm7lmBu1IY3qbfmBA(this));
    }
    
    public boolean update(final WifiConfiguration wifiConfiguration, final WifiInfo mInfo, final NetworkInfo mNetworkInfo) {
        boolean b = false;
        final int level = this.getLevel();
        if (mInfo != null && this.isInfoForThisAccessPoint(wifiConfiguration, mInfo)) {
            final boolean b2 = this.mInfo == null;
            if (this.mConfig != wifiConfiguration) {
                this.update(wifiConfiguration);
            }
            if (this.mRssi != mInfo.getRssi() && mInfo.getRssi() != -127) {
                this.mRssi = mInfo.getRssi();
                b = true;
            }
            else {
                b = b2;
                if (this.mNetworkInfo != null) {
                    b = b2;
                    if (mNetworkInfo != null) {
                        b = b2;
                        if (this.mNetworkInfo.getDetailedState() != mNetworkInfo.getDetailedState()) {
                            b = true;
                        }
                    }
                }
            }
            this.mInfo = mInfo;
            this.mNetworkInfo = mNetworkInfo;
        }
        else if (this.mInfo != null) {
            b = true;
            this.mInfo = null;
            this.mNetworkInfo = null;
        }
        if (b && this.mAccessPointListener != null) {
            ThreadUtils.postOnMainThread(new _$$Lambda$AccessPoint$S7H59e_8IxpVPy0V68Oc2_zX_rg(this));
            if (level != this.getLevel()) {
                ThreadUtils.postOnMainThread(new _$$Lambda$AccessPoint$QW_1Uw0oxoaKqUtEtPO0oPvH5ng(this));
            }
        }
        return b;
    }
    
    boolean update(final WifiNetworkScoreCache wifiNetworkScoreCache, final boolean b, final long n) {
        boolean updateScores = false;
        if (b) {
            updateScores = this.updateScores(wifiNetworkScoreCache, n);
        }
        return this.updateMetered(wifiNetworkScoreCache) || updateScores;
    }
    
    public interface AccessPointListener
    {
        void onAccessPointChanged(final AccessPoint p0);
        
        void onLevelChanged(final AccessPoint p0);
    }
}
