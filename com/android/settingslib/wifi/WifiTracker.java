package com.android.settingslib.wifi;

import android.net.NetworkCapabilities;
import android.net.Network;
import java.util.Objects;
import com.android.settingslib.utils.ThreadUtils;
import android.widget.Toast;
import com.android.settingslib.R;
import android.os.Message;
import android.net.ScoredNetwork;
import android.net.wifi.WifiNetworkScoreCache$CacheListener;
import android.content.ContentResolver;
import android.net.ConnectivityManager$NetworkCallback;
import android.provider.Settings;
import java.util.Collections;
import java.util.Map;
import android.util.ArrayMap;
import java.util.Collection;
import android.net.INetworkScoreCache;
import android.net.wifi.WifiConfiguration;
import java.util.Iterator;
import android.os.SystemClock;
import android.util.Log;
import com.android.settingslib.core.lifecycle.Lifecycle;
import android.net.NetworkRequest$Builder;
import android.content.Intent;
import android.util.ArraySet;
import java.util.ArrayList;
import android.os.HandlerThread;
import android.os.Handler;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkScoreCache;
import android.net.wifi.ScanResult;
import java.util.HashMap;
import android.net.NetworkKey;
import java.util.Set;
import android.content.BroadcastReceiver;
import android.net.NetworkScoreManager;
import android.net.NetworkRequest;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import java.util.List;
import android.content.IntentFilter;
import android.content.Context;
import android.net.ConnectivityManager;
import java.util.concurrent.atomic.AtomicBoolean;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import com.android.settingslib.core.lifecycle.LifecycleObserver;

public class WifiTracker implements LifecycleObserver, OnDestroy, OnStart, OnStop
{
    public static boolean sVerboseLogging;
    private final AtomicBoolean mConnected;
    private final ConnectivityManager mConnectivityManager;
    private final Context mContext;
    private final IntentFilter mFilter;
    private final List<AccessPoint> mInternalAccessPoints;
    private WifiInfo mLastInfo;
    private NetworkInfo mLastNetworkInfo;
    private final WifiListenerExecutor mListener;
    private final Object mLock;
    private long mMaxSpeedLabelScoreCacheAge;
    private WifiTrackerNetworkCallback mNetworkCallback;
    private final NetworkRequest mNetworkRequest;
    private final NetworkScoreManager mNetworkScoreManager;
    private boolean mNetworkScoringUiEnabled;
    final BroadcastReceiver mReceiver;
    private boolean mRegistered;
    private final Set<NetworkKey> mRequestedScores;
    private final HashMap<String, ScanResult> mScanResultCache;
    Scanner mScanner;
    private WifiNetworkScoreCache mScoreCache;
    private boolean mStaleScanResults;
    private final WifiManager mWifiManager;
    Handler mWorkHandler;
    private HandlerThread mWorkThread;
    
    WifiTracker(final Context mContext, final WifiListener wifiListener, final WifiManager mWifiManager, final ConnectivityManager mConnectivityManager, final NetworkScoreManager mNetworkScoreManager, final IntentFilter mFilter) {
        boolean sVerboseLogging = false;
        this.mConnected = new AtomicBoolean(false);
        this.mLock = new Object();
        this.mInternalAccessPoints = new ArrayList<AccessPoint>();
        this.mRequestedScores = (Set<NetworkKey>)new ArraySet();
        this.mStaleScanResults = true;
        this.mScanResultCache = new HashMap<String, ScanResult>();
        this.mReceiver = new BroadcastReceiver() {
            public void onReceive(final Context context, final Intent intent) {
                final String action = intent.getAction();
                if ("android.net.wifi.WIFI_STATE_CHANGED".equals(action)) {
                    WifiTracker.this.updateWifiState(intent.getIntExtra("wifi_state", 4));
                }
                else if ("android.net.wifi.SCAN_RESULTS".equals(action)) {
                    WifiTracker.this.mStaleScanResults = false;
                    WifiTracker.this.fetchScansAndConfigsAndUpdateAccessPoints();
                }
                else if (!"android.net.wifi.CONFIGURED_NETWORKS_CHANGE".equals(action) && !"android.net.wifi.LINK_CONFIGURATION_CHANGED".equals(action)) {
                    if ("android.net.wifi.STATE_CHANGE".equals(action)) {
                        WifiTracker.this.updateNetworkInfo((NetworkInfo)intent.getParcelableExtra("networkInfo"));
                        WifiTracker.this.fetchScansAndConfigsAndUpdateAccessPoints();
                    }
                    else if ("android.net.wifi.RSSI_CHANGED".equals(action)) {
                        WifiTracker.this.updateNetworkInfo(WifiTracker.this.mConnectivityManager.getNetworkInfo(WifiTracker.this.mWifiManager.getCurrentNetwork()));
                    }
                }
                else {
                    WifiTracker.this.fetchScansAndConfigsAndUpdateAccessPoints();
                }
            }
        };
        this.mContext = mContext;
        this.mWifiManager = mWifiManager;
        this.mListener = new WifiListenerExecutor(wifiListener);
        this.mConnectivityManager = mConnectivityManager;
        if (this.mWifiManager.getVerboseLoggingLevel() > 0) {
            sVerboseLogging = true;
        }
        WifiTracker.sVerboseLogging = sVerboseLogging;
        this.mFilter = mFilter;
        this.mNetworkRequest = new NetworkRequest$Builder().clearCapabilities().addCapability(15).addTransportType(1).build();
        this.mNetworkScoreManager = mNetworkScoreManager;
        final StringBuilder sb = new StringBuilder();
        sb.append("WifiTracker{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("}");
        final HandlerThread workThread = new HandlerThread(sb.toString(), 10);
        workThread.start();
        this.setWorkThread(workThread);
    }
    
    public WifiTracker(final Context context, final WifiListener wifiListener, final Lifecycle lifecycle, final boolean b, final boolean b2) {
        this(context, wifiListener, (WifiManager)context.getSystemService((Class)WifiManager.class), (ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class), (NetworkScoreManager)context.getSystemService((Class)NetworkScoreManager.class), newIntentFilter());
        lifecycle.addObserver(this);
    }
    
    private static final boolean DBG() {
        return Log.isLoggable("WifiTracker", 3);
    }
    
    private void clearAccessPointsAndConditionallyUpdate() {
        synchronized (this.mLock) {
            if (!this.mInternalAccessPoints.isEmpty()) {
                this.mInternalAccessPoints.clear();
                this.conditionallyNotifyListeners();
            }
        }
    }
    
    private void conditionallyNotifyListeners() {
        if (this.mStaleScanResults) {
            return;
        }
        this.mListener.onAccessPointsChanged();
    }
    
    private void evictOldScans() {
        final long elapsedRealtime = SystemClock.elapsedRealtime();
        final Iterator<ScanResult> iterator = this.mScanResultCache.values().iterator();
        while (iterator.hasNext()) {
            if (elapsedRealtime - iterator.next().timestamp / 1000L > 25000L) {
                iterator.remove();
            }
        }
    }
    
    private void fetchScansAndConfigsAndUpdateAccessPoints() {
        final List scanResults = this.mWifiManager.getScanResults();
        if (isVerboseLoggingEnabled()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Fetched scan results: ");
            sb.append(scanResults);
            Log.i("WifiTracker", sb.toString());
        }
        this.updateAccessPoints(scanResults, this.mWifiManager.getConfiguredNetworks());
    }
    
    private void forceUpdate() {
        this.mLastInfo = this.mWifiManager.getConnectionInfo();
        this.mLastNetworkInfo = this.mConnectivityManager.getNetworkInfo(this.mWifiManager.getCurrentNetwork());
        this.fetchScansAndConfigsAndUpdateAccessPoints();
    }
    
    private WifiConfiguration getWifiConfigurationForNetworkId(final int n, final List<WifiConfiguration> list) {
        if (list != null) {
            for (final WifiConfiguration wifiConfiguration : list) {
                if (this.mLastInfo != null && n == wifiConfiguration.networkId && (!wifiConfiguration.selfAdded || wifiConfiguration.numAssociation != 0)) {
                    return wifiConfiguration;
                }
            }
        }
        return null;
    }
    
    private static boolean isVerboseLoggingEnabled() {
        return WifiTracker.sVerboseLogging || Log.isLoggable("WifiTracker", 2);
    }
    
    private static IntentFilter newIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        intentFilter.addAction("android.net.wifi.NETWORK_IDS_CHANGED");
        intentFilter.addAction("android.net.wifi.supplicant.STATE_CHANGE");
        intentFilter.addAction("android.net.wifi.CONFIGURED_NETWORKS_CHANGE");
        intentFilter.addAction("android.net.wifi.LINK_CONFIGURATION_CHANGED");
        intentFilter.addAction("android.net.wifi.STATE_CHANGE");
        intentFilter.addAction("android.net.wifi.RSSI_CHANGED");
        return intentFilter;
    }
    
    private void pauseScanning() {
        if (this.mScanner != null) {
            this.mScanner.pause();
            this.mScanner = null;
        }
        this.mStaleScanResults = true;
    }
    
    private void registerScoreCache() {
        this.mNetworkScoreManager.registerNetworkScoreCache(1, (INetworkScoreCache)this.mScoreCache, 2);
    }
    
    private void requestScoresForNetworkKeys(final Collection<NetworkKey> collection) {
        if (collection.isEmpty()) {
            return;
        }
        if (DBG()) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Requesting scores for Network Keys: ");
            sb.append(collection);
            Log.d("WifiTracker", sb.toString());
        }
        this.mNetworkScoreManager.requestScores((NetworkKey[])collection.toArray(new NetworkKey[collection.size()]));
        synchronized (this.mLock) {
            this.mRequestedScores.addAll(collection);
        }
    }
    
    private void unregisterScoreCache() {
        this.mNetworkScoreManager.unregisterNetworkScoreCache(1, (INetworkScoreCache)this.mScoreCache);
        synchronized (this.mLock) {
            this.mRequestedScores.clear();
        }
    }
    
    private void updateAccessPoints(final List<ScanResult> list, final List<WifiConfiguration> list2) {
        final ArrayMap arrayMap = new ArrayMap(list2.size());
        if (list2 != null) {
            for (final WifiConfiguration wifiConfiguration : list2) {
                ((Map<String, WifiConfiguration>)arrayMap).put(AccessPoint.getKey(wifiConfiguration), wifiConfiguration);
            }
        }
        final ArrayMap<String, List<ScanResult>> updateScanResultCache = this.updateScanResultCache(list);
        WifiConfiguration wifiConfigurationForNetworkId = null;
        if (this.mLastInfo != null) {
            wifiConfigurationForNetworkId = this.getWifiConfigurationForNetworkId(this.mLastInfo.getNetworkId(), list2);
        }
        synchronized (this.mLock) {
            final ArrayList<AccessPoint> list3 = new ArrayList<AccessPoint>(this.mInternalAccessPoints);
            final ArrayList<Comparable> list4 = (ArrayList<Comparable>)new ArrayList<AccessPoint>();
            final ArrayList<NetworkKey> list5 = new ArrayList<NetworkKey>();
            for (final Map.Entry<Object, V> entry : updateScanResultCache.entrySet()) {
                final Iterator iterator3 = ((List)entry.getValue()).iterator();
                while (iterator3.hasNext()) {
                    final NetworkKey fromScanResult = NetworkKey.createFromScanResult((ScanResult)iterator3.next());
                    if (fromScanResult != null && !this.mRequestedScores.contains(fromScanResult)) {
                        list5.add(fromScanResult);
                    }
                }
                final AccessPoint cachedOrCreate = this.getCachedOrCreate((List<ScanResult>)entry.getValue(), list3);
                if (this.mLastInfo != null && this.mLastNetworkInfo != null) {
                    cachedOrCreate.update(wifiConfigurationForNetworkId, this.mLastInfo, this.mLastNetworkInfo);
                }
                cachedOrCreate.update(((Map<String, WifiConfiguration>)arrayMap).get(entry.getKey()));
                list4.add(cachedOrCreate);
            }
            if (list4.isEmpty() && wifiConfigurationForNetworkId != null) {
                final AccessPoint accessPoint = new AccessPoint(this.mContext, wifiConfigurationForNetworkId);
                accessPoint.update(wifiConfigurationForNetworkId, this.mLastInfo, this.mLastNetworkInfo);
                list4.add(accessPoint);
                list5.add(NetworkKey.createFromWifiInfo(this.mLastInfo));
            }
            this.requestScoresForNetworkKeys(list5);
            final Iterator<AccessPoint> iterator4 = list4.iterator();
            while (iterator4.hasNext()) {
                iterator4.next().update(this.mScoreCache, this.mNetworkScoringUiEnabled, this.mMaxSpeedLabelScoreCacheAge);
            }
            Collections.sort(list4);
            if (DBG()) {
                Log.d("WifiTracker", "------ Dumping SSIDs that were not seen on this scan ------");
                for (final AccessPoint accessPoint2 : this.mInternalAccessPoints) {
                    if (accessPoint2.getSsid() == null) {
                        continue;
                    }
                    final String ssidStr = accessPoint2.getSsidStr();
                    final boolean b = false;
                    final Iterator<AccessPoint> iterator6 = list4.iterator();
                    boolean b2;
                    while (true) {
                        b2 = b;
                        if (!iterator6.hasNext()) {
                            break;
                        }
                        final AccessPoint accessPoint3 = iterator6.next();
                        if (accessPoint3.getSsidStr() != null && accessPoint3.getSsidStr().equals(ssidStr)) {
                            b2 = true;
                            break;
                        }
                    }
                    if (b2) {
                        continue;
                    }
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Did not find ");
                    sb.append(ssidStr);
                    sb.append(" in this scan");
                    Log.d("WifiTracker", sb.toString());
                }
                Log.d("WifiTracker", "---- Done dumping SSIDs that were not seen on this scan ----");
            }
            this.mInternalAccessPoints.clear();
            this.mInternalAccessPoints.addAll((Collection<? extends AccessPoint>)list4);
            // monitorexit(this.mLock)
            this.conditionallyNotifyListeners();
        }
    }
    
    private void updateNetworkInfo(final NetworkInfo mLastNetworkInfo) {
        if (!this.mWifiManager.isWifiEnabled()) {
            this.clearAccessPointsAndConditionallyUpdate();
            return;
        }
        if (mLastNetworkInfo != null) {
            this.mLastNetworkInfo = mLastNetworkInfo;
            if (DBG()) {
                final StringBuilder sb = new StringBuilder();
                sb.append("mLastNetworkInfo set: ");
                sb.append(this.mLastNetworkInfo);
                Log.d("WifiTracker", sb.toString());
            }
            if (mLastNetworkInfo.isConnected() != this.mConnected.getAndSet(mLastNetworkInfo.isConnected())) {
                this.mListener.onConnectedChanged();
            }
        }
        WifiConfiguration wifiConfigurationForNetworkId = null;
        this.mLastInfo = this.mWifiManager.getConnectionInfo();
        if (DBG()) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("mLastInfo set as: ");
            sb2.append(this.mLastInfo);
            Log.d("WifiTracker", sb2.toString());
        }
        if (this.mLastInfo != null) {
            wifiConfigurationForNetworkId = this.getWifiConfigurationForNetworkId(this.mLastInfo.getNetworkId(), this.mWifiManager.getConfiguredNetworks());
        }
        boolean b = false;
        int n = 0;
        synchronized (this.mLock) {
            for (int i = this.mInternalAccessPoints.size() - 1; i >= 0; --i) {
                final AccessPoint accessPoint = this.mInternalAccessPoints.get(i);
                final boolean active = accessPoint.isActive();
                int n2 = n;
                if (accessPoint.update(wifiConfigurationForNetworkId, this.mLastInfo, this.mLastNetworkInfo)) {
                    b = true;
                    n2 = n;
                    if (active != accessPoint.isActive()) {
                        n2 = 1;
                        b = b;
                    }
                }
                n = n2;
                if (accessPoint.update(this.mScoreCache, this.mNetworkScoringUiEnabled, this.mMaxSpeedLabelScoreCacheAge)) {
                    n = 1;
                    b = true;
                }
            }
            if (n != 0) {
                Collections.sort(this.mInternalAccessPoints);
            }
            if (b) {
                this.conditionallyNotifyListeners();
            }
        }
    }
    
    private void updateNetworkScores() {
        final Object mLock = this.mLock;
        // monitorenter(mLock)
        boolean b = false;
        int i = 0;
        try {
            while (i < this.mInternalAccessPoints.size()) {
                if (this.mInternalAccessPoints.get(i).update(this.mScoreCache, this.mNetworkScoringUiEnabled, this.mMaxSpeedLabelScoreCacheAge)) {
                    b = true;
                }
                ++i;
            }
            if (b) {
                Collections.sort(this.mInternalAccessPoints);
                this.conditionallyNotifyListeners();
            }
        }
        finally {
        }
        // monitorexit(mLock)
    }
    
    private ArrayMap<String, List<ScanResult>> updateScanResultCache(final List<ScanResult> list) {
        for (final ScanResult scanResult : list) {
            if (scanResult.SSID != null) {
                if (scanResult.SSID.isEmpty()) {
                    continue;
                }
                this.mScanResultCache.put(scanResult.BSSID, scanResult);
            }
        }
        if (!this.mStaleScanResults) {
            this.evictOldScans();
        }
        final ArrayMap arrayMap = new ArrayMap();
        for (final ScanResult scanResult2 : this.mScanResultCache.values()) {
            if (scanResult2.SSID != null && scanResult2.SSID.length() != 0) {
                if (scanResult2.capabilities.contains("[IBSS]")) {
                    continue;
                }
                final String key = AccessPoint.getKey(scanResult2);
                List<ScanResult> list2;
                if (arrayMap.containsKey((Object)key)) {
                    list2 = (List<ScanResult>)arrayMap.get((Object)key);
                }
                else {
                    list2 = new ArrayList<ScanResult>();
                    arrayMap.put((Object)key, (Object)list2);
                }
                list2.add(scanResult2);
            }
        }
        return (ArrayMap<String, List<ScanResult>>)arrayMap;
    }
    
    private void updateWifiState(final int n) {
        if (n == 3) {
            if (this.mScanner != null) {
                this.mScanner.resume();
            }
        }
        else {
            this.clearAccessPointsAndConditionallyUpdate();
            this.mLastInfo = null;
            this.mLastNetworkInfo = null;
            if (this.mScanner != null) {
                this.mScanner.pause();
            }
            this.mStaleScanResults = true;
        }
        this.mListener.onWifiStateChanged(n);
    }
    
    public List<AccessPoint> getAccessPoints() {
        synchronized (this.mLock) {
            return new ArrayList<AccessPoint>(this.mInternalAccessPoints);
        }
    }
    
    AccessPoint getCachedOrCreate(final List<ScanResult> scanResults, final List<AccessPoint> list) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (list.get(i).getKey().equals(AccessPoint.getKey(scanResults.get(0)))) {
                final AccessPoint accessPoint = list.remove(i);
                accessPoint.setScanResults(scanResults);
                return accessPoint;
            }
        }
        return new AccessPoint(this.mContext, scanResults);
    }
    
    public WifiManager getManager() {
        return this.mWifiManager;
    }
    
    public int getNumSavedNetworks() {
        return WifiSavedConfigUtils.getAllConfigs(this.mContext, this.mWifiManager).size();
    }
    
    public boolean isConnected() {
        return this.mConnected.get();
    }
    
    @Override
    public void onDestroy() {
        this.mWorkThread.quit();
    }
    
    @Override
    public void onStart() {
        this.forceUpdate();
        this.registerScoreCache();
        final ContentResolver contentResolver = this.mContext.getContentResolver();
        boolean mNetworkScoringUiEnabled = false;
        if (Settings.Global.getInt(contentResolver, "network_scoring_ui_enabled", 0) == 1) {
            mNetworkScoringUiEnabled = true;
        }
        this.mNetworkScoringUiEnabled = mNetworkScoringUiEnabled;
        this.mMaxSpeedLabelScoreCacheAge = Settings.Global.getLong(this.mContext.getContentResolver(), "speed_label_cache_eviction_age_millis", 1200000L);
        this.resumeScanning();
        if (!this.mRegistered) {
            this.mContext.registerReceiver(this.mReceiver, this.mFilter, (String)null, this.mWorkHandler);
            this.mNetworkCallback = new WifiTrackerNetworkCallback();
            this.mConnectivityManager.registerNetworkCallback(this.mNetworkRequest, (ConnectivityManager$NetworkCallback)this.mNetworkCallback, this.mWorkHandler);
            this.mRegistered = true;
        }
    }
    
    @Override
    public void onStop() {
        if (this.mRegistered) {
            this.mContext.unregisterReceiver(this.mReceiver);
            this.mConnectivityManager.unregisterNetworkCallback((ConnectivityManager$NetworkCallback)this.mNetworkCallback);
            this.mRegistered = false;
        }
        this.unregisterScoreCache();
        this.pauseScanning();
        this.mWorkHandler.removeCallbacksAndMessages((Object)null);
    }
    
    public void resumeScanning() {
        if (this.mScanner == null) {
            this.mScanner = new Scanner();
        }
        if (this.mWifiManager.isWifiEnabled()) {
            this.mScanner.resume();
        }
    }
    
    void setWorkThread(final HandlerThread mWorkThread) {
        this.mWorkThread = mWorkThread;
        this.mWorkHandler = new Handler(mWorkThread.getLooper());
        this.mScoreCache = new WifiNetworkScoreCache(this.mContext, (WifiNetworkScoreCache$CacheListener)new WifiNetworkScoreCache$CacheListener(this.mWorkHandler) {
            public void networkCacheUpdated(final List<ScoredNetwork> list) {
                if (!WifiTracker.this.mRegistered) {
                    return;
                }
                if (Log.isLoggable("WifiTracker", 2)) {
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Score cache was updated with networks: ");
                    sb.append(list);
                    Log.v("WifiTracker", sb.toString());
                }
                WifiTracker.this.updateNetworkScores();
            }
        });
    }
    
    class Scanner extends Handler
    {
        private int mRetry;
        
        Scanner() {
            this.mRetry = 0;
        }
        
        public void handleMessage(final Message message) {
            if (message.what != 0) {
                return;
            }
            if (WifiTracker.this.mWifiManager.startScan()) {
                this.mRetry = 0;
            }
            else if (++this.mRetry >= 3) {
                this.mRetry = 0;
                if (WifiTracker.this.mContext != null) {
                    Toast.makeText(WifiTracker.this.mContext, R.string.wifi_fail_to_scan, 1).show();
                }
                return;
            }
            this.sendEmptyMessageDelayed(0, 10000L);
        }
        
        boolean isScanning() {
            return this.hasMessages(0);
        }
        
        void pause() {
            this.removeMessages(this.mRetry = 0);
        }
        
        void resume() {
            if (!this.hasMessages(0)) {
                this.sendEmptyMessage(0);
            }
        }
    }
    
    public interface WifiListener
    {
        void onAccessPointsChanged();
        
        void onConnectedChanged();
        
        void onWifiStateChanged(final int p0);
    }
    
    class WifiListenerExecutor implements WifiListener
    {
        private final WifiListener mDelegatee;
        
        public WifiListenerExecutor(final WifiListener mDelegatee) {
            this.mDelegatee = mDelegatee;
        }
        
        private void runAndLog(final Runnable runnable, final String s) {
            ThreadUtils.postOnMainThread(new _$$Lambda$WifiTracker$WifiListenerExecutor$BMWc3s6WnR_Ijg_9a3gQADAjI3Y(this, s, runnable));
        }
        
        @Override
        public void onAccessPointsChanged() {
            final WifiListener mDelegatee = this.mDelegatee;
            Objects.requireNonNull(mDelegatee);
            this.runAndLog(new _$$Lambda$evcvquoPxZkPmBIit31UXvhXEJk(mDelegatee), "Invoking onAccessPointsChanged callback");
        }
        
        @Override
        public void onConnectedChanged() {
            final WifiListener mDelegatee = this.mDelegatee;
            Objects.requireNonNull(mDelegatee);
            this.runAndLog(new _$$Lambda$6PbPNXCvqbAnKbPWPJrs_dDWQEQ(mDelegatee), "Invoking onConnectedChanged callback");
        }
        
        @Override
        public void onWifiStateChanged(final int n) {
            this.runAndLog(new _$$Lambda$WifiTracker$WifiListenerExecutor$PZBvWEzpVHhaI95PbZNbzEgAH1I(this, n), String.format("Invoking onWifiStateChanged callback with state %d", n));
        }
    }
    
    private final class WifiTrackerNetworkCallback extends ConnectivityManager$NetworkCallback
    {
        public void onCapabilitiesChanged(final Network network, final NetworkCapabilities networkCapabilities) {
            if (network.equals((Object)WifiTracker.this.mWifiManager.getCurrentNetwork())) {
                WifiTracker.this.updateNetworkInfo(null);
            }
        }
    }
}
