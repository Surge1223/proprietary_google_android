package com.android.settingslib;

import android.content.ContentResolver;
import android.provider.Settings;
import android.content.Context;

public class WirelessUtils
{
    public static boolean isAirplaneModeOn(final Context context) {
        final ContentResolver contentResolver = context.getContentResolver();
        boolean b = false;
        if (Settings.Global.getInt(contentResolver, "airplane_mode_on", 0) != 0) {
            b = true;
        }
        return b;
    }
    
    public static boolean isRadioAllowed(final Context context, final String s) {
        final boolean airplaneModeOn = isAirplaneModeOn(context);
        boolean b = true;
        if (!airplaneModeOn) {
            return true;
        }
        final String string = Settings.Global.getString(context.getContentResolver(), "airplane_mode_toggleable_radios");
        if (string == null || !string.contains(s)) {
            b = false;
        }
        return b;
    }
}
