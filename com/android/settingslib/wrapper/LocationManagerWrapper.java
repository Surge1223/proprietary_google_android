package com.android.settingslib.wrapper;

import android.os.UserHandle;
import android.location.LocationManager;

public class LocationManagerWrapper
{
    private LocationManager mLocationManager;
    
    public LocationManagerWrapper(final LocationManager mLocationManager) {
        this.mLocationManager = mLocationManager;
    }
    
    public void setLocationEnabledForUser(final boolean b, final UserHandle userHandle) {
        this.mLocationManager.setLocationEnabledForUser(b, userHandle);
    }
}
