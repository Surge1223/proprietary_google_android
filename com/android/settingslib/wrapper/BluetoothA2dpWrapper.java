package com.android.settingslib.wrapper;

import android.bluetooth.BluetoothCodecStatus;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothA2dp;

public class BluetoothA2dpWrapper
{
    private BluetoothA2dp mService;
    
    public BluetoothA2dpWrapper(final BluetoothA2dp mService) {
        this.mService = mService;
    }
    
    public BluetoothCodecStatus getCodecStatus(final BluetoothDevice bluetoothDevice) {
        return this.mService.getCodecStatus(bluetoothDevice);
    }
    
    public int getOptionalCodecsEnabled(final BluetoothDevice bluetoothDevice) {
        return this.mService.getOptionalCodecsEnabled(bluetoothDevice);
    }
    
    public void setOptionalCodecsEnabled(final BluetoothDevice bluetoothDevice, final int n) {
        this.mService.setOptionalCodecsEnabled(bluetoothDevice, n);
    }
    
    public int supportsOptionalCodecs(final BluetoothDevice bluetoothDevice) {
        return this.mService.supportsOptionalCodecs(bluetoothDevice);
    }
}
