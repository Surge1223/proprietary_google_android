package com.android.settingslib.wrapper;

import android.content.IntentFilter;
import android.content.Intent;
import android.os.storage.VolumeInfo;
import android.content.pm.PackageInfo;
import android.os.UserHandle;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.PackageManager;

public class PackageManagerWrapper
{
    private final PackageManager mPm;
    
    public PackageManagerWrapper(final PackageManager mPm) {
        this.mPm = mPm;
    }
    
    public void deletePackageAsUser(final String s, final IPackageDeleteObserver packageDeleteObserver, final int n, final int n2) {
        this.mPm.deletePackageAsUser(s, packageDeleteObserver, n, n2);
    }
    
    public int getApplicationEnabledSetting(final String s) {
        return this.mPm.getApplicationEnabledSetting(s);
    }
    
    public ApplicationInfo getApplicationInfo(final String s, final int n) throws PackageManager$NameNotFoundException {
        return this.mPm.getApplicationInfo(s, n);
    }
    
    public ApplicationInfo getApplicationInfoAsUser(final String s, final int n, final int n2) throws PackageManager$NameNotFoundException {
        return this.mPm.getApplicationInfoAsUser(s, n, n2);
    }
    
    public CharSequence getApplicationLabel(final ApplicationInfo applicationInfo) {
        return this.mPm.getApplicationLabel(applicationInfo);
    }
    
    public String getDefaultBrowserPackageNameAsUser(final int n) {
        return this.mPm.getDefaultBrowserPackageNameAsUser(n);
    }
    
    public ComponentName getHomeActivities(final List<ResolveInfo> list) {
        return this.mPm.getHomeActivities((List)list);
    }
    
    public int getInstallReason(final String s, final UserHandle userHandle) {
        return this.mPm.getInstallReason(s, userHandle);
    }
    
    public List<ApplicationInfo> getInstalledApplicationsAsUser(final int n, final int n2) {
        return (List<ApplicationInfo>)this.mPm.getInstalledApplicationsAsUser(n, n2);
    }
    
    public List<PackageInfo> getInstalledPackagesAsUser(final int n, final int n2) {
        return (List<PackageInfo>)this.mPm.getInstalledPackagesAsUser(n, n2);
    }
    
    public PackageManager getPackageManager() {
        return this.mPm;
    }
    
    public int getPackageUidAsUser(final String s, final int n) throws PackageManager$NameNotFoundException {
        return this.mPm.getPackageUidAsUser(s, n);
    }
    
    public VolumeInfo getPrimaryStorageCurrentVolume() {
        return this.mPm.getPrimaryStorageCurrentVolume();
    }
    
    public boolean hasSystemFeature(final String s) {
        return this.mPm.hasSystemFeature(s);
    }
    
    public List<ResolveInfo> queryBroadcastReceivers(final Intent intent, final int n) {
        return (List<ResolveInfo>)this.mPm.queryBroadcastReceivers(intent, n);
    }
    
    public List<ResolveInfo> queryIntentActivities(final Intent intent, final int n) {
        return (List<ResolveInfo>)this.mPm.queryIntentActivities(intent, n);
    }
    
    public List<ResolveInfo> queryIntentActivitiesAsUser(final Intent intent, final int n, final int n2) {
        return (List<ResolveInfo>)this.mPm.queryIntentActivitiesAsUser(intent, n, n2);
    }
    
    public List<ResolveInfo> queryIntentServices(final Intent intent, final int n) {
        return (List<ResolveInfo>)this.mPm.queryIntentServices(intent, n);
    }
    
    public List<ResolveInfo> queryIntentServicesAsUser(final Intent intent, final int n, final int n2) {
        return (List<ResolveInfo>)this.mPm.queryIntentServicesAsUser(intent, n, n2);
    }
    
    public void replacePreferredActivity(final IntentFilter intentFilter, final int n, final ComponentName[] array, final ComponentName componentName) {
        this.mPm.replacePreferredActivity(intentFilter, n, array, componentName);
    }
    
    public void setApplicationEnabledSetting(final String s, final int n, final int n2) {
        this.mPm.setApplicationEnabledSetting(s, n, n2);
    }
    
    public boolean setDefaultBrowserPackageNameAsUser(final String s, final int n) {
        return this.mPm.setDefaultBrowserPackageNameAsUser(s, n);
    }
}
