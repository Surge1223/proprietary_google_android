package com.android.settingslib.license;

import org.xmlpull.v1.XmlPullParser;
import android.text.TextUtils;
import java.io.Reader;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import android.util.Log;
import java.util.Iterator;
import java.util.Collections;
import java.util.Collection;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.util.HashMap;
import java.io.File;
import java.util.List;
import java.util.Map;

class LicenseHtmlGeneratorFromXml
{
    private final Map<String, String> mContentIdToFileContentMap;
    private final Map<String, String> mFileNameToContentIdMap;
    private final List<File> mXmlFiles;
    
    private LicenseHtmlGeneratorFromXml(final List<File> mXmlFiles) {
        this.mFileNameToContentIdMap = new HashMap<String, String>();
        this.mContentIdToFileContentMap = new HashMap<String, String>();
        this.mXmlFiles = mXmlFiles;
    }
    
    static void generateHtml(final Map<String, String> map, final Map<String, String> map2, final PrintWriter printWriter) {
        final ArrayList<String> list = (ArrayList<String>)new ArrayList<Comparable>();
        list.addAll((Collection<? extends Comparable>)map.keySet());
        Collections.sort((List<Comparable>)list);
        printWriter.println("<html><head>\n<style type=\"text/css\">\nbody { padding: 0; font-family: sans-serif; }\n.same-license { background-color: #eeeeee;\n                border-top: 20px solid white;\n                padding: 10px; }\n.label { font-weight: bold; }\n.file-list { margin-left: 1em; color: blue; }\n</style>\n</head><body topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\" bottommargin=\"0\">\n<div class=\"toc\">\n<ul>");
        int n = 0;
        final HashMap<Object, Integer> hashMap = new HashMap<Object, Integer>();
        final ArrayList<ContentIdAndFileNames> list2 = new ArrayList<ContentIdAndFileNames>();
        for (final String s : list) {
            final String s2 = map.get(s);
            int n2 = n;
            if (!hashMap.containsKey(s2)) {
                hashMap.put(s2, n);
                list2.add(new ContentIdAndFileNames(s2));
                n2 = n + 1;
            }
            final int intValue = hashMap.get(s2);
            ((ContentIdAndFileNames)list2.get(intValue)).mFileNameList.add(s);
            printWriter.format("<li><a href=\"#id%d\">%s</a></li>\n", intValue, s);
            n = n2;
        }
        printWriter.println("</ul>\n</div><!-- table of contents -->\n<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
        int n3 = 0;
        for (final ContentIdAndFileNames contentIdAndFileNames : list2) {
            printWriter.format("<tr id=\"id%d\"><td class=\"same-license\">\n", n3);
            printWriter.println("<div class=\"label\">Notices for file(s):</div>");
            printWriter.println("<div class=\"file-list\">");
            final Iterator<String> iterator3 = contentIdAndFileNames.mFileNameList.iterator();
            while (iterator3.hasNext()) {
                printWriter.format("%s <br/>\n", iterator3.next());
            }
            printWriter.println("</div><!-- file-list -->");
            printWriter.println("<pre class=\"license-text\">");
            printWriter.println(map2.get(contentIdAndFileNames.mContentId));
            printWriter.println("</pre><!-- license-text -->");
            printWriter.println("</td></tr><!-- same-license -->");
            ++n3;
        }
        printWriter.println("</table></body></html>");
    }
    
    private boolean generateHtml(final File file) {
        final Iterator<File> iterator = this.mXmlFiles.iterator();
        while (iterator.hasNext()) {
            this.parse(iterator.next());
        }
        if (!this.mFileNameToContentIdMap.isEmpty()) {
            if (!this.mContentIdToFileContentMap.isEmpty()) {
                PrintWriter printWriter2;
                final PrintWriter printWriter = printWriter2 = null;
                try {
                    printWriter2 = printWriter;
                    final PrintWriter printWriter3 = printWriter2 = new PrintWriter(file);
                    generateHtml(this.mFileNameToContentIdMap, this.mContentIdToFileContentMap, printWriter3);
                    printWriter2 = printWriter3;
                    printWriter3.flush();
                    printWriter2 = printWriter3;
                    printWriter3.close();
                    return true;
                }
                catch (FileNotFoundException | SecurityException ex) {
                    final Object o2;
                    final Object o = o2;
                    final StringBuilder sb = new StringBuilder();
                    sb.append("Failed to generate ");
                    sb.append(file);
                    Log.e("LicenseHtmlGeneratorFromXml", sb.toString(), (Throwable)o);
                    if (printWriter2 != null) {
                        printWriter2.close();
                    }
                    return false;
                }
            }
        }
        return false;
    }
    
    public static boolean generateHtml(final List<File> list, final File file) {
        return new LicenseHtmlGeneratorFromXml(list).generateHtml(file);
    }
    
    private void parse(final File file) {
        if (file != null && file.exists() && file.length() != 0L) {
            InputStreamReader inputStreamReader2;
            final InputStreamReader inputStreamReader = inputStreamReader2 = null;
            try {
                InputStreamReader inputStreamReader3;
                if (file.getName().endsWith(".gz")) {
                    inputStreamReader2 = inputStreamReader;
                    inputStreamReader3 = new(java.io.InputStreamReader.class);
                    inputStreamReader2 = inputStreamReader;
                    inputStreamReader2 = inputStreamReader;
                    inputStreamReader2 = inputStreamReader;
                    final FileInputStream fileInputStream = new FileInputStream(file);
                    inputStreamReader2 = inputStreamReader;
                    final GZIPInputStream gzipInputStream = new GZIPInputStream(fileInputStream);
                    inputStreamReader2 = inputStreamReader;
                    new InputStreamReader(gzipInputStream);
                }
                else {
                    inputStreamReader2 = inputStreamReader;
                    inputStreamReader3 = new FileReader(file);
                }
                inputStreamReader2 = inputStreamReader3;
                parse(inputStreamReader3, this.mFileNameToContentIdMap, this.mContentIdToFileContentMap);
                inputStreamReader2 = inputStreamReader3;
                inputStreamReader3.close();
            }
            catch (XmlPullParserException | IOException ex) {
                final Object o2;
                final Object o = o2;
                final StringBuilder sb = new StringBuilder();
                sb.append("Failed to parse ");
                sb.append(file);
                Log.e("LicenseHtmlGeneratorFromXml", sb.toString(), (Throwable)o);
                if (inputStreamReader2 != null) {
                    try {
                        inputStreamReader2.close();
                    }
                    catch (IOException ex2) {
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Failed to close ");
                        sb2.append(file);
                        Log.w("LicenseHtmlGeneratorFromXml", sb2.toString());
                    }
                }
            }
        }
    }
    
    static void parse(final InputStreamReader input, final Map<String, String> map, final Map<String, String> map2) throws XmlPullParserException, IOException {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        final HashMap<String, String> hashMap2 = new HashMap<String, String>();
        final XmlPullParser pullParser = Xml.newPullParser();
        pullParser.setInput((Reader)input);
        pullParser.nextTag();
        pullParser.require(2, "", "licenses");
        for (int i = pullParser.getEventType(); i != 1; i = pullParser.next()) {
            if (i == 2) {
                if ("file-name".equals(pullParser.getName())) {
                    final String attributeValue = pullParser.getAttributeValue("", "contentId");
                    if (!TextUtils.isEmpty((CharSequence)attributeValue)) {
                        final String trim = readText(pullParser).trim();
                        if (!TextUtils.isEmpty((CharSequence)trim)) {
                            hashMap.put(trim, attributeValue);
                        }
                    }
                }
                else if ("file-content".equals(pullParser.getName())) {
                    final String attributeValue2 = pullParser.getAttributeValue("", "contentId");
                    if (!TextUtils.isEmpty((CharSequence)attributeValue2) && !map2.containsKey(attributeValue2) && !hashMap2.containsKey(attributeValue2)) {
                        final String text = readText(pullParser);
                        if (!TextUtils.isEmpty((CharSequence)text)) {
                            hashMap2.put(attributeValue2, text);
                        }
                    }
                }
            }
        }
        map.putAll(hashMap);
        map2.putAll(hashMap2);
    }
    
    private static String readText(final XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        final StringBuffer sb = new StringBuffer();
        for (int i = xmlPullParser.next(); i == 4; i = xmlPullParser.next()) {
            sb.append(xmlPullParser.getText());
        }
        return sb.toString();
    }
    
    static class ContentIdAndFileNames
    {
        final String mContentId;
        final List<String> mFileNameList;
        
        ContentIdAndFileNames(final String mContentId) {
            this.mFileNameList = new ArrayList<String>();
            this.mContentId = mContentId;
        }
    }
}
