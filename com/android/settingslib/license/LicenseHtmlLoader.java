package com.android.settingslib.license;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import android.content.Context;
import java.io.File;
import com.android.settingslib.utils.AsyncLoader;

public class LicenseHtmlLoader extends AsyncLoader<File>
{
    private static final String[] DEFAULT_LICENSE_XML_PATHS;
    private Context mContext;
    
    static {
        DEFAULT_LICENSE_XML_PATHS = new String[] { "/system/etc/NOTICE.xml.gz", "/vendor/etc/NOTICE.xml.gz", "/odm/etc/NOTICE.xml.gz", "/oem/etc/NOTICE.xml.gz" };
    }
    
    public LicenseHtmlLoader(final Context mContext) {
        super(mContext);
        this.mContext = mContext;
    }
    
    private File generateHtmlFromDefaultXmlFiles() {
        final List<File> vaildXmlFiles = this.getVaildXmlFiles();
        if (vaildXmlFiles.isEmpty()) {
            Log.e("LicenseHtmlLoader", "No notice file exists.");
            return null;
        }
        final File cachedHtmlFile = this.getCachedHtmlFile();
        if (this.isCachedHtmlFileOutdated(vaildXmlFiles, cachedHtmlFile) && !this.generateHtmlFile(vaildXmlFiles, cachedHtmlFile)) {
            return null;
        }
        return cachedHtmlFile;
    }
    
    boolean generateHtmlFile(final List<File> list, final File file) {
        return LicenseHtmlGeneratorFromXml.generateHtml(list, file);
    }
    
    File getCachedHtmlFile() {
        return new File(this.mContext.getCacheDir(), "NOTICE.html");
    }
    
    List<File> getVaildXmlFiles() {
        final ArrayList<File> list = new ArrayList<File>();
        final String[] default_LICENSE_XML_PATHS = LicenseHtmlLoader.DEFAULT_LICENSE_XML_PATHS;
        for (int length = default_LICENSE_XML_PATHS.length, i = 0; i < length; ++i) {
            final File file = new File(default_LICENSE_XML_PATHS[i]);
            if (file.exists() && file.length() != 0L) {
                list.add(file);
            }
        }
        return list;
    }
    
    boolean isCachedHtmlFileOutdated(final List<File> list, final File file) {
        boolean b = true;
        if (file.exists()) {
            b = b;
            if (file.length() != 0L) {
                final boolean b2 = false;
                final Iterator<File> iterator = list.iterator();
                while (true) {
                    b = b2;
                    if (!iterator.hasNext()) {
                        break;
                    }
                    if (file.lastModified() < iterator.next().lastModified()) {
                        b = true;
                        break;
                    }
                }
            }
        }
        return b;
    }
    
    public File loadInBackground() {
        return this.generateHtmlFromDefaultXmlFiles();
    }
    
    @Override
    protected void onDiscardResult(final File file) {
    }
}
