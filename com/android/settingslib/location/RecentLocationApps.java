package com.android.settingslib.location;

import java.util.Collections;
import java.util.Comparator;
import android.os.UserManager;
import java.util.ArrayList;
import android.app.AppOpsManager;
import java.util.List;
import android.graphics.drawable.Drawable;
import android.content.pm.ApplicationInfo;
import java.util.Iterator;
import android.content.pm.PackageManager;
import android.os.UserHandle;
import android.util.Log;
import android.app.AppOpsManager$OpEntry;
import android.app.AppOpsManager$PackageOps;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.content.Context;

public class RecentLocationApps
{
    static final String ANDROID_SYSTEM_PACKAGE_NAME = "android";
    static final int[] LOCATION_OPS;
    private static final String TAG;
    private final Context mContext;
    private final IconDrawableFactory mDrawableFactory;
    private final PackageManager mPackageManager;
    
    static {
        TAG = RecentLocationApps.class.getSimpleName();
        LOCATION_OPS = new int[] { 41, 42 };
    }
    
    public RecentLocationApps(final Context mContext) {
        this.mContext = mContext;
        this.mPackageManager = mContext.getPackageManager();
        this.mDrawableFactory = IconDrawableFactory.newInstance(mContext);
    }
    
    private Request getRequestFromOps(final long n, final AppOpsManager$PackageOps appOpsManager$PackageOps) {
        final String packageName = appOpsManager$PackageOps.getPackageName();
        final Iterator<AppOpsManager$OpEntry> iterator = (Iterator<AppOpsManager$OpEntry>)appOpsManager$PackageOps.getOps().iterator();
        boolean b = false;
        int n2 = 0;
        long n3 = 0L;
        while (iterator.hasNext()) {
            final AppOpsManager$OpEntry appOpsManager$OpEntry = iterator.next();
            boolean b2 = false;
            int n4 = 0;
            Label_0148: {
                if (!appOpsManager$OpEntry.isRunning()) {
                    b2 = b;
                    n4 = n2;
                    if (appOpsManager$OpEntry.getTime() < n - 86400000L) {
                        break Label_0148;
                    }
                }
                n3 = appOpsManager$OpEntry.getTime() + appOpsManager$OpEntry.getDuration();
                switch (appOpsManager$OpEntry.getOp()) {
                    default: {
                        b2 = b;
                        n4 = n2;
                        break;
                    }
                    case 42: {
                        b2 = true;
                        n4 = n2;
                        break;
                    }
                    case 41: {
                        n4 = 1;
                        b2 = b;
                        break;
                    }
                }
            }
            b = b2;
            n2 = n4;
        }
        if (!b && n2 == 0) {
            if (Log.isLoggable(RecentLocationApps.TAG, 2)) {
                final String tag = RecentLocationApps.TAG;
                final StringBuilder sb = new StringBuilder();
                sb.append(packageName);
                sb.append(" hadn't used location within the time interval.");
                Log.v(tag, sb.toString());
            }
            return null;
        }
        final int userId = UserHandle.getUserId(appOpsManager$PackageOps.getUid());
        final Request request = null;
        Label_0408: {
            try {
                final ApplicationInfo applicationInfoAsUser = this.mPackageManager.getApplicationInfoAsUser(packageName, 128, userId);
                if (applicationInfoAsUser == null) {
                    try {
                        final String tag2 = RecentLocationApps.TAG;
                        final StringBuilder sb2 = new StringBuilder();
                        sb2.append("Null application info retrieved for package ");
                        sb2.append(packageName);
                        sb2.append(", userId ");
                        sb2.append(userId);
                        Log.w(tag2, sb2.toString());
                        return null;
                    }
                    catch (PackageManager$NameNotFoundException ex) {
                        break Label_0408;
                    }
                }
                final UserHandle userHandle = new UserHandle(userId);
                final Drawable badgedIcon = this.mDrawableFactory.getBadgedIcon(applicationInfoAsUser, userId);
                final CharSequence applicationLabel = this.mPackageManager.getApplicationLabel(applicationInfoAsUser);
                CharSequence userBadgedLabel;
                if (applicationLabel.toString().contentEquals(userBadgedLabel = this.mPackageManager.getUserBadgedLabel(applicationLabel, userHandle))) {
                    userBadgedLabel = null;
                }
                try {
                    final Request request2 = new Request(packageName, userHandle, badgedIcon, applicationLabel, b, userBadgedLabel, n3);
                }
                catch (PackageManager$NameNotFoundException ex2) {}
            }
            catch (PackageManager$NameNotFoundException ex3) {}
        }
        final String tag3 = RecentLocationApps.TAG;
        final StringBuilder sb3 = new StringBuilder();
        sb3.append("package name not found for ");
        sb3.append(packageName);
        sb3.append(", userId ");
        sb3.append(userId);
        Log.w(tag3, sb3.toString());
        return request;
    }
    
    public List<Request> getAppList() {
        final List packagesForOps = ((AppOpsManager)this.mContext.getSystemService("appops")).getPackagesForOps(RecentLocationApps.LOCATION_OPS);
        int size;
        if (packagesForOps != null) {
            size = packagesForOps.size();
        }
        else {
            size = 0;
        }
        final ArrayList list = new ArrayList<Request>(size);
        final long currentTimeMillis = System.currentTimeMillis();
        final List userProfiles = ((UserManager)this.mContext.getSystemService("user")).getUserProfiles();
        for (int i = 0; i < size; ++i) {
            final AppOpsManager$PackageOps appOpsManager$PackageOps = packagesForOps.get(i);
            final String packageName = appOpsManager$PackageOps.getPackageName();
            final int uid = appOpsManager$PackageOps.getUid();
            final int userId = UserHandle.getUserId(uid);
            if (uid != 1000 || !"android".equals(packageName)) {
                if (userProfiles.contains(new UserHandle(userId))) {
                    final Request requestFromOps = this.getRequestFromOps(currentTimeMillis, appOpsManager$PackageOps);
                    if (requestFromOps != null) {
                        list.add(requestFromOps);
                    }
                }
            }
        }
        return (List<Request>)list;
    }
    
    public List<Request> getAppListSorted() {
        final List<Request> appList = this.getAppList();
        Collections.sort((List<Object>)appList, (Comparator<? super Object>)Collections.reverseOrder((Comparator<? super T>)new Comparator<Request>() {
            @Override
            public int compare(final Request request, final Request request2) {
                return Long.compare(request.requestFinishTime, request2.requestFinishTime);
            }
        }));
        return appList;
    }
    
    public static class Request
    {
        public final CharSequence contentDescription;
        public final Drawable icon;
        public final boolean isHighBattery;
        public final CharSequence label;
        public final String packageName;
        public final long requestFinishTime;
        public final UserHandle userHandle;
        
        private Request(final String packageName, final UserHandle userHandle, final Drawable icon, final CharSequence label, final boolean isHighBattery, final CharSequence contentDescription, final long requestFinishTime) {
            this.packageName = packageName;
            this.userHandle = userHandle;
            this.icon = icon;
            this.label = label;
            this.isHighBattery = isHighBattery;
            this.contentDescription = contentDescription;
            this.requestFinishTime = requestFinishTime;
        }
    }
}
