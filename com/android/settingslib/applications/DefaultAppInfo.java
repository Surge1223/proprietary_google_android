package com.android.settingslib.applications;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.IconDrawableFactory;
import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import android.app.AppGlobals;
import android.content.pm.ComponentInfo;
import android.os.UserHandle;
import android.content.pm.PackageItemInfo;
import com.android.settingslib.wrapper.PackageManagerWrapper;
import android.content.Context;
import android.content.ComponentName;
import com.android.settingslib.widget.CandidateInfo;

public class DefaultAppInfo extends CandidateInfo
{
    public final ComponentName componentName;
    private final Context mContext;
    protected final PackageManagerWrapper mPm;
    public final PackageItemInfo packageItemInfo;
    public final String summary;
    public final int userId;
    
    public DefaultAppInfo(final Context context, final PackageManagerWrapper packageManagerWrapper, final int n, final ComponentName componentName) {
        this(context, packageManagerWrapper, n, componentName, null, true);
    }
    
    public DefaultAppInfo(final Context mContext, final PackageManagerWrapper mPm, final int userId, final ComponentName componentName, final String summary, final boolean b) {
        super(b);
        this.mContext = mContext;
        this.mPm = mPm;
        this.packageItemInfo = null;
        this.userId = userId;
        this.componentName = componentName;
        this.summary = summary;
    }
    
    public DefaultAppInfo(final Context context, final PackageManagerWrapper packageManagerWrapper, final PackageItemInfo packageItemInfo) {
        this(context, packageManagerWrapper, packageItemInfo, null, true);
    }
    
    public DefaultAppInfo(final Context mContext, final PackageManagerWrapper mPm, final PackageItemInfo packageItemInfo, final String summary, final boolean b) {
        super(b);
        this.mContext = mContext;
        this.mPm = mPm;
        this.userId = UserHandle.myUserId();
        this.packageItemInfo = packageItemInfo;
        this.componentName = null;
        this.summary = summary;
    }
    
    private ComponentInfo getComponentInfo() {
        try {
            Object o;
            if ((o = AppGlobals.getPackageManager().getActivityInfo(this.componentName, 0, this.userId)) == null) {
                o = AppGlobals.getPackageManager().getServiceInfo(this.componentName, 0, this.userId);
            }
            return (ComponentInfo)o;
        }
        catch (RemoteException ex) {
            return null;
        }
    }
    
    @Override
    public String getKey() {
        if (this.componentName != null) {
            return this.componentName.flattenToString();
        }
        if (this.packageItemInfo != null) {
            return this.packageItemInfo.packageName;
        }
        return null;
    }
    
    @Override
    public Drawable loadIcon() {
        final IconDrawableFactory instance = IconDrawableFactory.newInstance(this.mContext);
        if (this.componentName != null) {
            try {
                final ComponentInfo componentInfo = this.getComponentInfo();
                final ApplicationInfo applicationInfoAsUser = this.mPm.getApplicationInfoAsUser(this.componentName.getPackageName(), 0, this.userId);
                if (componentInfo != null) {
                    return instance.getBadgedIcon((PackageItemInfo)componentInfo, applicationInfoAsUser, this.userId);
                }
                return instance.getBadgedIcon(applicationInfoAsUser);
            }
            catch (PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        if (this.packageItemInfo != null) {
            try {
                return instance.getBadgedIcon(this.packageItemInfo, this.mPm.getApplicationInfoAsUser(this.packageItemInfo.packageName, 0, this.userId), this.userId);
            }
            catch (PackageManager$NameNotFoundException ex2) {
                return null;
            }
        }
        return null;
    }
    
    @Override
    public CharSequence loadLabel() {
        if (this.componentName != null) {
            try {
                final ComponentInfo componentInfo = this.getComponentInfo();
                if (componentInfo != null) {
                    return componentInfo.loadLabel(this.mPm.getPackageManager());
                }
                return this.mPm.getApplicationInfoAsUser(this.componentName.getPackageName(), 0, this.userId).loadLabel(this.mPm.getPackageManager());
            }
            catch (PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        if (this.packageItemInfo != null) {
            return this.packageItemInfo.loadLabel(this.mPm.getPackageManager());
        }
        return null;
    }
}
