package com.android.settingslib.applications;

import android.os.SystemProperties;
import android.content.pm.ApplicationInfo;
import android.os.RemoteException;
import android.util.Log;
import java.util.List;
import java.util.ArrayList;
import com.android.settingslib.R;
import android.os.UserHandle;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.usb.IUsbManager;
import com.android.settingslib.applications.instantapps.InstantAppDataProvider;

public class AppUtils
{
    private static InstantAppDataProvider sInstantAppDataProvider;
    
    static {
        AppUtils.sInstantAppDataProvider = null;
    }
    
    public static CharSequence getLaunchByDefaultSummary(final ApplicationsState.AppEntry appEntry, final IUsbManager usbManager, final PackageManager packageManager, final Context context) {
        final String packageName = appEntry.info.packageName;
        final boolean hasPreferredActivities = hasPreferredActivities(packageManager, packageName);
        boolean b = true;
        final boolean b2 = hasPreferredActivities || hasUsbDefaults(usbManager, packageName);
        if (packageManager.getIntentVerificationStatusAsUser(packageName, UserHandle.myUserId()) == 0) {
            b = false;
        }
        int n;
        if (!b2 && !b) {
            n = R.string.launch_defaults_none;
        }
        else {
            n = R.string.launch_defaults_some;
        }
        return context.getString(n);
    }
    
    public static boolean hasPreferredActivities(final PackageManager packageManager, final String s) {
        final ArrayList list = new ArrayList();
        packageManager.getPreferredActivities((List)new ArrayList(), (List)list, s);
        final StringBuilder sb = new StringBuilder();
        sb.append("Have ");
        sb.append(list.size());
        sb.append(" number of activities in preferred list");
        Log.d("AppUtils", sb.toString());
        return list.size() > 0;
    }
    
    public static boolean hasUsbDefaults(final IUsbManager usbManager, final String s) {
        if (usbManager != null) {
            try {
                return usbManager.hasDefaults(s, UserHandle.myUserId());
            }
            catch (RemoteException ex) {
                Log.e("AppUtils", "mUsbManager.hasDefaults", (Throwable)ex);
            }
        }
        return false;
    }
    
    public static boolean isInstant(final ApplicationInfo applicationInfo) {
        if (AppUtils.sInstantAppDataProvider != null) {
            if (AppUtils.sInstantAppDataProvider.isInstantApp(applicationInfo)) {
                return true;
            }
        }
        else if (applicationInfo.isInstantApp()) {
            return true;
        }
        final String value = SystemProperties.get("settingsdebug.instant.packages");
        if (value != null && !value.isEmpty() && applicationInfo.packageName != null) {
            final String[] split = value.split(",");
            if (split != null) {
                for (int length = split.length, i = 0; i < length; ++i) {
                    if (applicationInfo.packageName.contains(split[i])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
