package com.android.settingslib.applications;

import java.util.Objects;
import android.arch.lifecycle.OnLifecycleEvent;
import java.util.Collections;
import android.os.Process;
import android.arch.lifecycle.LifecycleObserver;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.os.Message;
import android.content.pm.IPackageStatsObserver$Stub;
import android.os.Handler;
import android.graphics.drawable.Drawable;
import java.io.File;
import android.arch.lifecycle.Lifecycle;
import java.util.Collection;
import android.content.pm.UserInfo;
import java.util.Iterator;
import android.app.usage.StorageStats;
import java.io.IOException;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.RemoteException;
import android.text.format.Formatter;
import android.content.pm.PackageStats;
import android.app.AppGlobals;
import android.os.Looper;
import android.app.Application;
import com.android.internal.util.ArrayUtils;
import android.app.ActivityManager;
import android.os.UserHandle;
import java.text.Collator;
import android.os.UserManager;
import android.os.HandlerThread;
import android.app.usage.StorageStatsManager;
import android.content.pm.PackageManager;
import android.content.pm.IPackageManager;
import java.util.HashMap;
import android.util.SparseArray;
import android.util.IconDrawableFactory;
import java.util.UUID;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.util.List;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.Comparator;

public final class _$$Lambda$ApplicationsState$LuXUFbWTiS5lu_nO9WUp0g2nHmU implements Runnable
{
    @Override
    public final void run() {
        ApplicationsState.lambda$requestSize$0(this.f$0, this.f$1, this.f$2, this.f$3);
    }
}
