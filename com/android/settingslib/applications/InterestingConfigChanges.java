package com.android.settingslib.applications;

import android.content.res.Resources;
import android.content.res.Configuration;

public class InterestingConfigChanges
{
    private final int mFlags;
    private final Configuration mLastConfiguration;
    private int mLastDensity;
    
    public InterestingConfigChanges() {
        this(-2147482876);
    }
    
    public InterestingConfigChanges(final int mFlags) {
        this.mLastConfiguration = new Configuration();
        this.mFlags = mFlags;
    }
    
    public boolean applyNewConfig(final Resources resources) {
        final int update = this.mLastConfiguration.updateFrom(Configuration.generateDelta(this.mLastConfiguration, resources.getConfiguration()));
        if (this.mLastDensity == resources.getDisplayMetrics().densityDpi && (this.mFlags & update) == 0x0) {
            return false;
        }
        this.mLastDensity = resources.getDisplayMetrics().densityDpi;
        return true;
    }
}
