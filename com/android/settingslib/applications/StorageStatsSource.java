package com.android.settingslib.applications;

import android.app.usage.ExternalStorageStats;
import android.app.usage.StorageStats;
import android.content.pm.PackageManager;
import java.io.IOException;
import android.os.UserHandle;
import android.content.Context;
import android.app.usage.StorageStatsManager;

public class StorageStatsSource
{
    private StorageStatsManager mStorageStatsManager;
    
    public StorageStatsSource(final Context context) {
        this.mStorageStatsManager = (StorageStatsManager)context.getSystemService((Class)StorageStatsManager.class);
    }
    
    public long getCacheQuotaBytes(final String s, final int n) {
        return this.mStorageStatsManager.getCacheQuotaBytes(s, n);
    }
    
    public ExternalStorageStats getExternalStorageStats(final String s, final UserHandle userHandle) throws IOException {
        return new ExternalStorageStats(this.mStorageStatsManager.queryExternalStatsForUser(s, userHandle));
    }
    
    public AppStorageStats getStatsForPackage(final String s, final String s2, final UserHandle userHandle) throws PackageManager$NameNotFoundException, IOException {
        return (AppStorageStats)new AppStorageStatsImpl(this.mStorageStatsManager.queryStatsForPackage(s, s2, userHandle));
    }
    
    public interface AppStorageStats
    {
        long getCacheBytes();
        
        long getCodeBytes();
        
        long getDataBytes();
        
        long getTotalBytes();
    }
    
    public static class AppStorageStatsImpl implements AppStorageStats
    {
        private StorageStats mStats;
        
        public AppStorageStatsImpl(final StorageStats mStats) {
            this.mStats = mStats;
        }
        
        @Override
        public long getCacheBytes() {
            return this.mStats.getCacheBytes();
        }
        
        @Override
        public long getCodeBytes() {
            return this.mStats.getCodeBytes();
        }
        
        @Override
        public long getDataBytes() {
            return this.mStats.getDataBytes();
        }
        
        @Override
        public long getTotalBytes() {
            return this.mStats.getAppBytes() + this.mStats.getDataBytes();
        }
    }
    
    public static class ExternalStorageStats
    {
        public long appBytes;
        public long audioBytes;
        public long imageBytes;
        public long totalBytes;
        public long videoBytes;
        
        public ExternalStorageStats(final long totalBytes, final long audioBytes, final long videoBytes, final long imageBytes, final long appBytes) {
            this.totalBytes = totalBytes;
            this.audioBytes = audioBytes;
            this.videoBytes = videoBytes;
            this.imageBytes = imageBytes;
            this.appBytes = appBytes;
        }
        
        public ExternalStorageStats(final android.app.usage.ExternalStorageStats externalStorageStats) {
            this.totalBytes = externalStorageStats.getTotalBytes();
            this.audioBytes = externalStorageStats.getAudioBytes();
            this.videoBytes = externalStorageStats.getVideoBytes();
            this.imageBytes = externalStorageStats.getImageBytes();
            this.appBytes = externalStorageStats.getAppBytes();
        }
    }
}
