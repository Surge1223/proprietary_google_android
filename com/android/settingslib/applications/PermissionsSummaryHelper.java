package com.android.settingslib.applications;

import android.os.Handler;
import java.util.Comparator;
import java.util.Collections;
import java.text.Collator;
import java.util.ArrayList;
import android.content.pm.permission.RuntimePermissionPresentationInfo;
import java.util.List;
import android.content.pm.permission.RuntimePermissionPresenter$OnResultCallback;
import android.content.pm.permission.RuntimePermissionPresenter;
import android.content.Context;

public class PermissionsSummaryHelper
{
    public static void getPermissionSummary(final Context context, final String s, final PermissionsResultCallback permissionsResultCallback) {
        RuntimePermissionPresenter.getInstance(context).getAppPermissions(s, (RuntimePermissionPresenter$OnResultCallback)new RuntimePermissionPresenter$OnResultCallback() {
            public void onGetAppPermissions(final List<RuntimePermissionPresentationInfo> list) {
                final int size = list.size();
                int n = 0;
                final ArrayList<Object> list2 = new ArrayList<Object>();
                int n2 = 0;
                int n3 = 0;
                int n4;
                int n5;
                int n6;
                for (int i = 0; i < size; ++i, n3 = n5, n = n4, n2 = n6) {
                    final RuntimePermissionPresentationInfo runtimePermissionPresentationInfo = list.get(i);
                    n4 = n + 1;
                    n5 = n3;
                    n6 = n2;
                    if (runtimePermissionPresentationInfo.isGranted()) {
                        if (runtimePermissionPresentationInfo.isStandard()) {
                            list2.add(runtimePermissionPresentationInfo.getLabel());
                            n5 = n3 + 1;
                            n6 = n2;
                        }
                        else {
                            n6 = n2 + 1;
                            n5 = n3;
                        }
                    }
                }
                final Collator instance = Collator.getInstance();
                instance.setStrength(0);
                Collections.sort(list2, instance);
                permissionsResultCallback.onPermissionSummaryResult(n3, n, n2, (List<CharSequence>)list2);
            }
        }, (Handler)null);
    }
    
    public abstract static class PermissionsResultCallback
    {
        public void onPermissionSummaryResult(final int n, final int n2, final int n3, final List<CharSequence> list) {
        }
    }
}
