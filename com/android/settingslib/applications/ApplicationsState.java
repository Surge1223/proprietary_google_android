package com.android.settingslib.applications;

import java.util.Objects;
import android.arch.lifecycle.OnLifecycleEvent;
import java.util.Collections;
import android.os.Process;
import android.arch.lifecycle.LifecycleObserver;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.os.Message;
import android.content.pm.IPackageStatsObserver$Stub;
import android.os.Handler;
import android.graphics.drawable.Drawable;
import java.io.File;
import android.arch.lifecycle.Lifecycle;
import java.util.Collection;
import android.content.pm.UserInfo;
import java.util.Iterator;
import android.app.usage.StorageStats;
import java.io.IOException;
import android.content.pm.PackageManager;
import android.util.Log;
import android.os.RemoteException;
import android.text.format.Formatter;
import android.content.pm.PackageStats;
import android.app.AppGlobals;
import android.os.Looper;
import android.app.Application;
import com.android.internal.util.ArrayUtils;
import android.app.ActivityManager;
import android.os.UserHandle;
import java.text.Collator;
import android.os.UserManager;
import android.os.HandlerThread;
import android.app.usage.StorageStatsManager;
import android.content.pm.PackageManager;
import android.content.pm.IPackageManager;
import java.util.HashMap;
import android.util.SparseArray;
import android.util.IconDrawableFactory;
import java.util.UUID;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import java.util.List;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.Comparator;

public class ApplicationsState
{
    public static final Comparator<AppEntry> ALPHA_COMPARATOR;
    public static final Comparator<AppEntry> EXTERNAL_SIZE_COMPARATOR;
    public static final AppFilter FILTER_ALL_ENABLED;
    public static final AppFilter FILTER_AUDIO;
    public static final AppFilter FILTER_DISABLED;
    public static final AppFilter FILTER_DOWNLOADED_AND_LAUNCHER;
    public static final AppFilter FILTER_DOWNLOADED_AND_LAUNCHER_AND_INSTANT;
    public static final AppFilter FILTER_EVERYTHING;
    public static final AppFilter FILTER_GAMES;
    public static final AppFilter FILTER_INSTANT;
    public static final AppFilter FILTER_MOVIES;
    public static final AppFilter FILTER_NOT_HIDE;
    public static final AppFilter FILTER_OTHER_APPS;
    public static final AppFilter FILTER_PERSONAL;
    public static final AppFilter FILTER_PHOTOS;
    public static final AppFilter FILTER_THIRD_PARTY;
    public static final AppFilter FILTER_WITHOUT_DISABLED_UNTIL_USED;
    public static final AppFilter FILTER_WITH_DOMAIN_URLS;
    public static final AppFilter FILTER_WORK;
    public static final Comparator<AppEntry> INTERNAL_SIZE_COMPARATOR;
    static final Pattern REMOVE_DIACRITICALS_PATTERN;
    public static final Comparator<AppEntry> SIZE_COMPARATOR;
    static ApplicationsState sInstance;
    static final Object sLock;
    final ArrayList<WeakReference<Session>> mActiveSessions;
    final int mAdminRetrieveFlags;
    final ArrayList<AppEntry> mAppEntries;
    List<ApplicationInfo> mApplications;
    final BackgroundHandler mBackgroundHandler;
    final Context mContext;
    String mCurComputingSizePkg;
    int mCurComputingSizeUserId;
    UUID mCurComputingSizeUuid;
    long mCurId;
    final IconDrawableFactory mDrawableFactory;
    final SparseArray<HashMap<String, AppEntry>> mEntriesMap;
    boolean mHaveDisabledApps;
    boolean mHaveInstantApps;
    final InterestingConfigChanges mInterestingConfigChanges;
    final IPackageManager mIpm;
    final MainHandler mMainHandler;
    PackageIntentReceiver mPackageIntentReceiver;
    final PackageManager mPm;
    final ArrayList<Session> mRebuildingSessions;
    boolean mResumed;
    final int mRetrieveFlags;
    final ArrayList<Session> mSessions;
    boolean mSessionsChanged;
    final StorageStatsManager mStats;
    final HandlerThread mThread;
    final UserManager mUm;
    
    static {
        REMOVE_DIACRITICALS_PATTERN = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        sLock = new Object();
        ALPHA_COMPARATOR = new Comparator<AppEntry>() {
            private final Collator sCollator = Collator.getInstance();
            
            @Override
            public int compare(final AppEntry appEntry, final AppEntry appEntry2) {
                final int compare = this.sCollator.compare(appEntry.label, appEntry2.label);
                if (compare != 0) {
                    return compare;
                }
                if (appEntry.info != null && appEntry2.info != null) {
                    final int compare2 = this.sCollator.compare(appEntry.info.packageName, appEntry2.info.packageName);
                    if (compare2 != 0) {
                        return compare2;
                    }
                }
                return appEntry.info.uid - appEntry2.info.uid;
            }
        };
        SIZE_COMPARATOR = new Comparator<AppEntry>() {
            @Override
            public int compare(final AppEntry appEntry, final AppEntry appEntry2) {
                if (appEntry.size < appEntry2.size) {
                    return 1;
                }
                if (appEntry.size > appEntry2.size) {
                    return -1;
                }
                return ApplicationsState.ALPHA_COMPARATOR.compare(appEntry, appEntry2);
            }
        };
        INTERNAL_SIZE_COMPARATOR = new Comparator<AppEntry>() {
            @Override
            public int compare(final AppEntry appEntry, final AppEntry appEntry2) {
                if (appEntry.internalSize < appEntry2.internalSize) {
                    return 1;
                }
                if (appEntry.internalSize > appEntry2.internalSize) {
                    return -1;
                }
                return ApplicationsState.ALPHA_COMPARATOR.compare(appEntry, appEntry2);
            }
        };
        EXTERNAL_SIZE_COMPARATOR = new Comparator<AppEntry>() {
            @Override
            public int compare(final AppEntry appEntry, final AppEntry appEntry2) {
                if (appEntry.externalSize < appEntry2.externalSize) {
                    return 1;
                }
                if (appEntry.externalSize > appEntry2.externalSize) {
                    return -1;
                }
                return ApplicationsState.ALPHA_COMPARATOR.compare(appEntry, appEntry2);
            }
        };
        FILTER_PERSONAL = (AppFilter)new AppFilter() {
            private int mCurrentUser;
            
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return UserHandle.getUserId(appEntry.info.uid) == this.mCurrentUser;
            }
            
            @Override
            public void init() {
                this.mCurrentUser = ActivityManager.getCurrentUser();
            }
        };
        FILTER_WITHOUT_DISABLED_UNTIL_USED = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.info.enabledSetting != 4;
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_WORK = (AppFilter)new AppFilter() {
            private int mCurrentUser;
            
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return UserHandle.getUserId(appEntry.info.uid) != this.mCurrentUser;
            }
            
            @Override
            public void init() {
                this.mCurrentUser = ActivityManager.getCurrentUser();
            }
        };
        FILTER_DOWNLOADED_AND_LAUNCHER = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return !AppUtils.isInstant(appEntry.info) && (hasFlag(appEntry.info.flags, 128) || !hasFlag(appEntry.info.flags, 1) || appEntry.hasLauncherEntry || (hasFlag(appEntry.info.flags, 1) && appEntry.isHomeApp));
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_DOWNLOADED_AND_LAUNCHER_AND_INSTANT = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return AppUtils.isInstant(appEntry.info) || ApplicationsState.FILTER_DOWNLOADED_AND_LAUNCHER.filterApp(appEntry);
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_THIRD_PARTY = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return hasFlag(appEntry.info.flags, 128) || !hasFlag(appEntry.info.flags, 1);
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_DISABLED = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return !appEntry.info.enabled && !AppUtils.isInstant(appEntry.info);
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_INSTANT = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return AppUtils.isInstant(appEntry.info);
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_ALL_ENABLED = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return appEntry.info.enabled && !AppUtils.isInstant(appEntry.info);
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_EVERYTHING = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return true;
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_WITH_DOMAIN_URLS = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                return !AppUtils.isInstant(appEntry.info) && hasFlag(appEntry.info.privateFlags, 16);
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_NOT_HIDE = (AppFilter)new AppFilter() {
            private String[] mHidePackageNames;
            
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                if (ArrayUtils.contains((Object[])this.mHidePackageNames, (Object)appEntry.info.packageName)) {
                    if (!appEntry.info.enabled) {
                        return false;
                    }
                    if (appEntry.info.enabledSetting == 4) {
                        return false;
                    }
                }
                return true;
            }
            
            @Override
            public void init() {
            }
            
            @Override
            public void init(final Context context) {
                this.mHidePackageNames = context.getResources().getStringArray(17236014);
            }
        };
        FILTER_GAMES = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                synchronized (appEntry.info) {
                    return hasFlag(appEntry.info.flags, 33554432) || appEntry.info.category == 0;
                }
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_AUDIO = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                synchronized (appEntry) {
                    final int category = appEntry.info.category;
                    boolean b = true;
                    if (category != 1) {
                        b = false;
                    }
                    return b;
                }
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_MOVIES = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                synchronized (appEntry) {
                    return appEntry.info.category == 2;
                }
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_PHOTOS = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                synchronized (appEntry) {
                    return appEntry.info.category == 3;
                }
            }
            
            @Override
            public void init() {
            }
        };
        FILTER_OTHER_APPS = (AppFilter)new AppFilter() {
            @Override
            public boolean filterApp(final AppEntry appEntry) {
                synchronized (appEntry) {
                    final boolean filterApp = ApplicationsState.FILTER_AUDIO.filterApp(appEntry);
                    boolean b = true;
                    final boolean b2 = filterApp || ApplicationsState.FILTER_GAMES.filterApp(appEntry) || ApplicationsState.FILTER_MOVIES.filterApp(appEntry) || ApplicationsState.FILTER_PHOTOS.filterApp(appEntry);
                    // monitorexit(appEntry)
                    if (b2) {
                        b = false;
                    }
                    return b;
                }
            }
            
            @Override
            public void init() {
            }
        };
    }
    
    private ApplicationsState(Application mEntriesMap) {
        this.mSessions = new ArrayList<Session>();
        this.mRebuildingSessions = new ArrayList<Session>();
        this.mInterestingConfigChanges = new InterestingConfigChanges();
        this.mEntriesMap = (SparseArray<HashMap<String, AppEntry>>)new SparseArray();
        this.mAppEntries = new ArrayList<AppEntry>();
        this.mApplications = new ArrayList<ApplicationInfo>();
        this.mCurId = 1L;
        this.mActiveSessions = new ArrayList<WeakReference<Session>>();
        this.mMainHandler = new MainHandler(Looper.getMainLooper());
        this.mContext = (Context)mEntriesMap;
        this.mPm = this.mContext.getPackageManager();
        this.mDrawableFactory = IconDrawableFactory.newInstance(this.mContext);
        this.mIpm = AppGlobals.getPackageManager();
        this.mUm = (UserManager)this.mContext.getSystemService((Class)UserManager.class);
        this.mStats = (StorageStatsManager)this.mContext.getSystemService((Class)StorageStatsManager.class);
        final int[] profileIdsWithDisabled = this.mUm.getProfileIdsWithDisabled(UserHandle.myUserId());
        for (int length = profileIdsWithDisabled.length, i = 0; i < length; ++i) {
            this.mEntriesMap.put(profileIdsWithDisabled[i], (Object)new HashMap());
        }
        (this.mThread = new HandlerThread("ApplicationsState.Loader", 10)).start();
        this.mBackgroundHandler = new BackgroundHandler(this.mThread.getLooper());
        this.mAdminRetrieveFlags = 4227584;
        this.mRetrieveFlags = 33280;
        mEntriesMap = (Application)this.mEntriesMap;
        // monitorenter(mEntriesMap)
        try {
            try {
                this.mEntriesMap.wait(1L);
            }
            finally {
            }
            // monitorexit(mEntriesMap)
            // monitorexit(mEntriesMap)
        }
        catch (InterruptedException ex) {}
    }
    
    private void addUser(final int n) {
        if (ArrayUtils.contains(this.mUm.getProfileIdsWithDisabled(UserHandle.myUserId()), n)) {
            synchronized (this.mEntriesMap) {
                this.mEntriesMap.put(n, (Object)new HashMap());
                if (this.mResumed) {
                    this.doPauseLocked();
                    this.doResumeIfNeededLocked();
                }
                if (!this.mMainHandler.hasMessages(2)) {
                    this.mMainHandler.sendEmptyMessage(2);
                }
            }
        }
    }
    
    private ApplicationInfo getAppInfoLocked(final String s, final int n) {
        for (int i = 0; i < this.mApplications.size(); ++i) {
            final ApplicationInfo applicationInfo = this.mApplications.get(i);
            if (s.equals(applicationInfo.packageName) && n == UserHandle.getUserId(applicationInfo.uid)) {
                return applicationInfo;
            }
        }
        return null;
    }
    
    private AppEntry getEntryLocked(final ApplicationInfo info) {
        final int userId = UserHandle.getUserId(info.uid);
        final AppEntry appEntry = ((HashMap)this.mEntriesMap.get(userId)).get(info.packageName);
        AppEntry appEntry2;
        if (appEntry == null) {
            final Context mContext = this.mContext;
            final long mCurId = this.mCurId;
            this.mCurId = 1L + mCurId;
            appEntry2 = new AppEntry(mContext, info, mCurId);
            ((HashMap)this.mEntriesMap.get(userId)).put(info.packageName, appEntry2);
            this.mAppEntries.add(appEntry2);
        }
        else {
            appEntry2 = appEntry;
            if (appEntry.info != info) {
                appEntry.info = info;
                appEntry2 = appEntry;
            }
        }
        return appEntry2;
    }
    
    public static ApplicationsState getInstance(final Application application) {
        synchronized (ApplicationsState.sLock) {
            if (ApplicationsState.sInstance == null) {
                ApplicationsState.sInstance = new ApplicationsState(application);
            }
            return ApplicationsState.sInstance;
        }
    }
    
    private String getSizeStr(final long n) {
        if (n >= 0L) {
            return Formatter.formatFileSize(this.mContext, n);
        }
        return null;
    }
    
    private long getTotalExternalSize(final PackageStats packageStats) {
        if (packageStats != null) {
            return packageStats.externalCodeSize + packageStats.externalDataSize + packageStats.externalCacheSize + packageStats.externalMediaSize + packageStats.externalObbSize;
        }
        return -2L;
    }
    
    private long getTotalInternalSize(final PackageStats packageStats) {
        if (packageStats != null) {
            return packageStats.codeSize + packageStats.dataSize;
        }
        return -2L;
    }
    
    private static boolean hasFlag(final int n, final int n2) {
        return (n & n2) != 0x0;
    }
    
    private void removeUser(final int n) {
        synchronized (this.mEntriesMap) {
            final HashMap hashMap = (HashMap)this.mEntriesMap.get(n);
            if (hashMap != null) {
                for (final AppEntry appEntry : hashMap.values()) {
                    this.mAppEntries.remove(appEntry);
                    this.mApplications.remove(appEntry.info);
                }
                this.mEntriesMap.remove(n);
                if (!this.mMainHandler.hasMessages(2)) {
                    this.mMainHandler.sendEmptyMessage(2);
                }
            }
        }
    }
    
    void addPackage(final String s, final int n) {
        try {
            synchronized (this.mEntriesMap) {
                if (!this.mResumed) {
                    return;
                }
                if (this.indexOfApplicationInfoLocked(s, n) >= 0) {
                    return;
                }
                final IPackageManager mIpm = this.mIpm;
                int n2;
                if (this.mUm.isUserAdmin(n)) {
                    n2 = this.mAdminRetrieveFlags;
                }
                else {
                    n2 = this.mRetrieveFlags;
                }
                final ApplicationInfo applicationInfo = mIpm.getApplicationInfo(s, n2, n);
                if (applicationInfo == null) {
                    return;
                }
                if (!applicationInfo.enabled) {
                    if (applicationInfo.enabledSetting != 3) {
                        return;
                    }
                    this.mHaveDisabledApps = true;
                }
                if (AppUtils.isInstant(applicationInfo)) {
                    this.mHaveInstantApps = true;
                }
                this.mApplications.add(applicationInfo);
                if (!this.mBackgroundHandler.hasMessages(2)) {
                    this.mBackgroundHandler.sendEmptyMessage(2);
                }
                if (!this.mMainHandler.hasMessages(2)) {
                    this.mMainHandler.sendEmptyMessage(2);
                }
            }
        }
        catch (RemoteException ex) {}
    }
    
    void clearEntries() {
        for (int i = 0; i < this.mEntriesMap.size(); ++i) {
            ((HashMap)this.mEntriesMap.valueAt(i)).clear();
        }
        this.mAppEntries.clear();
    }
    
    void doPauseIfNeededLocked() {
        if (!this.mResumed) {
            return;
        }
        for (int i = 0; i < this.mSessions.size(); ++i) {
            if (this.mSessions.get(i).mResumed) {
                return;
            }
        }
        this.doPauseLocked();
    }
    
    void doPauseLocked() {
        this.mResumed = false;
        if (this.mPackageIntentReceiver != null) {
            this.mPackageIntentReceiver.unregisterReceiver();
            this.mPackageIntentReceiver = null;
        }
    }
    
    void doResumeIfNeededLocked() {
        if (this.mResumed) {
            return;
        }
        this.mResumed = true;
        if (this.mPackageIntentReceiver == null) {
            (this.mPackageIntentReceiver = new PackageIntentReceiver()).registerReceiver();
        }
        this.mApplications = new ArrayList<ApplicationInfo>();
        for (final UserInfo userInfo : this.mUm.getProfiles(UserHandle.myUserId())) {
            try {
                if (this.mEntriesMap.indexOfKey(userInfo.id) < 0) {
                    this.mEntriesMap.put(userInfo.id, (Object)new HashMap());
                }
                final IPackageManager mIpm = this.mIpm;
                int n;
                if (userInfo.isAdmin()) {
                    n = this.mAdminRetrieveFlags;
                }
                else {
                    n = this.mRetrieveFlags;
                }
                this.mApplications.addAll(mIpm.getInstalledApplications(n, userInfo.id).getList());
            }
            catch (RemoteException ex) {}
        }
        final boolean applyNewConfig = this.mInterestingConfigChanges.applyNewConfig(this.mContext.getResources());
        final int n2 = 0;
        if (applyNewConfig) {
            this.clearEntries();
        }
        else {
            for (int i = 0; i < this.mAppEntries.size(); ++i) {
                this.mAppEntries.get(i).sizeStale = true;
            }
        }
        this.mHaveDisabledApps = false;
        this.mHaveInstantApps = false;
        int n3;
        for (int j = n2; j < this.mApplications.size(); j = n3 + 1) {
            final ApplicationInfo info = this.mApplications.get(j);
            if (!info.enabled) {
                if (info.enabledSetting != 3) {
                    this.mApplications.remove(j);
                    n3 = j - 1;
                    continue;
                }
                this.mHaveDisabledApps = true;
            }
            if (!this.mHaveInstantApps && AppUtils.isInstant(info)) {
                this.mHaveInstantApps = true;
            }
            final AppEntry appEntry = ((HashMap)this.mEntriesMap.get(UserHandle.getUserId(info.uid))).get(info.packageName);
            n3 = j;
            if (appEntry != null) {
                appEntry.info = info;
                n3 = j;
            }
        }
        if (this.mAppEntries.size() > this.mApplications.size()) {
            this.clearEntries();
        }
        this.mCurComputingSizePkg = null;
        if (!this.mBackgroundHandler.hasMessages(2)) {
            this.mBackgroundHandler.sendEmptyMessage(2);
        }
    }
    
    public void ensureIcon(final AppEntry appEntry) {
        if (appEntry.icon != null) {
            return;
        }
        synchronized (appEntry) {
            appEntry.ensureIconLocked(this.mContext, this.mDrawableFactory);
        }
    }
    
    public Looper getBackgroundLooper() {
        return this.mThread.getLooper();
    }
    
    public AppEntry getEntry(final String s, final int n) {
        synchronized (this.mEntriesMap) {
            AppEntry entryLocked;
            final AppEntry appEntry = entryLocked = ((HashMap)this.mEntriesMap.get(n)).get(s);
            if (appEntry == null) {
                ApplicationInfo applicationInfo;
                if ((applicationInfo = this.getAppInfoLocked(s, n)) == null) {
                    try {
                        applicationInfo = this.mIpm.getApplicationInfo(s, 0, n);
                    }
                    catch (RemoteException ex) {
                        Log.w("ApplicationsState", "getEntry couldn't reach PackageManager", (Throwable)ex);
                        return null;
                    }
                }
                entryLocked = appEntry;
                if (applicationInfo != null) {
                    entryLocked = this.getEntryLocked(applicationInfo);
                }
            }
            return entryLocked;
        }
    }
    
    public boolean haveDisabledApps() {
        return this.mHaveDisabledApps;
    }
    
    public boolean haveInstantApps() {
        return this.mHaveInstantApps;
    }
    
    int indexOfApplicationInfoLocked(final String s, final int n) {
        for (int i = this.mApplications.size() - 1; i >= 0; --i) {
            final ApplicationInfo applicationInfo = this.mApplications.get(i);
            if (applicationInfo.packageName.equals(s) && UserHandle.getUserId(applicationInfo.uid) == n) {
                return i;
            }
        }
        return -1;
    }
    
    public void invalidatePackage(final String s, final int n) {
        this.removePackage(s, n);
        this.addPackage(s, n);
    }
    
    public Session newSession(final Callbacks callbacks) {
        return this.newSession(callbacks, null);
    }
    
    public Session newSession(final Callbacks callbacks, final Lifecycle lifecycle) {
        final Session session = new Session(callbacks, lifecycle);
        synchronized (this.mEntriesMap) {
            this.mSessions.add(session);
            return session;
        }
    }
    
    void rebuildActiveSessions() {
        synchronized (this.mEntriesMap) {
            if (!this.mSessionsChanged) {
                return;
            }
            this.mActiveSessions.clear();
            for (int i = 0; i < this.mSessions.size(); ++i) {
                final Session session = this.mSessions.get(i);
                if (session.mResumed) {
                    this.mActiveSessions.add(new WeakReference<Session>(session));
                }
            }
        }
    }
    
    public void removePackage(final String s, final int n) {
        synchronized (this.mEntriesMap) {
            final int indexOfApplicationInfoLocked = this.indexOfApplicationInfoLocked(s, n);
            if (indexOfApplicationInfoLocked >= 0) {
                final AppEntry appEntry = ((HashMap)this.mEntriesMap.get(n)).get(s);
                if (appEntry != null) {
                    ((HashMap)this.mEntriesMap.get(n)).remove(s);
                    this.mAppEntries.remove(appEntry);
                }
                final ApplicationInfo applicationInfo = this.mApplications.get(indexOfApplicationInfoLocked);
                this.mApplications.remove(indexOfApplicationInfoLocked);
                if (!applicationInfo.enabled) {
                    this.mHaveDisabledApps = false;
                    final Iterator<ApplicationInfo> iterator = this.mApplications.iterator();
                    while (iterator.hasNext()) {
                        if (!iterator.next().enabled) {
                            this.mHaveDisabledApps = true;
                            break;
                        }
                    }
                }
                if (AppUtils.isInstant(applicationInfo)) {
                    this.mHaveInstantApps = false;
                    final Iterator<ApplicationInfo> iterator2 = this.mApplications.iterator();
                    while (iterator2.hasNext()) {
                        if (AppUtils.isInstant(iterator2.next())) {
                            this.mHaveInstantApps = true;
                            break;
                        }
                    }
                }
                if (!this.mMainHandler.hasMessages(2)) {
                    this.mMainHandler.sendEmptyMessage(2);
                }
            }
        }
    }
    
    public void requestSize(final String s, final int n) {
        synchronized (this.mEntriesMap) {
            final AppEntry appEntry = ((HashMap)this.mEntriesMap.get(n)).get(s);
            if (appEntry != null && hasFlag(appEntry.info.flags, 8388608)) {
                this.mBackgroundHandler.post((Runnable)new _$$Lambda$ApplicationsState$LuXUFbWTiS5lu_nO9WUp0g2nHmU(this, appEntry, s, n));
            }
        }
    }
    
    public static class AppEntry extends SizeInfo
    {
        public final File apkFile;
        public long externalSize;
        public String externalSizeStr;
        public Object extraInfo;
        public boolean hasLauncherEntry;
        public Drawable icon;
        public final long id;
        public ApplicationInfo info;
        public long internalSize;
        public String internalSizeStr;
        public boolean isHomeApp;
        public String label;
        public boolean launcherEntryEnabled;
        public boolean mounted;
        public long size;
        public long sizeLoadStart;
        public boolean sizeStale;
        public String sizeStr;
        
        public AppEntry(final Context context, final ApplicationInfo info, final long id) {
            this.apkFile = new File(info.sourceDir);
            this.id = id;
            this.info = info;
            this.size = -1L;
            this.sizeStale = true;
            this.ensureLabel(context);
        }
        
        boolean ensureIconLocked(final Context context, final IconDrawableFactory iconDrawableFactory) {
            if (this.icon == null) {
                if (this.apkFile.exists()) {
                    this.icon = iconDrawableFactory.getBadgedIcon(this.info);
                    return true;
                }
                this.mounted = false;
                this.icon = context.getDrawable(17303559);
            }
            else if (!this.mounted && this.apkFile.exists()) {
                this.mounted = true;
                this.icon = iconDrawableFactory.getBadgedIcon(this.info);
                return true;
            }
            return false;
        }
        
        public void ensureLabel(final Context context) {
            if (this.label == null || !this.mounted) {
                if (!this.apkFile.exists()) {
                    this.mounted = false;
                    this.label = this.info.packageName;
                }
                else {
                    this.mounted = true;
                    final CharSequence loadLabel = this.info.loadLabel(context.getPackageManager());
                    String label;
                    if (loadLabel != null) {
                        label = loadLabel.toString();
                    }
                    else {
                        label = this.info.packageName;
                    }
                    this.label = label;
                }
            }
        }
    }
    
    public interface AppFilter
    {
        boolean filterApp(final AppEntry p0);
        
        void init();
        
        default void init(final Context context) {
            this.init();
        }
    }
    
    private class BackgroundHandler extends Handler
    {
        boolean mRunning;
        final IPackageStatsObserver$Stub mStatsObserver;
        
        BackgroundHandler(final Looper looper) {
            super(looper);
            this.mStatsObserver = new IPackageStatsObserver$Stub() {
                public void onGetStatsCompleted(final PackageStats p0, final boolean p1) {
                    // 
                    // This method could not be decompiled.
                    // 
                    // Original Bytecode:
                    // 
                    //     1: ifne            5
                    //     4: return         
                    //     5: aload_0        
                    //     6: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //     9: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //    12: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
                    //    15: astore_3       
                    //    16: aload_3        
                    //    17: monitorenter   
                    //    18: aload_0        
                    //    19: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //    22: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //    25: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
                    //    28: aload_1        
                    //    29: getfield        android/content/pm/PackageStats.userHandle:I
                    //    32: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
                    //    35: checkcast       Ljava/util/HashMap;
                    //    38: astore          4
                    //    40: aload           4
                    //    42: ifnonnull       48
                    //    45: aload_3        
                    //    46: monitorexit    
                    //    47: return         
                    //    48: aload           4
                    //    50: aload_1        
                    //    51: getfield        android/content/pm/PackageStats.packageName:Ljava/lang/String;
                    //    54: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
                    //    57: checkcast       Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
                    //    60: astore          4
                    //    62: aload           4
                    //    64: ifnull          434
                    //    67: aload           4
                    //    69: monitorenter   
                    //    70: aload           4
                    //    72: iconst_0       
                    //    73: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeStale:Z
                    //    76: aload           4
                    //    78: lconst_0       
                    //    79: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeLoadStart:J
                    //    82: aload_1        
                    //    83: getfield        android/content/pm/PackageStats.externalCodeSize:J
                    //    86: aload_1        
                    //    87: getfield        android/content/pm/PackageStats.externalObbSize:J
                    //    90: ladd           
                    //    91: lstore          5
                    //    93: aload_1        
                    //    94: getfield        android/content/pm/PackageStats.externalDataSize:J
                    //    97: aload_1        
                    //    98: getfield        android/content/pm/PackageStats.externalMediaSize:J
                    //   101: ladd           
                    //   102: lstore          7
                    //   104: lload           5
                    //   106: lload           7
                    //   108: ladd           
                    //   109: aload_0        
                    //   110: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   113: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   116: aload_1        
                    //   117: invokestatic    com/android/settingslib/applications/ApplicationsState.access$400:(Lcom/android/settingslib/applications/ApplicationsState;Landroid/content/pm/PackageStats;)J
                    //   120: ladd           
                    //   121: lstore          9
                    //   123: aload           4
                    //   125: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.size:J
                    //   128: lload           9
                    //   130: lcmp           
                    //   131: ifne            221
                    //   134: aload           4
                    //   136: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.cacheSize:J
                    //   139: lstore          11
                    //   141: lload           11
                    //   143: aload_1        
                    //   144: getfield        android/content/pm/PackageStats.cacheSize:J
                    //   147: lcmp           
                    //   148: ifne            221
                    //   151: aload           4
                    //   153: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.codeSize:J
                    //   156: aload_1        
                    //   157: getfield        android/content/pm/PackageStats.codeSize:J
                    //   160: lcmp           
                    //   161: ifne            221
                    //   164: aload           4
                    //   166: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.dataSize:J
                    //   169: aload_1        
                    //   170: getfield        android/content/pm/PackageStats.dataSize:J
                    //   173: lcmp           
                    //   174: ifne            221
                    //   177: aload           4
                    //   179: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalCodeSize:J
                    //   182: lload           5
                    //   184: lcmp           
                    //   185: ifne            221
                    //   188: aload           4
                    //   190: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalDataSize:J
                    //   193: lload           7
                    //   195: lcmp           
                    //   196: ifne            221
                    //   199: aload           4
                    //   201: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalCacheSize:J
                    //   204: aload_1        
                    //   205: getfield        android/content/pm/PackageStats.externalCacheSize:J
                    //   208: lcmp           
                    //   209: ifeq            215
                    //   212: goto            221
                    //   215: iconst_0       
                    //   216: istore          13
                    //   218: goto            373
                    //   221: aload           4
                    //   223: lload           9
                    //   225: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.size:J
                    //   228: aload           4
                    //   230: aload_1        
                    //   231: getfield        android/content/pm/PackageStats.cacheSize:J
                    //   234: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.cacheSize:J
                    //   237: aload           4
                    //   239: aload_1        
                    //   240: getfield        android/content/pm/PackageStats.codeSize:J
                    //   243: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.codeSize:J
                    //   246: aload           4
                    //   248: aload_1        
                    //   249: getfield        android/content/pm/PackageStats.dataSize:J
                    //   252: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.dataSize:J
                    //   255: aload           4
                    //   257: lload           5
                    //   259: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalCodeSize:J
                    //   262: aload           4
                    //   264: lload           7
                    //   266: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalDataSize:J
                    //   269: aload           4
                    //   271: aload_1        
                    //   272: getfield        android/content/pm/PackageStats.externalCacheSize:J
                    //   275: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalCacheSize:J
                    //   278: aload           4
                    //   280: aload_0        
                    //   281: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   284: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   287: aload           4
                    //   289: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.size:J
                    //   292: invokestatic    com/android/settingslib/applications/ApplicationsState.access$500:(Lcom/android/settingslib/applications/ApplicationsState;J)Ljava/lang/String;
                    //   295: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeStr:Ljava/lang/String;
                    //   298: aload           4
                    //   300: aload_0        
                    //   301: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   304: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   307: aload_1        
                    //   308: invokestatic    com/android/settingslib/applications/ApplicationsState.access$400:(Lcom/android/settingslib/applications/ApplicationsState;Landroid/content/pm/PackageStats;)J
                    //   311: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.internalSize:J
                    //   314: aload           4
                    //   316: aload_0        
                    //   317: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   320: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   323: aload           4
                    //   325: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.internalSize:J
                    //   328: invokestatic    com/android/settingslib/applications/ApplicationsState.access$500:(Lcom/android/settingslib/applications/ApplicationsState;J)Ljava/lang/String;
                    //   331: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.internalSizeStr:Ljava/lang/String;
                    //   334: aload           4
                    //   336: aload_0        
                    //   337: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   340: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   343: aload_1        
                    //   344: invokestatic    com/android/settingslib/applications/ApplicationsState.access$600:(Lcom/android/settingslib/applications/ApplicationsState;Landroid/content/pm/PackageStats;)J
                    //   347: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalSize:J
                    //   350: aload           4
                    //   352: aload_0        
                    //   353: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   356: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   359: aload           4
                    //   361: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalSize:J
                    //   364: invokestatic    com/android/settingslib/applications/ApplicationsState.access$500:(Lcom/android/settingslib/applications/ApplicationsState;J)Ljava/lang/String;
                    //   367: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.externalSizeStr:Ljava/lang/String;
                    //   370: iconst_1       
                    //   371: istore          13
                    //   373: aload           4
                    //   375: monitorexit    
                    //   376: iload           13
                    //   378: ifeq            434
                    //   381: aload_0        
                    //   382: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   385: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   388: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
                    //   391: iconst_4       
                    //   392: aload_1        
                    //   393: getfield        android/content/pm/PackageStats.packageName:Ljava/lang/String;
                    //   396: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
                    //   399: astore          4
                    //   401: aload_0        
                    //   402: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   405: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   408: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
                    //   411: aload           4
                    //   413: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendMessage:(Landroid/os/Message;)Z
                    //   416: pop            
                    //   417: goto            434
                    //   420: astore_1       
                    //   421: goto            425
                    //   424: astore_1       
                    //   425: aload           4
                    //   427: monitorexit    
                    //   428: aload_1        
                    //   429: athrow         
                    //   430: astore_1       
                    //   431: goto            425
                    //   434: aload_0        
                    //   435: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   438: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   441: getfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizePkg:Ljava/lang/String;
                    //   444: ifnull          505
                    //   447: aload_0        
                    //   448: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   451: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   454: getfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizePkg:Ljava/lang/String;
                    //   457: aload_1        
                    //   458: getfield        android/content/pm/PackageStats.packageName:Ljava/lang/String;
                    //   461: invokevirtual   java/lang/String.equals:(Ljava/lang/Object;)Z
                    //   464: ifeq            505
                    //   467: aload_0        
                    //   468: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   471: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   474: getfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizeUserId:I
                    //   477: aload_1        
                    //   478: getfield        android/content/pm/PackageStats.userHandle:I
                    //   481: if_icmpne       505
                    //   484: aload_0        
                    //   485: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   488: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
                    //   491: aconst_null    
                    //   492: putfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizePkg:Ljava/lang/String;
                    //   495: aload_0        
                    //   496: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler$1.this$1:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
                    //   499: bipush          7
                    //   501: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
                    //   504: pop            
                    //   505: aload_3        
                    //   506: monitorexit    
                    //   507: return         
                    //   508: astore_1       
                    //   509: aload_3        
                    //   510: monitorexit    
                    //   511: aload_1        
                    //   512: athrow         
                    //   513: astore_1       
                    //   514: goto            509
                    //    Exceptions:
                    //  Try           Handler
                    //  Start  End    Start  End    Type
                    //  -----  -----  -----  -----  ----
                    //  18     40     508    509    Any
                    //  45     47     513    517    Any
                    //  48     62     508    509    Any
                    //  67     70     508    509    Any
                    //  70     141    424    425    Any
                    //  141    212    420    424    Any
                    //  221    370    420    424    Any
                    //  373    376    430    434    Any
                    //  381    417    513    517    Any
                    //  425    428    430    434    Any
                    //  428    430    513    517    Any
                    //  434    505    513    517    Any
                    //  505    507    513    517    Any
                    //  509    511    513    517    Any
                    // 
                    // The error that occurred was:
                    // 
                    // java.lang.IllegalStateException: Expression is linked from several locations: Label_0373:
                    //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
                    //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
                    //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformCall(AstMethodBodyBuilder.java:1163)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:1010)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformByteCode(AstMethodBodyBuilder.java:554)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformExpression(AstMethodBodyBuilder.java:540)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformNode(AstMethodBodyBuilder.java:392)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.transformBlock(AstMethodBodyBuilder.java:333)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:294)
                    //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createConstructor(AstBuilder.java:692)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:529)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
                    //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
                    //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
                    //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
                    //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
                    //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
                    //     at java.lang.Thread.run(Thread.java:745)
                    // 
                    throw new IllegalStateException("An error occurred while decompiling this method.");
                }
            };
        }
        
        private int getCombinedSessionFlags(final List<Session> list) {
            final SparseArray<HashMap<String, AppEntry>> mEntriesMap = ApplicationsState.this.mEntriesMap;
            // monitorenter(mEntriesMap)
            int n = 0;
            try {
                final Iterator<Session> iterator = list.iterator();
                while (iterator.hasNext()) {
                    n |= iterator.next().mFlags;
                }
                return n;
            }
            finally {
            }
            // monitorexit(mEntriesMap)
        }
        
        public void handleMessage(final Message p0) {
            // 
            // This method could not be decompiled.
            // 
            // Original Bytecode:
            // 
            //     1: astore_2       
            //     2: aload_0        
            //     3: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //     6: getfield        com/android/settingslib/applications/ApplicationsState.mRebuildingSessions:Ljava/util/ArrayList;
            //     9: astore_3       
            //    10: aload_3        
            //    11: monitorenter   
            //    12: aload_0        
            //    13: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //    16: getfield        com/android/settingslib/applications/ApplicationsState.mRebuildingSessions:Ljava/util/ArrayList;
            //    19: invokevirtual   java/util/ArrayList.size:()I
            //    22: ifle            50
            //    25: new             Ljava/util/ArrayList;
            //    28: astore_2       
            //    29: aload_2        
            //    30: aload_0        
            //    31: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //    34: getfield        com/android/settingslib/applications/ApplicationsState.mRebuildingSessions:Ljava/util/ArrayList;
            //    37: invokespecial   java/util/ArrayList.<init>:(Ljava/util/Collection;)V
            //    40: aload_0        
            //    41: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //    44: getfield        com/android/settingslib/applications/ApplicationsState.mRebuildingSessions:Ljava/util/ArrayList;
            //    47: invokevirtual   java/util/ArrayList.clear:()V
            //    50: aload_3        
            //    51: monitorexit    
            //    52: iconst_0       
            //    53: istore          4
            //    55: aload_2        
            //    56: ifnull          89
            //    59: iconst_0       
            //    60: istore          5
            //    62: iload           5
            //    64: aload_2        
            //    65: invokevirtual   java/util/ArrayList.size:()I
            //    68: if_icmpge       89
            //    71: aload_2        
            //    72: iload           5
            //    74: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
            //    77: checkcast       Lcom/android/settingslib/applications/ApplicationsState$Session;
            //    80: invokevirtual   com/android/settingslib/applications/ApplicationsState$Session.handleRebuildList:()V
            //    83: iinc            5, 1
            //    86: goto            62
            //    89: aload_0        
            //    90: aload_0        
            //    91: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //    94: getfield        com/android/settingslib/applications/ApplicationsState.mSessions:Ljava/util/ArrayList;
            //    97: invokespecial   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.getCombinedSessionFlags:(Ljava/util/List;)I
            //   100: istore          5
            //   102: aload_1        
            //   103: getfield        android/os/Message.what:I
            //   106: tableswitch {
            //                2: 1622
            //                3: 1294
            //                4: 1135
            //                5: 707
            //                6: 707
            //                7: 475
            //                8: 151
            //          default: 148
            //        }
            //   148: goto            1622
            //   151: iload           5
            //   153: iconst_4       
            //   154: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //   157: ifeq            472
            //   160: aload_0        
            //   161: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   164: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //   167: astore_1       
            //   168: aload_1        
            //   169: monitorenter   
            //   170: aload_0        
            //   171: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   174: getfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizePkg:Ljava/lang/String;
            //   177: ifnull          183
            //   180: aload_1        
            //   181: monitorexit    
            //   182: return         
            //   183: invokestatic    android/os/SystemClock.uptimeMillis:()J
            //   186: lstore          6
            //   188: iconst_0       
            //   189: istore          5
            //   191: iload           5
            //   193: aload_0        
            //   194: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   197: getfield        com/android/settingslib/applications/ApplicationsState.mAppEntries:Ljava/util/ArrayList;
            //   200: invokevirtual   java/util/ArrayList.size:()I
            //   203: if_icmpge       402
            //   206: aload_0        
            //   207: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   210: getfield        com/android/settingslib/applications/ApplicationsState.mAppEntries:Ljava/util/ArrayList;
            //   213: iload           5
            //   215: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
            //   218: checkcast       Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
            //   221: astore_2       
            //   222: aload_2        
            //   223: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.info:Landroid/content/pm/ApplicationInfo;
            //   226: getfield        android/content/pm/ApplicationInfo.flags:I
            //   229: ldc             8388608
            //   231: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //   234: ifeq            396
            //   237: aload_2        
            //   238: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.size:J
            //   241: ldc2_w          -1
            //   244: lcmp           
            //   245: ifeq            255
            //   248: aload_2        
            //   249: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeStale:Z
            //   252: ifeq            396
            //   255: aload_2        
            //   256: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeLoadStart:J
            //   259: lconst_0       
            //   260: lcmp           
            //   261: ifeq            278
            //   264: aload_2        
            //   265: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeLoadStart:J
            //   268: lload           6
            //   270: ldc2_w          20000
            //   273: lsub           
            //   274: lcmp           
            //   275: ifge            393
            //   278: aload_0        
            //   279: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //   282: ifne            319
            //   285: aload_0        
            //   286: iconst_1       
            //   287: putfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //   290: aload_0        
            //   291: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   294: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   297: bipush          6
            //   299: iconst_1       
            //   300: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   303: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
            //   306: astore_3       
            //   307: aload_0        
            //   308: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   311: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   314: aload_3        
            //   315: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendMessage:(Landroid/os/Message;)Z
            //   318: pop            
            //   319: aload_2        
            //   320: lload           6
            //   322: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.sizeLoadStart:J
            //   325: aload_0        
            //   326: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   329: aload_2        
            //   330: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.info:Landroid/content/pm/ApplicationInfo;
            //   333: getfield        android/content/pm/ApplicationInfo.storageUuid:Ljava/util/UUID;
            //   336: putfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizeUuid:Ljava/util/UUID;
            //   339: aload_0        
            //   340: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   343: aload_2        
            //   344: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.info:Landroid/content/pm/ApplicationInfo;
            //   347: getfield        android/content/pm/ApplicationInfo.packageName:Ljava/lang/String;
            //   350: putfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizePkg:Ljava/lang/String;
            //   353: aload_0        
            //   354: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   357: aload_2        
            //   358: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.info:Landroid/content/pm/ApplicationInfo;
            //   361: getfield        android/content/pm/ApplicationInfo.uid:I
            //   364: invokestatic    android/os/UserHandle.getUserId:(I)I
            //   367: putfield        com/android/settingslib/applications/ApplicationsState.mCurComputingSizeUserId:I
            //   370: aload_0        
            //   371: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   374: getfield        com/android/settingslib/applications/ApplicationsState.mBackgroundHandler:Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;
            //   377: astore_2       
            //   378: new             Lcom/android/settingslib/applications/_$$Lambda$ApplicationsState$BackgroundHandler$7jhXQzAcRoT6ACDzmPBTQMi7Ldc;
            //   381: astore_3       
            //   382: aload_3        
            //   383: aload_0        
            //   384: invokespecial   com/android/settingslib/applications/_$$Lambda$ApplicationsState$BackgroundHandler$7jhXQzAcRoT6ACDzmPBTQMi7Ldc.<init>:(Lcom/android/settingslib/applications/ApplicationsState$BackgroundHandler;)V
            //   387: aload_2        
            //   388: aload_3        
            //   389: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.post:(Ljava/lang/Runnable;)Z
            //   392: pop            
            //   393: aload_1        
            //   394: monitorexit    
            //   395: return         
            //   396: iinc            5, 1
            //   399: goto            191
            //   402: aload_0        
            //   403: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   406: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   409: iconst_5       
            //   410: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.hasMessages:(I)Z
            //   413: ifne            462
            //   416: aload_0        
            //   417: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   420: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   423: iconst_5       
            //   424: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendEmptyMessage:(I)Z
            //   427: pop            
            //   428: aload_0        
            //   429: iconst_0       
            //   430: putfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //   433: aload_0        
            //   434: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   437: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   440: bipush          6
            //   442: iconst_0       
            //   443: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   446: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
            //   449: astore_3       
            //   450: aload_0        
            //   451: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   454: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   457: aload_3        
            //   458: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendMessage:(Landroid/os/Message;)Z
            //   461: pop            
            //   462: aload_1        
            //   463: monitorexit    
            //   464: goto            472
            //   467: astore_3       
            //   468: aload_1        
            //   469: monitorexit    
            //   470: aload_3        
            //   471: athrow         
            //   472: goto            1622
            //   475: iload           5
            //   477: iconst_2       
            //   478: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //   481: ifeq            697
            //   484: iconst_0       
            //   485: istore          5
            //   487: aload_0        
            //   488: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   491: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //   494: astore_1       
            //   495: aload_1        
            //   496: monitorenter   
            //   497: iload           4
            //   499: aload_0        
            //   500: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   503: getfield        com/android/settingslib/applications/ApplicationsState.mAppEntries:Ljava/util/ArrayList;
            //   506: invokevirtual   java/util/ArrayList.size:()I
            //   509: if_icmpge       643
            //   512: iload           5
            //   514: iconst_2       
            //   515: if_icmpge       643
            //   518: aload_0        
            //   519: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   522: getfield        com/android/settingslib/applications/ApplicationsState.mAppEntries:Ljava/util/ArrayList;
            //   525: iload           4
            //   527: invokevirtual   java/util/ArrayList.get:(I)Ljava/lang/Object;
            //   530: checkcast       Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
            //   533: astore_3       
            //   534: aload_3        
            //   535: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.icon:Landroid/graphics/drawable/Drawable;
            //   538: ifnull          552
            //   541: iload           5
            //   543: istore          8
            //   545: aload_3        
            //   546: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.mounted:Z
            //   549: ifne            628
            //   552: aload_3        
            //   553: monitorenter   
            //   554: iload           5
            //   556: istore          8
            //   558: aload_3        
            //   559: aload_0        
            //   560: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   563: getfield        com/android/settingslib/applications/ApplicationsState.mContext:Landroid/content/Context;
            //   566: aload_0        
            //   567: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   570: getfield        com/android/settingslib/applications/ApplicationsState.mDrawableFactory:Landroid/util/IconDrawableFactory;
            //   573: invokevirtual   com/android/settingslib/applications/ApplicationsState$AppEntry.ensureIconLocked:(Landroid/content/Context;Landroid/util/IconDrawableFactory;)Z
            //   576: ifeq            626
            //   579: aload_0        
            //   580: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //   583: ifne            620
            //   586: aload_0        
            //   587: iconst_1       
            //   588: putfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //   591: aload_0        
            //   592: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   595: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   598: bipush          6
            //   600: iconst_1       
            //   601: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //   604: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
            //   607: astore_2       
            //   608: aload_0        
            //   609: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   612: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   615: aload_2        
            //   616: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendMessage:(Landroid/os/Message;)Z
            //   619: pop            
            //   620: iload           5
            //   622: iconst_1       
            //   623: iadd           
            //   624: istore          8
            //   626: aload_3        
            //   627: monitorexit    
            //   628: iinc            4, 1
            //   631: iload           8
            //   633: istore          5
            //   635: goto            497
            //   638: astore_2       
            //   639: aload_3        
            //   640: monitorexit    
            //   641: aload_2        
            //   642: athrow         
            //   643: aload_1        
            //   644: monitorexit    
            //   645: iload           5
            //   647: ifle            676
            //   650: aload_0        
            //   651: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   654: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   657: iconst_3       
            //   658: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.hasMessages:(I)Z
            //   661: ifne            676
            //   664: aload_0        
            //   665: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   668: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //   671: iconst_3       
            //   672: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendEmptyMessage:(I)Z
            //   675: pop            
            //   676: iload           5
            //   678: iconst_2       
            //   679: if_icmplt       697
            //   682: aload_0        
            //   683: bipush          6
            //   685: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //   688: pop            
            //   689: goto            472
            //   692: astore_3       
            //   693: aload_1        
            //   694: monitorexit    
            //   695: aload_3        
            //   696: athrow         
            //   697: aload_0        
            //   698: bipush          7
            //   700: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //   703: pop            
            //   704: goto            472
            //   707: aload_1        
            //   708: getfield        android/os/Message.what:I
            //   711: iconst_4       
            //   712: if_icmpne       725
            //   715: iload           5
            //   717: bipush          8
            //   719: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //   722: ifne            743
            //   725: aload_1        
            //   726: getfield        android/os/Message.what:I
            //   729: iconst_5       
            //   730: if_icmpne       1108
            //   733: iload           5
            //   735: bipush          16
            //   737: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //   740: ifeq            1108
            //   743: new             Landroid/content/Intent;
            //   746: dup            
            //   747: ldc_w           "android.intent.action.MAIN"
            //   750: aconst_null    
            //   751: invokespecial   android/content/Intent.<init>:(Ljava/lang/String;Landroid/net/Uri;)V
            //   754: astore          9
            //   756: aload_1        
            //   757: getfield        android/os/Message.what:I
            //   760: iconst_4       
            //   761: if_icmpne       771
            //   764: ldc_w           "android.intent.category.LAUNCHER"
            //   767: astore_3       
            //   768: goto            775
            //   771: ldc_w           "android.intent.category.LEANBACK_LAUNCHER"
            //   774: astore_3       
            //   775: aload           9
            //   777: aload_3        
            //   778: invokevirtual   android/content/Intent.addCategory:(Ljava/lang/String;)Landroid/content/Intent;
            //   781: pop            
            //   782: iconst_0       
            //   783: istore          5
            //   785: aload           9
            //   787: astore_3       
            //   788: iload           5
            //   790: aload_0        
            //   791: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   794: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //   797: invokevirtual   android/util/SparseArray.size:()I
            //   800: if_icmpge       1077
            //   803: aload_0        
            //   804: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   807: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //   810: iload           5
            //   812: invokevirtual   android/util/SparseArray.keyAt:(I)I
            //   815: istore          10
            //   817: aload_0        
            //   818: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   821: getfield        com/android/settingslib/applications/ApplicationsState.mPm:Landroid/content/pm/PackageManager;
            //   824: aload_3        
            //   825: ldc_w           786944
            //   828: iload           10
            //   830: invokevirtual   android/content/pm/PackageManager.queryIntentActivitiesAsUser:(Landroid/content/Intent;II)Ljava/util/List;
            //   833: astore          11
            //   835: aload_0        
            //   836: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   839: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //   842: astore          12
            //   844: aload           12
            //   846: monitorenter   
            //   847: aload_2        
            //   848: astore          9
            //   850: aload_3        
            //   851: astore          9
            //   853: aload_0        
            //   854: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //   857: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //   860: iload           5
            //   862: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
            //   865: checkcast       Ljava/util/HashMap;
            //   868: astore          13
            //   870: aload_2        
            //   871: astore          9
            //   873: aload_3        
            //   874: astore          9
            //   876: aload           11
            //   878: invokeinterface java/util/List.size:()I
            //   883: istore          8
            //   885: iconst_0       
            //   886: istore          4
            //   888: iload           4
            //   890: iload           8
            //   892: if_icmpge       1058
            //   895: aload_2        
            //   896: astore          9
            //   898: aload_3        
            //   899: astore          9
            //   901: aload           11
            //   903: iload           4
            //   905: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
            //   910: checkcast       Landroid/content/pm/ResolveInfo;
            //   913: astore          14
            //   915: aload_2        
            //   916: astore          9
            //   918: aload_3        
            //   919: astore          9
            //   921: aload           14
            //   923: getfield        android/content/pm/ResolveInfo.activityInfo:Landroid/content/pm/ActivityInfo;
            //   926: getfield        android/content/pm/ActivityInfo.packageName:Ljava/lang/String;
            //   929: astore          15
            //   931: aload_2        
            //   932: astore          9
            //   934: aload_3        
            //   935: astore          9
            //   937: aload           13
            //   939: aload           15
            //   941: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
            //   944: checkcast       Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
            //   947: astore          9
            //   949: aload           9
            //   951: ifnull          994
            //   954: aload           9
            //   956: iconst_1       
            //   957: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.hasLauncherEntry:Z
            //   960: aload           9
            //   962: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.launcherEntryEnabled:Z
            //   965: istore          16
            //   967: aload           9
            //   969: aload           14
            //   971: getfield        android/content/pm/ResolveInfo.activityInfo:Landroid/content/pm/ActivityInfo;
            //   974: getfield        android/content/pm/ActivityInfo.enabled:Z
            //   977: iload           16
            //   979: ior            
            //   980: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.launcherEntryEnabled:Z
            //   983: goto            1048
            //   986: astore_1       
            //   987: goto            1068
            //   990: astore_1       
            //   991: goto            1068
            //   994: new             Ljava/lang/StringBuilder;
            //   997: dup            
            //   998: invokespecial   java/lang/StringBuilder.<init>:()V
            //  1001: astore          9
            //  1003: aload           9
            //  1005: ldc_w           "Cannot find pkg: "
            //  1008: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //  1011: pop            
            //  1012: aload           9
            //  1014: aload           15
            //  1016: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //  1019: pop            
            //  1020: aload           9
            //  1022: ldc_w           " on user "
            //  1025: invokevirtual   java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
            //  1028: pop            
            //  1029: aload           9
            //  1031: iload           10
            //  1033: invokevirtual   java/lang/StringBuilder.append:(I)Ljava/lang/StringBuilder;
            //  1036: pop            
            //  1037: ldc             "ApplicationsState"
            //  1039: aload           9
            //  1041: invokevirtual   java/lang/StringBuilder.toString:()Ljava/lang/String;
            //  1044: invokestatic    android/util/Log.w:(Ljava/lang/String;Ljava/lang/String;)I
            //  1047: pop            
            //  1048: iinc            4, 1
            //  1051: goto            888
            //  1054: astore_1       
            //  1055: goto            1068
            //  1058: aload           12
            //  1060: monitorexit    
            //  1061: iinc            5, 1
            //  1064: goto            788
            //  1067: astore_1       
            //  1068: aload           12
            //  1070: monitorexit    
            //  1071: aload_1        
            //  1072: athrow         
            //  1073: astore_1       
            //  1074: goto            1068
            //  1077: aload_0        
            //  1078: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1081: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //  1084: bipush          7
            //  1086: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.hasMessages:(I)Z
            //  1089: ifne            1108
            //  1092: aload_0        
            //  1093: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1096: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //  1099: bipush          7
            //  1101: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendEmptyMessage:(I)Z
            //  1104: pop            
            //  1105: goto            1108
            //  1108: aload_1        
            //  1109: getfield        android/os/Message.what:I
            //  1112: iconst_4       
            //  1113: if_icmpne       1125
            //  1116: aload_0        
            //  1117: iconst_5       
            //  1118: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //  1121: pop            
            //  1122: goto            1622
            //  1125: aload_0        
            //  1126: bipush          6
            //  1128: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //  1131: pop            
            //  1132: goto            1622
            //  1135: iload           5
            //  1137: iconst_1       
            //  1138: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //  1141: ifeq            1285
            //  1144: new             Ljava/util/ArrayList;
            //  1147: dup            
            //  1148: invokespecial   java/util/ArrayList.<init>:()V
            //  1151: astore          9
            //  1153: aload_0        
            //  1154: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1157: getfield        com/android/settingslib/applications/ApplicationsState.mPm:Landroid/content/pm/PackageManager;
            //  1160: aload           9
            //  1162: invokevirtual   android/content/pm/PackageManager.getHomeActivities:(Ljava/util/List;)Landroid/content/ComponentName;
            //  1165: pop            
            //  1166: aload_0        
            //  1167: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1170: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1173: astore_1       
            //  1174: aload_1        
            //  1175: monitorenter   
            //  1176: aload_0        
            //  1177: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1180: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1183: invokevirtual   android/util/SparseArray.size:()I
            //  1186: istore          8
            //  1188: iconst_0       
            //  1189: istore          5
            //  1191: iload           5
            //  1193: iload           8
            //  1195: if_icmpge       1275
            //  1198: aload_0        
            //  1199: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1202: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1205: iload           5
            //  1207: invokevirtual   android/util/SparseArray.valueAt:(I)Ljava/lang/Object;
            //  1210: checkcast       Ljava/util/HashMap;
            //  1213: astore_3       
            //  1214: aload           9
            //  1216: invokeinterface java/util/List.iterator:()Ljava/util/Iterator;
            //  1221: astore_2       
            //  1222: aload_2        
            //  1223: invokeinterface java/util/Iterator.hasNext:()Z
            //  1228: ifeq            1269
            //  1231: aload_3        
            //  1232: aload_2        
            //  1233: invokeinterface java/util/Iterator.next:()Ljava/lang/Object;
            //  1238: checkcast       Landroid/content/pm/ResolveInfo;
            //  1241: getfield        android/content/pm/ResolveInfo.activityInfo:Landroid/content/pm/ActivityInfo;
            //  1244: getfield        android/content/pm/ActivityInfo.packageName:Ljava/lang/String;
            //  1247: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
            //  1250: checkcast       Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
            //  1253: astore          12
            //  1255: aload           12
            //  1257: ifnull          1266
            //  1260: aload           12
            //  1262: iconst_1       
            //  1263: putfield        com/android/settingslib/applications/ApplicationsState$AppEntry.isHomeApp:Z
            //  1266: goto            1222
            //  1269: iinc            5, 1
            //  1272: goto            1191
            //  1275: aload_1        
            //  1276: monitorexit    
            //  1277: goto            1285
            //  1280: astore_3       
            //  1281: aload_1        
            //  1282: monitorexit    
            //  1283: aload_3        
            //  1284: athrow         
            //  1285: aload_0        
            //  1286: iconst_4       
            //  1287: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //  1290: pop            
            //  1291: goto            1622
            //  1294: aload_0        
            //  1295: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1298: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1301: astore_1       
            //  1302: aload_1        
            //  1303: monitorenter   
            //  1304: iconst_0       
            //  1305: istore          5
            //  1307: iconst_0       
            //  1308: istore          8
            //  1310: iload           8
            //  1312: aload_0        
            //  1313: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1316: getfield        com/android/settingslib/applications/ApplicationsState.mApplications:Ljava/util/List;
            //  1319: invokeinterface java/util/List.size:()I
            //  1324: if_icmpge       1562
            //  1327: iload           5
            //  1329: bipush          6
            //  1331: if_icmpge       1562
            //  1334: aload_0        
            //  1335: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //  1338: ifne            1378
            //  1341: aload_0        
            //  1342: iconst_1       
            //  1343: putfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.mRunning:Z
            //  1346: aload_0        
            //  1347: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1350: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //  1353: bipush          6
            //  1355: iconst_1       
            //  1356: invokestatic    java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
            //  1359: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.obtainMessage:(ILjava/lang/Object;)Landroid/os/Message;
            //  1362: astore_3       
            //  1363: aload_0        
            //  1364: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1367: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //  1370: aload_3        
            //  1371: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendMessage:(Landroid/os/Message;)Z
            //  1374: pop            
            //  1375: goto            1378
            //  1378: aload_0        
            //  1379: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1382: getfield        com/android/settingslib/applications/ApplicationsState.mApplications:Ljava/util/List;
            //  1385: iload           8
            //  1387: invokeinterface java/util/List.get:(I)Ljava/lang/Object;
            //  1392: checkcast       Landroid/content/pm/ApplicationInfo;
            //  1395: astore_2       
            //  1396: aload_2        
            //  1397: getfield        android/content/pm/ApplicationInfo.uid:I
            //  1400: invokestatic    android/os/UserHandle.getUserId:(I)I
            //  1403: istore          10
            //  1405: iload           5
            //  1407: istore          4
            //  1409: aload_0        
            //  1410: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1413: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1416: iload           10
            //  1418: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
            //  1421: checkcast       Ljava/util/HashMap;
            //  1424: aload_2        
            //  1425: getfield        android/content/pm/ApplicationInfo.packageName:Ljava/lang/String;
            //  1428: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
            //  1431: ifnonnull       1449
            //  1434: iload           5
            //  1436: iconst_1       
            //  1437: iadd           
            //  1438: istore          4
            //  1440: aload_0        
            //  1441: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1444: aload_2        
            //  1445: invokestatic    com/android/settingslib/applications/ApplicationsState.access$100:(Lcom/android/settingslib/applications/ApplicationsState;Landroid/content/pm/ApplicationInfo;)Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
            //  1448: pop            
            //  1449: iload           10
            //  1451: ifeq            1552
            //  1454: aload_0        
            //  1455: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1458: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1461: iconst_0       
            //  1462: invokevirtual   android/util/SparseArray.indexOfKey:(I)I
            //  1465: iflt            1549
            //  1468: aload_0        
            //  1469: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1472: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1475: iconst_0       
            //  1476: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
            //  1479: checkcast       Ljava/util/HashMap;
            //  1482: aload_2        
            //  1483: getfield        android/content/pm/ApplicationInfo.packageName:Ljava/lang/String;
            //  1486: invokevirtual   java/util/HashMap.get:(Ljava/lang/Object;)Ljava/lang/Object;
            //  1489: checkcast       Lcom/android/settingslib/applications/ApplicationsState$AppEntry;
            //  1492: astore_3       
            //  1493: aload_3        
            //  1494: ifnull          1552
            //  1497: aload_3        
            //  1498: getfield        com/android/settingslib/applications/ApplicationsState$AppEntry.info:Landroid/content/pm/ApplicationInfo;
            //  1501: getfield        android/content/pm/ApplicationInfo.flags:I
            //  1504: ldc             8388608
            //  1506: invokestatic    com/android/settingslib/applications/ApplicationsState.access$200:(II)Z
            //  1509: ifne            1552
            //  1512: aload_0        
            //  1513: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1516: getfield        com/android/settingslib/applications/ApplicationsState.mEntriesMap:Landroid/util/SparseArray;
            //  1519: iconst_0       
            //  1520: invokevirtual   android/util/SparseArray.get:(I)Ljava/lang/Object;
            //  1523: checkcast       Ljava/util/HashMap;
            //  1526: aload_2        
            //  1527: getfield        android/content/pm/ApplicationInfo.packageName:Ljava/lang/String;
            //  1530: invokevirtual   java/util/HashMap.remove:(Ljava/lang/Object;)Ljava/lang/Object;
            //  1533: pop            
            //  1534: aload_0        
            //  1535: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1538: getfield        com/android/settingslib/applications/ApplicationsState.mAppEntries:Ljava/util/ArrayList;
            //  1541: aload_3        
            //  1542: invokevirtual   java/util/ArrayList.remove:(Ljava/lang/Object;)Z
            //  1545: pop            
            //  1546: goto            1552
            //  1549: goto            1552
            //  1552: iinc            8, 1
            //  1555: iload           4
            //  1557: istore          5
            //  1559: goto            1310
            //  1562: aload_1        
            //  1563: monitorexit    
            //  1564: iload           5
            //  1566: bipush          6
            //  1568: if_icmplt       1580
            //  1571: aload_0        
            //  1572: iconst_2       
            //  1573: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //  1576: pop            
            //  1577: goto            1614
            //  1580: aload_0        
            //  1581: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1584: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //  1587: bipush          8
            //  1589: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.hasMessages:(I)Z
            //  1592: ifne            1608
            //  1595: aload_0        
            //  1596: getfield        com/android/settingslib/applications/ApplicationsState$BackgroundHandler.this$0:Lcom/android/settingslib/applications/ApplicationsState;
            //  1599: getfield        com/android/settingslib/applications/ApplicationsState.mMainHandler:Lcom/android/settingslib/applications/ApplicationsState$MainHandler;
            //  1602: bipush          8
            //  1604: invokevirtual   com/android/settingslib/applications/ApplicationsState$MainHandler.sendEmptyMessage:(I)Z
            //  1607: pop            
            //  1608: aload_0        
            //  1609: iconst_3       
            //  1610: invokevirtual   com/android/settingslib/applications/ApplicationsState$BackgroundHandler.sendEmptyMessage:(I)Z
            //  1613: pop            
            //  1614: goto            1622
            //  1617: astore_3       
            //  1618: aload_1        
            //  1619: monitorexit    
            //  1620: aload_3        
            //  1621: athrow         
            //  1622: return         
            //  1623: astore_1       
            //  1624: goto            1628
            //  1627: astore_1       
            //  1628: aload_3        
            //  1629: monitorexit    
            //  1630: aload_1        
            //  1631: athrow         
            //    Exceptions:
            //  Try           Handler
            //  Start  End    Start  End    Type
            //  -----  -----  -----  -----  ----
            //  12     40     1627   1628   Any
            //  40     50     1627   1628   Any
            //  50     52     1623   1627   Any
            //  170    182    467    472    Any
            //  183    188    467    472    Any
            //  191    255    467    472    Any
            //  255    278    467    472    Any
            //  278    319    467    472    Any
            //  319    393    467    472    Any
            //  393    395    467    472    Any
            //  402    462    467    472    Any
            //  462    464    467    472    Any
            //  468    470    467    472    Any
            //  497    512    692    697    Any
            //  518    541    692    697    Any
            //  545    552    692    697    Any
            //  552    554    692    697    Any
            //  558    620    638    643    Any
            //  626    628    638    643    Any
            //  639    641    638    643    Any
            //  641    643    692    697    Any
            //  643    645    692    697    Any
            //  693    695    692    697    Any
            //  853    870    1067   1068   Any
            //  876    885    1067   1068   Any
            //  901    915    1067   1068   Any
            //  921    931    1067   1068   Any
            //  937    949    1067   1068   Any
            //  954    967    990    994    Any
            //  967    983    986    990    Any
            //  994    1003   1054   1058   Any
            //  1003   1048   1073   1077   Any
            //  1058   1061   1073   1077   Any
            //  1068   1071   1073   1077   Any
            //  1176   1188   1280   1285   Any
            //  1198   1222   1280   1285   Any
            //  1222   1255   1280   1285   Any
            //  1260   1266   1280   1285   Any
            //  1275   1277   1280   1285   Any
            //  1281   1283   1280   1285   Any
            //  1310   1327   1617   1622   Any
            //  1334   1375   1617   1622   Any
            //  1378   1405   1617   1622   Any
            //  1409   1434   1617   1622   Any
            //  1440   1449   1617   1622   Any
            //  1454   1493   1617   1622   Any
            //  1497   1546   1617   1622   Any
            //  1562   1564   1617   1622   Any
            //  1618   1620   1617   1622   Any
            //  1628   1630   1627   1628   Any
            // 
            // The error that occurred was:
            // 
            // java.lang.IllegalStateException: Expression is linked from several locations: Label_1048:
            //     at com.strobel.decompiler.ast.Error.expressionLinkedFromMultipleLocations(Error.java:27)
            //     at com.strobel.decompiler.ast.AstOptimizer.mergeDisparateObjectInitializations(AstOptimizer.java:2596)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:235)
            //     at com.strobel.decompiler.ast.AstOptimizer.optimize(AstOptimizer.java:42)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:214)
            //     at com.strobel.decompiler.languages.java.ast.AstMethodBodyBuilder.createMethodBody(AstMethodBodyBuilder.java:99)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethodBody(AstBuilder.java:757)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createMethod(AstBuilder.java:655)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:532)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addTypeMembers(AstBuilder.java:556)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeCore(AstBuilder.java:499)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createTypeNoCache(AstBuilder.java:141)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.createType(AstBuilder.java:130)
            //     at com.strobel.decompiler.languages.java.ast.AstBuilder.addType(AstBuilder.java:105)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.buildAst(JavaLanguage.java:71)
            //     at com.strobel.decompiler.languages.java.JavaLanguage.decompileType(JavaLanguage.java:59)
            //     at us.deathmarine.luyten.FileSaver.doSaveJarDecompiled(FileSaver.java:192)
            //     at us.deathmarine.luyten.FileSaver.access$300(FileSaver.java:45)
            //     at us.deathmarine.luyten.FileSaver$4.run(FileSaver.java:112)
            //     at java.lang.Thread.run(Thread.java:745)
            // 
            throw new IllegalStateException("An error occurred while decompiling this method.");
        }
    }
    
    public interface Callbacks
    {
        void onAllSizesComputed();
        
        void onLauncherInfoChanged();
        
        void onLoadEntriesCompleted();
        
        void onPackageIconChanged();
        
        void onPackageListChanged();
        
        void onPackageSizeChanged(final String p0);
        
        void onRebuildComplete(final ArrayList<AppEntry> p0);
        
        void onRunningStateChanged(final boolean p0);
    }
    
    public static class CompoundFilter implements AppFilter
    {
        private final AppFilter mFirstFilter;
        private final AppFilter mSecondFilter;
        
        public CompoundFilter(final AppFilter mFirstFilter, final AppFilter mSecondFilter) {
            this.mFirstFilter = mFirstFilter;
            this.mSecondFilter = mSecondFilter;
        }
        
        @Override
        public boolean filterApp(final AppEntry appEntry) {
            return this.mFirstFilter.filterApp(appEntry) && this.mSecondFilter.filterApp(appEntry);
        }
        
        @Override
        public void init() {
            this.mFirstFilter.init();
            this.mSecondFilter.init();
        }
        
        @Override
        public void init(final Context context) {
            this.mFirstFilter.init(context);
            this.mSecondFilter.init(context);
        }
    }
    
    class MainHandler extends Handler
    {
        public MainHandler(final Looper looper) {
            super(looper);
        }
        
        public void handleMessage(final Message message) {
            ApplicationsState.this.rebuildActiveSessions();
            switch (message.what) {
                case 8: {
                    final Iterator<WeakReference<Session>> iterator = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator.hasNext()) {
                        final Session session = iterator.next().get();
                        if (session != null) {
                            session.mCallbacks.onLoadEntriesCompleted();
                        }
                    }
                    break;
                }
                case 7: {
                    final Iterator<WeakReference<Session>> iterator2 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator2.hasNext()) {
                        final Session session2 = iterator2.next().get();
                        if (session2 != null) {
                            session2.mCallbacks.onLauncherInfoChanged();
                        }
                    }
                    break;
                }
                case 6: {
                    final Iterator<WeakReference<Session>> iterator3 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator3.hasNext()) {
                        final Session session3 = iterator3.next().get();
                        if (session3 != null) {
                            session3.mCallbacks.onRunningStateChanged(message.arg1 != 0);
                        }
                    }
                    break;
                }
                case 5: {
                    final Iterator<WeakReference<Session>> iterator4 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator4.hasNext()) {
                        final Session session4 = iterator4.next().get();
                        if (session4 != null) {
                            session4.mCallbacks.onAllSizesComputed();
                        }
                    }
                    break;
                }
                case 4: {
                    final Iterator<WeakReference<Session>> iterator5 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator5.hasNext()) {
                        final Session session5 = iterator5.next().get();
                        if (session5 != null) {
                            session5.mCallbacks.onPackageSizeChanged((String)message.obj);
                        }
                    }
                    break;
                }
                case 3: {
                    final Iterator<WeakReference<Session>> iterator6 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator6.hasNext()) {
                        final Session session6 = iterator6.next().get();
                        if (session6 != null) {
                            session6.mCallbacks.onPackageIconChanged();
                        }
                    }
                    break;
                }
                case 2: {
                    final Iterator<WeakReference<Session>> iterator7 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator7.hasNext()) {
                        final Session session7 = iterator7.next().get();
                        if (session7 != null) {
                            session7.mCallbacks.onPackageListChanged();
                        }
                    }
                    break;
                }
                case 1: {
                    final Session session8 = (Session)message.obj;
                    final Iterator<WeakReference<Session>> iterator8 = ApplicationsState.this.mActiveSessions.iterator();
                    while (iterator8.hasNext()) {
                        final Session session9 = iterator8.next().get();
                        if (session9 != null && session9 == session8) {
                            session8.mCallbacks.onRebuildComplete(session8.mLastAppList);
                        }
                    }
                    break;
                }
            }
        }
    }
    
    private class PackageIntentReceiver extends BroadcastReceiver
    {
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            final boolean equals = "android.intent.action.PACKAGE_ADDED".equals(action);
            final int n = 0;
            final int n2 = 0;
            int i = 0;
            if (equals) {
                final String encodedSchemeSpecificPart = intent.getData().getEncodedSchemeSpecificPart();
                while (i < ApplicationsState.this.mEntriesMap.size()) {
                    ApplicationsState.this.addPackage(encodedSchemeSpecificPart, ApplicationsState.this.mEntriesMap.keyAt(i));
                    ++i;
                }
            }
            else if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                final String encodedSchemeSpecificPart2 = intent.getData().getEncodedSchemeSpecificPart();
                for (int j = n; j < ApplicationsState.this.mEntriesMap.size(); ++j) {
                    ApplicationsState.this.removePackage(encodedSchemeSpecificPart2, ApplicationsState.this.mEntriesMap.keyAt(j));
                }
            }
            else if ("android.intent.action.PACKAGE_CHANGED".equals(action)) {
                final String encodedSchemeSpecificPart3 = intent.getData().getEncodedSchemeSpecificPart();
                for (int k = n2; k < ApplicationsState.this.mEntriesMap.size(); ++k) {
                    ApplicationsState.this.invalidatePackage(encodedSchemeSpecificPart3, ApplicationsState.this.mEntriesMap.keyAt(k));
                }
            }
            else if (!"android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(action) && !"android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(action)) {
                if ("android.intent.action.USER_ADDED".equals(action)) {
                    ApplicationsState.this.addUser(intent.getIntExtra("android.intent.extra.user_handle", -10000));
                }
                else if ("android.intent.action.USER_REMOVED".equals(action)) {
                    ApplicationsState.this.removeUser(intent.getIntExtra("android.intent.extra.user_handle", -10000));
                }
            }
            else {
                final String[] stringArrayExtra = intent.getStringArrayExtra("android.intent.extra.changed_package_list");
                if (stringArrayExtra == null || stringArrayExtra.length == 0) {
                    return;
                }
                if ("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(action)) {
                    for (final String s : stringArrayExtra) {
                        for (int n3 = 0; n3 < ApplicationsState.this.mEntriesMap.size(); ++n3) {
                            ApplicationsState.this.invalidatePackage(s, ApplicationsState.this.mEntriesMap.keyAt(n3));
                        }
                    }
                }
            }
        }
        
        void registerReceiver() {
            final IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
            intentFilter.addAction("android.intent.action.PACKAGE_CHANGED");
            intentFilter.addDataScheme("package");
            ApplicationsState.this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter);
            final IntentFilter intentFilter2 = new IntentFilter();
            intentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
            intentFilter2.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
            ApplicationsState.this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter2);
            final IntentFilter intentFilter3 = new IntentFilter();
            intentFilter3.addAction("android.intent.action.USER_ADDED");
            intentFilter3.addAction("android.intent.action.USER_REMOVED");
            ApplicationsState.this.mContext.registerReceiver((BroadcastReceiver)this, intentFilter3);
        }
        
        void unregisterReceiver() {
            ApplicationsState.this.mContext.unregisterReceiver((BroadcastReceiver)this);
        }
    }
    
    public class Session implements LifecycleObserver
    {
        final Callbacks mCallbacks;
        private int mFlags;
        private final boolean mHasLifecycle;
        ArrayList<AppEntry> mLastAppList;
        boolean mRebuildAsync;
        Comparator<AppEntry> mRebuildComparator;
        AppFilter mRebuildFilter;
        boolean mRebuildForeground;
        boolean mRebuildRequested;
        ArrayList<AppEntry> mRebuildResult;
        final Object mRebuildSync;
        boolean mResumed;
        
        Session(final Callbacks mCallbacks, final Lifecycle lifecycle) {
            this.mRebuildSync = new Object();
            this.mFlags = 15;
            this.mCallbacks = mCallbacks;
            if (lifecycle != null) {
                lifecycle.addObserver(this);
                this.mHasLifecycle = true;
            }
            else {
                this.mHasLifecycle = false;
            }
        }
        
        public ArrayList<AppEntry> getAllApps() {
            synchronized (ApplicationsState.this.mEntriesMap) {
                return new ArrayList<AppEntry>((Collection<? extends AppEntry>)ApplicationsState.this.mAppEntries);
            }
        }
        
        void handleRebuildList() {
            Object o = this.mRebuildSync;
            synchronized (o) {
                if (!this.mRebuildRequested) {
                    return;
                }
                final AppFilter mRebuildFilter = this.mRebuildFilter;
                final Comparator<AppEntry> mRebuildComparator = this.mRebuildComparator;
                int i = 0;
                this.mRebuildRequested = false;
                this.mRebuildFilter = null;
                this.mRebuildComparator = null;
                if (this.mRebuildForeground) {
                    Process.setThreadPriority(-2);
                    this.mRebuildForeground = false;
                }
                // monitorexit(o)
                if (mRebuildFilter != null) {
                    mRebuildFilter.init(ApplicationsState.this.mContext);
                }
                o = ApplicationsState.this.mEntriesMap;
                synchronized (o) {
                    final ArrayList<AppEntry> list = new ArrayList<AppEntry>((Collection<? extends AppEntry>)ApplicationsState.this.mAppEntries);
                    // monitorexit(o)
                    o = new ArrayList<Object>();
                    while (i < list.size()) {
                        final AppEntry appEntry = list.get(i);
                        if (appEntry != null && (mRebuildFilter == null || mRebuildFilter.filterApp(appEntry))) {
                            final SparseArray<HashMap<String, AppEntry>> mEntriesMap = ApplicationsState.this.mEntriesMap;
                            // monitorenter(mEntriesMap)
                            Label_0199: {
                                if (mRebuildComparator == null) {
                                    break Label_0199;
                                }
                                try {
                                    appEntry.ensureLabel(ApplicationsState.this.mContext);
                                    break Label_0199;
                                }
                                finally {
                                    // monitorexit(mEntriesMap)
                                    ((ArrayList<AppEntry>)o).add(appEntry);
                                }
                                // monitorexit(mEntriesMap)
                            }
                        }
                        ++i;
                    }
                    if (mRebuildComparator != null) {
                        synchronized (ApplicationsState.this.mEntriesMap) {
                            Collections.sort((List<Object>)o, (Comparator<? super Object>)mRebuildComparator);
                        }
                    }
                    synchronized (this.mRebuildSync) {
                        if (!this.mRebuildRequested) {
                            this.mLastAppList = (ArrayList<AppEntry>)o;
                            if (!this.mRebuildAsync) {
                                this.mRebuildResult = (ArrayList<AppEntry>)o;
                                this.mRebuildSync.notifyAll();
                            }
                            else if (!ApplicationsState.this.mMainHandler.hasMessages(1, (Object)this)) {
                                ApplicationsState.this.mMainHandler.sendMessage(ApplicationsState.this.mMainHandler.obtainMessage(1, (Object)this));
                            }
                        }
                        // monitorexit(this.mRebuildSync)
                        Process.setThreadPriority(10);
                    }
                }
            }
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        public void onDestroy() {
            if (!this.mHasLifecycle) {
                this.onPause();
            }
            synchronized (ApplicationsState.this.mEntriesMap) {
                ApplicationsState.this.mSessions.remove(this);
            }
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void onPause() {
            synchronized (ApplicationsState.this.mEntriesMap) {
                if (this.mResumed) {
                    this.mResumed = false;
                    ApplicationsState.this.mSessionsChanged = true;
                    ApplicationsState.this.mBackgroundHandler.removeMessages(1, (Object)this);
                    ApplicationsState.this.doPauseIfNeededLocked();
                }
            }
        }
        
        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        public void onResume() {
            synchronized (ApplicationsState.this.mEntriesMap) {
                if (!this.mResumed) {
                    this.mResumed = true;
                    ApplicationsState.this.mSessionsChanged = true;
                    ApplicationsState.this.doResumeIfNeededLocked();
                }
            }
        }
        
        public ArrayList<AppEntry> rebuild(final AppFilter appFilter, final Comparator<AppEntry> comparator) {
            return this.rebuild(appFilter, comparator, true);
        }
        
        public ArrayList<AppEntry> rebuild(final AppFilter mRebuildFilter, final Comparator<AppEntry> mRebuildComparator, final boolean mRebuildForeground) {
            synchronized (this.mRebuildSync) {
                synchronized (ApplicationsState.this.mRebuildingSessions) {
                    ApplicationsState.this.mRebuildingSessions.add(this);
                    this.mRebuildRequested = true;
                    this.mRebuildAsync = true;
                    this.mRebuildFilter = mRebuildFilter;
                    this.mRebuildComparator = mRebuildComparator;
                    this.mRebuildForeground = mRebuildForeground;
                    this.mRebuildResult = null;
                    if (!ApplicationsState.this.mBackgroundHandler.hasMessages(1)) {
                        ApplicationsState.this.mBackgroundHandler.sendMessage(ApplicationsState.this.mBackgroundHandler.obtainMessage(1));
                    }
                    return null;
                }
            }
        }
    }
    
    public static class SizeInfo
    {
        public long cacheSize;
        public long codeSize;
        public long dataSize;
        public long externalCacheSize;
        public long externalCodeSize;
        public long externalDataSize;
    }
    
    public static class VolumeFilter implements AppFilter
    {
        private final String mVolumeUuid;
        
        public VolumeFilter(final String mVolumeUuid) {
            this.mVolumeUuid = mVolumeUuid;
        }
        
        @Override
        public boolean filterApp(final AppEntry appEntry) {
            return Objects.equals(appEntry.info.volumeUuid, this.mVolumeUuid);
        }
        
        @Override
        public void init() {
        }
    }
}
