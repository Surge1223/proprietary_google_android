package com.android.settingslib.core;

import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceGroup;
import android.support.v7.preference.PreferenceScreen;
import android.content.Context;

public abstract class AbstractPreferenceController
{
    protected final Context mContext;
    
    public AbstractPreferenceController(final Context mContext) {
        this.mContext = mContext;
    }
    
    public void displayPreference(final PreferenceScreen preferenceScreen) {
        final String preferenceKey = this.getPreferenceKey();
        if (this.isAvailable()) {
            this.setVisible(preferenceScreen, preferenceKey, true);
            if (this instanceof Preference.OnPreferenceChangeListener) {
                preferenceScreen.findPreference(preferenceKey).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener)this);
            }
        }
        else {
            this.setVisible(preferenceScreen, preferenceKey, false);
        }
    }
    
    public abstract String getPreferenceKey();
    
    public CharSequence getSummary() {
        return null;
    }
    
    public boolean handlePreferenceTreeClick(final Preference preference) {
        return false;
    }
    
    public abstract boolean isAvailable();
    
    protected void refreshSummary(final Preference preference) {
        if (preference == null) {
            return;
        }
        final CharSequence summary = this.getSummary();
        if (summary == null) {
            return;
        }
        preference.setSummary(summary);
    }
    
    protected final void setVisible(final PreferenceGroup preferenceGroup, final String s, final boolean visible) {
        final Preference preference = preferenceGroup.findPreference(s);
        if (preference != null) {
            preference.setVisible(visible);
        }
    }
    
    public void updateState(final Preference preference) {
        this.refreshSummary(preference);
    }
}
