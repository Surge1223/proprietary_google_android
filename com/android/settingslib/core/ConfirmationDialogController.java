package com.android.settingslib.core;

import android.support.v7.preference.Preference;

public interface ConfirmationDialogController
{
    void showConfirmationDialog(final Preference p0);
}
