package com.android.settingslib.core.instrumentation;

import android.content.Context;
import android.util.Pair;

public interface LogWriter
{
    void action(final int p0, final boolean p1, final Pair<Integer, Object>... p2);
    
    @Deprecated
    void action(final Context p0, final int p1, final int p2);
    
    void action(final Context p0, final int p1, final String p2, final Pair<Integer, Object>... p3);
    
    @Deprecated
    void action(final Context p0, final int p1, final boolean p2);
    
    void action(final Context p0, final int p1, final Pair<Integer, Object>... p2);
    
    void actionWithSource(final Context p0, final int p1, final int p2);
    
    void count(final Context p0, final String p1, final int p2);
    
    void hidden(final Context p0, final int p1);
    
    void histogram(final Context p0, final String p1, final int p2);
    
    void visible(final Context p0, final int p1, final int p2);
}
