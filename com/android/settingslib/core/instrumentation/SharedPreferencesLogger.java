package com.android.settingslib.core.instrumentation;

import android.text.TextUtils;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.SharedPreferences$OnSharedPreferenceChangeListener;
import java.util.Map;
import android.content.SharedPreferences$Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.Set;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesLogger implements SharedPreferences
{
    private final Context mContext;
    private final MetricsFeatureProvider mMetricsFeature;
    private final Set<String> mPreferenceKeySet;
    private final String mTag;
    
    public SharedPreferencesLogger(final Context mContext, final String mTag, final MetricsFeatureProvider mMetricsFeature) {
        this.mContext = mContext;
        this.mTag = mTag;
        this.mMetricsFeature = mMetricsFeature;
        this.mPreferenceKeySet = new ConcurrentSkipListSet<String>();
    }
    
    public static String buildCountName(final String s, final Object o) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("|");
        sb.append(o);
        return sb.toString();
    }
    
    public static String buildPrefKey(final String s, final String s2) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("/");
        sb.append(s2);
        return sb.toString();
    }
    
    private void logValue(final String s, final Object o) {
        this.logValue(s, o, false);
    }
    
    private void logValue(final String s, final Object o, final boolean b) {
        final String buildPrefKey = buildPrefKey(this.mTag, s);
        if (!b && !this.mPreferenceKeySet.contains(buildPrefKey)) {
            this.mPreferenceKeySet.add(buildPrefKey);
            return;
        }
        this.mMetricsFeature.count(this.mContext, buildCountName(buildPrefKey, o), 1);
        Pair pair;
        if (o instanceof Long) {
            final Long n = (Long)o;
            int intValue;
            if (n > 2147483647L) {
                intValue = Integer.MAX_VALUE;
            }
            else if (n < -2147483648L) {
                intValue = Integer.MIN_VALUE;
            }
            else {
                intValue = (int)(Object)n;
            }
            pair = Pair.create((Object)1089, (Object)intValue);
        }
        else if (o instanceof Integer) {
            pair = Pair.create((Object)1089, o);
        }
        else if (o instanceof Boolean) {
            pair = Pair.create((Object)1089, (Object)(int)(((boolean)o) ? 1 : 0));
        }
        else if (o instanceof Float) {
            pair = Pair.create((Object)995, o);
        }
        else if (o instanceof String) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Tried to log string preference ");
            sb.append(buildPrefKey);
            sb.append(" = ");
            sb.append(o);
            Log.d("SharedPreferencesLogger", sb.toString());
            pair = null;
        }
        else {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("Tried to log unloggable object");
            sb2.append(o);
            Log.w("SharedPreferencesLogger", sb2.toString());
            pair = null;
        }
        if (pair != null) {
            this.mMetricsFeature.action(this.mContext, 853, Pair.create((Object)854, (Object)buildPrefKey), pair);
        }
    }
    
    private void safeLogValue(final String s, final String s2) {
        new AsyncPackageCheck().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Object[])new String[] { s, s2 });
    }
    
    public boolean contains(final String s) {
        return false;
    }
    
    public SharedPreferences$Editor edit() {
        return (SharedPreferences$Editor)new EditorLogger();
    }
    
    public Map<String, ?> getAll() {
        return null;
    }
    
    public boolean getBoolean(final String s, final boolean b) {
        return b;
    }
    
    public float getFloat(final String s, final float n) {
        return n;
    }
    
    public int getInt(final String s, final int n) {
        return n;
    }
    
    public long getLong(final String s, final long n) {
        return n;
    }
    
    public String getString(final String s, final String s2) {
        return s2;
    }
    
    public Set<String> getStringSet(final String s, final Set<String> set) {
        return set;
    }
    
    void logPackageName(String string, final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mTag);
        sb.append("/");
        sb.append(string);
        string = sb.toString();
        this.mMetricsFeature.action(this.mContext, 853, s, Pair.create((Object)854, (Object)string));
    }
    
    public void registerOnSharedPreferenceChangeListener(final SharedPreferences$OnSharedPreferenceChangeListener sharedPreferences$OnSharedPreferenceChangeListener) {
    }
    
    public void unregisterOnSharedPreferenceChangeListener(final SharedPreferences$OnSharedPreferenceChangeListener sharedPreferences$OnSharedPreferenceChangeListener) {
    }
    
    private class AsyncPackageCheck extends AsyncTask<String, Void, Void>
    {
        protected Void doInBackground(String... packageName) {
            final String s = packageName[0];
            final String s2 = packageName[1];
            final PackageManager packageManager = SharedPreferencesLogger.this.mContext.getPackageManager();
            try {
                final ComponentName unflattenFromString = ComponentName.unflattenFromString(s2);
                packageName = s2;
                if (s2 != null) {
                    packageName = unflattenFromString.getPackageName();
                }
            }
            catch (Exception ex) {
                packageName = s2;
            }
            try {
                packageManager.getPackageInfo((String)packageName, 4194304);
                SharedPreferencesLogger.this.logPackageName(s, (String)packageName);
            }
            catch (PackageManager$NameNotFoundException ex2) {
                SharedPreferencesLogger.this.logValue(s, packageName, true);
            }
            return null;
        }
    }
    
    public class EditorLogger implements SharedPreferences$Editor
    {
        public void apply() {
        }
        
        public SharedPreferences$Editor clear() {
            return (SharedPreferences$Editor)this;
        }
        
        public boolean commit() {
            return true;
        }
        
        public SharedPreferences$Editor putBoolean(final String s, final boolean b) {
            SharedPreferencesLogger.this.logValue(s, b);
            return (SharedPreferences$Editor)this;
        }
        
        public SharedPreferences$Editor putFloat(final String s, final float n) {
            SharedPreferencesLogger.this.logValue(s, n);
            return (SharedPreferences$Editor)this;
        }
        
        public SharedPreferences$Editor putInt(final String s, final int n) {
            SharedPreferencesLogger.this.logValue(s, n);
            return (SharedPreferences$Editor)this;
        }
        
        public SharedPreferences$Editor putLong(final String s, final long n) {
            SharedPreferencesLogger.this.logValue(s, n);
            return (SharedPreferences$Editor)this;
        }
        
        public SharedPreferences$Editor putString(final String s, final String s2) {
            SharedPreferencesLogger.this.safeLogValue(s, s2);
            return (SharedPreferences$Editor)this;
        }
        
        public SharedPreferences$Editor putStringSet(final String s, final Set<String> set) {
            SharedPreferencesLogger.this.safeLogValue(s, TextUtils.join((CharSequence)",", (Iterable)set));
            return (SharedPreferences$Editor)this;
        }
        
        public SharedPreferences$Editor remove(final String s) {
            return (SharedPreferences$Editor)this;
        }
    }
}
