package com.android.settingslib.core.instrumentation;

import android.content.Context;
import android.metrics.LogMaker;
import android.util.Pair;
import com.android.internal.logging.MetricsLogger;

public class EventLogWriter implements LogWriter
{
    private final MetricsLogger mMetricsLogger;
    
    public EventLogWriter() {
        this.mMetricsLogger = new MetricsLogger();
    }
    
    public void action(int i, int length, final Pair<Integer, Object>... array) {
        if (array != null && array.length != 0) {
            final LogMaker setSubtype = new LogMaker(i).setType(4).setSubtype(length);
            Pair<Integer, Object> pair;
            for (length = array.length, i = 0; i < length; ++i) {
                pair = array[i];
                setSubtype.addTaggedData((int)pair.first, pair.second);
            }
            this.mMetricsLogger.write(setSubtype);
        }
        else {
            this.mMetricsLogger.action(i, length);
        }
    }
    
    @Override
    public void action(final int n, final boolean b, final Pair<Integer, Object>... array) {
        this.action(n, b ? 1 : 0, array);
    }
    
    @Deprecated
    @Override
    public void action(final Context context, final int n, final int n2) {
        MetricsLogger.action(context, n, n2);
    }
    
    @Override
    public void action(final Context context, int i, final String packageName, final Pair<Integer, Object>... array) {
        if (array != null && array.length != 0) {
            final LogMaker setPackageName = new LogMaker(i).setType(4).setPackageName(packageName);
            int length;
            Pair<Integer, Object> pair;
            for (length = array.length, i = 0; i < length; ++i) {
                pair = array[i];
                setPackageName.addTaggedData((int)pair.first, pair.second);
            }
            MetricsLogger.action(setPackageName);
        }
        else {
            MetricsLogger.action(context, i, packageName);
        }
    }
    
    @Deprecated
    @Override
    public void action(final Context context, final int n, final boolean b) {
        MetricsLogger.action(context, n, b);
    }
    
    @Override
    public void action(final Context context, final int n, final Pair<Integer, Object>... array) {
        this.action(context, n, "", array);
    }
    
    @Override
    public void actionWithSource(final Context context, final int n, final int n2) {
        final LogMaker setType = new LogMaker(n2).setType(4);
        if (n != 0) {
            setType.addTaggedData(833, (Object)n);
        }
        MetricsLogger.action(setType);
    }
    
    @Override
    public void count(final Context context, final String s, final int n) {
        MetricsLogger.count(context, s, n);
    }
    
    @Override
    public void hidden(final Context context, final int n) {
        MetricsLogger.hidden(context, n);
    }
    
    @Override
    public void histogram(final Context context, final String s, final int n) {
        MetricsLogger.histogram(context, s, n);
    }
    
    @Override
    public void visible(final Context context, final int n, final int n2) {
        MetricsLogger.action(new LogMaker(n2).setType(1).addTaggedData(833, (Object)n));
    }
}
