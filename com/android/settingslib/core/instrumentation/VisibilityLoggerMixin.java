package com.android.settingslib.core.instrumentation;

import android.content.Intent;
import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;
import android.os.SystemClock;
import android.arch.lifecycle.LifecycleObserver;

public class VisibilityLoggerMixin implements LifecycleObserver
{
    private final int mMetricsCategory;
    private MetricsFeatureProvider mMetricsFeature;
    private int mSourceMetricsCategory;
    private long mVisibleTimestamp;
    
    private VisibilityLoggerMixin() {
        this.mSourceMetricsCategory = 0;
        this.mMetricsCategory = 0;
    }
    
    public VisibilityLoggerMixin(final int mMetricsCategory, final MetricsFeatureProvider mMetricsFeature) {
        this.mSourceMetricsCategory = 0;
        this.mMetricsCategory = mMetricsCategory;
        this.mMetricsFeature = mMetricsFeature;
    }
    
    public long elapsedTimeSinceVisible() {
        if (this.mVisibleTimestamp == 0L) {
            return 0L;
        }
        return SystemClock.elapsedRealtime() - this.mVisibleTimestamp;
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void onPause() {
        this.mVisibleTimestamp = 0L;
        if (this.mMetricsFeature != null && this.mMetricsCategory != 0) {
            this.mMetricsFeature.hidden(null, this.mMetricsCategory);
        }
    }
    
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onResume() {
        this.mVisibleTimestamp = SystemClock.elapsedRealtime();
        if (this.mMetricsFeature != null && this.mMetricsCategory != 0) {
            this.mMetricsFeature.visible(null, this.mSourceMetricsCategory, this.mMetricsCategory);
        }
    }
    
    public void setSourceMetricsCategory(final Activity activity) {
        if (this.mSourceMetricsCategory != 0 || activity == null) {
            return;
        }
        final Intent intent = activity.getIntent();
        if (intent == null) {
            return;
        }
        this.mSourceMetricsCategory = intent.getIntExtra(":settings:source_metrics", 0);
    }
}
