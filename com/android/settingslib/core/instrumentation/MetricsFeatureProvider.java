package com.android.settingslib.core.instrumentation;

import android.content.ComponentName;
import android.text.TextUtils;
import android.content.Intent;
import java.util.Iterator;
import android.content.Context;
import android.util.Pair;
import java.util.ArrayList;
import java.util.List;

public class MetricsFeatureProvider
{
    private List<LogWriter> mLoggerWriters;
    
    public MetricsFeatureProvider() {
        this.mLoggerWriters = new ArrayList<LogWriter>();
        this.installLogWriters();
    }
    
    private Pair<Integer, Object> sinceVisibleTaggedData(final long n) {
        return (Pair<Integer, Object>)Pair.create((Object)794, (Object)n);
    }
    
    @Deprecated
    public void action(final Context context, final int n, final int n2) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().action(context, n, n2);
        }
    }
    
    public void action(final Context context, final int n, final String s, final Pair<Integer, Object>... array) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().action(context, n, s, array);
        }
    }
    
    @Deprecated
    public void action(final Context context, final int n, final boolean b) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().action(context, n, b);
        }
    }
    
    public void action(final Context context, final int n, final Pair<Integer, Object>... array) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().action(context, n, array);
        }
    }
    
    public void action(final VisibilityLoggerMixin visibilityLoggerMixin, final int n, final boolean b) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().action(n, b, this.sinceVisibleTaggedData(visibilityLoggerMixin.elapsedTimeSinceVisible()));
        }
    }
    
    public void actionWithSource(final Context context, final int n, final int n2) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().actionWithSource(context, n, n2);
        }
    }
    
    public void count(final Context context, final String s, final int n) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().count(context, s, n);
        }
    }
    
    public int getMetricsCategory(final Object o) {
        if (o != null && o instanceof Instrumentable) {
            return ((Instrumentable)o).getMetricsCategory();
        }
        return 0;
    }
    
    public void hidden(final Context context, final int n) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().hidden(context, n);
        }
    }
    
    public void histogram(final Context context, final String s, final int n) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().histogram(context, s, n);
        }
    }
    
    protected void installLogWriters() {
        this.mLoggerWriters.add(new EventLogWriter());
    }
    
    public void logDashboardStartIntent(final Context context, final Intent intent, final int n) {
        if (intent == null) {
            return;
        }
        final ComponentName component = intent.getComponent();
        if (component == null) {
            final String action = intent.getAction();
            if (TextUtils.isEmpty((CharSequence)action)) {
                return;
            }
            this.action(context, 830, action, Pair.create((Object)833, (Object)n));
        }
        else {
            if (TextUtils.equals((CharSequence)component.getPackageName(), (CharSequence)context.getPackageName())) {
                return;
            }
            this.action(context, 830, component.flattenToString(), Pair.create((Object)833, (Object)n));
        }
    }
    
    public void visible(final Context context, final int n, final int n2) {
        final Iterator<LogWriter> iterator = this.mLoggerWriters.iterator();
        while (iterator.hasNext()) {
            iterator.next().visible(context, n, n2);
        }
    }
}
