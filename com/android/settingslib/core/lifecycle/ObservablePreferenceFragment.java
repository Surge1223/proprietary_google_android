package com.android.settingslib.core.lifecycle;

import android.support.v7.preference.PreferenceScreen;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import android.content.Context;
import android.arch.lifecycle.LifecycleOwner;
import android.support.v14.preference.PreferenceFragment;

public abstract class ObservablePreferenceFragment extends PreferenceFragment implements LifecycleOwner
{
    private final Lifecycle mLifecycle;
    
    public ObservablePreferenceFragment() {
        this.mLifecycle = new Lifecycle(this);
    }
    
    @Override
    public Lifecycle getLifecycle() {
        return this.mLifecycle;
    }
    
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mLifecycle.onAttach(context);
    }
    
    @Override
    public void onCreate(final Bundle bundle) {
        this.mLifecycle.onCreate(bundle);
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_CREATE);
        super.onCreate(bundle);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        this.mLifecycle.onCreateOptionsMenu(menu, menuInflater);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    public void onDestroy() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_DESTROY);
        super.onDestroy();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final boolean onOptionsItemSelected = this.mLifecycle.onOptionsItemSelected(menuItem);
        if (!onOptionsItemSelected) {
            return super.onOptionsItemSelected(menuItem);
        }
        return onOptionsItemSelected;
    }
    
    public void onPause() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_PAUSE);
        super.onPause();
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        this.mLifecycle.onPrepareOptionsMenu(menu);
        super.onPrepareOptionsMenu(menu);
    }
    
    public void onResume() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_RESUME);
        super.onResume();
    }
    
    @Override
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mLifecycle.onSaveInstanceState(bundle);
    }
    
    @Override
    public void onStart() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_START);
        super.onStart();
    }
    
    @Override
    public void onStop() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_STOP);
        super.onStop();
    }
    
    @Override
    public void setPreferenceScreen(final PreferenceScreen preferenceScreen) {
        this.mLifecycle.setPreferenceScreen(preferenceScreen);
        super.setPreferenceScreen(preferenceScreen);
    }
}
