package com.android.settingslib.core.lifecycle;

import android.arch.lifecycle.OnLifecycleEvent;
import android.util.Log;
import com.android.settingslib.core.lifecycle.events.SetPreferenceScreen;
import android.support.v7.preference.PreferenceScreen;
import com.android.settingslib.core.lifecycle.events.OnSaveInstanceState;
import com.android.settingslib.core.lifecycle.events.OnPrepareOptionsMenu;
import com.android.settingslib.core.lifecycle.events.OnOptionsItemSelected;
import android.view.MenuItem;
import com.android.settingslib.core.lifecycle.events.OnCreateOptionsMenu;
import android.view.MenuInflater;
import android.view.Menu;
import com.android.settingslib.core.lifecycle.events.OnCreate;
import android.os.Bundle;
import com.android.settingslib.core.lifecycle.events.OnAttach;
import android.content.Context;
import com.android.settingslib.utils.ThreadUtils;
import com.android.settingslib.core.lifecycle.events.OnStop;
import com.android.settingslib.core.lifecycle.events.OnStart;
import com.android.settingslib.core.lifecycle.events.OnResume;
import com.android.settingslib.core.lifecycle.events.OnPause;
import com.android.settingslib.core.lifecycle.events.OnDestroy;
import java.util.ArrayList;
import android.arch.lifecycle.LifecycleOwner;
import java.util.List;
import android.arch.lifecycle.LifecycleRegistry;

public class Lifecycle extends LifecycleRegistry
{
    private final List<LifecycleObserver> mObservers;
    private final LifecycleProxy mProxy;
    
    public Lifecycle(final LifecycleOwner lifecycleOwner) {
        super(lifecycleOwner);
        this.mObservers = new ArrayList<LifecycleObserver>();
        this.addObserver(this.mProxy = new LifecycleProxy());
    }
    
    private void onDestroy() {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnDestroy) {
                ((OnDestroy)lifecycleObserver).onDestroy();
            }
        }
    }
    
    private void onPause() {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnPause) {
                ((OnPause)lifecycleObserver).onPause();
            }
        }
    }
    
    private void onResume() {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnResume) {
                ((OnResume)lifecycleObserver).onResume();
            }
        }
    }
    
    private void onStart() {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnStart) {
                ((OnStart)lifecycleObserver).onStart();
            }
        }
    }
    
    private void onStop() {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnStop) {
                ((OnStop)lifecycleObserver).onStop();
            }
        }
    }
    
    @Override
    public void addObserver(final android.arch.lifecycle.LifecycleObserver lifecycleObserver) {
        ThreadUtils.ensureMainThread();
        super.addObserver(lifecycleObserver);
        if (lifecycleObserver instanceof LifecycleObserver) {
            this.mObservers.add((LifecycleObserver)lifecycleObserver);
        }
    }
    
    public void onAttach(final Context context) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnAttach) {
                ((OnAttach)lifecycleObserver).onAttach(context);
            }
        }
    }
    
    public void onCreate(final Bundle bundle) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnCreate) {
                ((OnCreate)lifecycleObserver).onCreate(bundle);
            }
        }
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnCreateOptionsMenu) {
                ((OnCreateOptionsMenu)lifecycleObserver).onCreateOptionsMenu(menu, menuInflater);
            }
        }
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnOptionsItemSelected && ((OnOptionsItemSelected)lifecycleObserver).onOptionsItemSelected(menuItem)) {
                return true;
            }
        }
        return false;
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnPrepareOptionsMenu) {
                ((OnPrepareOptionsMenu)lifecycleObserver).onPrepareOptionsMenu(menu);
            }
        }
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof OnSaveInstanceState) {
                ((OnSaveInstanceState)lifecycleObserver).onSaveInstanceState(bundle);
            }
        }
    }
    
    @Override
    public void removeObserver(final android.arch.lifecycle.LifecycleObserver lifecycleObserver) {
        ThreadUtils.ensureMainThread();
        super.removeObserver(lifecycleObserver);
        if (lifecycleObserver instanceof LifecycleObserver) {
            this.mObservers.remove(lifecycleObserver);
        }
    }
    
    public void setPreferenceScreen(final PreferenceScreen preferenceScreen) {
        for (int i = 0; i < this.mObservers.size(); ++i) {
            final LifecycleObserver lifecycleObserver = this.mObservers.get(i);
            if (lifecycleObserver instanceof SetPreferenceScreen) {
                ((SetPreferenceScreen)lifecycleObserver).setPreferenceScreen(preferenceScreen);
            }
        }
    }
    
    private class LifecycleProxy implements LifecycleObserver
    {
        @OnLifecycleEvent(Event.ON_ANY)
        public void onLifecycleEvent(final LifecycleOwner lifecycleOwner, final Event event) {
            switch (event) {
                default: {}
                case ON_CREATE: {}
                case ON_ANY: {
                    Log.wtf("LifecycleObserver", "Should not receive an 'ANY' event!");
                }
                case ON_DESTROY: {
                    Lifecycle.this.onDestroy();
                }
                case ON_STOP: {
                    Lifecycle.this.onStop();
                }
                case ON_PAUSE: {
                    Lifecycle.this.onPause();
                }
                case ON_RESUME: {
                    Lifecycle.this.onResume();
                }
                case ON_START: {
                    Lifecycle.this.onStart();
                }
            }
        }
    }
}
