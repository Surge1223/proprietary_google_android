package com.android.settingslib.core.lifecycle.events;

import android.view.Menu;

public interface OnPrepareOptionsMenu
{
    void onPrepareOptionsMenu(final Menu p0);
}
