package com.android.settingslib.core.lifecycle.events;

import android.os.Bundle;

public interface OnSaveInstanceState
{
    void onSaveInstanceState(final Bundle p0);
}
