package com.android.settingslib.core.lifecycle.events;

import android.content.Context;

@Deprecated
public interface OnAttach
{
    void onAttach(final Context p0);
}
