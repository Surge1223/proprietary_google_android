package com.android.settingslib.core.lifecycle.events;

import android.os.Bundle;

@Deprecated
public interface OnCreate
{
    void onCreate(final Bundle p0);
}
