package com.android.settingslib.core.lifecycle.events;

import android.view.MenuItem;

public interface OnOptionsItemSelected
{
    boolean onOptionsItemSelected(final MenuItem p0);
}
