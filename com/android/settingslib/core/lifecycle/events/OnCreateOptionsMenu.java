package com.android.settingslib.core.lifecycle.events;

import android.view.MenuInflater;
import android.view.Menu;

public interface OnCreateOptionsMenu
{
    void onCreateOptionsMenu(final Menu p0, final MenuInflater p1);
}
