package com.android.settingslib.core.lifecycle;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.PersistableBundle;
import android.content.Context;
import android.os.Bundle;
import android.arch.lifecycle.LifecycleOwner;
import android.app.Activity;

public class ObservableActivity extends Activity implements LifecycleOwner
{
    private final Lifecycle mLifecycle;
    
    public ObservableActivity() {
        this.mLifecycle = new Lifecycle(this);
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycle;
    }
    
    protected void onCreate(final Bundle bundle) {
        this.mLifecycle.onAttach((Context)this);
        this.mLifecycle.onCreate(bundle);
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_CREATE);
        super.onCreate(bundle);
    }
    
    public void onCreate(final Bundle bundle, final PersistableBundle persistableBundle) {
        this.mLifecycle.onAttach((Context)this);
        this.mLifecycle.onCreate(bundle);
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_CREATE);
        super.onCreate(bundle, persistableBundle);
    }
    
    public boolean onCreateOptionsMenu(final Menu menu) {
        if (super.onCreateOptionsMenu(menu)) {
            this.mLifecycle.onCreateOptionsMenu(menu, null);
            return true;
        }
        return false;
    }
    
    protected void onDestroy() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_DESTROY);
        super.onDestroy();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final boolean onOptionsItemSelected = this.mLifecycle.onOptionsItemSelected(menuItem);
        if (!onOptionsItemSelected) {
            return super.onOptionsItemSelected(menuItem);
        }
        return onOptionsItemSelected;
    }
    
    protected void onPause() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_PAUSE);
        super.onPause();
    }
    
    public boolean onPrepareOptionsMenu(final Menu menu) {
        if (super.onPrepareOptionsMenu(menu)) {
            this.mLifecycle.onPrepareOptionsMenu(menu);
            return true;
        }
        return false;
    }
    
    protected void onResume() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_RESUME);
        super.onResume();
    }
    
    protected void onStart() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_START);
        super.onStart();
    }
    
    protected void onStop() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_STOP);
        super.onStop();
    }
}
