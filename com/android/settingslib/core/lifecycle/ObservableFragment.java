package com.android.settingslib.core.lifecycle;

import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.Menu;
import android.os.Bundle;
import android.content.Context;
import android.arch.lifecycle.LifecycleOwner;
import android.app.Fragment;

public class ObservableFragment extends Fragment implements LifecycleOwner
{
    private final Lifecycle mLifecycle;
    
    public ObservableFragment() {
        this.mLifecycle = new Lifecycle(this);
    }
    
    public Lifecycle getLifecycle() {
        return this.mLifecycle;
    }
    
    public void onAttach(final Context context) {
        super.onAttach(context);
        this.mLifecycle.onAttach(context);
    }
    
    public void onCreate(final Bundle bundle) {
        this.mLifecycle.onCreate(bundle);
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_CREATE);
        super.onCreate(bundle);
    }
    
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater menuInflater) {
        this.mLifecycle.onCreateOptionsMenu(menu, menuInflater);
        super.onCreateOptionsMenu(menu, menuInflater);
    }
    
    public void onDestroy() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_DESTROY);
        super.onDestroy();
    }
    
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        final boolean onOptionsItemSelected = this.mLifecycle.onOptionsItemSelected(menuItem);
        if (!onOptionsItemSelected) {
            return super.onOptionsItemSelected(menuItem);
        }
        return onOptionsItemSelected;
    }
    
    public void onPause() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_PAUSE);
        super.onPause();
    }
    
    public void onPrepareOptionsMenu(final Menu menu) {
        this.mLifecycle.onPrepareOptionsMenu(menu);
        super.onPrepareOptionsMenu(menu);
    }
    
    public void onResume() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_RESUME);
        super.onResume();
    }
    
    public void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        this.mLifecycle.onSaveInstanceState(bundle);
    }
    
    public void onStart() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_START);
        super.onStart();
    }
    
    public void onStop() {
        this.mLifecycle.handleLifecycleEvent(android.arch.lifecycle.Lifecycle.Event.ON_STOP);
        super.onStop();
    }
}
