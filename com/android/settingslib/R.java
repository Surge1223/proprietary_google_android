package com.android.settingslib;

public final class R
{
    public static final class array
    {
        public static final int batterymeter_bolt_points = 2130903057;
        public static final int batterymeter_color_levels = 2130903058;
        public static final int batterymeter_color_values = 2130903059;
        public static final int batterymeter_plus_points = 2130903060;
        public static final int bluetooth_a2dp_codec_titles = 2130903076;
        public static final int select_logd_size_lowram_titles = 2130903141;
        public static final int select_logd_size_summaries = 2130903142;
        public static final int select_logd_size_titles = 2130903143;
        public static final int select_logd_size_values = 2130903144;
        public static final int select_logpersist_summaries = 2130903145;
        public static final int select_logpersist_values = 2130903147;
        public static final int wifi_status = 2130903199;
        public static final int wifi_status_with_ssid = 2130903200;
    }
    
    public static final class attr
    {
        public static final int footerPreferenceStyle = 2130968790;
        public static final int preferenceStyle = 2130968910;
        public static final int state_encrypted = 2130968970;
        public static final int state_metered = 2130968971;
        public static final int switchPreferenceStyle = 2130969044;
        public static final int wifi_friction = 2130969110;
    }
    
    public static final class color
    {
        public static final int batterymeter_bolt_color = 2131099686;
        public static final int batterymeter_plus_color = 2131099687;
        public static final int disabled_text_color = 2131099731;
        public static final int meter_background_color = 2131099812;
        public static final int meter_consumed_color = 2131099813;
    }
    
    public static final class dimen
    {
        public static final int appear_y_translation_start = 2131165295;
        public static final int battery_height = 2131165309;
        public static final int battery_powersave_outline_thickness = 2131165313;
        public static final int battery_width = 2131165314;
        public static final int bt_battery_padding = 2131165323;
        public static final int circle_avatar_size = 2131165331;
        public static final int restricted_icon_padding = 2131165525;
        public static final int restricted_icon_size = 2131165526;
        public static final int two_target_pref_medium_icon_size = 2131165677;
        public static final int two_target_pref_small_icon_size = 2131165678;
        public static final int wifi_preference_badge_padding = 2131165706;
    }
    
    public static final class drawable
    {
        public static final int ic_bt_cellphone = 2131230979;
        public static final int ic_bt_headphones_a2dp = 2131230980;
        public static final int ic_bt_headset_hfp = 2131230981;
        public static final int ic_bt_hearing_aid = 2131230982;
        public static final int ic_bt_laptop = 2131230983;
        public static final int ic_bt_misc_hid = 2131230984;
        public static final int ic_bt_network_pan = 2131230985;
        public static final int ic_bt_pointing_hid = 2131230986;
        public static final int ic_help_actionbar = 2131231030;
        public static final int ic_info = 2131231047;
        public static final int ic_info_outline_24dp = 2131231048;
        public static final int ic_lockscreen_ime = 2131231059;
        public static final int ic_settings_bluetooth = 2131231118;
        public static final int ic_settings_print = 2131231137;
    }
    
    public static final class fraction
    {
        public static final int battery_button_height_fraction = 2131296256;
        public static final int battery_subpixel_smoothing_left = 2131296257;
        public static final int battery_subpixel_smoothing_right = 2131296258;
        public static final int bt_battery_button_height_fraction = 2131296259;
        public static final int bt_battery_ratio_fraction = 2131296260;
    }
    
    public static final class id
    {
        public static final int action_bar = 2131361807;
        public static final int additional_summary = 2131361842;
        public static final int container = 2131362007;
        public static final int content_frame = 2131362012;
        public static final int content_header_container = 2131362013;
        public static final int friction_icon = 2131362173;
        public static final int restricted_icon = 2131362525;
        public static final int two_target_divider = 2131362758;
        public static final int zen_alarm_warning = 2131362840;
        public static final int zen_duration_container = 2131362843;
        public static final int zen_radio_buttons = 2131362859;
        public static final int zen_radio_buttons_content = 2131362860;
    }
    
    public static final class layout
    {
        public static final int access_point_friction_widget = 2131558439;
        public static final int preference_access_point = 2131558643;
        public static final int preference_two_target = 2131558673;
        public static final int restricted_icon = 2131558713;
        public static final int restricted_switch_preference = 2131558718;
        public static final int restricted_switch_widget = 2131558719;
        public static final int settings_with_drawer = 2131558747;
        public static final int user_preference = 2131558876;
        public static final int zen_mode_condition = 2131558899;
        public static final int zen_mode_duration_dialog = 2131558900;
        public static final int zen_mode_radio_button = 2131558901;
        public static final int zen_mode_turn_on_dialog_container = 2131558903;
    }
    
    public static final class string
    {
        public static final int accessibility_no_wifi = 2131886170;
        public static final int accessibility_wifi_one_bar = 2131886217;
        public static final int accessibility_wifi_security_type_none = 2131886218;
        public static final int accessibility_wifi_security_type_secured = 2131886219;
        public static final int accessibility_wifi_signal_full = 2131886220;
        public static final int accessibility_wifi_three_bars = 2131886221;
        public static final int accessibility_wifi_two_bars = 2131886222;
        public static final int active_input_method_subtypes = 2131886246;
        public static final int alarm_template = 2131886280;
        public static final int alarm_template_far = 2131886281;
        public static final int available_via_carrier = 2131886515;
        public static final int available_via_passpoint = 2131886516;
        public static final int battery_info_status_charging = 2131886601;
        public static final int battery_info_status_discharging = 2131886603;
        public static final int battery_info_status_full = 2131886604;
        public static final int battery_info_status_not_charging = 2131886605;
        public static final int battery_info_status_unknown = 2131886606;
        public static final int battery_meter_very_low_overlay_symbol = 2131886611;
        public static final int bluetooth_active_battery_level = 2131886689;
        public static final int bluetooth_active_no_battery_level = 2131886690;
        public static final int bluetooth_battery_level = 2131886705;
        public static final int bluetooth_connected = 2131886710;
        public static final int bluetooth_connecting = 2131886721;
        public static final int bluetooth_disconnected = 2131886762;
        public static final int bluetooth_disconnecting = 2131886763;
        public static final int bluetooth_pairing = 2131886817;
        public static final int bluetooth_pairing_device_down_error_message = 2131886821;
        public static final int bluetooth_pairing_error_message = 2131886826;
        public static final int bluetooth_pairing_pin_error_message = 2131886829;
        public static final int bluetooth_pairing_rejected_error_message = 2131886831;
        public static final int bluetooth_profile_a2dp = 2131886854;
        public static final int bluetooth_profile_a2dp_high_quality = 2131886855;
        public static final int bluetooth_profile_a2dp_high_quality_unknown_codec = 2131886856;
        public static final int bluetooth_profile_headset = 2131886858;
        public static final int bluetooth_profile_hearing_aid = 2131886859;
        public static final int bluetooth_profile_hid = 2131886860;
        public static final int bluetooth_profile_map = 2131886861;
        public static final int bluetooth_profile_opp = 2131886862;
        public static final int bluetooth_profile_pan = 2131886863;
        public static final int bluetooth_profile_pan_nap = 2131886864;
        public static final int bluetooth_profile_pbap = 2131886865;
        public static final int bluetooth_talkback_bluetooth = 2131886900;
        public static final int bluetooth_talkback_computer = 2131886901;
        public static final int bluetooth_talkback_headphone = 2131886902;
        public static final int bluetooth_talkback_headset = 2131886903;
        public static final int bluetooth_talkback_imaging = 2131886904;
        public static final int bluetooth_talkback_input_peripheral = 2131886905;
        public static final int bluetooth_talkback_phone = 2131886906;
        public static final int cancel = 2131886949;
        public static final int category_personal = 2131886983;
        public static final int category_work = 2131886984;
        public static final int certinstaller_package = 2131886996;
        public static final int choose_profile = 2131887009;
        public static final int connected_via_carrier = 2131887124;
        public static final int connected_via_network_scorer = 2131887125;
        public static final int connected_via_network_scorer_default = 2131887126;
        public static final int connected_via_passpoint = 2131887127;
        public static final int data_usage_uninstalled_apps = 2131887297;
        public static final int data_usage_uninstalled_apps_users = 2131887298;
        public static final int direct_boot_unaware_dialog_message = 2131887413;
        public static final int disabled_by_admin = 2131887424;
        public static final int disabled_by_admin_summary_text = 2131887425;
        public static final int enabled_by_admin = 2131887539;
        public static final int failed_to_open_app_settings_toast = 2131887623;
        public static final int help_feedback_label = 2131887767;
        public static final int ime_security_warning = 2131887861;
        public static final int ims_reg_status_not_registered = 2131887867;
        public static final int ims_reg_status_registered = 2131887868;
        public static final int launch_defaults_none = 2131888017;
        public static final int launch_defaults_some = 2131888018;
        public static final int managed_user_title = 2131888228;
        public static final int oem_preferred_feedback_reporter = 2131888525;
        public static final int okay = 2131888533;
        public static final int power_discharge_by = 2131888586;
        public static final int power_discharge_by_enhanced = 2131888587;
        public static final int power_discharge_by_only = 2131888588;
        public static final int power_discharge_by_only_enhanced = 2131888589;
        public static final int power_discharging_duration = 2131888591;
        public static final int power_discharging_duration_enhanced = 2131888592;
        public static final int power_remaining_duration_only = 2131888599;
        public static final int power_remaining_duration_only_enhanced = 2131888600;
        public static final int power_remaining_duration_only_shutdown_imminent = 2131888602;
        public static final int power_remaining_duration_shutdown_imminent = 2131888603;
        public static final int power_remaining_less_than_duration = 2131888604;
        public static final int power_remaining_less_than_duration_only = 2131888605;
        public static final int power_remaining_more_than_subtext = 2131888606;
        public static final int power_remaining_only_more_than_subtext = 2131888607;
        public static final int preference_summary_default_combination = 2131888623;
        public static final int process_kernel_label = 2131888677;
        public static final int running_process_item_user_label = 2131888866;
        public static final int saved_network = 2131888890;
        public static final int screen_zoom_summary_custom = 2131888922;
        public static final int screen_zoom_summary_default = 2131888923;
        public static final int screen_zoom_summary_extremely_large = 2131888924;
        public static final int screen_zoom_summary_large = 2131888925;
        public static final int screen_zoom_summary_small = 2131888926;
        public static final int screen_zoom_summary_very_large = 2131888927;
        public static final int settings_package = 2131889061;
        public static final int speed_label_fast = 2131889205;
        public static final int speed_label_okay = 2131889207;
        public static final int speed_label_slow = 2131889208;
        public static final int speed_label_very_fast = 2131889209;
        public static final int status_unavailable = 2131889249;
        public static final int tether_settings_title_all = 2131889467;
        public static final int tether_settings_title_bluetooth = 2131889468;
        public static final int tether_settings_title_usb = 2131889469;
        public static final int tether_settings_title_usb_bluetooth = 2131889470;
        public static final int tether_settings_title_wifi = 2131889471;
        public static final int time_unit_just_now = 2131889481;
        public static final int unknown = 2131889545;
        public static final int use_system_language_to_select_input_method_subtypes = 2131889675;
        public static final int user_guest = 2131889733;
        public static final int wifi_ap_unable_to_handle_new_sta = 2131889905;
        public static final int wifi_check_password_try_again = 2131889934;
        public static final int wifi_connected_no_internet = 2131889942;
        public static final int wifi_disabled_by_recommendation_provider = 2131889949;
        public static final int wifi_disabled_generic = 2131889950;
        public static final int wifi_disabled_network_failure = 2131889951;
        public static final int wifi_disabled_password_failure = 2131889952;
        public static final int wifi_fail_to_scan = 2131889994;
        public static final int wifi_metered_label = 2131890047;
        public static final int wifi_no_internet = 2131890054;
        public static final int wifi_no_internet_no_reconnect = 2131890055;
        public static final int wifi_not_in_range = 2131890056;
        public static final int wifi_remembered = 2131890081;
        public static final int wifi_security_eap = 2131890094;
        public static final int wifi_security_none = 2131890095;
        public static final int wifi_security_psk_generic = 2131890097;
        public static final int wifi_security_short_eap = 2131890098;
        public static final int wifi_security_short_psk_generic = 2131890099;
        public static final int wifi_security_short_wep = 2131890100;
        public static final int wifi_security_short_wpa = 2131890101;
        public static final int wifi_security_short_wpa2 = 2131890102;
        public static final int wifi_security_short_wpa_wpa2 = 2131890103;
        public static final int wifi_security_wep = 2131890104;
        public static final int wifi_security_wpa = 2131890105;
        public static final int wifi_security_wpa2 = 2131890106;
        public static final int wifi_security_wpa_wpa2 = 2131890107;
        public static final int wifi_status_mac_randomized = 2131890168;
        public static final int wifi_status_no_internet = 2131890169;
        public static final int wifi_status_sign_in_required = 2131890170;
        public static final int wifi_unmetered_label = 2131890186;
        public static final int zen_alarm_warning = 2131890239;
        public static final int zen_alarm_warning_indef = 2131890240;
        public static final int zen_mode_duration_always_prompt_title = 2131890300;
        public static final int zen_mode_duration_settings_title = 2131890301;
        public static final int zen_mode_enable_dialog_turn_on = 2131890305;
        public static final int zen_mode_settings_turn_on_dialog_title = 2131890377;
    }
    
    public static final class styleable
    {
        public static final int[] ActionBar;
        public static final int[] ActionBarLayout;
        public static final int[] ActionMenuItemView;
        public static final int[] ActionMenuView;
        public static final int[] ActionMode;
        public static final int[] ActivityChooserView;
        public static final int[] AdsAttrs;
        public static final int[] AlertDialog;
        public static final int[] AppCompatImageView;
        public static final int[] AppCompatSeekBar;
        public static final int[] AppCompatTextHelper;
        public static final int[] AppCompatTextView;
        public static final int[] AppCompatTheme;
        public static final int[] AppDataSearch;
        public static final int[] AspectRatioFrameLayout;
        public static final int[] AssistGestureTrainingProgressBar;
        public static final int[] AutoResizeTextView;
        public static final int[] BackgroundStyle;
        public static final int[] BatteryHistoryChart;
        public static final int[] ButtonBarLayout;
        public static final int[] ButtonWithMaxTextSize;
        public static final int[] CardView;
        public static final int[] ChartGridView;
        public static final int[] ChartNetworkSeriesView;
        public static final int[] ChartSweepView;
        public static final int[] ChartView;
        public static final int[] CheckBoxPreference;
        public static final int[] ColorStateListItem;
        public static final int[] CompoundButton;
        public static final int[] ConversationMessageView;
        public static final int[] CoordinatorLayout;
        public static final int[] CoordinatorLayout_Layout;
        public static final int[] Corpus;
        public static final int[] DialogPreference;
        public static final int[] DonutView;
        public static final int[] DotsPageIndicator;
        public static final int[] DrawerArrowToggle;
        public static final int[] FeatureParam;
        public static final int[] FixedLineSummaryPreference;
        public static final int[] FontFamily;
        public static final int[] FontFamilyFont;
        public static final int[] GlobalSearch;
        public static final int[] GlobalSearchCorpus;
        public static final int[] GlobalSearchSection;
        public static final int[] IMECorpus;
        public static final int[] LinearLayoutCompat;
        public static final int[] LinearLayoutCompat_Layout;
        public static final int[] ListPopupWindow;
        public static final int[] ListPreference;
        public static final int[] LoadingImageView;
        public static final int[] MenuGroup;
        public static final int[] MenuItem;
        public static final int[] MenuView;
        public static final int[] MultiSelectListPreference;
        public static final int[] PercentageBarChart;
        public static final int[] PopupWindow;
        public static final int[] PopupWindowBackgroundState;
        public static final int[] Preference;
        public static final int[] PreferenceFragment;
        public static final int[] PreferenceFragmentCompat;
        public static final int[] PreferenceGroup;
        public static final int[] PreferenceImageView;
        public static final int[] PreferenceTheme;
        public static final int[] RecycleListView;
        public static final int[] RecyclerView;
        public static final int[] RestrictedPreference;
        public static final int RestrictedPreference_useAdminDisabledSummary = 0;
        public static final int RestrictedPreference_userRestriction = 1;
        public static final int[] RestrictedSwitchPreference;
        public static final int RestrictedSwitchPreference_restrictedSwitchSummary = 0;
        public static final int RestrictedSwitchPreference_useAdditionalSummary = 1;
        public static final int[] SearchView;
        public static final int[] Section;
        public static final int[] SectionFeature;
        public static final int[] SeekBarPreference;
        public static final int[] SignInButton;
        public static final int[] SliceView;
        public static final int[] Spinner;
        public static final int[] SuwAbstractItem;
        public static final int[] SuwButtonItem;
        public static final int[] SuwColoredHeaderMixin;
        public static final int[] SuwDividerItemDecoration;
        public static final int[] SuwExpandableSwitchItem;
        public static final int[] SuwFillContentLayout;
        public static final int[] SuwGlifLayout;
        public static final int[] SuwHeaderMixin;
        public static final int[] SuwHeaderRecyclerView;
        public static final int[] SuwIconMixin;
        public static final int[] SuwIllustration;
        public static final int[] SuwIllustrationVideoView;
        public static final int[] SuwIntrinsicSizeFrameLayout;
        public static final int[] SuwItem;
        public static final int[] SuwListMixin;
        public static final int[] SuwRecyclerItemAdapter;
        public static final int[] SuwRecyclerMixin;
        public static final int[] SuwSetupWizardLayout;
        public static final int[] SuwStatusBarBackgroundLayout;
        public static final int[] SuwStickyHeaderListView;
        public static final int[] SuwSwitchItem;
        public static final int[] SuwTemplateLayout;
        public static final int[] SwitchCompat;
        public static final int[] SwitchPreference;
        public static final int[] SwitchPreferenceCompat;
        public static final int[] TextAppearance;
        @Deprecated
        public static final int[] Toolbar;
        public static final int[] TwoStateButtonPreference;
        public static final int[] UsageView;
        public static final int[] VideoPreference;
        public static final int[] View;
        public static final int[] ViewBackgroundHelper;
        public static final int[] ViewStubCompat;
        public static final int[] WifiEncryptionState;
        public static final int[] WifiMeteredState;
        public static final int[] WifiSavedState;
        public static final int[] WorkPreference;
        
        static {
            ActionBar = new int[] { 2130968640, 2130968641, 2130968642, 2130968704, 2130968705, 2130968706, 2130968707, 2130968708, 2130968709, 2130968722, 2130968738, 2130968739, 2130968755, 2130968804, 2130968805, 2130968806, 2130968807, 2130968809, 2130968821, 2130968828, 2130968856, 2130968870, 2130968894, 2130968915, 2130968916, 2130968978, 2130968983, 2130969077, 2130969088 };
            ActionBarLayout = new int[] { 16842931 };
            ActionMenuItemView = new int[] { 16843071 };
            ActionMenuView = new int[0];
            ActionMode = new int[] { 2130968640, 2130968641, 2130968680, 2130968804, 2130968983, 2130969088 };
            ActivityChooserView = new int[] { 2130968760, 2130968823 };
            AdsAttrs = new int[] { 2130968610, 2130968611, 2130968612 };
            AlertDialog = new int[] { 16842994, 2130968660, 2130968661, 2130968847, 2130968848, 2130968867, 2130968955, 2130968958 };
            AppCompatImageView = new int[] { 16843033, 2130968967, 2130969074, 2130969076 };
            AppCompatSeekBar = new int[] { 16843074, 2130969070, 2130969071, 2130969072 };
            AppCompatTextHelper = new int[] { 16842804, 16843117, 16843118, 16843119, 16843120, 16843666, 16843667 };
            AppCompatTextView = new int[] { 16842804, 2130968635, 2130968636, 2130968637, 2130968638, 2130968639, 2130968777, 2130968780, 2130968835, 2130968844, 2130969049 };
            AppCompatTheme = new int[] { 16842839, 16842926, 2130968576, 2130968577, 2130968578, 2130968579, 2130968580, 2130968581, 2130968582, 2130968583, 2130968584, 2130968585, 2130968586, 2130968587, 2130968588, 2130968590, 2130968591, 2130968592, 2130968593, 2130968594, 2130968595, 2130968596, 2130968597, 2130968598, 2130968599, 2130968600, 2130968601, 2130968602, 2130968603, 2130968604, 2130968605, 2130968606, 2130968609, 2130968614, 2130968615, 2130968616, 2130968617, 2130968632, 2130968652, 2130968654, 2130968655, 2130968656, 2130968657, 2130968658, 2130968663, 2130968664, 2130968676, 2130968677, 2130968684, 2130968685, 2130968686, 2130968687, 2130968688, 2130968689, 2130968690, 2130968691, 2130968692, 2130968694, 2130968716, 2130968729, 2130968734, 2130968735, 2130968740, 2130968742, 2130968748, 2130968749, 2130968751, 2130968752, 2130968754, 2130968806, 2130968819, 2130968845, 2130968846, 2130968849, 2130968850, 2130968851, 2130968852, 2130968853, 2130968854, 2130968855, 2130968885, 2130968886, 2130968887, 2130968893, 2130968895, 2130968919, 2130968920, 2130968921, 2130968922, 2130968934, 2130968943, 2130968945, 2130968946, 2130968964, 2130968965, 2130969045, 2130969050, 2130969051, 2130969052, 2130969053, 2130969054, 2130969055, 2130969056, 2130969057, 2130969059, 2130969060, 2130969090, 2130969091, 2130969092, 2130969093, 2130969107, 2130969113, 2130969114, 2130969115, 2130969116, 2130969117, 2130969118, 2130969119, 2130969120, 2130969121, 2130969122 };
            AppDataSearch = new int[0];
            AspectRatioFrameLayout = new int[] { 2130968631 };
            AssistGestureTrainingProgressBar = new int[] { 16843087 };
            AutoResizeTextView = new int[] { 2130968633, 2130968634 };
            BackgroundStyle = new int[] { 16843534, 2130968945 };
            BatteryHistoryChart = new int[] { 16842804, 16842901, 16842902, 16842903, 16842904, 16843105, 16843106, 16843107, 16843108, 2130968646, 2130968647, 2130968674, 2130968800 };
            ButtonBarLayout = new int[] { 2130968622 };
            ButtonWithMaxTextSize = new int[] { 2130969063 };
            CardView = new int[] { 16843071, 16843072, 2130968667, 2130968668, 2130968669, 2130968670, 2130968671, 2130968672, 2130968710, 2130968711, 2130968712, 2130968713, 2130968714 };
            ChartGridView = new int[] { 16842804, 16842904, 2130968651, 2130968914, 2130968935 };
            ChartNetworkSeriesView = new int[] { 2130968767, 2130968768, 2130968926, 2130968974 };
            ChartSweepView = new int[] { 2130968778, 2130968832, 2130968833, 2130968834, 2130968872, 2130968926, 2130969035 };
            ChartView = new int[] { 2130968875, 2130968876 };
            CheckBoxPreference = new int[] { 16843247, 16843248, 16843249, 2130968737, 2130968987, 2130968988 };
            ColorStateListItem = new int[] { 16843173, 16843551, 2130968623 };
            CompoundButton = new int[] { 16843015, 2130968665, 2130968666 };
            ConversationMessageView = new int[] { 2130968810, 2130968812, 2130968813, 2130968820, 2130968862, 2130969073 };
            CoordinatorLayout = new int[] { 2130968830, 2130968973 };
            CoordinatorLayout_Layout = new int[] { 16842931, 2130968838, 2130968839, 2130968840, 2130968841, 2130968842, 2130968843 };
            Corpus = new int[] { 2130968715, 2130968719, 2130968720, 2130968743, 2130968890, 2130968928, 2130968947, 2130969098 };
            DialogPreference = new int[] { 16843250, 16843251, 16843252, 16843253, 16843254, 16843255, 2130968730, 2130968731, 2130968732, 2130968736, 2130968871, 2130968896 };
            DonutView = new int[] { 2130968628, 2130968863, 2130968864, 2130968952, 2130969066 };
            DotsPageIndicator = new int[] { 2130968626, 2130968721, 2130968744, 2130968745, 2130968884 };
            DrawerArrowToggle = new int[] { 2130968629, 2130968630, 2130968645, 2130968683, 2130968746, 2130968793, 2130968963, 2130969066 };
            FeatureParam = new int[] { 2130968888, 2130968889 };
            FixedLineSummaryPreference = new int[] { 2130968986 };
            FontFamily = new int[] { 2130968781, 2130968782, 2130968783, 2130968784, 2130968785, 2130968786 };
            FontFamilyFont = new int[] { 16844082, 16844083, 16844095, 16844143, 16844144, 2130968779, 2130968787, 2130968788, 2130968789, 2130969099 };
            GlobalSearch = new int[] { 2130968723, 2130968724, 2130968725, 2130968930, 2130968933, 2130968948 };
            GlobalSearchCorpus = new int[] { 2130968621 };
            GlobalSearchSection = new int[] { 2130968936, 2130968939 };
            IMECorpus = new int[] { 2130968825, 2130968961, 2130969089, 2130969103, 2130969104, 2130969105 };
            LinearLayoutCompat = new int[] { 16842927, 16842948, 16843046, 16843047, 16843048, 2130968739, 2130968741, 2130968861, 2130968951 };
            LinearLayoutCompat_Layout = new int[] { 16842931, 16842996, 16842997, 16843137 };
            ListPopupWindow = new int[] { 16843436, 16843437 };
            ListPreference = new int[] { 16842930, 16843256, 2130968758, 2130968759 };
            LoadingImageView = new int[] { 2130968678, 2130968817, 2130968818 };
            MenuGroup = new int[] { 16842766, 16842960, 16843156, 16843230, 16843231, 16843232 };
            MenuItem = new int[] { 16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, 2130968589, 2130968607, 2130968608, 2130968624, 2130968703, 2130968814, 2130968815, 2130968874, 2130968950, 2130969094 };
            MenuView = new int[] { 16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, 2130968912, 2130968975 };
            MultiSelectListPreference = new int[] { 16842930, 16843256, 2130968758, 2130968759 };
            PercentageBarChart = new int[] { 2130968756, 2130968866 };
            PopupWindow = new int[] { 16843126, 16843465, 2130968879 };
            PopupWindowBackgroundState = new int[] { 2130968969 };
            Preference = new int[] { 16842754, 16842765, 16842766, 16842994, 16843233, 16843238, 16843240, 16843241, 16843242, 16843243, 16843244, 16843245, 16843246, 16843491, 16844124, 16844129, 2130968618, 2130968620, 2130968717, 2130968727, 2130968728, 2130968757, 2130968792, 2130968809, 2130968811, 2130968827, 2130968829, 2130968831, 2130968836, 2130968877, 2130968891, 2130968892, 2130968944, 2130968949, 2130968959, 2130968985, 2130969077, 2130969109 };
            PreferenceFragment = new int[] { 16842994, 16843049, 16843050, 2130968619 };
            PreferenceFragmentCompat = new int[] { 16842994, 16843049, 16843050, 2130968619 };
            PreferenceGroup = new int[] { 16843239, 2130968824, 2130968878 };
            PreferenceImageView = new int[] { 16843039, 16843040, 2130968859, 2130968860 };
            PreferenceTheme = new int[] { 2130968675, 2130968733, 2130968750, 2130968753, 2130968897, 2130968899, 2130968900, 2130968901, 2130968902, 2130968903, 2130968904, 2130968905, 2130968906, 2130968907, 2130968908, 2130968909, 2130968910, 2130968911, 2130968925, 2130968942, 2130969043, 2130969044, 2130969123 };
            RecycleListView = new int[] { 2130968880, 2130968883 };
            RecyclerView = new int[] { 16842948, 16842993, 2130968761, 2130968762, 2130968763, 2130968764, 2130968765, 2130968837, 2130968924, 2130968962, 2130968968 };
            RestrictedPreference = new int[] { 2130969102, 2130969106 };
            RestrictedSwitchPreference = new int[] { 2130968923, 2130969101 };
            SearchView = new int[] { 16842970, 16843039, 16843296, 16843364, 2130968679, 2130968700, 2130968726, 2130968794, 2130968816, 2130968836, 2130968917, 2130968918, 2130968931, 2130968932, 2130968976, 2130968984, 2130969108 };
            Section = new int[] { 2130968822, 2130968873, 2130968927, 2130968937, 2130968938, 2130968940, 2130968977 };
            SectionFeature = new int[] { 2130968766 };
            SeekBarPreference = new int[] { 16842994, 16843062, 2130968613, 2130968865, 2130968941, 2130968953 };
            SignInButton = new int[] { 2130968662, 2130968693, 2130968929 };
            SliceView = new int[] { 2130968795, 2130968796, 2130968797, 2130968798, 2130968799, 2130968801, 2130968802, 2130968803, 2130968979, 2130968980, 2130969064, 2130969075, 2130969078, 2130969085 };
            Spinner = new int[] { 16842930, 16843126, 16843131, 16843362, 2130968894 };
            SuwAbstractItem = new int[] { 16842960 };
            SuwButtonItem = new int[] { 16842752, 16842766, 16842824, 16843087 };
            SuwColoredHeaderMixin = new int[] { 2130969015 };
            SuwDividerItemDecoration = new int[] { 16843050, 16843284, 2130969002 };
            SuwExpandableSwitchItem = new int[] { 2130968998, 2130969008 };
            SuwFillContentLayout = new int[] { 16843039, 16843040 };
            SuwGlifLayout = new int[] { 2130968991, 2130968992, 2130968999, 2130969010, 2130969023, 2130969033 };
            SuwHeaderMixin = new int[] { 2130969016 };
            SuwHeaderRecyclerView = new int[] { 2130969014 };
            SuwIconMixin = new int[] { 16842754 };
            SuwIllustration = new int[] { 2130968989 };
            SuwIllustrationVideoView = new int[] { 2130969034 };
            SuwIntrinsicSizeFrameLayout = new int[] { 16843093, 16843097 };
            SuwItem = new int[] { 16842754, 16842766, 16842994, 16843156, 16843233, 16843241 };
            SuwListMixin = new int[] { 16842930, 2130969003, 2130969004, 2130969005 };
            SuwRecyclerItemAdapter = new int[] { 16842801, 16843534, 2130968945 };
            SuwRecyclerMixin = new int[] { 16842930, 2130969003, 2130969004, 2130969005, 2130969013 };
            SuwSetupWizardLayout = new int[] { 2130968990, 2130968993, 2130969001, 2130969017, 2130969018, 2130969019, 2130969020 };
            SuwStatusBarBackgroundLayout = new int[] { 2130969032 };
            SuwStickyHeaderListView = new int[] { 2130969014 };
            SuwSwitchItem = new int[] { 16843014 };
            SuwTemplateLayout = new int[] { 16842994, 2130969000 };
            SwitchCompat = new int[] { 16843044, 16843045, 16843074, 2130968954, 2130968966, 2130969041, 2130969042, 2130969046, 2130969067, 2130969068, 2130969069, 2130969095, 2130969096, 2130969097 };
            SwitchPreference = new int[] { 16843247, 16843248, 16843249, 16843627, 16843628, 2130968737, 2130968987, 2130968988, 2130969047, 2130969048 };
            SwitchPreferenceCompat = new int[] { 16843247, 16843248, 16843249, 16843627, 16843628, 2130968737, 2130968987, 2130968988, 2130969047, 2130969048 };
            TextAppearance = new int[] { 16842901, 16842902, 16842903, 16842904, 16842906, 16842907, 16843105, 16843106, 16843107, 16843108, 16843692, 2130968780, 2130969049 };
            Toolbar = new int[] { 16842927, 16843072, 2130968659, 2130968681, 2130968682, 2130968704, 2130968705, 2130968706, 2130968707, 2130968708, 2130968709, 2130968856, 2130968857, 2130968858, 2130968868, 2130968869, 2130968894, 2130968978, 2130968981, 2130968982, 2130969077, 2130969079, 2130969080, 2130969081, 2130969082, 2130969083, 2130969084, 2130969086, 2130969087 };
            TwoStateButtonPreference = new int[] { 2130969061, 2130969062 };
            UsageView = new int[] { 16842927, 16843829, 2130968653, 2130968956, 2130969058 };
            VideoPreference = new int[] { 2130968625, 2130968913 };
            View = new int[] { 16842752, 16842970, 2130968881, 2130968882, 2130969065 };
            ViewBackgroundHelper = new int[] { 16842964, 2130968643, 2130968644 };
            ViewStubCompat = new int[] { 16842960, 16842994, 16842995 };
            WifiEncryptionState = new int[] { 2130968970 };
            WifiMeteredState = new int[] { 2130968971 };
            WifiSavedState = new int[] { 2130968972 };
            WorkPreference = new int[] { 2130968791 };
        }
    }
    
    public static final class xml
    {
        public static final int timezones = 2132082846;
    }
}
