package com.android.settingslib.graph;

import android.graphics.ColorFilter;
import android.graphics.Path$Op;
import android.graphics.Path$Direction;
import android.graphics.Path.FillType;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.graphics.Paint$Align;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import com.android.settingslib.Utils;
import com.android.settingslib.R;
import android.graphics.Rect;
import android.content.Context;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public final class _$$Lambda$BatteryMeterDrawableBase$ExJ0HHRzS2_LMtcBJqtFiovbn0w implements Runnable
{
    @Override
    public final void run() {
        BatteryMeterDrawableBase.lambda$ExJ0HHRzS2_LMtcBJqtFiovbn0w(this.f$0);
    }
}
