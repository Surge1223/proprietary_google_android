package com.android.settingslib.graph;

import android.graphics.ColorFilter;
import android.graphics.Path$Op;
import android.graphics.Path$Direction;
import android.graphics.Path.FillType;
import android.graphics.Canvas;
import android.content.res.TypedArray;
import android.content.res.Resources;
import android.graphics.Paint$Align;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import com.android.settingslib.Utils;
import com.android.settingslib.R;
import android.graphics.Rect;
import android.content.Context;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public class BatteryMeterDrawableBase extends Drawable
{
    public static final String TAG;
    protected final Paint mBatteryPaint;
    private final RectF mBoltFrame;
    protected final Paint mBoltPaint;
    private final Path mBoltPath;
    private final float[] mBoltPoints;
    private final RectF mButtonFrame;
    protected float mButtonHeightFraction;
    private int mChargeColor;
    private boolean mCharging;
    private final int[] mColors;
    protected final Context mContext;
    private final int mCriticalLevel;
    private final RectF mFrame;
    protected final Paint mFramePaint;
    private int mHeight;
    private int mIconTint;
    private final int mIntrinsicHeight;
    private final int mIntrinsicWidth;
    private int mLevel;
    private float mOldDarkIntensity;
    private final Path mOutlinePath;
    private final Rect mPadding;
    private final RectF mPlusFrame;
    protected final Paint mPlusPaint;
    private final Path mPlusPath;
    private final float[] mPlusPoints;
    protected boolean mPowerSaveAsColorError;
    private boolean mPowerSaveEnabled;
    protected final Paint mPowersavePaint;
    private final Path mShapePath;
    private boolean mShowPercent;
    private float mSubpixelSmoothingLeft;
    private float mSubpixelSmoothingRight;
    private float mTextHeight;
    protected final Paint mTextPaint;
    private final Path mTextPath;
    private String mWarningString;
    private float mWarningTextHeight;
    protected final Paint mWarningTextPaint;
    private int mWidth;
    
    static {
        TAG = BatteryMeterDrawableBase.class.getSimpleName();
    }
    
    public BatteryMeterDrawableBase(final Context mContext, final int color) {
        this.mLevel = -1;
        this.mPowerSaveAsColorError = true;
        this.mIconTint = -1;
        this.mOldDarkIntensity = -1.0f;
        this.mBoltPath = new Path();
        this.mPlusPath = new Path();
        this.mPadding = new Rect();
        this.mFrame = new RectF();
        this.mButtonFrame = new RectF();
        this.mBoltFrame = new RectF();
        this.mPlusFrame = new RectF();
        this.mShapePath = new Path();
        this.mOutlinePath = new Path();
        this.mTextPath = new Path();
        this.mContext = mContext;
        final Resources resources = mContext.getResources();
        final TypedArray obtainTypedArray = resources.obtainTypedArray(R.array.batterymeter_color_levels);
        final TypedArray obtainTypedArray2 = resources.obtainTypedArray(R.array.batterymeter_color_values);
        final int length = obtainTypedArray.length();
        this.mColors = new int[2 * length];
        for (int i = 0; i < length; ++i) {
            this.mColors[2 * i] = obtainTypedArray.getInt(i, 0);
            if (obtainTypedArray2.getType(i) == 2) {
                this.mColors[2 * i + 1] = Utils.getColorAttr(mContext, obtainTypedArray2.getThemeAttributeId(i, 0));
            }
            else {
                this.mColors[2 * i + 1] = obtainTypedArray2.getColor(i, 0);
            }
        }
        obtainTypedArray.recycle();
        obtainTypedArray2.recycle();
        this.mWarningString = mContext.getString(R.string.battery_meter_very_low_overlay_symbol);
        this.mCriticalLevel = this.mContext.getResources().getInteger(17694757);
        this.mButtonHeightFraction = mContext.getResources().getFraction(R.fraction.battery_button_height_fraction, 1, 1);
        this.mSubpixelSmoothingLeft = mContext.getResources().getFraction(R.fraction.battery_subpixel_smoothing_left, 1, 1);
        this.mSubpixelSmoothingRight = mContext.getResources().getFraction(R.fraction.battery_subpixel_smoothing_right, 1, 1);
        (this.mFramePaint = new Paint(1)).setColor(color);
        this.mFramePaint.setDither(true);
        this.mFramePaint.setStrokeWidth(0.0f);
        this.mFramePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        (this.mBatteryPaint = new Paint(1)).setDither(true);
        this.mBatteryPaint.setStrokeWidth(0.0f);
        this.mBatteryPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        (this.mTextPaint = new Paint(1)).setTypeface(Typeface.create("sans-serif-condensed", 1));
        this.mTextPaint.setTextAlign(Paint$Align.CENTER);
        (this.mWarningTextPaint = new Paint(1)).setTypeface(Typeface.create("sans-serif", 1));
        this.mWarningTextPaint.setTextAlign(Paint$Align.CENTER);
        if (this.mColors.length > 1) {
            this.mWarningTextPaint.setColor(this.mColors[1]);
        }
        this.mChargeColor = Utils.getDefaultColor(this.mContext, R.color.meter_consumed_color);
        (this.mBoltPaint = new Paint(1)).setColor(Utils.getDefaultColor(this.mContext, R.color.batterymeter_bolt_color));
        this.mBoltPoints = loadPoints(resources, R.array.batterymeter_bolt_points);
        (this.mPlusPaint = new Paint(1)).setColor(Utils.getDefaultColor(this.mContext, R.color.batterymeter_plus_color));
        this.mPlusPoints = loadPoints(resources, R.array.batterymeter_plus_points);
        (this.mPowersavePaint = new Paint(1)).setColor(this.mPlusPaint.getColor());
        this.mPowersavePaint.setStyle(Paint.Style.STROKE);
        this.mPowersavePaint.setStrokeWidth((float)mContext.getResources().getDimensionPixelSize(R.dimen.battery_powersave_outline_thickness));
        this.mIntrinsicWidth = mContext.getResources().getDimensionPixelSize(R.dimen.battery_width);
        this.mIntrinsicHeight = mContext.getResources().getDimensionPixelSize(R.dimen.battery_height);
    }
    
    private int getColorForLevel(final int n) {
        int n2 = 0;
        int i = 0;
        while (i < this.mColors.length) {
            final int n3 = this.mColors[i];
            n2 = this.mColors[i + 1];
            if (n <= n3) {
                if (i == this.mColors.length - 2) {
                    return this.mIconTint;
                }
                return n2;
            }
            else {
                i += 2;
            }
        }
        return n2;
    }
    
    private static float[] loadPoints(final Resources resources, int max) {
        final int[] intArray = resources.getIntArray(max);
        final int n = 0;
        max = 0;
        int max2 = 0;
        for (int i = 0; i < intArray.length; i += 2) {
            max2 = Math.max(max2, intArray[i]);
            max = Math.max(max, intArray[i + 1]);
        }
        final float[] array = new float[intArray.length];
        for (int j = n; j < intArray.length; j += 2) {
            array[j] = intArray[j] / max2;
            array[j + 1] = intArray[j + 1] / max;
        }
        return array;
    }
    
    private void updateSize() {
        final Rect bounds = this.getBounds();
        this.mHeight = bounds.bottom - this.mPadding.bottom - (bounds.top + this.mPadding.top);
        this.mWidth = bounds.right - this.mPadding.right - (bounds.left + this.mPadding.left);
        this.mWarningTextPaint.setTextSize(this.mHeight * 0.75f);
        this.mWarningTextHeight = -this.mWarningTextPaint.getFontMetrics().ascent;
    }
    
    protected int batteryColorForLevel(int n) {
        if (!this.mCharging && (!this.mPowerSaveEnabled || !this.mPowerSaveAsColorError)) {
            n = this.getColorForLevel(n);
        }
        else {
            n = this.mChargeColor;
        }
        return n;
    }
    
    public void draw(final Canvas canvas) {
        final int mLevel = this.mLevel;
        final Rect bounds = this.getBounds();
        if (mLevel == -1) {
            return;
        }
        float n = mLevel / 100.0f;
        final int mHeight = this.mHeight;
        final int n2 = (int)(this.getAspectRatio() * this.mHeight);
        final int n3 = (this.mWidth - n2) / 2;
        final int round = Math.round(mHeight * this.mButtonHeightFraction);
        final int n4 = this.mPadding.left + bounds.left;
        final int n5 = bounds.bottom - this.mPadding.bottom - mHeight;
        this.mFrame.set((float)n4, (float)n5, (float)(n2 + n4), (float)(mHeight + n5));
        this.mFrame.offset((float)n3, 0.0f);
        this.mButtonFrame.set(this.mFrame.left + Math.round(n2 * 0.28f), this.mFrame.top, this.mFrame.right - Math.round(n2 * 0.28f), this.mFrame.top + round);
        final RectF mFrame = this.mFrame;
        mFrame.top += round;
        this.mBatteryPaint.setColor(this.batteryColorForLevel(mLevel));
        if (mLevel >= 96) {
            n = 1.0f;
        }
        else if (mLevel <= this.mCriticalLevel) {
            n = 0.0f;
        }
        float top;
        if (n == 1.0f) {
            top = this.mButtonFrame.top;
        }
        else {
            top = this.mFrame.top + this.mFrame.height() * (1.0f - n);
        }
        this.mShapePath.reset();
        this.mOutlinePath.reset();
        final float n6 = this.getRadiusRatio() * (this.mFrame.height() + round);
        this.mShapePath.setFillType(Path.FillType.WINDING);
        this.mShapePath.addRoundRect(this.mFrame, n6, n6, Path$Direction.CW);
        this.mShapePath.addRect(this.mButtonFrame, Path$Direction.CW);
        this.mOutlinePath.addRoundRect(this.mFrame, n6, n6, Path$Direction.CW);
        final Path path = new Path();
        path.addRect(this.mButtonFrame, Path$Direction.CW);
        this.mOutlinePath.op(path, Path$Op.XOR);
        final boolean mCharging = this.mCharging;
        final boolean b = true;
        if (mCharging) {
            final float n7 = this.mFrame.left + this.mFrame.width() / 4.0f + 1.0f;
            final float n8 = this.mFrame.top + this.mFrame.height() / 6.0f;
            final float n9 = this.mFrame.right - this.mFrame.width() / 4.0f + 1.0f;
            final float n10 = this.mFrame.bottom - this.mFrame.height() / 10.0f;
            if (this.mBoltFrame.left != n7 || this.mBoltFrame.top != n8 || this.mBoltFrame.right != n9 || this.mBoltFrame.bottom != n10) {
                this.mBoltFrame.set(n7, n8, n9, n10);
                this.mBoltPath.reset();
                this.mBoltPath.moveTo(this.mBoltFrame.left + this.mBoltPoints[0] * this.mBoltFrame.width(), this.mBoltFrame.top + this.mBoltPoints[1] * this.mBoltFrame.height());
                for (int i = 2; i < this.mBoltPoints.length; i += 2) {
                    this.mBoltPath.lineTo(this.mBoltFrame.left + this.mBoltPoints[i] * this.mBoltFrame.width(), this.mBoltFrame.top + this.mBoltPoints[i + 1] * this.mBoltFrame.height());
                }
                this.mBoltPath.lineTo(this.mBoltFrame.left + this.mBoltPoints[0] * this.mBoltFrame.width(), this.mBoltFrame.top + this.mBoltPoints[1] * this.mBoltFrame.height());
            }
            if (Math.min(Math.max((this.mBoltFrame.bottom - top) / (this.mBoltFrame.bottom - this.mBoltFrame.top), 0.0f), 1.0f) <= 0.3f) {
                canvas.drawPath(this.mBoltPath, this.mBoltPaint);
            }
            else {
                this.mShapePath.op(this.mBoltPath, Path$Op.DIFFERENCE);
            }
        }
        else if (this.mPowerSaveEnabled) {
            final float n11 = this.mFrame.width() * 2.0f / 3.0f;
            final float n12 = this.mFrame.left + (this.mFrame.width() - n11) / 2.0f;
            final float n13 = this.mFrame.top + (this.mFrame.height() - n11) / 2.0f;
            final float n14 = this.mFrame.right - (this.mFrame.width() - n11) / 2.0f;
            final float n15 = this.mFrame.bottom - (this.mFrame.height() - n11) / 2.0f;
            if (this.mPlusFrame.left != n12 || this.mPlusFrame.top != n13 || this.mPlusFrame.right != n14 || this.mPlusFrame.bottom != n15) {
                this.mPlusFrame.set(n12, n13, n14, n15);
                this.mPlusPath.reset();
                this.mPlusPath.moveTo(this.mPlusFrame.left + this.mPlusPoints[0] * this.mPlusFrame.width(), this.mPlusFrame.top + this.mPlusPoints[1] * this.mPlusFrame.height());
                for (int j = 2; j < this.mPlusPoints.length; j += 2) {
                    this.mPlusPath.lineTo(this.mPlusFrame.left + this.mPlusPoints[j] * this.mPlusFrame.width(), this.mPlusFrame.top + this.mPlusPoints[j + 1] * this.mPlusFrame.height());
                }
                this.mPlusPath.lineTo(this.mPlusFrame.left + this.mPlusPoints[0] * this.mPlusFrame.width(), this.mPlusFrame.top + this.mPlusPoints[1] * this.mPlusFrame.height());
            }
            this.mShapePath.op(this.mPlusPath, Path$Op.DIFFERENCE);
            if (this.mPowerSaveAsColorError) {
                canvas.drawPath(this.mPlusPath, this.mPlusPaint);
            }
        }
        final boolean b2 = false;
        final float n16 = 0.0f;
        final float n17 = 0.0f;
        final String s = null;
        boolean b3 = b2;
        float n18 = n16;
        float n19 = n17;
        String s2 = s;
        if (!this.mCharging) {
            b3 = b2;
            n18 = n16;
            n19 = n17;
            s2 = s;
            if (!this.mPowerSaveEnabled) {
                b3 = b2;
                n18 = n16;
                n19 = n17;
                s2 = s;
                if (mLevel > this.mCriticalLevel) {
                    b3 = b2;
                    n18 = n16;
                    n19 = n17;
                    s2 = s;
                    if (this.mShowPercent) {
                        this.mTextPaint.setColor(this.getColorForLevel(mLevel));
                        final Paint mTextPaint = this.mTextPaint;
                        final float n20 = mHeight;
                        float n21;
                        if (this.mLevel == 100) {
                            n21 = 0.38f;
                        }
                        else {
                            n21 = 0.5f;
                        }
                        mTextPaint.setTextSize(n20 * n21);
                        this.mTextHeight = -this.mTextPaint.getFontMetrics().ascent;
                        final String value = String.valueOf(mLevel);
                        final float n22 = this.mWidth * 0.5f + n4;
                        final float n23 = (this.mHeight + this.mTextHeight) * 0.47f + n5;
                        final boolean b4 = b3 = (top > n23 && b);
                        n18 = n22;
                        n19 = n23;
                        s2 = value;
                        if (!b4) {
                            this.mTextPath.reset();
                            this.mTextPaint.getTextPath(value, 0, value.length(), n22, n23, this.mTextPath);
                            this.mShapePath.op(this.mTextPath, Path$Op.DIFFERENCE);
                            s2 = value;
                            n19 = n23;
                            n18 = n22;
                            b3 = b4;
                        }
                    }
                }
            }
        }
        canvas.drawPath(this.mShapePath, this.mFramePaint);
        this.mFrame.top = top;
        canvas.save();
        canvas.clipRect(this.mFrame);
        canvas.drawPath(this.mShapePath, this.mBatteryPaint);
        canvas.restore();
        if (!this.mCharging && !this.mPowerSaveEnabled) {
            if (mLevel <= this.mCriticalLevel) {
                canvas.drawText(this.mWarningString, this.mWidth * 0.5f + n4, (this.mHeight + this.mWarningTextHeight) * 0.48f + n5, this.mWarningTextPaint);
            }
            else if (b3) {
                canvas.drawText(s2, n18, n19, this.mTextPaint);
            }
        }
        if (!this.mCharging && this.mPowerSaveEnabled && this.mPowerSaveAsColorError) {
            canvas.drawPath(this.mOutlinePath, this.mPowersavePaint);
        }
    }
    
    protected float getAspectRatio() {
        return 0.58f;
    }
    
    public int getBatteryLevel() {
        return this.mLevel;
    }
    
    public boolean getCharging() {
        return this.mCharging;
    }
    
    public int getCriticalLevel() {
        return this.mCriticalLevel;
    }
    
    public int getIntrinsicHeight() {
        return this.mIntrinsicHeight;
    }
    
    public int getIntrinsicWidth() {
        return this.mIntrinsicWidth;
    }
    
    public int getOpacity() {
        return 0;
    }
    
    public boolean getPadding(final Rect rect) {
        if (this.mPadding.left == 0 && this.mPadding.top == 0 && this.mPadding.right == 0 && this.mPadding.bottom == 0) {
            return super.getPadding(rect);
        }
        rect.set(this.mPadding);
        return true;
    }
    
    protected float getRadiusRatio() {
        return 0.05882353f;
    }
    
    protected void postInvalidate() {
        this.unscheduleSelf((Runnable)new _$$Lambda$BatteryMeterDrawableBase$ExJ0HHRzS2_LMtcBJqtFiovbn0w(this));
        this.scheduleSelf((Runnable)new _$$Lambda$BatteryMeterDrawableBase$ExJ0HHRzS2_LMtcBJqtFiovbn0w(this), 0L);
    }
    
    public void setAlpha(final int n) {
    }
    
    public void setBatteryLevel(final int mLevel) {
        this.mLevel = mLevel;
        this.postInvalidate();
    }
    
    public void setBounds(final int n, final int n2, final int n3, final int n4) {
        super.setBounds(n, n2, n3, n4);
        this.updateSize();
    }
    
    public void setCharging(final boolean mCharging) {
        this.mCharging = mCharging;
        this.postInvalidate();
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
        this.mFramePaint.setColorFilter(colorFilter);
        this.mBatteryPaint.setColorFilter(colorFilter);
        this.mWarningTextPaint.setColorFilter(colorFilter);
        this.mBoltPaint.setColorFilter(colorFilter);
        this.mPlusPaint.setColorFilter(colorFilter);
    }
    
    public void setPadding(final int left, final int top, final int right, final int bottom) {
        this.mPadding.left = left;
        this.mPadding.top = top;
        this.mPadding.right = right;
        this.mPadding.bottom = bottom;
        this.updateSize();
    }
    
    public void setShowPercent(final boolean mShowPercent) {
        this.mShowPercent = mShowPercent;
        this.postInvalidate();
    }
}
