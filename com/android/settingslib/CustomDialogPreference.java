package com.android.settingslib;

import android.os.Bundle;
import android.support.v14.preference.PreferenceDialogFragment;
import android.content.DialogInterface$OnClickListener;
import android.app.AlertDialog$Builder;
import android.content.DialogInterface;
import android.view.View;
import android.app.Dialog;
import android.util.AttributeSet;
import android.content.Context;
import android.content.DialogInterface$OnShowListener;
import android.support.v7.preference.DialogPreference;

public class CustomDialogPreference extends DialogPreference
{
    private CustomPreferenceDialogFragment mFragment;
    private DialogInterface$OnShowListener mOnShowListener;
    
    public CustomDialogPreference(final Context context) {
        super(context);
    }
    
    public CustomDialogPreference(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public CustomDialogPreference(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public CustomDialogPreference(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
    }
    
    private DialogInterface$OnShowListener getOnShowListener() {
        return this.mOnShowListener;
    }
    
    private void setFragment(final CustomPreferenceDialogFragment mFragment) {
        this.mFragment = mFragment;
    }
    
    public Dialog getDialog() {
        Dialog dialog;
        if (this.mFragment != null) {
            dialog = this.mFragment.getDialog();
        }
        else {
            dialog = null;
        }
        return dialog;
    }
    
    protected void onBindDialogView(final View view) {
    }
    
    protected void onClick(final DialogInterface dialogInterface, final int n) {
    }
    
    protected void onDialogClosed(final boolean b) {
    }
    
    protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder, final DialogInterface$OnClickListener dialogInterface$OnClickListener) {
    }
    
    public static class CustomPreferenceDialogFragment extends PreferenceDialogFragment
    {
        private CustomDialogPreference getCustomizablePreference() {
            return (CustomDialogPreference)this.getPreference();
        }
        
        public static CustomPreferenceDialogFragment newInstance(final String s) {
            final CustomPreferenceDialogFragment customPreferenceDialogFragment = new CustomPreferenceDialogFragment();
            final Bundle arguments = new Bundle(1);
            arguments.putString("key", s);
            customPreferenceDialogFragment.setArguments(arguments);
            return customPreferenceDialogFragment;
        }
        
        @Override
        protected void onBindDialogView(final View view) {
            super.onBindDialogView(view);
            this.getCustomizablePreference().onBindDialogView(view);
        }
        
        @Override
        public void onClick(final DialogInterface dialogInterface, final int n) {
            super.onClick(dialogInterface, n);
            this.getCustomizablePreference().onClick(dialogInterface, n);
        }
        
        @Override
        public Dialog onCreateDialog(final Bundle bundle) {
            final Dialog onCreateDialog = super.onCreateDialog(bundle);
            onCreateDialog.setOnShowListener(this.getCustomizablePreference().getOnShowListener());
            return onCreateDialog;
        }
        
        @Override
        public void onDialogClosed(final boolean b) {
            this.getCustomizablePreference().onDialogClosed(b);
        }
        
        @Override
        protected void onPrepareDialogBuilder(final AlertDialog$Builder alertDialog$Builder) {
            super.onPrepareDialogBuilder(alertDialog$Builder);
            this.getCustomizablePreference().setFragment(this);
            this.getCustomizablePreference().onPrepareDialogBuilder(alertDialog$Builder, (DialogInterface$OnClickListener)this);
        }
    }
}
