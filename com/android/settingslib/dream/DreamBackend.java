package com.android.settingslib.dream;

import android.graphics.drawable.Drawable;
import java.util.Iterator;
import java.util.Comparator;
import java.util.Collections;
import java.util.ArrayList;
import android.content.Intent;
import java.util.List;
import android.content.pm.ServiceInfo;
import android.os.RemoteException;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import android.content.pm.PackageManager;
import com.android.internal.R$styleable;
import org.xmlpull.v1.XmlPullParser;
import android.util.Xml;
import android.util.Log;
import android.content.pm.PackageManager;
import android.content.ComponentName;
import android.content.pm.ResolveInfo;
import android.provider.Settings;
import android.service.dreams.IDreamManager$Stub;
import android.os.ServiceManager;
import android.service.dreams.IDreamManager;
import android.content.Context;

public class DreamBackend
{
    private static DreamBackend sInstance;
    private final DreamInfoComparator mComparator;
    private final Context mContext;
    private final IDreamManager mDreamManager;
    private final boolean mDreamsActivatedOnDockByDefault;
    private final boolean mDreamsActivatedOnSleepByDefault;
    private final boolean mDreamsEnabledByDefault;
    
    public DreamBackend(final Context context) {
        this.mContext = context.getApplicationContext();
        this.mDreamManager = IDreamManager$Stub.asInterface(ServiceManager.getService("dreams"));
        this.mComparator = new DreamInfoComparator(this.getDefaultDream());
        this.mDreamsEnabledByDefault = this.mContext.getResources().getBoolean(17956943);
        this.mDreamsActivatedOnSleepByDefault = this.mContext.getResources().getBoolean(17956942);
        this.mDreamsActivatedOnDockByDefault = this.mContext.getResources().getBoolean(17956941);
    }
    
    private boolean getBoolean(final String s, final boolean b) {
        final int int1 = Settings.Secure.getInt(this.mContext.getContentResolver(), s, (int)(b ? 1 : 0));
        boolean b2 = true;
        if (int1 != 1) {
            b2 = false;
        }
        return b2;
    }
    
    private static ComponentName getDreamComponentName(final ResolveInfo resolveInfo) {
        if (resolveInfo != null && resolveInfo.serviceInfo != null) {
            return new ComponentName(resolveInfo.serviceInfo.packageName, resolveInfo.serviceInfo.name);
        }
        return null;
    }
    
    public static DreamBackend getInstance(final Context context) {
        if (DreamBackend.sInstance == null) {
            DreamBackend.sInstance = new DreamBackend(context);
        }
        return DreamBackend.sInstance;
    }
    
    private static ComponentName getSettingsComponentName(final PackageManager packageManager, final ResolveInfo resolveInfo) {
        final ComponentName componentName = null;
        if (resolveInfo == null || resolveInfo.serviceInfo == null || resolveInfo.serviceInfo.metaData == null) {
            return null;
        }
        final Object o = null;
        Object o2 = null;
        Object o3 = null;
        final Throwable t = null;
        final Throwable t2 = null;
        while (true) {
            try {
                final XmlResourceParser loadXmlMetaData = resolveInfo.serviceInfo.loadXmlMetaData(packageManager, "android.service.dream");
                if (loadXmlMetaData == null) {
                    o3 = loadXmlMetaData;
                    o2 = loadXmlMetaData;
                    Log.w("DreamBackend", "No android.service.dream meta-data");
                    return null;
                }
                o3 = loadXmlMetaData;
                o2 = loadXmlMetaData;
                final Resources resourcesForApplication = packageManager.getResourcesForApplication(resolveInfo.serviceInfo.applicationInfo);
                o3 = loadXmlMetaData;
                o2 = loadXmlMetaData;
                final AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)loadXmlMetaData);
                int next;
                do {
                    o3 = loadXmlMetaData;
                    o2 = loadXmlMetaData;
                    next = loadXmlMetaData.next();
                } while (next != 1 && next != 2);
                o3 = loadXmlMetaData;
                o2 = loadXmlMetaData;
                if (!"dream".equals(loadXmlMetaData.getName())) {
                    o3 = loadXmlMetaData;
                    o2 = loadXmlMetaData;
                    Log.w("DreamBackend", "Meta-data does not start with dream tag");
                    return null;
                }
                o3 = loadXmlMetaData;
                o2 = loadXmlMetaData;
                final TypedArray obtainAttributes = resourcesForApplication.obtainAttributes(attributeSet, R$styleable.Dream);
                o3 = loadXmlMetaData;
                o2 = loadXmlMetaData;
                final String string = obtainAttributes.getString(0);
                o3 = loadXmlMetaData;
                o2 = loadXmlMetaData;
                obtainAttributes.recycle();
                o3 = t;
                if (loadXmlMetaData != null) {
                    o3 = t2;
                    o2 = loadXmlMetaData;
                    ((XmlResourceParser)o2).close();
                }
            }
            catch (PackageManager$NameNotFoundException ex2) {}
            catch (IOException ex3) {}
            catch (XmlPullParserException ex) {
                o3 = ex;
                if (o2 != null) {
                    o3 = ex;
                    continue;
                }
            }
            finally {
                if (o3 != null) {
                    ((XmlResourceParser)o3).close();
                }
            }
            break;
        }
        if (o3 != null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Error parsing : ");
            sb.append(resolveInfo.serviceInfo.packageName);
            Log.w("DreamBackend", sb.toString(), (Throwable)o3);
            return null;
        }
        final String s;
        String string2;
        if ((string2 = s) != null) {
            string2 = s;
            if (s.indexOf(47) < 0) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append(resolveInfo.serviceInfo.packageName);
                sb2.append("/");
                sb2.append(s);
                string2 = sb2.toString();
            }
        }
        ComponentName unflattenFromString;
        if (string2 == null) {
            unflattenFromString = componentName;
        }
        else {
            unflattenFromString = ComponentName.unflattenFromString(string2);
        }
        return unflattenFromString;
    }
    
    private static void logd(final String s, final Object... array) {
    }
    
    private void setBoolean(final String s, final boolean b) {
        Settings.Secure.putInt(this.mContext.getContentResolver(), s, (int)(b ? 1 : 0));
    }
    
    public ComponentName getActiveDream() {
        final IDreamManager mDreamManager = this.mDreamManager;
        final ComponentName componentName = null;
        if (mDreamManager == null) {
            return null;
        }
        try {
            final ComponentName[] dreamComponents = this.mDreamManager.getDreamComponents();
            ComponentName componentName2 = componentName;
            if (dreamComponents != null) {
                componentName2 = componentName;
                if (dreamComponents.length > 0) {
                    componentName2 = dreamComponents[0];
                }
            }
            return componentName2;
        }
        catch (RemoteException ex) {
            Log.w("DreamBackend", "Failed to get active dream", (Throwable)ex);
            return null;
        }
    }
    
    public CharSequence getActiveDreamName() {
        final ComponentName activeDream = this.getActiveDream();
        if (activeDream != null) {
            final PackageManager packageManager = this.mContext.getPackageManager();
            try {
                final ServiceInfo serviceInfo = packageManager.getServiceInfo(activeDream, 0);
                if (serviceInfo != null) {
                    return serviceInfo.loadLabel(packageManager);
                }
            }
            catch (PackageManager$NameNotFoundException ex) {
                return null;
            }
        }
        return null;
    }
    
    public ComponentName getDefaultDream() {
        if (this.mDreamManager == null) {
            return null;
        }
        try {
            return this.mDreamManager.getDefaultDreamComponent();
        }
        catch (RemoteException ex) {
            Log.w("DreamBackend", "Failed to get default dream", (Throwable)ex);
            return null;
        }
    }
    
    public List<DreamInfo> getDreamInfos() {
        logd("getDreamInfos()", new Object[0]);
        final ComponentName activeDream = this.getActiveDream();
        final PackageManager packageManager = this.mContext.getPackageManager();
        final List queryIntentServices = packageManager.queryIntentServices(new Intent("android.service.dreams.DreamService"), 128);
        final ArrayList list = new ArrayList<Object>(queryIntentServices.size());
        for (final ResolveInfo resolveInfo : queryIntentServices) {
            if (resolveInfo.serviceInfo == null) {
                continue;
            }
            final DreamInfo dreamInfo = new DreamInfo();
            dreamInfo.caption = resolveInfo.loadLabel(packageManager);
            dreamInfo.icon = resolveInfo.loadIcon(packageManager);
            dreamInfo.componentName = getDreamComponentName(resolveInfo);
            dreamInfo.isActive = dreamInfo.componentName.equals((Object)activeDream);
            dreamInfo.settingsComponentName = getSettingsComponentName(packageManager, resolveInfo);
            list.add(dreamInfo);
        }
        Collections.sort((List<Object>)list, (Comparator<? super Object>)this.mComparator);
        return (List<DreamInfo>)list;
    }
    
    public int getWhenToDreamSetting() {
        final boolean enabled = this.isEnabled();
        int n = 3;
        if (!enabled) {
            return 3;
        }
        if (this.isActivatedOnDock() && this.isActivatedOnSleep()) {
            n = 2;
        }
        else if (this.isActivatedOnDock()) {
            n = 1;
        }
        else if (this.isActivatedOnSleep()) {
            n = 0;
        }
        return n;
    }
    
    public boolean isActivatedOnDock() {
        return this.getBoolean("screensaver_activate_on_dock", this.mDreamsActivatedOnDockByDefault);
    }
    
    public boolean isActivatedOnSleep() {
        return this.getBoolean("screensaver_activate_on_sleep", this.mDreamsActivatedOnSleepByDefault);
    }
    
    public boolean isEnabled() {
        return this.getBoolean("screensaver_enabled", this.mDreamsEnabledByDefault);
    }
    
    public void launchSettings(final Context context, final DreamInfo dreamInfo) {
        logd("launchSettings(%s)", dreamInfo);
        if (dreamInfo != null && dreamInfo.settingsComponentName != null) {
            context.startActivity(new Intent().setComponent(dreamInfo.settingsComponentName));
        }
    }
    
    public void setActivatedOnDock(final boolean b) {
        logd("setActivatedOnDock(%s)", b);
        this.setBoolean("screensaver_activate_on_dock", b);
    }
    
    public void setActivatedOnSleep(final boolean b) {
        logd("setActivatedOnSleep(%s)", b);
        this.setBoolean("screensaver_activate_on_sleep", b);
    }
    
    public void setActiveDream(final ComponentName componentName) {
        logd("setActiveDream(%s)", componentName);
        if (this.mDreamManager == null) {
            return;
        }
        try {
            final IDreamManager mDreamManager = this.mDreamManager;
            ComponentName[] dreamComponents;
            if (componentName == null) {
                dreamComponents = null;
            }
            else {
                dreamComponents = new ComponentName[] { componentName };
            }
            mDreamManager.setDreamComponents(dreamComponents);
        }
        catch (RemoteException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Failed to set active dream to ");
            sb.append(componentName);
            Log.w("DreamBackend", sb.toString(), (Throwable)ex);
        }
    }
    
    public void setEnabled(final boolean b) {
        logd("setEnabled(%s)", b);
        this.setBoolean("screensaver_enabled", b);
    }
    
    public void setWhenToDream(final int n) {
        this.setEnabled(n != 3);
        switch (n) {
            case 2: {
                this.setActivatedOnDock(true);
                this.setActivatedOnSleep(true);
                break;
            }
            case 1: {
                this.setActivatedOnDock(true);
                this.setActivatedOnSleep(false);
                break;
            }
            case 0: {
                this.setActivatedOnDock(false);
                this.setActivatedOnSleep(true);
                break;
            }
        }
    }
    
    public void startDreaming() {
        logd("startDreaming()", new Object[0]);
        if (this.mDreamManager == null) {
            return;
        }
        try {
            this.mDreamManager.dream();
        }
        catch (RemoteException ex) {
            Log.w("DreamBackend", "Failed to dream", (Throwable)ex);
        }
    }
    
    public static class DreamInfo
    {
        public CharSequence caption;
        public ComponentName componentName;
        public Drawable icon;
        public boolean isActive;
        public ComponentName settingsComponentName;
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder(DreamInfo.class.getSimpleName());
            sb.append('[');
            sb.append(this.caption);
            if (this.isActive) {
                sb.append(",active");
            }
            sb.append(',');
            sb.append(this.componentName);
            if (this.settingsComponentName != null) {
                sb.append("settings=");
                sb.append(this.settingsComponentName);
            }
            sb.append(']');
            return sb.toString();
        }
    }
    
    private static class DreamInfoComparator implements Comparator<DreamInfo>
    {
        private final ComponentName mDefaultDream;
        
        public DreamInfoComparator(final ComponentName mDefaultDream) {
            this.mDefaultDream = mDefaultDream;
        }
        
        private String sortKey(final DreamInfo dreamInfo) {
            final StringBuilder sb = new StringBuilder();
            char c;
            if (dreamInfo.componentName.equals((Object)this.mDefaultDream)) {
                c = '0';
            }
            else {
                c = '1';
            }
            sb.append(c);
            sb.append(dreamInfo.caption);
            return sb.toString();
        }
        
        @Override
        public int compare(final DreamInfo dreamInfo, final DreamInfo dreamInfo2) {
            return this.sortKey(dreamInfo).compareTo(this.sortKey(dreamInfo2));
        }
    }
}
