package com.android.settingslib;

import com.android.settingslib.wrapper.LocationManagerWrapper;
import android.location.LocationManager;
import android.os.UserHandle;
import android.content.ContentResolver;
import android.provider.Settings;
import android.os.SystemProperties;
import android.media.AudioManager;
import android.graphics.Bitmap;
import com.android.internal.util.UserIcons;
import com.android.settingslib.drawable.UserIconDrawable;
import android.graphics.drawable.Drawable;
import android.content.pm.UserInfo;
import android.os.UserManager;
import android.net.ConnectivityManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager;
import android.content.pm.PackageInfo;
import android.content.res.Resources$NotFoundException;
import android.content.res.TypedArray;
import android.content.Context;
import android.content.res.Resources;
import android.content.Intent;
import java.text.NumberFormat;
import android.content.pm.Signature;
import com.android.internal.annotations.VisibleForTesting;

public class Utils
{
    @VisibleForTesting
    static final String STORAGE_MANAGER_ENABLED_PROPERTY = "ro.storage_manager.enabled";
    static final int[] WIFI_PIE;
    private static String sPermissionControllerPackageName;
    private static String sServicesSystemSharedLibPackageName;
    private static String sSharedSystemSharedLibPackageName;
    private static Signature[] sSystemSignature;
    
    static {
        WIFI_PIE = new int[] { 17302781, 17302782, 17302783, 17302784, 17302785 };
    }
    
    public static String formatPercentage(final double n) {
        return NumberFormat.getPercentInstance().format(n);
    }
    
    public static String formatPercentage(final double n, final boolean b) {
        int round;
        if (b) {
            round = Math.round((float)n);
        }
        else {
            round = (int)n;
        }
        return formatPercentage(round);
    }
    
    public static String formatPercentage(final int n) {
        return formatPercentage(n / 100.0);
    }
    
    public static String formatPercentage(final long n, final long n2) {
        return formatPercentage(n / n2);
    }
    
    public static int getBatteryLevel(final Intent intent) {
        return intent.getIntExtra("level", 0) * 100 / intent.getIntExtra("scale", 100);
    }
    
    public static String getBatteryStatus(final Resources resources, final Intent intent) {
        final int intExtra = intent.getIntExtra("status", 1);
        String s;
        if (intExtra == 2) {
            s = resources.getString(R.string.battery_info_status_charging);
        }
        else if (intExtra == 3) {
            s = resources.getString(R.string.battery_info_status_discharging);
        }
        else if (intExtra == 4) {
            s = resources.getString(R.string.battery_info_status_not_charging);
        }
        else if (intExtra == 5) {
            s = resources.getString(R.string.battery_info_status_full);
        }
        else {
            s = resources.getString(R.string.battery_info_status_unknown);
        }
        return s;
    }
    
    public static int getColorAccent(final Context context) {
        return getColorAttr(context, 16843829);
    }
    
    public static int getColorAttr(final Context context, int color) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { color });
        color = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        return color;
    }
    
    public static int getDefaultColor(final Context context, final int n) {
        return context.getResources().getColorStateList(n, context.getTheme()).getDefaultColor();
    }
    
    public static int getDefaultStorageManagerDaysToRetain(final Resources resources) {
        int integer = 90;
        try {
            integer = resources.getInteger(17694870);
        }
        catch (Resources$NotFoundException ex) {}
        return integer;
    }
    
    private static Signature getFirstSignature(final PackageInfo packageInfo) {
        if (packageInfo != null && packageInfo.signatures != null && packageInfo.signatures.length > 0) {
            return packageInfo.signatures[0];
        }
        return null;
    }
    
    private static Signature getSystemSignature(final PackageManager packageManager) {
        try {
            return getFirstSignature(packageManager.getPackageInfo("android", 64));
        }
        catch (PackageManager$NameNotFoundException ex) {
            return null;
        }
    }
    
    public static int getTetheringLabel(final ConnectivityManager connectivityManager) {
        final String[] tetherableUsbRegexs = connectivityManager.getTetherableUsbRegexs();
        final String[] tetherableWifiRegexs = connectivityManager.getTetherableWifiRegexs();
        final String[] tetherableBluetoothRegexs = connectivityManager.getTetherableBluetoothRegexs();
        final int length = tetherableUsbRegexs.length;
        boolean b = false;
        final boolean b2 = length != 0;
        final boolean b3 = tetherableWifiRegexs.length != 0;
        if (tetherableBluetoothRegexs.length != 0) {
            b = true;
        }
        if (b3 && b2 && b) {
            return R.string.tether_settings_title_all;
        }
        if (b3 && b2) {
            return R.string.tether_settings_title_all;
        }
        if (b3 && b) {
            return R.string.tether_settings_title_all;
        }
        if (b3) {
            return R.string.tether_settings_title_wifi;
        }
        if (b2 && b) {
            return R.string.tether_settings_title_usb_bluetooth;
        }
        if (b2) {
            return R.string.tether_settings_title_usb;
        }
        return R.string.tether_settings_title_bluetooth;
    }
    
    public static Drawable getUserIcon(final Context context, final UserManager userManager, final UserInfo userInfo) {
        final int sizeForList = UserIconDrawable.getSizeForList(context);
        if (userInfo.isManagedProfile()) {
            final Drawable managedUserDrawable = UserIconDrawable.getManagedUserDrawable(context);
            managedUserDrawable.setBounds(0, 0, sizeForList, sizeForList);
            return managedUserDrawable;
        }
        if (userInfo.iconPath != null) {
            final Bitmap userIcon = userManager.getUserIcon(userInfo.id);
            if (userIcon != null) {
                return new UserIconDrawable(sizeForList).setIcon(userIcon).bake();
            }
        }
        return new UserIconDrawable(sizeForList).setIconDrawable(UserIcons.getDefaultUserIcon(context.getResources(), userInfo.id, false)).bake();
    }
    
    public static String getUserLabel(final Context context, final UserInfo userInfo) {
        String s;
        if (userInfo != null) {
            s = userInfo.name;
        }
        else {
            s = null;
        }
        if (userInfo.isManagedProfile()) {
            return context.getString(R.string.managed_user_title);
        }
        if (userInfo.isGuest()) {
            s = context.getString(R.string.user_guest);
        }
        if (s == null && userInfo != null) {
            s = Integer.toString(userInfo.id);
        }
        else if (userInfo == null) {
            s = context.getString(R.string.unknown);
        }
        return context.getResources().getString(R.string.running_process_item_user_label, new Object[] { s });
    }
    
    public static int getWifiIconResource(final int n) {
        if (n >= 0 && n < Utils.WIFI_PIE.length) {
            return Utils.WIFI_PIE[n];
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No Wifi icon found for level: ");
        sb.append(n);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static boolean isAudioModeOngoingCall(final Context context) {
        final int mode = ((AudioManager)context.getSystemService((Class)AudioManager.class)).getMode();
        boolean b2;
        final boolean b = b2 = true;
        if (mode != 1) {
            b2 = b;
            if (mode != 2) {
                b2 = (mode == 3 && b);
            }
        }
        return b2;
    }
    
    public static boolean isDeviceProvisioningPackage(final Resources resources, final String s) {
        final String string = resources.getString(17039662);
        return string != null && string.equals(s);
    }
    
    public static boolean isStorageManagerEnabled(final Context context) {
        final boolean b = false;
        boolean boolean1;
        try {
            boolean1 = SystemProperties.getBoolean("ro.storage_manager.enabled", false);
        }
        catch (Resources$NotFoundException ex) {
            boolean1 = false;
        }
        final ContentResolver contentResolver = context.getContentResolver();
        int n;
        if (boolean1) {
            n = 1;
        }
        else {
            n = 0;
        }
        boolean b2 = b;
        if (Settings.Secure.getInt(contentResolver, "automatic_storage_manager_enabled", n) != 0) {
            b2 = true;
        }
        return b2;
    }
    
    public static boolean isSystemPackage(final Resources resources, final PackageManager packageManager, final PackageInfo packageInfo) {
        final Signature[] sSystemSignature = Utils.sSystemSignature;
        boolean b = true;
        if (sSystemSignature == null) {
            Utils.sSystemSignature = new Signature[] { getSystemSignature(packageManager) };
        }
        if (Utils.sPermissionControllerPackageName == null) {
            Utils.sPermissionControllerPackageName = packageManager.getPermissionControllerPackageName();
        }
        if (Utils.sServicesSystemSharedLibPackageName == null) {
            Utils.sServicesSystemSharedLibPackageName = packageManager.getServicesSystemSharedLibraryPackageName();
        }
        if (Utils.sSharedSystemSharedLibPackageName == null) {
            Utils.sSharedSystemSharedLibPackageName = packageManager.getSharedSystemSharedLibraryPackageName();
        }
        if ((Utils.sSystemSignature[0] == null || !Utils.sSystemSignature[0].equals((Object)getFirstSignature(packageInfo))) && !packageInfo.packageName.equals(Utils.sPermissionControllerPackageName) && !packageInfo.packageName.equals(Utils.sServicesSystemSharedLibPackageName) && !packageInfo.packageName.equals(Utils.sSharedSystemSharedLibPackageName) && !packageInfo.packageName.equals("com.android.printspooler")) {
            if (!isDeviceProvisioningPackage(resources, packageInfo.packageName)) {
                b = false;
            }
        }
        return b;
    }
    
    public static boolean isWifiOnly(final Context context) {
        return ((ConnectivityManager)context.getSystemService((Class)ConnectivityManager.class)).isNetworkSupported(0) ^ true;
    }
    
    public static void updateLocationEnabled(final Context context, final boolean b, final int n, int n2) {
        Settings.Secure.putIntForUser(context.getContentResolver(), "location_changer", n2, n);
        final Intent intent = new Intent("com.android.settings.location.MODE_CHANGING");
        final ContentResolver contentResolver = context.getContentResolver();
        n2 = 0;
        final int intForUser = Settings.Secure.getIntForUser(contentResolver, "location_mode", 0, n);
        if (b) {
            n2 = 3;
        }
        intent.putExtra("CURRENT_MODE", intForUser);
        intent.putExtra("NEW_MODE", n2);
        context.sendBroadcastAsUser(intent, UserHandle.of(n), "android.permission.WRITE_SECURE_SETTINGS");
        new LocationManagerWrapper((LocationManager)context.getSystemService("location")).setLocationEnabledForUser(b, UserHandle.of(n));
    }
}
