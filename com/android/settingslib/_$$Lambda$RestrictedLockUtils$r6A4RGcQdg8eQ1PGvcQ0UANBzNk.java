package com.android.settingslib;

import java.util.Objects;
import android.widget.TextView;
import android.view.MenuItem$OnMenuItemClickListener;
import android.view.MenuItem;
import android.text.style.ForegroundColorSpan;
import android.text.SpannableStringBuilder;
import android.os.Parcelable;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.UserManager$EnforcingUser;
import android.os.UserHandle;
import android.content.pm.IPackageManager;
import android.os.RemoteException;
import android.app.AppGlobals;
import java.util.List;
import java.util.Iterator;
import android.content.pm.UserInfo;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import android.content.Context;
import android.content.ComponentName;
import android.app.admin.DevicePolicyManager;

public final class _$$Lambda$RestrictedLockUtils$r6A4RGcQdg8eQ1PGvcQ0UANBzNk implements LockSettingCheck
{
    @Override
    public final boolean isEnforcing(final DevicePolicyManager devicePolicyManager, final ComponentName componentName, final int n) {
        return RestrictedLockUtils.lambda$checkIfKeyguardFeaturesDisabled$0(this.f$0, this.f$1, devicePolicyManager, componentName, n);
    }
}
