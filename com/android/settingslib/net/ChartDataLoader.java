package com.android.settingslib.net;

import android.os.RemoteException;
import android.net.NetworkStatsHistory;
import android.os.Parcelable;
import com.android.settingslib.AppItem;
import android.net.NetworkTemplate;
import android.content.Context;
import android.net.INetworkStatsSession;
import android.os.Bundle;
import android.content.AsyncTaskLoader;

public class ChartDataLoader extends AsyncTaskLoader<ChartData>
{
    private final Bundle mArgs;
    private final INetworkStatsSession mSession;
    
    public ChartDataLoader(final Context context, final INetworkStatsSession mSession, final Bundle mArgs) {
        super(context);
        this.mSession = mSession;
        this.mArgs = mArgs;
    }
    
    public static Bundle buildArgs(final NetworkTemplate networkTemplate, final AppItem appItem) {
        return buildArgs(networkTemplate, appItem, 10);
    }
    
    public static Bundle buildArgs(final NetworkTemplate networkTemplate, final AppItem appItem, final int n) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("template", (Parcelable)networkTemplate);
        bundle.putParcelable("app", (Parcelable)appItem);
        bundle.putInt("fields", n);
        return bundle;
    }
    
    private NetworkStatsHistory collectHistoryForUid(final NetworkTemplate networkTemplate, final int n, final int n2, final NetworkStatsHistory networkStatsHistory) throws RemoteException {
        final NetworkStatsHistory historyForUid = this.mSession.getHistoryForUid(networkTemplate, n, n2, 0, 10);
        if (networkStatsHistory != null) {
            networkStatsHistory.recordEntireHistory(historyForUid);
            return networkStatsHistory;
        }
        return historyForUid;
    }
    
    private ChartData loadInBackground(final NetworkTemplate networkTemplate, final AppItem appItem, int i) throws RemoteException {
        final ChartData chartData = new ChartData();
        chartData.network = this.mSession.getHistoryForNetwork(networkTemplate, i);
        if (appItem != null) {
            int size;
            int key;
            for (size = appItem.uids.size(), i = 0; i < size; ++i) {
                key = appItem.uids.keyAt(i);
                chartData.detailDefault = this.collectHistoryForUid(networkTemplate, key, 0, chartData.detailDefault);
                chartData.detailForeground = this.collectHistoryForUid(networkTemplate, key, 1, chartData.detailForeground);
            }
            if (size > 0) {
                (chartData.detail = new NetworkStatsHistory(chartData.detailForeground.getBucketDuration())).recordEntireHistory(chartData.detailDefault);
                chartData.detail.recordEntireHistory(chartData.detailForeground);
            }
            else {
                chartData.detailDefault = new NetworkStatsHistory(3600000L);
                chartData.detailForeground = new NetworkStatsHistory(3600000L);
                chartData.detail = new NetworkStatsHistory(3600000L);
            }
        }
        return chartData;
    }
    
    public ChartData loadInBackground() {
        final NetworkTemplate networkTemplate = (NetworkTemplate)this.mArgs.getParcelable("template");
        final AppItem appItem = (AppItem)this.mArgs.getParcelable("app");
        final int int1 = this.mArgs.getInt("fields");
        try {
            return this.loadInBackground(networkTemplate, appItem, int1);
        }
        catch (RemoteException ex) {
            throw new RuntimeException("problem reading network stats", (Throwable)ex);
        }
    }
    
    protected void onReset() {
        super.onReset();
        this.cancelLoad();
    }
    
    protected void onStartLoading() {
        super.onStartLoading();
        this.forceLoad();
    }
    
    protected void onStopLoading() {
        super.onStopLoading();
        this.cancelLoad();
    }
}
