package com.android.settingslib.net;

import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import android.net.NetworkStatsHistory;
import java.util.Date;
import java.time.ZonedDateTime;
import android.util.Pair;
import android.net.NetworkStatsHistory$Entry;
import android.os.RemoteException;
import android.telephony.SubscriptionManager;
import android.text.format.DateUtils;
import android.net.NetworkPolicy;
import android.net.NetworkTemplate;
import android.net.INetworkStatsService$Stub;
import android.os.ServiceManager;
import java.util.Locale;
import android.util.Log;
import android.telephony.TelephonyManager;
import android.net.INetworkStatsService;
import android.net.INetworkStatsSession;
import android.net.NetworkPolicyManager;
import android.content.Context;
import android.net.ConnectivityManager;
import java.util.Formatter;

public class DataUsageController
{
    private static final boolean DEBUG;
    private static final StringBuilder PERIOD_BUILDER;
    private static final Formatter PERIOD_FORMATTER;
    private Callback mCallback;
    private final ConnectivityManager mConnectivityManager;
    private final Context mContext;
    private NetworkNameProvider mNetworkController;
    private final NetworkPolicyManager mPolicyManager;
    private INetworkStatsSession mSession;
    private final INetworkStatsService mStatsService;
    private final TelephonyManager mTelephonyManager;
    
    static {
        DEBUG = Log.isLoggable("DataUsageController", 3);
        PERIOD_BUILDER = new StringBuilder(50);
        PERIOD_FORMATTER = new Formatter(DataUsageController.PERIOD_BUILDER, Locale.getDefault());
    }
    
    public DataUsageController(final Context mContext) {
        this.mContext = mContext;
        this.mTelephonyManager = TelephonyManager.from(mContext);
        this.mConnectivityManager = ConnectivityManager.from(mContext);
        this.mStatsService = INetworkStatsService$Stub.asInterface(ServiceManager.getService("netstats"));
        this.mPolicyManager = NetworkPolicyManager.from(this.mContext);
    }
    
    private NetworkPolicy findNetworkPolicy(final NetworkTemplate networkTemplate) {
        if (this.mPolicyManager == null || networkTemplate == null) {
            return null;
        }
        final NetworkPolicy[] networkPolicies = this.mPolicyManager.getNetworkPolicies();
        if (networkPolicies == null) {
            return null;
        }
        for (final NetworkPolicy networkPolicy : networkPolicies) {
            if (networkPolicy != null && networkTemplate.equals((Object)networkPolicy.template)) {
                return networkPolicy;
            }
        }
        return null;
    }
    
    private String formatDateRange(final long n, final long n2) {
        final StringBuilder period_BUILDER = DataUsageController.PERIOD_BUILDER;
        // monitorenter(period_BUILDER)
        try {
            DataUsageController.PERIOD_BUILDER.setLength(0);
            final DataUsageController dataUsageController = this;
            final Context context = dataUsageController.mContext;
            final Formatter formatter = DataUsageController.PERIOD_FORMATTER;
            final long n3 = n;
            final long n4 = n2;
            final int n5 = 65552;
            final String s = null;
            final Formatter formatter2 = DateUtils.formatDateRange(context, formatter, n3, n4, n5, s);
            final String s2 = formatter2.toString();
            final StringBuilder sb = period_BUILDER;
            // monitorexit(sb)
            return s2;
        }
        finally {
            final Object o2;
            final Object o = o2;
        }
        while (true) {
            try {
                final DataUsageController dataUsageController = this;
                final Context context = dataUsageController.mContext;
                final Formatter formatter = DataUsageController.PERIOD_FORMATTER;
                final long n3 = n;
                final long n4 = n2;
                final int n5 = 65552;
                final String s = null;
                final Formatter formatter2 = DateUtils.formatDateRange(context, formatter, n3, n4, n5, s);
                final String s2 = formatter2.toString();
                final StringBuilder sb = period_BUILDER;
                // monitorexit(sb)
                return s2;
                // monitorexit(period_BUILDER)
                throw;
            }
            finally {
                continue;
            }
            break;
        }
    }
    
    private static String getActiveSubscriberId(final Context context) {
        return TelephonyManager.from(context).getSubscriberId(SubscriptionManager.getDefaultDataSubscriptionId());
    }
    
    private INetworkStatsSession getSession() {
        if (this.mSession == null) {
            try {
                this.mSession = this.mStatsService.openSession();
            }
            catch (RuntimeException ex) {
                Log.w("DataUsageController", "Failed to open stats session", (Throwable)ex);
            }
            catch (RemoteException ex2) {
                Log.w("DataUsageController", "Failed to open stats session", (Throwable)ex2);
            }
        }
        return this.mSession;
    }
    
    private static String historyEntryToString(final NetworkStatsHistory$Entry networkStatsHistory$Entry) {
        String string;
        if (networkStatsHistory$Entry == null) {
            string = null;
        }
        else {
            final StringBuilder sb = new StringBuilder("Entry[");
            sb.append("bucketDuration=");
            sb.append(networkStatsHistory$Entry.bucketDuration);
            sb.append(",bucketStart=");
            sb.append(networkStatsHistory$Entry.bucketStart);
            sb.append(",activeTime=");
            sb.append(networkStatsHistory$Entry.activeTime);
            sb.append(",rxBytes=");
            sb.append(networkStatsHistory$Entry.rxBytes);
            sb.append(",rxPackets=");
            sb.append(networkStatsHistory$Entry.rxPackets);
            sb.append(",txBytes=");
            sb.append(networkStatsHistory$Entry.txBytes);
            sb.append(",txPackets=");
            sb.append(networkStatsHistory$Entry.txPackets);
            sb.append(",operations=");
            sb.append(networkStatsHistory$Entry.operations);
            sb.append(']');
            string = sb.toString();
        }
        return string;
    }
    
    private DataUsageInfo warn(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Failed to get data usage, ");
        sb.append(s);
        Log.w("DataUsageController", sb.toString());
        return null;
    }
    
    public DataUsageInfo getDataUsageInfo() {
        final String activeSubscriberId = getActiveSubscriberId(this.mContext);
        if (activeSubscriberId == null) {
            return this.warn("no subscriber id");
        }
        return this.getDataUsageInfo(NetworkTemplate.normalize(NetworkTemplate.buildTemplateMobileAll(activeSubscriberId), this.mTelephonyManager.getMergedSubscriberIds()));
    }
    
    public DataUsageInfo getDataUsageInfo(final NetworkTemplate networkTemplate) {
        final INetworkStatsSession session = this.getSession();
        if (session == null) {
            return this.warn("no stats session");
        }
        final NetworkPolicy networkPolicy = this.findNetworkPolicy(networkTemplate);
        try {
            final NetworkStatsHistory historyForNetwork = session.getHistoryForNetwork(networkTemplate, 10);
            final long currentTimeMillis = System.currentTimeMillis();
            long epochMilli = 0L;
            long epochMilli2 = 0L;
            Label_0104: {
                if (networkPolicy != null) {
                    try {
                        final Pair pair = NetworkPolicyManager.cycleIterator(networkPolicy).next();
                        epochMilli = ((ZonedDateTime)pair.first).toInstant().toEpochMilli();
                        epochMilli2 = ((ZonedDateTime)pair.second).toInstant().toEpochMilli();
                        break Label_0104;
                    }
                    catch (RemoteException ex) {
                        return this.warn("remote call failed");
                    }
                }
                epochMilli2 = currentTimeMillis;
                epochMilli = currentTimeMillis - 2419200000L;
            }
            final long currentTimeMillis2 = System.currentTimeMillis();
            try {
                final NetworkStatsHistory$Entry values = historyForNetwork.getValues(epochMilli, epochMilli2, currentTimeMillis, (NetworkStatsHistory$Entry)null);
                final long currentTimeMillis3 = System.currentTimeMillis();
                if (DataUsageController.DEBUG) {
                    try {
                        Log.d("DataUsageController", String.format("history call from %s to %s now=%s took %sms: %s", new Date(epochMilli), new Date(epochMilli2), new Date(currentTimeMillis), currentTimeMillis3 - currentTimeMillis2, historyEntryToString(values)));
                    }
                    catch (RemoteException ex2) {
                        return this.warn("remote call failed");
                    }
                }
                if (values == null) {
                    try {
                        return this.warn("no entry data");
                    }
                    catch (RemoteException ex3) {
                        return this.warn("remote call failed");
                    }
                }
                try {
                    final long rxBytes = values.rxBytes;
                    final long txBytes = values.txBytes;
                    final DataUsageInfo dataUsageInfo = new DataUsageInfo();
                    dataUsageInfo.startDate = epochMilli;
                    dataUsageInfo.usageLevel = rxBytes + txBytes;
                    dataUsageInfo.period = this.formatDateRange(epochMilli, epochMilli2);
                    dataUsageInfo.cycleStart = epochMilli;
                    dataUsageInfo.cycleEnd = epochMilli2;
                    Label_0380: {
                        if (networkPolicy != null) {
                            try {
                                long limitBytes;
                                if (networkPolicy.limitBytes > 0L) {
                                    limitBytes = networkPolicy.limitBytes;
                                }
                                else {
                                    limitBytes = 0L;
                                }
                                dataUsageInfo.limitLevel = limitBytes;
                                long warningBytes;
                                if (networkPolicy.warningBytes > 0L) {
                                    warningBytes = networkPolicy.warningBytes;
                                }
                                else {
                                    warningBytes = 0L;
                                }
                                dataUsageInfo.warningLevel = warningBytes;
                                break Label_0380;
                            }
                            catch (RemoteException ex4) {
                                return this.warn("remote call failed");
                            }
                        }
                        dataUsageInfo.warningLevel = this.getDefaultWarningLevel();
                    }
                    if (this.mNetworkController != null) {
                        dataUsageInfo.carrier = this.mNetworkController.getMobileDataNetworkName();
                    }
                    return dataUsageInfo;
                }
                catch (RemoteException ex5) {}
            }
            catch (RemoteException ex6) {}
        }
        catch (RemoteException ex7) {}
        return this.warn("remote call failed");
    }
    
    public long getDefaultWarningLevel() {
        return 1048576L * this.mContext.getResources().getInteger(17694944);
    }
    
    public boolean isMobileDataEnabled() {
        return this.mTelephonyManager.getDataEnabled();
    }
    
    public boolean isMobileDataSupported() {
        final ConnectivityManager mConnectivityManager = this.mConnectivityManager;
        boolean b = false;
        if (mConnectivityManager.isNetworkSupported(0)) {
            b = b;
            if (this.mTelephonyManager.getSimState() == 5) {
                b = true;
            }
        }
        return b;
    }
    
    public void setMobileDataEnabled(final boolean dataEnabled) {
        final StringBuilder sb = new StringBuilder();
        sb.append("setMobileDataEnabled: enabled=");
        sb.append(dataEnabled);
        Log.d("DataUsageController", sb.toString());
        this.mTelephonyManager.setDataEnabled(dataEnabled);
        if (this.mCallback != null) {
            this.mCallback.onMobileDataEnabled(dataEnabled);
        }
    }
    
    public interface Callback
    {
        void onMobileDataEnabled(final boolean p0);
    }
    
    public static class DataUsageInfo
    {
        public String carrier;
        public long cycleEnd;
        public long cycleStart;
        public long limitLevel;
        public String period;
        public long startDate;
        public long usageLevel;
        public long warningLevel;
    }
    
    public interface NetworkNameProvider
    {
        String getMobileDataNetworkName();
    }
}
