package com.android.settingslib.net;

import android.content.pm.PackageInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.UserInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import android.util.Log;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.app.AppGlobals;
import android.os.UserHandle;
import com.android.settingslib.Utils;
import android.os.UserManager;
import com.android.settingslib.R;
import android.util.SparseArray;
import android.content.Context;

public class UidDetailProvider
{
    private final Context mContext;
    private final SparseArray<UidDetail> mUidDetailCache;
    
    public UidDetailProvider(final Context context) {
        this.mContext = context.getApplicationContext();
        this.mUidDetailCache = (SparseArray<UidDetail>)new SparseArray();
    }
    
    public static int buildKeyForUser(final int n) {
        return -2000 - n;
    }
    
    private UidDetail buildUidDetail(int n) {
        final Resources resources = this.mContext.getResources();
        final PackageManager packageManager = this.mContext.getPackageManager();
        final UidDetail uidDetail = new UidDetail();
        uidDetail.label = packageManager.getNameForUid(n);
        uidDetail.icon = packageManager.getDefaultActivityIcon();
        if (n == 1000) {
            uidDetail.label = resources.getString(R.string.process_kernel_label);
            uidDetail.icon = packageManager.getDefaultActivityIcon();
            return uidDetail;
        }
        switch (n) {
            default: {
                final UserManager userManager = (UserManager)this.mContext.getSystemService("user");
                if (isKeyForUser(n)) {
                    final UserInfo userInfo = userManager.getUserInfo(getUserIdForKey(n));
                    if (userInfo != null) {
                        uidDetail.label = Utils.getUserLabel(this.mContext, userInfo);
                        uidDetail.icon = Utils.getUserIcon(this.mContext, userManager, userInfo);
                        return uidDetail;
                    }
                }
                final String[] packagesForUid = packageManager.getPackagesForUid(n);
                int length;
                if (packagesForUid != null) {
                    length = ((Throwable)(Object)packagesForUid).length;
                }
                else {
                    length = 0;
                }
                Label_0572: {
                    try {
                        final int userId = UserHandle.getUserId(n);
                        final UserHandle userHandle = new UserHandle(userId);
                        final IPackageManager packageManager2 = AppGlobals.getPackageManager();
                        Label_0503: {
                            if (length == 1) {
                                try {
                                    final ApplicationInfo applicationInfo = packageManager2.getApplicationInfo(packagesForUid[0], 0, userId);
                                    Label_0279: {
                                        if (applicationInfo != null) {
                                            uidDetail.label = applicationInfo.loadLabel(packageManager).toString();
                                            uidDetail.icon = userManager.getBadgedIconForUser(applicationInfo.loadIcon(packageManager), new UserHandle(userId));
                                            break Label_0279;
                                        }
                                        break Label_0279;
                                    }
                                    break Label_0503;
                                }
                                catch (RemoteException packagesForUid) {
                                    goto Label_0532;
                                }
                                catch (PackageManager$NameNotFoundException packagesForUid) {
                                    break Label_0572;
                                }
                            }
                            if (length <= 1) {
                                break Label_0503;
                            }
                            uidDetail.detailLabels = new CharSequence[length];
                            uidDetail.detailContentDescriptions = new CharSequence[length];
                            int n2 = 0;
                            while (true) {
                                if (n2 >= length) {
                                    break Label_0503;
                                }
                                final String s = packagesForUid[n2];
                                final PackageInfo packageInfo = packageManager.getPackageInfo(s, 0);
                                final ApplicationInfo applicationInfo2 = packageManager2.getApplicationInfo(s, 0, userId);
                                Label_0497: {
                                    if (applicationInfo2 == null) {
                                        break Label_0497;
                                    }
                                    final CharSequence[] detailLabels = uidDetail.detailLabels;
                                    try {
                                        detailLabels[n2] = applicationInfo2.loadLabel(packageManager).toString();
                                        uidDetail.detailContentDescriptions[n2] = userManager.getBadgedLabelForUser(uidDetail.detailLabels[n2], userHandle);
                                        if (packageInfo.sharedUserLabel != 0) {
                                            uidDetail.label = packageManager.getText(s, packageInfo.sharedUserLabel, packageInfo.applicationInfo).toString();
                                            uidDetail.icon = userManager.getBadgedIconForUser(applicationInfo2.loadIcon(packageManager), userHandle);
                                        }
                                        ++n2;
                                        continue;
                                        uidDetail.contentDescription = userManager.getBadgedLabelForUser(uidDetail.label, userHandle);
                                    }
                                    catch (RemoteException packagesForUid) {}
                                    catch (PackageManager$NameNotFoundException packagesForUid) {}
                                }
                                break;
                            }
                        }
                    }
                    catch (RemoteException ex) {}
                    catch (PackageManager$NameNotFoundException ex2) {}
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("Error while building UI detail for uid ");
                sb.append(n);
                Log.w("DataUsage", sb.toString(), (Throwable)(Object)packagesForUid);
                if (TextUtils.isEmpty(uidDetail.label)) {
                    uidDetail.label = Integer.toString(n);
                }
                return uidDetail;
            }
            case -4: {
                if (UserManager.supportsMultipleUsers()) {
                    n = R.string.data_usage_uninstalled_apps_users;
                }
                else {
                    n = R.string.data_usage_uninstalled_apps;
                }
                uidDetail.label = resources.getString(n);
                uidDetail.icon = packageManager.getDefaultActivityIcon();
                return uidDetail;
            }
            case -5: {
                uidDetail.label = resources.getString(Utils.getTetheringLabel((ConnectivityManager)this.mContext.getSystemService("connectivity")));
                uidDetail.icon = packageManager.getDefaultActivityIcon();
                return uidDetail;
            }
        }
    }
    
    public static int getUserIdForKey(final int n) {
        return -2000 - n;
    }
    
    public static boolean isKeyForUser(final int n) {
        return n <= -2000;
    }
    
    public void clearCache() {
        synchronized (this.mUidDetailCache) {
            this.mUidDetailCache.clear();
        }
    }
    
    public UidDetail getUidDetail(final int n, final boolean b) {
        synchronized (this.mUidDetailCache) {
            final UidDetail uidDetail = (UidDetail)this.mUidDetailCache.get(n);
            // monitorexit(this.mUidDetailCache)
            if (uidDetail != null) {
                return uidDetail;
            }
            if (!b) {
                return null;
            }
            final UidDetail buildUidDetail = this.buildUidDetail(n);
            final SparseArray<UidDetail> mUidDetailCache = this.mUidDetailCache;
            synchronized (this.mUidDetailCache) {
                this.mUidDetailCache.put(n, (Object)buildUidDetail);
                return buildUidDetail;
            }
        }
    }
}
