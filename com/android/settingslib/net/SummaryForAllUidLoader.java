package com.android.settingslib.net;

import android.os.RemoteException;
import android.os.Parcelable;
import android.net.NetworkTemplate;
import android.content.Context;
import android.net.INetworkStatsSession;
import android.os.Bundle;
import android.net.NetworkStats;
import android.content.AsyncTaskLoader;

public class SummaryForAllUidLoader extends AsyncTaskLoader<NetworkStats>
{
    private final Bundle mArgs;
    private final INetworkStatsSession mSession;
    
    public SummaryForAllUidLoader(final Context context, final INetworkStatsSession mSession, final Bundle mArgs) {
        super(context);
        this.mSession = mSession;
        this.mArgs = mArgs;
    }
    
    public static Bundle buildArgs(final NetworkTemplate networkTemplate, final long n, final long n2) {
        final Bundle bundle = new Bundle();
        bundle.putParcelable("template", (Parcelable)networkTemplate);
        bundle.putLong("start", n);
        bundle.putLong("end", n2);
        return bundle;
    }
    
    public NetworkStats loadInBackground() {
        final NetworkTemplate networkTemplate = (NetworkTemplate)this.mArgs.getParcelable("template");
        final long long1 = this.mArgs.getLong("start");
        final long long2 = this.mArgs.getLong("end");
        try {
            return this.mSession.getSummaryForAllUid(networkTemplate, long1, long2, false);
        }
        catch (RemoteException ex) {
            return null;
        }
    }
    
    protected void onReset() {
        super.onReset();
        this.cancelLoad();
    }
    
    protected void onStartLoading() {
        super.onStartLoading();
        this.forceLoad();
    }
    
    protected void onStopLoading() {
        super.onStopLoading();
        this.cancelLoad();
    }
}
