package com.android.settingslib;

import java.util.Objects;
import android.widget.TextView;
import android.view.MenuItem$OnMenuItemClickListener;
import android.view.MenuItem;
import android.text.style.ForegroundColorSpan;
import android.text.SpannableStringBuilder;
import android.os.Parcelable;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.UserManager$EnforcingUser;
import android.os.UserHandle;
import android.content.pm.IPackageManager;
import android.os.RemoteException;
import android.app.AppGlobals;
import java.util.List;
import java.util.Iterator;
import android.content.ComponentName;
import android.content.pm.UserInfo;
import android.os.UserManager;
import com.android.internal.widget.LockPatternUtils;
import android.app.admin.DevicePolicyManager;
import android.content.Context;

public class RestrictedLockUtils
{
    static Proxy sProxy;
    
    static {
        RestrictedLockUtils.sProxy = new Proxy();
    }
    
    private static EnforcedAdmin checkForLockSetting(final Context context, final int n, final LockSettingCheck lockSettingCheck) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final LockPatternUtils lockPatternUtils = new LockPatternUtils(context);
        final EnforcedAdmin enforcedAdmin = null;
        final Iterator<UserInfo> iterator = (Iterator<UserInfo>)UserManager.get(context).getProfiles(n).iterator();
        EnforcedAdmin enforcedAdmin2 = enforcedAdmin;
        while (iterator.hasNext()) {
            final UserInfo userInfo = iterator.next();
            final List activeAdminsAsUser = devicePolicyManager.getActiveAdminsAsUser(userInfo.id);
            if (activeAdminsAsUser == null) {
                continue;
            }
            final boolean separateProfileChallengeEnabled = RestrictedLockUtils.sProxy.isSeparateProfileChallengeEnabled(lockPatternUtils, userInfo.id);
            for (final ComponentName componentName : activeAdminsAsUser) {
                if (!separateProfileChallengeEnabled && lockSettingCheck.isEnforcing(devicePolicyManager, componentName, userInfo.id)) {
                    if (enforcedAdmin2 != null) {
                        return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
                    }
                    enforcedAdmin2 = new EnforcedAdmin(componentName, userInfo.id);
                }
                else {
                    EnforcedAdmin enforcedAdmin3 = enforcedAdmin2;
                    if (userInfo.isManagedProfile()) {
                        enforcedAdmin3 = enforcedAdmin2;
                        if (lockSettingCheck.isEnforcing(RestrictedLockUtils.sProxy.getParentProfileInstance(devicePolicyManager, userInfo), componentName, userInfo.id)) {
                            if (enforcedAdmin2 != null) {
                                return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
                            }
                            enforcedAdmin3 = new EnforcedAdmin(componentName, userInfo.id);
                        }
                    }
                    enforcedAdmin2 = enforcedAdmin3;
                }
            }
        }
        return enforcedAdmin2;
    }
    
    public static EnforcedAdmin checkIfAccessibilityServiceDisallowed(final Context context, final String s, int managedProfileId) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final EnforcedAdmin profileOrDeviceOwner = getProfileOrDeviceOwner(context, managedProfileId);
        boolean accessibilityServicePermittedByAdmin = true;
        if (profileOrDeviceOwner != null) {
            accessibilityServicePermittedByAdmin = devicePolicyManager.isAccessibilityServicePermittedByAdmin(profileOrDeviceOwner.component, s, managedProfileId);
        }
        managedProfileId = getManagedProfileId(context, managedProfileId);
        final EnforcedAdmin profileOrDeviceOwner2 = getProfileOrDeviceOwner(context, managedProfileId);
        boolean accessibilityServicePermittedByAdmin2 = true;
        if (profileOrDeviceOwner2 != null) {
            accessibilityServicePermittedByAdmin2 = devicePolicyManager.isAccessibilityServicePermittedByAdmin(profileOrDeviceOwner2.component, s, managedProfileId);
        }
        if (!accessibilityServicePermittedByAdmin && !accessibilityServicePermittedByAdmin2) {
            return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
        }
        if (!accessibilityServicePermittedByAdmin) {
            return profileOrDeviceOwner;
        }
        if (!accessibilityServicePermittedByAdmin2) {
            return profileOrDeviceOwner2;
        }
        return null;
    }
    
    public static EnforcedAdmin checkIfAccountManagementDisabled(final Context context, final String s, final int n) {
        if (s == null) {
            return null;
        }
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (!context.getPackageManager().hasSystemFeature("android.software.device_admin") || devicePolicyManager == null) {
            return null;
        }
        final boolean b = false;
        final String[] accountTypesWithManagementDisabledAsUser = devicePolicyManager.getAccountTypesWithManagementDisabledAsUser(n);
        final int length = accountTypesWithManagementDisabledAsUser.length;
        int n2 = 0;
        boolean b2;
        while (true) {
            b2 = b;
            if (n2 >= length) {
                break;
            }
            if (s.equals(accountTypesWithManagementDisabledAsUser[n2])) {
                b2 = true;
                break;
            }
            ++n2;
        }
        if (!b2) {
            return null;
        }
        return getProfileOrDeviceOwner(context, n);
    }
    
    public static EnforcedAdmin checkIfApplicationIsSuspended(final Context context, final String s, final int n) {
        final IPackageManager packageManager = AppGlobals.getPackageManager();
        try {
            if (packageManager.isPackageSuspendedForUser(s, n)) {
                return getProfileOrDeviceOwner(context, n);
            }
        }
        catch (RemoteException ex) {}
        catch (IllegalArgumentException ex2) {}
        return null;
    }
    
    public static EnforcedAdmin checkIfAutoTimeRequired(final Context context) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager != null && devicePolicyManager.getAutoTimeRequired()) {
            return new EnforcedAdmin(devicePolicyManager.getDeviceOwnerComponentOnCallingUser(), UserHandle.myUserId());
        }
        return null;
    }
    
    public static EnforcedAdmin checkIfInputMethodDisallowed(final Context context, final String s, int managedProfileId) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final EnforcedAdmin profileOrDeviceOwner = getProfileOrDeviceOwner(context, managedProfileId);
        boolean inputMethodPermittedByAdmin = true;
        if (profileOrDeviceOwner != null) {
            inputMethodPermittedByAdmin = devicePolicyManager.isInputMethodPermittedByAdmin(profileOrDeviceOwner.component, s, managedProfileId);
        }
        managedProfileId = getManagedProfileId(context, managedProfileId);
        final EnforcedAdmin profileOrDeviceOwner2 = getProfileOrDeviceOwner(context, managedProfileId);
        boolean inputMethodPermittedByAdmin2 = true;
        if (profileOrDeviceOwner2 != null) {
            inputMethodPermittedByAdmin2 = devicePolicyManager.isInputMethodPermittedByAdmin(profileOrDeviceOwner2.component, s, managedProfileId);
        }
        if (!inputMethodPermittedByAdmin && !inputMethodPermittedByAdmin2) {
            return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
        }
        if (!inputMethodPermittedByAdmin) {
            return profileOrDeviceOwner;
        }
        if (!inputMethodPermittedByAdmin2) {
            return profileOrDeviceOwner2;
        }
        return null;
    }
    
    public static EnforcedAdmin checkIfKeyguardFeaturesDisabled(final Context context, final int n, final int n2) {
        final _$$Lambda$RestrictedLockUtils$r6A4RGcQdg8eQ1PGvcQ0UANBzNk $$Lambda$RestrictedLockUtils$r6A4RGcQdg8eQ1PGvcQ0UANBzNk = new _$$Lambda$RestrictedLockUtils$r6A4RGcQdg8eQ1PGvcQ0UANBzNk(n2, n);
        if (UserManager.get(context).getUserInfo(n2).isManagedProfile()) {
            final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
            return findEnforcedAdmin(devicePolicyManager.getActiveAdminsAsUser(n2), devicePolicyManager, n2, (LockSettingCheck)$$Lambda$RestrictedLockUtils$r6A4RGcQdg8eQ1PGvcQ0UANBzNk);
        }
        return checkForLockSetting(context, n2, (LockSettingCheck)$$Lambda$RestrictedLockUtils$r6A4RGcQdg8eQ1PGvcQ0UANBzNk);
    }
    
    public static EnforcedAdmin checkIfMaximumTimeToLockIsSet(final Context context) {
        return checkForLockSetting(context, UserHandle.myUserId(), (LockSettingCheck)_$$Lambda$RestrictedLockUtils$sbYwAwFLTMW969YNG1W7ojc_r04.INSTANCE);
    }
    
    public static EnforcedAdmin checkIfMeteredDataRestricted(final Context context, final String s, final int n) {
        final EnforcedAdmin profileOrDeviceOwner = getProfileOrDeviceOwner(context, n);
        final EnforcedAdmin enforcedAdmin = null;
        if (profileOrDeviceOwner == null) {
            return null;
        }
        EnforcedAdmin enforcedAdmin2;
        if (((DevicePolicyManager)context.getSystemService("device_policy")).isMeteredDataDisabledPackageForUser(profileOrDeviceOwner.component, s, n)) {
            enforcedAdmin2 = profileOrDeviceOwner;
        }
        else {
            enforcedAdmin2 = enforcedAdmin;
        }
        return enforcedAdmin2;
    }
    
    public static EnforcedAdmin checkIfPasswordQualityIsSet(final Context context, final int n) {
        final -$$Lambda$RestrictedLockUtils$ZGpdJ-Goya42TrXyPazgpDXw5os instance = _$$Lambda$RestrictedLockUtils$ZGpdJ_Goya42TrXyPazgpDXw5os.INSTANCE;
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        if (!RestrictedLockUtils.sProxy.isSeparateProfileChallengeEnabled(new LockPatternUtils(context), n)) {
            return checkForLockSetting(context, n, (LockSettingCheck)instance);
        }
        final List activeAdminsAsUser = devicePolicyManager.getActiveAdminsAsUser(n);
        if (activeAdminsAsUser == null) {
            return null;
        }
        EnforcedAdmin enforcedAdmin = null;
        for (final ComponentName componentName : activeAdminsAsUser) {
            EnforcedAdmin enforcedAdmin2 = enforcedAdmin;
            if (((LockSettingCheck)instance).isEnforcing(devicePolicyManager, componentName, n)) {
                if (enforcedAdmin != null) {
                    return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
                }
                enforcedAdmin2 = new EnforcedAdmin(componentName, n);
            }
            enforcedAdmin = enforcedAdmin2;
        }
        return enforcedAdmin;
    }
    
    public static EnforcedAdmin checkIfRemoteContactSearchDisallowed(final Context context, final int n) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final EnforcedAdmin profileOwner = getProfileOwner(context, n);
        if (profileOwner == null) {
            return null;
        }
        final UserHandle of = UserHandle.of(n);
        if (devicePolicyManager.getCrossProfileContactsSearchDisabled(of) && devicePolicyManager.getCrossProfileCallerIdDisabled(of)) {
            return profileOwner;
        }
        return null;
    }
    
    public static EnforcedAdmin checkIfRestrictionEnforced(final Context context, final String s, final int n) {
        if (context.getSystemService("device_policy") == null) {
            return null;
        }
        final UserManager value = UserManager.get(context);
        final List userRestrictionSources = value.getUserRestrictionSources(s, UserHandle.of(n));
        if (userRestrictionSources.isEmpty()) {
            return null;
        }
        if (userRestrictionSources.size() > 1) {
            return EnforcedAdmin.createDefaultEnforcedAdminWithRestriction(s);
        }
        final int userRestrictionSource = userRestrictionSources.get(0).getUserRestrictionSource();
        final int identifier = userRestrictionSources.get(0).getUserHandle().getIdentifier();
        if (userRestrictionSource == 4) {
            if (identifier == n) {
                return getProfileOwner(context, s, identifier);
            }
            final UserInfo profileParent = value.getProfileParent(identifier);
            EnforcedAdmin enforcedAdmin;
            if (profileParent != null && profileParent.id == n) {
                enforcedAdmin = getProfileOwner(context, s, identifier);
            }
            else {
                enforcedAdmin = EnforcedAdmin.createDefaultEnforcedAdminWithRestriction(s);
            }
            return enforcedAdmin;
        }
        else {
            if (userRestrictionSource == 2) {
                EnforcedAdmin enforcedAdmin2;
                if (identifier == n) {
                    enforcedAdmin2 = getDeviceOwner(context, s);
                }
                else {
                    enforcedAdmin2 = EnforcedAdmin.createDefaultEnforcedAdminWithRestriction(s);
                }
                return enforcedAdmin2;
            }
            return null;
        }
    }
    
    public static EnforcedAdmin checkIfUninstallBlocked(final Context context, final String s, final int n) {
        final EnforcedAdmin checkIfRestrictionEnforced = checkIfRestrictionEnforced(context, "no_control_apps", n);
        if (checkIfRestrictionEnforced != null) {
            return checkIfRestrictionEnforced;
        }
        final EnforcedAdmin checkIfRestrictionEnforced2 = checkIfRestrictionEnforced(context, "no_uninstall_apps", n);
        if (checkIfRestrictionEnforced2 != null) {
            return checkIfRestrictionEnforced2;
        }
        final IPackageManager packageManager = AppGlobals.getPackageManager();
        try {
            if (packageManager.getBlockUninstallForUser(s, n)) {
                return getProfileOrDeviceOwner(context, n);
            }
        }
        catch (RemoteException ex) {}
        return null;
    }
    
    private static EnforcedAdmin findEnforcedAdmin(final List<ComponentName> list, final DevicePolicyManager devicePolicyManager, final int n, final LockSettingCheck lockSettingCheck) {
        if (list == null) {
            return null;
        }
        EnforcedAdmin enforcedAdmin = null;
        for (final ComponentName componentName : list) {
            EnforcedAdmin enforcedAdmin2 = enforcedAdmin;
            if (lockSettingCheck.isEnforcing(devicePolicyManager, componentName, n)) {
                if (enforcedAdmin != null) {
                    return EnforcedAdmin.MULTIPLE_ENFORCED_ADMIN;
                }
                enforcedAdmin2 = new EnforcedAdmin(componentName, n);
            }
            enforcedAdmin = enforcedAdmin2;
        }
        return enforcedAdmin;
    }
    
    public static EnforcedAdmin getDeviceOwner(final Context context) {
        return getDeviceOwner(context, null);
    }
    
    private static EnforcedAdmin getDeviceOwner(final Context context, final String s) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final ComponentName deviceOwnerComponentOnAnyUser = devicePolicyManager.getDeviceOwnerComponentOnAnyUser();
        if (deviceOwnerComponentOnAnyUser != null) {
            return new EnforcedAdmin(deviceOwnerComponentOnAnyUser, s, devicePolicyManager.getDeviceOwnerUserId());
        }
        return null;
    }
    
    private static int getManagedProfileId(final Context context, final int n) {
        for (final UserInfo userInfo : ((UserManager)context.getSystemService("user")).getProfiles(n)) {
            if (userInfo.id == n) {
                continue;
            }
            if (userInfo.isManagedProfile()) {
                return userInfo.id;
            }
        }
        return -10000;
    }
    
    public static EnforcedAdmin getProfileOrDeviceOwner(final Context context, final int n) {
        return getProfileOrDeviceOwner(context, null, n);
    }
    
    public static EnforcedAdmin getProfileOrDeviceOwner(final Context context, final String s, final int n) {
        if (n == -10000) {
            return null;
        }
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final ComponentName profileOwnerAsUser = devicePolicyManager.getProfileOwnerAsUser(n);
        if (profileOwnerAsUser != null) {
            return new EnforcedAdmin(profileOwnerAsUser, s, n);
        }
        if (devicePolicyManager.getDeviceOwnerUserId() == n) {
            final ComponentName deviceOwnerComponentOnAnyUser = devicePolicyManager.getDeviceOwnerComponentOnAnyUser();
            if (deviceOwnerComponentOnAnyUser != null) {
                return new EnforcedAdmin(deviceOwnerComponentOnAnyUser, s, n);
            }
        }
        return null;
    }
    
    private static EnforcedAdmin getProfileOwner(final Context context, final int n) {
        return getProfileOwner(context, null, n);
    }
    
    private static EnforcedAdmin getProfileOwner(final Context context, final String s, final int n) {
        if (n == -10000) {
            return null;
        }
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        if (devicePolicyManager == null) {
            return null;
        }
        final ComponentName profileOwnerAsUser = devicePolicyManager.getProfileOwnerAsUser(n);
        if (profileOwnerAsUser != null) {
            return new EnforcedAdmin(profileOwnerAsUser, s, n);
        }
        return null;
    }
    
    public static Drawable getRestrictedPadlock(final Context context) {
        final Drawable drawable = context.getDrawable(R.drawable.ic_info);
        final int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.restricted_icon_size);
        drawable.setBounds(0, 0, dimensionPixelSize, dimensionPixelSize);
        return drawable;
    }
    
    public static Intent getShowAdminSupportDetailsIntent(final Context context, final EnforcedAdmin enforcedAdmin) {
        final Intent intent = new Intent("android.settings.SHOW_ADMIN_SUPPORT_DETAILS");
        if (enforcedAdmin != null) {
            if (enforcedAdmin.component != null) {
                intent.putExtra("android.app.extra.DEVICE_ADMIN", (Parcelable)enforcedAdmin.component);
            }
            int n = UserHandle.myUserId();
            if (enforcedAdmin.userId != -10000) {
                n = enforcedAdmin.userId;
            }
            intent.putExtra("android.intent.extra.USER_ID", n);
        }
        return intent;
    }
    
    public static boolean hasBaseUserRestriction(final Context context, final String s, final int n) {
        return ((UserManager)context.getSystemService("user")).hasBaseUserRestriction(s, UserHandle.of(n));
    }
    
    public static boolean isAdminInCurrentUserOrProfile(final Context context, final ComponentName componentName) {
        final DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context.getSystemService("device_policy");
        final Iterator<UserInfo> iterator = (Iterator<UserInfo>)UserManager.get(context).getProfiles(UserHandle.myUserId()).iterator();
        while (iterator.hasNext()) {
            if (devicePolicyManager.isAdminActiveAsUser(componentName, iterator.next().id)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isCurrentUserOrProfile(final Context context, final int n) {
        final Iterator<UserInfo> iterator = UserManager.get(context).getProfiles(UserHandle.myUserId()).iterator();
        while (iterator.hasNext()) {
            if (iterator.next().id == n) {
                return true;
            }
        }
        return false;
    }
    
    private static void removeExistingRestrictedSpans(final SpannableStringBuilder spannableStringBuilder) {
        final int length = spannableStringBuilder.length();
        final RestrictedLockImageSpan[] array = (RestrictedLockImageSpan[])spannableStringBuilder.getSpans(length - 1, length, (Class)RestrictedLockImageSpan.class);
        final int length2 = array.length;
        final int n = 0;
        for (final RestrictedLockImageSpan restrictedLockImageSpan : array) {
            final int spanStart = spannableStringBuilder.getSpanStart((Object)restrictedLockImageSpan);
            final int spanEnd = spannableStringBuilder.getSpanEnd((Object)restrictedLockImageSpan);
            spannableStringBuilder.removeSpan((Object)restrictedLockImageSpan);
            spannableStringBuilder.delete(spanStart, spanEnd);
        }
        final ForegroundColorSpan[] array2 = (ForegroundColorSpan[])spannableStringBuilder.getSpans(0, length, (Class)ForegroundColorSpan.class);
        for (int length3 = array2.length, j = n; j < length3; ++j) {
            spannableStringBuilder.removeSpan((Object)array2[j]);
        }
    }
    
    public static void sendShowAdminSupportDetailsIntent(final Context context, final EnforcedAdmin enforcedAdmin) {
        final Intent showAdminSupportDetailsIntent = getShowAdminSupportDetailsIntent(context, enforcedAdmin);
        int n2;
        final int n = n2 = UserHandle.myUserId();
        if (enforcedAdmin != null) {
            n2 = n;
            if (enforcedAdmin.userId != -10000) {
                n2 = n;
                if (isCurrentUserOrProfile(context, enforcedAdmin.userId)) {
                    n2 = enforcedAdmin.userId;
                }
            }
        }
        showAdminSupportDetailsIntent.putExtra("android.app.extra.RESTRICTION", enforcedAdmin.enforcedRestriction);
        context.startActivityAsUser(showAdminSupportDetailsIntent, new UserHandle(n2));
    }
    
    public static void setMenuItemAsDisabledByAdmin(final Context context, final MenuItem menuItem, final EnforcedAdmin enforcedAdmin) {
        final SpannableStringBuilder title = new SpannableStringBuilder(menuItem.getTitle());
        removeExistingRestrictedSpans(title);
        if (enforcedAdmin != null) {
            title.setSpan((Object)new ForegroundColorSpan(context.getColor(R.color.disabled_text_color)), 0, title.length(), 33);
            title.append((CharSequence)" ", (Object)new RestrictedLockImageSpan(context), 33);
            menuItem.setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)new MenuItem$OnMenuItemClickListener() {
                public boolean onMenuItemClick(final MenuItem menuItem) {
                    RestrictedLockUtils.sendShowAdminSupportDetailsIntent(context, enforcedAdmin);
                    return true;
                }
            });
        }
        else {
            menuItem.setOnMenuItemClickListener((MenuItem$OnMenuItemClickListener)null);
        }
        menuItem.setTitle((CharSequence)title);
    }
    
    public static void setTextViewAsDisabledByAdmin(final Context context, final TextView textView, final boolean b) {
        final SpannableStringBuilder text = new SpannableStringBuilder(textView.getText());
        removeExistingRestrictedSpans(text);
        if (b) {
            text.setSpan((Object)new ForegroundColorSpan(context.getColor(R.color.disabled_text_color)), 0, text.length(), 33);
            textView.setCompoundDrawables((Drawable)null, (Drawable)null, getRestrictedPadlock(context), (Drawable)null);
            textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelSize(R.dimen.restricted_icon_padding));
        }
        else {
            textView.setCompoundDrawables((Drawable)null, (Drawable)null, (Drawable)null, (Drawable)null);
        }
        textView.setText((CharSequence)text);
    }
    
    public static class EnforcedAdmin
    {
        public static final EnforcedAdmin MULTIPLE_ENFORCED_ADMIN;
        public ComponentName component;
        public String enforcedRestriction;
        public int userId;
        
        static {
            MULTIPLE_ENFORCED_ADMIN = new EnforcedAdmin();
        }
        
        public EnforcedAdmin() {
            this.component = null;
            this.enforcedRestriction = null;
            this.userId = -10000;
        }
        
        public EnforcedAdmin(final ComponentName component, final int userId) {
            this.component = null;
            this.enforcedRestriction = null;
            this.userId = -10000;
            this.component = component;
            this.userId = userId;
        }
        
        public EnforcedAdmin(final ComponentName component, final String enforcedRestriction, final int userId) {
            this.component = null;
            this.enforcedRestriction = null;
            this.userId = -10000;
            this.component = component;
            this.enforcedRestriction = enforcedRestriction;
            this.userId = userId;
        }
        
        public static EnforcedAdmin createDefaultEnforcedAdminWithRestriction(final String enforcedRestriction) {
            final EnforcedAdmin enforcedAdmin = new EnforcedAdmin();
            enforcedAdmin.enforcedRestriction = enforcedRestriction;
            return enforcedAdmin;
        }
        
        @Override
        public boolean equals(final Object o) {
            boolean b = true;
            if (this == o) {
                return true;
            }
            if (o != null && this.getClass() == o.getClass()) {
                final EnforcedAdmin enforcedAdmin = (EnforcedAdmin)o;
                if (this.userId != enforcedAdmin.userId || !Objects.equals(this.component, enforcedAdmin.component) || !Objects.equals(this.enforcedRestriction, enforcedAdmin.enforcedRestriction)) {
                    b = false;
                }
                return b;
            }
            return false;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(this.component, this.enforcedRestriction, this.userId);
        }
        
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder();
            sb.append("EnforcedAdmin{component=");
            sb.append(this.component);
            sb.append(", enforcedRestriction='");
            sb.append(this.enforcedRestriction);
            sb.append(", userId=");
            sb.append(this.userId);
            sb.append('}');
            return sb.toString();
        }
    }
    
    private interface LockSettingCheck
    {
        boolean isEnforcing(final DevicePolicyManager p0, final ComponentName p1, final int p2);
    }
    
    static class Proxy
    {
        public DevicePolicyManager getParentProfileInstance(final DevicePolicyManager devicePolicyManager, final UserInfo userInfo) {
            return devicePolicyManager.getParentProfileInstance(userInfo);
        }
        
        public boolean isSeparateProfileChallengeEnabled(final LockPatternUtils lockPatternUtils, final int n) {
            return lockPatternUtils.isSeparateProfileChallengeEnabled(n);
        }
    }
}
