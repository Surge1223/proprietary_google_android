package com.android.settingslib;

import android.os.SystemProperties;
import android.telephony.CarrierConfigManager;
import android.content.Context;

public class TetherUtil
{
    static boolean isEntitlementCheckRequired(final Context context) {
        final CarrierConfigManager carrierConfigManager = (CarrierConfigManager)context.getSystemService("carrier_config");
        return carrierConfigManager == null || carrierConfigManager.getConfig() == null || carrierConfigManager.getConfig().getBoolean("require_entitlement_checks_bool");
    }
    
    public static boolean isProvisioningNeeded(final Context context) {
        final String[] stringArray = context.getResources().getStringArray(17236021);
        boolean b = false;
        if (SystemProperties.getBoolean("net.tethering.noprovisioning", false) || stringArray == null) {
            return false;
        }
        if (!isEntitlementCheckRequired(context)) {
            return false;
        }
        if (stringArray.length == 2) {
            b = true;
        }
        return b;
    }
}
