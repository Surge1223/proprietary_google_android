package com.android.settingslib;

import com.android.internal.logging.MetricsLogger;
import android.content.Context;

public final class TronUtils
{
    public static void logWifiSettingsSpeed(final Context context, final int n) {
        MetricsLogger.histogram(context, "settings_wifi_speed_labels", n);
    }
}
