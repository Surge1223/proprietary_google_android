package com.android.setupwizardlib;

import com.android.setupwizardlib.util.FallbackThemeWrapper;
import android.support.annotation.Keep;
import android.annotation.TargetApi;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.util.AttributeSet;
import java.util.HashMap;
import android.content.Context;
import android.view.ViewTreeObserver$OnPreDrawListener;
import com.android.setupwizardlib.template.Mixin;
import java.util.Map;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class TemplateLayout extends FrameLayout
{
    private ViewGroup mContainer;
    private Map<Class<? extends Mixin>, Mixin> mMixins;
    private ViewTreeObserver$OnPreDrawListener mPreDrawListener;
    private float mXFraction;
    
    public TemplateLayout(final Context context, final int n, final int n2) {
        super(context);
        this.mMixins = new HashMap<Class<? extends Mixin>, Mixin>();
        this.init(n, n2, null, R.attr.suwLayoutTheme);
    }
    
    public TemplateLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mMixins = new HashMap<Class<? extends Mixin>, Mixin>();
        this.init(0, 0, set, R.attr.suwLayoutTheme);
    }
    
    public TemplateLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mMixins = new HashMap<Class<? extends Mixin>, Mixin>();
        this.init(0, 0, set, n);
    }
    
    private void addViewInternal(final View view) {
        super.addView(view, -1, (ViewGroup.LayoutParams)this.generateDefaultLayoutParams());
    }
    
    private void inflateTemplate(final int n, final int n2) {
        this.addViewInternal(this.onInflateTemplate(LayoutInflater.from(this.getContext()), n));
        this.mContainer = this.findContainer(n2);
        if (this.mContainer != null) {
            this.onTemplateInflated();
            return;
        }
        throw new IllegalArgumentException("Container cannot be null in TemplateLayout");
    }
    
    private void init(int resourceId, final int n, final AttributeSet set, int resourceId2) {
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SuwTemplateLayout, resourceId2, 0);
        resourceId2 = resourceId;
        if (resourceId == 0) {
            resourceId2 = obtainStyledAttributes.getResourceId(R.styleable.SuwTemplateLayout_android_layout, 0);
        }
        if ((resourceId = n) == 0) {
            resourceId = obtainStyledAttributes.getResourceId(R.styleable.SuwTemplateLayout_suwContainer, 0);
        }
        this.inflateTemplate(resourceId2, resourceId);
        obtainStyledAttributes.recycle();
    }
    
    public void addView(final View view, final int n, final ViewGroup.LayoutParams viewGroup.LayoutParams) {
        this.mContainer.addView(view, n, viewGroup.LayoutParams);
    }
    
    protected ViewGroup findContainer(final int n) {
        int containerId = n;
        if (n == 0) {
            containerId = this.getContainerId();
        }
        return (ViewGroup)this.findViewById(containerId);
    }
    
    public <T extends View> T findManagedViewById(final int n) {
        return (T)this.findViewById(n);
    }
    
    @Deprecated
    protected int getContainerId() {
        return 0;
    }
    
    public <M extends Mixin> M getMixin(final Class<M> clazz) {
        return (M)this.mMixins.get(clazz);
    }
    
    @TargetApi(11)
    @Keep
    public float getXFraction() {
        return this.mXFraction;
    }
    
    protected final View inflateTemplate(final LayoutInflater layoutInflater, final int n, final int n2) {
        if (n2 != 0) {
            LayoutInflater from = layoutInflater;
            if (n != 0) {
                from = LayoutInflater.from((Context)new FallbackThemeWrapper(layoutInflater.getContext(), n));
            }
            return from.inflate(n2, (ViewGroup)this, false);
        }
        throw new IllegalArgumentException("android:layout not specified for TemplateLayout");
    }
    
    protected View onInflateTemplate(final LayoutInflater layoutInflater, final int n) {
        return this.inflateTemplate(layoutInflater, 0, n);
    }
    
    protected void onTemplateInflated() {
    }
    
    protected <M extends Mixin> void registerMixin(final Class<M> clazz, final M m) {
        this.mMixins.put(clazz, m);
    }
    
    @TargetApi(11)
    @Keep
    public void setXFraction(final float mxFraction) {
        this.mXFraction = mxFraction;
        final int width = this.getWidth();
        if (width != 0) {
            this.setTranslationX(width * mxFraction);
        }
        else if (this.mPreDrawListener == null) {
            this.mPreDrawListener = (ViewTreeObserver$OnPreDrawListener)new ViewTreeObserver$OnPreDrawListener() {
                public boolean onPreDraw() {
                    TemplateLayout.this.getViewTreeObserver().removeOnPreDrawListener(TemplateLayout.this.mPreDrawListener);
                    TemplateLayout.this.setXFraction(TemplateLayout.this.mXFraction);
                    return true;
                }
            };
            this.getViewTreeObserver().addOnPreDrawListener(this.mPreDrawListener);
        }
    }
}
