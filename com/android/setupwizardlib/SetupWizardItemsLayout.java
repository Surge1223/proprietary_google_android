package com.android.setupwizardlib;

import com.android.setupwizardlib.items.ItemAdapter;
import android.widget.ListAdapter;
import android.util.AttributeSet;
import android.content.Context;

@Deprecated
public class SetupWizardItemsLayout extends SetupWizardListLayout
{
    public SetupWizardItemsLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public SetupWizardItemsLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    public ItemAdapter getAdapter() {
        final ListAdapter adapter = super.getAdapter();
        if (adapter instanceof ItemAdapter) {
            return (ItemAdapter)adapter;
        }
        return null;
    }
}
