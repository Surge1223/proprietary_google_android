package com.android.setupwizardlib.items;

import android.content.Context;

public class ItemInflater extends ReflectionInflater<ItemHierarchy>
{
    public ItemInflater(final Context context) {
        super(context);
        final StringBuilder sb = new StringBuilder();
        sb.append(Item.class.getPackage().getName());
        sb.append(".");
        this.setDefaultPackage(sb.toString());
    }
    
    @Override
    protected void onAddChildItem(final ItemHierarchy itemHierarchy, final ItemHierarchy itemHierarchy2) {
        if (itemHierarchy instanceof ItemParent) {
            ((ItemParent)itemHierarchy).addChild(itemHierarchy2);
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Cannot add child item to ");
        sb.append(itemHierarchy);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public interface ItemParent
    {
        void addChild(final ItemHierarchy p0);
    }
}
