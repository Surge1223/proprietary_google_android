package com.android.setupwizardlib.items;

import android.graphics.Rect;
import android.graphics.drawable.LayerDrawable;
import android.content.res.TypedArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.util.Log;
import android.graphics.drawable.Drawable;
import com.android.setupwizardlib.R;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;

public class RecyclerItemAdapter extends Adapter<ItemViewHolder> implements Observer
{
    private final ItemHierarchy mItemHierarchy;
    private OnItemSelectedListener mListener;
    
    public RecyclerItemAdapter(final ItemHierarchy mItemHierarchy) {
        (this.mItemHierarchy = mItemHierarchy).registerObserver((ItemHierarchy.Observer)this);
    }
    
    public IItem getItem(final int n) {
        return this.mItemHierarchy.getItemAt(n);
    }
    
    @Override
    public int getItemCount() {
        return this.mItemHierarchy.getCount();
    }
    
    @Override
    public long getItemId(int id) {
        final IItem item = this.getItem(id);
        final boolean b = item instanceof AbstractItem;
        long n = -1L;
        if (b) {
            id = ((AbstractItem)item).getId();
            if (id > 0) {
                n = id;
            }
            return n;
        }
        return -1L;
    }
    
    @Override
    public int getItemViewType(final int n) {
        return this.getItem(n).getLayoutResource();
    }
    
    public void onBindViewHolder(final ItemViewHolder itemViewHolder, final int n) {
        final IItem item = this.getItem(n);
        itemViewHolder.setEnabled(item.isEnabled());
        itemViewHolder.setItem(item);
        item.onBindView(itemViewHolder.itemView);
    }
    
    public ItemViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        final View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(n, viewGroup, false);
        final ItemViewHolder itemViewHolder = new ItemViewHolder(inflate);
        if (!"noBackground".equals(inflate.getTag())) {
            final TypedArray obtainStyledAttributes = viewGroup.getContext().obtainStyledAttributes(R.styleable.SuwRecyclerItemAdapter);
            Drawable drawable;
            if ((drawable = obtainStyledAttributes.getDrawable(R.styleable.SuwRecyclerItemAdapter_android_selectableItemBackground)) == null) {
                drawable = obtainStyledAttributes.getDrawable(R.styleable.SuwRecyclerItemAdapter_selectableItemBackground);
            }
            Drawable drawable2;
            if ((drawable2 = inflate.getBackground()) == null) {
                drawable2 = obtainStyledAttributes.getDrawable(R.styleable.SuwRecyclerItemAdapter_android_colorBackground);
            }
            if (drawable != null && drawable2 != null) {
                inflate.setBackgroundDrawable((Drawable)new PatchedLayerDrawable(new Drawable[] { drawable2, drawable }));
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("Cannot resolve required attributes. selectableItemBackground=");
                sb.append(drawable);
                sb.append(" background=");
                sb.append(drawable2);
                Log.e("RecyclerItemAdapter", sb.toString());
            }
            obtainStyledAttributes.recycle();
        }
        inflate.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                final IItem item = itemViewHolder.getItem();
                if (RecyclerItemAdapter.this.mListener != null && item != null && item.isEnabled()) {
                    RecyclerItemAdapter.this.mListener.onItemSelected(item);
                }
            }
        });
        return itemViewHolder;
    }
    
    @Override
    public void onItemRangeChanged(final ItemHierarchy itemHierarchy, final int n, final int n2) {
        ((RecyclerView.Adapter)this).notifyItemRangeChanged(n, n2);
    }
    
    @Override
    public void onItemRangeInserted(final ItemHierarchy itemHierarchy, final int n, final int n2) {
        ((RecyclerView.Adapter)this).notifyItemRangeInserted(n, n2);
    }
    
    public interface OnItemSelectedListener
    {
        void onItemSelected(final IItem p0);
    }
    
    static class PatchedLayerDrawable extends LayerDrawable
    {
        PatchedLayerDrawable(final Drawable[] array) {
            super(array);
        }
        
        public boolean getPadding(final Rect rect) {
            return super.getPadding(rect) && (rect.left != 0 || rect.top != 0 || rect.right != 0 || rect.bottom != 0);
        }
    }
}
