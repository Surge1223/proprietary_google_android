package com.android.setupwizardlib.items;

import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.BaseAdapter;

public class ItemAdapter extends BaseAdapter implements Observer
{
    private final ItemHierarchy mItemHierarchy;
    private ViewTypes mViewTypes;
    
    public ItemAdapter(final ItemHierarchy mItemHierarchy) {
        this.mViewTypes = new ViewTypes();
        (this.mItemHierarchy = mItemHierarchy).registerObserver((ItemHierarchy.Observer)this);
        this.refreshViewTypes();
    }
    
    private void refreshViewTypes() {
        for (int i = 0; i < this.getCount(); ++i) {
            this.mViewTypes.add(this.getItem(i).getLayoutResource());
        }
    }
    
    public int getCount() {
        return this.mItemHierarchy.getCount();
    }
    
    public IItem getItem(final int n) {
        return this.mItemHierarchy.getItemAt(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public int getItemViewType(int layoutResource) {
        layoutResource = this.getItem(layoutResource).getLayoutResource();
        return this.mViewTypes.get(layoutResource);
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        final IItem item = this.getItem(n);
        View inflate = view;
        if (view == null) {
            inflate = LayoutInflater.from(viewGroup.getContext()).inflate(item.getLayoutResource(), viewGroup, false);
        }
        item.onBindView(inflate);
        return inflate;
    }
    
    public int getViewTypeCount() {
        return this.mViewTypes.size();
    }
    
    public boolean isEnabled(final int n) {
        return this.getItem(n).isEnabled();
    }
    
    public void onChanged(final ItemHierarchy itemHierarchy) {
        this.refreshViewTypes();
        this.notifyDataSetChanged();
    }
    
    public void onItemRangeChanged(final ItemHierarchy itemHierarchy, final int n, final int n2) {
        this.onChanged(itemHierarchy);
    }
    
    public void onItemRangeInserted(final ItemHierarchy itemHierarchy, final int n, final int n2) {
        this.onChanged(itemHierarchy);
    }
    
    private static class ViewTypes
    {
        private SparseIntArray mPositionMap;
        private int nextPosition;
        
        private ViewTypes() {
            this.mPositionMap = new SparseIntArray();
            this.nextPosition = 0;
        }
        
        public int add(final int n) {
            if (this.mPositionMap.indexOfKey(n) < 0) {
                this.mPositionMap.put(n, this.nextPosition);
                ++this.nextPosition;
            }
            return this.mPositionMap.get(n);
        }
        
        public int get(final int n) {
            return this.mPositionMap.get(n);
        }
        
        public int size() {
            return this.mPositionMap.size();
        }
    }
}
