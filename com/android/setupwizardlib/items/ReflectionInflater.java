package com.android.setupwizardlib.items;

import android.view.InflateException;
import android.util.AttributeSet;
import android.content.Context;
import java.lang.reflect.Constructor;
import java.util.HashMap;

public abstract class ReflectionInflater<T> extends SimpleInflater<T>
{
    private static final Class<?>[] CONSTRUCTOR_SIGNATURE;
    private static final HashMap<String, Constructor<?>> sConstructorMap;
    private final Context mContext;
    private String mDefaultPackage;
    private final Object[] mTempConstructorArgs;
    
    static {
        CONSTRUCTOR_SIGNATURE = new Class[] { Context.class, AttributeSet.class };
        sConstructorMap = new HashMap<String, Constructor<?>>();
    }
    
    protected ReflectionInflater(final Context mContext) {
        super(mContext.getResources());
        this.mTempConstructorArgs = new Object[2];
        this.mContext = mContext;
    }
    
    public final T createItem(final String s, final String s2, final AttributeSet set) {
        String concat = s;
        if (s2 != null) {
            concat = s;
            if (s.indexOf(46) == -1) {
                concat = s2.concat(s);
            }
        }
        Constructor<?> constructor = null;
        Label_0095: {
            if ((constructor = ReflectionInflater.sConstructorMap.get(concat)) == null) {
                Label_0137: {
                    try {
                        constructor = this.mContext.getClassLoader().loadClass(concat).getConstructor(ReflectionInflater.CONSTRUCTOR_SIGNATURE);
                        constructor.setAccessible(true);
                        ReflectionInflater.sConstructorMap.put(s, constructor);
                    }
                    catch (Exception ex) {
                        break Label_0137;
                    }
                    break Label_0095;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append(set.getPositionDescription());
                sb.append(": Error inflating class ");
                sb.append(concat);
                final Exception ex;
                throw new InflateException(sb.toString(), (Throwable)ex);
            }
        }
        this.mTempConstructorArgs[0] = this.mContext;
        this.mTempConstructorArgs[1] = set;
        final Object instance = constructor.newInstance(this.mTempConstructorArgs);
        this.mTempConstructorArgs[0] = null;
        this.mTempConstructorArgs[1] = null;
        return (T)instance;
    }
    
    @Override
    protected T onCreateItem(final String s, final AttributeSet set) {
        return this.createItem(s, this.mDefaultPackage, set);
    }
    
    public void setDefaultPackage(final String mDefaultPackage) {
        this.mDefaultPackage = mDefaultPackage;
    }
}
