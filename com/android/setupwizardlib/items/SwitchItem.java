package com.android.setupwizardlib.items;

import android.widget.CompoundButton;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CompoundButton$OnCheckedChangeListener;

public class SwitchItem extends Item implements CompoundButton$OnCheckedChangeListener
{
    private boolean mChecked;
    private OnCheckedChangeListener mListener;
    
    public SwitchItem() {
        this.mChecked = false;
    }
    
    public SwitchItem(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChecked = false;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwSwitchItem);
        this.mChecked = obtainStyledAttributes.getBoolean(R.styleable.SuwSwitchItem_android_checked, false);
        obtainStyledAttributes.recycle();
    }
    
    @Override
    protected int getDefaultLayoutResource() {
        return R.layout.suw_items_switch;
    }
    
    @Override
    public void onBindView(final View view) {
        super.onBindView(view);
        final SwitchCompat switchCompat = (SwitchCompat)view.findViewById(R.id.suw_items_switch);
        switchCompat.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)null);
        switchCompat.setChecked(this.mChecked);
        switchCompat.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
        switchCompat.setEnabled(this.isEnabled());
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean mChecked) {
        this.mChecked = mChecked;
        if (this.mListener != null) {
            this.mListener.onCheckedChange(this, mChecked);
        }
    }
    
    public interface OnCheckedChangeListener
    {
        void onCheckedChange(final SwitchItem p0, final boolean p1);
    }
}
