package com.android.setupwizardlib.items;

import android.util.Xml;
import android.content.res.XmlResourceParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParser;
import android.view.InflateException;
import android.util.AttributeSet;
import android.content.res.Resources;

public abstract class SimpleInflater<T>
{
    protected final Resources mResources;
    
    protected SimpleInflater(final Resources mResources) {
        this.mResources = mResources;
    }
    
    private T createItemFromTag(final String s, final AttributeSet set) {
        try {
            return this.onCreateItem(s, set);
        }
        catch (Exception ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append(set.getPositionDescription());
            sb.append(": Error inflating class ");
            sb.append(s);
            throw new InflateException(sb.toString(), (Throwable)ex);
        }
        catch (InflateException ex2) {
            throw ex2;
        }
    }
    
    private void rInflate(final XmlPullParser xmlPullParser, final T t, final AttributeSet set) throws XmlPullParserException, IOException {
        final int depth = xmlPullParser.getDepth();
        while (true) {
            final int next = xmlPullParser.next();
            if ((next == 3 && xmlPullParser.getDepth() <= depth) || next == 1) {
                break;
            }
            if (next != 2) {
                continue;
            }
            if (this.onInterceptCreateItem(xmlPullParser, t, set)) {
                continue;
            }
            final T itemFromTag = this.createItemFromTag(xmlPullParser.getName(), set);
            this.onAddChildItem(t, itemFromTag);
            this.rInflate(xmlPullParser, itemFromTag, set);
        }
    }
    
    public Resources getResources() {
        return this.mResources;
    }
    
    public T inflate(final int n) {
        final XmlResourceParser xml = this.getResources().getXml(n);
        try {
            return this.inflate((XmlPullParser)xml);
        }
        finally {
            xml.close();
        }
    }
    
    public T inflate(final XmlPullParser xmlPullParser) {
        final AttributeSet attributeSet = Xml.asAttributeSet(xmlPullParser);
        try {
            int next;
            do {
                next = xmlPullParser.next();
            } while (next != 2 && next != 1);
            if (next == 2) {
                final T itemFromTag = this.createItemFromTag(xmlPullParser.getName(), attributeSet);
                this.rInflate(xmlPullParser, itemFromTag, attributeSet);
                return itemFromTag;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append(xmlPullParser.getPositionDescription());
            sb.append(": No start tag found!");
            throw new InflateException(sb.toString());
        }
        catch (IOException ex) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append(xmlPullParser.getPositionDescription());
            sb2.append(": ");
            sb2.append(ex.getMessage());
            throw new InflateException(sb2.toString(), (Throwable)ex);
        }
        catch (XmlPullParserException ex2) {
            throw new InflateException(ex2.getMessage(), (Throwable)ex2);
        }
    }
    
    protected abstract void onAddChildItem(final T p0, final T p1);
    
    protected abstract T onCreateItem(final String p0, final AttributeSet p1);
    
    protected boolean onInterceptCreateItem(final XmlPullParser xmlPullParser, final T t, final AttributeSet set) throws XmlPullParserException {
        return false;
    }
}
