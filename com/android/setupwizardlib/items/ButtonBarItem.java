package com.android.setupwizardlib.items;

import java.util.Iterator;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.view.View;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.ArrayList;

public class ButtonBarItem extends AbstractItem implements ItemParent
{
    private final ArrayList<ButtonItem> mButtons;
    private boolean mVisible;
    
    public ButtonBarItem() {
        this.mButtons = new ArrayList<ButtonItem>();
        this.mVisible = true;
    }
    
    public ButtonBarItem(final Context context, final AttributeSet set) {
        super(context, set);
        this.mButtons = new ArrayList<ButtonItem>();
        this.mVisible = true;
    }
    
    @Override
    public void addChild(final ItemHierarchy itemHierarchy) {
        if (itemHierarchy instanceof ButtonItem) {
            this.mButtons.add((ButtonItem)itemHierarchy);
            return;
        }
        throw new UnsupportedOperationException("Cannot add non-button item to Button Bar");
    }
    
    @Override
    public int getCount() {
        return this.isVisible() ? 1 : 0;
    }
    
    @Override
    public int getLayoutResource() {
        return R.layout.suw_items_button_bar;
    }
    
    @Override
    public int getViewId() {
        return this.getId();
    }
    
    @Override
    public boolean isEnabled() {
        return false;
    }
    
    public boolean isVisible() {
        return this.mVisible;
    }
    
    @Override
    public void onBindView(final View view) {
        final LinearLayout linearLayout = (LinearLayout)view;
        linearLayout.removeAllViews();
        final Iterator<ButtonItem> iterator = this.mButtons.iterator();
        while (iterator.hasNext()) {
            linearLayout.addView((View)iterator.next().createButton((ViewGroup)linearLayout));
        }
        view.setId(this.getViewId());
    }
}
