package com.android.setupwizardlib.items;

import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;

public class Item extends AbstractItem
{
    private boolean mEnabled;
    private Drawable mIcon;
    private int mLayoutRes;
    private CharSequence mSummary;
    private CharSequence mTitle;
    private boolean mVisible;
    
    public Item() {
        this.mEnabled = true;
        this.mVisible = true;
        this.mLayoutRes = this.getDefaultLayoutResource();
    }
    
    public Item(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEnabled = true;
        this.mVisible = true;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwItem);
        this.mEnabled = obtainStyledAttributes.getBoolean(R.styleable.SuwItem_android_enabled, true);
        this.mIcon = obtainStyledAttributes.getDrawable(R.styleable.SuwItem_android_icon);
        this.mTitle = obtainStyledAttributes.getText(R.styleable.SuwItem_android_title);
        this.mSummary = obtainStyledAttributes.getText(R.styleable.SuwItem_android_summary);
        this.mLayoutRes = obtainStyledAttributes.getResourceId(R.styleable.SuwItem_android_layout, this.getDefaultLayoutResource());
        this.mVisible = obtainStyledAttributes.getBoolean(R.styleable.SuwItem_android_visible, true);
        obtainStyledAttributes.recycle();
    }
    
    @Override
    public int getCount() {
        return this.isVisible() ? 1 : 0;
    }
    
    protected int getDefaultLayoutResource() {
        return R.layout.suw_items_default;
    }
    
    public Drawable getIcon() {
        return this.mIcon;
    }
    
    @Override
    public int getLayoutResource() {
        return this.mLayoutRes;
    }
    
    public CharSequence getSummary() {
        return this.mSummary;
    }
    
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    @Override
    public int getViewId() {
        return this.getId();
    }
    
    @Override
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public boolean isVisible() {
        return this.mVisible;
    }
    
    @Override
    public void onBindView(final View view) {
        ((TextView)view.findViewById(R.id.suw_items_title)).setText(this.getTitle());
        final TextView textView = (TextView)view.findViewById(R.id.suw_items_summary);
        final CharSequence summary = this.getSummary();
        if (summary != null && summary.length() > 0) {
            textView.setText(summary);
            textView.setVisibility(0);
        }
        else {
            textView.setVisibility(8);
        }
        final View viewById = view.findViewById(R.id.suw_items_icon_container);
        final Drawable icon = this.getIcon();
        if (icon != null) {
            final ImageView imageView = (ImageView)view.findViewById(R.id.suw_items_icon);
            imageView.setImageDrawable((Drawable)null);
            this.onMergeIconStateAndLevels(imageView, icon);
            imageView.setImageDrawable(icon);
            viewById.setVisibility(0);
        }
        else {
            viewById.setVisibility(8);
        }
        view.setId(this.getViewId());
    }
    
    protected void onMergeIconStateAndLevels(final ImageView imageView, final Drawable drawable) {
        imageView.setImageState(drawable.getState(), false);
        imageView.setImageLevel(drawable.getLevel());
    }
}
