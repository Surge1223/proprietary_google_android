package com.android.setupwizardlib.items;

import android.util.AttributeSet;
import android.content.Context;

public abstract class AbstractItem extends AbstractItemHierarchy implements IItem
{
    public AbstractItem() {
    }
    
    public AbstractItem(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    @Override
    public int getCount() {
        return 1;
    }
    
    @Override
    public IItem getItemAt(final int n) {
        return this;
    }
    
    public void notifyItemChanged() {
        this.notifyItemRangeChanged(0, 1);
    }
}
