package com.android.setupwizardlib.items;

import android.util.Log;
import android.util.AttributeSet;
import android.content.Context;
import java.util.ArrayList;
import android.util.SparseIntArray;
import java.util.List;

public class ItemGroup extends AbstractItemHierarchy implements Observer, ItemParent
{
    private List<ItemHierarchy> mChildren;
    private int mCount;
    private boolean mDirty;
    private SparseIntArray mHierarchyStart;
    
    public ItemGroup() {
        this.mChildren = new ArrayList<ItemHierarchy>();
        this.mHierarchyStart = new SparseIntArray();
        this.mCount = 0;
        this.mDirty = false;
    }
    
    public ItemGroup(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChildren = new ArrayList<ItemHierarchy>();
        this.mHierarchyStart = new SparseIntArray();
        this.mCount = 0;
        this.mDirty = false;
    }
    
    private static int binarySearch(final SparseIntArray sparseIntArray, final int n) {
        int size = sparseIntArray.size();
        int i = 0;
        --size;
        while (i <= size) {
            final int n2 = i + size >>> 1;
            final int value = sparseIntArray.valueAt(n2);
            if (value < n) {
                i = n2 + 1;
            }
            else {
                if (value <= n) {
                    return sparseIntArray.keyAt(n2);
                }
                size = n2 - 1;
            }
        }
        return sparseIntArray.keyAt(i - 1);
    }
    
    private int getChildPosition(int value) {
        this.updateDataIfNeeded();
        if (value != -1) {
            final int size = this.mChildren.size();
            final int n = -1;
            int n2;
            for (n2 = value, value = n; value < 0 && n2 < size; value = this.mHierarchyStart.get(n2, -1), ++n2) {}
            int count;
            if ((count = value) < 0) {
                count = this.getCount();
            }
            return count;
        }
        return -1;
    }
    
    private int getChildPosition(final ItemHierarchy itemHierarchy) {
        return this.getChildPosition(identityIndexOf(this.mChildren, itemHierarchy));
    }
    
    private int getItemIndex(int binarySearch) {
        this.updateDataIfNeeded();
        if (binarySearch < 0 || binarySearch >= this.mCount) {
            final StringBuilder sb = new StringBuilder();
            sb.append("size=");
            sb.append(this.mCount);
            sb.append("; index=");
            sb.append(binarySearch);
            throw new IndexOutOfBoundsException(sb.toString());
        }
        binarySearch = binarySearch(this.mHierarchyStart, binarySearch);
        if (binarySearch >= 0) {
            return binarySearch;
        }
        throw new IllegalStateException("Cannot have item start index < 0");
    }
    
    private static <T> int identityIndexOf(final List<T> list, final T t) {
        for (int size = list.size(), i = 0; i < size; ++i) {
            if (list.get(i) == t) {
                return i;
            }
        }
        return -1;
    }
    
    private void updateDataIfNeeded() {
        if (this.mDirty) {
            this.mCount = 0;
            this.mHierarchyStart.clear();
            for (int i = 0; i < this.mChildren.size(); ++i) {
                final ItemHierarchy itemHierarchy = this.mChildren.get(i);
                if (itemHierarchy.getCount() > 0) {
                    this.mHierarchyStart.put(i, this.mCount);
                }
                this.mCount += itemHierarchy.getCount();
            }
            this.mDirty = false;
        }
    }
    
    @Override
    public void addChild(final ItemHierarchy itemHierarchy) {
        this.mDirty = true;
        this.mChildren.add(itemHierarchy);
        itemHierarchy.registerObserver((ItemHierarchy.Observer)this);
        final int count = itemHierarchy.getCount();
        if (count > 0) {
            this.notifyItemRangeInserted(this.getChildPosition(itemHierarchy), count);
        }
    }
    
    @Override
    public int getCount() {
        this.updateDataIfNeeded();
        return this.mCount;
    }
    
    @Override
    public IItem getItemAt(final int n) {
        final int itemIndex = this.getItemIndex(n);
        return this.mChildren.get(itemIndex).getItemAt(n - this.mHierarchyStart.get(itemIndex));
    }
    
    @Override
    public void onItemRangeChanged(final ItemHierarchy itemHierarchy, final int n, final int n2) {
        final int childPosition = this.getChildPosition(itemHierarchy);
        if (childPosition >= 0) {
            this.notifyItemRangeChanged(childPosition + n, n2);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected child change ");
            sb.append(itemHierarchy);
            Log.e("ItemGroup", sb.toString());
        }
    }
    
    @Override
    public void onItemRangeInserted(final ItemHierarchy itemHierarchy, final int n, final int n2) {
        this.mDirty = true;
        final int childPosition = this.getChildPosition(itemHierarchy);
        if (childPosition >= 0) {
            this.notifyItemRangeInserted(childPosition + n, n2);
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unexpected child insert ");
            sb.append(itemHierarchy);
            Log.e("ItemGroup", sb.toString());
        }
    }
}
