package com.android.setupwizardlib.items;

import android.view.View;
import android.view.ContextThemeWrapper;
import android.annotation.SuppressLint;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.content.Context;
import com.android.setupwizardlib.R;
import android.widget.Button;
import android.view.View.OnClickListener;

public class ButtonItem extends AbstractItem implements View.OnClickListener
{
    private Button mButton;
    private boolean mEnabled;
    private OnClickListener mListener;
    private CharSequence mText;
    private int mTheme;
    
    public ButtonItem() {
        this.mEnabled = true;
        this.mTheme = R.style.SuwButtonItem;
    }
    
    public ButtonItem(final Context context, final AttributeSet set) {
        super(context, set);
        this.mEnabled = true;
        this.mTheme = R.style.SuwButtonItem;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwButtonItem);
        this.mEnabled = obtainStyledAttributes.getBoolean(R.styleable.SuwButtonItem_android_enabled, true);
        this.mText = obtainStyledAttributes.getText(R.styleable.SuwButtonItem_android_text);
        this.mTheme = obtainStyledAttributes.getResourceId(R.styleable.SuwButtonItem_android_theme, R.style.SuwButtonItem);
        obtainStyledAttributes.recycle();
    }
    
    @SuppressLint({ "InflateParams" })
    private Button createButton(final Context context) {
        return (Button)LayoutInflater.from(context).inflate(R.layout.suw_button, (ViewGroup)null, false);
    }
    
    protected Button createButton(final ViewGroup viewGroup) {
        if (this.mButton == null) {
            Object context = viewGroup.getContext();
            if (this.mTheme != 0) {
                context = new ContextThemeWrapper((Context)context, this.mTheme);
            }
            (this.mButton = this.createButton((Context)context)).setOnClickListener((View.OnClickListener)this);
        }
        else if (this.mButton.getParent() instanceof ViewGroup) {
            ((ViewGroup)this.mButton.getParent()).removeView((View)this.mButton);
        }
        this.mButton.setEnabled(this.mEnabled);
        this.mButton.setText(this.mText);
        this.mButton.setId(this.getViewId());
        return this.mButton;
    }
    
    @Override
    public int getCount() {
        return 0;
    }
    
    public int getLayoutResource() {
        return 0;
    }
    
    public boolean isEnabled() {
        return this.mEnabled;
    }
    
    public final void onBindView(final View view) {
        throw new UnsupportedOperationException("Cannot bind to ButtonItem's view");
    }
    
    public void onClick(final View view) {
        if (this.mListener != null) {
            this.mListener.onClick(this);
        }
    }
    
    public interface OnClickListener
    {
        void onClick(final ButtonItem p0);
    }
}
