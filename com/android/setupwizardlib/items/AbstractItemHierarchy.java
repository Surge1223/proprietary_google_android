package com.android.setupwizardlib.items;

import java.util.Iterator;
import android.util.Log;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import java.util.ArrayList;

public abstract class AbstractItemHierarchy implements ItemHierarchy
{
    private int mId;
    private ArrayList<Observer> mObservers;
    
    public AbstractItemHierarchy() {
        this.mObservers = new ArrayList<Observer>();
        this.mId = 0;
    }
    
    public AbstractItemHierarchy(final Context context, final AttributeSet set) {
        this.mObservers = new ArrayList<Observer>();
        this.mId = 0;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwAbstractItem);
        this.mId = obtainStyledAttributes.getResourceId(R.styleable.SuwAbstractItem_android_id, 0);
        obtainStyledAttributes.recycle();
    }
    
    public int getId() {
        return this.mId;
    }
    
    public int getViewId() {
        return this.getId();
    }
    
    public void notifyItemRangeChanged(final int n, final int n2) {
        if (n < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("notifyItemRangeChanged: Invalid position=");
            sb.append(n);
            Log.w("AbstractItemHierarchy", sb.toString());
            return;
        }
        if (n2 < 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("notifyItemRangeChanged: Invalid itemCount=");
            sb2.append(n2);
            Log.w("AbstractItemHierarchy", sb2.toString());
            return;
        }
        final Iterator<Observer> iterator = this.mObservers.iterator();
        while (iterator.hasNext()) {
            iterator.next().onItemRangeChanged(this, n, n2);
        }
    }
    
    public void notifyItemRangeInserted(final int n, final int n2) {
        if (n < 0) {
            final StringBuilder sb = new StringBuilder();
            sb.append("notifyItemRangeInserted: Invalid position=");
            sb.append(n);
            Log.w("AbstractItemHierarchy", sb.toString());
            return;
        }
        if (n2 < 0) {
            final StringBuilder sb2 = new StringBuilder();
            sb2.append("notifyItemRangeInserted: Invalid itemCount=");
            sb2.append(n2);
            Log.w("AbstractItemHierarchy", sb2.toString());
            return;
        }
        final Iterator<Observer> iterator = this.mObservers.iterator();
        while (iterator.hasNext()) {
            iterator.next().onItemRangeInserted(this, n, n2);
        }
    }
    
    @Override
    public void registerObserver(final Observer observer) {
        this.mObservers.add(observer);
    }
}
