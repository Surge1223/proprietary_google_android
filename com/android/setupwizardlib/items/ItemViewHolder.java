package com.android.setupwizardlib.items;

import android.view.View;
import com.android.setupwizardlib.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;

class ItemViewHolder extends ViewHolder implements DividedViewHolder
{
    private boolean mIsEnabled;
    private IItem mItem;
    
    ItemViewHolder(final View view) {
        super(view);
    }
    
    public IItem getItem() {
        return this.mItem;
    }
    
    @Override
    public boolean isDividerAllowedAbove() {
        return this.mIsEnabled;
    }
    
    @Override
    public boolean isDividerAllowedBelow() {
        return this.mIsEnabled;
    }
    
    public void setEnabled(final boolean b) {
        this.mIsEnabled = b;
        this.itemView.setClickable(b);
        this.itemView.setEnabled(b);
        this.itemView.setFocusable(b);
    }
    
    public void setItem(final IItem mItem) {
        this.mItem = mItem;
    }
}
