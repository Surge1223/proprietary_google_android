package com.android.setupwizardlib.items;

import com.android.setupwizardlib.view.CheckableLinearLayout;
import android.graphics.drawable.Drawable;
import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.graphics.PorterDuff.Mode;
import android.widget.TextView;
import android.view.View;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.View.OnClickListener;

public class ExpandableSwitchItem extends SwitchItem implements View.OnClickListener, CompoundButton$OnCheckedChangeListener
{
    private CharSequence mCollapsedSummary;
    private CharSequence mExpandedSummary;
    private boolean mIsExpanded;
    
    public ExpandableSwitchItem() {
        this.mIsExpanded = false;
    }
    
    public ExpandableSwitchItem(final Context context, final AttributeSet set) {
        super(context, set);
        this.mIsExpanded = false;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwExpandableSwitchItem);
        this.mCollapsedSummary = obtainStyledAttributes.getText(R.styleable.SuwExpandableSwitchItem_suwCollapsedSummary);
        this.mExpandedSummary = obtainStyledAttributes.getText(R.styleable.SuwExpandableSwitchItem_suwExpandedSummary);
        obtainStyledAttributes.recycle();
    }
    
    private void tintCompoundDrawables(final View view) {
        final Context context = view.getContext();
        final int n = 0;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { 16842806 });
        final ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(0);
        obtainStyledAttributes.recycle();
        if (colorStateList != null) {
            final TextView textView = (TextView)view.findViewById(R.id.suw_items_title);
            for (final Drawable drawable : textView.getCompoundDrawables()) {
                if (drawable != null) {
                    drawable.setColorFilter(colorStateList.getDefaultColor(), PorterDuff.Mode.SRC_IN);
                }
            }
            if (Build.VERSION.SDK_INT >= 17) {
                final Drawable[] compoundDrawablesRelative = textView.getCompoundDrawablesRelative();
                for (int length2 = compoundDrawablesRelative.length, j = n; j < length2; ++j) {
                    final Drawable drawable2 = compoundDrawablesRelative[j];
                    if (drawable2 != null) {
                        drawable2.setColorFilter(colorStateList.getDefaultColor(), PorterDuff.Mode.SRC_IN);
                    }
                }
            }
        }
    }
    
    public CharSequence getCollapsedSummary() {
        return this.mCollapsedSummary;
    }
    
    @Override
    protected int getDefaultLayoutResource() {
        return R.layout.suw_items_expandable_switch;
    }
    
    public CharSequence getExpandedSummary() {
        return this.mExpandedSummary;
    }
    
    public CharSequence getSummary() {
        CharSequence charSequence;
        if (this.mIsExpanded) {
            charSequence = this.getExpandedSummary();
        }
        else {
            charSequence = this.getCollapsedSummary();
        }
        return charSequence;
    }
    
    public boolean isExpanded() {
        return this.mIsExpanded;
    }
    
    @Override
    public void onBindView(final View view) {
        super.onBindView(view);
        final View viewById = view.findViewById(R.id.suw_items_expandable_switch_content);
        viewById.setOnClickListener((View.OnClickListener)this);
        if (viewById instanceof CheckableLinearLayout) {
            ((CheckableLinearLayout)viewById).setChecked(this.isExpanded());
        }
        this.tintCompoundDrawables(view);
        view.setFocusable(false);
    }
    
    public void onClick(final View view) {
        this.setExpanded(this.isExpanded() ^ true);
    }
    
    public void setExpanded(final boolean mIsExpanded) {
        if (this.mIsExpanded == mIsExpanded) {
            return;
        }
        this.mIsExpanded = mIsExpanded;
        this.notifyItemChanged();
    }
}
