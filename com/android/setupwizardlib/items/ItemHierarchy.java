package com.android.setupwizardlib.items;

public interface ItemHierarchy
{
    int getCount();
    
    IItem getItemAt(final int p0);
    
    void registerObserver(final Observer p0);
    
    public interface Observer
    {
        void onItemRangeChanged(final ItemHierarchy p0, final int p1, final int p2);
        
        void onItemRangeInserted(final ItemHierarchy p0, final int p1, final int p2);
    }
}
