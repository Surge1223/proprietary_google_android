package com.android.setupwizardlib;

import android.view.View;
import android.view.LayoutInflater;
import android.widget.ListView;
import android.graphics.drawable.Drawable;
import android.widget.ListAdapter;
import android.view.ViewGroup;
import com.android.setupwizardlib.template.ListViewScrollHandlingDelegate;
import com.android.setupwizardlib.template.RequireScrollMixin;
import android.util.AttributeSet;
import android.content.Context;
import com.android.setupwizardlib.template.ListMixin;

public class SetupWizardListLayout extends SetupWizardLayout
{
    private ListMixin mListMixin;
    
    public SetupWizardListLayout(final Context context) {
        this(context, 0, 0);
    }
    
    public SetupWizardListLayout(final Context context, final int n, final int n2) {
        super(context, n, n2);
        this.init(context, null, 0);
    }
    
    public SetupWizardListLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(context, set, 0);
    }
    
    public SetupWizardListLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(context, set, n);
    }
    
    private void init(final Context context, final AttributeSet set, final int n) {
        this.registerMixin(ListMixin.class, this.mListMixin = new ListMixin(this, set, n));
        final RequireScrollMixin requireScrollMixin = this.getMixin(RequireScrollMixin.class);
        requireScrollMixin.setScrollHandlingDelegate((RequireScrollMixin.ScrollHandlingDelegate)new ListViewScrollHandlingDelegate(requireScrollMixin, this.getListView()));
    }
    
    @Override
    protected ViewGroup findContainer(final int n) {
        int n2 = n;
        if (n == 0) {
            n2 = 16908298;
        }
        return super.findContainer(n2);
    }
    
    public ListAdapter getAdapter() {
        return this.mListMixin.getAdapter();
    }
    
    public Drawable getDivider() {
        return this.mListMixin.getDivider();
    }
    
    @Deprecated
    public int getDividerInset() {
        return this.mListMixin.getDividerInset();
    }
    
    public int getDividerInsetEnd() {
        return this.mListMixin.getDividerInsetEnd();
    }
    
    public int getDividerInsetStart() {
        return this.mListMixin.getDividerInsetStart();
    }
    
    public ListView getListView() {
        return this.mListMixin.getListView();
    }
    
    @Override
    protected View onInflateTemplate(final LayoutInflater layoutInflater, final int n) {
        int suw_list_template = n;
        if (n == 0) {
            suw_list_template = R.layout.suw_list_template;
        }
        return super.onInflateTemplate(layoutInflater, suw_list_template);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.mListMixin.onLayout();
    }
    
    public void setAdapter(final ListAdapter adapter) {
        this.mListMixin.setAdapter(adapter);
    }
    
    @Deprecated
    public void setDividerInset(final int dividerInset) {
        this.mListMixin.setDividerInset(dividerInset);
    }
}
