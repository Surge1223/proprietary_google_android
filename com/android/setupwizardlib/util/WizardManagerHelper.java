package com.android.setupwizardlib.util;

import java.util.Iterator;
import java.util.Arrays;
import android.content.Intent;

public class WizardManagerHelper
{
    static final String EXTRA_ACTION_ID = "actionId";
    static final String EXTRA_IS_DEFERRED_SETUP = "deferredSetup";
    static final String EXTRA_IS_FIRST_RUN = "firstRun";
    static final String EXTRA_IS_PRE_DEFERRED_SETUP = "preDeferredSetup";
    static final String EXTRA_SCRIPT_URI = "scriptUri";
    static final String EXTRA_WIZARD_BUNDLE = "wizardBundle";
    
    public static void copyWizardManagerExtras(final Intent intent, final Intent intent2) {
        intent2.putExtra("wizardBundle", intent.getBundleExtra("wizardBundle"));
        for (final String s : Arrays.asList("firstRun", "deferredSetup", "preDeferredSetup")) {
            intent2.putExtra(s, intent.getBooleanExtra(s, false));
        }
        for (final String s2 : Arrays.asList("theme", "scriptUri", "actionId")) {
            intent2.putExtra(s2, intent.getStringExtra(s2));
        }
    }
    
    public static boolean isDeferredSetupWizard(final Intent intent) {
        boolean b = false;
        if (intent != null) {
            b = b;
            if (intent.getBooleanExtra("deferredSetup", false)) {
                b = true;
            }
        }
        return b;
    }
    
    public static boolean isSetupWizardIntent(final Intent intent) {
        return intent.getBooleanExtra("firstRun", false);
    }
}
