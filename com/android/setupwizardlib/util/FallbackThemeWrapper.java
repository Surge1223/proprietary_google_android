package com.android.setupwizardlib.util;

import android.content.res.Resources.Theme;
import android.content.Context;
import android.view.ContextThemeWrapper;

public class FallbackThemeWrapper extends ContextThemeWrapper
{
    public FallbackThemeWrapper(final Context context, final int n) {
        super(context, n);
    }
    
    protected void onApplyThemeResource(final Resources.Theme resources$Theme, final int n, final boolean b) {
        resources$Theme.applyStyle(n, false);
    }
}
