package com.android.setupwizardlib.util;

import android.util.Log;
import java.util.List;
import android.text.Layout;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.ViewGroup;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeProviderCompat;
import android.view.accessibility.AccessibilityEvent;
import android.view.View;
import android.support.v4.widget.ExploreByTouchHelper;
import android.view.MotionEvent;
import android.os.Build.VERSION;
import android.widget.TextView;
import android.support.v4.view.AccessibilityDelegateCompat;

public class LinkAccessibilityHelper extends AccessibilityDelegateCompat
{
    private final AccessibilityDelegateCompat mDelegate;
    
    LinkAccessibilityHelper(final AccessibilityDelegateCompat mDelegate) {
        this.mDelegate = mDelegate;
    }
    
    public LinkAccessibilityHelper(final TextView textView) {
        AccessibilityDelegateCompat accessibilityDelegateCompat;
        if (Build.VERSION.SDK_INT >= 26) {
            accessibilityDelegateCompat = new AccessibilityDelegateCompat();
        }
        else {
            accessibilityDelegateCompat = new PreOLinkAccessibilityHelper(textView);
        }
        this(accessibilityDelegateCompat);
    }
    
    public final boolean dispatchHoverEvent(final MotionEvent motionEvent) {
        return this.mDelegate instanceof ExploreByTouchHelper && ((ExploreByTouchHelper)this.mDelegate).dispatchHoverEvent(motionEvent);
    }
    
    @Override
    public boolean dispatchPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        return this.mDelegate.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }
    
    @Override
    public AccessibilityNodeProviderCompat getAccessibilityNodeProvider(final View view) {
        return this.mDelegate.getAccessibilityNodeProvider(view);
    }
    
    @Override
    public void onInitializeAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        this.mDelegate.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }
    
    @Override
    public void onInitializeAccessibilityNodeInfo(final View view, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        this.mDelegate.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
    }
    
    @Override
    public void onPopulateAccessibilityEvent(final View view, final AccessibilityEvent accessibilityEvent) {
        this.mDelegate.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }
    
    @Override
    public boolean onRequestSendAccessibilityEvent(final ViewGroup viewGroup, final View view, final AccessibilityEvent accessibilityEvent) {
        return this.mDelegate.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }
    
    @Override
    public boolean performAccessibilityAction(final View view, final int n, final Bundle bundle) {
        return this.mDelegate.performAccessibilityAction(view, n, bundle);
    }
    
    @Override
    public void sendAccessibilityEvent(final View view, final int n) {
        this.mDelegate.sendAccessibilityEvent(view, n);
    }
    
    @Override
    public void sendAccessibilityEventUnchecked(final View view, final AccessibilityEvent accessibilityEvent) {
        this.mDelegate.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }
    
    static class PreOLinkAccessibilityHelper extends ExploreByTouchHelper
    {
        private final Rect mTempRect;
        private final TextView mView;
        
        PreOLinkAccessibilityHelper(final TextView mView) {
            super((View)mView);
            this.mTempRect = new Rect();
            this.mView = mView;
        }
        
        private static float convertToLocalHorizontalCoordinate(final TextView textView, float max) {
            max = Math.max(0.0f, max - textView.getTotalPaddingLeft());
            return Math.min(textView.getWidth() - textView.getTotalPaddingRight() - 1, max) + textView.getScrollX();
        }
        
        private Rect getBoundsForSpan(final ClickableSpan clickableSpan, final Rect rect) {
            final CharSequence text = this.mView.getText();
            rect.setEmpty();
            if (text instanceof Spanned) {
                final Layout layout = this.mView.getLayout();
                if (layout != null) {
                    final Spanned spanned = (Spanned)text;
                    final int spanStart = spanned.getSpanStart((Object)clickableSpan);
                    final int spanEnd = spanned.getSpanEnd((Object)clickableSpan);
                    final float primaryHorizontal = layout.getPrimaryHorizontal(spanStart);
                    final float primaryHorizontal2 = layout.getPrimaryHorizontal(spanEnd);
                    final int lineForOffset = layout.getLineForOffset(spanStart);
                    final int lineForOffset2 = layout.getLineForOffset(spanEnd);
                    layout.getLineBounds(lineForOffset, rect);
                    if (lineForOffset2 == lineForOffset) {
                        rect.left = (int)Math.min(primaryHorizontal, primaryHorizontal2);
                        rect.right = (int)Math.max(primaryHorizontal, primaryHorizontal2);
                    }
                    else if (layout.getParagraphDirection(lineForOffset) == -1) {
                        rect.right = (int)primaryHorizontal;
                    }
                    else {
                        rect.left = (int)primaryHorizontal;
                    }
                    rect.offset(this.mView.getTotalPaddingLeft(), this.mView.getTotalPaddingTop());
                }
            }
            return rect;
        }
        
        private static int getLineAtCoordinate(final TextView textView, float max) {
            max = Math.max(0.0f, max - textView.getTotalPaddingTop());
            final float min = Math.min(textView.getHeight() - textView.getTotalPaddingBottom() - 1, max);
            max = textView.getScrollY();
            return textView.getLayout().getLineForVertical((int)(min + max));
        }
        
        private static int getOffsetAtCoordinate(final TextView textView, final int n, float convertToLocalHorizontalCoordinate) {
            convertToLocalHorizontalCoordinate = convertToLocalHorizontalCoordinate(textView, convertToLocalHorizontalCoordinate);
            return textView.getLayout().getOffsetForHorizontal(n, convertToLocalHorizontalCoordinate);
        }
        
        private static int getOffsetForPosition(final TextView textView, final float n, final float n2) {
            if (textView.getLayout() == null) {
                return -1;
            }
            return getOffsetAtCoordinate(textView, getLineAtCoordinate(textView, n2), n);
        }
        
        private ClickableSpan getSpanForOffset(final int n) {
            final CharSequence text = this.mView.getText();
            if (text instanceof Spanned) {
                final ClickableSpan[] array = (ClickableSpan[])((Spanned)text).getSpans(n, n, (Class)ClickableSpan.class);
                if (array.length == 1) {
                    return array[0];
                }
            }
            return null;
        }
        
        private CharSequence getTextForSpan(final ClickableSpan clickableSpan) {
            final CharSequence text = this.mView.getText();
            if (text instanceof Spanned) {
                final Spanned spanned = (Spanned)text;
                return spanned.subSequence(spanned.getSpanStart((Object)clickableSpan), spanned.getSpanEnd((Object)clickableSpan));
            }
            return text;
        }
        
        @Override
        protected int getVirtualViewAt(final float n, final float n2) {
            final CharSequence text = this.mView.getText();
            if (text instanceof Spanned) {
                final Spanned spanned = (Spanned)text;
                final int offsetForPosition = getOffsetForPosition(this.mView, n, n2);
                final ClickableSpan[] array = (ClickableSpan[])spanned.getSpans(offsetForPosition, offsetForPosition, (Class)ClickableSpan.class);
                if (array.length == 1) {
                    return spanned.getSpanStart((Object)array[0]);
                }
            }
            return Integer.MIN_VALUE;
        }
        
        @Override
        protected void getVisibleVirtualViews(final List<Integer> list) {
            final CharSequence text = this.mView.getText();
            if (text instanceof Spanned) {
                final Spanned spanned = (Spanned)text;
                final int length = spanned.length();
                int i = 0;
                for (ClickableSpan[] array = (ClickableSpan[])spanned.getSpans(0, length, (Class)ClickableSpan.class); i < array.length; ++i) {
                    list.add(spanned.getSpanStart((Object)array[i]));
                }
            }
        }
        
        @Override
        protected boolean onPerformActionForVirtualView(final int n, final int n2, final Bundle bundle) {
            if (n2 == 16) {
                final ClickableSpan spanForOffset = this.getSpanForOffset(n);
                if (spanForOffset != null) {
                    spanForOffset.onClick((View)this.mView);
                    return true;
                }
                final StringBuilder sb = new StringBuilder();
                sb.append("LinkSpan is null for offset: ");
                sb.append(n);
                Log.e("LinkAccessibilityHelper", sb.toString());
            }
            return false;
        }
        
        @Override
        protected void onPopulateEventForVirtualView(final int n, final AccessibilityEvent accessibilityEvent) {
            final ClickableSpan spanForOffset = this.getSpanForOffset(n);
            if (spanForOffset != null) {
                accessibilityEvent.setContentDescription(this.getTextForSpan(spanForOffset));
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("LinkSpan is null for offset: ");
                sb.append(n);
                Log.e("LinkAccessibilityHelper", sb.toString());
                accessibilityEvent.setContentDescription(this.mView.getText());
            }
        }
        
        @Override
        protected void onPopulateNodeForVirtualView(final int n, final AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            final ClickableSpan spanForOffset = this.getSpanForOffset(n);
            if (spanForOffset != null) {
                accessibilityNodeInfoCompat.setContentDescription(this.getTextForSpan(spanForOffset));
            }
            else {
                final StringBuilder sb = new StringBuilder();
                sb.append("LinkSpan is null for offset: ");
                sb.append(n);
                Log.e("LinkAccessibilityHelper", sb.toString());
                accessibilityNodeInfoCompat.setContentDescription(this.mView.getText());
            }
            accessibilityNodeInfoCompat.setFocusable(true);
            accessibilityNodeInfoCompat.setClickable(true);
            this.getBoundsForSpan(spanForOffset, this.mTempRect);
            if (this.mTempRect.isEmpty()) {
                final StringBuilder sb2 = new StringBuilder();
                sb2.append("LinkSpan bounds is empty for: ");
                sb2.append(n);
                Log.e("LinkAccessibilityHelper", sb2.toString());
                this.mTempRect.set(0, 0, 1, 1);
            }
            accessibilityNodeInfoCompat.setBoundsInParent(this.mTempRect);
            accessibilityNodeInfoCompat.addAction(16);
        }
    }
}
