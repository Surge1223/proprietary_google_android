package com.android.setupwizardlib.util;

import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import android.graphics.drawable.InsetDrawable;
import android.view.View;
import android.graphics.drawable.Drawable;

public class DrawableLayoutDirectionHelper
{
    @SuppressLint({ "InlinedApi" })
    public static InsetDrawable createRelativeInsetDrawable(final Drawable drawable, final int n, final int n2, final int n3, final int n4, final View view) {
        final int sdk_INT = Build.VERSION.SDK_INT;
        boolean b = true;
        if (sdk_INT < 17 || view.getLayoutDirection() != 1) {
            b = false;
        }
        return createRelativeInsetDrawable(drawable, n, n2, n3, n4, b);
    }
    
    private static InsetDrawable createRelativeInsetDrawable(final Drawable drawable, final int n, final int n2, final int n3, final int n4, final boolean b) {
        if (b) {
            return new InsetDrawable(drawable, n3, n2, n, n4);
        }
        return new InsetDrawable(drawable, n, n2, n3, n4);
    }
}
