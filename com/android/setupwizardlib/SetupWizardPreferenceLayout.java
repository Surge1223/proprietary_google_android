package com.android.setupwizardlib;

import com.android.setupwizardlib.template.RecyclerMixin;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.util.AttributeSet;
import android.content.Context;

public class SetupWizardPreferenceLayout extends SetupWizardRecyclerLayout
{
    public SetupWizardPreferenceLayout(final Context context) {
        super(context);
    }
    
    public SetupWizardPreferenceLayout(final Context context, final AttributeSet set) {
        super(context, set);
    }
    
    public SetupWizardPreferenceLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    @Override
    protected ViewGroup findContainer(final int n) {
        int suw_layout_content = n;
        if (n == 0) {
            suw_layout_content = R.id.suw_layout_content;
        }
        return super.findContainer(suw_layout_content);
    }
    
    @Override
    protected View onInflateTemplate(final LayoutInflater layoutInflater, final int n) {
        int suw_preference_template = n;
        if (n == 0) {
            suw_preference_template = R.layout.suw_preference_template;
        }
        return super.onInflateTemplate(layoutInflater, suw_preference_template);
    }
    
    @Override
    protected void onTemplateInflated() {
        this.mRecyclerMixin = new RecyclerMixin(this, (RecyclerView)LayoutInflater.from(this.getContext()).inflate(R.layout.suw_preference_recycler_view, (ViewGroup)this, false));
    }
}
