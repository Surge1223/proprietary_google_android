package com.android.setupwizardlib.view;

import android.text.Selection;
import android.text.Spannable;
import android.widget.TextView;
import android.text.method.LinkMovementMethod;
import android.view.MotionEvent;

public interface TouchableMovementMethod
{
    MotionEvent getLastTouchEvent();
    
    boolean isLastTouchEventHandled();
    
    public static class TouchableLinkMovementMethod extends LinkMovementMethod implements TouchableMovementMethod
    {
        MotionEvent mLastEvent;
        boolean mLastEventResult;
        
        public TouchableLinkMovementMethod() {
            this.mLastEventResult = false;
        }
        
        public static TouchableLinkMovementMethod getInstance() {
            return new TouchableLinkMovementMethod();
        }
        
        public MotionEvent getLastTouchEvent() {
            return this.mLastEvent;
        }
        
        public boolean isLastTouchEventHandled() {
            return this.mLastEventResult;
        }
        
        public boolean onTouchEvent(final TextView textView, final Spannable spannable, final MotionEvent mLastEvent) {
            this.mLastEvent = mLastEvent;
            final boolean onTouchEvent = super.onTouchEvent(textView, spannable, mLastEvent);
            if (mLastEvent.getAction() == 0) {
                this.mLastEventResult = (Selection.getSelectionStart((CharSequence)spannable) != -1);
            }
            else {
                this.mLastEventResult = onTouchEvent;
            }
            return onTouchEvent;
        }
    }
}
