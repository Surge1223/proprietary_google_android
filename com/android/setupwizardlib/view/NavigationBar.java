package com.android.setupwizardlib.view;

import android.view.ViewGroup;
import android.view.View;
import android.view.ContextThemeWrapper;
import android.content.res.TypedArray;
import android.graphics.Color;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class NavigationBar extends LinearLayout implements View.OnClickListener
{
    private Button mBackButton;
    private NavigationBarListener mListener;
    private Button mMoreButton;
    private Button mNextButton;
    
    public NavigationBar(final Context context) {
        super(getThemedContext(context));
        this.init();
    }
    
    public NavigationBar(final Context context, final AttributeSet set) {
        super(getThemedContext(context), set);
        this.init();
    }
    
    public NavigationBar(final Context context, final AttributeSet set, final int n) {
        super(getThemedContext(context), set, n);
        this.init();
    }
    
    private static int getNavbarTheme(final Context context) {
        final int suwNavBarTheme = R.attr.suwNavBarTheme;
        final boolean b = true;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { suwNavBarTheme, 16842800, 16842801 });
        int n;
        if ((n = obtainStyledAttributes.getResourceId(0, 0)) == 0) {
            final float[] array = new float[3];
            final float[] array2 = new float[3];
            Color.colorToHSV(obtainStyledAttributes.getColor(1, 0), array);
            Color.colorToHSV(obtainStyledAttributes.getColor(2, 0), array2);
            if (array[2] > array2[2] && b) {
                n = R.style.SuwNavBarThemeDark;
            }
            else {
                n = R.style.SuwNavBarThemeLight;
            }
        }
        obtainStyledAttributes.recycle();
        return n;
    }
    
    private static Context getThemedContext(final Context context) {
        return (Context)new ContextThemeWrapper(context, getNavbarTheme(context));
    }
    
    private void init() {
        View.inflate(this.getContext(), R.layout.suw_navbar_view, (ViewGroup)this);
        this.mNextButton = (Button)this.findViewById(R.id.suw_navbar_next);
        this.mBackButton = (Button)this.findViewById(R.id.suw_navbar_back);
        this.mMoreButton = (Button)this.findViewById(R.id.suw_navbar_more);
    }
    
    public Button getBackButton() {
        return this.mBackButton;
    }
    
    public Button getMoreButton() {
        return this.mMoreButton;
    }
    
    public Button getNextButton() {
        return this.mNextButton;
    }
    
    public void onClick(final View view) {
        if (this.mListener != null) {
            if (view == this.getBackButton()) {
                this.mListener.onNavigateBack();
            }
            else if (view == this.getNextButton()) {
                this.mListener.onNavigateNext();
            }
        }
    }
    
    public void setNavigationBarListener(final NavigationBarListener mListener) {
        this.mListener = mListener;
        if (this.mListener != null) {
            this.getBackButton().setOnClickListener((View.OnClickListener)this);
            this.getNextButton().setOnClickListener((View.OnClickListener)this);
        }
    }
    
    public interface NavigationBarListener
    {
        void onNavigateBack();
        
        void onNavigateNext();
    }
}
