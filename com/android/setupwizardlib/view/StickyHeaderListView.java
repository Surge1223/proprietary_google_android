package com.android.setupwizardlib.view;

import android.os.Build.VERSION;
import android.view.accessibility.AccessibilityEvent;
import android.annotation.TargetApi;
import android.view.WindowInsets;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.content.res.TypedArray;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.RectF;
import android.view.View;
import android.widget.ListView;

public class StickyHeaderListView extends ListView
{
    private int mStatusBarInset;
    private View mSticky;
    private View mStickyContainer;
    private RectF mStickyRect;
    
    public StickyHeaderListView(final Context context) {
        super(context);
        this.mStatusBarInset = 0;
        this.mStickyRect = new RectF();
        this.init(null, 16842868);
    }
    
    public StickyHeaderListView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mStatusBarInset = 0;
        this.mStickyRect = new RectF();
        this.init(set, 16842868);
    }
    
    public StickyHeaderListView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mStatusBarInset = 0;
        this.mStickyRect = new RectF();
        this.init(set, n);
    }
    
    private void init(final AttributeSet set, int resourceId) {
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SuwStickyHeaderListView, resourceId, 0);
        resourceId = obtainStyledAttributes.getResourceId(R.styleable.SuwStickyHeaderListView_suwHeader, 0);
        if (resourceId != 0) {
            this.addHeaderView(LayoutInflater.from(this.getContext()).inflate(resourceId, (ViewGroup)this, false), (Object)null, false);
        }
        obtainStyledAttributes.recycle();
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        if (this.mStickyRect.contains(motionEvent.getX(), motionEvent.getY())) {
            motionEvent.offsetLocation(-this.mStickyRect.left, -this.mStickyRect.top);
            return this.mStickyContainer.dispatchTouchEvent(motionEvent);
        }
        return super.dispatchTouchEvent(motionEvent);
    }
    
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        if (this.mSticky != null) {
            final int save = canvas.save();
            View view;
            if (this.mStickyContainer != null) {
                view = this.mStickyContainer;
            }
            else {
                view = this.mSticky;
            }
            int top;
            if (this.mStickyContainer != null) {
                top = this.mSticky.getTop();
            }
            else {
                top = 0;
            }
            if (view.getTop() + top >= this.mStatusBarInset && view.isShown()) {
                this.mStickyRect.setEmpty();
            }
            else {
                this.mStickyRect.set(0.0f, (float)(-top + this.mStatusBarInset), (float)view.getWidth(), (float)(view.getHeight() - top + this.mStatusBarInset));
                canvas.translate(0.0f, this.mStickyRect.top);
                canvas.clipRect(0, 0, view.getWidth(), view.getHeight());
                view.draw(canvas);
            }
            canvas.restoreToCount(save);
        }
    }
    
    @TargetApi(21)
    public WindowInsets onApplyWindowInsets(final WindowInsets windowInsets) {
        if (this.getFitsSystemWindows()) {
            this.mStatusBarInset = windowInsets.getSystemWindowInsetTop();
            windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), 0, windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        }
        return windowInsets;
    }
    
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        int n;
        if (this.mSticky != null) {
            n = 1;
        }
        else {
            n = 0;
        }
        accessibilityEvent.setItemCount(accessibilityEvent.getItemCount() - n);
        accessibilityEvent.setFromIndex(Math.max(accessibilityEvent.getFromIndex() - n, 0));
        if (Build.VERSION.SDK_INT >= 14) {
            accessibilityEvent.setToIndex(Math.max(accessibilityEvent.getToIndex() - n, 0));
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (this.mSticky == null) {
            this.updateStickyView();
        }
    }
    
    public void updateStickyView() {
        this.mSticky = this.findViewWithTag((Object)"sticky");
        this.mStickyContainer = this.findViewWithTag((Object)"stickyContainer");
    }
}
