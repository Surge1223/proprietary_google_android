package com.android.setupwizardlib.view;

import android.util.AttributeSet;
import android.content.Context;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout implements Checkable
{
    private boolean mChecked;
    
    public CheckableLinearLayout(final Context context) {
        super(context);
        this.mChecked = false;
        this.setFocusable(true);
    }
    
    public CheckableLinearLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mChecked = false;
        this.setFocusable(true);
    }
    
    public CheckableLinearLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mChecked = false;
        this.setFocusable(true);
    }
    
    public CheckableLinearLayout(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mChecked = false;
        this.setFocusable(true);
    }
    
    public boolean isChecked() {
        return this.mChecked;
    }
    
    protected int[] onCreateDrawableState(final int n) {
        if (this.mChecked) {
            return mergeDrawableStates(super.onCreateDrawableState(n + 1), new int[] { 16842912 });
        }
        return super.onCreateDrawableState(n);
    }
    
    public void setChecked(final boolean mChecked) {
        this.mChecked = mChecked;
        this.refreshDrawableState();
    }
    
    public void toggle() {
        this.setChecked(this.isChecked() ^ true);
    }
}
