package com.android.setupwizardlib.view;

import android.annotation.TargetApi;
import android.view.WindowInsets;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;

public class StickyHeaderScrollView extends BottomScrollView
{
    private int mStatusBarInset;
    private View mSticky;
    private View mStickyContainer;
    
    public StickyHeaderScrollView(final Context context) {
        super(context);
        this.mStatusBarInset = 0;
    }
    
    public StickyHeaderScrollView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mStatusBarInset = 0;
    }
    
    public StickyHeaderScrollView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mStatusBarInset = 0;
    }
    
    private void updateStickyHeaderPosition() {
        if (Build.VERSION.SDK_INT >= 11 && this.mSticky != null) {
            View view;
            if (this.mStickyContainer != null) {
                view = this.mStickyContainer;
            }
            else {
                view = this.mSticky;
            }
            int top;
            if (this.mStickyContainer != null) {
                top = this.mSticky.getTop();
            }
            else {
                top = 0;
            }
            if (view.getTop() - this.getScrollY() + top >= this.mStatusBarInset && view.isShown()) {
                view.setTranslationY(0.0f);
            }
            else {
                view.setTranslationY((float)(this.getScrollY() - top));
            }
        }
    }
    
    @TargetApi(21)
    public WindowInsets onApplyWindowInsets(final WindowInsets windowInsets) {
        WindowInsets replaceSystemWindowInsets = windowInsets;
        if (this.getFitsSystemWindows()) {
            this.mStatusBarInset = windowInsets.getSystemWindowInsetTop();
            replaceSystemWindowInsets = windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), 0, windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        }
        return replaceSystemWindowInsets;
    }
    
    @Override
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (this.mSticky == null) {
            this.updateStickyView();
        }
        this.updateStickyHeaderPosition();
    }
    
    @Override
    protected void onScrollChanged(final int n, final int n2, final int n3, final int n4) {
        super.onScrollChanged(n, n2, n3, n4);
        this.updateStickyHeaderPosition();
    }
    
    public void updateStickyView() {
        this.mSticky = this.findViewWithTag((Object)"sticky");
        this.mStickyContainer = this.findViewWithTag((Object)"stickyContainer");
    }
}
