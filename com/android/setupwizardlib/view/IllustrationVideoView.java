package com.android.setupwizardlib.view;

import android.view.View$MeasureSpec;
import android.graphics.SurfaceTexture;
import android.util.Log;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.Surface;
import android.media.MediaPlayer;
import android.annotation.TargetApi;
import android.view.TextureView$SurfaceTextureListener;
import android.media.MediaPlayer$OnSeekCompleteListener;
import android.media.MediaPlayer$OnPreparedListener;
import android.media.MediaPlayer$OnInfoListener;
import android.graphics.drawable.Animatable;
import android.view.TextureView;

@TargetApi(14)
public class IllustrationVideoView extends TextureView implements Animatable, MediaPlayer$OnInfoListener, MediaPlayer$OnPreparedListener, MediaPlayer$OnSeekCompleteListener, TextureView$SurfaceTextureListener
{
    protected float mAspectRatio;
    protected MediaPlayer mMediaPlayer;
    Surface mSurface;
    private int mVideoResId;
    protected int mWindowVisibility;
    
    public IllustrationVideoView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mAspectRatio = 1.0f;
        this.mVideoResId = 0;
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwIllustrationVideoView);
        this.mVideoResId = obtainStyledAttributes.getResourceId(R.styleable.SuwIllustrationVideoView_suwVideo, 0);
        obtainStyledAttributes.recycle();
        this.setScaleX(0.9999999f);
        this.setScaleX(0.9999999f);
        this.setSurfaceTextureListener((TextureView$SurfaceTextureListener)this);
    }
    
    private void initVideo() {
        if (this.mWindowVisibility != 0) {
            return;
        }
        this.createSurface();
        if (this.mSurface != null) {
            this.createMediaPlayer();
        }
        else {
            Log.w("IllustrationVideoView", "Surface creation failed");
        }
    }
    
    private void reattach() {
        if (this.mSurface == null) {
            this.initVideo();
        }
    }
    
    protected void createMediaPlayer() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.release();
        }
        if (this.mSurface != null && this.mVideoResId != 0) {
            this.mMediaPlayer = MediaPlayer.create(this.getContext(), this.mVideoResId);
            if (this.mMediaPlayer != null) {
                this.mMediaPlayer.setSurface(this.mSurface);
                this.mMediaPlayer.setOnPreparedListener((MediaPlayer$OnPreparedListener)this);
                this.mMediaPlayer.setOnSeekCompleteListener((MediaPlayer$OnSeekCompleteListener)this);
                this.mMediaPlayer.setOnInfoListener((MediaPlayer$OnInfoListener)this);
                final float mAspectRatio = this.mMediaPlayer.getVideoHeight() / this.mMediaPlayer.getVideoWidth();
                if (this.mAspectRatio != mAspectRatio) {
                    this.mAspectRatio = mAspectRatio;
                    this.requestLayout();
                }
            }
            else {
                Log.wtf("IllustrationVideoView", "Unable to initialize media player for video view");
            }
            if (this.mWindowVisibility == 0) {
                this.start();
            }
        }
    }
    
    protected void createSurface() {
        if (this.mSurface != null) {
            this.mSurface.release();
            this.mSurface = null;
        }
        final SurfaceTexture surfaceTexture = this.getSurfaceTexture();
        if (surfaceTexture != null) {
            this.setVisibility(4);
            this.mSurface = new Surface(surfaceTexture);
        }
    }
    
    public int getCurrentPosition() {
        int currentPosition;
        if (this.mMediaPlayer == null) {
            currentPosition = 0;
        }
        else {
            currentPosition = this.mMediaPlayer.getCurrentPosition();
        }
        return currentPosition;
    }
    
    public boolean isRunning() {
        return this.mMediaPlayer != null && this.mMediaPlayer.isPlaying();
    }
    
    public boolean onInfo(final MediaPlayer mediaPlayer, final int n, final int n2) {
        if (n == 3) {
            this.setVisibility(0);
            this.onRenderingStart();
        }
        return false;
    }
    
    protected void onMeasure(int size, int n) {
        final int size2 = View$MeasureSpec.getSize(size);
        size = View$MeasureSpec.getSize(n);
        if (size < size2 * this.mAspectRatio) {
            n = (int)(size / this.mAspectRatio);
        }
        else {
            size = (int)(size2 * this.mAspectRatio);
            n = size2;
        }
        super.onMeasure(View$MeasureSpec.makeMeasureSpec(n, 1073741824), View$MeasureSpec.makeMeasureSpec(size, 1073741824));
    }
    
    public void onPrepared(final MediaPlayer mediaPlayer) {
        mediaPlayer.setLooping(this.shouldLoop());
    }
    
    protected void onRenderingStart() {
    }
    
    public void onSeekComplete(final MediaPlayer mediaPlayer) {
        mediaPlayer.start();
    }
    
    public void onSurfaceTextureAvailable(final SurfaceTexture surfaceTexture, final int n, final int n2) {
        this.setVisibility(4);
        this.initVideo();
    }
    
    public boolean onSurfaceTextureDestroyed(final SurfaceTexture surfaceTexture) {
        this.release();
        return true;
    }
    
    public void onSurfaceTextureSizeChanged(final SurfaceTexture surfaceTexture, final int n, final int n2) {
    }
    
    public void onSurfaceTextureUpdated(final SurfaceTexture surfaceTexture) {
    }
    
    public void onWindowFocusChanged(final boolean b) {
        super.onWindowFocusChanged(b);
        if (b) {
            this.start();
        }
        else {
            this.stop();
        }
    }
    
    protected void onWindowVisibilityChanged(final int mWindowVisibility) {
        super.onWindowVisibilityChanged(mWindowVisibility);
        this.mWindowVisibility = mWindowVisibility;
        if (mWindowVisibility == 0) {
            this.reattach();
        }
        else {
            this.release();
        }
    }
    
    public void release() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
        }
        if (this.mSurface != null) {
            this.mSurface.release();
            this.mSurface = null;
        }
    }
    
    public void setVideoResource(final int mVideoResId) {
        if (mVideoResId != this.mVideoResId) {
            this.mVideoResId = mVideoResId;
            this.createMediaPlayer();
        }
    }
    
    protected boolean shouldLoop() {
        return true;
    }
    
    public void start() {
        if (this.mMediaPlayer != null && !this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.start();
        }
    }
    
    public void stop() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.pause();
        }
    }
}
