package com.android.setupwizardlib.view;

import com.android.setupwizardlib.DividerItemDecoration;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.widget.FrameLayout;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Build.VERSION;
import android.view.accessibility.AccessibilityEvent;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.view.View;
import android.support.v7.widget.RecyclerView;

public class HeaderRecyclerView extends RecyclerView
{
    private View mHeader;
    private int mHeaderRes;
    
    public HeaderRecyclerView(final Context context) {
        super(context);
        this.init(null, 0);
    }
    
    public HeaderRecyclerView(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(set, 0);
    }
    
    public HeaderRecyclerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(set, n);
    }
    
    private void init(final AttributeSet set, final int n) {
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SuwHeaderRecyclerView, n, 0);
        this.mHeaderRes = obtainStyledAttributes.getResourceId(R.styleable.SuwHeaderRecyclerView_suwHeader, 0);
        obtainStyledAttributes.recycle();
    }
    
    public View getHeader() {
        return this.mHeader;
    }
    
    public void onInitializeAccessibilityEvent(final AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        int n;
        if (this.mHeader != null) {
            n = 1;
        }
        else {
            n = 0;
        }
        accessibilityEvent.setItemCount(accessibilityEvent.getItemCount() - n);
        accessibilityEvent.setFromIndex(Math.max(accessibilityEvent.getFromIndex() - n, 0));
        if (Build.VERSION.SDK_INT >= 14) {
            accessibilityEvent.setToIndex(Math.max(accessibilityEvent.getToIndex() - n, 0));
        }
    }
    
    @Override
    public void setAdapter(final Adapter adapter) {
        Object adapter2 = adapter;
        if (this.mHeader != null && (adapter2 = adapter) != null) {
            adapter2 = new HeaderAdapter(adapter);
            ((HeaderAdapter)adapter2).setHeader(this.mHeader);
        }
        super.setAdapter((Adapter)adapter2);
    }
    
    public void setHeader(final View mHeader) {
        this.mHeader = mHeader;
    }
    
    @Override
    public void setLayoutManager(final LayoutManager layoutManager) {
        super.setLayoutManager(layoutManager);
        if (layoutManager != null && this.mHeader == null && this.mHeaderRes != 0) {
            this.mHeader = LayoutInflater.from(this.getContext()).inflate(this.mHeaderRes, (ViewGroup)this, false);
        }
    }
    
    public static class HeaderAdapter<CVH extends ViewHolder> extends Adapter<ViewHolder>
    {
        private Adapter<CVH> mAdapter;
        private View mHeader;
        private final AdapterDataObserver mObserver;
        
        public HeaderAdapter(final Adapter<CVH> mAdapter) {
            this.mObserver = new AdapterDataObserver() {
                @Override
                public void onChanged() {
                    HeaderAdapter.this.notifyDataSetChanged();
                }
                
                @Override
                public void onItemRangeChanged(final int n, final int n2) {
                    int n3 = n;
                    if (HeaderAdapter.this.mHeader != null) {
                        n3 = n + 1;
                    }
                    HeaderAdapter.this.notifyItemRangeChanged(n3, n2);
                }
                
                @Override
                public void onItemRangeInserted(final int n, final int n2) {
                    int n3 = n;
                    if (HeaderAdapter.this.mHeader != null) {
                        n3 = n + 1;
                    }
                    HeaderAdapter.this.notifyItemRangeInserted(n3, n2);
                }
                
                @Override
                public void onItemRangeMoved(int i, final int n, final int n2) {
                    int n3 = i;
                    int n4 = n;
                    if (HeaderAdapter.this.mHeader != null) {
                        n3 = i + 1;
                        n4 = n + 1;
                    }
                    for (i = 0; i < n2; ++i) {
                        HeaderAdapter.this.notifyItemMoved(n3 + i, n4 + i);
                    }
                }
                
                @Override
                public void onItemRangeRemoved(final int n, final int n2) {
                    int n3 = n;
                    if (HeaderAdapter.this.mHeader != null) {
                        n3 = n + 1;
                    }
                    HeaderAdapter.this.notifyItemRangeRemoved(n3, n2);
                }
            };
            (this.mAdapter = mAdapter).registerAdapterDataObserver(this.mObserver);
            this.setHasStableIds(this.mAdapter.hasStableIds());
        }
        
        @Override
        public int getItemCount() {
            int itemCount = this.mAdapter.getItemCount();
            if (this.mHeader != null) {
                ++itemCount;
            }
            return itemCount;
        }
        
        @Override
        public long getItemId(final int n) {
            int n2 = n;
            if (this.mHeader != null) {
                n2 = n - 1;
            }
            if (n2 < 0) {
                return Long.MAX_VALUE;
            }
            return this.mAdapter.getItemId(n2);
        }
        
        @Override
        public int getItemViewType(final int n) {
            int n2 = n;
            if (this.mHeader != null) {
                n2 = n - 1;
            }
            if (n2 < 0) {
                return Integer.MAX_VALUE;
            }
            return this.mAdapter.getItemViewType(n2);
        }
        
        public Adapter<CVH> getWrappedAdapter() {
            return this.mAdapter;
        }
        
        @Override
        public void onBindViewHolder(final ViewHolder viewHolder, final int n) {
            int n2 = n;
            if (this.mHeader != null) {
                n2 = n - 1;
            }
            if (viewHolder instanceof HeaderViewHolder) {
                if (this.mHeader == null) {
                    throw new IllegalStateException("HeaderViewHolder cannot find mHeader");
                }
                if (this.mHeader.getParent() != null) {
                    ((ViewGroup)this.mHeader.getParent()).removeView(this.mHeader);
                }
                ((FrameLayout)viewHolder.itemView).addView(this.mHeader);
            }
            else {
                this.mAdapter.onBindViewHolder((CVH)viewHolder, n2);
            }
        }
        
        @Override
        public ViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
            if (n == Integer.MAX_VALUE) {
                final FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
                frameLayout.setLayoutParams((ViewGroup.LayoutParams)new FrameLayout$LayoutParams(-1, -2));
                return new HeaderViewHolder((View)frameLayout);
            }
            return this.mAdapter.onCreateViewHolder(viewGroup, n);
        }
        
        public void setHeader(final View mHeader) {
            this.mHeader = mHeader;
        }
    }
    
    private static class HeaderViewHolder extends ViewHolder implements DividedViewHolder
    {
        HeaderViewHolder(final View view) {
            super(view);
        }
        
        @Override
        public boolean isDividerAllowedAbove() {
            return false;
        }
        
        @Override
        public boolean isDividerAllowedBelow() {
            return false;
        }
    }
}
