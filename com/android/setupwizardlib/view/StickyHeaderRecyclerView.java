package com.android.setupwizardlib.view;

import android.annotation.TargetApi;
import android.view.WindowInsets;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.RectF;
import android.view.View;

public class StickyHeaderRecyclerView extends HeaderRecyclerView
{
    private int mStatusBarInset;
    private View mSticky;
    private RectF mStickyRect;
    
    public StickyHeaderRecyclerView(final Context context) {
        super(context);
        this.mStatusBarInset = 0;
        this.mStickyRect = new RectF();
    }
    
    public StickyHeaderRecyclerView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mStatusBarInset = 0;
        this.mStickyRect = new RectF();
    }
    
    public StickyHeaderRecyclerView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mStatusBarInset = 0;
        this.mStickyRect = new RectF();
    }
    
    public boolean dispatchTouchEvent(final MotionEvent motionEvent) {
        if (this.mStickyRect.contains(motionEvent.getX(), motionEvent.getY())) {
            motionEvent.offsetLocation(-this.mStickyRect.left, -this.mStickyRect.top);
            return this.getHeader().dispatchTouchEvent(motionEvent);
        }
        return super.dispatchTouchEvent(motionEvent);
    }
    
    @Override
    public void draw(final Canvas canvas) {
        super.draw(canvas);
        if (this.mSticky != null) {
            final View header = this.getHeader();
            final int save = canvas.save();
            View mSticky;
            if (header != null) {
                mSticky = header;
            }
            else {
                mSticky = this.mSticky;
            }
            int top;
            if (header != null) {
                top = this.mSticky.getTop();
            }
            else {
                top = 0;
            }
            if (mSticky.getTop() + top >= this.mStatusBarInset && mSticky.isShown()) {
                this.mStickyRect.setEmpty();
            }
            else {
                this.mStickyRect.set(0.0f, (float)(-top + this.mStatusBarInset), (float)mSticky.getWidth(), (float)(mSticky.getHeight() - top + this.mStatusBarInset));
                canvas.translate(0.0f, this.mStickyRect.top);
                canvas.clipRect(0, 0, mSticky.getWidth(), mSticky.getHeight());
                mSticky.draw(canvas);
            }
            canvas.restoreToCount(save);
        }
    }
    
    @TargetApi(21)
    public WindowInsets onApplyWindowInsets(final WindowInsets windowInsets) {
        if (this.getFitsSystemWindows()) {
            this.mStatusBarInset = windowInsets.getSystemWindowInsetTop();
            windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), 0, windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        }
        return windowInsets;
    }
    
    @Override
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        if (this.mSticky == null) {
            this.updateStickyView();
        }
        if (this.mSticky != null) {
            final View header = this.getHeader();
            if (header != null && header.getHeight() == 0) {
                header.layout(0, -header.getMeasuredHeight(), header.getMeasuredWidth(), 0);
            }
        }
    }
    
    @Override
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(n, n2);
        if (this.mSticky != null) {
            this.measureChild(this.getHeader(), n, n2);
        }
    }
    
    public void updateStickyView() {
        final View header = this.getHeader();
        if (header != null) {
            this.mSticky = header.findViewWithTag((Object)"sticky");
        }
    }
}
