package com.android.setupwizardlib.view;

import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.view.View$MeasureSpec;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.FrameLayout;

public class IntrinsicSizeFrameLayout extends FrameLayout
{
    private int mIntrinsicHeight;
    private int mIntrinsicWidth;
    
    public IntrinsicSizeFrameLayout(final Context context) {
        super(context);
        this.mIntrinsicHeight = 0;
        this.init(context, null, this.mIntrinsicWidth = 0);
    }
    
    public IntrinsicSizeFrameLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mIntrinsicHeight = 0;
        this.init(context, set, this.mIntrinsicWidth = 0);
    }
    
    public IntrinsicSizeFrameLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mIntrinsicHeight = 0;
        this.mIntrinsicWidth = 0;
        this.init(context, set, n);
    }
    
    private int getIntrinsicMeasureSpec(final int n, int mode) {
        if (mode <= 0) {
            return n;
        }
        mode = View$MeasureSpec.getMode(n);
        final int size = View$MeasureSpec.getSize(n);
        if (mode == 0) {
            return View$MeasureSpec.makeMeasureSpec(this.mIntrinsicHeight, 1073741824);
        }
        if (mode == Integer.MIN_VALUE) {
            return View$MeasureSpec.makeMeasureSpec(Math.min(size, this.mIntrinsicHeight), 1073741824);
        }
        return n;
    }
    
    private void init(final Context context, final AttributeSet set, final int n) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwIntrinsicSizeFrameLayout, n, 0);
        this.mIntrinsicHeight = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwIntrinsicSizeFrameLayout_android_height, 0);
        this.mIntrinsicWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwIntrinsicSizeFrameLayout_android_width, 0);
        obtainStyledAttributes.recycle();
    }
    
    protected void onMeasure(final int n, final int n2) {
        super.onMeasure(this.getIntrinsicMeasureSpec(n, this.mIntrinsicWidth), this.getIntrinsicMeasureSpec(n2, this.mIntrinsicHeight));
    }
}
