package com.android.setupwizardlib.view;

import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.LayerDrawable;
import android.content.res.ColorStateList;
import android.os.Build.VERSION;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.content.Context;
import android.annotation.SuppressLint;
import android.widget.Button;

@SuppressLint({ "AppCompatCustomView" })
public class NavigationBarButton extends Button
{
    public NavigationBarButton(final Context context) {
        super(context);
        this.init();
    }
    
    public NavigationBarButton(final Context context, final AttributeSet set) {
        super(context, set);
        this.init();
    }
    
    private Drawable[] getAllCompoundDrawables() {
        final Drawable[] array = new Drawable[6];
        final Drawable[] compoundDrawables = this.getCompoundDrawables();
        array[0] = compoundDrawables[0];
        array[1] = compoundDrawables[1];
        array[2] = compoundDrawables[2];
        array[3] = compoundDrawables[3];
        if (Build.VERSION.SDK_INT >= 17) {
            final Drawable[] compoundDrawablesRelative = this.getCompoundDrawablesRelative();
            array[4] = compoundDrawablesRelative[0];
            array[5] = compoundDrawablesRelative[2];
        }
        return array;
    }
    
    private void init() {
        if (Build.VERSION.SDK_INT >= 17) {
            final Drawable[] compoundDrawablesRelative = this.getCompoundDrawablesRelative();
            for (int i = 0; i < compoundDrawablesRelative.length; ++i) {
                if (compoundDrawablesRelative[i] != null) {
                    compoundDrawablesRelative[i] = (Drawable)TintedDrawable.wrap(compoundDrawablesRelative[i]);
                }
            }
            this.setCompoundDrawablesRelativeWithIntrinsicBounds(compoundDrawablesRelative[0], compoundDrawablesRelative[1], compoundDrawablesRelative[2], compoundDrawablesRelative[3]);
        }
    }
    
    private void tintDrawables() {
        final ColorStateList textColors = this.getTextColors();
        if (textColors != null) {
            for (final Drawable drawable : this.getAllCompoundDrawables()) {
                if (drawable instanceof TintedDrawable) {
                    ((TintedDrawable)drawable).setTintListCompat(textColors);
                }
            }
            this.invalidate();
        }
    }
    
    public void setCompoundDrawables(Drawable wrap, Drawable wrap2, Drawable wrap3, final Drawable drawable) {
        Object wrap4 = wrap;
        if (wrap != null) {
            wrap4 = TintedDrawable.wrap((Drawable)wrap);
        }
        if ((wrap = wrap2) != null) {
            wrap = TintedDrawable.wrap((Drawable)wrap2);
        }
        if ((wrap2 = wrap3) != null) {
            wrap2 = TintedDrawable.wrap((Drawable)wrap3);
        }
        if ((wrap3 = drawable) != null) {
            wrap3 = TintedDrawable.wrap(drawable);
        }
        super.setCompoundDrawables((Drawable)wrap4, (Drawable)wrap, (Drawable)wrap2, (Drawable)wrap3);
        this.tintDrawables();
    }
    
    public void setCompoundDrawablesRelative(Drawable wrap, Drawable wrap2, Drawable wrap3, final Drawable drawable) {
        Object wrap4 = wrap;
        if (wrap != null) {
            wrap4 = TintedDrawable.wrap((Drawable)wrap);
        }
        if ((wrap = wrap2) != null) {
            wrap = TintedDrawable.wrap((Drawable)wrap2);
        }
        if ((wrap2 = wrap3) != null) {
            wrap2 = TintedDrawable.wrap((Drawable)wrap3);
        }
        if ((wrap3 = drawable) != null) {
            wrap3 = TintedDrawable.wrap(drawable);
        }
        super.setCompoundDrawablesRelative((Drawable)wrap4, (Drawable)wrap, (Drawable)wrap2, (Drawable)wrap3);
        this.tintDrawables();
    }
    
    public void setTextColor(final ColorStateList textColor) {
        super.setTextColor(textColor);
        this.tintDrawables();
    }
    
    private static class TintedDrawable extends LayerDrawable
    {
        private ColorStateList mTintList;
        
        TintedDrawable(final Drawable drawable) {
            super(new Drawable[] { drawable });
            this.mTintList = null;
        }
        
        private boolean updateState() {
            if (this.mTintList != null) {
                this.setColorFilter(this.mTintList.getColorForState(this.getState(), 0), PorterDuff.Mode.SRC_IN);
                return true;
            }
            return false;
        }
        
        public static TintedDrawable wrap(final Drawable drawable) {
            if (drawable instanceof TintedDrawable) {
                return (TintedDrawable)drawable;
            }
            return new TintedDrawable(drawable.mutate());
        }
        
        public boolean isStateful() {
            return true;
        }
        
        public boolean setState(final int[] state) {
            final boolean setState = super.setState(state);
            final boolean updateState = this.updateState();
            return setState || updateState;
        }
        
        public void setTintListCompat(final ColorStateList mTintList) {
            this.mTintList = mTintList;
            if (this.updateState()) {
                this.invalidateSelf();
            }
        }
    }
}
