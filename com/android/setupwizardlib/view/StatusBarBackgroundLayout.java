package com.android.setupwizardlib.view;

import android.graphics.Canvas;
import android.os.Build.VERSION;
import android.view.WindowInsets;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;

public class StatusBarBackgroundLayout extends FrameLayout
{
    private Object mLastInsets;
    private Drawable mStatusBarBackground;
    
    public StatusBarBackgroundLayout(final Context context) {
        super(context);
        this.init(context, null, 0);
    }
    
    public StatusBarBackgroundLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(context, set, 0);
    }
    
    public StatusBarBackgroundLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(context, set, n);
    }
    
    private void init(final Context context, final AttributeSet set, final int n) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwStatusBarBackgroundLayout, n, 0);
        this.setStatusBarBackground(obtainStyledAttributes.getDrawable(R.styleable.SuwStatusBarBackgroundLayout_suwStatusBarBackground));
        obtainStyledAttributes.recycle();
    }
    
    public Drawable getStatusBarBackground() {
        return this.mStatusBarBackground;
    }
    
    public WindowInsets onApplyWindowInsets(final WindowInsets mLastInsets) {
        this.mLastInsets = mLastInsets;
        return super.onApplyWindowInsets(mLastInsets);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= 21 && this.mLastInsets == null) {
            this.requestApplyInsets();
        }
    }
    
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (Build.VERSION.SDK_INT >= 21 && this.mLastInsets != null) {
            final int systemWindowInsetTop = ((WindowInsets)this.mLastInsets).getSystemWindowInsetTop();
            if (systemWindowInsetTop > 0) {
                this.mStatusBarBackground.setBounds(0, 0, this.getWidth(), systemWindowInsetTop);
                this.mStatusBarBackground.draw(canvas);
            }
        }
    }
    
    public void setStatusBarBackground(final Drawable mStatusBarBackground) {
        this.mStatusBarBackground = mStatusBarBackground;
        if (Build.VERSION.SDK_INT >= 21) {
            final boolean b = false;
            this.setWillNotDraw(mStatusBarBackground == null);
            boolean fitsSystemWindows = b;
            if (mStatusBarBackground != null) {
                fitsSystemWindows = true;
            }
            this.setFitsSystemWindows(fitsSystemWindows);
            this.invalidate();
        }
    }
}
