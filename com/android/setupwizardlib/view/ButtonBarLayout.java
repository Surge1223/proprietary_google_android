package com.android.setupwizardlib.view;

import android.view.View$MeasureSpec;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.android.setupwizardlib.R;
import android.widget.LinearLayout$LayoutParams;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.LinearLayout;

public class ButtonBarLayout extends LinearLayout
{
    private int mOriginalPaddingLeft;
    private int mOriginalPaddingRight;
    private boolean mStacked;
    
    public ButtonBarLayout(final Context context) {
        super(context);
        this.mStacked = false;
    }
    
    public ButtonBarLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mStacked = false;
    }
    
    private void setStacked(final boolean b) {
        if (this.mStacked == b) {
            return;
        }
        this.mStacked = b;
        final int childCount = this.getChildCount();
        for (int i = 0; i < childCount; ++i) {
            final View child = this.getChildAt(i);
            final LinearLayout$LayoutParams layoutParams = (LinearLayout$LayoutParams)child.getLayoutParams();
            if (b) {
                child.setTag(R.id.suw_original_weight, (Object)layoutParams.weight);
                layoutParams.weight = 0.0f;
            }
            else {
                final Float n = (Float)child.getTag(R.id.suw_original_weight);
                if (n != null) {
                    layoutParams.weight = n;
                }
            }
            child.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
        }
        this.setOrientation((int)(b ? 1 : 0));
        for (int j = childCount - 1; j >= 0; --j) {
            this.bringChildToFront(this.getChildAt(j));
        }
        if ((b ? 1 : 0) != 0) {
            this.mOriginalPaddingLeft = this.getPaddingLeft();
            this.mOriginalPaddingRight = this.getPaddingRight();
            final int max = Math.max(this.mOriginalPaddingLeft, this.mOriginalPaddingRight);
            this.setPadding(max, this.getPaddingTop(), max, this.getPaddingBottom());
        }
        else {
            this.setPadding(this.mOriginalPaddingLeft, this.getPaddingTop(), this.mOriginalPaddingRight, this.getPaddingBottom());
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        final int size = View$MeasureSpec.getSize(n);
        this.setStacked(false);
        boolean b = false;
        int measureSpec = n;
        if (View$MeasureSpec.getMode(n) == 1073741824) {
            measureSpec = View$MeasureSpec.makeMeasureSpec(0, 0);
            b = true;
        }
        super.onMeasure(measureSpec, n2);
        if (this.getMeasuredWidth() > size) {
            this.setStacked(true);
            b = true;
        }
        if (b) {
            super.onMeasure(n, n2);
        }
    }
}
