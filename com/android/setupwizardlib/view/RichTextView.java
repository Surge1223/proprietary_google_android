package com.android.setupwizardlib.view;

import android.widget.TextView$BufferType;
import android.text.method.MovementMethod;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.MotionEvent;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.view.View;
import android.support.v4.view.ViewCompat;
import android.widget.TextView;
import android.text.style.ClickableSpan;
import android.text.Spannable;
import com.android.setupwizardlib.span.SpanHelper;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.text.Annotation;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.AttributeSet;
import android.content.Context;
import com.android.setupwizardlib.util.LinkAccessibilityHelper;
import com.android.setupwizardlib.span.LinkSpan;
import android.support.v7.widget.AppCompatTextView;

public class RichTextView extends AppCompatTextView implements OnLinkClickListener
{
    private LinkAccessibilityHelper mAccessibilityHelper;
    private OnLinkClickListener mOnLinkClickListener;
    
    public RichTextView(final Context context) {
        super(context);
        this.init();
    }
    
    public RichTextView(final Context context, final AttributeSet set) {
        super(context, set);
        this.init();
    }
    
    public static CharSequence getRichText(final Context context, final CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            final SpannableString spannableString = new SpannableString(charSequence);
            final int length = spannableString.length();
            int i = 0;
            for (Annotation[] array = (Annotation[])spannableString.getSpans(0, length, (Class)Annotation.class); i < array.length; ++i) {
                final Annotation annotation = array[i];
                final String key = annotation.getKey();
                if ("textAppearance".equals(key)) {
                    final int identifier = context.getResources().getIdentifier(annotation.getValue(), "style", context.getPackageName());
                    if (identifier == 0) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Cannot find resource: ");
                        sb.append(identifier);
                        Log.w("RichTextView", sb.toString());
                    }
                    SpanHelper.replaceSpan((Spannable)spannableString, annotation, new TextAppearanceSpan(context, identifier));
                }
                else if ("link".equals(key)) {
                    SpanHelper.replaceSpan((Spannable)spannableString, annotation, new LinkSpan(annotation.getValue()));
                }
            }
            return (CharSequence)spannableString;
        }
        return charSequence;
    }
    
    private boolean hasLinks(final CharSequence charSequence) {
        final boolean b = charSequence instanceof Spanned;
        boolean b2 = false;
        if (b) {
            if (((ClickableSpan[])((Spanned)charSequence).getSpans(0, charSequence.length(), (Class)ClickableSpan.class)).length > 0) {
                b2 = true;
            }
            return b2;
        }
        return false;
    }
    
    private void init() {
        ViewCompat.setAccessibilityDelegate((View)this, this.mAccessibilityHelper = new LinkAccessibilityHelper(this));
    }
    
    protected boolean dispatchHoverEvent(final MotionEvent motionEvent) {
        return (this.mAccessibilityHelper != null && this.mAccessibilityHelper.dispatchHoverEvent(motionEvent)) || super.dispatchHoverEvent(motionEvent);
    }
    
    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (Build.VERSION.SDK_INT >= 17) {
            final int[] drawableState = this.getDrawableState();
            for (final Drawable drawable : this.getCompoundDrawablesRelative()) {
                if (drawable != null && drawable.setState(drawableState)) {
                    this.invalidateDrawable(drawable);
                }
            }
        }
    }
    
    public OnLinkClickListener getOnLinkClickListener() {
        return this.mOnLinkClickListener;
    }
    
    @Override
    public boolean onLinkClick(final LinkSpan linkSpan) {
        return this.mOnLinkClickListener != null && this.mOnLinkClickListener.onLinkClick(linkSpan);
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final boolean onTouchEvent = super.onTouchEvent(motionEvent);
        final MovementMethod movementMethod = this.getMovementMethod();
        if (movementMethod instanceof TouchableMovementMethod) {
            final TouchableMovementMethod touchableMovementMethod = (TouchableMovementMethod)movementMethod;
            if (touchableMovementMethod.getLastTouchEvent() == motionEvent) {
                return touchableMovementMethod.isLastTouchEventHandled();
            }
        }
        return onTouchEvent;
    }
    
    public void setOnLinkClickListener(final OnLinkClickListener mOnLinkClickListener) {
        this.mOnLinkClickListener = mOnLinkClickListener;
    }
    
    public void setText(CharSequence richText, final TextView$BufferType textView$BufferType) {
        richText = getRichText(this.getContext(), richText);
        super.setText(richText, textView$BufferType);
        final boolean hasLinks = this.hasLinks(richText);
        if (hasLinks) {
            this.setMovementMethod((MovementMethod)TouchableMovementMethod.TouchableLinkMovementMethod.getInstance());
        }
        else {
            this.setMovementMethod((MovementMethod)null);
        }
        this.setFocusable(hasLinks);
        if (Build.VERSION.SDK_INT >= 25) {
            this.setRevealOnFocusHint(false);
            this.setFocusableInTouchMode(hasLinks);
        }
    }
}
