package com.android.setupwizardlib.view;

import android.view.ViewGroup$MarginLayoutParams;
import android.view.View;
import android.content.res.TypedArray;
import android.view.View$MeasureSpec;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.FrameLayout;

public class FillContentLayout extends FrameLayout
{
    private int mMaxHeight;
    private int mMaxWidth;
    
    public FillContentLayout(final Context context) {
        this(context, null);
    }
    
    public FillContentLayout(final Context context, final AttributeSet set) {
        this(context, set, R.attr.suwFillContentLayoutStyle);
    }
    
    public FillContentLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(context, set, n);
    }
    
    private static int getMaxSizeMeasureSpec(int max, final int n, final int n2) {
        max = Math.max(0, max - n);
        if (n2 >= 0) {
            return View$MeasureSpec.makeMeasureSpec(n2, 1073741824);
        }
        if (n2 == -1) {
            return View$MeasureSpec.makeMeasureSpec(max, 1073741824);
        }
        if (n2 == -2) {
            return View$MeasureSpec.makeMeasureSpec(max, Integer.MIN_VALUE);
        }
        return 0;
    }
    
    private void init(final Context context, final AttributeSet set, final int n) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwFillContentLayout, n, 0);
        this.mMaxHeight = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwFillContentLayout_android_maxHeight, -1);
        this.mMaxWidth = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwFillContentLayout_android_maxWidth, -1);
        obtainStyledAttributes.recycle();
    }
    
    private void measureIllustrationChild(final View view, final int n, final int n2) {
        final ViewGroup$MarginLayoutParams viewGroup$MarginLayoutParams = (ViewGroup$MarginLayoutParams)view.getLayoutParams();
        view.measure(getMaxSizeMeasureSpec(Math.min(this.mMaxWidth, n), this.getPaddingLeft() + this.getPaddingRight() + viewGroup$MarginLayoutParams.leftMargin + viewGroup$MarginLayoutParams.rightMargin, viewGroup$MarginLayoutParams.width), getMaxSizeMeasureSpec(Math.min(this.mMaxHeight, n2), this.getPaddingTop() + this.getPaddingBottom() + viewGroup$MarginLayoutParams.topMargin + viewGroup$MarginLayoutParams.bottomMargin, viewGroup$MarginLayoutParams.height));
    }
    
    protected void onMeasure(int i, int childCount) {
        this.setMeasuredDimension(getDefaultSize(this.getSuggestedMinimumWidth(), i), getDefaultSize(this.getSuggestedMinimumHeight(), childCount));
        for (childCount = this.getChildCount(), i = 0; i < childCount; ++i) {
            this.measureIllustrationChild(this.getChildAt(i), this.getMeasuredWidth(), this.getMeasuredHeight());
        }
    }
}
