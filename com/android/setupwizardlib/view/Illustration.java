package com.android.setupwizardlib.view;

import android.view.ViewOutlineProvider;
import android.view.View$MeasureSpec;
import android.view.Gravity;
import android.graphics.Canvas;
import android.os.Build.VERSION;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;

public class Illustration extends FrameLayout
{
    private float mAspectRatio;
    private Drawable mBackground;
    private float mBaselineGridSize;
    private Drawable mIllustration;
    private final Rect mIllustrationBounds;
    private float mScale;
    private final Rect mViewBounds;
    
    public Illustration(final Context context) {
        super(context);
        this.mViewBounds = new Rect();
        this.mIllustrationBounds = new Rect();
        this.mScale = 1.0f;
        this.mAspectRatio = 0.0f;
        this.init(null, 0);
    }
    
    public Illustration(final Context context, final AttributeSet set) {
        super(context, set);
        this.mViewBounds = new Rect();
        this.mIllustrationBounds = new Rect();
        this.mScale = 1.0f;
        this.mAspectRatio = 0.0f;
        this.init(set, 0);
    }
    
    public Illustration(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mViewBounds = new Rect();
        this.mIllustrationBounds = new Rect();
        this.mScale = 1.0f;
        this.mAspectRatio = 0.0f;
        this.init(set, n);
    }
    
    private void init(final AttributeSet set, final int n) {
        if (set != null) {
            final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SuwIllustration, n, 0);
            this.mAspectRatio = obtainStyledAttributes.getFloat(R.styleable.SuwIllustration_suwAspectRatio, 0.0f);
            obtainStyledAttributes.recycle();
        }
        this.mBaselineGridSize = this.getResources().getDisplayMetrics().density * 8.0f;
        this.setWillNotDraw(false);
    }
    
    private boolean shouldMirrorDrawable(final Drawable drawable, final int n) {
        boolean b = false;
        if (n == 1) {
            if (Build.VERSION.SDK_INT >= 19) {
                return drawable.isAutoMirrored();
            }
            if (Build.VERSION.SDK_INT >= 17) {
                if ((0x400000 & this.getContext().getApplicationInfo().flags) != 0x0) {
                    b = true;
                }
                return b;
            }
        }
        return false;
    }
    
    public void onDraw(final Canvas canvas) {
        if (this.mBackground != null) {
            canvas.save();
            canvas.translate(0.0f, (float)this.mIllustrationBounds.height());
            canvas.scale(this.mScale, this.mScale, 0.0f, 0.0f);
            if (Build.VERSION.SDK_INT > 17 && this.shouldMirrorDrawable(this.mBackground, this.getLayoutDirection())) {
                canvas.scale(-1.0f, 1.0f);
                canvas.translate((float)(-this.mBackground.getBounds().width()), 0.0f);
            }
            this.mBackground.draw(canvas);
            canvas.restore();
        }
        if (this.mIllustration != null) {
            canvas.save();
            if (Build.VERSION.SDK_INT > 17 && this.shouldMirrorDrawable(this.mIllustration, this.getLayoutDirection())) {
                canvas.scale(-1.0f, 1.0f);
                canvas.translate((float)(-this.mIllustrationBounds.width()), 0.0f);
            }
            this.mIllustration.draw(canvas);
            canvas.restore();
        }
        super.onDraw(canvas);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        final int n5 = n3 - n;
        final int n6 = n4 - n2;
        if (this.mIllustration != null) {
            final int intrinsicWidth = this.mIllustration.getIntrinsicWidth();
            final int intrinsicHeight = this.mIllustration.getIntrinsicHeight();
            this.mViewBounds.set(0, 0, n5, n6);
            int n7 = intrinsicWidth;
            int n8 = intrinsicHeight;
            if (this.mAspectRatio != 0.0f) {
                this.mScale = n5 / intrinsicWidth;
                n7 = n5;
                n8 = (int)(intrinsicHeight * this.mScale);
            }
            Gravity.apply(55, n7, n8, this.mViewBounds, this.mIllustrationBounds);
            this.mIllustration.setBounds(this.mIllustrationBounds);
        }
        if (this.mBackground != null) {
            this.mBackground.setBounds(0, 0, (int)Math.ceil(n5 / this.mScale), (int)Math.ceil((n6 - this.mIllustrationBounds.height()) / this.mScale));
        }
        super.onLayout(b, n, n2, n3, n4);
    }
    
    protected void onMeasure(final int n, final int n2) {
        if (this.mAspectRatio != 0.0f) {
            final int n3 = (int)(View$MeasureSpec.getSize(n) / this.mAspectRatio);
            this.setPadding(0, (int)(n3 - n3 % this.mBaselineGridSize), 0, 0);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.setOutlineProvider(ViewOutlineProvider.BOUNDS);
        }
        super.onMeasure(n, n2);
    }
    
    public void setAspectRatio(final float mAspectRatio) {
        this.mAspectRatio = mAspectRatio;
        this.invalidate();
        this.requestLayout();
    }
    
    public void setBackgroundDrawable(final Drawable mBackground) {
        if (mBackground == this.mBackground) {
            return;
        }
        this.mBackground = mBackground;
        this.invalidate();
        this.requestLayout();
    }
    
    @Deprecated
    public void setForeground(final Drawable illustration) {
        this.setIllustration(illustration);
    }
    
    public void setIllustration(final Drawable mIllustration) {
        if (mIllustration == this.mIllustration) {
            return;
        }
        this.mIllustration = mIllustration;
        this.invalidate();
        this.requestLayout();
    }
}
