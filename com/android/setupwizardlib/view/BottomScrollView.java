package com.android.setupwizardlib.view;

import android.view.View;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ScrollView;

public class BottomScrollView extends ScrollView
{
    private final Runnable mCheckScrollRunnable;
    private BottomScrollListener mListener;
    private boolean mRequiringScroll;
    private int mScrollThreshold;
    
    public BottomScrollView(final Context context) {
        super(context);
        this.mRequiringScroll = false;
        this.mCheckScrollRunnable = new Runnable() {
            @Override
            public void run() {
                BottomScrollView.this.checkScroll();
            }
        };
    }
    
    public BottomScrollView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mRequiringScroll = false;
        this.mCheckScrollRunnable = new Runnable() {
            @Override
            public void run() {
                BottomScrollView.this.checkScroll();
            }
        };
    }
    
    public BottomScrollView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mRequiringScroll = false;
        this.mCheckScrollRunnable = new Runnable() {
            @Override
            public void run() {
                BottomScrollView.this.checkScroll();
            }
        };
    }
    
    private void checkScroll() {
        if (this.mListener != null) {
            if (this.getScrollY() >= this.mScrollThreshold) {
                this.mListener.onScrolledToBottom();
            }
            else if (!this.mRequiringScroll) {
                this.mRequiringScroll = true;
                this.mListener.onRequiresScroll();
            }
        }
    }
    
    public int getScrollThreshold() {
        return this.mScrollThreshold;
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        final View child = this.getChildAt(0);
        if (child != null) {
            this.mScrollThreshold = Math.max(0, child.getMeasuredHeight() - n4 + n2 - this.getPaddingBottom());
        }
        if (n4 - n2 > 0) {
            this.post(this.mCheckScrollRunnable);
        }
    }
    
    protected void onScrollChanged(final int n, final int n2, final int n3, final int n4) {
        super.onScrollChanged(n, n2, n3, n4);
        if (n4 != n2) {
            this.checkScroll();
        }
    }
    
    public void setBottomScrollListener(final BottomScrollListener mListener) {
        this.mListener = mListener;
    }
    
    public interface BottomScrollListener
    {
        void onRequiresScroll();
        
        void onScrolledToBottom();
    }
}
