package com.android.setupwizardlib;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;
import android.util.Log;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.content.res.ColorStateList;
import com.android.setupwizardlib.view.NavigationBar;
import android.widget.TextView;
import android.view.ViewGroup;
import com.android.setupwizardlib.view.Illustration;
import android.content.res.TypedArray;
import android.widget.ScrollView;
import android.util.TypedValue;
import com.android.setupwizardlib.template.ScrollViewScrollHandlingDelegate;
import com.android.setupwizardlib.template.RequireScrollMixin;
import com.android.setupwizardlib.template.NavigationBarMixin;
import com.android.setupwizardlib.template.ProgressBarMixin;
import com.android.setupwizardlib.template.HeaderMixin;
import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import android.graphics.drawable.LayerDrawable;
import android.graphics.Shader$TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.content.Context;

public class SetupWizardLayout extends TemplateLayout
{
    public SetupWizardLayout(final Context context) {
        super(context, 0, 0);
        this.init(null, R.attr.suwLayoutTheme);
    }
    
    public SetupWizardLayout(final Context context, final int n, final int n2) {
        super(context, n, n2);
        this.init(null, R.attr.suwLayoutTheme);
    }
    
    public SetupWizardLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(set, R.attr.suwLayoutTheme);
    }
    
    public SetupWizardLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(set, n);
    }
    
    @SuppressLint({ "RtlHardcoded" })
    private Drawable getIllustration(final Drawable drawable, final Drawable drawable2) {
        if (this.getContext().getResources().getBoolean(R.bool.suwUseTabletLayout)) {
            if (drawable2 instanceof BitmapDrawable) {
                ((BitmapDrawable)drawable2).setTileModeX(Shader$TileMode.REPEAT);
                ((BitmapDrawable)drawable2).setGravity(48);
            }
            if (drawable instanceof BitmapDrawable) {
                ((BitmapDrawable)drawable).setGravity(51);
            }
            final LayerDrawable layerDrawable = new LayerDrawable(new Drawable[] { drawable2, drawable });
            if (Build.VERSION.SDK_INT >= 19) {
                layerDrawable.setAutoMirrored(true);
            }
            return (Drawable)layerDrawable;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            drawable.setAutoMirrored(true);
        }
        return drawable;
    }
    
    private void init(final AttributeSet set, int decorPaddingTop) {
        this.registerMixin(HeaderMixin.class, new HeaderMixin(this, set, decorPaddingTop));
        this.registerMixin(ProgressBarMixin.class, new ProgressBarMixin(this));
        this.registerMixin(NavigationBarMixin.class, new NavigationBarMixin(this));
        final RequireScrollMixin requireScrollMixin = new RequireScrollMixin(this);
        this.registerMixin(RequireScrollMixin.class, requireScrollMixin);
        final ScrollView scrollView = this.getScrollView();
        if (scrollView != null) {
            requireScrollMixin.setScrollHandlingDelegate((RequireScrollMixin.ScrollHandlingDelegate)new ScrollViewScrollHandlingDelegate(requireScrollMixin, scrollView));
        }
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SuwSetupWizardLayout, decorPaddingTop, 0);
        final Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.SuwSetupWizardLayout_suwBackground);
        if (drawable != null) {
            this.setLayoutBackground(drawable);
        }
        else {
            final Drawable drawable2 = obtainStyledAttributes.getDrawable(R.styleable.SuwSetupWizardLayout_suwBackgroundTile);
            if (drawable2 != null) {
                this.setBackgroundTile(drawable2);
            }
        }
        final Drawable drawable3 = obtainStyledAttributes.getDrawable(R.styleable.SuwSetupWizardLayout_suwIllustration);
        if (drawable3 != null) {
            this.setIllustration(drawable3);
        }
        else {
            final Drawable drawable4 = obtainStyledAttributes.getDrawable(R.styleable.SuwSetupWizardLayout_suwIllustrationImage);
            final Drawable drawable5 = obtainStyledAttributes.getDrawable(R.styleable.SuwSetupWizardLayout_suwIllustrationHorizontalTile);
            if (drawable4 != null && drawable5 != null) {
                this.setIllustration(drawable4, drawable5);
            }
        }
        if ((decorPaddingTop = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwSetupWizardLayout_suwDecorPaddingTop, -1)) == -1) {
            decorPaddingTop = this.getResources().getDimensionPixelSize(R.dimen.suw_decor_padding_top);
        }
        this.setDecorPaddingTop(decorPaddingTop);
        float illustrationAspectRatio;
        if ((illustrationAspectRatio = obtainStyledAttributes.getFloat(R.styleable.SuwSetupWizardLayout_suwIllustrationAspectRatio, -1.0f)) == -1.0f) {
            final TypedValue typedValue = new TypedValue();
            this.getResources().getValue(R.dimen.suw_illustration_aspect_ratio, typedValue, true);
            illustrationAspectRatio = typedValue.getFloat();
        }
        this.setIllustrationAspectRatio(illustrationAspectRatio);
        obtainStyledAttributes.recycle();
    }
    
    private void setBackgroundTile(final Drawable layoutBackground) {
        if (layoutBackground instanceof BitmapDrawable) {
            ((BitmapDrawable)layoutBackground).setTileModeXY(Shader$TileMode.REPEAT, Shader$TileMode.REPEAT);
        }
        this.setLayoutBackground(layoutBackground);
    }
    
    private void setIllustration(final Drawable drawable, final Drawable drawable2) {
        final Illustration managedViewById = this.findManagedViewById(R.id.suw_layout_decor);
        if (managedViewById instanceof Illustration) {
            managedViewById.setIllustration(this.getIllustration(drawable, drawable2));
        }
    }
    
    @Override
    protected ViewGroup findContainer(final int n) {
        int suw_layout_content = n;
        if (n == 0) {
            suw_layout_content = R.id.suw_layout_content;
        }
        return super.findContainer(suw_layout_content);
    }
    
    public CharSequence getHeaderText() {
        return this.getMixin(HeaderMixin.class).getText();
    }
    
    public TextView getHeaderTextView() {
        return this.getMixin(HeaderMixin.class).getTextView();
    }
    
    public NavigationBar getNavigationBar() {
        return this.getMixin(NavigationBarMixin.class).getNavigationBar();
    }
    
    public ColorStateList getProgressBarColor() {
        return this.getMixin(ProgressBarMixin.class).getColor();
    }
    
    public ScrollView getScrollView() {
        final View managedViewById = this.findManagedViewById(R.id.suw_bottom_scroll_view);
        ScrollView scrollView;
        if (managedViewById instanceof ScrollView) {
            scrollView = (ScrollView)managedViewById;
        }
        else {
            scrollView = null;
        }
        return scrollView;
    }
    
    public boolean isProgressBarShown() {
        return this.getMixin(ProgressBarMixin.class).isShown();
    }
    
    @Override
    protected View onInflateTemplate(final LayoutInflater layoutInflater, final int n) {
        int suw_template = n;
        if (n == 0) {
            suw_template = R.layout.suw_template;
        }
        return this.inflateTemplate(layoutInflater, R.style.SuwThemeMaterial_Light, suw_template);
    }
    
    protected void onRestoreInstanceState(final Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Ignoring restore instance state ");
            sb.append(parcelable);
            Log.w("SetupWizardLayout", sb.toString());
            super.onRestoreInstanceState(parcelable);
            return;
        }
        final SavedState savedState = (SavedState)parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.setProgressBarShown(savedState.mIsProgressBarShown);
    }
    
    protected Parcelable onSaveInstanceState() {
        final SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.mIsProgressBarShown = this.isProgressBarShown();
        return (Parcelable)savedState;
    }
    
    public void setBackgroundTile(final int n) {
        this.setBackgroundTile(this.getContext().getResources().getDrawable(n));
    }
    
    public void setDecorPaddingTop(final int n) {
        final View managedViewById = this.findManagedViewById(R.id.suw_layout_decor);
        if (managedViewById != null) {
            managedViewById.setPadding(managedViewById.getPaddingLeft(), n, managedViewById.getPaddingRight(), managedViewById.getPaddingBottom());
        }
    }
    
    public void setHeaderText(final int text) {
        this.getMixin(HeaderMixin.class).setText(text);
    }
    
    public void setHeaderText(final CharSequence text) {
        this.getMixin(HeaderMixin.class).setText(text);
    }
    
    public void setIllustration(final Drawable illustration) {
        final Illustration managedViewById = this.findManagedViewById(R.id.suw_layout_decor);
        if (managedViewById instanceof Illustration) {
            managedViewById.setIllustration(illustration);
        }
    }
    
    public void setIllustrationAspectRatio(final float aspectRatio) {
        final Illustration managedViewById = this.findManagedViewById(R.id.suw_layout_decor);
        if (managedViewById instanceof Illustration) {
            managedViewById.setAspectRatio(aspectRatio);
        }
    }
    
    public void setLayoutBackground(final Drawable backgroundDrawable) {
        final View managedViewById = this.findManagedViewById(R.id.suw_layout_decor);
        if (managedViewById != null) {
            managedViewById.setBackgroundDrawable(backgroundDrawable);
        }
    }
    
    public void setProgressBarColor(final ColorStateList color) {
        this.getMixin(ProgressBarMixin.class).setColor(color);
    }
    
    public void setProgressBarShown(final boolean shown) {
        this.getMixin(ProgressBarMixin.class).setShown(shown);
    }
    
    protected static class SavedState extends View.BaseSavedState
    {
        public static final Parcelable.Creator<SavedState> CREATOR;
        boolean mIsProgressBarShown;
        
        static {
            CREATOR = (Parcelable.Creator)new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(final Parcel parcel) {
                    return new SavedState(parcel);
                }
                
                public SavedState[] newArray(final int n) {
                    return new SavedState[n];
                }
            };
        }
        
        public SavedState(final Parcel parcel) {
            super(parcel);
            boolean mIsProgressBarShown = false;
            this.mIsProgressBarShown = false;
            if (parcel.readInt() != 0) {
                mIsProgressBarShown = true;
            }
            this.mIsProgressBarShown = mIsProgressBarShown;
        }
        
        public SavedState(final Parcelable parcelable) {
            super(parcelable);
            this.mIsProgressBarShown = false;
        }
        
        public void writeToParcel(final Parcel parcel, final int n) {
            super.writeToParcel(parcel, n);
            parcel.writeInt((int)(this.mIsProgressBarShown ? 1 : 0));
        }
    }
}
