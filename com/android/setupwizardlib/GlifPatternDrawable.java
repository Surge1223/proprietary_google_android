package com.android.setupwizardlib;

import android.graphics.ColorFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Bitmap$Config;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Bitmap;
import java.lang.ref.SoftReference;
import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;

public class GlifPatternDrawable extends Drawable
{
    @SuppressLint({ "InlinedApi" })
    private static final int[] ATTRS_PRIMARY_COLOR;
    private static SoftReference<Bitmap> sBitmapCache;
    private static int[] sPatternLightness;
    private static Path[] sPatternPaths;
    private int mColor;
    private Paint mTempPaint;
    
    static {
        ATTRS_PRIMARY_COLOR = new int[] { 16843827 };
    }
    
    public GlifPatternDrawable(final int color) {
        this.mTempPaint = new Paint(1);
        this.setColor(color);
    }
    
    public static void invalidatePattern() {
        GlifPatternDrawable.sBitmapCache = null;
    }
    
    private void renderOnCanvas(final Canvas canvas, final float n) {
        canvas.save();
        canvas.scale(n, n);
        this.mTempPaint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.SRC));
        final Path[] sPatternPaths = GlifPatternDrawable.sPatternPaths;
        int i = 0;
        if (sPatternPaths == null) {
            GlifPatternDrawable.sPatternPaths = new Path[7];
            GlifPatternDrawable.sPatternLightness = new int[] { 10, 40, 51, 66, 91, 112, 130 };
            final Path[] sPatternPaths2 = GlifPatternDrawable.sPatternPaths;
            final Path path = new Path();
            (sPatternPaths2[0] = path).moveTo(1029.4f, 357.5f);
            path.lineTo(1366.0f, 759.1f);
            path.lineTo(1366.0f, 0.0f);
            path.lineTo(1137.7f, 0.0f);
            path.close();
            final Path[] sPatternPaths3 = GlifPatternDrawable.sPatternPaths;
            final Path path2 = new Path();
            (sPatternPaths3[1] = path2).moveTo(1138.1f, 0.0f);
            path2.rLineTo(-144.8f, 768.0f);
            path2.rLineTo(372.7f, 0.0f);
            path2.rLineTo(0.0f, -524.0f);
            path2.cubicTo(1290.7f, 121.6f, 1219.2f, 41.1f, 1178.7f, 0.0f);
            path2.close();
            final Path[] sPatternPaths4 = GlifPatternDrawable.sPatternPaths;
            final Path path3 = new Path();
            (sPatternPaths4[2] = path3).moveTo(949.8f, 768.0f);
            path3.rCubicTo(92.6f, -170.6f, 213.0f, -440.3f, 269.4f, -768.0f);
            path3.lineTo(585.0f, 0.0f);
            path3.rLineTo(2.1f, 766.0f);
            path3.close();
            final Path[] sPatternPaths5 = GlifPatternDrawable.sPatternPaths;
            final Path path4 = new Path();
            (sPatternPaths5[3] = path4).moveTo(471.1f, 768.0f);
            path4.rMoveTo(704.5f, 0.0f);
            path4.cubicTo(1123.6f, 563.3f, 1027.4f, 275.2f, 856.2f, 0.0f);
            path4.lineTo(476.4f, 0.0f);
            path4.rLineTo(-5.3f, 768.0f);
            path4.close();
            final Path[] sPatternPaths6 = GlifPatternDrawable.sPatternPaths;
            final Path path5 = new Path();
            (sPatternPaths6[4] = path5).moveTo(323.1f, 768.0f);
            path5.moveTo(777.5f, 768.0f);
            path5.cubicTo(661.9f, 348.8f, 427.2f, 21.4f, 401.2f, 25.4f);
            path5.lineTo(323.1f, 768.0f);
            path5.close();
            final Path[] sPatternPaths7 = GlifPatternDrawable.sPatternPaths;
            final Path path6 = new Path();
            (sPatternPaths7[5] = path6).moveTo(178.44286f, 766.8571f);
            path6.lineTo(308.7f, 768.0f);
            path6.cubicTo(381.7f, 604.6f, 481.6f, 344.3f, 562.2f, 0.0f);
            path6.lineTo(0.0f, 0.0f);
            path6.close();
            final Path[] sPatternPaths8 = GlifPatternDrawable.sPatternPaths;
            final Path path7 = new Path();
            (sPatternPaths8[6] = path7).moveTo(146.0f, 0.0f);
            path7.lineTo(0.0f, 0.0f);
            path7.lineTo(0.0f, 768.0f);
            path7.lineTo(394.2f, 768.0f);
            path7.cubicTo(327.7f, 475.3f, 228.5f, 201.0f, 146.0f, 0.0f);
            path7.close();
        }
        while (i < 7) {
            this.mTempPaint.setColor(GlifPatternDrawable.sPatternLightness[i] << 24);
            canvas.drawPath(GlifPatternDrawable.sPatternPaths[i], this.mTempPaint);
            ++i;
        }
        canvas.restore();
        this.mTempPaint.reset();
    }
    
    public Bitmap createBitmapCache(final int n, final int n2) {
        final float min = Math.min(1.5f, Math.max(n / 1366.0f, n2 / 768.0f));
        final Bitmap bitmap = Bitmap.createBitmap((int)(1366.0f * min), (int)(768.0f * min), Bitmap$Config.ALPHA_8);
        this.renderOnCanvas(new Canvas(bitmap), min);
        return bitmap;
    }
    
    public void draw(final Canvas canvas) {
        final Rect bounds = this.getBounds();
        final int width = bounds.width();
        final int height = bounds.height();
        Bitmap bitmap = null;
        if (GlifPatternDrawable.sBitmapCache != null) {
            bitmap = GlifPatternDrawable.sBitmapCache.get();
        }
        Bitmap bitmap2;
        if ((bitmap2 = bitmap) != null) {
            final int width2 = bitmap.getWidth();
            final int height2 = bitmap.getHeight();
            if (width > width2 && width2 < 2049.0f) {
                bitmap2 = null;
            }
            else {
                bitmap2 = bitmap;
                if (height > height2) {
                    bitmap2 = bitmap;
                    if (height2 < 1152.0f) {
                        bitmap2 = null;
                    }
                }
            }
        }
        Bitmap bitmapCache;
        if ((bitmapCache = bitmap2) == null) {
            this.mTempPaint.reset();
            bitmapCache = this.createBitmapCache(width, height);
            GlifPatternDrawable.sBitmapCache = new SoftReference<Bitmap>(bitmapCache);
            this.mTempPaint.reset();
        }
        canvas.save();
        canvas.clipRect(bounds);
        this.scaleCanvasToBounds(canvas, bitmapCache, bounds);
        canvas.drawColor(-16777216);
        this.mTempPaint.setColor(-1);
        canvas.drawBitmap(bitmapCache, 0.0f, 0.0f, this.mTempPaint);
        canvas.drawColor(this.mColor);
        canvas.restore();
    }
    
    public int getOpacity() {
        return 0;
    }
    
    public void scaleCanvasToBounds(final Canvas canvas, final Bitmap bitmap, final Rect rect) {
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final float n = rect.width() / width;
        final float n2 = rect.height() / height;
        canvas.scale(n, n2);
        if (n2 > n) {
            canvas.scale(n2 / n, 1.0f, 0.146f * width, 0.0f);
        }
        else if (n > n2) {
            canvas.scale(1.0f, n / n2, 0.0f, 0.228f * height);
        }
    }
    
    public void setAlpha(final int n) {
    }
    
    public void setColor(final int n) {
        this.mColor = Color.argb(204, Color.red(n), Color.green(n), Color.blue(n));
        this.invalidateSelf();
    }
    
    public void setColorFilter(final ColorFilter colorFilter) {
    }
}
