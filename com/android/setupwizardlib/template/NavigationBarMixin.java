package com.android.setupwizardlib.template;

import android.view.View;
import com.android.setupwizardlib.R;
import com.android.setupwizardlib.view.NavigationBar;
import com.android.setupwizardlib.TemplateLayout;

public class NavigationBarMixin implements Mixin
{
    private TemplateLayout mTemplateLayout;
    
    public NavigationBarMixin(final TemplateLayout mTemplateLayout) {
        this.mTemplateLayout = mTemplateLayout;
    }
    
    public NavigationBar getNavigationBar() {
        final View managedViewById = this.mTemplateLayout.findManagedViewById(R.id.suw_layout_navigation_bar);
        NavigationBar navigationBar;
        if (managedViewById instanceof NavigationBar) {
            navigationBar = (NavigationBar)managedViewById;
        }
        else {
            navigationBar = null;
        }
        return navigationBar;
    }
}
