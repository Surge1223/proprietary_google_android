package com.android.setupwizardlib.template;

import android.widget.ImageView;
import android.graphics.drawable.Drawable;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import com.android.setupwizardlib.TemplateLayout;

public class IconMixin implements Mixin
{
    private TemplateLayout mTemplateLayout;
    
    public IconMixin(final TemplateLayout mTemplateLayout, final AttributeSet set, int resourceId) {
        this.mTemplateLayout = mTemplateLayout;
        final TypedArray obtainStyledAttributes = mTemplateLayout.getContext().obtainStyledAttributes(set, R.styleable.SuwIconMixin, resourceId, 0);
        resourceId = obtainStyledAttributes.getResourceId(R.styleable.SuwIconMixin_android_icon, 0);
        if (resourceId != 0) {
            this.setIcon(resourceId);
        }
        obtainStyledAttributes.recycle();
    }
    
    public Drawable getIcon() {
        final ImageView view = this.getView();
        Drawable drawable;
        if (view != null) {
            drawable = view.getDrawable();
        }
        else {
            drawable = null;
        }
        return drawable;
    }
    
    protected ImageView getView() {
        return this.mTemplateLayout.findManagedViewById(R.id.suw_layout_icon);
    }
    
    public void setIcon(int n) {
        final ImageView view = this.getView();
        if (view != null) {
            view.setImageResource(n);
            if (n != 0) {
                n = 0;
            }
            else {
                n = 8;
            }
            view.setVisibility(n);
        }
    }
    
    public void setIcon(final Drawable imageDrawable) {
        final ImageView view = this.getView();
        if (view != null) {
            view.setImageDrawable(imageDrawable);
            int visibility;
            if (imageDrawable != null) {
                visibility = 0;
            }
            else {
                visibility = 8;
            }
            view.setVisibility(visibility);
        }
    }
}
