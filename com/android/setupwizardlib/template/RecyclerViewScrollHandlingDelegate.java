package com.android.setupwizardlib.template;

import android.support.v7.widget.RecyclerView;

public class RecyclerViewScrollHandlingDelegate implements ScrollHandlingDelegate
{
    private final RecyclerView mRecyclerView;
    private final RequireScrollMixin mRequireScrollMixin;
    
    public RecyclerViewScrollHandlingDelegate(final RequireScrollMixin mRequireScrollMixin, final RecyclerView mRecyclerView) {
        this.mRequireScrollMixin = mRequireScrollMixin;
        this.mRecyclerView = mRecyclerView;
    }
}
