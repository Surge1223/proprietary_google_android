package com.android.setupwizardlib.template;

import android.content.res.TypedArray;
import android.content.Context;
import com.android.setupwizardlib.items.RecyclerItemAdapter;
import com.android.setupwizardlib.items.ItemInflater;
import com.android.setupwizardlib.items.ItemHierarchy;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import com.android.setupwizardlib.util.DrawableLayoutDirectionHelper;
import android.os.Build.VERSION;
import com.android.setupwizardlib.view.HeaderRecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import com.android.setupwizardlib.TemplateLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.android.setupwizardlib.DividerItemDecoration;
import android.graphics.drawable.Drawable;

public class RecyclerMixin implements Mixin
{
    private Drawable mDefaultDivider;
    private Drawable mDivider;
    private DividerItemDecoration mDividerDecoration;
    private int mDividerInsetEnd;
    private int mDividerInsetStart;
    private View mHeader;
    private final RecyclerView mRecyclerView;
    private TemplateLayout mTemplateLayout;
    
    public RecyclerMixin(final TemplateLayout mTemplateLayout, final RecyclerView mRecyclerView) {
        this.mTemplateLayout = mTemplateLayout;
        this.mDividerDecoration = new DividerItemDecoration(this.mTemplateLayout.getContext());
        (this.mRecyclerView = mRecyclerView).setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(this.mTemplateLayout.getContext()));
        if (mRecyclerView instanceof HeaderRecyclerView) {
            this.mHeader = ((HeaderRecyclerView)mRecyclerView).getHeader();
        }
        this.mRecyclerView.addItemDecoration((RecyclerView.ItemDecoration)this.mDividerDecoration);
    }
    
    private void updateDivider() {
        boolean layoutDirectionResolved = true;
        if (Build.VERSION.SDK_INT >= 19) {
            layoutDirectionResolved = this.mTemplateLayout.isLayoutDirectionResolved();
        }
        if (layoutDirectionResolved) {
            if (this.mDefaultDivider == null) {
                this.mDefaultDivider = this.mDividerDecoration.getDivider();
            }
            this.mDivider = (Drawable)DrawableLayoutDirectionHelper.createRelativeInsetDrawable(this.mDefaultDivider, this.mDividerInsetStart, 0, this.mDividerInsetEnd, 0, (View)this.mTemplateLayout);
            this.mDividerDecoration.setDivider(this.mDivider);
        }
    }
    
    public RecyclerView.Adapter<? extends RecyclerView.ViewHolder> getAdapter() {
        final RecyclerView.Adapter adapter = this.mRecyclerView.getAdapter();
        if (adapter instanceof HeaderRecyclerView.HeaderAdapter) {
            return ((HeaderRecyclerView.HeaderAdapter<? extends RecyclerView.ViewHolder>)adapter).getWrappedAdapter();
        }
        return (RecyclerView.Adapter<? extends RecyclerView.ViewHolder>)adapter;
    }
    
    public Drawable getDivider() {
        return this.mDivider;
    }
    
    @Deprecated
    public int getDividerInset() {
        return this.getDividerInsetStart();
    }
    
    public int getDividerInsetEnd() {
        return this.mDividerInsetEnd;
    }
    
    public int getDividerInsetStart() {
        return this.mDividerInsetStart;
    }
    
    public View getHeader() {
        return this.mHeader;
    }
    
    public RecyclerView getRecyclerView() {
        return this.mRecyclerView;
    }
    
    public void onLayout() {
        if (this.mDivider == null) {
            this.updateDivider();
        }
    }
    
    public void parseAttributes(final AttributeSet set, int dividerInset) {
        final Context context = this.mTemplateLayout.getContext();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwRecyclerMixin, dividerInset, 0);
        dividerInset = obtainStyledAttributes.getResourceId(R.styleable.SuwRecyclerMixin_android_entries, 0);
        if (dividerInset != 0) {
            final RecyclerItemAdapter adapter = new RecyclerItemAdapter(new ItemInflater(context).inflate(dividerInset));
            ((RecyclerView.Adapter)adapter).setHasStableIds(obtainStyledAttributes.getBoolean(R.styleable.SuwRecyclerMixin_suwHasStableIds, false));
            this.setAdapter(adapter);
        }
        dividerInset = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwRecyclerMixin_suwDividerInset, -1);
        if (dividerInset != -1) {
            this.setDividerInset(dividerInset);
        }
        else {
            this.setDividerInsets(obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwRecyclerMixin_suwDividerInsetStart, 0), obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwRecyclerMixin_suwDividerInsetEnd, 0));
        }
        obtainStyledAttributes.recycle();
    }
    
    public void setAdapter(final RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        this.mRecyclerView.setAdapter((RecyclerView.Adapter)adapter);
    }
    
    @Deprecated
    public void setDividerInset(final int n) {
        this.setDividerInsets(n, 0);
    }
    
    public void setDividerInsets(final int mDividerInsetStart, final int mDividerInsetEnd) {
        this.mDividerInsetStart = mDividerInsetStart;
        this.mDividerInsetEnd = mDividerInsetEnd;
        this.updateDivider();
    }
    
    public void setDividerItemDecoration(final DividerItemDecoration mDividerDecoration) {
        this.mRecyclerView.removeItemDecoration((RecyclerView.ItemDecoration)this.mDividerDecoration);
        this.mDividerDecoration = mDividerDecoration;
        this.mRecyclerView.addItemDecoration((RecyclerView.ItemDecoration)this.mDividerDecoration);
        this.updateDivider();
    }
}
