package com.android.setupwizardlib.template;

import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.AbsListView$OnScrollListener;

public class ListViewScrollHandlingDelegate implements AbsListView$OnScrollListener, ScrollHandlingDelegate
{
    private final ListView mListView;
    private final RequireScrollMixin mRequireScrollMixin;
    
    public ListViewScrollHandlingDelegate(final RequireScrollMixin mRequireScrollMixin, final ListView mListView) {
        this.mRequireScrollMixin = mRequireScrollMixin;
        this.mListView = mListView;
    }
    
    public void onScroll(final AbsListView absListView, final int n, final int n2, final int n3) {
        if (n + n2 >= n3) {
            this.mRequireScrollMixin.notifyScrollabilityChange(false);
        }
        else {
            this.mRequireScrollMixin.notifyScrollabilityChange(true);
        }
    }
    
    public void onScrollStateChanged(final AbsListView absListView, final int n) {
    }
}
