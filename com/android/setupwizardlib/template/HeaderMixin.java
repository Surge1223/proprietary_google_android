package com.android.setupwizardlib.template;

import android.widget.TextView;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import com.android.setupwizardlib.TemplateLayout;

public class HeaderMixin implements Mixin
{
    private TemplateLayout mTemplateLayout;
    
    public HeaderMixin(final TemplateLayout mTemplateLayout, final AttributeSet set, final int n) {
        this.mTemplateLayout = mTemplateLayout;
        final TypedArray obtainStyledAttributes = mTemplateLayout.getContext().obtainStyledAttributes(set, R.styleable.SuwHeaderMixin, n, 0);
        final CharSequence text = obtainStyledAttributes.getText(R.styleable.SuwHeaderMixin_suwHeaderText);
        if (text != null) {
            this.setText(text);
        }
        obtainStyledAttributes.recycle();
    }
    
    public CharSequence getText() {
        final TextView textView = this.getTextView();
        CharSequence text;
        if (textView != null) {
            text = textView.getText();
        }
        else {
            text = null;
        }
        return text;
    }
    
    public TextView getTextView() {
        return this.mTemplateLayout.findManagedViewById(R.id.suw_layout_title);
    }
    
    public void setText(final int text) {
        final TextView textView = this.getTextView();
        if (textView != null) {
            textView.setText(text);
        }
    }
    
    public void setText(final CharSequence text) {
        final TextView textView = this.getTextView();
        if (textView != null) {
            textView.setText(text);
        }
    }
}
