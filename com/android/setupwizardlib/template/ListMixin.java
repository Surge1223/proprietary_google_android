package com.android.setupwizardlib.template;

import android.widget.HeaderViewListAdapter;
import android.view.View;
import com.android.setupwizardlib.util.DrawableLayoutDirectionHelper;
import android.os.Build.VERSION;
import android.content.res.TypedArray;
import android.content.Context;
import android.widget.ListAdapter;
import com.android.setupwizardlib.items.ItemHierarchy;
import com.android.setupwizardlib.items.ItemAdapter;
import com.android.setupwizardlib.items.ItemInflater;
import com.android.setupwizardlib.items.ItemGroup;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import com.android.setupwizardlib.TemplateLayout;
import android.widget.ListView;
import android.graphics.drawable.Drawable;

public class ListMixin implements Mixin
{
    private Drawable mDefaultDivider;
    private Drawable mDivider;
    private int mDividerInsetEnd;
    private int mDividerInsetStart;
    private ListView mListView;
    private TemplateLayout mTemplateLayout;
    
    public ListMixin(final TemplateLayout mTemplateLayout, final AttributeSet set, int dividerInset) {
        this.mTemplateLayout = mTemplateLayout;
        final Context context = mTemplateLayout.getContext();
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(set, R.styleable.SuwListMixin, dividerInset, 0);
        dividerInset = obtainStyledAttributes.getResourceId(R.styleable.SuwListMixin_android_entries, 0);
        if (dividerInset != 0) {
            this.setAdapter((ListAdapter)new ItemAdapter(new ItemInflater(context).inflate(dividerInset)));
        }
        dividerInset = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwListMixin_suwDividerInset, -1);
        if (dividerInset != -1) {
            this.setDividerInset(dividerInset);
        }
        else {
            this.setDividerInsets(obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwListMixin_suwDividerInsetStart, 0), obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwListMixin_suwDividerInsetEnd, 0));
        }
        obtainStyledAttributes.recycle();
    }
    
    private ListView getListViewInternal() {
        if (this.mListView == null) {
            final ListView managedViewById = this.mTemplateLayout.findManagedViewById(16908298);
            if (managedViewById instanceof ListView) {
                this.mListView = managedViewById;
            }
        }
        return this.mListView;
    }
    
    private void updateDivider() {
        final ListView listViewInternal = this.getListViewInternal();
        if (listViewInternal == null) {
            return;
        }
        boolean layoutDirectionResolved = true;
        if (Build.VERSION.SDK_INT >= 19) {
            layoutDirectionResolved = this.mTemplateLayout.isLayoutDirectionResolved();
        }
        if (layoutDirectionResolved) {
            if (this.mDefaultDivider == null) {
                this.mDefaultDivider = listViewInternal.getDivider();
            }
            listViewInternal.setDivider(this.mDivider = (Drawable)DrawableLayoutDirectionHelper.createRelativeInsetDrawable(this.mDefaultDivider, this.mDividerInsetStart, 0, this.mDividerInsetEnd, 0, (View)this.mTemplateLayout));
        }
    }
    
    public ListAdapter getAdapter() {
        final ListView listViewInternal = this.getListViewInternal();
        if (listViewInternal == null) {
            return null;
        }
        final ListAdapter adapter = listViewInternal.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            return ((HeaderViewListAdapter)adapter).getWrappedAdapter();
        }
        return adapter;
    }
    
    public Drawable getDivider() {
        return this.mDivider;
    }
    
    @Deprecated
    public int getDividerInset() {
        return this.getDividerInsetStart();
    }
    
    public int getDividerInsetEnd() {
        return this.mDividerInsetEnd;
    }
    
    public int getDividerInsetStart() {
        return this.mDividerInsetStart;
    }
    
    public ListView getListView() {
        return this.getListViewInternal();
    }
    
    public void onLayout() {
        if (this.mDivider == null) {
            this.updateDivider();
        }
    }
    
    public void setAdapter(final ListAdapter adapter) {
        final ListView listViewInternal = this.getListViewInternal();
        if (listViewInternal != null) {
            listViewInternal.setAdapter(adapter);
        }
    }
    
    @Deprecated
    public void setDividerInset(final int n) {
        this.setDividerInsets(n, 0);
    }
    
    public void setDividerInsets(final int mDividerInsetStart, final int mDividerInsetEnd) {
        this.mDividerInsetStart = mDividerInsetStart;
        this.mDividerInsetEnd = mDividerInsetEnd;
        this.updateDivider();
    }
}
