package com.android.setupwizardlib.template;

import android.util.Log;
import android.widget.ScrollView;
import com.android.setupwizardlib.view.BottomScrollView;

public class ScrollViewScrollHandlingDelegate implements ScrollHandlingDelegate, BottomScrollListener
{
    private final RequireScrollMixin mRequireScrollMixin;
    private final BottomScrollView mScrollView;
    
    public ScrollViewScrollHandlingDelegate(final RequireScrollMixin mRequireScrollMixin, final ScrollView scrollView) {
        this.mRequireScrollMixin = mRequireScrollMixin;
        if (scrollView instanceof BottomScrollView) {
            this.mScrollView = (BottomScrollView)scrollView;
        }
        else {
            final StringBuilder sb = new StringBuilder();
            sb.append("Cannot set non-BottomScrollView. Found=");
            sb.append(scrollView);
            Log.w("ScrollViewDelegate", sb.toString());
            this.mScrollView = null;
        }
    }
    
    @Override
    public void onRequiresScroll() {
        this.mRequireScrollMixin.notifyScrollabilityChange(true);
    }
    
    @Override
    public void onScrolledToBottom() {
        this.mRequireScrollMixin.notifyScrollabilityChange(false);
    }
}
