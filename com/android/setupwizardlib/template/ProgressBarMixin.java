package com.android.setupwizardlib.template;

import android.os.Build.VERSION;
import android.view.View;
import com.android.setupwizardlib.R;
import android.view.ViewStub;
import android.widget.ProgressBar;
import com.android.setupwizardlib.TemplateLayout;
import android.content.res.ColorStateList;

public class ProgressBarMixin implements Mixin
{
    private ColorStateList mColor;
    private TemplateLayout mTemplateLayout;
    
    public ProgressBarMixin(final TemplateLayout mTemplateLayout) {
        this.mTemplateLayout = mTemplateLayout;
    }
    
    private ProgressBar getProgressBar() {
        if (this.peekProgressBar() == null) {
            final ViewStub viewStub = this.mTemplateLayout.findManagedViewById(R.id.suw_layout_progress_stub);
            if (viewStub != null) {
                viewStub.inflate();
            }
            this.setColor(this.mColor);
        }
        return this.peekProgressBar();
    }
    
    public ColorStateList getColor() {
        return this.mColor;
    }
    
    public boolean isShown() {
        final View managedViewById = this.mTemplateLayout.findManagedViewById(R.id.suw_layout_progress);
        return managedViewById != null && managedViewById.getVisibility() == 0;
    }
    
    public ProgressBar peekProgressBar() {
        return this.mTemplateLayout.findManagedViewById(R.id.suw_layout_progress);
    }
    
    public void setColor(final ColorStateList progressBackgroundTintList) {
        this.mColor = progressBackgroundTintList;
        if (Build.VERSION.SDK_INT >= 21) {
            final ProgressBar peekProgressBar = this.peekProgressBar();
            if (peekProgressBar != null) {
                peekProgressBar.setIndeterminateTintList(progressBackgroundTintList);
                if (Build.VERSION.SDK_INT >= 23 || progressBackgroundTintList != null) {
                    peekProgressBar.setProgressBackgroundTintList(progressBackgroundTintList);
                }
            }
        }
    }
    
    public void setShown(final boolean b) {
        if (b) {
            final ProgressBar progressBar = this.getProgressBar();
            if (progressBar != null) {
                ((View)progressBar).setVisibility(0);
            }
        }
        else {
            final ProgressBar peekProgressBar = this.peekProgressBar();
            if (peekProgressBar != null) {
                ((View)peekProgressBar).setVisibility(8);
            }
        }
    }
}
