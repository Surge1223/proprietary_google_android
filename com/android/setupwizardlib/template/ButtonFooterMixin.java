package com.android.setupwizardlib.template;

import com.android.setupwizardlib.R;
import com.android.setupwizardlib.TemplateLayout;
import android.view.ViewStub;
import android.content.Context;

public class ButtonFooterMixin implements Mixin
{
    private final Context mContext;
    private final ViewStub mFooterStub;
    
    public ButtonFooterMixin(final TemplateLayout templateLayout) {
        this.mContext = templateLayout.getContext();
        this.mFooterStub = templateLayout.findManagedViewById(R.id.suw_layout_footer);
    }
}
