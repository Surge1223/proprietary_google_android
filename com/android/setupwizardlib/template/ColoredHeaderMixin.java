package com.android.setupwizardlib.template;

import android.widget.TextView;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import com.android.setupwizardlib.R;
import android.util.AttributeSet;
import com.android.setupwizardlib.TemplateLayout;

public class ColoredHeaderMixin extends HeaderMixin
{
    public ColoredHeaderMixin(final TemplateLayout templateLayout, final AttributeSet set, final int n) {
        super(templateLayout, set, n);
        final TypedArray obtainStyledAttributes = templateLayout.getContext().obtainStyledAttributes(set, R.styleable.SuwColoredHeaderMixin, n, 0);
        final ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(R.styleable.SuwColoredHeaderMixin_suwHeaderColor);
        if (colorStateList != null) {
            this.setColor(colorStateList);
        }
        obtainStyledAttributes.recycle();
    }
    
    public ColorStateList getColor() {
        final TextView textView = this.getTextView();
        ColorStateList textColors;
        if (textView != null) {
            textColors = textView.getTextColors();
        }
        else {
            textColors = null;
        }
        return textColors;
    }
    
    public void setColor(final ColorStateList textColor) {
        final TextView textView = this.getTextView();
        if (textView != null) {
            textView.setTextColor(textColor);
        }
    }
}
