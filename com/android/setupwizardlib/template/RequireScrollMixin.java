package com.android.setupwizardlib.template;

import android.os.Looper;
import com.android.setupwizardlib.TemplateLayout;
import android.os.Handler;

public class RequireScrollMixin implements Mixin
{
    private ScrollHandlingDelegate mDelegate;
    private boolean mEverScrolledToBottom;
    private final Handler mHandler;
    private OnRequireScrollStateChangedListener mListener;
    private boolean mRequiringScrollToBottom;
    private final TemplateLayout mTemplateLayout;
    
    public RequireScrollMixin(final TemplateLayout mTemplateLayout) {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mRequiringScrollToBottom = false;
        this.mEverScrolledToBottom = false;
        this.mTemplateLayout = mTemplateLayout;
    }
    
    private void postScrollStateChange(final boolean b) {
        this.mHandler.post((Runnable)new Runnable() {
            @Override
            public void run() {
                if (RequireScrollMixin.this.mListener != null) {
                    RequireScrollMixin.this.mListener.onRequireScrollStateChanged(b);
                }
            }
        });
    }
    
    void notifyScrollabilityChange(final boolean b) {
        if (b == this.mRequiringScrollToBottom) {
            return;
        }
        if (b) {
            if (!this.mEverScrolledToBottom) {
                this.postScrollStateChange(true);
                this.mRequiringScrollToBottom = true;
            }
        }
        else {
            this.postScrollStateChange(false);
            this.mRequiringScrollToBottom = false;
            this.mEverScrolledToBottom = true;
        }
    }
    
    public void setScrollHandlingDelegate(final ScrollHandlingDelegate mDelegate) {
        this.mDelegate = mDelegate;
    }
    
    public interface OnRequireScrollStateChangedListener
    {
        void onRequireScrollStateChanged(final boolean p0);
    }
    
    interface ScrollHandlingDelegate
    {
    }
}
