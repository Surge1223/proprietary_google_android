package com.android.setupwizardlib.span;

import android.text.Spannable;

public class SpanHelper
{
    public static void replaceSpan(final Spannable spannable, final Object o, final Object o2) {
        final int spanStart = spannable.getSpanStart(o);
        final int spanEnd = spannable.getSpanEnd(o);
        spannable.removeSpan(o);
        spannable.setSpan(o2, spanStart, spanEnd, 0);
    }
}
