package com.android.setupwizardlib.span;

import android.text.TextPaint;
import android.text.Selection;
import android.text.Spannable;
import android.widget.TextView;
import android.util.Log;
import android.os.Build.VERSION;
import android.content.ContextWrapper;
import android.content.Context;
import android.view.View;
import android.graphics.Typeface;
import android.text.style.ClickableSpan;

public class LinkSpan extends ClickableSpan
{
    private static final Typeface TYPEFACE_MEDIUM;
    private final String mId;
    
    static {
        TYPEFACE_MEDIUM = Typeface.create("sans-serif-medium", 0);
    }
    
    public LinkSpan(final String mId) {
        this.mId = mId;
    }
    
    private boolean dispatchClick(final View view) {
        boolean onLinkClick = false;
        if (view instanceof OnLinkClickListener) {
            onLinkClick = ((OnLinkClickListener)view).onLinkClick(this);
        }
        boolean b = onLinkClick;
        if (!onLinkClick) {
            final OnClickListener legacyListenerFromContext = this.getLegacyListenerFromContext(view.getContext());
            b = onLinkClick;
            if (legacyListenerFromContext != null) {
                legacyListenerFromContext.onClick(this);
                b = true;
            }
        }
        return b;
    }
    
    @Deprecated
    private OnClickListener getLegacyListenerFromContext(Context baseContext) {
        while (!(baseContext instanceof OnClickListener)) {
            if (!(baseContext instanceof ContextWrapper)) {
                return null;
            }
            baseContext = ((ContextWrapper)baseContext).getBaseContext();
        }
        return (OnClickListener)baseContext;
    }
    
    public String getId() {
        return this.mId;
    }
    
    public void onClick(final View view) {
        if (this.dispatchClick(view)) {
            if (Build.VERSION.SDK_INT >= 19) {
                view.cancelPendingInputEvents();
            }
        }
        else {
            Log.w("LinkSpan", "Dropping click event. No listener attached.");
        }
        if (view instanceof TextView) {
            final CharSequence text = ((TextView)view).getText();
            if (text instanceof Spannable) {
                Selection.setSelection((Spannable)text, 0);
            }
        }
    }
    
    public void updateDrawState(final TextPaint textPaint) {
        super.updateDrawState(textPaint);
        textPaint.setUnderlineText(false);
        textPaint.setTypeface(LinkSpan.TYPEFACE_MEDIUM);
    }
    
    @Deprecated
    public interface OnClickListener
    {
        void onClick(final LinkSpan p0);
    }
    
    public interface OnLinkClickListener
    {
        boolean onLinkClick(final LinkSpan p0);
    }
}
