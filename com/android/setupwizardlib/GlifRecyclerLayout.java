package com.android.setupwizardlib;

import android.view.LayoutInflater;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import com.android.setupwizardlib.template.RecyclerViewScrollHandlingDelegate;
import com.android.setupwizardlib.template.RequireScrollMixin;
import android.util.AttributeSet;
import android.content.Context;
import com.android.setupwizardlib.template.RecyclerMixin;

public class GlifRecyclerLayout extends GlifLayout
{
    protected RecyclerMixin mRecyclerMixin;
    
    public GlifRecyclerLayout(final Context context) {
        this(context, 0, 0);
    }
    
    public GlifRecyclerLayout(final Context context, final int n) {
        this(context, n, 0);
    }
    
    public GlifRecyclerLayout(final Context context, final int n, final int n2) {
        super(context, n, n2);
        this.init(context, null, 0);
    }
    
    public GlifRecyclerLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.init(context, set, 0);
    }
    
    public GlifRecyclerLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.init(context, set, n);
    }
    
    private void init(final Context context, final AttributeSet set, final int n) {
        this.mRecyclerMixin.parseAttributes(set, n);
        this.registerMixin(RecyclerMixin.class, this.mRecyclerMixin);
        final RequireScrollMixin requireScrollMixin = this.getMixin(RequireScrollMixin.class);
        requireScrollMixin.setScrollHandlingDelegate((RequireScrollMixin.ScrollHandlingDelegate)new RecyclerViewScrollHandlingDelegate(requireScrollMixin, this.getRecyclerView()));
    }
    
    @Override
    protected ViewGroup findContainer(final int n) {
        int suw_recycler_view = n;
        if (n == 0) {
            suw_recycler_view = R.id.suw_recycler_view;
        }
        return super.findContainer(suw_recycler_view);
    }
    
    @Override
    public <T extends View> T findManagedViewById(final int n) {
        final View header = this.mRecyclerMixin.getHeader();
        if (header != null) {
            final View viewById = header.findViewById(n);
            if (viewById != null) {
                return (T)viewById;
            }
        }
        return (T)super.findViewById(n);
    }
    
    public RecyclerView.Adapter<? extends RecyclerView.ViewHolder> getAdapter() {
        return this.mRecyclerMixin.getAdapter();
    }
    
    public Drawable getDivider() {
        return this.mRecyclerMixin.getDivider();
    }
    
    @Deprecated
    public int getDividerInset() {
        return this.mRecyclerMixin.getDividerInset();
    }
    
    public int getDividerInsetEnd() {
        return this.mRecyclerMixin.getDividerInsetEnd();
    }
    
    public int getDividerInsetStart() {
        return this.mRecyclerMixin.getDividerInsetStart();
    }
    
    public RecyclerView getRecyclerView() {
        return this.mRecyclerMixin.getRecyclerView();
    }
    
    @Override
    protected View onInflateTemplate(final LayoutInflater layoutInflater, final int n) {
        int suw_glif_recycler_template = n;
        if (n == 0) {
            suw_glif_recycler_template = R.layout.suw_glif_recycler_template;
        }
        return super.onInflateTemplate(layoutInflater, suw_glif_recycler_template);
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.mRecyclerMixin.onLayout();
    }
    
    @Override
    protected void onTemplateInflated() {
        final View viewById = this.findViewById(R.id.suw_recycler_view);
        if (viewById instanceof RecyclerView) {
            this.mRecyclerMixin = new RecyclerMixin(this, (RecyclerView)viewById);
            return;
        }
        throw new IllegalStateException("GlifRecyclerLayout should use a template with recycler view");
    }
    
    public void setAdapter(final RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter) {
        this.mRecyclerMixin.setAdapter(adapter);
    }
    
    @Deprecated
    public void setDividerInset(final int dividerInset) {
        this.mRecyclerMixin.setDividerInset(dividerInset);
    }
    
    public void setDividerItemDecoration(final DividerItemDecoration dividerItemDecoration) {
        this.mRecyclerMixin.setDividerItemDecoration(dividerItemDecoration);
    }
}
