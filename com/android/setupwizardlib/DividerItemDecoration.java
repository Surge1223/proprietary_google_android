package com.android.setupwizardlib;

import android.support.v4.view.ViewCompat;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import android.content.res.TypedArray;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;

public class DividerItemDecoration extends ItemDecoration
{
    private Drawable mDivider;
    private int mDividerCondition;
    private int mDividerHeight;
    private int mDividerIntrinsicHeight;
    
    public DividerItemDecoration() {
    }
    
    public DividerItemDecoration(final Context context) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(R.styleable.SuwDividerItemDecoration);
        final Drawable drawable = obtainStyledAttributes.getDrawable(R.styleable.SuwDividerItemDecoration_android_listDivider);
        final int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.SuwDividerItemDecoration_android_dividerHeight, 0);
        final int int1 = obtainStyledAttributes.getInt(R.styleable.SuwDividerItemDecoration_suwDividerCondition, 0);
        obtainStyledAttributes.recycle();
        this.setDivider(drawable);
        this.setDividerHeight(dimensionPixelSize);
        this.setDividerCondition(int1);
    }
    
    private boolean shouldDrawDividerBelow(final View view, final RecyclerView recyclerView) {
        final RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(view);
        final int layoutPosition = childViewHolder.getLayoutPosition();
        final int n = recyclerView.getAdapter().getItemCount() - 1;
        if (this.isDividerAllowedBelow(childViewHolder)) {
            if (this.mDividerCondition == 0) {
                return true;
            }
        }
        else if (this.mDividerCondition == 1 || layoutPosition == n) {
            return false;
        }
        return layoutPosition >= n || this.isDividerAllowedAbove(recyclerView.findViewHolderForLayoutPosition(layoutPosition + 1));
    }
    
    public Drawable getDivider() {
        return this.mDivider;
    }
    
    @Override
    public void getItemOffsets(final Rect rect, final View view, final RecyclerView recyclerView, final State state) {
        if (this.shouldDrawDividerBelow(view, recyclerView)) {
            int bottom;
            if (this.mDividerHeight != 0) {
                bottom = this.mDividerHeight;
            }
            else {
                bottom = this.mDividerIntrinsicHeight;
            }
            rect.bottom = bottom;
        }
    }
    
    protected boolean isDividerAllowedAbove(final ViewHolder viewHolder) {
        return !(viewHolder instanceof DividedViewHolder) || ((DividedViewHolder)viewHolder).isDividerAllowedAbove();
    }
    
    protected boolean isDividerAllowedBelow(final ViewHolder viewHolder) {
        return !(viewHolder instanceof DividedViewHolder) || ((DividedViewHolder)viewHolder).isDividerAllowedBelow();
    }
    
    @Override
    public void onDraw(final Canvas canvas, final RecyclerView recyclerView, final State state) {
        if (this.mDivider == null) {
            return;
        }
        final int childCount = recyclerView.getChildCount();
        final int width = recyclerView.getWidth();
        int n;
        if (this.mDividerHeight != 0) {
            n = this.mDividerHeight;
        }
        else {
            n = this.mDividerIntrinsicHeight;
        }
        for (int i = 0; i < childCount; ++i) {
            final View child = recyclerView.getChildAt(i);
            if (this.shouldDrawDividerBelow(child, recyclerView)) {
                final int n2 = (int)ViewCompat.getY(child) + child.getHeight();
                this.mDivider.setBounds(0, n2, width, n2 + n);
                this.mDivider.draw(canvas);
            }
        }
    }
    
    public void setDivider(final Drawable mDivider) {
        if (mDivider != null) {
            this.mDividerIntrinsicHeight = mDivider.getIntrinsicHeight();
        }
        else {
            this.mDividerIntrinsicHeight = 0;
        }
        this.mDivider = mDivider;
    }
    
    public void setDividerCondition(final int mDividerCondition) {
        this.mDividerCondition = mDividerCondition;
    }
    
    public void setDividerHeight(final int mDividerHeight) {
        this.mDividerHeight = mDividerHeight;
    }
    
    public interface DividedViewHolder
    {
        boolean isDividerAllowedAbove();
        
        boolean isDividerAllowedBelow();
    }
}
