package com.android.setupwizardlib;

import android.view.LayoutInflater;
import android.view.ViewStub;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.graphics.drawable.Drawable;
import com.android.setupwizardlib.view.StatusBarBackgroundLayout;
import android.graphics.drawable.ColorDrawable;
import android.content.res.TypedArray;
import android.widget.ScrollView;
import android.os.Build.VERSION;
import com.android.setupwizardlib.template.ScrollViewScrollHandlingDelegate;
import com.android.setupwizardlib.template.RequireScrollMixin;
import com.android.setupwizardlib.template.ButtonFooterMixin;
import com.android.setupwizardlib.template.ProgressBarMixin;
import com.android.setupwizardlib.template.IconMixin;
import com.android.setupwizardlib.template.ColoredHeaderMixin;
import com.android.setupwizardlib.template.HeaderMixin;
import android.util.AttributeSet;
import android.content.Context;
import android.content.res.ColorStateList;

public class GlifLayout extends TemplateLayout
{
    private ColorStateList mBackgroundBaseColor;
    private boolean mBackgroundPatterned;
    private boolean mLayoutFullscreen;
    private ColorStateList mPrimaryColor;
    
    public GlifLayout(final Context context) {
        this(context, 0, 0);
    }
    
    public GlifLayout(final Context context, final int n) {
        this(context, n, 0);
    }
    
    public GlifLayout(final Context context, final int n, final int n2) {
        super(context, n, n2);
        this.mBackgroundPatterned = true;
        this.mLayoutFullscreen = true;
        this.init(null, R.attr.suwLayoutTheme);
    }
    
    public GlifLayout(final Context context, final AttributeSet set) {
        super(context, set);
        this.mBackgroundPatterned = true;
        this.mLayoutFullscreen = true;
        this.init(set, R.attr.suwLayoutTheme);
    }
    
    public GlifLayout(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mBackgroundPatterned = true;
        this.mLayoutFullscreen = true;
        this.init(set, n);
    }
    
    private void init(final AttributeSet set, int n) {
        this.registerMixin((Class<ColoredHeaderMixin>)HeaderMixin.class, new ColoredHeaderMixin(this, set, n));
        this.registerMixin(IconMixin.class, new IconMixin(this, set, n));
        this.registerMixin(ProgressBarMixin.class, new ProgressBarMixin(this));
        this.registerMixin(ButtonFooterMixin.class, new ButtonFooterMixin(this));
        final RequireScrollMixin requireScrollMixin = new RequireScrollMixin(this);
        this.registerMixin(RequireScrollMixin.class, requireScrollMixin);
        final ScrollView scrollView = this.getScrollView();
        if (scrollView != null) {
            requireScrollMixin.setScrollHandlingDelegate((RequireScrollMixin.ScrollHandlingDelegate)new ScrollViewScrollHandlingDelegate(requireScrollMixin, scrollView));
        }
        final TypedArray obtainStyledAttributes = this.getContext().obtainStyledAttributes(set, R.styleable.SuwGlifLayout, n, 0);
        final ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(R.styleable.SuwGlifLayout_suwColorPrimary);
        if (colorStateList != null) {
            this.setPrimaryColor(colorStateList);
        }
        this.setBackgroundBaseColor(obtainStyledAttributes.getColorStateList(R.styleable.SuwGlifLayout_suwBackgroundBaseColor));
        this.setBackgroundPatterned(obtainStyledAttributes.getBoolean(R.styleable.SuwGlifLayout_suwBackgroundPatterned, true));
        n = obtainStyledAttributes.getResourceId(R.styleable.SuwGlifLayout_suwFooter, 0);
        if (n != 0) {
            this.inflateFooter(n);
        }
        n = obtainStyledAttributes.getResourceId(R.styleable.SuwGlifLayout_suwStickyHeader, 0);
        if (n != 0) {
            this.inflateStickyHeader(n);
        }
        this.mLayoutFullscreen = obtainStyledAttributes.getBoolean(R.styleable.SuwGlifLayout_suwLayoutFullscreen, true);
        obtainStyledAttributes.recycle();
        if (Build.VERSION.SDK_INT >= 21 && this.mLayoutFullscreen) {
            this.setSystemUiVisibility(1024);
        }
    }
    
    private void updateBackground() {
        final View managedViewById = this.findManagedViewById(R.id.suw_pattern_bg);
        if (managedViewById != null) {
            int n = 0;
            if (this.mBackgroundBaseColor != null) {
                n = this.mBackgroundBaseColor.getDefaultColor();
            }
            else if (this.mPrimaryColor != null) {
                n = this.mPrimaryColor.getDefaultColor();
            }
            Object o;
            if (this.mBackgroundPatterned) {
                o = new GlifPatternDrawable(n);
            }
            else {
                o = new ColorDrawable(n);
            }
            if (managedViewById instanceof StatusBarBackgroundLayout) {
                ((StatusBarBackgroundLayout)managedViewById).setStatusBarBackground((Drawable)o);
            }
            else {
                managedViewById.setBackgroundDrawable((Drawable)o);
            }
        }
    }
    
    @Override
    protected ViewGroup findContainer(final int n) {
        int suw_layout_content = n;
        if (n == 0) {
            suw_layout_content = R.id.suw_layout_content;
        }
        return super.findContainer(suw_layout_content);
    }
    
    public ColorStateList getBackgroundBaseColor() {
        return this.mBackgroundBaseColor;
    }
    
    public ColorStateList getHeaderColor() {
        return this.getMixin((Class<ColoredHeaderMixin>)HeaderMixin.class).getColor();
    }
    
    public CharSequence getHeaderText() {
        return this.getMixin(HeaderMixin.class).getText();
    }
    
    public TextView getHeaderTextView() {
        return this.getMixin(HeaderMixin.class).getTextView();
    }
    
    public Drawable getIcon() {
        return this.getMixin(IconMixin.class).getIcon();
    }
    
    public ColorStateList getPrimaryColor() {
        return this.mPrimaryColor;
    }
    
    public ScrollView getScrollView() {
        final View managedViewById = this.findManagedViewById(R.id.suw_scroll_view);
        ScrollView scrollView;
        if (managedViewById instanceof ScrollView) {
            scrollView = (ScrollView)managedViewById;
        }
        else {
            scrollView = null;
        }
        return scrollView;
    }
    
    public View inflateFooter(final int layoutResource) {
        final ViewStub viewStub = this.findManagedViewById(R.id.suw_layout_footer);
        viewStub.setLayoutResource(layoutResource);
        return viewStub.inflate();
    }
    
    public View inflateStickyHeader(final int layoutResource) {
        final ViewStub viewStub = this.findManagedViewById(R.id.suw_layout_sticky_header);
        viewStub.setLayoutResource(layoutResource);
        return viewStub.inflate();
    }
    
    @Override
    protected View onInflateTemplate(final LayoutInflater layoutInflater, final int n) {
        int suw_glif_template = n;
        if (n == 0) {
            suw_glif_template = R.layout.suw_glif_template;
        }
        return this.inflateTemplate(layoutInflater, R.style.SuwThemeGlif_Light, suw_glif_template);
    }
    
    public void setBackgroundBaseColor(final ColorStateList mBackgroundBaseColor) {
        this.mBackgroundBaseColor = mBackgroundBaseColor;
        this.updateBackground();
    }
    
    public void setBackgroundPatterned(final boolean mBackgroundPatterned) {
        this.mBackgroundPatterned = mBackgroundPatterned;
        this.updateBackground();
    }
    
    public void setHeaderColor(final ColorStateList color) {
        this.getMixin((Class<ColoredHeaderMixin>)HeaderMixin.class).setColor(color);
    }
    
    public void setHeaderText(final int text) {
        this.getMixin(HeaderMixin.class).setText(text);
    }
    
    public void setHeaderText(final CharSequence text) {
        this.getMixin(HeaderMixin.class).setText(text);
    }
    
    public void setIcon(final Drawable icon) {
        this.getMixin(IconMixin.class).setIcon(icon);
    }
    
    public void setPrimaryColor(final ColorStateList list) {
        this.mPrimaryColor = list;
        this.updateBackground();
        this.getMixin(ProgressBarMixin.class).setColor(list);
    }
    
    public void setProgressBarShown(final boolean shown) {
        this.getMixin(ProgressBarMixin.class).setShown(shown);
    }
}
