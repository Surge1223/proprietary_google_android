package com.android.volley.toolbox;

import java.util.LinkedList;
import android.graphics.Bitmap$Config;
import com.android.volley.Response;
import com.android.volley.Request;
import android.graphics.Bitmap;
import android.os.Looper;
import android.widget.ImageView$ScaleType;
import com.android.volley.RequestQueue;
import java.util.HashMap;

public class ImageLoader
{
    private final HashMap<String, BatchedImageRequest> mBatchedResponses;
    private final ImageCache mCache;
    private final HashMap<String, BatchedImageRequest> mInFlightRequests;
    private final RequestQueue mRequestQueue;
    
    private static String getCacheKey(final String s, final int n, final int n2, final ImageView$ScaleType imageView$ScaleType) {
        final StringBuilder sb = new StringBuilder(s.length() + 12);
        sb.append("#W");
        sb.append(n);
        sb.append("#H");
        sb.append(n2);
        sb.append("#S");
        sb.append(imageView$ScaleType.ordinal());
        sb.append(s);
        return sb.toString();
    }
    
    private void throwIfNotOnMainThread() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return;
        }
        throw new IllegalStateException("ImageLoader must be invoked from the main thread.");
    }
    
    public ImageContainer get(final String s, final ImageListener imageListener, final int n, final int n2, final ImageView$ScaleType imageView$ScaleType) {
        this.throwIfNotOnMainThread();
        final String cacheKey = getCacheKey(s, n, n2, imageView$ScaleType);
        final Bitmap bitmap = this.mCache.getBitmap(cacheKey);
        if (bitmap != null) {
            final ImageContainer imageContainer = new ImageContainer(bitmap, s, null, null);
            imageListener.onResponse(imageContainer, true);
            return imageContainer;
        }
        final ImageContainer imageContainer2 = new ImageContainer(null, s, cacheKey, imageListener);
        imageListener.onResponse(imageContainer2, true);
        final BatchedImageRequest batchedImageRequest = this.mInFlightRequests.get(cacheKey);
        if (batchedImageRequest != null) {
            batchedImageRequest.addContainer(imageContainer2);
            return imageContainer2;
        }
        final Request<Bitmap> imageRequest = this.makeImageRequest(s, n, n2, imageView$ScaleType, cacheKey);
        this.mRequestQueue.add((Request<Object>)imageRequest);
        this.mInFlightRequests.put(cacheKey, new BatchedImageRequest(imageRequest, imageContainer2));
        return imageContainer2;
    }
    
    protected Request<Bitmap> makeImageRequest(final String s, final int n, final int n2, final ImageView$ScaleType imageView$ScaleType, final String s2) {
        return new ImageRequest(s, new Response.Listener<Bitmap>() {}, n, n2, imageView$ScaleType, Bitmap$Config.RGB_565, new Response.ErrorListener() {});
    }
    
    private class BatchedImageRequest
    {
        private final LinkedList<ImageContainer> mContainers;
        private final Request<?> mRequest;
        
        public BatchedImageRequest(final Request<?> mRequest, final ImageContainer imageContainer) {
            this.mContainers = new LinkedList<ImageContainer>();
            this.mRequest = mRequest;
            this.mContainers.add(imageContainer);
        }
        
        public void addContainer(final ImageContainer imageContainer) {
            this.mContainers.add(imageContainer);
        }
        
        public boolean removeContainerAndCancelIfNecessary(final ImageContainer imageContainer) {
            this.mContainers.remove(imageContainer);
            if (this.mContainers.size() == 0) {
                this.mRequest.cancel();
                return true;
            }
            return false;
        }
    }
    
    public interface ImageCache
    {
        Bitmap getBitmap(final String p0);
    }
    
    public class ImageContainer
    {
        private Bitmap mBitmap;
        private final String mCacheKey;
        private final ImageListener mListener;
        private final String mRequestUrl;
        
        public ImageContainer(final Bitmap mBitmap, final String mRequestUrl, final String mCacheKey, final ImageListener mListener) {
            this.mBitmap = mBitmap;
            this.mRequestUrl = mRequestUrl;
            this.mCacheKey = mCacheKey;
            this.mListener = mListener;
        }
        
        public void cancelRequest() {
            if (this.mListener == null) {
                return;
            }
            final BatchedImageRequest batchedImageRequest = ImageLoader.this.mInFlightRequests.get(this.mCacheKey);
            if (batchedImageRequest != null) {
                if (batchedImageRequest.removeContainerAndCancelIfNecessary(this)) {
                    ImageLoader.this.mInFlightRequests.remove(this.mCacheKey);
                }
            }
            else {
                final BatchedImageRequest batchedImageRequest2 = ImageLoader.this.mBatchedResponses.get(this.mCacheKey);
                if (batchedImageRequest2 != null) {
                    batchedImageRequest2.removeContainerAndCancelIfNecessary(this);
                    if (batchedImageRequest2.mContainers.size() == 0) {
                        ImageLoader.this.mBatchedResponses.remove(this.mCacheKey);
                    }
                }
            }
        }
        
        public Bitmap getBitmap() {
            return this.mBitmap;
        }
        
        public String getRequestUrl() {
            return this.mRequestUrl;
        }
    }
    
    public interface ImageListener extends ErrorListener
    {
        void onResponse(final ImageContainer p0, final boolean p1);
    }
}
