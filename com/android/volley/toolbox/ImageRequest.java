package com.android.volley.toolbox;

import com.android.volley.RetryPolicy;
import com.android.volley.DefaultRetryPolicy;
import android.widget.ImageView$ScaleType;
import com.android.volley.Response;
import android.graphics.Bitmap$Config;
import android.graphics.Bitmap;
import com.android.volley.Request;

public class ImageRequest extends Request<Bitmap>
{
    private static final Object sDecodeLock;
    private final Bitmap$Config mDecodeConfig;
    private Response.Listener<Bitmap> mListener;
    private final Object mLock;
    private final int mMaxHeight;
    private final int mMaxWidth;
    private final ImageView$ScaleType mScaleType;
    
    static {
        sDecodeLock = new Object();
    }
    
    public ImageRequest(final String s, final Response.Listener<Bitmap> mListener, final int mMaxWidth, final int mMaxHeight, final ImageView$ScaleType mScaleType, final Bitmap$Config mDecodeConfig, final Response.ErrorListener errorListener) {
        super(0, s, errorListener);
        this.mLock = new Object();
        this.setRetryPolicy(new DefaultRetryPolicy(1000, 2, 2.0f));
        this.mListener = mListener;
        this.mDecodeConfig = mDecodeConfig;
        this.mMaxWidth = mMaxWidth;
        this.mMaxHeight = mMaxHeight;
        this.mScaleType = mScaleType;
    }
    
    @Override
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }
    
    @Override
    public Priority getPriority() {
        return Priority.LOW;
    }
}
