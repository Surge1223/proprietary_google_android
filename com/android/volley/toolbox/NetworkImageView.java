package com.android.volley.toolbox;

import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView$ScaleType;
import android.text.TextUtils;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.ImageView;

public class NetworkImageView extends ImageView
{
    private int mDefaultImageId;
    private int mErrorImageId;
    private ImageLoader.ImageContainer mImageContainer;
    private ImageLoader mImageLoader;
    private String mUrl;
    
    public NetworkImageView(final Context context) {
        this(context, null);
    }
    
    public NetworkImageView(final Context context, final AttributeSet set) {
        this(context, set, 0);
    }
    
    public NetworkImageView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
    }
    
    private void setDefaultImageOrNull() {
        if (this.mDefaultImageId != 0) {
            this.setImageResource(this.mDefaultImageId);
        }
        else {
            this.setImageBitmap((Bitmap)null);
        }
    }
    
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        this.invalidate();
    }
    
    void loadImageIfNecessary(final boolean b) {
        final int width = this.getWidth();
        final int height = this.getHeight();
        final ImageView$ScaleType scaleType = this.getScaleType();
        boolean b2 = false;
        boolean b3 = false;
        final ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
        boolean b4 = true;
        final int n = 0;
        if (layoutParams != null) {
            b2 = (this.getLayoutParams().width == -2);
            b3 = (this.getLayoutParams().height == -2);
        }
        if (!b2 || !b3) {
            b4 = false;
        }
        if (width == 0 && height == 0 && !b4) {
            return;
        }
        if (TextUtils.isEmpty((CharSequence)this.mUrl)) {
            if (this.mImageContainer != null) {
                this.mImageContainer.cancelRequest();
                this.mImageContainer = null;
            }
            this.setDefaultImageOrNull();
            return;
        }
        if (this.mImageContainer != null && this.mImageContainer.getRequestUrl() != null) {
            if (this.mImageContainer.getRequestUrl().equals(this.mUrl)) {
                return;
            }
            this.mImageContainer.cancelRequest();
            this.setDefaultImageOrNull();
        }
        int n2;
        if (b2) {
            n2 = 0;
        }
        else {
            n2 = width;
        }
        int n3;
        if (b3) {
            n3 = n;
        }
        else {
            n3 = height;
        }
        this.mImageContainer = this.mImageLoader.get(this.mUrl, (ImageLoader.ImageListener)new ImageLoader.ImageListener() {
            @Override
            public void onResponse(final ImageContainer imageContainer, final boolean b) {
                if (b && b) {
                    NetworkImageView.this.post((Runnable)new Runnable() {
                        @Override
                        public void run() {
                            ImageListener.this.onResponse(imageContainer, false);
                        }
                    });
                    return;
                }
                if (imageContainer.getBitmap() != null) {
                    NetworkImageView.this.setImageBitmap(imageContainer.getBitmap());
                }
                else if (NetworkImageView.this.mDefaultImageId != 0) {
                    NetworkImageView.this.setImageResource(NetworkImageView.this.mDefaultImageId);
                }
            }
        }, n2, n3, scaleType);
    }
    
    protected void onDetachedFromWindow() {
        if (this.mImageContainer != null) {
            this.mImageContainer.cancelRequest();
            this.setImageBitmap((Bitmap)null);
            this.mImageContainer = null;
        }
        super.onDetachedFromWindow();
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        super.onLayout(b, n, n2, n3, n4);
        this.loadImageIfNecessary(true);
    }
    
    public void setDefaultImageResId(final int mDefaultImageId) {
        this.mDefaultImageId = mDefaultImageId;
    }
    
    public void setErrorImageResId(final int mErrorImageId) {
        this.mErrorImageId = mErrorImageId;
    }
}
