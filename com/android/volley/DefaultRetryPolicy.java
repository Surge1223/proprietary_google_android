package com.android.volley;

public class DefaultRetryPolicy implements RetryPolicy
{
    private final float mBackoffMultiplier;
    private int mCurrentTimeoutMs;
    private final int mMaxNumRetries;
    
    public DefaultRetryPolicy() {
        this(2500, 1, 1.0f);
    }
    
    public DefaultRetryPolicy(final int mCurrentTimeoutMs, final int mMaxNumRetries, final float mBackoffMultiplier) {
        this.mCurrentTimeoutMs = mCurrentTimeoutMs;
        this.mMaxNumRetries = mMaxNumRetries;
        this.mBackoffMultiplier = mBackoffMultiplier;
    }
}
