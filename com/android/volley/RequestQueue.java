package com.android.volley;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;

public class RequestQueue
{
    private final PriorityBlockingQueue<Request<?>> mCacheQueue;
    private final Set<Request<?>> mCurrentRequests;
    private final PriorityBlockingQueue<Request<?>> mNetworkQueue;
    private final AtomicInteger mSequenceGenerator;
    
    public <T> Request<T> add(final Request<T> request) {
        request.setRequestQueue(this);
        synchronized (this.mCurrentRequests) {
            this.mCurrentRequests.add(request);
            // monitorexit(this.mCurrentRequests)
            request.setSequence(this.getSequenceNumber());
            request.addMarker("add-to-queue");
            if (!request.shouldCache()) {
                this.mNetworkQueue.add(request);
                return request;
            }
            this.mCacheQueue.add(request);
            return request;
        }
    }
    
    public int getSequenceNumber() {
        return this.mSequenceGenerator.incrementAndGet();
    }
}
