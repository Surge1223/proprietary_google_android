package com.android.volley;

import android.net.Uri;
import android.text.TextUtils;

public abstract class Request<T> implements Comparable<Request<T>>
{
    private Cache.Entry mCacheEntry;
    private boolean mCanceled;
    private final int mDefaultTrafficStatsTag;
    private Response.ErrorListener mErrorListener;
    private final VolleyLog.MarkerLog mEventLog;
    private final Object mLock;
    private final int mMethod;
    private RequestQueue mRequestQueue;
    private boolean mResponseDelivered;
    private RetryPolicy mRetryPolicy;
    private Integer mSequence;
    private boolean mShouldCache;
    private boolean mShouldRetryServerErrors;
    private final String mUrl;
    
    public Request(final int mMethod, final String mUrl, final Response.ErrorListener mErrorListener) {
        VolleyLog.MarkerLog mEventLog;
        if (VolleyLog.MarkerLog.ENABLED) {
            mEventLog = new VolleyLog.MarkerLog();
        }
        else {
            mEventLog = null;
        }
        this.mEventLog = mEventLog;
        this.mLock = new Object();
        this.mShouldCache = true;
        this.mCanceled = false;
        this.mResponseDelivered = false;
        this.mShouldRetryServerErrors = false;
        this.mCacheEntry = null;
        this.mMethod = mMethod;
        this.mUrl = mUrl;
        this.mErrorListener = mErrorListener;
        this.setRetryPolicy(new DefaultRetryPolicy());
        this.mDefaultTrafficStatsTag = findDefaultTrafficStatsTag(mUrl);
    }
    
    private static int findDefaultTrafficStatsTag(String host) {
        if (!TextUtils.isEmpty((CharSequence)host)) {
            final Uri parse = Uri.parse(host);
            if (parse != null) {
                host = parse.getHost();
                if (host != null) {
                    return host.hashCode();
                }
            }
        }
        return 0;
    }
    
    public void addMarker(final String s) {
        if (VolleyLog.MarkerLog.ENABLED) {
            this.mEventLog.add(s, Thread.currentThread().getId());
        }
    }
    
    public void cancel() {
        synchronized (this.mLock) {
            this.mCanceled = true;
            this.mErrorListener = null;
        }
    }
    
    @Override
    public int compareTo(final Request<T> request) {
        final Priority priority = this.getPriority();
        final Priority priority2 = request.getPriority();
        int n;
        if (priority == priority2) {
            n = this.mSequence - request.mSequence;
        }
        else {
            n = priority2.ordinal() - priority.ordinal();
        }
        return n;
    }
    
    public Priority getPriority() {
        return Priority.NORMAL;
    }
    
    public int getTrafficStatsTag() {
        return this.mDefaultTrafficStatsTag;
    }
    
    public String getUrl() {
        return this.mUrl;
    }
    
    public Request<?> setRequestQueue(final RequestQueue mRequestQueue) {
        this.mRequestQueue = mRequestQueue;
        return this;
    }
    
    public Request<?> setRetryPolicy(final RetryPolicy mRetryPolicy) {
        this.mRetryPolicy = mRetryPolicy;
        return this;
    }
    
    public final Request<?> setSequence(final int n) {
        this.mSequence = n;
        return this;
    }
    
    public final boolean shouldCache() {
        return this.mShouldCache;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("0x");
        sb.append(Integer.toHexString(this.getTrafficStatsTag()));
        final String string = sb.toString();
        final StringBuilder sb2 = new StringBuilder();
        String s;
        if (this.mCanceled) {
            s = "[X] ";
        }
        else {
            s = "[ ] ";
        }
        sb2.append(s);
        sb2.append(this.getUrl());
        sb2.append(" ");
        sb2.append(string);
        sb2.append(" ");
        sb2.append(this.getPriority());
        sb2.append(" ");
        sb2.append(this.mSequence);
        return sb2.toString();
    }
    
    public enum Priority
    {
        HIGH, 
        IMMEDIATE, 
        LOW, 
        NORMAL;
    }
}
