Easy method

pick a dir to first try and revere out cat the sources out to a txt file for javac


find com/google/android/settings/external    -name "*.java" > sources.txt

you can build a dir at a time or a file at a time as you please 

javac  -cp ./android-full.jar @sources.txt 

or for single files:

javac  -cp ./android-full.jar com/google/android/settings/external/specialcase/ActiveEdgeSensitivitySetting.java


once you make any *.class files its trivial to make a tmp test jar


jar cvf <name-of-jar> `find /<dir-with-*.class-files> -nae "*.class"`

then you have a jar with classes, but we need to convert this out to dex and have java 8 compat so run this on the lib you just made and it will output a 0x39 java-8 classes.dex  


d8 --release --output .   --min-api 28  <name-of-jar> 

next you can make a new jar that acts similar to framework jars by doing this ...

jar cvf <name 0f new jar> classes.dex 



This is much easier/faster than using gradle/aosp build system to try to fix up stuff

also should be ready to go for aosp once they build local


im attaching a link for my android-full.jar below, its aosp built + studio sdk 29 on top so its pretty much a full needed fw to easily have the entire classpath needed for building java -> class -> bytecode/dex 

https://transfer.sh/9P4yO/android-full.jar


you can see what currently buildable/reversed by looking at the buildable.txt file in the repo