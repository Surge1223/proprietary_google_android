package androidx.versionedparcelable;

import android.util.SparseIntArray;
import android.os.Parcel;

class VersionedParcelParcel extends VersionedParcel
{
    private int mCurrentField;
    private final int mEnd;
    private int mNextRead;
    private final int mOffset;
    private final Parcel mParcel;
    private final SparseIntArray mPositionLookup;
    private final String mPrefix;
    
    VersionedParcelParcel(final Parcel parcel) {
        this(parcel, parcel.dataPosition(), parcel.dataSize(), "");
    }
    
    VersionedParcelParcel(final Parcel mParcel, final int mOffset, final int mEnd, final String mPrefix) {
        this.mPositionLookup = new SparseIntArray();
        this.mCurrentField = -1;
        this.mNextRead = 0;
        this.mParcel = mParcel;
        this.mOffset = mOffset;
        this.mEnd = mEnd;
        this.mNextRead = this.mOffset;
        this.mPrefix = mPrefix;
    }
    
    public void closeField() {
        if (this.mCurrentField >= 0) {
            final int value = this.mPositionLookup.get(this.mCurrentField);
            final int dataPosition = this.mParcel.dataPosition();
            this.mParcel.setDataPosition(value);
            this.mParcel.writeInt(dataPosition - value);
            this.mParcel.setDataPosition(dataPosition);
        }
    }
    
    @Override
    protected VersionedParcel createSubParcel() {
        final Parcel mParcel = this.mParcel;
        final int dataPosition = this.mParcel.dataPosition();
        int n;
        if (this.mNextRead == this.mOffset) {
            n = this.mEnd;
        }
        else {
            n = this.mNextRead;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append(this.mPrefix);
        sb.append("  ");
        return new VersionedParcelParcel(mParcel, dataPosition, n, sb.toString());
    }
    
    public String readString() {
        return this.mParcel.readString();
    }
    
    public void writeString(final String s) {
        this.mParcel.writeString(s);
    }
}
