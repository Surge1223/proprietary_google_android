package androidx.versionedparcelable;

import java.lang.reflect.InvocationTargetException;

public abstract class VersionedParcel
{
    private static <T extends VersionedParcelable> Class findParcelClass(final T t) throws ClassNotFoundException {
        return findParcelClass(t.getClass());
    }
    
    private static Class findParcelClass(final Class<? extends VersionedParcelable> clazz) throws ClassNotFoundException {
        return Class.forName(String.format("%s.%sParcelizer", clazz.getPackage().getName(), clazz.getSimpleName()), false, clazz.getClassLoader());
    }
    
    protected static <T extends VersionedParcelable> T readFromParcel(final String s, final VersionedParcel versionedParcel) {
        try {
            return (T)Class.forName(s, true, VersionedParcel.class.getClassLoader()).getDeclaredMethod("read", VersionedParcel.class).invoke(null, versionedParcel);
        }
        catch (ClassNotFoundException ex) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", ex);
        }
        catch (NoSuchMethodException ex2) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", ex2);
        }
        catch (InvocationTargetException ex3) {
            if (ex3.getCause() instanceof RuntimeException) {
                throw (RuntimeException)ex3.getCause();
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", ex3);
        }
        catch (IllegalAccessException ex4) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", ex4);
        }
    }
    
    protected static <T extends VersionedParcelable> void writeToParcel(final T t, final VersionedParcel versionedParcel) {
        try {
            findParcelClass(t).getDeclaredMethod("write", t.getClass(), VersionedParcel.class).invoke(null, t, versionedParcel);
        }
        catch (ClassNotFoundException ex) {
            throw new RuntimeException("VersionedParcel encountered ClassNotFoundException", ex);
        }
        catch (NoSuchMethodException ex2) {
            throw new RuntimeException("VersionedParcel encountered NoSuchMethodException", ex2);
        }
        catch (InvocationTargetException ex3) {
            if (ex3.getCause() instanceof RuntimeException) {
                throw (RuntimeException)ex3.getCause();
            }
            throw new RuntimeException("VersionedParcel encountered InvocationTargetException", ex3);
        }
        catch (IllegalAccessException ex4) {
            throw new RuntimeException("VersionedParcel encountered IllegalAccessException", ex4);
        }
    }
    
    private void writeVersionedParcelableCreator(final VersionedParcelable versionedParcelable) {
        try {
            this.writeString(findParcelClass(versionedParcelable.getClass()).getName());
        }
        catch (ClassNotFoundException ex) {
            final StringBuilder sb = new StringBuilder();
            sb.append(versionedParcelable.getClass().getSimpleName());
            sb.append(" does not have a Parcelizer");
            throw new RuntimeException(sb.toString(), ex);
        }
    }
    
    protected abstract void closeField();
    
    protected abstract VersionedParcel createSubParcel();
    
    protected abstract String readString();
    
    protected <T extends VersionedParcelable> T readVersionedParcelable() {
        final String string = this.readString();
        if (string == null) {
            return null;
        }
        return (T)readFromParcel(string, this.createSubParcel());
    }
    
    protected abstract void writeString(final String p0);
    
    protected void writeVersionedParcelable(final VersionedParcelable versionedParcelable) {
        if (versionedParcelable == null) {
            this.writeString(null);
            return;
        }
        this.writeVersionedParcelableCreator(versionedParcelable);
        final VersionedParcel subParcel = this.createSubParcel();
        writeToParcel(versionedParcelable, subParcel);
        subParcel.closeField();
    }
}
