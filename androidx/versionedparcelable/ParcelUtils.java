package androidx.versionedparcelable;

import android.os.Parcelable;

public class ParcelUtils
{
    public static <T extends VersionedParcelable> T fromParcelable(final Parcelable parcelable) {
        if (parcelable instanceof ParcelImpl) {
            return ((ParcelImpl)parcelable).getVersionedParcel();
        }
        throw new IllegalArgumentException("Invalid parcel");
    }
    
    public static Parcelable toParcelable(final VersionedParcelable versionedParcelable) {
        return (Parcelable)new ParcelImpl(versionedParcelable);
    }
}
