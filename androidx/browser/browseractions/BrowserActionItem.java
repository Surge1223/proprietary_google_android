package androidx.browser.browseractions;

import android.app.PendingIntent;

public class BrowserActionItem
{
    private final PendingIntent mAction;
    private final int mIconId;
    private final String mTitle;
    
    public BrowserActionItem(final String mTitle, final PendingIntent mAction, final int mIconId) {
        this.mTitle = mTitle;
        this.mAction = mAction;
        this.mIconId = mIconId;
    }
    
    public PendingIntent getAction() {
        return this.mAction;
    }
    
    public int getIconId() {
        return this.mIconId;
    }
    
    public String getTitle() {
        return this.mTitle;
    }
}
