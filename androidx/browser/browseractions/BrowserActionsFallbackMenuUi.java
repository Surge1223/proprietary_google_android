package androidx.browser.browseractions;

import android.app.PendingIntent;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import android.widget.AdapterView;
import android.content.DialogInterface;
import android.content.DialogInterface$OnShowListener;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.text.TextUtils$TruncateAt;
import android.support.v4.widget.TextViewCompat;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.support.customtabs.R;
import android.view.View;
import android.net.Uri;
import java.util.List;
import android.content.Context;
import android.widget.AdapterView$OnItemClickListener;

class BrowserActionsFallbackMenuUi implements AdapterView$OnItemClickListener
{
    private BrowserActionsFallbackMenuDialog mBrowserActionsDialog;
    private final Context mContext;
    private final List<BrowserActionItem> mMenuItems;
    private BrowserActionsFallMenuUiListener mMenuUiListener;
    private final Uri mUri;
    
    BrowserActionsFallbackMenuUi(final Context mContext, final Uri mUri, final List<BrowserActionItem> mMenuItems) {
        this.mContext = mContext;
        this.mUri = mUri;
        this.mMenuItems = mMenuItems;
    }
    
    private BrowserActionsFallbackMenuView initMenuView(final View view) {
        final BrowserActionsFallbackMenuView browserActionsFallbackMenuView = (BrowserActionsFallbackMenuView)view.findViewById(R.id.browser_actions_menu_view);
        final TextView textView = (TextView)view.findViewById(R.id.browser_actions_header_text);
        textView.setText((CharSequence)this.mUri.toString());
        textView.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                if (TextViewCompat.getMaxLines(textView) == Integer.MAX_VALUE) {
                    textView.setMaxLines(1);
                    textView.setEllipsize(TextUtils$TruncateAt.END);
                }
                else {
                    textView.setMaxLines(Integer.MAX_VALUE);
                    textView.setEllipsize((TextUtils$TruncateAt)null);
                }
            }
        });
        final ListView listView = (ListView)view.findViewById(R.id.browser_actions_menu_items);
        listView.setAdapter((ListAdapter)new BrowserActionsFallbackMenuAdapter(this.mMenuItems, this.mContext));
        listView.setOnItemClickListener((AdapterView$OnItemClickListener)this);
        return browserActionsFallbackMenuView;
    }
    
    public void displayMenu() {
        final View inflate = LayoutInflater.from(this.mContext).inflate(R.layout.browser_actions_context_menu_page, (ViewGroup)null);
        (this.mBrowserActionsDialog = new BrowserActionsFallbackMenuDialog(this.mContext, (View)this.initMenuView(inflate))).setContentView(inflate);
        if (this.mMenuUiListener != null) {
            this.mBrowserActionsDialog.setOnShowListener((DialogInterface$OnShowListener)new DialogInterface$OnShowListener() {
                public void onShow(final DialogInterface dialogInterface) {
                    BrowserActionsFallbackMenuUi.this.mMenuUiListener.onMenuShown(inflate);
                }
            });
        }
        this.mBrowserActionsDialog.show();
    }
    
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int n, final long n2) {
        final PendingIntent action = this.mMenuItems.get(n).getAction();
        try {
            action.send();
            this.mBrowserActionsDialog.dismiss();
        }
        catch (PendingIntent$CanceledException ex) {
            Log.e("BrowserActionskMenuUi", "Failed to send custom item action", (Throwable)ex);
        }
    }
    
    void setMenuUiListener(final BrowserActionsFallMenuUiListener mMenuUiListener) {
        this.mMenuUiListener = mMenuUiListener;
    }
    
    interface BrowserActionsFallMenuUiListener
    {
        void onMenuShown(final View p0);
    }
}
