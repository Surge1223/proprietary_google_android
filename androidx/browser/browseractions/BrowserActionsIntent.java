package androidx.browser.browseractions;

import android.text.TextUtils;
import android.app.PendingIntent;
import java.util.ArrayList;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.net.Uri;
import android.content.pm.ResolveInfo;
import java.util.List;
import android.content.Intent;
import android.content.Context;

public class BrowserActionsIntent
{
    private static BrowserActionsFallDialogListener sDialogListenter;
    
    static void launchIntent(final Context context, final Intent intent, final List<ResolveInfo> list) {
        if (list != null && list.size() != 0) {
            final int size = list.size();
            int i = 0;
            if (size == 1) {
                intent.setPackage(list.get(0).activityInfo.packageName);
            }
            else {
                final ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://www.example.com")), 65536);
                if (resolveActivity != null) {
                    final String packageName = resolveActivity.activityInfo.packageName;
                    while (i < list.size()) {
                        if (packageName.equals(list.get(i).activityInfo.packageName)) {
                            intent.setPackage(packageName);
                            break;
                        }
                        ++i;
                    }
                }
            }
            ContextCompat.startActivity(context, intent, null);
            return;
        }
        openFallbackBrowserActionsMenu(context, intent);
    }
    
    private static void openFallbackBrowserActionsMenu(final Context context, final Intent intent) {
        final Uri data = intent.getData();
        final int intExtra = intent.getIntExtra("androidx.browser.browseractions.extra.TYPE", 0);
        final ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("androidx.browser.browseractions.extra.MENU_ITEMS");
        List<BrowserActionItem> browserActionItems;
        if (parcelableArrayListExtra != null) {
            browserActionItems = parseBrowserActionItems(parcelableArrayListExtra);
        }
        else {
            browserActionItems = null;
        }
        openFallbackBrowserActionsMenu(context, data, intExtra, browserActionItems);
    }
    
    private static void openFallbackBrowserActionsMenu(final Context context, final Uri uri, final int n, final List<BrowserActionItem> list) {
        new BrowserActionsFallbackMenuUi(context, uri, list).displayMenu();
        if (BrowserActionsIntent.sDialogListenter != null) {
            BrowserActionsIntent.sDialogListenter.onDialogShown();
        }
    }
    
    public static List<BrowserActionItem> parseBrowserActionItems(final ArrayList<Bundle> list) {
        final ArrayList<BrowserActionItem> list2 = new ArrayList<BrowserActionItem>();
        for (int i = 0; i < list.size(); ++i) {
            final Bundle bundle = list.get(i);
            final String string = bundle.getString("androidx.browser.browseractions.TITLE");
            final PendingIntent pendingIntent = (PendingIntent)bundle.getParcelable("androidx.browser.browseractions.ACTION");
            final int int1 = bundle.getInt("androidx.browser.browseractions.ICON_ID");
            if (TextUtils.isEmpty((CharSequence)string) || pendingIntent == null) {
                throw new IllegalArgumentException("Custom item should contain a non-empty title and non-null intent.");
            }
            list2.add(new BrowserActionItem(string, pendingIntent, int1));
        }
        return list2;
    }
    
    static void setDialogShownListenter(final BrowserActionsFallDialogListener sDialogListenter) {
        BrowserActionsIntent.sDialogListenter = sDialogListenter;
    }
    
    interface BrowserActionsFallDialogListener
    {
        void onDialogShown();
    }
}
