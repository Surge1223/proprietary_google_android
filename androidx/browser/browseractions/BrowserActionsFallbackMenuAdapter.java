package androidx.browser.browseractions;

import android.graphics.drawable.Drawable;
import android.content.res.Resources.Theme;
import android.support.v4.content.res.ResourcesCompat;
import android.widget.TextView;
import android.widget.ImageView;
import android.support.customtabs.R;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import java.util.List;
import android.content.Context;
import android.widget.BaseAdapter;

class BrowserActionsFallbackMenuAdapter extends BaseAdapter
{
    private final Context mContext;
    private final List<BrowserActionItem> mMenuItems;
    
    BrowserActionsFallbackMenuAdapter(final List<BrowserActionItem> mMenuItems, final Context mContext) {
        this.mMenuItems = mMenuItems;
        this.mContext = mContext;
    }
    
    public int getCount() {
        return this.mMenuItems.size();
    }
    
    public Object getItem(final int n) {
        return this.mMenuItems.get(n);
    }
    
    public long getItemId(final int n) {
        return n;
    }
    
    public View getView(final int n, final View view, final ViewGroup viewGroup) {
        final BrowserActionItem browserActionItem = this.mMenuItems.get(n);
        View inflate;
        ViewHolderItem tag;
        if (view == null) {
            inflate = LayoutInflater.from(this.mContext).inflate(R.layout.browser_actions_context_menu_row, (ViewGroup)null);
            tag = new ViewHolderItem();
            tag.mIcon = (ImageView)inflate.findViewById(R.id.browser_actions_menu_item_icon);
            tag.mText = (TextView)inflate.findViewById(R.id.browser_actions_menu_item_text);
            inflate.setTag((Object)tag);
        }
        else {
            final ViewHolderItem viewHolderItem = (ViewHolderItem)view.getTag();
            inflate = view;
            tag = viewHolderItem;
        }
        tag.mText.setText((CharSequence)browserActionItem.getTitle());
        if (browserActionItem.getIconId() != 0) {
            tag.mIcon.setImageDrawable(ResourcesCompat.getDrawable(this.mContext.getResources(), browserActionItem.getIconId(), null));
        }
        else {
            tag.mIcon.setImageDrawable((Drawable)null);
        }
        return inflate;
    }
    
    private static class ViewHolderItem
    {
        ImageView mIcon;
        TextView mText;
    }
}
