package androidx.slice.widget;

import android.view.View$MeasureSpec;
import android.util.Log;
import android.widget.Button;
import java.util.ArrayList;
import android.text.style.StyleSpan;
import android.text.SpannableString;
import android.text.TextUtils;
import android.graphics.drawable.Drawable;
import android.app.PendingIntent$CanceledException;
import android.content.Intent;
import android.widget.SeekBar$OnSeekBarChangeListener;
import androidx.slice.core.SliceQuery;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.widget.SeekBar;
import android.support.v4.graphics.drawable.IconCompat;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.widget.ImageView;
import androidx.slice.SliceItem;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import androidx.slice.view.R;
import android.content.Context;
import android.util.ArrayMap;
import androidx.slice.core.SliceActionImpl;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.slice.core.SliceAction;
import java.util.List;
import android.view.View;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;

public class RowView extends SliceChildView implements View.OnClickListener
{
    private LinearLayout mContent;
    private View mDivider;
    private LinearLayout mEndContainer;
    private List<SliceAction> mHeaderActions;
    private int mIconSize;
    private int mImageSize;
    private boolean mIsHeader;
    private boolean mIsSingleItem;
    private TextView mLastUpdatedText;
    private TextView mPrimaryText;
    private ProgressBar mRangeBar;
    private int mRangeHeight;
    private LinearLayout mRootView;
    private SliceActionImpl mRowAction;
    private RowContent mRowContent;
    private int mRowIndex;
    private TextView mSecondaryText;
    private View mSeeMoreView;
    private LinearLayout mStartContainer;
    private ArrayMap<SliceActionImpl, SliceActionView> mToggles;
    
    public RowView(final Context context) {
        super(context);
        this.mToggles = (ArrayMap<SliceActionImpl, SliceActionView>)new ArrayMap();
        this.mIconSize = this.getContext().getResources().getDimensionPixelSize(R.dimen.abc_slice_icon_size);
        this.mImageSize = this.getContext().getResources().getDimensionPixelSize(R.dimen.abc_slice_small_image_size);
        this.addView((View)(this.mRootView = (LinearLayout)LayoutInflater.from(context).inflate(R.layout.abc_slice_small_template, (ViewGroup)this, false)));
        this.mStartContainer = (LinearLayout)this.findViewById(R.id.icon_frame);
        this.mContent = (LinearLayout)this.findViewById(16908290);
        this.mPrimaryText = (TextView)this.findViewById(16908310);
        this.mSecondaryText = (TextView)this.findViewById(16908304);
        this.mLastUpdatedText = (TextView)this.findViewById(R.id.last_updated);
        this.mDivider = this.findViewById(R.id.divider);
        this.mEndContainer = (LinearLayout)this.findViewById(16908312);
        this.mRangeHeight = context.getResources().getDimensionPixelSize(R.dimen.abc_slice_row_range_height);
    }
    
    private void addAction(final SliceActionImpl sliceActionImpl, final int n, final ViewGroup viewGroup, final boolean b) {
        final SliceActionView sliceActionView = new SliceActionView(this.getContext());
        viewGroup.addView((View)sliceActionView);
        final boolean toggle = sliceActionImpl.isToggle();
        int n2;
        if (toggle) {
            n2 = 3;
        }
        else {
            n2 = 0;
        }
        final EventInfo eventInfo = new EventInfo(this.getMode(), (toggle ^ true) ? 1 : 0, n2, this.mRowIndex);
        if (b) {
            eventInfo.setPosition(0, 0, 1);
        }
        sliceActionView.setAction(sliceActionImpl, eventInfo, this.mObserver, n);
        if (toggle) {
            this.mToggles.put((Object)sliceActionImpl, (Object)sliceActionView);
        }
    }
    
    private boolean addItem(final SliceItem sliceItem, int colorFilter, final boolean b) {
        final IconCompat iconCompat = null;
        final boolean b2 = false;
        SliceItem sliceItem2 = null;
        LinearLayout linearLayout;
        if (b) {
            linearLayout = this.mStartContainer;
        }
        else {
            linearLayout = this.mEndContainer;
        }
        final boolean equals = "slice".equals(sliceItem.getFormat());
        final boolean b3 = true;
        SliceItem sliceItem3 = null;
        Label_0107: {
            if (!equals) {
                sliceItem3 = sliceItem;
                if (!"action".equals(sliceItem.getFormat())) {
                    break Label_0107;
                }
            }
            if (sliceItem.hasHint("shortcut")) {
                this.addAction(new SliceActionImpl(sliceItem), colorFilter, (ViewGroup)linearLayout, b);
                return true;
            }
            sliceItem3 = sliceItem.getSlice().getItems().get(0);
        }
        IconCompat icon;
        boolean hasHint;
        if ("image".equals(sliceItem3.getFormat())) {
            icon = sliceItem3.getIcon();
            hasHint = sliceItem3.hasHint("no_tint");
        }
        else {
            icon = iconCompat;
            hasHint = b2;
            if ("long".equals(sliceItem3.getFormat())) {
                sliceItem2 = sliceItem3;
                hasHint = b2;
                icon = iconCompat;
            }
        }
        final TextView textView = null;
        TextView textView2;
        if (icon != null) {
            final boolean b4 = !hasHint;
            final ImageView imageView = new ImageView(this.getContext());
            imageView.setImageDrawable(icon.loadDrawable(this.getContext()));
            if (b4 && colorFilter != -1) {
                imageView.setColorFilter(colorFilter);
            }
            ((ViewGroup)linearLayout).addView((View)imageView);
            final LinearLayout$LayoutParams layoutParams = (LinearLayout$LayoutParams)imageView.getLayoutParams();
            layoutParams.width = this.mImageSize;
            layoutParams.height = this.mImageSize;
            imageView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            if (b4) {
                colorFilter = this.mIconSize / 2;
            }
            else {
                colorFilter = 0;
            }
            imageView.setPadding(colorFilter, colorFilter, colorFilter, colorFilter);
            textView2 = (TextView)imageView;
        }
        else {
            textView2 = textView;
            if (sliceItem2 != null) {
                textView2 = new TextView(this.getContext());
                textView2.setText(SliceViewUtil.getRelativeTimeString(sliceItem3.getTimestamp()));
                textView2.setTextSize(0, (float)this.mSubtitleSize);
                textView2.setTextColor(this.mSubtitleColor);
                ((ViewGroup)linearLayout).addView((View)textView2);
            }
        }
        return textView2 != null && b3;
    }
    
    private void addRange(final SliceItem sliceItem) {
        final boolean equals = "action".equals(sliceItem.getFormat());
        Object mRangeBar;
        if (equals) {
            mRangeBar = new SeekBar(this.getContext());
        }
        else {
            mRangeBar = new ProgressBar(this.getContext(), (AttributeSet)null, 16842872);
        }
        final Drawable wrap = DrawableCompat.wrap(((ProgressBar)mRangeBar).getProgressDrawable());
        if (this.mTintColor != -1 && wrap != null) {
            DrawableCompat.setTint(wrap, this.mTintColor);
            ((ProgressBar)mRangeBar).setProgressDrawable(wrap);
        }
        final SliceItem subtype = SliceQuery.findSubtype(sliceItem, "int", "min");
        int int1 = 0;
        if (subtype != null) {
            int1 = subtype.getInt();
        }
        final SliceItem subtype2 = SliceQuery.findSubtype(sliceItem, "int", "max");
        if (subtype2 != null) {
            ((ProgressBar)mRangeBar).setMax(subtype2.getInt() - int1);
        }
        final SliceItem subtype3 = SliceQuery.findSubtype(sliceItem, "int", "value");
        if (subtype3 != null) {
            ((ProgressBar)mRangeBar).setProgress(subtype3.getInt() - int1);
        }
        ((ProgressBar)mRangeBar).setVisibility(0);
        this.addView((View)mRangeBar);
        this.mRangeBar = (ProgressBar)mRangeBar;
        if (equals) {
            final SliceItem inputRangeThumb = this.mRowContent.getInputRangeThumb();
            final SeekBar seekBar = (SeekBar)this.mRangeBar;
            if (inputRangeThumb != null) {
                final Drawable loadDrawable = inputRangeThumb.getIcon().loadDrawable(this.getContext());
                if (loadDrawable != null) {
                    seekBar.setThumb(loadDrawable);
                }
            }
            final Drawable wrap2 = DrawableCompat.wrap(seekBar.getThumb());
            if (this.mTintColor != -1 && wrap2 != null) {
                DrawableCompat.setTint(wrap2, this.mTintColor);
                seekBar.setThumb(wrap2);
            }
            seekBar.setOnSeekBarChangeListener((SeekBar$OnSeekBarChangeListener)new SeekBar$OnSeekBarChangeListener() {
                public void onProgressChanged(final SeekBar seekBar, final int n, final boolean b) {
                    final int val$finalMinValue = int1;
                    try {
                        sliceItem.fireAction(RowView.this.getContext(), new Intent().putExtra("android.app.slice.extra.RANGE_VALUE", n + val$finalMinValue));
                    }
                    catch (PendingIntent$CanceledException ex) {}
                }
                
                public void onStartTrackingTouch(final SeekBar seekBar) {
                }
                
                public void onStopTrackingTouch(final SeekBar seekBar) {
                }
            });
        }
    }
    
    private void addSubtitle(final SliceItem sliceItem) {
        final String s = null;
        final boolean mShowLastUpdated = this.mShowLastUpdated;
        final boolean b = true;
        final boolean b2 = false;
        String string = s;
        if (mShowLastUpdated) {
            string = s;
            if (this.mLastUpdated != -1L) {
                string = this.getResources().getString(R.string.abc_slice_updated, new Object[] { SliceViewUtil.getRelativeTimeString(this.mLastUpdated) });
            }
        }
        CharSequence text;
        if (sliceItem != null) {
            text = sliceItem.getText();
        }
        else {
            text = null;
        }
        boolean b3 = b;
        if (TextUtils.isEmpty(text)) {
            b3 = (sliceItem != null && sliceItem.hasHint("partial") && b);
        }
        if (b3) {
            this.mSecondaryText.setText(text);
            final TextView mSecondaryText = this.mSecondaryText;
            int n;
            if (this.mIsHeader) {
                n = this.mHeaderSubtitleSize;
            }
            else {
                n = this.mSubtitleSize;
            }
            mSecondaryText.setTextSize(0, (float)n);
            this.mSecondaryText.setTextColor(this.mSubtitleColor);
            int n2;
            if (this.mIsHeader) {
                n2 = this.mVerticalHeaderTextPadding;
            }
            else {
                n2 = this.mVerticalTextPadding;
            }
            this.mSecondaryText.setPadding(0, n2, 0, 0);
        }
        String string2;
        if ((string2 = string) != null) {
            string2 = string;
            if (!TextUtils.isEmpty(text)) {
                final StringBuilder sb = new StringBuilder();
                sb.append(" · ");
                sb.append((Object)string);
                string2 = sb.toString();
            }
            final SpannableString text2 = new SpannableString((CharSequence)string2);
            text2.setSpan((Object)new StyleSpan(2), 0, string2.length(), 0);
            this.mLastUpdatedText.setText((CharSequence)text2);
            final TextView mLastUpdatedText = this.mLastUpdatedText;
            int n3;
            if (this.mIsHeader) {
                n3 = this.mHeaderSubtitleSize;
            }
            else {
                n3 = this.mSubtitleSize;
            }
            mLastUpdatedText.setTextSize(0, (float)n3);
            this.mLastUpdatedText.setTextColor(this.mSubtitleColor);
        }
        final TextView mLastUpdatedText2 = this.mLastUpdatedText;
        int visibility;
        if (TextUtils.isEmpty((CharSequence)string2)) {
            visibility = 8;
        }
        else {
            visibility = 0;
        }
        mLastUpdatedText2.setVisibility(visibility);
        final TextView mSecondaryText2 = this.mSecondaryText;
        int visibility2;
        if (b3) {
            visibility2 = (b2 ? 1 : 0);
        }
        else {
            visibility2 = 8;
        }
        mSecondaryText2.setVisibility(visibility2);
        this.mSecondaryText.requestLayout();
        this.mLastUpdatedText.requestLayout();
    }
    
    private int getRowContentHeight() {
        int n;
        if (this.getMode() != 1 && !this.mIsSingleItem) {
            n = this.getActualHeight();
        }
        else {
            n = this.getSmallHeight();
        }
        int n2 = n;
        if (this.mRangeBar != null) {
            n2 = n - this.mRangeHeight;
        }
        return n2;
    }
    
    private void populateViews() {
        this.resetView();
        if (this.mRowContent.getLayoutDirItem() != null) {
            this.setLayoutDirection(this.mRowContent.getLayoutDirItem().getInt());
        }
        if (this.mRowContent.isDefaultSeeMore()) {
            this.showSeeMore();
            return;
        }
        final CharSequence contentDescription = this.mRowContent.getContentDescription();
        if (contentDescription != null) {
            this.mContent.setContentDescription(contentDescription);
        }
        final SliceItem startItem = this.mRowContent.getStartItem();
        boolean addItem;
        if (addItem = (startItem != null && this.mRowIndex > 0)) {
            addItem = this.addItem(startItem, this.mTintColor, true);
        }
        final LinearLayout mStartContainer = this.mStartContainer;
        int visibility;
        if (addItem) {
            visibility = 0;
        }
        else {
            visibility = 8;
        }
        mStartContainer.setVisibility(visibility);
        final SliceItem titleItem = this.mRowContent.getTitleItem();
        if (titleItem != null) {
            this.mPrimaryText.setText(titleItem.getText());
        }
        final TextView mPrimaryText = this.mPrimaryText;
        int n;
        if (this.mIsHeader) {
            n = this.mHeaderTitleSize;
        }
        else {
            n = this.mTitleSize;
        }
        mPrimaryText.setTextSize(0, (float)n);
        this.mPrimaryText.setTextColor(this.mTitleColor);
        final TextView mPrimaryText2 = this.mPrimaryText;
        int visibility2;
        if (titleItem != null) {
            visibility2 = 0;
        }
        else {
            visibility2 = 8;
        }
        mPrimaryText2.setVisibility(visibility2);
        SliceItem sliceItem;
        if (this.getMode() == 1) {
            sliceItem = this.mRowContent.getSummaryItem();
        }
        else {
            sliceItem = this.mRowContent.getSubtitleItem();
        }
        this.addSubtitle(sliceItem);
        final SliceItem primaryAction = this.mRowContent.getPrimaryAction();
        if (primaryAction != null && primaryAction != startItem) {
            this.mRowAction = new SliceActionImpl(primaryAction);
            if (this.mRowAction.isToggle()) {
                this.addAction(this.mRowAction, this.mTintColor, (ViewGroup)this.mEndContainer, false);
                this.setViewClickable((View)this.mRootView, true);
                return;
            }
        }
        final SliceItem range = this.mRowContent.getRange();
        if (range != null) {
            if (this.mRowAction != null) {
                this.setViewClickable((View)this.mRootView, true);
            }
            this.addRange(range);
            return;
        }
        Object o;
        final ArrayList<SliceItem> list = (ArrayList<SliceItem>)(o = this.mRowContent.getEndItems());
        if (this.mHeaderActions != null) {
            o = list;
            if (this.mHeaderActions.size() > 0) {
                o = this.mHeaderActions;
            }
        }
        int n2 = 0;
        SliceItem sliceItem2 = null;
        int n3 = 0;
        int n4;
        int n5;
        SliceItem sliceItem4;
        for (int i = 0; i < ((List)o).size(); ++i, n2 = n4, n3 = n5, sliceItem2 = sliceItem4) {
            SliceItem sliceItem3;
            if (((List<SliceItem>)o).get(i) instanceof SliceItem) {
                sliceItem3 = ((List<SliceItem>)o).get(i);
            }
            else {
                sliceItem3 = ((SliceActionImpl)((List<SliceItem>)o).get(i)).getSliceItem();
            }
            n4 = n2;
            n5 = n3;
            sliceItem4 = sliceItem2;
            if (n3 < 3) {
                n4 = n2;
                n5 = n3;
                sliceItem4 = sliceItem2;
                if (this.addItem(sliceItem3, this.mTintColor, false)) {
                    SliceItem sliceItem5;
                    if ((sliceItem5 = sliceItem2) == null) {
                        sliceItem5 = sliceItem2;
                        if (SliceQuery.find(sliceItem3, "action") != null) {
                            sliceItem5 = sliceItem3;
                        }
                    }
                    final int n6 = n3 + 1;
                    n4 = n2;
                    n5 = n6;
                    sliceItem4 = sliceItem5;
                    if (n6 == 1) {
                        final boolean b = !this.mToggles.isEmpty() && SliceQuery.find(sliceItem3.getSlice(), "image") == null;
                        sliceItem4 = sliceItem5;
                        n5 = n6;
                        n4 = (b ? 1 : 0);
                    }
                }
            }
        }
        final View mDivider = this.mDivider;
        int visibility3;
        if (this.mRowAction != null && n2 != 0) {
            visibility3 = 0;
        }
        else {
            visibility3 = 8;
        }
        mDivider.setVisibility(visibility3);
        final boolean b2 = startItem != null && SliceQuery.find(startItem, "action") != null;
        final boolean b3 = sliceItem2 != null;
        if (this.mRowAction != null) {
            LinearLayout linearLayout;
            if (!b3 && !b2) {
                linearLayout = this.mRootView;
            }
            else {
                linearLayout = this.mContent;
            }
            this.setViewClickable((View)linearLayout, true);
        }
        else if (b3 != b2 && (n3 == 1 || b2)) {
            if (!this.mToggles.isEmpty()) {
                this.mRowAction = this.mToggles.keySet().iterator().next();
            }
            else {
                if (sliceItem2 == null) {
                    sliceItem2 = startItem;
                }
                this.mRowAction = new SliceActionImpl(sliceItem2);
            }
            this.setViewClickable((View)this.mRootView, true);
        }
    }
    
    private void setViewClickable(final View view, final boolean clickable) {
        final Drawable drawable = null;
        Object onClickListener;
        if (clickable) {
            onClickListener = this;
        }
        else {
            onClickListener = null;
        }
        view.setOnClickListener((View.OnClickListener)onClickListener);
        Drawable drawable2 = drawable;
        if (clickable) {
            drawable2 = SliceViewUtil.getDrawable(this.getContext(), 16843534);
        }
        view.setBackground(drawable2);
        view.setClickable(clickable);
    }
    
    private void showSeeMore() {
        final Button mSeeMoreView = (Button)LayoutInflater.from(this.getContext()).inflate(R.layout.abc_slice_row_show_more, (ViewGroup)this, false);
        mSeeMoreView.setOnClickListener((View.OnClickListener)new View.OnClickListener() {
            public void onClick(final View view) {
                try {
                    if (RowView.this.mObserver != null) {
                        RowView.this.mObserver.onSliceAction(new EventInfo(RowView.this.getMode(), 4, 0, RowView.this.mRowIndex), RowView.this.mRowContent.getSlice());
                    }
                    RowView.this.mRowContent.getSlice().fireAction(null, null);
                }
                catch (PendingIntent$CanceledException ex) {
                    Log.e("RowView", "PendingIntent for slice cannot be sent", (Throwable)ex);
                }
            }
        });
        if (this.mTintColor != -1) {
            mSeeMoreView.setTextColor(this.mTintColor);
        }
        this.mSeeMoreView = (View)mSeeMoreView;
        this.mRootView.addView(this.mSeeMoreView);
    }
    
    @Override
    public int getActualHeight() {
        if (this.mIsSingleItem) {
            return this.getSmallHeight();
        }
        int actualHeight;
        if (this.mRowContent != null && this.mRowContent.isValid()) {
            actualHeight = this.mRowContent.getActualHeight();
        }
        else {
            actualHeight = 0;
        }
        return actualHeight;
    }
    
    @Override
    public int getSmallHeight() {
        int smallHeight;
        if (this.mRowContent != null && this.mRowContent.isValid()) {
            smallHeight = this.mRowContent.getSmallHeight();
        }
        else {
            smallHeight = 0;
        }
        return smallHeight;
    }
    
    public void onClick(final View view) {
        if (this.mRowAction != null && this.mRowAction.getActionItem() != null) {
            if (this.mRowAction.isToggle() && !(view instanceof SliceActionView)) {
                final SliceActionView sliceActionView = (SliceActionView)this.mToggles.get((Object)this.mRowAction);
                if (sliceActionView != null) {
                    sliceActionView.toggle();
                }
            }
            else {
                try {
                    this.mRowAction.getActionItem().fireAction(null, null);
                    if (this.mObserver != null) {
                        this.mObserver.onSliceAction(new EventInfo(this.getMode(), 3, 0, this.mRowIndex), this.mRowAction.getSliceItem());
                    }
                }
                catch (PendingIntent$CanceledException ex) {
                    Log.e("RowView", "PendingIntent for slice cannot be sent", (Throwable)ex);
                }
            }
        }
    }
    
    protected void onLayout(final boolean b, final int n, final int n2, final int n3, final int n4) {
        this.mRootView.layout(0, 0, this.mRootView.getMeasuredWidth(), this.getRowContentHeight());
        if (this.mRangeBar != null) {
            this.mRangeBar.layout(0, this.getRowContentHeight(), this.mRangeBar.getMeasuredWidth(), this.getRowContentHeight() + this.mRangeHeight);
        }
    }
    
    protected void onMeasure(final int n, int n2) {
        if (this.getMode() == 1) {
            n2 = this.getSmallHeight();
        }
        else {
            n2 = this.getActualHeight();
        }
        final int rowContentHeight = this.getRowContentHeight();
        if (rowContentHeight != 0) {
            this.mRootView.setVisibility(0);
            this.measureChild((View)this.mRootView, n, View$MeasureSpec.makeMeasureSpec(rowContentHeight, 1073741824));
        }
        else {
            this.mRootView.setVisibility(8);
        }
        if (this.mRangeBar != null) {
            this.measureChild((View)this.mRangeBar, n, View$MeasureSpec.makeMeasureSpec(this.mRangeHeight, 1073741824));
        }
        super.onMeasure(n, View$MeasureSpec.makeMeasureSpec(n2, 1073741824));
    }
    
    @Override
    public void resetView() {
        this.mRootView.setVisibility(0);
        this.setLayoutDirection(2);
        this.setViewClickable((View)this.mRootView, false);
        this.setViewClickable((View)this.mContent, false);
        this.mStartContainer.removeAllViews();
        this.mEndContainer.removeAllViews();
        this.mPrimaryText.setText((CharSequence)null);
        this.mSecondaryText.setText((CharSequence)null);
        this.mLastUpdatedText.setText((CharSequence)null);
        this.mLastUpdatedText.setVisibility(8);
        this.mToggles.clear();
        this.mRowAction = null;
        this.mDivider.setVisibility(8);
        if (this.mRangeBar != null) {
            this.removeView((View)this.mRangeBar);
            this.mRangeBar = null;
        }
        if (this.mSeeMoreView != null) {
            this.mRootView.removeView(this.mSeeMoreView);
            this.mSeeMoreView = null;
        }
    }
    
    @Override
    public void setShowLastUpdated(final boolean showLastUpdated) {
        super.setShowLastUpdated(showLastUpdated);
        if (this.mRowContent != null) {
            this.populateViews();
        }
    }
    
    public void setSingleItem(final boolean mIsSingleItem) {
        this.mIsSingleItem = mIsSingleItem;
    }
    
    @Override
    public void setSliceActions(final List<SliceAction> mHeaderActions) {
        this.mHeaderActions = mHeaderActions;
        if (this.mRowContent != null) {
            this.populateViews();
        }
    }
    
    @Override
    public void setSliceItem(final SliceItem sliceItem, final boolean b, final int mRowIndex, final int n, final SliceView.OnSliceActionListener sliceActionListener) {
        this.setSliceActionListener(sliceActionListener);
        this.mRowIndex = mRowIndex;
        this.mIsHeader = ListContent.isValidHeader(sliceItem);
        this.mHeaderActions = null;
        this.mRowContent = new RowContent(this.getContext(), sliceItem, this.mIsHeader);
        this.populateViews();
    }
    
    @Override
    public void setTint(final int tint) {
        super.setTint(tint);
        if (this.mRowContent != null) {
            this.populateViews();
        }
    }
}
