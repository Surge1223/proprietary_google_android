package androidx.slice.widget;

import android.view.ViewGroup;
import android.view.View;
import android.widget.ImageView;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.graphics.drawable.shapes.Shape;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.Drawable;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.app.PendingIntent;
import androidx.slice.Slice;
import androidx.slice.core.SliceQuery;
import android.content.res.Resources;
import androidx.slice.view.R;
import android.content.Context;
import android.net.Uri;
import androidx.slice.SliceItem;

public class ShortcutView extends SliceChildView
{
    private SliceItem mActionItem;
    private SliceItem mIcon;
    private SliceItem mLabel;
    private int mLargeIconSize;
    private ListContent mListContent;
    private int mSmallIconSize;
    private Uri mUri;
    
    public ShortcutView(final Context context) {
        super(context);
        final Resources resources = this.getResources();
        this.mSmallIconSize = resources.getDimensionPixelSize(R.dimen.abc_slice_icon_size);
        this.mLargeIconSize = resources.getDimensionPixelSize(R.dimen.abc_slice_shortcut_size);
    }
    
    private void determineShortcutItems(final Context context) {
        if (this.mListContent == null) {
            return;
        }
        final SliceItem primaryAction = this.mListContent.getPrimaryAction();
        final Slice slice = this.mListContent.getSlice();
        if (primaryAction != null) {
            this.mActionItem = primaryAction.getSlice().getItems().get(0);
            final Slice slice2 = primaryAction.getSlice();
            final String s = null;
            this.mIcon = SliceQuery.find(slice2, "image", s, null);
            this.mLabel = SliceQuery.find(primaryAction.getSlice(), "text", s, null);
        }
        else {
            this.mActionItem = SliceQuery.find(slice, "action", null, (String)null);
        }
        if (this.mIcon == null) {
            this.mIcon = SliceQuery.find(slice, "image", "title", null);
        }
        if (this.mLabel == null) {
            this.mLabel = SliceQuery.find(slice, "text", "title", null);
        }
        if (this.mIcon == null) {
            this.mIcon = SliceQuery.find(slice, "image", null, (String)null);
        }
        if (this.mLabel == null) {
            this.mLabel = SliceQuery.find(slice, "text", null, (String)null);
        }
        if (this.mIcon == null || this.mLabel == null || this.mActionItem == null) {
            final PackageManager packageManager = context.getPackageManager();
            final ApplicationInfo applicationInfo = packageManager.resolveContentProvider(slice.getUri().getAuthority(), 0).applicationInfo;
            if (applicationInfo != null) {
                if (this.mIcon == null) {
                    final Slice.Builder builder = new Slice.Builder(slice.getUri());
                    builder.addIcon(SliceViewUtil.createIconFromDrawable(packageManager.getApplicationIcon(applicationInfo)), "large", new String[0]);
                    this.mIcon = builder.build().getItems().get(0);
                }
                if (this.mLabel == null) {
                    final Slice.Builder builder2 = new Slice.Builder(slice.getUri());
                    builder2.addText(packageManager.getApplicationLabel(applicationInfo), null, new String[0]);
                    this.mLabel = builder2.build().getItems().get(0);
                }
                if (this.mActionItem == null) {
                    this.mActionItem = new SliceItem(PendingIntent.getActivity(context, 0, packageManager.getLaunchIntentForPackage(applicationInfo.packageName), 0), new Slice.Builder(slice.getUri()).build(), "action", null, null);
                }
            }
        }
    }
    
    @Override
    public int getMode() {
        return 3;
    }
    
    public boolean performClick() {
        if (this.mListContent == null) {
            return false;
        }
        if (!this.callOnClick()) {
            try {
                if (this.mActionItem != null) {
                    this.mActionItem.fireAction(null, null);
                }
                else {
                    final Intent setData = new Intent("android.intent.action.VIEW").setData(this.mUri);
                    setData.addFlags(268435456);
                    this.getContext().startActivity(setData);
                }
                if (this.mObserver != null) {
                    final EventInfo eventInfo = new EventInfo(3, 1, -1, 0);
                    SliceItem mActionItem;
                    if (this.mActionItem != null) {
                        mActionItem = this.mActionItem;
                    }
                    else {
                        mActionItem = new SliceItem(this.mListContent.getSlice(), "slice", null, this.mListContent.getSlice().getHints());
                    }
                    this.mObserver.onSliceAction(eventInfo, mActionItem);
                }
            }
            catch (PendingIntent$CanceledException ex) {
                Log.e("ShortcutView", "PendingIntent for slice cannot be sent", (Throwable)ex);
            }
        }
        return true;
    }
    
    @Override
    public void resetView() {
        this.mListContent = null;
        this.mUri = null;
        this.mActionItem = null;
        this.mLabel = null;
        this.mIcon = null;
        this.setBackground((Drawable)null);
        this.removeAllViews();
    }
    
    @Override
    public void setSliceContent(final ListContent mListContent) {
        this.resetView();
        this.mListContent = mListContent;
        if (this.mListContent == null) {
            return;
        }
        this.determineShortcutItems(this.getContext());
        SliceItem sliceItem;
        if ((sliceItem = this.mListContent.getColorItem()) == null) {
            sliceItem = SliceQuery.findSubtype(mListContent.getSlice(), "int", "color");
        }
        int n;
        if (sliceItem != null) {
            n = sliceItem.getInt();
        }
        else {
            n = SliceViewUtil.getColorAccent(this.getContext());
        }
        final Drawable wrap = DrawableCompat.wrap((Drawable)new ShapeDrawable((Shape)new OvalShape()));
        DrawableCompat.setTint(wrap, n);
        final ImageView imageView = new ImageView(this.getContext());
        if (this.mIcon != null && !this.mIcon.hasHint("no_tint")) {
            imageView.setBackground(wrap);
        }
        this.addView((View)imageView);
        if (this.mIcon != null) {
            final boolean hasHint = this.mIcon.hasHint("no_tint");
            int n2;
            if (hasHint) {
                n2 = this.mLargeIconSize;
            }
            else {
                n2 = this.mSmallIconSize;
            }
            SliceViewUtil.createCircledIcon(this.getContext(), n2, this.mIcon.getIcon(), hasHint, (ViewGroup)this);
            this.mUri = mListContent.getSlice().getUri();
            this.setClickable(true);
        }
        else {
            this.setClickable(false);
        }
    }
}
