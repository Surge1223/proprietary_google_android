package androidx.slice.widget;

import java.util.Collection;
import android.support.v4.util.ArraySet;
import java.util.Arrays;
import androidx.slice.SliceSpecs;
import java.util.Set;
import androidx.slice.SliceSpec;

public final class SliceLiveData
{
    public static final SliceSpec OLD_BASIC;
    public static final SliceSpec OLD_LIST;
    public static final Set<SliceSpec> SUPPORTED_SPECS;
    
    static {
        OLD_BASIC = new SliceSpec("androidx.app.slice.BASIC", 1);
        OLD_LIST = new SliceSpec("androidx.app.slice.LIST", 1);
        SUPPORTED_SPECS = new ArraySet<SliceSpec>(Arrays.asList(SliceSpecs.BASIC, SliceSpecs.LIST, SliceLiveData.OLD_BASIC, SliceLiveData.OLD_LIST));
    }
}
