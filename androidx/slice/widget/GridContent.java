package androidx.slice.widget;

import androidx.slice.core.SliceQuery;
import java.util.List;
import android.content.res.Resources;
import androidx.slice.view.R;
import android.content.Context;
import java.util.ArrayList;
import androidx.slice.SliceItem;

public class GridContent
{
    private boolean mAllImages;
    private int mAllImagesHeight;
    private int mBigPicMaxHeight;
    private int mBigPicMinHeight;
    private SliceItem mColorItem;
    private SliceItem mContentDescr;
    private ArrayList<CellContent> mGridContent;
    private boolean mHasImage;
    private int mImageTextHeight;
    private int mLargestImageMode;
    private SliceItem mLayoutDirItem;
    private int mMaxCellLineCount;
    private int mMaxHeight;
    private int mMinHeight;
    private SliceItem mPrimaryAction;
    private SliceItem mSeeMoreItem;
    private SliceItem mTitleItem;
    
    public GridContent(final Context context, final SliceItem sliceItem) {
        this.mGridContent = new ArrayList<CellContent>();
        this.mLargestImageMode = -1;
        this.populate(sliceItem);
        if (context != null) {
            final Resources resources = context.getResources();
            this.mBigPicMinHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_big_pic_min_height);
            this.mBigPicMaxHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_big_pic_max_height);
            this.mAllImagesHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_image_only_height);
            this.mImageTextHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_image_text_height);
            this.mMinHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_min_height);
            this.mMaxHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_max_height);
        }
    }
    
    private List<SliceItem> filterAndProcessItems(final List<SliceItem> list) {
        final ArrayList<SliceItem> list2 = new ArrayList<SliceItem>();
        for (int i = 0; i < list.size(); ++i) {
            final SliceItem mContentDescr = list.get(i);
            final SliceItem find = SliceQuery.find(mContentDescr, null, "see_more", null);
            final boolean b = true;
            final boolean b2 = find != null;
            boolean b3 = b;
            if (!b2) {
                b3 = (mContentDescr.hasAnyHints("shortcut", "see_more", "keywords", "ttl", "last_updated") && b);
            }
            if ("content_description".equals(mContentDescr.getSubType())) {
                this.mContentDescr = mContentDescr;
            }
            else if (!b3) {
                list2.add(mContentDescr);
            }
        }
        return list2;
    }
    
    private int getHeight(final boolean b) {
        final boolean valid = this.isValid();
        boolean b2 = false;
        if (!valid) {
            return 0;
        }
        if (this.mAllImages) {
            int n;
            if (this.mGridContent.size() == 1) {
                if (b) {
                    n = this.mBigPicMinHeight;
                }
                else {
                    n = this.mBigPicMaxHeight;
                }
            }
            else if (this.mLargestImageMode == 0) {
                n = this.mMinHeight;
            }
            else {
                n = this.mAllImagesHeight;
            }
            return n;
        }
        if (this.getMaxCellLineCount() > 1) {
            b2 = true;
        }
        final boolean hasImage = this.hasImage();
        if (b2 && !b) {
            if (hasImage) {
                return this.mMaxHeight;
            }
        }
        else if (this.mLargestImageMode != 0) {
            return this.mImageTextHeight;
        }
        return this.mMinHeight;
    }
    
    private boolean populate(final SliceItem sliceItem) {
        this.mColorItem = SliceQuery.findSubtype(sliceItem, "int", "color");
        if ("slice".equals(sliceItem.getFormat()) || "action".equals(sliceItem.getFormat())) {
            this.mLayoutDirItem = SliceQuery.findTopLevelItem(sliceItem.getSlice(), "int", "layout_direction", null, null);
            if (this.mLayoutDirItem != null) {
                SliceItem mLayoutDirItem;
                if (SliceViewUtil.resolveLayoutDirection(this.mLayoutDirItem.getInt()) != -1) {
                    mLayoutDirItem = this.mLayoutDirItem;
                }
                else {
                    mLayoutDirItem = null;
                }
                this.mLayoutDirItem = mLayoutDirItem;
            }
        }
        this.mSeeMoreItem = SliceQuery.find(sliceItem, null, "see_more", null);
        final SliceItem mSeeMoreItem = this.mSeeMoreItem;
        int i = 0;
        if (mSeeMoreItem != null && "slice".equals(this.mSeeMoreItem.getFormat())) {
            this.mSeeMoreItem = this.mSeeMoreItem.getSlice().getItems().get(0);
        }
        this.mPrimaryAction = SliceQuery.find(sliceItem, "slice", new String[] { "shortcut", "title" }, new String[] { "actions" });
        this.mAllImages = true;
        if ("slice".equals(sliceItem.getFormat())) {
            List<SliceItem> list2;
            final List<SliceItem> list = list2 = sliceItem.getSlice().getItems();
            if (list.size() == 1) {
                list2 = list;
                if ("slice".equals(list.get(0).getFormat())) {
                    list2 = list.get(0).getSlice().getItems();
                }
            }
            List<SliceItem> list4;
            final List<SliceItem> list3 = list4 = this.filterAndProcessItems(list2);
            if (list3.size() == 1) {
                list4 = list3;
                if (list3.get(0).getFormat().equals("slice")) {
                    list4 = list3.get(0).getSlice().getItems();
                }
            }
            while (i < list4.size()) {
                final SliceItem mContentDescr = list4.get(i);
                if ("content_description".equals(mContentDescr.getSubType())) {
                    this.mContentDescr = mContentDescr;
                }
                else {
                    this.processContent(new CellContent(mContentDescr));
                }
                ++i;
            }
        }
        else {
            this.processContent(new CellContent(sliceItem));
        }
        return this.isValid();
    }
    
    private void processContent(final CellContent cellContent) {
        if (cellContent.isValid()) {
            if (this.mTitleItem == null && cellContent.getTitleItem() != null) {
                this.mTitleItem = cellContent.getTitleItem();
            }
            this.mGridContent.add(cellContent);
            if (!cellContent.isImageOnly()) {
                this.mAllImages = false;
            }
            this.mMaxCellLineCount = Math.max(this.mMaxCellLineCount, cellContent.getTextCount());
            this.mHasImage |= cellContent.hasImage();
            this.mLargestImageMode = Math.max(this.mLargestImageMode, cellContent.getImageMode());
        }
    }
    
    public int getActualHeight() {
        return this.getHeight(false);
    }
    
    public CharSequence getContentDescription() {
        CharSequence text;
        if (this.mContentDescr != null) {
            text = this.mContentDescr.getText();
        }
        else {
            text = null;
        }
        return text;
    }
    
    public SliceItem getContentIntent() {
        return this.mPrimaryAction;
    }
    
    public ArrayList<CellContent> getGridContent() {
        return this.mGridContent;
    }
    
    public int getLargestImageMode() {
        return this.mLargestImageMode;
    }
    
    public SliceItem getLayoutDirItem() {
        return this.mLayoutDirItem;
    }
    
    public int getMaxCellLineCount() {
        return this.mMaxCellLineCount;
    }
    
    public SliceItem getSeeMoreItem() {
        return this.mSeeMoreItem;
    }
    
    public int getSmallHeight() {
        return this.getHeight(true);
    }
    
    public boolean hasImage() {
        return this.mHasImage;
    }
    
    public boolean isAllImages() {
        return this.mAllImages;
    }
    
    public boolean isValid() {
        return this.mGridContent.size() > 0;
    }
    
    public static class CellContent
    {
        private ArrayList<SliceItem> mCellItems;
        private SliceItem mContentDescr;
        private SliceItem mContentIntent;
        private boolean mHasImage;
        private int mImageMode;
        private int mTextCount;
        private SliceItem mTitleItem;
        
        public CellContent(final SliceItem sliceItem) {
            this.mCellItems = new ArrayList<SliceItem>();
            this.mImageMode = -1;
            this.populate(sliceItem);
        }
        
        private boolean isValidCellContent(final SliceItem sliceItem) {
            final String format = sliceItem.getFormat();
            final boolean equals = "content_description".equals(sliceItem.getSubType());
            boolean b = true;
            if ((!equals && !sliceItem.hasAnyHints("keywords", "ttl", "last_updated")) || (!"text".equals(format) && !"long".equals(format) && !"image".equals(format))) {
                b = false;
            }
            return b;
        }
        
        public ArrayList<SliceItem> getCellItems() {
            return this.mCellItems;
        }
        
        public CharSequence getContentDescription() {
            CharSequence text;
            if (this.mContentDescr != null) {
                text = this.mContentDescr.getText();
            }
            else {
                text = null;
            }
            return text;
        }
        
        public SliceItem getContentIntent() {
            return this.mContentIntent;
        }
        
        public int getImageMode() {
            return this.mImageMode;
        }
        
        public int getTextCount() {
            return this.mTextCount;
        }
        
        public SliceItem getTitleItem() {
            return this.mTitleItem;
        }
        
        public boolean hasImage() {
            return this.mHasImage;
        }
        
        public boolean isImageOnly() {
            final int size = this.mCellItems.size();
            boolean b = false;
            if (size == 1) {
                b = b;
                if ("image".equals(this.mCellItems.get(0).getFormat())) {
                    b = true;
                }
            }
            return b;
        }
        
        public boolean isValid() {
            return this.mCellItems.size() > 0 && this.mCellItems.size() <= 3;
        }
        
        public boolean populate(final SliceItem mContentIntent) {
            final String format = mContentIntent.getFormat();
            if (!mContentIntent.hasHint("shortcut") && ("slice".equals(format) || "action".equals(format))) {
                List<SliceItem> list2;
                final List<SliceItem> list = list2 = mContentIntent.getSlice().getItems();
                Label_0130: {
                    if (list.size() == 1) {
                        if (!"action".equals(list.get(0).getFormat())) {
                            list2 = list;
                            if (!"slice".equals(list.get(0).getFormat())) {
                                break Label_0130;
                            }
                        }
                        this.mContentIntent = list.get(0);
                        list2 = list.get(0).getSlice().getItems();
                    }
                }
                if ("action".equals(format)) {
                    this.mContentIntent = mContentIntent;
                }
                this.mTextCount = 0;
                int n = 0;
                int n2;
                for (int i = 0; i < list2.size(); ++i, n = n2) {
                    final SliceItem sliceItem = list2.get(i);
                    final String format2 = sliceItem.getFormat();
                    if ("content_description".equals(sliceItem.getSubType())) {
                        this.mContentDescr = sliceItem;
                        n2 = n;
                    }
                    else {
                        final int mTextCount = this.mTextCount;
                        final int n3 = 2;
                        if (mTextCount < 2 && ("text".equals(format2) || "long".equals(format2))) {
                            ++this.mTextCount;
                            this.mCellItems.add(sliceItem);
                            if (this.mTitleItem != null) {
                                n2 = n;
                                if (this.mTitleItem.hasHint("title")) {
                                    continue;
                                }
                                n2 = n;
                                if (!sliceItem.hasHint("title")) {
                                    continue;
                                }
                            }
                            this.mTitleItem = sliceItem;
                            n2 = n;
                        }
                        else if ((n2 = n) < 1) {
                            n2 = n;
                            if ("image".equals(sliceItem.getFormat())) {
                                if (sliceItem.hasHint("no_tint")) {
                                    int mImageMode;
                                    if (sliceItem.hasHint("large")) {
                                        mImageMode = n3;
                                    }
                                    else {
                                        mImageMode = 1;
                                    }
                                    this.mImageMode = mImageMode;
                                }
                                else {
                                    this.mImageMode = 0;
                                }
                                n2 = n + 1;
                                this.mHasImage = true;
                                this.mCellItems.add(sliceItem);
                            }
                        }
                    }
                }
            }
            else if (this.isValidCellContent(mContentIntent)) {
                this.mCellItems.add(mContentIntent);
            }
            return this.isValid();
        }
    }
}
