package androidx.slice.widget;

import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import android.content.Intent;
import android.widget.Checkable;
import android.content.res.Resources;
import androidx.slice.view.R;
import android.content.Context;
import androidx.slice.core.SliceActionImpl;
import android.view.View;
import android.widget.CompoundButton$OnCheckedChangeListener;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;

public class SliceActionView extends FrameLayout implements View.OnClickListener, CompoundButton$OnCheckedChangeListener
{
    private static final int[] STATE_CHECKED;
    private View mActionView;
    private EventInfo mEventInfo;
    private int mIconSize;
    private int mImageSize;
    private SliceView.OnSliceActionListener mObserver;
    private SliceActionImpl mSliceAction;
    
    static {
        STATE_CHECKED = new int[] { 16842912 };
    }
    
    public SliceActionView(final Context context) {
        super(context);
        final Resources resources = this.getContext().getResources();
        this.mIconSize = resources.getDimensionPixelSize(R.dimen.abc_slice_icon_size);
        this.mImageSize = resources.getDimensionPixelSize(R.dimen.abc_slice_small_image_size);
    }
    
    private void sendAction() {
        try {
            if (this.mSliceAction.isToggle()) {
                final boolean checked = ((Checkable)this.mActionView).isChecked();
                this.mSliceAction.getActionItem().fireAction(this.getContext(), new Intent().putExtra("android.app.slice.extra.TOGGLE_STATE", checked));
                if (this.mEventInfo != null) {
                    this.mEventInfo.state = (checked ? 1 : 0);
                }
            }
            else {
                this.mSliceAction.getActionItem().fireAction(null, null);
            }
            if (this.mObserver != null && this.mEventInfo != null) {
                this.mObserver.onSliceAction(this.mEventInfo, this.mSliceAction.getSliceItem());
            }
        }
        catch (PendingIntent$CanceledException ex) {
            if (this.mActionView instanceof Checkable) {
                this.mActionView.setSelected(true ^ ((Checkable)this.mActionView).isChecked());
            }
            Log.e("SliceActionView", "PendingIntent for slice cannot be sent", (Throwable)ex);
        }
    }
    
    public SliceActionImpl getAction() {
        return this.mSliceAction;
    }
    
    public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
        if (this.mSliceAction != null && this.mActionView != null) {
            this.sendAction();
        }
    }
    
    public void onClick(final View view) {
        if (this.mSliceAction != null && this.mActionView != null) {
            this.sendAction();
        }
    }
    
    public void setAction(final SliceActionImpl mSliceAction, final EventInfo mEventInfo, final SliceView.OnSliceActionListener mObserver, int n) {
        this.mSliceAction = mSliceAction;
        this.mEventInfo = mEventInfo;
        this.mObserver = mObserver;
        this.mActionView = null;
        if (mSliceAction.isDefaultToggle()) {
            final Switch mActionView = new Switch(this.getContext());
            this.addView((View)mActionView);
            mActionView.setChecked(mSliceAction.isChecked());
            mActionView.setOnCheckedChangeListener((CompoundButton$OnCheckedChangeListener)this);
            mActionView.setMinimumHeight(this.mImageSize);
            mActionView.setMinimumWidth(this.mImageSize);
            this.mActionView = (View)mActionView;
        }
        else if (mSliceAction.getIcon() != null) {
            if (mSliceAction.isToggle()) {
                final ImageToggle mActionView2 = new ImageToggle(this.getContext());
                mActionView2.setChecked(mSliceAction.isChecked());
                this.mActionView = (View)mActionView2;
            }
            else {
                this.mActionView = (View)new ImageView(this.getContext());
            }
            this.addView(this.mActionView);
            final Drawable loadDrawable = this.mSliceAction.getIcon().loadDrawable(this.getContext());
            ((ImageView)this.mActionView).setImageDrawable(loadDrawable);
            if (n != -1 && this.mSliceAction.getImageMode() == 0 && loadDrawable != null) {
                DrawableCompat.setTint(loadDrawable, n);
            }
            final FrameLayout$LayoutParams layoutParams = (FrameLayout$LayoutParams)this.mActionView.getLayoutParams();
            layoutParams.width = this.mImageSize;
            layoutParams.height = this.mImageSize;
            this.mActionView.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            if (mSliceAction.getImageMode() == 0) {
                n = this.mIconSize / 2;
            }
            else {
                n = 0;
            }
            this.mActionView.setPadding(n, n, n, n);
            n = 16843534;
            if (Build.VERSION.SDK_INT >= 21) {
                n = 16843868;
            }
            this.mActionView.setBackground(SliceViewUtil.getDrawable(this.getContext(), n));
            this.mActionView.setOnClickListener((View.OnClickListener)this);
        }
        if (this.mActionView != null) {
            CharSequence contentDescription;
            if (mSliceAction.getContentDescription() != null) {
                contentDescription = mSliceAction.getContentDescription();
            }
            else {
                contentDescription = mSliceAction.getTitle();
            }
            this.mActionView.setContentDescription(contentDescription);
        }
    }
    
    public void toggle() {
        if (this.mActionView != null && this.mSliceAction != null && this.mSliceAction.isToggle()) {
            ((Checkable)this.mActionView).toggle();
        }
    }
    
    private static class ImageToggle extends ImageView implements View.OnClickListener, Checkable
    {
        private boolean mIsChecked;
        private View.OnClickListener mListener;
        
        ImageToggle(final Context context) {
            super(context);
            super.setOnClickListener((View.OnClickListener)this);
        }
        
        public boolean isChecked() {
            return this.mIsChecked;
        }
        
        public void onClick(final View view) {
            this.toggle();
        }
        
        public int[] onCreateDrawableState(final int n) {
            final int[] onCreateDrawableState = super.onCreateDrawableState(n + 1);
            if (this.mIsChecked) {
                mergeDrawableStates(onCreateDrawableState, SliceActionView.STATE_CHECKED);
            }
            return onCreateDrawableState;
        }
        
        public void setChecked(final boolean mIsChecked) {
            if (this.mIsChecked != mIsChecked) {
                this.mIsChecked = mIsChecked;
                this.refreshDrawableState();
                if (this.mListener != null) {
                    this.mListener.onClick((View)this);
                }
            }
        }
        
        public void setOnClickListener(final View.OnClickListener mListener) {
            this.mListener = mListener;
        }
        
        public void toggle() {
            this.setChecked(this.isChecked() ^ true);
        }
    }
}
