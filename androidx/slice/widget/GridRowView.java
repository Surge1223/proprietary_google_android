package androidx.slice.widget;

import android.view.View$MeasureSpec;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.view.LayoutInflater;
import androidx.slice.core.SliceQuery;
import android.widget.ImageView$ScaleType;
import android.widget.ImageView;
import java.util.Iterator;
import java.util.List;
import android.util.Pair;
import android.view.ViewGroup$MarginLayoutParams;
import android.widget.LinearLayout$LayoutParams;
import android.view.ViewGroup;
import androidx.slice.SliceItem;
import java.util.ArrayList;
import android.content.res.Resources;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.widget.FrameLayout$LayoutParams;
import android.util.AttributeSet;
import android.content.Context;
import androidx.slice.view.R;
import android.widget.LinearLayout;
import android.view.ViewTreeObserver$OnPreDrawListener;
import android.view.View.OnClickListener;

public class GridRowView extends SliceChildView implements View.OnClickListener
{
    private static final int TEXT_LAYOUT;
    private static final int TITLE_TEXT_LAYOUT;
    private GridContent mGridContent;
    private int mGutter;
    private int mIconSize;
    private int mLargeImageHeight;
    private boolean mMaxCellUpdateScheduled;
    private int mMaxCells;
    private ViewTreeObserver$OnPreDrawListener mMaxCellsUpdater;
    private int mRowCount;
    private int mRowIndex;
    private int mSmallImageMinWidth;
    private int mSmallImageSize;
    private int mTextPadding;
    private LinearLayout mViewContainer;
    
    static {
        TITLE_TEXT_LAYOUT = R.layout.abc_slice_title;
        TEXT_LAYOUT = R.layout.abc_slice_secondary_text;
    }
    
    public GridRowView(final Context context) {
        this(context, null);
    }
    
    public GridRowView(final Context context, final AttributeSet set) {
        super(context, set);
        this.mMaxCells = -1;
        this.mMaxCellsUpdater = (ViewTreeObserver$OnPreDrawListener)new ViewTreeObserver$OnPreDrawListener() {
            public boolean onPreDraw() {
                GridRowView.this.mMaxCells = GridRowView.this.getMaxCells();
                GridRowView.this.populateViews();
                GridRowView.this.getViewTreeObserver().removeOnPreDrawListener((ViewTreeObserver$OnPreDrawListener)this);
                GridRowView.this.mMaxCellUpdateScheduled = false;
                return true;
            }
        };
        final Resources resources = this.getContext().getResources();
        (this.mViewContainer = new LinearLayout(this.getContext())).setOrientation(0);
        this.addView((View)this.mViewContainer, (ViewGroup.LayoutParams)new FrameLayout$LayoutParams(-1, -1));
        this.mViewContainer.setGravity(16);
        this.mIconSize = resources.getDimensionPixelSize(R.dimen.abc_slice_icon_size);
        this.mSmallImageSize = resources.getDimensionPixelSize(R.dimen.abc_slice_small_image_size);
        this.mLargeImageHeight = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_image_only_height);
        this.mSmallImageMinWidth = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_image_min_width);
        this.mGutter = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_gutter);
        this.mTextPadding = resources.getDimensionPixelSize(R.dimen.abc_slice_grid_text_padding);
    }
    
    private void addCell(final GridContent.CellContent cellContent, final int n, final int n2) {
        int n3;
        if (this.getMode() == 1) {
            n3 = 1;
        }
        else {
            n3 = 2;
        }
        final LinearLayout linearLayout = new LinearLayout(this.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        final ArrayList<SliceItem> cellItems = cellContent.getCellItems();
        final SliceItem contentIntent = cellContent.getContentIntent();
        final boolean b = cellItems.size() == 1;
        List<SliceItem> list2;
        final List<SliceItem> list = list2 = null;
        if (!b) {
            list2 = list;
            if (this.getMode() == 1) {
                final ArrayList<SliceItem> list3 = new ArrayList<SliceItem>();
                for (final SliceItem sliceItem : cellItems) {
                    if ("text".equals(sliceItem.getFormat())) {
                        list3.add(sliceItem);
                    }
                }
                final Iterator<Object> iterator2 = list3.iterator();
                while (true) {
                    list2 = list3;
                    if (list3.size() <= 1) {
                        break;
                    }
                    if (iterator2.next().hasAnyHints("title", "large")) {
                        continue;
                    }
                    iterator2.remove();
                }
            }
        }
        int n4 = 0;
        int n5 = 0;
        boolean b2 = false;
        SliceItem sliceItem2 = null;
        for (int i = 0; i < cellItems.size(); ++i) {
            final SliceItem sliceItem3 = cellItems.get(i);
            final String format = sliceItem3.getFormat();
            final int determinePadding = this.determinePadding(sliceItem2);
            if (n4 < n3 && ("text".equals(format) || "long".equals(format))) {
                if (list2 == null || list2.contains(sliceItem3)) {
                    if (this.addItem(sliceItem3, this.mTintColor, (ViewGroup)linearLayout, determinePadding, b)) {
                        ++n4;
                        sliceItem2 = sliceItem3;
                        b2 = true;
                    }
                }
            }
            else {
                final int n6 = n5;
                if (n6 < 1 && "image".equals(sliceItem3.getFormat()) && this.addItem(sliceItem3, this.mTintColor, (ViewGroup)linearLayout, 0, b)) {
                    n5 = n6 + 1;
                    sliceItem2 = sliceItem3;
                    b2 = true;
                }
            }
        }
        if (b2) {
            final CharSequence contentDescription = cellContent.getContentDescription();
            if (contentDescription != null) {
                linearLayout.setContentDescription(contentDescription);
            }
            this.mViewContainer.addView((View)linearLayout, (ViewGroup.LayoutParams)new LinearLayout$LayoutParams(0, -2, 1.0f));
            if (n != n2 - 1) {
                final ViewGroup$MarginLayoutParams layoutParams = (ViewGroup$MarginLayoutParams)linearLayout.getLayoutParams();
                layoutParams.setMarginEnd(this.mGutter);
                linearLayout.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
            }
            if (contentIntent != null) {
                final EventInfo eventInfo = new EventInfo(this.getMode(), 1, 1, this.mRowIndex);
                eventInfo.setPosition(2, n, n2);
                linearLayout.setTag((Object)new Pair((Object)contentIntent, (Object)eventInfo));
                this.makeClickable((View)linearLayout, true);
            }
        }
    }
    
    private boolean addItem(final SliceItem sliceItem, int n, final ViewGroup viewGroup, int n2, final boolean b) {
        final String format = sliceItem.getFormat();
        Object o = null;
        final boolean equals = "text".equals(format);
        final boolean b2 = true;
        if (!equals && !"long".equals(format)) {
            if ("image".equals(format)) {
                final ImageView imageView = new ImageView(this.getContext());
                imageView.setImageDrawable(sliceItem.getIcon().loadDrawable(this.getContext()));
                LinearLayout$LayoutParams linearLayout$LayoutParams;
                if (sliceItem.hasHint("large")) {
                    imageView.setScaleType(ImageView$ScaleType.CENTER_CROP);
                    if (b) {
                        n2 = -1;
                    }
                    else {
                        n2 = this.mLargeImageHeight;
                    }
                    linearLayout$LayoutParams = new LinearLayout$LayoutParams(-1, n2);
                }
                else {
                    final boolean b3 = sliceItem.hasHint("no_tint") ^ true;
                    if (b3) {
                        n2 = this.mIconSize;
                    }
                    else {
                        n2 = this.mSmallImageSize;
                    }
                    ImageView$ScaleType scaleType;
                    if (b3) {
                        scaleType = ImageView$ScaleType.CENTER_INSIDE;
                    }
                    else {
                        scaleType = ImageView$ScaleType.CENTER_CROP;
                    }
                    imageView.setScaleType(scaleType);
                    linearLayout$LayoutParams = new LinearLayout$LayoutParams(n2, n2);
                }
                if (n != -1 && !sliceItem.hasHint("no_tint")) {
                    imageView.setColorFilter(n);
                }
                viewGroup.addView((View)imageView, (ViewGroup.LayoutParams)linearLayout$LayoutParams);
                o = imageView;
            }
        }
        else {
            final boolean hasAnyHints = SliceQuery.hasAnyHints(sliceItem, "large", "title");
            final LayoutInflater from = LayoutInflater.from(this.getContext());
            if (hasAnyHints) {
                n = GridRowView.TITLE_TEXT_LAYOUT;
            }
            else {
                n = GridRowView.TEXT_LAYOUT;
            }
            o = from.inflate(n, (ViewGroup)null);
            if (hasAnyHints) {
                n = this.mGridTitleSize;
            }
            else {
                n = this.mGridSubtitleSize;
            }
            ((TextView)o).setTextSize(0, (float)n);
            if (hasAnyHints) {
                n = this.mTitleColor;
            }
            else {
                n = this.mSubtitleColor;
            }
            ((TextView)o).setTextColor(n);
            CharSequence text;
            if ("long".equals(format)) {
                text = SliceViewUtil.getRelativeTimeString(sliceItem.getTimestamp());
            }
            else {
                text = sliceItem.getText();
            }
            ((TextView)o).setText(text);
            viewGroup.addView((View)o);
            ((TextView)o).setPadding(0, n2, 0, 0);
        }
        return o != null && b2;
    }
    
    private void addSeeMoreCount(final int n) {
        final View child = this.mViewContainer.getChildAt(this.mViewContainer.getChildCount() - 1);
        this.mViewContainer.removeView(child);
        final SliceItem seeMoreItem = this.mGridContent.getSeeMoreItem();
        final int childCount = this.mViewContainer.getChildCount();
        final int mMaxCells = this.mMaxCells;
        if (("slice".equals(seeMoreItem.getFormat()) || "action".equals(seeMoreItem.getFormat())) && seeMoreItem.getSlice().getItems().size() > 0) {
            this.addCell(new GridContent.CellContent(seeMoreItem), childCount, mMaxCells);
            return;
        }
        final LayoutInflater from = LayoutInflater.from(this.getContext());
        Object o;
        TextView textView;
        if (this.mGridContent.isAllImages()) {
            o = from.inflate(R.layout.abc_slice_grid_see_more_overlay, (ViewGroup)this.mViewContainer, false);
            ((ViewGroup)o).addView(child, 0, (ViewGroup.LayoutParams)new FrameLayout$LayoutParams(-1, -1));
            textView = (TextView)((ViewGroup)o).findViewById(R.id.text_see_more_count);
        }
        else {
            o = from.inflate(R.layout.abc_slice_grid_see_more, (ViewGroup)this.mViewContainer, false);
            textView = (TextView)((ViewGroup)o).findViewById(R.id.text_see_more_count);
            final TextView textView2 = (TextView)((ViewGroup)o).findViewById(R.id.text_see_more);
            textView2.setTextSize(0, (float)this.mGridTitleSize);
            textView2.setTextColor(this.mTitleColor);
        }
        this.mViewContainer.addView((View)o, (ViewGroup.LayoutParams)new LinearLayout$LayoutParams(0, -1, 1.0f));
        textView.setText((CharSequence)this.getResources().getString(R.string.abc_slice_more_content, new Object[] { n }));
        final EventInfo eventInfo = new EventInfo(this.getMode(), 4, 1, this.mRowIndex);
        eventInfo.setPosition(2, childCount, mMaxCells);
        ((ViewGroup)o).setTag((Object)new Pair((Object)seeMoreItem, (Object)eventInfo));
        this.makeClickable((View)o, true);
    }
    
    private int determinePadding(final SliceItem sliceItem) {
        if (sliceItem == null) {
            return 0;
        }
        if ("image".equals(sliceItem.getFormat())) {
            return this.mTextPadding;
        }
        if (!"text".equals(sliceItem.getFormat()) && !"long".equals(sliceItem.getFormat())) {
            return 0;
        }
        return this.mVerticalGridTextPadding;
    }
    
    private int getExtraBottomPadding() {
        if (this.mGridContent != null && this.mGridContent.isAllImages() && (this.mRowIndex == this.mRowCount - 1 || this.getMode() == 1)) {
            return this.mGridBottomPadding;
        }
        return 0;
    }
    
    private int getExtraTopPadding() {
        if (this.mGridContent != null && this.mGridContent.isAllImages() && this.mRowIndex == 0) {
            return this.mGridTopPadding;
        }
        return 0;
    }
    
    private int getMaxCells() {
        if (this.mGridContent == null || !this.mGridContent.isValid() || this.getWidth() == 0) {
            return -1;
        }
        if (this.mGridContent.getGridContent().size() > 1) {
            int n;
            if (this.mGridContent.getLargestImageMode() == 2) {
                n = this.mLargeImageHeight;
            }
            else {
                n = this.mSmallImageMinWidth;
            }
            return this.getWidth() / (this.mGutter + n);
        }
        return 1;
    }
    
    private void makeClickable(final View view, final boolean clickable) {
        final Drawable drawable = null;
        Object onClickListener;
        if (clickable) {
            onClickListener = this;
        }
        else {
            onClickListener = null;
        }
        view.setOnClickListener((View.OnClickListener)onClickListener);
        Drawable drawable2 = drawable;
        if (clickable) {
            drawable2 = SliceViewUtil.getDrawable(this.getContext(), 16843534);
        }
        view.setBackground(drawable2);
        view.setClickable(clickable);
    }
    
    private void populateViews() {
        if (this.mGridContent == null || !this.mGridContent.isValid()) {
            this.resetView();
            return;
        }
        if (this.scheduleMaxCellsUpdate()) {
            return;
        }
        if (this.mGridContent.getLayoutDirItem() != null) {
            this.setLayoutDirection(this.mGridContent.getLayoutDirItem().getInt());
        }
        final SliceItem contentIntent = this.mGridContent.getContentIntent();
        boolean b = true;
        if (contentIntent != null) {
            this.mViewContainer.setTag((Object)new Pair((Object)this.mGridContent.getContentIntent(), (Object)new EventInfo(this.getMode(), 3, 1, this.mRowIndex)));
            this.makeClickable((View)this.mViewContainer, true);
        }
        final CharSequence contentDescription = this.mGridContent.getContentDescription();
        if (contentDescription != null) {
            this.mViewContainer.setContentDescription(contentDescription);
        }
        final ArrayList<GridContent.CellContent> gridContent = this.mGridContent.getGridContent();
        if (this.mGridContent.getLargestImageMode() == 2) {
            this.mViewContainer.setGravity(48);
        }
        else {
            this.mViewContainer.setGravity(16);
        }
        final int mMaxCells = this.mMaxCells;
        final SliceItem seeMoreItem = this.mGridContent.getSeeMoreItem();
        int i = 0;
        if (seeMoreItem == null) {
            b = false;
        }
        while (i < gridContent.size()) {
            if (this.mViewContainer.getChildCount() >= mMaxCells) {
                if (b) {
                    this.addSeeMoreCount(gridContent.size() - mMaxCells);
                    break;
                }
                break;
            }
            else {
                this.addCell(gridContent.get(i), i, Math.min(gridContent.size(), mMaxCells));
                ++i;
            }
        }
    }
    
    private boolean scheduleMaxCellsUpdate() {
        if (this.mGridContent == null || !this.mGridContent.isValid()) {
            return true;
        }
        if (this.getWidth() == 0) {
            this.mMaxCellUpdateScheduled = true;
            this.getViewTreeObserver().addOnPreDrawListener(this.mMaxCellsUpdater);
            return true;
        }
        this.mMaxCells = this.getMaxCells();
        return false;
    }
    
    @Override
    public int getActualHeight() {
        if (this.mGridContent == null) {
            return 0;
        }
        return this.mGridContent.getActualHeight() + this.getExtraTopPadding() + this.getExtraBottomPadding();
    }
    
    @Override
    public int getSmallHeight() {
        if (this.mGridContent == null) {
            return 0;
        }
        return this.mGridContent.getSmallHeight() + this.getExtraTopPadding() + this.getExtraBottomPadding();
    }
    
    public void onClick(final View view) {
        final Pair pair = (Pair)view.getTag();
        final SliceItem sliceItem = (SliceItem)pair.first;
        final EventInfo eventInfo = (EventInfo)pair.second;
        if (sliceItem != null && "action".equals(sliceItem.getFormat())) {
            try {
                sliceItem.fireAction(null, null);
                if (this.mObserver != null) {
                    this.mObserver.onSliceAction(eventInfo, sliceItem);
                }
            }
            catch (PendingIntent$CanceledException ex) {
                Log.e("GridView", "PendingIntent for slice cannot be sent", (Throwable)ex);
            }
        }
    }
    
    protected void onMeasure(final int n, int height) {
        if (this.getMode() == 1) {
            height = this.getSmallHeight();
        }
        else {
            height = this.getActualHeight();
        }
        final int measureSpec = View$MeasureSpec.makeMeasureSpec(height, 1073741824);
        this.mViewContainer.getLayoutParams().height = height;
        super.onMeasure(n, measureSpec);
    }
    
    @Override
    public void resetView() {
        if (this.mMaxCellUpdateScheduled) {
            this.mMaxCellUpdateScheduled = false;
            this.getViewTreeObserver().removeOnPreDrawListener(this.mMaxCellsUpdater);
        }
        this.mViewContainer.removeAllViews();
        this.setLayoutDirection(2);
        this.makeClickable((View)this.mViewContainer, false);
    }
    
    @Override
    public void setSliceItem(final SliceItem sliceItem, final boolean b, final int mRowIndex, final int mRowCount, final SliceView.OnSliceActionListener sliceActionListener) {
        this.resetView();
        this.setSliceActionListener(sliceActionListener);
        this.mRowIndex = mRowIndex;
        this.mRowCount = mRowCount;
        this.mGridContent = new GridContent(this.getContext(), sliceItem);
        if (!this.scheduleMaxCellsUpdate()) {
            this.populateViews();
        }
        this.mViewContainer.setPadding(0, this.getExtraTopPadding(), 0, this.getExtraBottomPadding());
    }
    
    @Override
    public void setTint(final int tint) {
        super.setTint(tint);
        if (this.mGridContent != null) {
            this.resetView();
            this.populateViews();
        }
    }
}
