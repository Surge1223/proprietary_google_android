package androidx.slice.widget;

import android.content.res.TypedArray;
import androidx.slice.SliceMetadata;
import androidx.slice.core.SliceActionImpl;
import androidx.slice.core.SliceQuery;
import android.content.res.Resources.Theme;
import androidx.slice.view.R;
import android.util.AttributeSet;
import androidx.slice.core.SliceAction;
import java.util.List;
import androidx.slice.Slice;
import java.util.ArrayList;
import android.content.Context;
import androidx.slice.SliceItem;

public class ListContent
{
    private SliceItem mColorItem;
    private Context mContext;
    private int mGridBottomPadding;
    private int mGridSubtitleSize;
    private int mGridTitleSize;
    private int mGridTopPadding;
    private SliceItem mHeaderItem;
    private int mHeaderSubtitleSize;
    private int mHeaderTitleSize;
    private int mLargeHeight;
    private SliceItem mLayoutDirItem;
    private int mMinScrollHeight;
    private ArrayList<SliceItem> mRowItems;
    private SliceItem mSeeMoreItem;
    private Slice mSlice;
    private List<SliceAction> mSliceActions;
    private int mSubtitleSize;
    private int mTitleSize;
    private int mVerticalGridTextPadding;
    private int mVerticalHeaderTextPadding;
    private int mVerticalTextPadding;
    
    public ListContent(final Context context, final Slice slice) {
        this(context, slice, null, 0, 0);
    }
    
    public ListContent(final Context mContext, final Slice mSlice, AttributeSet obtainStyledAttributes, int dimensionPixelSize, final int n) {
        this.mRowItems = new ArrayList<SliceItem>();
        this.mSlice = mSlice;
        if (this.mSlice == null) {
            return;
        }
        if ((this.mContext = mContext) != null) {
            this.mMinScrollHeight = mContext.getResources().getDimensionPixelSize(R.dimen.abc_slice_row_min_height);
            this.mLargeHeight = mContext.getResources().getDimensionPixelSize(R.dimen.abc_slice_large_height);
            final Resources.Theme theme = mContext.getTheme();
            if (theme != null) {
                obtainStyledAttributes = (AttributeSet)theme.obtainStyledAttributes(obtainStyledAttributes, R.styleable.SliceView, dimensionPixelSize, n);
                try {
                    this.mHeaderTitleSize = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_headerTitleSize, 0.0f);
                    this.mHeaderSubtitleSize = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_headerSubtitleSize, 0.0f);
                    this.mVerticalHeaderTextPadding = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_headerTextVerticalPadding, 0.0f);
                    this.mTitleSize = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_titleSize, 0.0f);
                    this.mSubtitleSize = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_subtitleSize, 0.0f);
                    this.mVerticalTextPadding = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_textVerticalPadding, 0.0f);
                    this.mGridTitleSize = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_gridTitleSize, 0.0f);
                    this.mGridSubtitleSize = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_gridSubtitleSize, 0.0f);
                    dimensionPixelSize = mContext.getResources().getDimensionPixelSize(R.dimen.abc_slice_grid_text_inner_padding);
                    this.mVerticalGridTextPadding = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_gridTextVerticalPadding, (float)dimensionPixelSize);
                    this.mGridTopPadding = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_gridTopPadding, 0.0f);
                    this.mGridBottomPadding = (int)((TypedArray)obtainStyledAttributes).getDimension(R.styleable.SliceView_gridTopPadding, 0.0f);
                }
                finally {
                    ((TypedArray)obtainStyledAttributes).recycle();
                }
            }
        }
        this.populate(mSlice);
    }
    
    private static SliceItem findHeaderItem(final Slice slice) {
        final SliceItem find = SliceQuery.find(slice, "slice", null, new String[] { "list_item", "shortcut", "actions", "keywords", "ttl", "last_updated", "horizontal" });
        if (find != null && isValidHeader(find)) {
            return find;
        }
        return null;
    }
    
    private int getHeight(final SliceItem sliceItem, final boolean b, int n, final int n2, final int n3) {
        final Context mContext = this.mContext;
        final boolean b2 = false;
        if (mContext == null || sliceItem == null) {
            return 0;
        }
        if (sliceItem.hasHint("horizontal")) {
            final GridContent gridContent = new GridContent(this.mContext, sliceItem);
            int mGridTopPadding;
            if (gridContent.isAllImages() && n == 0) {
                mGridTopPadding = this.mGridTopPadding;
            }
            else {
                mGridTopPadding = 0;
            }
            int mGridBottomPadding = b2 ? 1 : 0;
            if (gridContent.isAllImages()) {
                mGridBottomPadding = (b2 ? 1 : 0);
                if (n == n2 - 1) {
                    mGridBottomPadding = this.mGridBottomPadding;
                }
            }
            if (n3 == 1) {
                n = gridContent.getSmallHeight();
            }
            else {
                n = gridContent.getActualHeight();
            }
            return n + mGridTopPadding + mGridBottomPadding;
        }
        final RowContent rowContent = new RowContent(this.mContext, sliceItem, b);
        if (n3 == 1) {
            n = rowContent.getSmallHeight();
        }
        else {
            n = rowContent.getActualHeight();
        }
        return n;
    }
    
    public static int getRowType(final Context context, final SliceItem sliceItem, final boolean b, final List<SliceAction> list) {
        int n = 0;
        if (sliceItem == null) {
            return 0;
        }
        if (sliceItem.hasHint("horizontal")) {
            return 1;
        }
        final RowContent rowContent = new RowContent(context, sliceItem, b);
        final SliceItem primaryAction = rowContent.getPrimaryAction();
        SliceAction sliceAction = null;
        if (primaryAction != null) {
            sliceAction = new SliceActionImpl(primaryAction);
        }
        if (rowContent.getRange() != null) {
            int n2;
            if ("action".equals(rowContent.getRange().getFormat())) {
                n2 = 4;
            }
            else {
                n2 = 5;
            }
            return n2;
        }
        if (sliceAction != null && sliceAction.isToggle()) {
            return 3;
        }
        if (b && list != null) {
            for (int i = 0; i < list.size(); ++i) {
                if (list.get(i).isToggle()) {
                    return 3;
                }
            }
            return 0;
        }
        if (rowContent.getToggleItems().size() > 0) {
            n = 3;
        }
        return n;
    }
    
    private static SliceItem getSeeMoreItem(final Slice slice) {
        final SliceItem topLevelItem = SliceQuery.findTopLevelItem(slice, null, null, new String[] { "see_more" }, null);
        if (topLevelItem == null || !"slice".equals(topLevelItem.getFormat())) {
            return null;
        }
        final List<SliceItem> items = topLevelItem.getSlice().getItems();
        if (items.size() == 1 && "action".equals(items.get(0).getFormat())) {
            return items.get(0);
        }
        return topLevelItem;
    }
    
    public static boolean isValidHeader(final SliceItem sliceItem) {
        final boolean equals = "slice".equals(sliceItem.getFormat());
        boolean b = false;
        if (equals && !sliceItem.hasAnyHints("list_item", "actions", "keywords", "see_more")) {
            if (SliceQuery.find(sliceItem, "text", null, (String)null) != null) {
                b = true;
            }
            return b;
        }
        return false;
    }
    
    private boolean populate(final Slice slice) {
        if (slice == null) {
            return false;
        }
        SliceItem mLayoutDirItem = null;
        this.mColorItem = SliceQuery.findTopLevelItem(slice, "int", "color", null, null);
        this.mLayoutDirItem = SliceQuery.findTopLevelItem(slice, "int", "layout_direction", null, null);
        if (this.mLayoutDirItem != null) {
            if (SliceViewUtil.resolveLayoutDirection(this.mLayoutDirItem.getInt()) != -1) {
                mLayoutDirItem = this.mLayoutDirItem;
            }
            this.mLayoutDirItem = mLayoutDirItem;
        }
        this.mSliceActions = SliceMetadata.getSliceActions(slice);
        this.mHeaderItem = findHeaderItem(slice);
        if (this.mHeaderItem != null) {
            this.mRowItems.add(this.mHeaderItem);
        }
        this.mSeeMoreItem = getSeeMoreItem(slice);
        final List<SliceItem> items = slice.getItems();
        for (int i = 0; i < items.size(); ++i) {
            final SliceItem mHeaderItem = items.get(i);
            final String format = mHeaderItem.getFormat();
            if (!mHeaderItem.hasAnyHints("actions", "see_more", "keywords", "ttl", "last_updated") && ("action".equals(format) || "slice".equals(format))) {
                if (this.mHeaderItem == null && !mHeaderItem.hasHint("list_item")) {
                    this.mHeaderItem = mHeaderItem;
                    this.mRowItems.add(0, mHeaderItem);
                }
                else if (mHeaderItem.hasHint("list_item")) {
                    this.mRowItems.add(mHeaderItem);
                }
            }
        }
        if (this.mHeaderItem == null && this.mRowItems.size() >= 1) {
            this.mHeaderItem = this.mRowItems.get(0);
        }
        return this.isValid();
    }
    
    public SliceItem getColorItem() {
        return this.mColorItem;
    }
    
    public SliceItem getHeaderItem() {
        return this.mHeaderItem;
    }
    
    public int getHeaderTemplateType() {
        return getRowType(this.mContext, this.mHeaderItem, true, this.mSliceActions);
    }
    
    public ArrayList<SliceItem> getItemsForNonScrollingList(final int n) {
        final ArrayList<SliceItem> list = new ArrayList<SliceItem>();
        if (this.mRowItems != null && this.mRowItems.size() != 0) {
            int n2;
            if (this.hasHeader()) {
                n2 = 2;
            }
            else {
                n2 = 1;
            }
            int n3 = 0;
            if (this.mSeeMoreItem != null) {
                n3 = 0 + new RowContent(this.mContext, this.mSeeMoreItem, false).getActualHeight();
            }
            final int size = this.mRowItems.size();
            final int n4 = 0;
            int n5 = n3;
            for (int i = n4; i < size; ++i) {
                final int height = this.getHeight(this.mRowItems.get(i), i == 0, i, size, 2);
                if (n > 0 && n5 + height > n) {
                    break;
                }
                n5 += height;
                list.add(this.mRowItems.get(i));
            }
            if (this.mSeeMoreItem != null && list.size() >= n2 && list.size() != size) {
                list.add(this.mSeeMoreItem);
            }
            if (list.size() == 0) {
                list.add(this.mRowItems.get(0));
            }
            return list;
        }
        return list;
    }
    
    public int getLargeHeight(int min, final boolean b) {
        final int listHeight = this.getListHeight(this.mRowItems);
        int mLargeHeight;
        if (min != -1) {
            mLargeHeight = min;
        }
        else {
            mLargeHeight = this.mLargeHeight;
        }
        if (listHeight - mLargeHeight >= this.mMinScrollHeight) {
            min = mLargeHeight;
        }
        else if (min == -1) {
            min = listHeight;
        }
        else {
            min = Math.min(mLargeHeight, listHeight);
        }
        int listHeight2 = min;
        if (!b) {
            listHeight2 = this.getListHeight(this.getItemsForNonScrollingList(min));
        }
        return listHeight2;
    }
    
    public SliceItem getLayoutDirItem() {
        return this.mLayoutDirItem;
    }
    
    public int getListHeight(final List<SliceItem> list) {
        if (list == null || this.mContext == null) {
            return 0;
        }
        boolean b = false;
        SliceItem sliceItem = null;
        if (!list.isEmpty()) {
            sliceItem = list.get(0);
            b = (sliceItem.hasAnyHints("list_item", "horizontal") ^ true);
        }
        if (list.size() == 1 && !sliceItem.hasHint("horizontal")) {
            return this.getHeight(sliceItem, true, 0, 1, 2);
        }
        final int size = list.size();
        int n = 0;
        for (int i = 0; i < list.size(); ++i) {
            n += this.getHeight(list.get(i), i == 0 && b, i, size, 2);
        }
        return n;
    }
    
    public SliceItem getPrimaryAction() {
        if (this.mHeaderItem == null) {
            return null;
        }
        if (this.mHeaderItem.hasHint("horizontal")) {
            return new GridContent(this.mContext, this.mHeaderItem).getContentIntent();
        }
        return new RowContent(this.mContext, this.mHeaderItem, false).getPrimaryAction();
    }
    
    public ArrayList<SliceItem> getRowItems() {
        return this.mRowItems;
    }
    
    public Slice getSlice() {
        return this.mSlice;
    }
    
    public List<SliceAction> getSliceActions() {
        return this.mSliceActions;
    }
    
    public int getSmallHeight() {
        return this.getHeight(this.mHeaderItem, true, 0, 1, 1);
    }
    
    public boolean hasHeader() {
        return this.mHeaderItem != null && isValidHeader(this.mHeaderItem);
    }
    
    public boolean isValid() {
        return this.mSlice != null && this.mRowItems.size() > 0;
    }
}
