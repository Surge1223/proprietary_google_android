package androidx.slice.widget;

import android.content.res.TypedArray;
import androidx.slice.view.R;
import androidx.slice.SliceItem;
import androidx.slice.core.SliceAction;
import java.util.List;
import android.util.AttributeSet;
import android.content.Context;
import android.widget.FrameLayout;

public abstract class SliceChildView extends FrameLayout
{
    protected int mGridBottomPadding;
    protected int mGridSubtitleSize;
    protected int mGridTitleSize;
    protected int mGridTopPadding;
    protected int mHeaderSubtitleSize;
    protected int mHeaderTitleSize;
    protected long mLastUpdated;
    protected int mMode;
    protected SliceView.OnSliceActionListener mObserver;
    protected boolean mShowLastUpdated;
    protected int mSubtitleColor;
    protected int mSubtitleSize;
    protected int mTintColor;
    protected int mTitleColor;
    protected int mTitleSize;
    protected int mVerticalGridTextPadding;
    protected int mVerticalHeaderTextPadding;
    protected int mVerticalTextPadding;
    
    public SliceChildView(final Context context) {
        super(context);
        this.mTintColor = -1;
        this.mLastUpdated = -1L;
    }
    
    public SliceChildView(final Context context, final AttributeSet set) {
        this(context);
    }
    
    public int getActualHeight() {
        return 0;
    }
    
    public int getMode() {
        return this.mMode;
    }
    
    public int getSmallHeight() {
        return 0;
    }
    
    public abstract void resetView();
    
    public void setLastUpdated(final long mLastUpdated) {
        this.mLastUpdated = mLastUpdated;
    }
    
    public void setMode(final int mMode) {
        this.mMode = mMode;
    }
    
    public void setShowLastUpdated(final boolean mShowLastUpdated) {
        this.mShowLastUpdated = mShowLastUpdated;
    }
    
    public void setSliceActionListener(final SliceView.OnSliceActionListener mObserver) {
        this.mObserver = mObserver;
    }
    
    public void setSliceActions(final List<SliceAction> list) {
    }
    
    public void setSliceContent(final ListContent listContent) {
    }
    
    public void setSliceItem(final SliceItem sliceItem, final boolean b, final int n, final int n2, final SliceView.OnSliceActionListener onSliceActionListener) {
    }
    
    public void setStyle(final AttributeSet set, int mTintColor, final int n) {
        final TypedArray obtainStyledAttributes = this.getContext().getTheme().obtainStyledAttributes(set, R.styleable.SliceView, mTintColor, n);
        try {
            mTintColor = obtainStyledAttributes.getColor(R.styleable.SliceView_tintColor, -1);
            if (mTintColor == -1) {
                mTintColor = this.mTintColor;
            }
            this.mTintColor = mTintColor;
            this.mTitleColor = obtainStyledAttributes.getColor(R.styleable.SliceView_titleColor, 0);
            this.mSubtitleColor = obtainStyledAttributes.getColor(R.styleable.SliceView_subtitleColor, 0);
            this.mHeaderTitleSize = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_headerTitleSize, 0.0f);
            this.mHeaderSubtitleSize = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_headerSubtitleSize, 0.0f);
            this.mVerticalHeaderTextPadding = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_headerTextVerticalPadding, 0.0f);
            this.mTitleSize = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_titleSize, 0.0f);
            this.mSubtitleSize = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_subtitleSize, 0.0f);
            this.mVerticalTextPadding = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_textVerticalPadding, 0.0f);
            this.mGridTitleSize = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_gridTitleSize, 0.0f);
            this.mGridSubtitleSize = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_gridSubtitleSize, 0.0f);
            mTintColor = this.getContext().getResources().getDimensionPixelSize(R.dimen.abc_slice_grid_text_inner_padding);
            this.mVerticalGridTextPadding = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_gridTextVerticalPadding, (float)mTintColor);
            this.mGridTopPadding = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_gridTopPadding, 0.0f);
            this.mGridBottomPadding = (int)obtainStyledAttributes.getDimension(R.styleable.SliceView_gridTopPadding, 0.0f);
        }
        finally {
            obtainStyledAttributes.recycle();
        }
    }
    
    public void setTint(final int mTintColor) {
        this.mTintColor = mTintColor;
    }
}
