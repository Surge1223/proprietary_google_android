package androidx.slice.widget;

import android.text.format.DateUtils;
import java.util.Calendar;
import android.content.res.TypedArray;
import android.graphics.Xfermode;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.widget.FrameLayout$LayoutParams;
import android.view.View;
import android.widget.ImageView$ScaleType;
import android.widget.ImageView;
import android.view.ViewGroup;
import android.support.v4.graphics.drawable.IconCompat;
import android.content.Context;

public class SliceViewUtil
{
    public static void createCircledIcon(final Context context, final int n, final IconCompat iconCompat, final boolean b, final ViewGroup viewGroup) {
        final ImageView imageView = new ImageView(context);
        imageView.setImageDrawable(iconCompat.loadDrawable(context));
        ImageView$ScaleType scaleType;
        if (b) {
            scaleType = ImageView$ScaleType.CENTER_CROP;
        }
        else {
            scaleType = ImageView$ScaleType.CENTER_INSIDE;
        }
        imageView.setScaleType(scaleType);
        viewGroup.addView((View)imageView);
        final FrameLayout$LayoutParams frameLayout$LayoutParams = (FrameLayout$LayoutParams)imageView.getLayoutParams();
        if (b) {
            final Bitmap bitmap = Bitmap.createBitmap(n, n, Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(bitmap);
            imageView.layout(0, 0, n, n);
            imageView.draw(canvas);
            imageView.setImageBitmap(getCircularBitmap(bitmap));
        }
        else {
            imageView.setColorFilter(-1);
        }
        frameLayout$LayoutParams.width = n;
        frameLayout$LayoutParams.height = n;
        frameLayout$LayoutParams.gravity = 17;
    }
    
    public static IconCompat createIconFromDrawable(final Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return IconCompat.createWithBitmap(((BitmapDrawable)drawable).getBitmap());
        }
        final Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return IconCompat.createWithBitmap(bitmap);
    }
    
    public static Bitmap getCircularBitmap(final Bitmap bitmap) {
        final Bitmap bitmap2 = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap$Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap2);
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle((float)(bitmap.getWidth() / 2), (float)(bitmap.getHeight() / 2), (float)(bitmap.getWidth() / 2), paint);
        paint.setXfermode((Xfermode)new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return bitmap2;
    }
    
    public static int getColorAccent(final Context context) {
        return getColorAttr(context, 16843829);
    }
    
    public static int getColorAttr(final Context context, int color) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { color });
        color = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        return color;
    }
    
    public static Drawable getDrawable(final Context context, final int n) {
        final TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new int[] { n });
        final Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }
    
    public static CharSequence getRelativeTimeString(final long n) {
        return DateUtils.getRelativeTimeSpanString(n, Calendar.getInstance().getTimeInMillis(), 60000L, 262144);
    }
    
    public static int resolveLayoutDirection(final int n) {
        if (n != 2 && n != 3 && n != 1 && n != 0) {
            return -1;
        }
        return n;
    }
}
