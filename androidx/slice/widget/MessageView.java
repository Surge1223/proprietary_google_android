package androidx.slice.widget;

import java.util.Iterator;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Bitmap$Config;
import android.util.TypedValue;
import androidx.slice.core.SliceQuery;
import androidx.slice.SliceItem;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageView extends SliceChildView
{
    private TextView mDetails;
    private ImageView mIcon;
    private int mRowIndex;
    
    public MessageView(final Context context) {
        super(context);
    }
    
    @Override
    public int getMode() {
        return 2;
    }
    
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mDetails = (TextView)this.findViewById(16908304);
        this.mIcon = (ImageView)this.findViewById(16908294);
    }
    
    @Override
    public void resetView() {
    }
    
    @Override
    public void setSliceItem(SliceItem sliceItem, final boolean b, int mRowIndex, final int n, final SliceView.OnSliceActionListener sliceActionListener) {
        this.setSliceActionListener(sliceActionListener);
        this.mRowIndex = mRowIndex;
        final SliceItem subtype = SliceQuery.findSubtype(sliceItem, "image", "source");
        if (subtype != null) {
            mRowIndex = (int)TypedValue.applyDimension(1, 24.0f, this.getContext().getResources().getDisplayMetrics());
            final Bitmap bitmap = Bitmap.createBitmap(mRowIndex, mRowIndex, Bitmap$Config.ARGB_8888);
            final Canvas canvas = new Canvas(bitmap);
            final Drawable loadDrawable = subtype.getIcon().loadDrawable(this.getContext());
            loadDrawable.setBounds(0, 0, mRowIndex, mRowIndex);
            loadDrawable.draw(canvas);
            this.mIcon.setImageBitmap(SliceViewUtil.getCircularBitmap(bitmap));
        }
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        final Iterator<SliceItem> iterator = SliceQuery.findAll(sliceItem, "text").iterator();
        while (iterator.hasNext()) {
            sliceItem = iterator.next();
            if (spannableStringBuilder.length() != 0) {
                spannableStringBuilder.append('\n');
            }
            spannableStringBuilder.append(sliceItem.getText());
        }
        this.mDetails.setText((CharSequence)spannableStringBuilder.toString());
    }
}
