package androidx.slice.widget;

import androidx.slice.core.SliceActionImpl;
import androidx.slice.Slice;
import android.util.Log;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.List;
import androidx.slice.core.SliceQuery;
import androidx.slice.view.R;
import android.content.Context;
import androidx.slice.core.SliceAction;
import java.util.ArrayList;
import androidx.slice.SliceItem;

public class RowContent
{
    private SliceItem mContentDescr;
    private ArrayList<SliceItem> mEndItems;
    private boolean mEndItemsContainAction;
    private boolean mIsHeader;
    private SliceItem mLayoutDirItem;
    private int mLineCount;
    private int mMaxHeight;
    private int mMinHeight;
    private SliceItem mPrimaryAction;
    private SliceItem mRange;
    private int mRangeHeight;
    private SliceItem mRowSlice;
    private SliceItem mStartItem;
    private SliceItem mSubtitleItem;
    private SliceItem mSummaryItem;
    private SliceItem mTitleItem;
    private ArrayList<SliceAction> mToggleItems;
    
    public RowContent(final Context context, final SliceItem sliceItem, final boolean b) {
        this.mEndItems = new ArrayList<SliceItem>();
        this.mToggleItems = new ArrayList<SliceAction>();
        this.mLineCount = 0;
        this.populate(sliceItem, b);
        if (context != null) {
            this.mMaxHeight = context.getResources().getDimensionPixelSize(R.dimen.abc_slice_row_max_height);
            this.mMinHeight = context.getResources().getDimensionPixelSize(R.dimen.abc_slice_row_min_height);
            this.mRangeHeight = context.getResources().getDimensionPixelSize(R.dimen.abc_slice_row_range_height);
        }
    }
    
    private void determineStartAndPrimaryAction(final SliceItem mPrimaryAction) {
        final List<SliceItem> all = SliceQuery.findAll(mPrimaryAction, null, "title", null);
        if (all.size() > 0) {
            final String format = all.get(0).getFormat();
            if (("action".equals(format) && SliceQuery.find(all.get(0), "image") != null) || "slice".equals(format) || "long".equals(format) || "image".equals(format)) {
                this.mStartItem = all.get(0);
            }
        }
        final List<SliceItem> all2 = SliceQuery.findAll(mPrimaryAction, "slice", new String[] { "shortcut", "title" }, null);
        if (all2.isEmpty() && "action".equals(mPrimaryAction.getFormat()) && mPrimaryAction.getSlice().getItems().size() == 1) {
            this.mPrimaryAction = mPrimaryAction;
        }
        else if (this.mStartItem != null && all2.size() > 1 && all2.get(0) == this.mStartItem) {
            this.mPrimaryAction = all2.get(1);
        }
        else if (all2.size() > 0) {
            this.mPrimaryAction = all2.get(0);
        }
    }
    
    private static ArrayList<SliceItem> filterInvalidItems(final SliceItem sliceItem) {
        final ArrayList<SliceItem> list = new ArrayList<SliceItem>();
        for (final SliceItem sliceItem2 : sliceItem.getSlice().getItems()) {
            if (isValidRowContent(sliceItem, sliceItem2)) {
                list.add(sliceItem2);
            }
        }
        return list;
    }
    
    private static boolean hasText(final SliceItem sliceItem) {
        return sliceItem != null && (sliceItem.hasHint("partial") || !TextUtils.isEmpty(sliceItem.getText()));
    }
    
    private static boolean isValidRow(final SliceItem sliceItem) {
        if (sliceItem == null) {
            return false;
        }
        if ("slice".equals(sliceItem.getFormat()) || "action".equals(sliceItem.getFormat())) {
            final List<SliceItem> items = sliceItem.getSlice().getItems();
            if (sliceItem.hasHint("see_more") && items.isEmpty()) {
                return true;
            }
            for (int i = 0; i < items.size(); ++i) {
                if (isValidRowContent(sliceItem, items.get(i))) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private static boolean isValidRowContent(final SliceItem sliceItem, final SliceItem sliceItem2) {
        final boolean hasAnyHints = sliceItem2.hasAnyHints("keywords", "ttl", "last_updated", "horizontal");
        boolean b = false;
        if (!hasAnyHints && !"content_description".equals(sliceItem2.getSubType())) {
            final String format = sliceItem2.getFormat();
            if ("image".equals(format) || "text".equals(format) || "long".equals(format) || "action".equals(format) || "input".equals(format) || "slice".equals(format) || ("int".equals(format) && "range".equals(sliceItem.getSubType()))) {
                b = true;
            }
            return b;
        }
        return false;
    }
    
    private boolean populate(SliceItem mLayoutDirItem, final boolean mIsHeader) {
        this.mIsHeader = mIsHeader;
        this.mRowSlice = mLayoutDirItem;
        if (!isValidRow(mLayoutDirItem)) {
            Log.w("RowContent", "Provided SliceItem is invalid for RowContent");
            return false;
        }
        this.determineStartAndPrimaryAction(mLayoutDirItem);
        this.mContentDescr = SliceQuery.findSubtype(mLayoutDirItem, "text", "content_description");
        ArrayList<SliceItem> list2;
        final ArrayList<SliceItem> list = list2 = filterInvalidItems(mLayoutDirItem);
        SliceItem mRange = mLayoutDirItem;
        Label_0178: {
            if (list.size() == 1) {
                if (!"action".equals(list.get(0).getFormat())) {
                    list2 = list;
                    mRange = mLayoutDirItem;
                    if (!"slice".equals(list.get(0).getFormat())) {
                        break Label_0178;
                    }
                }
                list2 = list;
                mRange = mLayoutDirItem;
                if (!list.get(0).hasAnyHints("shortcut", "title")) {
                    list2 = list;
                    mRange = mLayoutDirItem;
                    if (isValidRow(list.get(0))) {
                        mRange = list.get(0);
                        list2 = filterInvalidItems(mRange);
                    }
                }
            }
        }
        final Slice slice = mRange.getSlice();
        mLayoutDirItem = null;
        this.mLayoutDirItem = SliceQuery.findTopLevelItem(slice, "int", "layout_direction", null, null);
        if (this.mLayoutDirItem != null) {
            if (SliceViewUtil.resolveLayoutDirection(this.mLayoutDirItem.getInt()) != -1) {
                mLayoutDirItem = this.mLayoutDirItem;
            }
            this.mLayoutDirItem = mLayoutDirItem;
        }
        if ("range".equals(mRange.getSubType())) {
            this.mRange = mRange;
        }
        if (list2.size() > 0) {
            if (this.mStartItem != null) {
                list2.remove(this.mStartItem);
            }
            if (this.mPrimaryAction != null) {
                list2.remove(this.mPrimaryAction);
            }
            final ArrayList<SliceItem> list3 = new ArrayList<SliceItem>();
            for (int i = 0; i < list2.size(); ++i) {
                final SliceItem mSummaryItem = list2.get(i);
                if ("text".equals(mSummaryItem.getFormat())) {
                    if ((this.mTitleItem == null || !this.mTitleItem.hasHint("title")) && mSummaryItem.hasHint("title") && !mSummaryItem.hasHint("summary")) {
                        this.mTitleItem = mSummaryItem;
                    }
                    else if (this.mSubtitleItem == null && !mSummaryItem.hasHint("summary")) {
                        this.mSubtitleItem = mSummaryItem;
                    }
                    else if (this.mSummaryItem == null && mSummaryItem.hasHint("summary")) {
                        this.mSummaryItem = mSummaryItem;
                    }
                }
                else {
                    list3.add(mSummaryItem);
                }
            }
            if (hasText(this.mTitleItem)) {
                ++this.mLineCount;
            }
            if (hasText(this.mSubtitleItem)) {
                ++this.mLineCount;
            }
            int n;
            if (this.mStartItem != null && "long".equals(this.mStartItem.getFormat())) {
                n = 1;
            }
            else {
                n = 0;
            }
            int n2;
            for (int j = 0; j < list3.size(); ++j, n = n2) {
                final SliceItem sliceItem = list3.get(j);
                final boolean b = SliceQuery.find(sliceItem, "action") != null;
                if ("long".equals(sliceItem.getFormat())) {
                    if ((n2 = n) == 0) {
                        n2 = 1;
                        this.mEndItems.add(sliceItem);
                    }
                }
                else {
                    this.processContent(sliceItem, b);
                    n2 = n;
                }
            }
        }
        return this.isValid();
    }
    
    private void processContent(final SliceItem sliceItem, final boolean b) {
        if (b) {
            final SliceActionImpl sliceActionImpl = new SliceActionImpl(sliceItem);
            if (sliceActionImpl.isToggle()) {
                this.mToggleItems.add(sliceActionImpl);
            }
        }
        this.mEndItems.add(sliceItem);
        this.mEndItemsContainAction |= b;
    }
    
    public int getActualHeight() {
        if (!this.isValid()) {
            return 0;
        }
        int n;
        if (this.getLineCount() <= 1 && !this.mIsHeader) {
            n = this.mMinHeight;
        }
        else {
            n = this.mMaxHeight;
        }
        int n2 = n;
        if (this.getRange() != null) {
            if (this.getLineCount() > 0) {
                n2 = n + this.mRangeHeight;
            }
            else {
                int n3;
                if (this.mIsHeader) {
                    n3 = this.mMaxHeight;
                }
                else {
                    n3 = this.mRangeHeight;
                }
                n2 = n3;
            }
        }
        return n2;
    }
    
    public CharSequence getContentDescription() {
        CharSequence text;
        if (this.mContentDescr != null) {
            text = this.mContentDescr.getText();
        }
        else {
            text = null;
        }
        return text;
    }
    
    public ArrayList<SliceItem> getEndItems() {
        return this.mEndItems;
    }
    
    public SliceItem getInputRangeThumb() {
        if (this.mRange != null) {
            final List<SliceItem> items = this.mRange.getSlice().getItems();
            for (int i = 0; i < items.size(); ++i) {
                if ("image".equals(items.get(i).getFormat())) {
                    return items.get(i);
                }
            }
        }
        return null;
    }
    
    public SliceItem getLayoutDirItem() {
        return this.mLayoutDirItem;
    }
    
    public int getLineCount() {
        return this.mLineCount;
    }
    
    public SliceItem getPrimaryAction() {
        return this.mPrimaryAction;
    }
    
    public SliceItem getRange() {
        return this.mRange;
    }
    
    public SliceItem getSlice() {
        return this.mRowSlice;
    }
    
    public int getSmallHeight() {
        int n;
        if (this.getRange() != null) {
            n = this.getActualHeight();
        }
        else {
            n = this.mMaxHeight;
        }
        return n;
    }
    
    public SliceItem getStartItem() {
        SliceItem mStartItem;
        if (this.mIsHeader) {
            mStartItem = null;
        }
        else {
            mStartItem = this.mStartItem;
        }
        return mStartItem;
    }
    
    public SliceItem getSubtitleItem() {
        return this.mSubtitleItem;
    }
    
    public SliceItem getSummaryItem() {
        SliceItem sliceItem;
        if (this.mSummaryItem == null) {
            sliceItem = this.mSubtitleItem;
        }
        else {
            sliceItem = this.mSummaryItem;
        }
        return sliceItem;
    }
    
    public SliceItem getTitleItem() {
        return this.mTitleItem;
    }
    
    public ArrayList<SliceAction> getToggleItems() {
        return this.mToggleItems;
    }
    
    public boolean isDefaultSeeMore() {
        return "action".equals(this.mRowSlice.getFormat()) && this.mRowSlice.getSlice().hasHint("see_more") && this.mRowSlice.getSlice().getItems().isEmpty();
    }
    
    public boolean isValid() {
        return this.mStartItem != null || this.mPrimaryAction != null || this.mTitleItem != null || this.mSubtitleItem != null || this.mEndItems.size() > 0 || this.mRange != null || this.isDefaultSeeMore();
    }
}
