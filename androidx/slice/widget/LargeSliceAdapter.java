package androidx.slice.widget;

import android.view.MotionEvent;
import android.view.View.OnTouchListener;
import android.view.View.OnClickListener;
import androidx.slice.core.SliceQuery;
import android.support.v4.util.ArrayMap;
import java.util.Iterator;
import androidx.slice.SliceItem;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup;
import androidx.slice.view.R;
import android.view.LayoutInflater;
import android.view.View;
import java.util.ArrayList;
import androidx.slice.core.SliceAction;
import java.util.List;
import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.RecyclerView;

public class LargeSliceAdapter extends Adapter<SliceViewHolder>
{
    private AttributeSet mAttrs;
    private int mColor;
    private final Context mContext;
    private int mDefStyleAttr;
    private int mDefStyleRes;
    private final IdGenerator mIdGen;
    private long mLastUpdated;
    private SliceView mParent;
    private boolean mShowLastUpdated;
    private List<SliceAction> mSliceActions;
    private SliceView.OnSliceActionListener mSliceObserver;
    private List<SliceWrapper> mSlices;
    private LargeTemplateView mTemplateView;
    
    public LargeSliceAdapter(final Context mContext) {
        this.mIdGen = new IdGenerator();
        this.mSlices = new ArrayList<SliceWrapper>();
        this.mContext = mContext;
        ((RecyclerView.Adapter)this).setHasStableIds(true);
    }
    
    private View inflateForType(final int n) {
        Object o = new RowView(this.mContext);
        switch (n) {
            case 5: {
                o = LayoutInflater.from(this.mContext).inflate(R.layout.abc_slice_message_local, (ViewGroup)null);
                break;
            }
            case 4: {
                o = LayoutInflater.from(this.mContext).inflate(R.layout.abc_slice_message, (ViewGroup)null);
                break;
            }
            case 3: {
                o = LayoutInflater.from(this.mContext).inflate(R.layout.abc_slice_grid, (ViewGroup)null);
                break;
            }
        }
        return (View)o;
    }
    
    private void notifyHeaderChanged() {
        if (this.getItemCount() > 0) {
            ((RecyclerView.Adapter)this).notifyItemChanged(0);
        }
    }
    
    @Override
    public int getItemCount() {
        return this.mSlices.size();
    }
    
    @Override
    public long getItemId(final int n) {
        return this.mSlices.get(n).mId;
    }
    
    @Override
    public int getItemViewType(final int n) {
        return this.mSlices.get(n).mType;
    }
    
    public void onBindViewHolder(final SliceViewHolder sliceViewHolder, final int n) {
        sliceViewHolder.bind(this.mSlices.get(n).mItem, n);
    }
    
    public SliceViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int n) {
        final View inflateForType = this.inflateForType(n);
        inflateForType.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        return new SliceViewHolder(inflateForType);
    }
    
    public void setLastUpdated(final long mLastUpdated) {
        this.mLastUpdated = mLastUpdated;
        this.notifyHeaderChanged();
    }
    
    public void setParents(final SliceView mParent, final LargeTemplateView mTemplateView) {
        this.mParent = mParent;
        this.mTemplateView = mTemplateView;
    }
    
    public void setShowLastUpdated(final boolean mShowLastUpdated) {
        this.mShowLastUpdated = mShowLastUpdated;
        this.notifyHeaderChanged();
    }
    
    public void setSliceActions(final List<SliceAction> mSliceActions) {
        this.mSliceActions = mSliceActions;
        this.notifyHeaderChanged();
    }
    
    public void setSliceItems(final List<SliceItem> list, final int mColor, final int n) {
        if (list == null) {
            this.mSlices.clear();
        }
        else {
            this.mIdGen.resetUsage();
            this.mSlices = new ArrayList<SliceWrapper>(list.size());
            final Iterator<SliceItem> iterator = list.iterator();
            while (iterator.hasNext()) {
                this.mSlices.add(new SliceWrapper(iterator.next(), this.mIdGen, n));
            }
        }
        this.mColor = mColor;
        ((RecyclerView.Adapter)this).notifyDataSetChanged();
    }
    
    public void setSliceObserver(final SliceView.OnSliceActionListener mSliceObserver) {
        this.mSliceObserver = mSliceObserver;
    }
    
    public void setStyle(final AttributeSet mAttrs, final int mDefStyleAttr, final int mDefStyleRes) {
        this.mAttrs = mAttrs;
        this.mDefStyleAttr = mDefStyleAttr;
        this.mDefStyleRes = mDefStyleRes;
        ((RecyclerView.Adapter)this).notifyDataSetChanged();
    }
    
    private static class IdGenerator
    {
        private final ArrayMap<String, Long> mCurrentIds;
        private long mNextLong;
        private final ArrayMap<String, Integer> mUsedIds;
        
        private IdGenerator() {
            this.mNextLong = 0L;
            this.mCurrentIds = new ArrayMap<String, Long>();
            this.mUsedIds = new ArrayMap<String, Integer>();
        }
        
        private String genString(final SliceItem sliceItem) {
            if (!"slice".equals(sliceItem.getFormat()) && !"action".equals(sliceItem.getFormat())) {
                return sliceItem.toString();
            }
            return String.valueOf(sliceItem.getSlice().getItems().size());
        }
        
        public long getId(final SliceItem sliceItem, int intValue) {
            String s2;
            final String s = s2 = this.genString(sliceItem);
            if (SliceQuery.find(sliceItem, null, "summary", null) != null) {
                final StringBuilder sb = new StringBuilder();
                sb.append(s);
                sb.append(intValue);
                s2 = sb.toString();
            }
            if (!this.mCurrentIds.containsKey(s2)) {
                final ArrayMap<String, Long> mCurrentIds = this.mCurrentIds;
                final long mNextLong = this.mNextLong;
                this.mNextLong = 1L + mNextLong;
                mCurrentIds.put(s2, mNextLong);
            }
            final long longValue = this.mCurrentIds.get(s2);
            final Integer n = this.mUsedIds.get(s2);
            if (n != null) {
                intValue = n;
            }
            else {
                intValue = 0;
            }
            this.mUsedIds.put(s2, intValue + 1);
            return intValue * 10000 + longValue;
        }
        
        public void resetUsage() {
            this.mUsedIds.clear();
        }
    }
    
    public class SliceViewHolder extends ViewHolder implements View.OnClickListener, View.OnTouchListener
    {
        public final SliceChildView mSliceChildView;
        
        public SliceViewHolder(final View view) {
            super(view);
            SliceChildView mSliceChildView;
            if (view instanceof SliceChildView) {
                mSliceChildView = (SliceChildView)view;
            }
            else {
                mSliceChildView = null;
            }
            this.mSliceChildView = mSliceChildView;
        }
        
        void bind(final SliceItem sliceItem, final int n) {
            if (this.mSliceChildView != null && sliceItem != null) {
                this.mSliceChildView.setOnClickListener((View.OnClickListener)this);
                this.mSliceChildView.setOnTouchListener((View.OnTouchListener)this);
                final boolean b = n == 0;
                int mode;
                if (LargeSliceAdapter.this.mParent != null) {
                    mode = LargeSliceAdapter.this.mParent.getMode();
                }
                else {
                    mode = 2;
                }
                this.mSliceChildView.setMode(mode);
                this.mSliceChildView.setTint(LargeSliceAdapter.this.mColor);
                this.mSliceChildView.setStyle(LargeSliceAdapter.this.mAttrs, LargeSliceAdapter.this.mDefStyleAttr, LargeSliceAdapter.this.mDefStyleRes);
                this.mSliceChildView.setSliceItem(sliceItem, b, n, LargeSliceAdapter.this.getItemCount(), LargeSliceAdapter.this.mSliceObserver);
                final SliceChildView mSliceChildView = this.mSliceChildView;
                List<SliceAction> access$1000;
                if (b) {
                    access$1000 = LargeSliceAdapter.this.mSliceActions;
                }
                else {
                    access$1000 = null;
                }
                mSliceChildView.setSliceActions(access$1000);
                final SliceChildView mSliceChildView2 = this.mSliceChildView;
                long access$1001;
                if (b) {
                    access$1001 = LargeSliceAdapter.this.mLastUpdated;
                }
                else {
                    access$1001 = -1L;
                }
                mSliceChildView2.setLastUpdated(access$1001);
                this.mSliceChildView.setShowLastUpdated(b && LargeSliceAdapter.this.mShowLastUpdated);
                if (this.mSliceChildView instanceof RowView) {
                    ((RowView)this.mSliceChildView).setSingleItem(LargeSliceAdapter.this.getItemCount() == 1);
                }
                this.mSliceChildView.setTag((Object)new int[] { ListContent.getRowType(LargeSliceAdapter.this.mContext, sliceItem, b, LargeSliceAdapter.this.mSliceActions), n });
            }
        }
        
        public void onClick(final View view) {
            if (LargeSliceAdapter.this.mParent != null) {
                LargeSliceAdapter.this.mParent.setClickInfo((int[])view.getTag());
                LargeSliceAdapter.this.mParent.performClick();
            }
        }
        
        public boolean onTouch(final View view, final MotionEvent motionEvent) {
            if (LargeSliceAdapter.this.mTemplateView != null) {
                LargeSliceAdapter.this.mTemplateView.onForegroundActivated(motionEvent);
            }
            return false;
        }
    }
    
    protected static class SliceWrapper
    {
        private final long mId;
        private final SliceItem mItem;
        private final int mType;
        
        public SliceWrapper(final SliceItem mItem, final IdGenerator idGenerator, final int n) {
            this.mItem = mItem;
            this.mType = getFormat(mItem);
            this.mId = idGenerator.getId(mItem, n);
        }
        
        public static int getFormat(final SliceItem sliceItem) {
            if ("message".equals(sliceItem.getSubType())) {
                if (SliceQuery.findSubtype(sliceItem, null, "source") != null) {
                    return 4;
                }
                return 5;
            }
            else {
                if (sliceItem.hasHint("horizontal")) {
                    return 3;
                }
                if (!sliceItem.hasHint("list_item")) {
                    return 2;
                }
                return 1;
            }
        }
    }
}
