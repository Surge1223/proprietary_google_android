package androidx.slice.widget;

import android.util.AttributeSet;
import androidx.slice.core.SliceAction;
import android.view.View$MeasureSpec;
import android.os.Build.VERSION;
import android.view.MotionEvent;
import java.util.List;
import java.util.Collection;
import java.util.Arrays;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout$LayoutParams;
import android.support.v7.widget.LinearLayoutManager;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import androidx.slice.SliceItem;
import java.util.ArrayList;

public class LargeTemplateView extends SliceChildView
{
    private final LargeSliceAdapter mAdapter;
    private ArrayList<SliceItem> mDisplayedItems;
    private int mDisplayedItemsHeight;
    private final View mForeground;
    private ListContent mListContent;
    private int[] mLoc;
    private SliceView mParent;
    private final RecyclerView mRecyclerView;
    private boolean mScrollingEnabled;
    
    public LargeTemplateView(final Context context) {
        super(context);
        this.mDisplayedItems = new ArrayList<SliceItem>();
        this.mDisplayedItemsHeight = 0;
        this.mLoc = new int[2];
        (this.mRecyclerView = new RecyclerView(this.getContext())).setLayoutManager((RecyclerView.LayoutManager)new LinearLayoutManager(this.getContext()));
        this.mAdapter = new LargeSliceAdapter(context);
        this.mRecyclerView.setAdapter((RecyclerView.Adapter)this.mAdapter);
        this.addView((View)this.mRecyclerView);
        (this.mForeground = new View(this.getContext())).setBackground(SliceViewUtil.getDrawable(this.getContext(), 16843534));
        this.addView(this.mForeground);
        final FrameLayout$LayoutParams layoutParams = (FrameLayout$LayoutParams)this.mForeground.getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = -1;
        this.mForeground.setLayoutParams((ViewGroup.LayoutParams)layoutParams);
    }
    
    private void updateDisplayedItems(final int n) {
        if (this.mListContent != null && this.mListContent.isValid()) {
            final int mode = this.getMode();
            if (mode == 1) {
                this.mDisplayedItems = new ArrayList<SliceItem>(Arrays.asList(this.mListContent.getRowItems().get(0)));
            }
            else if (!this.mScrollingEnabled && n != 0) {
                this.mDisplayedItems = this.mListContent.getItemsForNonScrollingList(n);
            }
            else {
                this.mDisplayedItems = this.mListContent.getRowItems();
            }
            this.mDisplayedItemsHeight = this.mListContent.getListHeight(this.mDisplayedItems);
            this.mAdapter.setSliceItems(this.mDisplayedItems, this.mTintColor, mode);
            this.updateOverscroll();
            return;
        }
        this.resetView();
    }
    
    private void updateOverscroll() {
        final int mDisplayedItemsHeight = this.mDisplayedItemsHeight;
        final int measuredHeight = this.getMeasuredHeight();
        final boolean b = true;
        final boolean b2 = mDisplayedItemsHeight > measuredHeight;
        final RecyclerView mRecyclerView = this.mRecyclerView;
        int overScrollMode;
        if (this.mScrollingEnabled && b2) {
            overScrollMode = (b ? 1 : 0);
        }
        else {
            overScrollMode = 2;
        }
        mRecyclerView.setOverScrollMode(overScrollMode);
    }
    
    @Override
    public int getActualHeight() {
        return this.mDisplayedItemsHeight;
    }
    
    @Override
    public int getSmallHeight() {
        if (this.mListContent != null && this.mListContent.isValid()) {
            return this.mListContent.getSmallHeight();
        }
        return 0;
    }
    
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mParent = (SliceView)this.getParent();
        this.mAdapter.setParents(this.mParent, this);
    }
    
    public void onForegroundActivated(final MotionEvent motionEvent) {
        if (this.mParent != null && !this.mParent.isSliceViewClickable()) {
            this.mForeground.setPressed(false);
            return;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.mForeground.getLocationOnScreen(this.mLoc);
            this.mForeground.getBackground().setHotspot((float)(int)(motionEvent.getRawX() - this.mLoc[0]), (float)(int)(motionEvent.getRawY() - this.mLoc[1]));
        }
        final int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.mForeground.setPressed(true);
        }
        else if (actionMasked == 3 || actionMasked == 1 || actionMasked == 2) {
            this.mForeground.setPressed(false);
        }
    }
    
    protected void onMeasure(final int n, final int n2) {
        final int size = View$MeasureSpec.getSize(n2);
        if (!this.mScrollingEnabled && this.mDisplayedItems.size() > 0 && this.mDisplayedItemsHeight != size) {
            this.updateDisplayedItems(size);
        }
        super.onMeasure(n, n2);
    }
    
    @Override
    public void resetView() {
        this.mDisplayedItemsHeight = 0;
        this.mDisplayedItems.clear();
        this.mAdapter.setSliceItems(null, -1, this.getMode());
        this.mListContent = null;
    }
    
    @Override
    public void setLastUpdated(final long n) {
        super.setLastUpdated(n);
        this.mAdapter.setLastUpdated(n);
    }
    
    @Override
    public void setMode(final int mMode) {
        if (this.mMode != mMode) {
            this.mMode = mMode;
            if (this.mListContent != null && this.mListContent.isValid()) {
                this.updateDisplayedItems(this.mListContent.getLargeHeight(-1, this.mScrollingEnabled));
            }
        }
    }
    
    public void setScrollable(final boolean mScrollingEnabled) {
        if (this.mScrollingEnabled != mScrollingEnabled) {
            this.mScrollingEnabled = mScrollingEnabled;
            if (this.mListContent != null && this.mListContent.isValid()) {
                this.updateDisplayedItems(this.mListContent.getLargeHeight(-1, this.mScrollingEnabled));
            }
        }
    }
    
    @Override
    public void setShowLastUpdated(final boolean b) {
        super.setShowLastUpdated(b);
        this.mAdapter.setShowLastUpdated(b);
    }
    
    @Override
    public void setSliceActionListener(final SliceView.OnSliceActionListener mObserver) {
        this.mObserver = mObserver;
        if (this.mAdapter != null) {
            this.mAdapter.setSliceObserver(this.mObserver);
        }
    }
    
    @Override
    public void setSliceActions(final List<SliceAction> sliceActions) {
        this.mAdapter.setSliceActions(sliceActions);
    }
    
    @Override
    public void setSliceContent(final ListContent mListContent) {
        this.mListContent = mListContent;
        this.updateDisplayedItems(this.mListContent.getLargeHeight(-1, this.mScrollingEnabled));
    }
    
    @Override
    public void setStyle(final AttributeSet set, final int n, final int n2) {
        super.setStyle(set, n, n2);
        this.mAdapter.setStyle(set, n, n2);
    }
    
    @Override
    public void setTint(final int tint) {
        super.setTint(tint);
        this.updateDisplayedItems(this.getMeasuredHeight());
    }
}
