package androidx.slice.widget;

import android.content.res.TypedArray;
import androidx.slice.SliceMetadata;
import android.view.View$MeasureSpec;
import android.app.PendingIntent$CanceledException;
import android.util.Log;
import androidx.slice.core.SliceActionImpl;
import android.support.v4.os.BuildCompat;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewConfiguration;
import android.view.MotionEvent;
import androidx.slice.SliceItem;
import androidx.slice.core.SliceQuery;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import androidx.slice.view.R;
import android.content.Context;
import android.view.View$OnLongClickListener;
import android.os.Handler;
import android.app.slice.SliceMetrics;
import android.util.AttributeSet;
import androidx.slice.core.SliceAction;
import java.util.List;
import android.view.View.OnClickListener;
import androidx.slice.Slice;
import android.arch.lifecycle.Observer;
import android.view.ViewGroup;

public class SliceView extends ViewGroup implements Observer<Slice>, View.OnClickListener
{
    private ActionRow mActionRow;
    private int mActionRowHeight;
    private List<SliceAction> mActions;
    private AttributeSet mAttrs;
    int[] mClickInfo;
    private Slice mCurrentSlice;
    private boolean mCurrentSliceLoggedVisible;
    private SliceMetrics mCurrentSliceMetrics;
    private SliceChildView mCurrentView;
    private int mDefStyleAttr;
    private int mDefStyleRes;
    private int mDownX;
    private int mDownY;
    private Handler mHandler;
    private boolean mInLongpress;
    private boolean mIsScrollable;
    private int mLargeHeight;
    private ListContent mListContent;
    private View$OnLongClickListener mLongClickListener;
    Runnable mLongpressCheck;
    private int mMode;
    private View.OnClickListener mOnClickListener;
    private boolean mPressing;
    private int mShortcutSize;
    private boolean mShowActions;
    private boolean mShowLastUpdated;
    private OnSliceActionListener mSliceObserver;
    private int mThemeTintColor;
    private int mTouchSlopSquared;
    
    public SliceView(final Context context) {
        this(context, null);
    }
    
    public SliceView(final Context context, final AttributeSet set) {
        this(context, set, R.attr.sliceViewStyle);
    }
    
    public SliceView(final Context context, final AttributeSet set, final int n) {
        super(context, set, n);
        this.mMode = 2;
        this.mShowActions = false;
        this.mIsScrollable = true;
        this.mShowLastUpdated = true;
        this.mCurrentSliceLoggedVisible = false;
        this.mThemeTintColor = -1;
        this.mLongpressCheck = new Runnable() {
            @Override
            public void run() {
                if (SliceView.this.mPressing && SliceView.this.mLongClickListener != null) {
                    SliceView.this.mInLongpress = true;
                    SliceView.this.mLongClickListener.onLongClick((View)SliceView.this);
                    SliceView.this.performHapticFeedback(0);
                }
            }
        };
        this.init(context, set, n, R.style.Widget_SliceView);
    }
    
    public SliceView(final Context context, final AttributeSet set, final int n, final int n2) {
        super(context, set, n, n2);
        this.mMode = 2;
        this.mShowActions = false;
        this.mIsScrollable = true;
        this.mShowLastUpdated = true;
        this.mCurrentSliceLoggedVisible = false;
        this.mThemeTintColor = -1;
        this.mLongpressCheck = new Runnable() {
            @Override
            public void run() {
                if (SliceView.this.mPressing && SliceView.this.mLongClickListener != null) {
                    SliceView.this.mInLongpress = true;
                    SliceView.this.mLongClickListener.onLongClick((View)SliceView.this);
                    SliceView.this.performHapticFeedback(0);
                }
            }
        };
        this.init(context, set, n, n2);
    }
    
    private void applyConfigurations() {
        this.mCurrentView.setSliceActionListener(this.mSliceObserver);
        if (this.mCurrentView instanceof LargeTemplateView) {
            ((LargeTemplateView)this.mCurrentView).setScrollable(this.mIsScrollable);
        }
        this.mCurrentView.setStyle(this.mAttrs, this.mDefStyleAttr, this.mDefStyleRes);
        this.mCurrentView.setTint(this.getTintColor());
        if (this.mListContent != null && this.mListContent.getLayoutDirItem() != null) {
            this.mCurrentView.setLayoutDirection(this.mListContent.getLayoutDirItem().getInt());
        }
        else {
            this.mCurrentView.setLayoutDirection(2);
        }
    }
    
    private ViewGroup.LayoutParams getChildLp(final View view) {
        if (view instanceof ShortcutView) {
            return new ViewGroup.LayoutParams(this.mShortcutSize, this.mShortcutSize);
        }
        return new ViewGroup.LayoutParams(-1, -1);
    }
    
    private int getHeightForMode(int n) {
        if (this.mListContent == null || !this.mListContent.isValid()) {
            return 0;
        }
        final int mode = this.getMode();
        if (mode == 3) {
            return this.mShortcutSize;
        }
        if (mode == 2) {
            n = this.mListContent.getLargeHeight(n, this.mIsScrollable);
        }
        else {
            n = this.mListContent.getSmallHeight();
        }
        return n;
    }
    
    private int getTintColor() {
        if (this.mThemeTintColor != -1) {
            return this.mThemeTintColor;
        }
        final SliceItem subtype = SliceQuery.findSubtype(this.mCurrentSlice, "int", "color");
        int n;
        if (subtype != null) {
            n = subtype.getInt();
        }
        else {
            n = SliceViewUtil.getColorAccent(this.getContext());
        }
        return n;
    }
    
    private boolean handleTouchForLongpress(final MotionEvent motionEvent) {
        switch (motionEvent.getActionMasked()) {
            case 2: {
                final int n = (int)motionEvent.getRawX() - this.mDownX;
                final int n2 = (int)motionEvent.getRawY() - this.mDownY;
                if (n * n + n2 * n2 > this.mTouchSlopSquared) {
                    this.mPressing = false;
                    this.mHandler.removeCallbacks(this.mLongpressCheck);
                    break;
                }
                break;
            }
            case 1:
            case 3: {
                this.mPressing = false;
                this.mInLongpress = false;
                this.mHandler.removeCallbacks(this.mLongpressCheck);
                break;
            }
            case 0: {
                this.mHandler.removeCallbacks(this.mLongpressCheck);
                this.mDownX = (int)motionEvent.getRawX();
                this.mDownY = (int)motionEvent.getRawY();
                this.mPressing = true;
                this.mInLongpress = false;
                this.mHandler.postDelayed(this.mLongpressCheck, (long)ViewConfiguration.getLongPressTimeout());
                break;
            }
        }
        return this.mInLongpress;
    }
    
    private void init(final Context context, AttributeSet obtainStyledAttributes, int scaledTouchSlop, final int mDefStyleRes) {
        this.mAttrs = obtainStyledAttributes;
        this.mDefStyleAttr = scaledTouchSlop;
        this.mDefStyleRes = mDefStyleRes;
        obtainStyledAttributes = (AttributeSet)context.getTheme().obtainStyledAttributes(obtainStyledAttributes, R.styleable.SliceView, scaledTouchSlop, mDefStyleRes);
        try {
            this.mThemeTintColor = ((TypedArray)obtainStyledAttributes).getColor(R.styleable.SliceView_tintColor, -1);
            ((TypedArray)obtainStyledAttributes).recycle();
            this.mShortcutSize = this.getContext().getResources().getDimensionPixelSize(R.dimen.abc_slice_shortcut_size);
            this.mLargeHeight = this.getResources().getDimensionPixelSize(R.dimen.abc_slice_large_height);
            this.mActionRowHeight = this.getResources().getDimensionPixelSize(R.dimen.abc_slice_action_row_height);
            (this.mCurrentView = new LargeTemplateView(this.getContext())).setMode(this.getMode());
            this.addView((View)this.mCurrentView, this.getChildLp((View)this.mCurrentView));
            this.applyConfigurations();
            (this.mActionRow = new ActionRow(this.getContext(), true)).setBackground((Drawable)new ColorDrawable(-1118482));
            this.addView((View)this.mActionRow, this.getChildLp((View)this.mActionRow));
            this.updateActions();
            scaledTouchSlop = ViewConfiguration.get(this.getContext()).getScaledTouchSlop();
            this.mTouchSlopSquared = scaledTouchSlop * scaledTouchSlop;
            this.mHandler = new Handler();
            super.setOnClickListener((View.OnClickListener)this);
        }
        finally {
            ((TypedArray)obtainStyledAttributes).recycle();
        }
    }
    
    private void initSliceMetrics(final Slice slice) {
        if (BuildCompat.isAtLeastP()) {
            if (slice != null && slice.getUri() != null) {
                if (this.mCurrentSlice == null || !this.mCurrentSlice.getUri().equals((Object)slice.getUri())) {
                    this.logSliceMetricsVisibilityChange(false);
                    this.mCurrentSliceMetrics = new SliceMetrics(this.getContext(), slice.getUri());
                }
            }
            else {
                this.logSliceMetricsVisibilityChange(false);
                this.mCurrentSliceMetrics = null;
            }
        }
    }
    
    private void logSliceMetricsOnTouch(final SliceItem sliceItem, final EventInfo eventInfo) {
        if (BuildCompat.isAtLeastP() && this.mCurrentSliceMetrics != null && sliceItem.getSlice() != null && sliceItem.getSlice().getUri() != null) {
            this.mCurrentSliceMetrics.logTouch(eventInfo.actionType, this.mListContent.getPrimaryAction().getSlice().getUri());
        }
    }
    
    private void logSliceMetricsVisibilityChange(final boolean b) {
        if (BuildCompat.isAtLeastP() && this.mCurrentSliceMetrics != null) {
            if (b && !this.mCurrentSliceLoggedVisible) {
                this.mCurrentSliceMetrics.logVisible();
                this.mCurrentSliceLoggedVisible = true;
            }
            if (!b && this.mCurrentSliceLoggedVisible) {
                this.mCurrentSliceMetrics.logHidden();
                this.mCurrentSliceLoggedVisible = false;
            }
        }
    }
    
    public static String modeToString(final int n) {
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("unknown mode: ");
                sb.append(n);
                return sb.toString();
            }
            case 3: {
                return "MODE SHORTCUT";
            }
            case 2: {
                return "MODE LARGE";
            }
            case 1: {
                return "MODE SMALL";
            }
        }
    }
    
    private void updateActions() {
        if (this.mActions != null && !this.mActions.isEmpty()) {
            if (this.mShowActions && this.mMode != 3 && this.mActions.size() >= 2) {
                this.mActionRow.setActions(this.mActions, this.getTintColor());
                this.mActionRow.setVisibility(0);
                this.mCurrentView.setSliceActions(null);
            }
            else {
                this.mCurrentView.setSliceActions(this.mActions);
                this.mActionRow.setVisibility(8);
            }
            return;
        }
        this.mActionRow.setVisibility(8);
        this.mCurrentView.setSliceActions(null);
    }
    
    private void updateViewConfig() {
        final boolean b = false;
        final int mode = this.getMode();
        final boolean b2 = this.mCurrentView instanceof ShortcutView;
        boolean b3;
        if (mode == 3 && !b2) {
            this.removeView((View)this.mCurrentView);
            this.addView((View)(this.mCurrentView = new ShortcutView(this.getContext())), this.getChildLp((View)this.mCurrentView));
            b3 = true;
        }
        else {
            b3 = b;
            if (mode != 3) {
                b3 = b;
                if (b2) {
                    this.removeView((View)this.mCurrentView);
                    this.addView((View)(this.mCurrentView = new LargeTemplateView(this.getContext())), this.getChildLp((View)this.mCurrentView));
                    b3 = true;
                }
            }
        }
        this.mCurrentView.setMode(mode);
        if (b3) {
            this.applyConfigurations();
            if (this.mListContent != null && this.mListContent.isValid()) {
                this.mCurrentView.setSliceContent(this.mListContent);
            }
        }
        this.updateActions();
    }
    
    public int getMode() {
        return this.mMode;
    }
    
    public Slice getSlice() {
        return this.mCurrentSlice;
    }
    
    public List<SliceAction> getSliceActions() {
        return this.mActions;
    }
    
    public boolean isSliceViewClickable() {
        return this.mOnClickListener != null || (this.mListContent != null && this.mListContent.getPrimaryAction() != null);
    }
    
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.isShown()) {
            this.logSliceMetricsVisibilityChange(true);
        }
    }
    
    public void onChanged(final Slice slice) {
        this.setSlice(slice);
    }
    
    public void onClick(final View view) {
        if (this.mListContent != null && this.mListContent.getPrimaryAction() != null) {
            try {
                new SliceActionImpl(this.mListContent.getPrimaryAction()).getAction().send();
                if (this.mSliceObserver != null && this.mClickInfo != null && this.mClickInfo.length > 1) {
                    final EventInfo eventInfo = new EventInfo(this.getMode(), 3, this.mClickInfo[0], this.mClickInfo[1]);
                    final SliceItem primaryAction = this.mListContent.getPrimaryAction();
                    this.mSliceObserver.onSliceAction(eventInfo, primaryAction);
                    this.logSliceMetricsOnTouch(primaryAction, eventInfo);
                }
            }
            catch (PendingIntent$CanceledException ex) {
                Log.e("SliceView", "PendingIntent for slice cannot be sent", (Throwable)ex);
            }
        }
        else if (this.mOnClickListener != null) {
            this.mOnClickListener.onClick((View)this);
        }
    }
    
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.logSliceMetricsVisibilityChange(false);
    }
    
    public boolean onInterceptTouchEvent(final MotionEvent motionEvent) {
        final boolean onInterceptTouchEvent = super.onInterceptTouchEvent(motionEvent);
        if (this.mLongClickListener != null) {
            return this.handleTouchForLongpress(motionEvent);
        }
        return onInterceptTouchEvent;
    }
    
    protected void onLayout(final boolean b, int paddingLeft, int paddingTop, final int n, final int n2) {
        final SliceChildView mCurrentView = this.mCurrentView;
        paddingLeft = this.getPaddingLeft();
        paddingTop = this.getPaddingTop();
        ((View)mCurrentView).layout(paddingLeft, paddingTop, ((View)mCurrentView).getMeasuredWidth() + paddingLeft, ((View)mCurrentView).getMeasuredHeight() + paddingTop);
        if (this.mActionRow.getVisibility() != 8) {
            this.mActionRow.layout(paddingLeft, ((View)mCurrentView).getMeasuredHeight() + paddingTop, this.mActionRow.getMeasuredWidth() + paddingLeft, ((View)mCurrentView).getMeasuredHeight() + paddingTop + this.mActionRow.getMeasuredHeight());
        }
    }
    
    protected void onMeasure(int n, int measureSpec) {
        int size = View$MeasureSpec.getSize(n);
        int n2 = View$MeasureSpec.getSize(n);
        if (3 == this.mMode) {
            n2 = this.mShortcutSize;
            size = this.mShortcutSize + this.getPaddingLeft() + this.getPaddingRight();
        }
        int mActionRowHeight;
        if (this.mActionRow.getVisibility() != 8) {
            mActionRowHeight = this.mActionRowHeight;
        }
        else {
            mActionRowHeight = 0;
        }
        final int size2 = View$MeasureSpec.getSize(measureSpec);
        final int mode = View$MeasureSpec.getMode(measureSpec);
        final ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
        if ((layoutParams != null && layoutParams.height == -2) || mode == 0) {
            n = -1;
        }
        else {
            n = size2;
        }
        n = this.getHeightForMode(n);
        measureSpec = size2 - this.getPaddingTop() - this.getPaddingBottom();
        if (size2 < n + mActionRowHeight && mode != 0) {
            if (this.getMode() != 2 || size2 < this.mLargeHeight + mActionRowHeight) {
                n = measureSpec;
                if (this.getMode() == 3) {
                    n = this.mShortcutSize;
                }
            }
        }
        else if (mode == 1073741824) {
            n = Math.min(n, measureSpec);
        }
        final int paddingTop = this.getPaddingTop();
        final int paddingBottom = this.getPaddingBottom();
        measureSpec = View$MeasureSpec.makeMeasureSpec(n2, 1073741824);
        this.measureChild((View)this.mCurrentView, measureSpec, View$MeasureSpec.makeMeasureSpec(paddingTop + n + paddingBottom, 1073741824));
        this.measureChild((View)this.mActionRow, measureSpec, View$MeasureSpec.makeMeasureSpec(this.getPaddingTop() + mActionRowHeight + this.getPaddingBottom(), 1073741824));
        this.setMeasuredDimension(size, n + (this.getPaddingTop() + mActionRowHeight + this.getPaddingBottom()));
    }
    
    public boolean onTouchEvent(final MotionEvent motionEvent) {
        final boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (this.mLongClickListener != null) {
            return this.handleTouchForLongpress(motionEvent);
        }
        return onTouchEvent;
    }
    
    protected void onVisibilityChanged(final View view, final int n) {
        super.onVisibilityChanged(view, n);
        if (this.isAttachedToWindow()) {
            this.logSliceMetricsVisibilityChange(n == 0);
        }
    }
    
    protected void onWindowVisibilityChanged(final int n) {
        super.onWindowVisibilityChanged(n);
        this.logSliceMetricsVisibilityChange(n == 0);
    }
    
    public void setAccentColor(final int mThemeTintColor) {
        this.mThemeTintColor = mThemeTintColor;
        this.mCurrentView.setTint(this.getTintColor());
    }
    
    public void setClickInfo(final int[] mClickInfo) {
        this.mClickInfo = mClickInfo;
    }
    
    public void setMode(final int n) {
        this.setMode(n, false);
    }
    
    public void setMode(final int n, final boolean b) {
        if (b) {
            Log.e("SliceView", "Animation not supported yet");
        }
        if (this.mMode == n) {
            return;
        }
        int mMode = n;
        if (n != 1 && (mMode = n) != 2 && (mMode = n) != 3) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown mode: ");
            sb.append(n);
            sb.append(" please use one of MODE_SHORTCUT, MODE_SMALL, MODE_LARGE");
            Log.w("SliceView", sb.toString());
            mMode = 2;
        }
        this.mMode = mMode;
        this.updateViewConfig();
    }
    
    public void setOnClickListener(final View.OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }
    
    public void setOnLongClickListener(final View$OnLongClickListener view$OnLongClickListener) {
        super.setOnLongClickListener(view$OnLongClickListener);
        this.mLongClickListener = view$OnLongClickListener;
    }
    
    public void setOnSliceActionListener(final OnSliceActionListener mSliceObserver) {
        this.mSliceObserver = mSliceObserver;
        this.mCurrentView.setSliceActionListener(this.mSliceObserver);
    }
    
    public void setScrollable(final boolean mIsScrollable) {
        if (mIsScrollable != this.mIsScrollable) {
            this.mIsScrollable = mIsScrollable;
            if (this.mCurrentView instanceof LargeTemplateView) {
                ((LargeTemplateView)this.mCurrentView).setScrollable(this.mIsScrollable);
            }
        }
    }
    
    public void setShowActionRow(final boolean mShowActions) {
        this.mShowActions = mShowActions;
        this.updateActions();
    }
    
    public void setSlice(final Slice mCurrentSlice) {
        this.initSliceMetrics(mCurrentSlice);
        if (mCurrentSlice != null && (this.mCurrentSlice == null || !this.mCurrentSlice.getUri().equals((Object)mCurrentSlice.getUri()))) {
            this.mCurrentView.resetView();
        }
        this.mCurrentSlice = mCurrentSlice;
        this.mListContent = new ListContent(this.getContext(), this.mCurrentSlice, this.mAttrs, this.mDefStyleAttr, this.mDefStyleRes);
        if (!this.mListContent.isValid()) {
            this.mActions = null;
            this.mCurrentView.resetView();
            this.updateActions();
            return;
        }
        this.mActions = this.mListContent.getSliceActions();
        final SliceMetadata from = SliceMetadata.from(this.getContext(), this.mCurrentSlice);
        final long lastUpdatedTime = from.getLastUpdatedTime();
        final long expiry = from.getExpiry();
        final long currentTimeMillis = System.currentTimeMillis();
        this.mCurrentView.setLastUpdated(lastUpdatedTime);
        final boolean b = false;
        final boolean b2 = expiry != 0L && expiry != -1L && currentTimeMillis > expiry;
        final SliceChildView mCurrentView = this.mCurrentView;
        boolean showLastUpdated = b;
        if (this.mShowLastUpdated) {
            showLastUpdated = b;
            if (b2) {
                showLastUpdated = true;
            }
        }
        mCurrentView.setShowLastUpdated(showLastUpdated);
        this.mCurrentView.setTint(this.getTintColor());
        if (this.mListContent.getLayoutDirItem() != null) {
            this.mCurrentView.setLayoutDirection(this.mListContent.getLayoutDirItem().getInt());
        }
        else {
            this.mCurrentView.setLayoutDirection(2);
        }
        this.mCurrentView.setSliceContent(this.mListContent);
        this.updateActions();
        this.logSliceMetricsVisibilityChange(true);
    }
    
    public void setSliceActions(final List<SliceAction> mActions) {
        if (this.mCurrentSlice != null) {
            final List<SliceAction> sliceActions = this.mListContent.getSliceActions();
            if (sliceActions != null && mActions != null) {
                for (int i = 0; i < mActions.size(); ++i) {
                    if (!sliceActions.contains(mActions.get(i))) {
                        final StringBuilder sb = new StringBuilder();
                        sb.append("Trying to set an action that isn't available: ");
                        sb.append(mActions.get(i));
                        throw new IllegalArgumentException(sb.toString());
                    }
                }
            }
            this.mActions = mActions;
            this.updateActions();
            return;
        }
        throw new IllegalStateException("Trying to set actions on a view without a slice");
    }
    
    @Deprecated
    public void setTint(final int accentColor) {
        this.setAccentColor(accentColor);
    }
    
    public interface OnSliceActionListener
    {
        void onSliceAction(final EventInfo p0, final SliceItem p1);
    }
}
