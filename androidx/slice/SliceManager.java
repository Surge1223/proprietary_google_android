package androidx.slice;

import android.net.Uri;
import java.util.List;
import android.support.v4.os.BuildCompat;
import android.content.Context;

public abstract class SliceManager
{
    public static SliceManager getInstance(final Context context) {
        if (BuildCompat.isAtLeastP()) {
            return new SliceManagerWrapper(context);
        }
        return new SliceManagerCompat(context);
    }
    
    public abstract List<Uri> getPinnedSlices();
    
    public abstract void grantSlicePermission(final String p0, final Uri p1);
}
