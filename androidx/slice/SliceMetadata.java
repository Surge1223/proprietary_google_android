package androidx.slice;

import android.text.TextUtils;
import java.util.ArrayList;
import android.util.AttributeSet;
import androidx.slice.core.SliceQuery;
import androidx.slice.core.SliceAction;
import java.util.List;
import androidx.slice.core.SliceActionImpl;
import androidx.slice.widget.ListContent;
import android.content.Context;

public class SliceMetadata
{
    private Context mContext;
    private long mExpiry;
    private SliceItem mHeaderItem;
    private long mLastUpdated;
    private ListContent mListContent;
    private SliceActionImpl mPrimaryAction;
    private Slice mSlice;
    private List<SliceAction> mSliceActions;
    private int mTemplateType;
    
    private SliceMetadata(final Context mContext, final Slice mSlice) {
        this.mSlice = mSlice;
        this.mContext = mContext;
        final SliceItem find = SliceQuery.find(mSlice, "long", "ttl", null);
        if (find != null) {
            this.mExpiry = find.getTimestamp();
        }
        final SliceItem find2 = SliceQuery.find(mSlice, "long", "last_updated", null);
        if (find2 != null) {
            this.mLastUpdated = find2.getTimestamp();
        }
        this.mListContent = new ListContent(mContext, mSlice, null, 0, 0);
        this.mHeaderItem = this.mListContent.getHeaderItem();
        this.mTemplateType = this.mListContent.getHeaderTemplateType();
        this.mSliceActions = this.mListContent.getSliceActions();
        final SliceItem primaryAction = this.mListContent.getPrimaryAction();
        if (primaryAction != null) {
            this.mPrimaryAction = new SliceActionImpl(primaryAction);
        }
    }
    
    public static SliceMetadata from(final Context context, final Slice slice) {
        return new SliceMetadata(context, slice);
    }
    
    public static List<SliceAction> getSliceActions(final Slice slice) {
        final SliceItem find = SliceQuery.find(slice, "slice", "actions", null);
        List<SliceItem> all;
        if (find != null) {
            all = SliceQuery.findAll(find, "slice", new String[] { "actions", "shortcut" }, null);
        }
        else {
            all = null;
        }
        if (all != null) {
            final ArrayList list = new ArrayList<SliceActionImpl>(all.size());
            for (int i = 0; i < all.size(); ++i) {
                list.add(new SliceActionImpl(all.get(i)));
            }
            return (List<SliceAction>)list;
        }
        return null;
    }
    
    public long getExpiry() {
        return this.mExpiry;
    }
    
    public long getLastUpdatedTime() {
        return this.mLastUpdated;
    }
    
    public int getLoadingState() {
        final boolean b = SliceQuery.find(this.mSlice, null, "partial", null) != null;
        if (!this.mListContent.isValid()) {
            return 0;
        }
        if (b) {
            return 1;
        }
        return 2;
    }
    
    public List<String> getSliceKeywords() {
        final SliceItem find = SliceQuery.find(this.mSlice, "slice", "keywords", null);
        if (find != null) {
            final List<SliceItem> all = SliceQuery.findAll(find, "text");
            if (all != null) {
                final ArrayList<String> list = new ArrayList<String>();
                for (int i = 0; i < all.size(); ++i) {
                    final String s = (String)all.get(i).getText();
                    if (!TextUtils.isEmpty((CharSequence)s)) {
                        list.add(s);
                    }
                }
                return list;
            }
        }
        return null;
    }
}
