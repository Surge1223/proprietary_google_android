package androidx.slice;

import android.support.v4.util.ObjectsCompat;
import java.lang.reflect.Array;

class ArrayUtils
{
    public static <T> T[] appendElement(final Class<T> clazz, final T[] array, final T t) {
        int length;
        Object[] array2;
        if (array != null) {
            length = array.length;
            array2 = (Object[])Array.newInstance(clazz, length + 1);
            System.arraycopy(array, 0, array2, 0, length);
        }
        else {
            length = 0;
            array2 = (Object[])Array.newInstance(clazz, 1);
        }
        array2[length] = t;
        return (T[])array2;
    }
    
    public static <T> boolean contains(final T[] array, final T t) {
        for (int length = array.length, i = 0; i < length; ++i) {
            if (ObjectsCompat.equals(array[i], t)) {
                return true;
            }
        }
        return false;
    }
}
