package androidx.slice;

import java.util.Collection;
import android.net.Uri;
import androidx.slice.widget.SliceLiveData;
import android.content.Context;
import android.app.slice.SliceSpec;
import java.util.Set;
import android.app.slice.SliceManager;

class SliceViewManagerWrapper extends SliceViewManagerBase
{
    private final SliceManager mManager;
    private final Set<SliceSpec> mSpecs;
    
    SliceViewManagerWrapper(final Context context) {
        this(context, (SliceManager)context.getSystemService((Class)SliceManager.class));
    }
    
    SliceViewManagerWrapper(final Context context, final SliceManager mManager) {
        super(context);
        this.mManager = mManager;
        this.mSpecs = SliceConvert.unwrap(SliceLiveData.SUPPORTED_SPECS);
    }
    
    @Override
    public Slice bindSlice(final Uri uri) {
        return SliceConvert.wrap(this.mManager.bindSlice(uri, (Set)this.mSpecs));
    }
    
    @Override
    public Collection<Uri> getSliceDescendants(final Uri uri) {
        return (Collection<Uri>)this.mManager.getSliceDescendants(uri);
    }
    
    @Override
    public void pinSlice(final Uri uri) {
        this.mManager.pinSlice(uri, (Set)this.mSpecs);
    }
    
    @Override
    public void unpinSlice(final Uri uri) {
        this.mManager.unpinSlice(uri);
    }
}
