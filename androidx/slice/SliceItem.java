package androidx.slice;

import android.app.RemoteInput;
import java.util.Arrays;
import android.app.PendingIntent$CanceledException;
import android.os.Handler;
import android.app.PendingIntent$OnFinished;
import android.content.Intent;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.graphics.drawable.IconCompat;
import java.util.List;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.app.PendingIntent;
import androidx.versionedparcelable.CustomVersionedParcelable;

public final class SliceItem extends CustomVersionedParcelable
{
    String mFormat;
    protected String[] mHints;
    Object mObj;
    String mSubType;
    
    public SliceItem() {
        this.mHints = new String[0];
    }
    
    public SliceItem(final PendingIntent pendingIntent, final Slice slice, final String s, final String s2, final String[] array) {
        this(new Pair(pendingIntent, slice), s, s2, array);
    }
    
    public SliceItem(final Bundle bundle) {
        this.mHints = new String[0];
        this.mHints = bundle.getStringArray("hints");
        this.mFormat = bundle.getString("format");
        this.mSubType = bundle.getString("subtype");
        this.mObj = readObj(this.mFormat, bundle);
    }
    
    public SliceItem(final Object o, final String s, final String s2, final List<String> list) {
        this(o, s, s2, list.toArray(new String[list.size()]));
    }
    
    public SliceItem(final Object mObj, final String mFormat, final String mSubType, final String[] mHints) {
        this.mHints = new String[0];
        this.mHints = mHints;
        this.mFormat = mFormat;
        this.mSubType = mSubType;
        this.mObj = mObj;
    }
    
    private static Object readObj(final String s, final Bundle bundle) {
        int n = 0;
        Label_0176: {
            switch (s.hashCode()) {
                case 109526418: {
                    if (s.equals("slice")) {
                        n = 2;
                        break Label_0176;
                    }
                    break;
                }
                case 100358090: {
                    if (s.equals("input")) {
                        n = 1;
                        break Label_0176;
                    }
                    break;
                }
                case 100313435: {
                    if (s.equals("image")) {
                        n = 0;
                        break Label_0176;
                    }
                    break;
                }
                case 3556653: {
                    if (s.equals("text")) {
                        n = 3;
                        break Label_0176;
                    }
                    break;
                }
                case 3327612: {
                    if (s.equals("long")) {
                        n = 6;
                        break Label_0176;
                    }
                    break;
                }
                case 104431: {
                    if (s.equals("int")) {
                        n = 5;
                        break Label_0176;
                    }
                    break;
                }
                case -1422950858: {
                    if (s.equals("action")) {
                        n = 4;
                        break Label_0176;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unsupported type ");
                sb.append(s);
                throw new RuntimeException(sb.toString());
            }
            case 6: {
                return bundle.getLong("obj");
            }
            case 5: {
                return bundle.getInt("obj");
            }
            case 4: {
                return new Pair(bundle.getParcelable("obj"), new Slice(bundle.getBundle("obj_2")));
            }
            case 3: {
                return bundle.getCharSequence("obj");
            }
            case 2: {
                return new Slice(bundle.getBundle("obj"));
            }
            case 1: {
                return bundle.getParcelable("obj");
            }
            case 0: {
                return IconCompat.createFromBundle(bundle.getBundle("obj"));
            }
        }
    }
    
    public static String typeToString(final String s) {
        int n = 0;
        Label_0176: {
            switch (s.hashCode()) {
                case 109526418: {
                    if (s.equals("slice")) {
                        n = 0;
                        break Label_0176;
                    }
                    break;
                }
                case 100358090: {
                    if (s.equals("input")) {
                        n = 6;
                        break Label_0176;
                    }
                    break;
                }
                case 100313435: {
                    if (s.equals("image")) {
                        n = 2;
                        break Label_0176;
                    }
                    break;
                }
                case 3556653: {
                    if (s.equals("text")) {
                        n = 1;
                        break Label_0176;
                    }
                    break;
                }
                case 3327612: {
                    if (s.equals("long")) {
                        n = 5;
                        break Label_0176;
                    }
                    break;
                }
                case 104431: {
                    if (s.equals("int")) {
                        n = 4;
                        break Label_0176;
                    }
                    break;
                }
                case -1422950858: {
                    if (s.equals("action")) {
                        n = 3;
                        break Label_0176;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                final StringBuilder sb = new StringBuilder();
                sb.append("Unrecognized format: ");
                sb.append(s);
                return sb.toString();
            }
            case 6: {
                return "RemoteInput";
            }
            case 5: {
                return "Long";
            }
            case 4: {
                return "Int";
            }
            case 3: {
                return "Action";
            }
            case 2: {
                return "Image";
            }
            case 1: {
                return "Text";
            }
            case 0: {
                return "Slice";
            }
        }
    }
    
    private void writeObj(final Bundle bundle, final Object o, final String s) {
        int n = 0;
        Label_0184: {
            switch (s.hashCode()) {
                case 109526418: {
                    if (s.equals("slice")) {
                        n = 2;
                        break Label_0184;
                    }
                    break;
                }
                case 100358090: {
                    if (s.equals("input")) {
                        n = 1;
                        break Label_0184;
                    }
                    break;
                }
                case 100313435: {
                    if (s.equals("image")) {
                        n = 0;
                        break Label_0184;
                    }
                    break;
                }
                case 3556653: {
                    if (s.equals("text")) {
                        n = 4;
                        break Label_0184;
                    }
                    break;
                }
                case 3327612: {
                    if (s.equals("long")) {
                        n = 6;
                        break Label_0184;
                    }
                    break;
                }
                case 104431: {
                    if (s.equals("int")) {
                        n = 5;
                        break Label_0184;
                    }
                    break;
                }
                case -1422950858: {
                    if (s.equals("action")) {
                        n = 3;
                        break Label_0184;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            case 6: {
                bundle.putLong("obj", (long)this.mObj);
                break;
            }
            case 5: {
                bundle.putInt("obj", (int)this.mObj);
                break;
            }
            case 4: {
                bundle.putCharSequence("obj", (CharSequence)o);
                break;
            }
            case 3: {
                bundle.putParcelable("obj", (Parcelable)((Pair)o).first);
                bundle.putBundle("obj_2", ((Slice)((Pair)o).second).toBundle());
                break;
            }
            case 2: {
                bundle.putParcelable("obj", (Parcelable)((Slice)o).toBundle());
                break;
            }
            case 1: {
                bundle.putParcelable("obj", (Parcelable)o);
                break;
            }
            case 0: {
                bundle.putBundle("obj", ((IconCompat)o).toBundle());
                break;
            }
        }
    }
    
    public void addHint(final String s) {
        this.mHints = ArrayUtils.appendElement(String.class, this.mHints, s);
    }
    
    public void fireAction(final Context context, final Intent intent) throws PendingIntent$CanceledException {
        final F first = ((Pair)this.mObj).first;
        if (first instanceof PendingIntent) {
            ((PendingIntent)first).send(context, 0, intent, (PendingIntent$OnFinished)null, (Handler)null);
        }
        else {
            ((ActionHandler)first).onAction(this, context, intent);
        }
    }
    
    public PendingIntent getAction() {
        return (PendingIntent)((Pair)this.mObj).first;
    }
    
    public String getFormat() {
        return this.mFormat;
    }
    
    public List<String> getHints() {
        return Arrays.asList(this.mHints);
    }
    
    public IconCompat getIcon() {
        return (IconCompat)this.mObj;
    }
    
    public int getInt() {
        return (int)this.mObj;
    }
    
    public long getLong() {
        return (long)this.mObj;
    }
    
    public RemoteInput getRemoteInput() {
        return (RemoteInput)this.mObj;
    }
    
    public Slice getSlice() {
        if ("action".equals(this.getFormat())) {
            return (Slice)((Pair)this.mObj).second;
        }
        return (Slice)this.mObj;
    }
    
    public String getSubType() {
        return this.mSubType;
    }
    
    public CharSequence getText() {
        return (CharSequence)this.mObj;
    }
    
    @Deprecated
    public long getTimestamp() {
        return (long)this.mObj;
    }
    
    public boolean hasAnyHints(final String... array) {
        if (array == null) {
            return false;
        }
        for (int length = array.length, i = 0; i < length; ++i) {
            if (ArrayUtils.contains(this.mHints, array[i])) {
                return true;
            }
        }
        return false;
    }
    
    public boolean hasHint(final String s) {
        return ArrayUtils.contains(this.mHints, s);
    }
    
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        bundle.putStringArray("hints", this.mHints);
        bundle.putString("format", this.mFormat);
        bundle.putString("subtype", this.mSubType);
        this.writeObj(bundle, this.mObj, this.mFormat);
        return bundle;
    }
    
    @Override
    public String toString() {
        return this.toString("");
    }
    
    public String toString(final String s) {
        final StringBuilder sb = new StringBuilder();
        final String format = this.getFormat();
        int n = 0;
        Label_0172: {
            switch (format.hashCode()) {
                case 109526418: {
                    if (format.equals("slice")) {
                        n = 0;
                        break Label_0172;
                    }
                    break;
                }
                case 100313435: {
                    if (format.equals("image")) {
                        n = 3;
                        break Label_0172;
                    }
                    break;
                }
                case 3556653: {
                    if (format.equals("text")) {
                        n = 2;
                        break Label_0172;
                    }
                    break;
                }
                case 3327612: {
                    if (format.equals("long")) {
                        n = 5;
                        break Label_0172;
                    }
                    break;
                }
                case 104431: {
                    if (format.equals("int")) {
                        n = 4;
                        break Label_0172;
                    }
                    break;
                }
                case -1422950858: {
                    if (format.equals("action")) {
                        n = 1;
                        break Label_0172;
                    }
                    break;
                }
            }
            n = -1;
        }
        switch (n) {
            default: {
                sb.append(s);
                sb.append(typeToString(this.getFormat()));
                break;
            }
            case 5: {
                sb.append(s);
                sb.append(this.getLong());
                break;
            }
            case 4: {
                sb.append(s);
                sb.append(this.getInt());
                break;
            }
            case 3: {
                sb.append(s);
                sb.append(this.getIcon());
                break;
            }
            case 2: {
                sb.append(s);
                sb.append('\"');
                sb.append(this.getText());
                sb.append('\"');
                break;
            }
            case 1: {
                sb.append(s);
                sb.append(this.getAction());
                sb.append(",\n");
                sb.append(this.getSlice().toString(s));
                break;
            }
            case 0: {
                sb.append(this.getSlice().toString(s));
                break;
            }
        }
        if (!"slice".equals(this.getFormat())) {
            sb.append(' ');
            Slice.addHints(sb, this.mHints);
        }
        sb.append(",\n");
        return sb.toString();
    }
    
    public interface ActionHandler
    {
        void onAction(final SliceItem p0, final Context p1, final Intent p2);
    }
}
