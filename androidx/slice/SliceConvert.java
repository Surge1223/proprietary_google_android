package androidx.slice;

import android.support.v4.graphics.drawable.IconCompat;
import android.support.v4.util.ArraySet;
import java.util.Set;
import java.util.Iterator;
import java.util.List;
import android.app.slice.Slice$Builder;

public class SliceConvert
{
    public static android.app.slice.Slice unwrap(final Slice slice) {
        if (slice != null && slice.getUri() != null) {
            final Slice$Builder slice$Builder = new Slice$Builder(slice.getUri(), unwrap(slice.getSpec()));
            slice$Builder.addHints((List)slice.getHints());
            for (final SliceItem sliceItem : slice.getItems()) {
                final String format = sliceItem.getFormat();
                switch (format) {
                    default: {
                        continue;
                    }
                    case "long": {
                        slice$Builder.addLong(sliceItem.getLong(), sliceItem.getSubType(), (List)sliceItem.getHints());
                        continue;
                    }
                    case "int": {
                        slice$Builder.addInt(sliceItem.getInt(), sliceItem.getSubType(), (List)sliceItem.getHints());
                        continue;
                    }
                    case "text": {
                        slice$Builder.addText(sliceItem.getText(), sliceItem.getSubType(), (List)sliceItem.getHints());
                        continue;
                    }
                    case "action": {
                        slice$Builder.addAction(sliceItem.getAction(), unwrap(sliceItem.getSlice()), sliceItem.getSubType());
                        continue;
                    }
                    case "input": {
                        slice$Builder.addRemoteInput(sliceItem.getRemoteInput(), sliceItem.getSubType(), (List)sliceItem.getHints());
                        continue;
                    }
                    case "image": {
                        slice$Builder.addIcon(sliceItem.getIcon().toIcon(), sliceItem.getSubType(), (List)sliceItem.getHints());
                        continue;
                    }
                    case "slice": {
                        slice$Builder.addSubSlice(unwrap(sliceItem.getSlice()), sliceItem.getSubType());
                        continue;
                    }
                }
            }
            return slice$Builder.build();
        }
        return null;
    }
    
    private static android.app.slice.SliceSpec unwrap(final SliceSpec sliceSpec) {
        if (sliceSpec == null) {
            return null;
        }
        return new android.app.slice.SliceSpec(sliceSpec.getType(), sliceSpec.getRevision());
    }
    
    static Set<android.app.slice.SliceSpec> unwrap(final Set<SliceSpec> set) {
        final ArraySet<android.app.slice.SliceSpec> set2 = new ArraySet<android.app.slice.SliceSpec>();
        if (set != null) {
            final Iterator<SliceSpec> iterator = set.iterator();
            while (iterator.hasNext()) {
                set2.add(unwrap(iterator.next()));
            }
        }
        return set2;
    }
    
    public static Slice wrap(final android.app.slice.Slice slice) {
        if (slice != null && slice.getUri() != null) {
            final Slice.Builder builder = new Slice.Builder(slice.getUri());
            builder.addHints(slice.getHints());
            builder.setSpec(wrap(slice.getSpec()));
            for (final android.app.slice.SliceItem sliceItem : slice.getItems()) {
                final String format = sliceItem.getFormat();
                switch (format) {
                    default: {
                        continue;
                    }
                    case "long": {
                        builder.addLong(sliceItem.getLong(), sliceItem.getSubType(), sliceItem.getHints());
                        continue;
                    }
                    case "int": {
                        builder.addInt(sliceItem.getInt(), sliceItem.getSubType(), sliceItem.getHints());
                        continue;
                    }
                    case "text": {
                        builder.addText(sliceItem.getText(), sliceItem.getSubType(), sliceItem.getHints());
                        continue;
                    }
                    case "action": {
                        builder.addAction(sliceItem.getAction(), wrap(sliceItem.getSlice()), sliceItem.getSubType());
                        continue;
                    }
                    case "input": {
                        builder.addRemoteInput(sliceItem.getRemoteInput(), sliceItem.getSubType(), sliceItem.getHints());
                        continue;
                    }
                    case "image": {
                        builder.addIcon(IconCompat.createFromIcon(sliceItem.getIcon()), sliceItem.getSubType(), sliceItem.getHints());
                        continue;
                    }
                    case "slice": {
                        builder.addSubSlice(wrap(sliceItem.getSlice()), sliceItem.getSubType());
                        continue;
                    }
                }
            }
            return builder.build();
        }
        return null;
    }
    
    private static SliceSpec wrap(final android.app.slice.SliceSpec sliceSpec) {
        if (sliceSpec == null) {
            return null;
        }
        return new SliceSpec(sliceSpec.getType(), sliceSpec.getRevision());
    }
    
    public static Set<SliceSpec> wrap(final Set<android.app.slice.SliceSpec> set) {
        final ArraySet<SliceSpec> set2 = new ArraySet<SliceSpec>();
        if (set != null) {
            final Iterator<android.app.slice.SliceSpec> iterator = set.iterator();
            while (iterator.hasNext()) {
                set2.add(wrap(iterator.next()));
            }
        }
        return set2;
    }
}
