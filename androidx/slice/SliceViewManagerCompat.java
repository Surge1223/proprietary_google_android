package androidx.slice;

import java.util.Collection;
import androidx.slice.compat.SliceProviderCompat;
import androidx.slice.widget.SliceLiveData;
import android.net.Uri;
import android.content.Context;

class SliceViewManagerCompat extends SliceViewManagerBase
{
    SliceViewManagerCompat(final Context context) {
        super(context);
    }
    
    @Override
    public Slice bindSlice(final Uri uri) {
        return SliceProviderCompat.bindSlice(this.mContext, uri, SliceLiveData.SUPPORTED_SPECS);
    }
    
    @Override
    public Collection<Uri> getSliceDescendants(final Uri uri) {
        return SliceProviderCompat.getSliceDescendants(this.mContext, uri);
    }
    
    @Override
    public void pinSlice(final Uri uri) {
        SliceProviderCompat.pinSlice(this.mContext, uri, SliceLiveData.SUPPORTED_SPECS);
    }
    
    @Override
    public void unpinSlice(final Uri uri) {
        SliceProviderCompat.unpinSlice(this.mContext, uri, SliceLiveData.SUPPORTED_SPECS);
    }
}
