package androidx.slice;

import java.util.Collection;
import android.net.Uri;
import android.support.v4.os.BuildCompat;
import android.content.Context;

public abstract class SliceViewManager
{
    public static SliceViewManager getInstance(final Context context) {
        if (BuildCompat.isAtLeastP()) {
            return new SliceViewManagerWrapper(context);
        }
        return new SliceViewManagerCompat(context);
    }
    
    public abstract Slice bindSlice(final Uri p0);
    
    public abstract Collection<Uri> getSliceDescendants(final Uri p0);
    
    public abstract void pinSlice(final Uri p0);
    
    public abstract void registerSliceCallback(final Uri p0, final SliceCallback p1);
    
    public abstract void unpinSlice(final Uri p0);
    
    public abstract void unregisterSliceCallback(final Uri p0, final SliceCallback p1);
    
    public interface SliceCallback
    {
        void onSliceUpdated(final Slice p0);
    }
}
