package androidx.slice;

import android.app.RemoteInput;
import android.support.v4.graphics.drawable.IconCompat;
import java.util.Collection;
import android.support.v4.util.Preconditions;
import android.app.PendingIntent;
import java.util.Arrays;
import java.util.List;
import android.app.slice.SliceManager;
import androidx.slice.compat.SliceProviderCompat;
import android.support.v4.os.BuildCompat;
import java.util.Set;
import android.content.Context;
import android.net.Uri;
import java.util.ArrayList;
import android.os.Parcelable;
import android.os.Bundle;
import androidx.versionedparcelable.VersionedParcelable;

public final class Slice implements VersionedParcelable
{
    String[] mHints;
    SliceItem[] mItems;
    SliceSpec mSpec;
    String mUri;
    
    public Slice() {
        this.mItems = new SliceItem[0];
        this.mHints = new String[0];
    }
    
    public Slice(final Bundle bundle) {
        int i = 0;
        this.mItems = new SliceItem[0];
        this.mHints = new String[0];
        this.mHints = bundle.getStringArray("hints");
        final Parcelable[] parcelableArray = bundle.getParcelableArray("items");
        this.mItems = new SliceItem[parcelableArray.length];
        while (i < this.mItems.length) {
            if (parcelableArray[i] instanceof Bundle) {
                this.mItems[i] = new SliceItem((Bundle)parcelableArray[i]);
            }
            ++i;
        }
        this.mUri = bundle.getParcelable("uri").toString();
        SliceSpec mSpec;
        if (bundle.containsKey("type")) {
            mSpec = new SliceSpec(bundle.getString("type"), bundle.getInt("revision"));
        }
        else {
            mSpec = null;
        }
        this.mSpec = mSpec;
    }
    
    Slice(final ArrayList<SliceItem> list, final String[] mHints, final Uri uri, final SliceSpec mSpec) {
        this.mItems = new SliceItem[0];
        this.mHints = new String[0];
        this.mHints = mHints;
        this.mItems = list.toArray(new SliceItem[list.size()]);
        this.mUri = uri.toString();
        this.mSpec = mSpec;
    }
    
    public static void addHints(final StringBuilder sb, final String[] array) {
        if (array != null && array.length != 0) {
            sb.append("(");
            final int n = array.length - 1;
            for (int i = 0; i < n; ++i) {
                sb.append(array[i]);
                sb.append(", ");
            }
            sb.append(array[n]);
            sb.append(") ");
        }
    }
    
    public static Slice bindSlice(final Context context, final Uri uri, final Set<SliceSpec> set) {
        if (BuildCompat.isAtLeastP()) {
            return callBindSlice(context, uri, set);
        }
        return SliceProviderCompat.bindSlice(context, uri, set);
    }
    
    private static Slice callBindSlice(final Context context, final Uri uri, final Set<SliceSpec> set) {
        return SliceConvert.wrap(((SliceManager)context.getSystemService((Class)SliceManager.class)).bindSlice(uri, (Set)SliceConvert.unwrap(set)));
    }
    
    public List<String> getHints() {
        return Arrays.asList(this.mHints);
    }
    
    public List<SliceItem> getItems() {
        return Arrays.asList(this.mItems);
    }
    
    public SliceSpec getSpec() {
        return this.mSpec;
    }
    
    public Uri getUri() {
        return Uri.parse(this.mUri);
    }
    
    public boolean hasHint(final String s) {
        return ArrayUtils.contains(this.mHints, s);
    }
    
    public Bundle toBundle() {
        final Bundle bundle = new Bundle();
        bundle.putStringArray("hints", this.mHints);
        final Parcelable[] array = new Parcelable[this.mItems.length];
        for (int i = 0; i < this.mItems.length; ++i) {
            array[i] = (Parcelable)this.mItems[i].toBundle();
        }
        bundle.putParcelableArray("items", array);
        bundle.putParcelable("uri", (Parcelable)Uri.parse(this.mUri));
        if (this.mSpec != null) {
            bundle.putString("type", this.mSpec.getType());
            bundle.putInt("revision", this.mSpec.getRevision());
        }
        return bundle;
    }
    
    @Override
    public String toString() {
        return this.toString("");
    }
    
    public String toString(final String s) {
        final StringBuilder sb = new StringBuilder();
        sb.append(s);
        sb.append("slice ");
        addHints(sb, this.mHints);
        sb.append("{\n");
        final StringBuilder sb2 = new StringBuilder();
        sb2.append(s);
        sb2.append("  ");
        final String string = sb2.toString();
        for (int i = 0; i < this.mItems.length; ++i) {
            sb.append(this.mItems[i].toString(string));
        }
        sb.append(s);
        sb.append("}");
        return sb.toString();
    }
    
    public static class Builder
    {
        private ArrayList<String> mHints;
        private ArrayList<SliceItem> mItems;
        private SliceSpec mSpec;
        private final Uri mUri;
        
        public Builder(final Uri mUri) {
            this.mItems = new ArrayList<SliceItem>();
            this.mHints = new ArrayList<String>();
            this.mUri = mUri;
        }
        
        public Builder(final Builder builder) {
            this.mItems = new ArrayList<SliceItem>();
            this.mHints = new ArrayList<String>();
            this.mUri = builder.mUri.buildUpon().appendPath("_gen").appendPath(String.valueOf(this.mItems.size())).build();
        }
        
        public Builder addAction(final PendingIntent pendingIntent, final Slice slice, final String s) {
            Preconditions.checkNotNull(pendingIntent);
            Preconditions.checkNotNull(slice);
            String[] array;
            if (slice != null) {
                array = slice.getHints().toArray(new String[slice.getHints().size()]);
            }
            else {
                array = new String[0];
            }
            this.mItems.add(new SliceItem(pendingIntent, slice, "action", s, array));
            return this;
        }
        
        public Builder addHints(final List<String> list) {
            return this.addHints((String[])list.toArray(new String[list.size()]));
        }
        
        public Builder addHints(final String... array) {
            this.mHints.addAll(Arrays.asList(array));
            return this;
        }
        
        public Builder addIcon(final IconCompat iconCompat, final String s, final List<String> list) {
            Preconditions.checkNotNull(iconCompat);
            return this.addIcon(iconCompat, s, (String[])list.toArray(new String[list.size()]));
        }
        
        public Builder addIcon(final IconCompat iconCompat, final String s, final String... array) {
            Preconditions.checkNotNull(iconCompat);
            this.mItems.add(new SliceItem(iconCompat, "image", s, array));
            return this;
        }
        
        public Builder addInt(final int n, final String s, final List<String> list) {
            return this.addInt(n, s, (String[])list.toArray(new String[list.size()]));
        }
        
        public Builder addInt(final int n, final String s, final String... array) {
            this.mItems.add(new SliceItem(n, "int", s, array));
            return this;
        }
        
        public Builder addItem(final SliceItem sliceItem) {
            this.mItems.add(sliceItem);
            return this;
        }
        
        public Builder addLong(final long n, final String s, final List<String> list) {
            return this.addLong(n, s, (String[])list.toArray(new String[list.size()]));
        }
        
        public Builder addLong(final long n, final String s, final String... array) {
            this.mItems.add(new SliceItem(n, "long", s, array));
            return this;
        }
        
        public Builder addRemoteInput(final RemoteInput remoteInput, final String s, final List<String> list) {
            Preconditions.checkNotNull(remoteInput);
            return this.addRemoteInput(remoteInput, s, (String[])list.toArray(new String[list.size()]));
        }
        
        public Builder addRemoteInput(final RemoteInput remoteInput, final String s, final String... array) {
            Preconditions.checkNotNull(remoteInput);
            this.mItems.add(new SliceItem(remoteInput, "input", s, array));
            return this;
        }
        
        public Builder addSubSlice(final Slice slice) {
            Preconditions.checkNotNull(slice);
            return this.addSubSlice(slice, null);
        }
        
        public Builder addSubSlice(final Slice slice, final String s) {
            Preconditions.checkNotNull(slice);
            this.mItems.add(new SliceItem(slice, "slice", s, slice.getHints().toArray(new String[slice.getHints().size()])));
            return this;
        }
        
        public Builder addText(final CharSequence charSequence, final String s, final List<String> list) {
            return this.addText(charSequence, s, (String[])list.toArray(new String[list.size()]));
        }
        
        public Builder addText(final CharSequence charSequence, final String s, final String... array) {
            this.mItems.add(new SliceItem(charSequence, "text", s, array));
            return this;
        }
        
        @Deprecated
        public Builder addTimestamp(final long n, final String s, final String... array) {
            this.mItems.add(new SliceItem(n, "long", s, array));
            return this;
        }
        
        public Slice build() {
            return new Slice(this.mItems, this.mHints.toArray(new String[this.mHints.size()]), this.mUri, this.mSpec);
        }
        
        public Builder setSpec(final SliceSpec mSpec) {
            this.mSpec = mSpec;
            return this;
        }
    }
}
