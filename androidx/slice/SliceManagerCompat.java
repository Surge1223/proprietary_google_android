package androidx.slice;

import androidx.slice.compat.SliceProviderCompat;
import android.net.Uri;
import java.util.List;
import android.content.Context;

class SliceManagerCompat extends SliceManager
{
    private final Context mContext;
    
    SliceManagerCompat(final Context mContext) {
        this.mContext = mContext;
    }
    
    @Override
    public List<Uri> getPinnedSlices() {
        return SliceProviderCompat.getPinnedSlices(this.mContext);
    }
    
    @Override
    public void grantSlicePermission(final String s, final Uri uri) {
        SliceProviderCompat.grantSlicePermission(this.mContext, this.mContext.getPackageName(), s, uri);
    }
}
