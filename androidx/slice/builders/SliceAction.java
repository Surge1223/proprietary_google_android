package androidx.slice.builders;

import androidx.slice.Slice;
import android.support.v4.graphics.drawable.IconCompat;
import android.app.PendingIntent;
import androidx.slice.core.SliceActionImpl;

public class SliceAction implements androidx.slice.core.SliceAction
{
    private SliceActionImpl mSliceAction;
    
    public SliceAction(final PendingIntent pendingIntent, final IconCompat iconCompat, final int n, final CharSequence charSequence) {
        this.mSliceAction = new SliceActionImpl(pendingIntent, iconCompat, n, charSequence);
    }
    
    public SliceAction(final PendingIntent pendingIntent, final IconCompat iconCompat, final CharSequence charSequence) {
        this(pendingIntent, iconCompat, 0, charSequence);
    }
    
    public SliceAction(final PendingIntent pendingIntent, final CharSequence charSequence, final boolean b) {
        this.mSliceAction = new SliceActionImpl(pendingIntent, charSequence, b);
    }
    
    public Slice buildSlice(final Slice.Builder builder) {
        return this.mSliceAction.buildSlice(builder);
    }
    
    @Override
    public IconCompat getIcon() {
        return this.mSliceAction.getIcon();
    }
    
    @Override
    public int getImageMode() {
        return this.mSliceAction.getImageMode();
    }
    
    public SliceActionImpl getImpl() {
        return this.mSliceAction;
    }
    
    @Override
    public boolean isToggle() {
        return this.mSliceAction.isToggle();
    }
}
