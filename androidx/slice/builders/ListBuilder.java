package androidx.slice.builders;

import android.support.v4.graphics.drawable.IconCompat;
import android.app.PendingIntent;
import java.util.List;
import androidx.slice.builders.impl.ListBuilderBasicImpl;
import androidx.slice.builders.impl.ListBuilderV1Impl;
import androidx.slice.SliceSpecs;
import androidx.slice.builders.impl.TemplateBuilderImpl;
import android.support.v4.util.Consumer;
import android.net.Uri;
import android.content.Context;

public class ListBuilder extends TemplateSliceBuilder
{
    private androidx.slice.builders.impl.ListBuilder mImpl;
    
    public ListBuilder(final Context context, final Uri uri, final long ttl) {
        super(context, uri);
        this.mImpl.setTtl(ttl);
    }
    
    public ListBuilder addInputRange(final Consumer<InputRangeBuilder> consumer) {
        final InputRangeBuilder inputRangeBuilder = new InputRangeBuilder(this);
        consumer.accept(inputRangeBuilder);
        return this.addInputRange(inputRangeBuilder);
    }
    
    public ListBuilder addInputRange(final InputRangeBuilder inputRangeBuilder) {
        this.mImpl.addInputRange((TemplateBuilderImpl)inputRangeBuilder.mImpl);
        return this;
    }
    
    public ListBuilder addRow(final Consumer<RowBuilder> consumer) {
        final RowBuilder rowBuilder = new RowBuilder(this);
        consumer.accept(rowBuilder);
        return this.addRow(rowBuilder);
    }
    
    public ListBuilder addRow(final RowBuilder rowBuilder) {
        this.mImpl.addRow((TemplateBuilderImpl)rowBuilder.mImpl);
        return this;
    }
    
    @Override
    protected TemplateBuilderImpl selectImpl() {
        if (this.checkCompatible(SliceSpecs.LIST)) {
            return new ListBuilderV1Impl(this.getBuilder(), SliceSpecs.LIST, this.getClock());
        }
        if (this.checkCompatible(SliceSpecs.BASIC)) {
            return new ListBuilderBasicImpl(this.getBuilder(), SliceSpecs.BASIC);
        }
        return null;
    }
    
    public ListBuilder setAccentColor(final int color) {
        this.mImpl.setColor(color);
        return this;
    }
    
    @Deprecated
    public ListBuilder setColor(final int accentColor) {
        return this.setAccentColor(accentColor);
    }
    
    @Override
    void setImpl(final TemplateBuilderImpl templateBuilderImpl) {
        this.mImpl = (androidx.slice.builders.impl.ListBuilder)templateBuilderImpl;
    }
    
    public ListBuilder setKeywords(final List<String> keywords) {
        this.mImpl.setKeywords(keywords);
        return this;
    }
    
    public static class InputRangeBuilder extends TemplateSliceBuilder
    {
        private androidx.slice.builders.impl.ListBuilder.InputRangeBuilder mImpl;
        
        public InputRangeBuilder(final ListBuilder listBuilder) {
            super(listBuilder.mImpl.createInputRangeBuilder());
        }
        
        @Override
        void setImpl(final TemplateBuilderImpl templateBuilderImpl) {
            this.mImpl = (androidx.slice.builders.impl.ListBuilder.InputRangeBuilder)templateBuilderImpl;
        }
        
        public InputRangeBuilder setInputAction(final PendingIntent inputAction) {
            this.mImpl.setInputAction(inputAction);
            return this;
        }
        
        public InputRangeBuilder setMax(final int max) {
            ((androidx.slice.builders.impl.ListBuilder.RangeBuilder)this.mImpl).setMax(max);
            return this;
        }
        
        public InputRangeBuilder setPrimaryAction(final SliceAction primaryAction) {
            ((androidx.slice.builders.impl.ListBuilder.RangeBuilder)this.mImpl).setPrimaryAction(primaryAction);
            return this;
        }
        
        public InputRangeBuilder setSubtitle(final CharSequence subtitle) {
            ((androidx.slice.builders.impl.ListBuilder.RangeBuilder)this.mImpl).setSubtitle(subtitle);
            return this;
        }
        
        public InputRangeBuilder setTitle(final CharSequence title) {
            ((androidx.slice.builders.impl.ListBuilder.RangeBuilder)this.mImpl).setTitle(title);
            return this;
        }
        
        public InputRangeBuilder setValue(final int value) {
            ((androidx.slice.builders.impl.ListBuilder.RangeBuilder)this.mImpl).setValue(value);
            return this;
        }
    }
    
    public static class RowBuilder extends TemplateSliceBuilder
    {
        private boolean mHasDefaultToggle;
        private boolean mHasEndActionOrToggle;
        private boolean mHasEndImage;
        private androidx.slice.builders.impl.ListBuilder.RowBuilder mImpl;
        
        public RowBuilder(final ListBuilder listBuilder) {
            super(listBuilder.mImpl.createRowBuilder());
        }
        
        public RowBuilder addEndItem(final SliceAction sliceAction) {
            return this.addEndItem(sliceAction, false);
        }
        
        public RowBuilder addEndItem(final SliceAction sliceAction, final boolean b) {
            if (this.mHasEndImage) {
                throw new IllegalArgumentException("Trying to add an action to end items when anicon has already been added. End items cannot have a mixture of actions and icons.");
            }
            if (!this.mHasDefaultToggle) {
                this.mImpl.addEndItem(sliceAction, b);
                this.mHasDefaultToggle = sliceAction.getImpl().isDefaultToggle();
                this.mHasEndActionOrToggle = true;
                return this;
            }
            throw new IllegalStateException("Only one non-custom toggle can be added in a single row. If you would like to include multiple toggles in a row, set a custom icon for each toggle.");
        }
        
        @Override
        void setImpl(final TemplateBuilderImpl templateBuilderImpl) {
            this.mImpl = (androidx.slice.builders.impl.ListBuilder.RowBuilder)templateBuilderImpl;
        }
        
        public RowBuilder setPrimaryAction(final SliceAction primaryAction) {
            this.mImpl.setPrimaryAction(primaryAction);
            return this;
        }
        
        public RowBuilder setSubtitle(final CharSequence charSequence) {
            return this.setSubtitle(charSequence, false);
        }
        
        public RowBuilder setSubtitle(final CharSequence charSequence, final boolean b) {
            this.mImpl.setSubtitle(charSequence, b);
            return this;
        }
        
        public RowBuilder setTitle(final CharSequence title) {
            this.mImpl.setTitle(title);
            return this;
        }
        
        @Deprecated
        public RowBuilder setTitleItem(final IconCompat iconCompat) {
            return this.setTitleItem(iconCompat, 0);
        }
        
        public RowBuilder setTitleItem(final IconCompat iconCompat, final int n) {
            this.mImpl.setTitleItem(iconCompat, n, false);
            return this;
        }
    }
}
