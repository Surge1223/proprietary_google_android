package androidx.slice.builders;

import androidx.slice.SystemClock;
import androidx.slice.Clock;
import java.util.Arrays;
import androidx.slice.SliceSpecs;
import android.util.Log;
import java.util.Collection;
import java.util.ArrayList;
import androidx.slice.SliceProvider;
import android.net.Uri;
import androidx.slice.SliceSpec;
import java.util.List;
import androidx.slice.builders.impl.TemplateBuilderImpl;
import android.content.Context;
import androidx.slice.Slice;

public abstract class TemplateSliceBuilder
{
    private final Slice.Builder mBuilder;
    private final Context mContext;
    private final TemplateBuilderImpl mImpl;
    private List<SliceSpec> mSpecs;
    
    public TemplateSliceBuilder(final Context mContext, final Uri uri) {
        this.mBuilder = new Slice.Builder(uri);
        this.mContext = mContext;
        this.mSpecs = this.getSpecs();
        this.mImpl = this.selectImpl();
        if (this.mImpl != null) {
            this.setImpl(this.mImpl);
            return;
        }
        throw new IllegalArgumentException("No valid specs found");
    }
    
    protected TemplateSliceBuilder(final TemplateBuilderImpl mImpl) {
        this.mContext = null;
        this.mBuilder = null;
        this.setImpl(this.mImpl = mImpl);
    }
    
    private List<SliceSpec> getSpecs() {
        if (SliceProvider.getCurrentSpecs() != null) {
            return new ArrayList<SliceSpec>(SliceProvider.getCurrentSpecs());
        }
        Log.w("TemplateSliceBuilder", "Not currently bunding a slice");
        return Arrays.asList(SliceSpecs.BASIC);
    }
    
    public Slice build() {
        return this.mImpl.build();
    }
    
    protected boolean checkCompatible(final SliceSpec sliceSpec) {
        for (int size = this.mSpecs.size(), i = 0; i < size; ++i) {
            if (this.mSpecs.get(i).canRender(sliceSpec)) {
                return true;
            }
        }
        return false;
    }
    
    protected Slice.Builder getBuilder() {
        return this.mBuilder;
    }
    
    protected Clock getClock() {
        if (SliceProvider.getClock() != null) {
            return SliceProvider.getClock();
        }
        return new SystemClock();
    }
    
    protected TemplateBuilderImpl selectImpl() {
        return null;
    }
    
    abstract void setImpl(final TemplateBuilderImpl p0);
}
