package androidx.slice.builders.impl;

import androidx.slice.SliceItem;
import java.util.ArrayList;
import androidx.slice.builders.SliceAction;
import android.support.v4.graphics.drawable.IconCompat;
import android.app.PendingIntent;
import androidx.slice.Clock;
import androidx.slice.SliceSpec;
import androidx.slice.Slice;
import java.util.List;

public class ListBuilderV1Impl extends TemplateBuilderImpl implements ListBuilder
{
    private boolean mIsError;
    private List<Slice> mSliceActions;
    private Slice mSliceHeader;
    
    public ListBuilderV1Impl(final Slice.Builder builder, final SliceSpec sliceSpec, final Clock clock) {
        super(builder, sliceSpec, clock);
    }
    
    @Override
    public void addInputRange(final TemplateBuilderImpl templateBuilderImpl) {
        this.getBuilder().addSubSlice(templateBuilderImpl.build(), "range");
    }
    
    @Override
    public void addRow(final TemplateBuilderImpl templateBuilderImpl) {
        templateBuilderImpl.getBuilder().addHints("list_item");
        this.getBuilder().addSubSlice(templateBuilderImpl.build());
    }
    
    @Override
    public void apply(final Slice.Builder builder) {
        builder.addLong(this.getClock().currentTimeMillis(), "millis", "last_updated");
        if (this.mSliceHeader != null) {
            builder.addSubSlice(this.mSliceHeader);
        }
        if (this.mSliceActions != null) {
            final Slice.Builder builder2 = new Slice.Builder(builder);
            for (int i = 0; i < this.mSliceActions.size(); ++i) {
                builder2.addSubSlice(this.mSliceActions.get(i));
            }
            builder.addSubSlice(builder2.addHints("actions").build());
        }
        if (this.mIsError) {
            builder.addHints("error");
        }
    }
    
    @Override
    public TemplateBuilderImpl createInputRangeBuilder() {
        return new InputRangeBuilderImpl(this.createChildBuilder());
    }
    
    @Override
    public TemplateBuilderImpl createRowBuilder() {
        return new RowBuilderImpl(this);
    }
    
    @Override
    public void setColor(final int n) {
        this.getBuilder().addInt(n, "color", new String[0]);
    }
    
    @Override
    public void setKeywords(final List<String> list) {
        final Slice.Builder builder = new Slice.Builder(this.getBuilder());
        for (int i = 0; i < list.size(); ++i) {
            builder.addText(list.get(i), null, new String[0]);
        }
        this.getBuilder().addSubSlice(builder.addHints("keywords").build());
    }
    
    @Override
    public void setTtl(long n) {
        final long n2 = -1L;
        if (n == -1L) {
            n = n2;
        }
        else {
            n += this.getClock().currentTimeMillis();
        }
        this.getBuilder().addTimestamp(n, "millis", "ttl");
    }
    
    public static class InputRangeBuilderImpl extends RangeBuilderImpl implements InputRangeBuilder
    {
        private PendingIntent mAction;
        private IconCompat mThumb;
        
        public InputRangeBuilderImpl(final Slice.Builder builder) {
            super(builder);
        }
        
        @Override
        public void apply(final Slice.Builder builder) {
            if (this.mAction != null) {
                final Slice.Builder builder2 = new Slice.Builder(builder);
                super.apply(builder2);
                if (this.mThumb != null) {
                    builder2.addIcon(this.mThumb, null, new String[0]);
                }
                builder.addAction(this.mAction, builder2.build(), "range").addHints("list_item");
                return;
            }
            throw new IllegalStateException("Input ranges must have an associated action.");
        }
        
        @Override
        public void setInputAction(final PendingIntent mAction) {
            this.mAction = mAction;
        }
    }
    
    public static class RangeBuilderImpl extends TemplateBuilderImpl implements RangeBuilder
    {
        private CharSequence mContentDescr;
        private int mLayoutDir;
        private int mMax;
        private int mMin;
        private SliceAction mPrimaryAction;
        private CharSequence mSubtitle;
        private CharSequence mTitle;
        private int mValue;
        private boolean mValueSet;
        
        public RangeBuilderImpl(final Slice.Builder builder) {
            super(builder, null);
            this.mMin = 0;
            this.mMax = 100;
            this.mValue = 0;
            this.mValueSet = false;
            this.mLayoutDir = -1;
        }
        
        @Override
        public void apply(final Slice.Builder builder) {
            if (!this.mValueSet) {
                this.mValue = this.mMin;
            }
            if (this.mMin <= this.mValue && this.mValue <= this.mMax && this.mMin < this.mMax) {
                if (this.mTitle != null) {
                    builder.addText(this.mTitle, null, "title");
                }
                if (this.mSubtitle != null) {
                    builder.addText(this.mSubtitle, null, new String[0]);
                }
                if (this.mContentDescr != null) {
                    builder.addText(this.mContentDescr, "content_description", new String[0]);
                }
                if (this.mPrimaryAction != null) {
                    builder.addSubSlice(this.mPrimaryAction.buildSlice(new Slice.Builder(this.getBuilder()).addHints("title", "shortcut")), null);
                }
                if (this.mLayoutDir != -1) {
                    builder.addInt(this.mLayoutDir, "layout_direction", new String[0]);
                }
                builder.addHints("list_item").addInt(this.mMin, "min", new String[0]).addInt(this.mMax, "max", new String[0]).addInt(this.mValue, "value", new String[0]);
                return;
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("Invalid range values, min=");
            sb.append(this.mMin);
            sb.append(", value=");
            sb.append(this.mValue);
            sb.append(", max=");
            sb.append(this.mMax);
            sb.append(" ensure value falls within (min, max) and min < max.");
            throw new IllegalArgumentException(sb.toString());
        }
        
        @Override
        public void setMax(final int mMax) {
            this.mMax = mMax;
        }
        
        @Override
        public void setPrimaryAction(final SliceAction mPrimaryAction) {
            this.mPrimaryAction = mPrimaryAction;
        }
        
        @Override
        public void setSubtitle(final CharSequence mSubtitle) {
            this.mSubtitle = mSubtitle;
        }
        
        @Override
        public void setTitle(final CharSequence mTitle) {
            this.mTitle = mTitle;
        }
        
        @Override
        public void setValue(final int mValue) {
            this.mValue = mValue;
            this.mValueSet = true;
        }
    }
    
    public static class RowBuilderImpl extends TemplateBuilderImpl implements RowBuilder
    {
        private CharSequence mContentDescr;
        private ArrayList<Slice> mEndItems;
        private SliceAction mPrimaryAction;
        private Slice mStartItem;
        private SliceItem mSubtitleItem;
        private SliceItem mTitleItem;
        
        public RowBuilderImpl(final ListBuilderV1Impl listBuilderV1Impl) {
            super(listBuilderV1Impl.createChildBuilder(), null);
            this.mEndItems = new ArrayList<Slice>();
        }
        
        @Override
        public void addEndItem(final SliceAction sliceAction, final boolean b) {
            final Slice.Builder builder = new Slice.Builder(this.getBuilder());
            if (b) {
                builder.addHints("partial");
            }
            this.mEndItems.add(sliceAction.buildSlice(builder));
        }
        
        @Override
        public void apply(final Slice.Builder builder) {
            if (this.mStartItem != null) {
                builder.addSubSlice(this.mStartItem);
            }
            if (this.mTitleItem != null) {
                builder.addItem(this.mTitleItem);
            }
            if (this.mSubtitleItem != null) {
                builder.addItem(this.mSubtitleItem);
            }
            for (int i = 0; i < this.mEndItems.size(); ++i) {
                builder.addSubSlice(this.mEndItems.get(i));
            }
            if (this.mContentDescr != null) {
                builder.addText(this.mContentDescr, "content_description", new String[0]);
            }
            if (this.mPrimaryAction != null) {
                builder.addSubSlice(this.mPrimaryAction.buildSlice(new Slice.Builder(this.getBuilder()).addHints("title", "shortcut")), null);
            }
        }
        
        @Override
        public void setPrimaryAction(final SliceAction mPrimaryAction) {
            this.mPrimaryAction = mPrimaryAction;
        }
        
        @Override
        public void setSubtitle(final CharSequence charSequence, final boolean b) {
            this.mSubtitleItem = new SliceItem(charSequence, "text", null, new String[0]);
            if (b) {
                this.mSubtitleItem.addHint("partial");
            }
        }
        
        @Override
        public void setTitle(final CharSequence charSequence) {
            this.setTitle(charSequence, false);
        }
        
        public void setTitle(final CharSequence charSequence, final boolean b) {
            this.mTitleItem = new SliceItem(charSequence, "text", null, new String[] { "title" });
            if (b) {
                this.mTitleItem.addHint("partial");
            }
        }
        
        @Override
        public void setTitleItem(final IconCompat iconCompat, final int n, final boolean b) {
            final ArrayList<String> list = new ArrayList<String>();
            if (n != 0) {
                list.add("no_tint");
            }
            if (n == 2) {
                list.add("large");
            }
            if (b) {
                list.add("partial");
            }
            final Slice.Builder addIcon = new Slice.Builder(this.getBuilder()).addIcon(iconCompat, null, list);
            if (b) {
                addIcon.addHints("partial");
            }
            this.mStartItem = addIcon.addHints("title").build();
        }
    }
}
