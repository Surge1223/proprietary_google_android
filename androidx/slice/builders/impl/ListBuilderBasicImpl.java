package androidx.slice.builders.impl;

import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.builders.SliceAction;
import java.util.List;
import androidx.slice.SliceSpec;
import androidx.slice.Slice;

public class ListBuilderBasicImpl extends TemplateBuilderImpl implements ListBuilder
{
    boolean mIsError;
    
    public ListBuilderBasicImpl(final Slice.Builder builder, final SliceSpec sliceSpec) {
        super(builder, sliceSpec);
    }
    
    @Override
    public void addInputRange(final TemplateBuilderImpl templateBuilderImpl) {
    }
    
    @Override
    public void addRow(final TemplateBuilderImpl templateBuilderImpl) {
    }
    
    @Override
    public void apply(final Slice.Builder builder) {
        if (this.mIsError) {
            builder.addHints("error");
        }
    }
    
    @Override
    public TemplateBuilderImpl createInputRangeBuilder() {
        return new ListBuilderV1Impl.InputRangeBuilderImpl(this.getBuilder());
    }
    
    @Override
    public TemplateBuilderImpl createRowBuilder() {
        return new RowBuilderImpl(this);
    }
    
    @Override
    public void setColor(final int n) {
    }
    
    @Override
    public void setKeywords(final List<String> list) {
        final Slice.Builder builder = new Slice.Builder(this.getBuilder());
        for (int i = 0; i < list.size(); ++i) {
            builder.addText(list.get(i), null, new String[0]);
        }
        this.getBuilder().addSubSlice(builder.addHints("keywords").build());
    }
    
    @Override
    public void setTtl(final long n) {
    }
    
    public static class RowBuilderImpl extends TemplateBuilderImpl implements RowBuilder
    {
        public RowBuilderImpl(final ListBuilderBasicImpl listBuilderBasicImpl) {
            super(listBuilderBasicImpl.createChildBuilder(), null);
        }
        
        @Override
        public void addEndItem(final SliceAction sliceAction, final boolean b) {
        }
        
        @Override
        public void apply(final Slice.Builder builder) {
        }
        
        @Override
        public void setPrimaryAction(final SliceAction sliceAction) {
        }
        
        @Override
        public void setSubtitle(final CharSequence charSequence, final boolean b) {
        }
        
        @Override
        public void setTitle(final CharSequence charSequence) {
        }
        
        @Override
        public void setTitleItem(final IconCompat iconCompat, final int n, final boolean b) {
        }
    }
}
