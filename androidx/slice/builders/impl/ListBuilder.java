package androidx.slice.builders.impl;

import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.builders.SliceAction;
import android.app.PendingIntent;
import java.util.List;

public interface ListBuilder
{
    void addInputRange(final TemplateBuilderImpl p0);
    
    void addRow(final TemplateBuilderImpl p0);
    
    TemplateBuilderImpl createInputRangeBuilder();
    
    TemplateBuilderImpl createRowBuilder();
    
    void setColor(final int p0);
    
    void setKeywords(final List<String> p0);
    
    void setTtl(final long p0);
    
    public interface InputRangeBuilder extends RangeBuilder
    {
        void setInputAction(final PendingIntent p0);
    }
    
    public interface RangeBuilder
    {
        void setMax(final int p0);
        
        void setPrimaryAction(final SliceAction p0);
        
        void setSubtitle(final CharSequence p0);
        
        void setTitle(final CharSequence p0);
        
        void setValue(final int p0);
    }
    
    public interface RowBuilder
    {
        void addEndItem(final SliceAction p0, final boolean p1);
        
        void setPrimaryAction(final SliceAction p0);
        
        void setSubtitle(final CharSequence p0, final boolean p1);
        
        void setTitle(final CharSequence p0);
        
        void setTitleItem(final IconCompat p0, final int p1, final boolean p2);
    }
}
