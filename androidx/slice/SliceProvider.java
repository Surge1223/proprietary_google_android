package androidx.slice;

import android.database.Cursor;
import android.os.CancellationSignal;
import java.util.Collections;
import android.os.Process;
import androidx.slice.compat.CompatPermissionManager;
import java.util.Collection;
import java.util.ArrayList;
import androidx.slice.compat.SliceProviderWrapperContainer;
import android.support.v4.os.BuildCompat;
import android.os.Bundle;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager;
import androidx.slice.core.R;
import android.os.Parcelable;
import android.content.ComponentName;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import java.util.List;
import androidx.slice.compat.SliceProviderCompat;
import java.util.Set;
import android.support.v4.app.CoreComponentFactory;
import android.content.ContentProvider;

public abstract class SliceProvider extends ContentProvider implements CompatWrapped
{
    private static Clock sClock;
    private static Set<SliceSpec> sSpecs;
    private final String[] mAutoGrantPermissions;
    private SliceProviderCompat mCompat;
    private List<Uri> mPinnedSliceUris;
    
    public SliceProvider() {
        this.mAutoGrantPermissions = new String[0];
    }
    
    public SliceProvider(final String... mAutoGrantPermissions) {
        this.mAutoGrantPermissions = mAutoGrantPermissions;
    }
    
    public static PendingIntent createPermissionIntent(final Context context, final Uri uri, final String s) {
        final Intent intent = new Intent();
        intent.setComponent(new ComponentName(context.getPackageName(), "androidx.slice.compat.SlicePermissionActivity"));
        intent.putExtra("slice_uri", (Parcelable)uri);
        intent.putExtra("pkg", s);
        intent.putExtra("provider_pkg", context.getPackageName());
        intent.setData(uri.buildUpon().appendQueryParameter("package", s).build());
        return PendingIntent.getActivity(context, 0, intent, 0);
    }
    
    public static Slice createPermissionSlice(final Context context, final Uri uri, final String s) {
        final Slice.Builder builder = new Slice.Builder(uri);
        builder.addSubSlice(new Slice.Builder(uri.buildUpon().appendPath("permission").build()).addText(getPermissionString(context, s), null, new String[0]).addSubSlice(new Slice.Builder(builder).addHints("title", "shortcut").addAction(createPermissionIntent(context, uri, s), new Slice.Builder(builder).build(), null).build()).build());
        return builder.addHints("permission_request").build();
    }
    
    public static Clock getClock() {
        return SliceProvider.sClock;
    }
    
    public static Set<SliceSpec> getCurrentSpecs() {
        return SliceProvider.sSpecs;
    }
    
    public static CharSequence getPermissionString(final Context context, final String s) {
        final PackageManager packageManager = context.getPackageManager();
        try {
            return context.getString(R.string.abc_slices_permission_request, new Object[] { packageManager.getApplicationInfo(s, 0).loadLabel(packageManager), context.getApplicationInfo().loadLabel(packageManager) });
        }
        catch (PackageManager$NameNotFoundException ex) {
            throw new RuntimeException("Unknown calling app", (Throwable)ex);
        }
    }
    
    public static void setSpecs(final Set<SliceSpec> sSpecs) {
        SliceProvider.sSpecs = sSpecs;
    }
    
    public final int bulkInsert(final Uri uri, final ContentValues[] array) {
        return 0;
    }
    
    public Bundle call(final String s, final String s2, final Bundle bundle) {
        Bundle call;
        if (this.mCompat != null) {
            call = this.mCompat.call(s, s2, bundle);
        }
        else {
            call = null;
        }
        return call;
    }
    
    public final Uri canonicalize(final Uri uri) {
        return null;
    }
    
    public final int delete(final Uri uri, final String s, final String[] array) {
        return 0;
    }
    
    public final String getType(final Uri uri) {
        return "vnd.android.slice";
    }
    
    public Object getWrapper() {
        if (BuildCompat.isAtLeastP()) {
            return new SliceProviderWrapperContainer.SliceProviderWrapper(this, this.mAutoGrantPermissions);
        }
        return null;
    }
    
    public void handleSlicePinned(final Uri uri) {
        if (!this.mPinnedSliceUris.contains(uri)) {
            this.mPinnedSliceUris.add(uri);
        }
    }
    
    public void handleSliceUnpinned(final Uri uri) {
        if (this.mPinnedSliceUris.contains(uri)) {
            this.mPinnedSliceUris.remove(uri);
        }
    }
    
    public final Uri insert(final Uri uri, final ContentValues contentValues) {
        return null;
    }
    
    public abstract Slice onBindSlice(final Uri p0);
    
    public final boolean onCreate() {
        this.mPinnedSliceUris = new ArrayList<Uri>(SliceManager.getInstance(this.getContext()).getPinnedSlices());
        if (!BuildCompat.isAtLeastP()) {
            this.mCompat = new SliceProviderCompat(this, this.onCreatePermissionManager(this.mAutoGrantPermissions), this.getContext());
        }
        return this.onCreateSliceProvider();
    }
    
    protected CompatPermissionManager onCreatePermissionManager(final String[] array) {
        final Context context = this.getContext();
        final StringBuilder sb = new StringBuilder();
        sb.append("slice_perms_");
        sb.append(this.getClass().getName());
        return new CompatPermissionManager(context, sb.toString(), Process.myUid(), array);
    }
    
    public abstract boolean onCreateSliceProvider();
    
    public Collection<Uri> onGetSliceDescendants(final Uri uri) {
        return (Collection<Uri>)Collections.emptyList();
    }
    
    public Uri onMapIntentToUri(final Intent intent) {
        throw new UnsupportedOperationException("This provider has not implemented intent to uri mapping");
    }
    
    public void onSlicePinned(final Uri uri) {
    }
    
    public void onSliceUnpinned(final Uri uri) {
    }
    
    public final Cursor query(final Uri uri, final String[] array, final Bundle bundle, final CancellationSignal cancellationSignal) {
        return null;
    }
    
    public final Cursor query(final Uri uri, final String[] array, final String s, final String[] array2, final String s2) {
        return null;
    }
    
    public final Cursor query(final Uri uri, final String[] array, final String s, final String[] array2, final String s2, final CancellationSignal cancellationSignal) {
        return null;
    }
    
    public final int update(final Uri uri, final ContentValues contentValues, final String s, final String[] array) {
        return 0;
    }
}
