package androidx.slice.compat;

import android.os.Build.VERSION;
import android.content.Intent;
import androidx.versionedparcelable.VersionedParcelable;
import android.os.StrictMode$ThreadPolicy;
import android.os.StrictMode$ThreadPolicy$Builder;
import android.os.StrictMode;
import android.os.Binder;
import java.util.List;
import android.os.RemoteException;
import androidx.versionedparcelable.ParcelUtils;
import android.os.Parcelable;
import androidx.slice.Slice;
import java.util.Iterator;
import java.util.ArrayList;
import androidx.slice.SliceSpec;
import android.os.Bundle;
import android.content.ContentProviderClient;
import android.net.Uri;
import android.content.ContentResolver;
import android.content.SharedPreferences;
import java.util.Collection;
import android.support.v4.util.ArraySet;
import java.util.Set;
import java.util.Collections;
import android.util.Log;
import android.os.Process;
import android.os.Looper;
import androidx.slice.SliceProvider;
import android.os.Handler;
import android.content.Context;

public class SliceProviderCompat
{
    private final Runnable mAnr;
    private String mCallback;
    private final Context mContext;
    private final Handler mHandler;
    private CompatPermissionManager mPermissionManager;
    private CompatPinnedList mPinnedList;
    private final SliceProvider mProvider;
    
    public SliceProviderCompat(final SliceProvider mProvider, final CompatPermissionManager mPermissionManager, final Context mContext) {
        this.mHandler = new Handler(Looper.getMainLooper());
        this.mAnr = new Runnable() {
            @Override
            public void run() {
                Process.sendSignal(Process.myPid(), 3);
                final StringBuilder sb = new StringBuilder();
                sb.append("Timed out while handling slice callback ");
                sb.append(SliceProviderCompat.this.mCallback);
                Log.wtf("SliceProviderCompat", sb.toString());
            }
        };
        this.mProvider = mProvider;
        this.mContext = mContext;
        final StringBuilder sb = new StringBuilder();
        sb.append("slice_data_");
        sb.append(this.getClass().getName());
        final String string = sb.toString();
        final SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("slice_data_all_slice_files", 0);
        final Set stringSet = sharedPreferences.getStringSet("slice_data_all_slice_files", (Set)Collections.emptySet());
        if (!stringSet.contains(string)) {
            final ArraySet set = new ArraySet<String>(stringSet);
            set.add(string);
            sharedPreferences.edit().putStringSet("slice_data_all_slice_files", (Set)set).commit();
        }
        this.mPinnedList = new CompatPinnedList(this.mContext, string);
        this.mPermissionManager = mPermissionManager;
    }
    
    private static ProviderHolder acquireClient(final ContentResolver contentResolver, final Uri uri) {
        final ContentProviderClient acquireContentProviderClient = contentResolver.acquireContentProviderClient(uri);
        if (acquireContentProviderClient != null) {
            return new ProviderHolder(acquireContentProviderClient);
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("No provider found for ");
        sb.append(uri);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void addSpecs(final Bundle bundle, final Set<SliceSpec> set) {
        final ArrayList<String> list = new ArrayList<String>();
        final ArrayList<Integer> list2 = new ArrayList<Integer>();
        for (final SliceSpec sliceSpec : set) {
            list.add(sliceSpec.getType());
            list2.add(sliceSpec.getRevision());
        }
        bundle.putStringArrayList("specs", (ArrayList)list);
        bundle.putIntegerArrayList("revs", (ArrayList)list2);
    }
    
    public static Slice bindSlice(final Context context, final Uri uri, final Set<SliceSpec> set) {
        final ProviderHolder acquireClient = acquireClient(context.getContentResolver(), uri);
        if (acquireClient.mProvider == null) {
            final StringBuilder sb = new StringBuilder();
            sb.append("Unknown URI ");
            sb.append(uri);
            throw new IllegalArgumentException(sb.toString());
        }
        try {
            try {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("slice_uri", (Parcelable)uri);
                addSpecs(bundle, set);
                final Bundle call = acquireClient.mProvider.call("bind_slice", "supports_versioned_parcelable", bundle);
                if (call == null) {
                    return null;
                }
                call.setClassLoader(SliceProviderCompat.class.getClassLoader());
                final Parcelable parcelable = call.getParcelable("slice");
                if (parcelable == null) {
                    return null;
                }
                if (parcelable instanceof Bundle) {
                    return new Slice((Bundle)parcelable);
                }
                return (Slice)ParcelUtils.fromParcelable(parcelable);
            }
            finally {}
        }
        catch (RemoteException ex) {
            Log.e("SliceProviderCompat", "Unable to bind slice", (Throwable)ex);
            return null;
        }
    }
    
    private Context getContext() {
        return this.mContext;
    }
    
    public static List<Uri> getPinnedSlices(final Context context) {
        final ArrayList<Uri> list = new ArrayList<Uri>();
        final Iterator<String> iterator = context.getSharedPreferences("slice_data_all_slice_files", 0).getStringSet("slice_data_all_slice_files", (Set)Collections.emptySet()).iterator();
        while (iterator.hasNext()) {
            list.addAll(new CompatPinnedList(context, iterator.next()).getPinnedSlices());
        }
        return list;
    }
    
    public static Collection<Uri> getSliceDescendants(Context context, final Uri uri) {
        final ContentResolver contentResolver = context.getContentResolver();
        try {
            final ProviderHolder acquireClient = acquireClient(contentResolver, uri);
            final Context context2 = context = null;
            try {
                try {
                    context = context2;
                    final Bundle bundle = new Bundle();
                    context = context2;
                    bundle.putParcelable("slice_uri", (Parcelable)uri);
                    context = context2;
                    final Bundle call = acquireClient.mProvider.call("get_descendants", "supports_versioned_parcelable", bundle);
                    if (call != null) {
                        context = context2;
                        final ArrayList parcelableArrayList = call.getParcelableArrayList("slice_descendants");
                        if (acquireClient != null) {
                            acquireClient.close();
                        }
                        return (Collection<Uri>)parcelableArrayList;
                    }
                    if (acquireClient != null) {
                        acquireClient.close();
                    }
                    return (Collection<Uri>)Collections.emptyList();
                }
                finally {
                    if (acquireClient != null) {
                        if (context != null) {
                            final ProviderHolder providerHolder = acquireClient;
                            providerHolder.close();
                        }
                        else {
                            acquireClient.close();
                        }
                    }
                }
            }
            catch (Throwable t) {}
            try {
                final ProviderHolder providerHolder = acquireClient;
                providerHolder.close();
            }
            catch (Throwable t2) {}
        }
        catch (RemoteException ex) {
            Log.e("SliceProviderCompat", "Unable to get slice descendants", (Throwable)ex);
        }
        return (Collection<Uri>)Collections.emptyList();
    }
    
    public static Set<SliceSpec> getSpecs(final Bundle bundle) {
        final ArraySet<SliceSpec> set = new ArraySet<SliceSpec>();
        final ArrayList stringArrayList = bundle.getStringArrayList("specs");
        final ArrayList integerArrayList = bundle.getIntegerArrayList("revs");
        if (stringArrayList != null && integerArrayList != null) {
            for (int i = 0; i < stringArrayList.size(); ++i) {
                set.add(new SliceSpec(stringArrayList.get(i), integerArrayList.get(i)));
            }
        }
        return set;
    }
    
    public static void grantSlicePermission(Context context, final String s, final String s2, final Uri uri) {
        final ContentResolver contentResolver = context.getContentResolver();
        try {
            final ProviderHolder acquireClient = acquireClient(contentResolver, uri);
            final Context context2 = context = null;
            try {
                try {
                    context = context2;
                    final Bundle bundle = new Bundle();
                    context = context2;
                    bundle.putParcelable("slice_uri", (Parcelable)uri);
                    context = context2;
                    bundle.putString("provider_pkg", s);
                    context = context2;
                    bundle.putString("pkg", s2);
                    context = context2;
                    acquireClient.mProvider.call("grant_perms", "supports_versioned_parcelable", bundle);
                    if (acquireClient != null) {
                        acquireClient.close();
                    }
                }
                finally {
                    if (acquireClient != null) {
                        if (context != null) {
                            final ProviderHolder providerHolder = acquireClient;
                            providerHolder.close();
                        }
                        else {
                            acquireClient.close();
                        }
                    }
                }
            }
            catch (Throwable t) {}
            try {
                final ProviderHolder providerHolder = acquireClient;
                providerHolder.close();
            }
            catch (Throwable t2) {}
        }
        catch (RemoteException ex) {
            Log.e("SliceProviderCompat", "Unable to get slice descendants", (Throwable)ex);
        }
    }
    
    private Slice handleBindSlice(final Uri uri, final Set<SliceSpec> set, String nameForUid) {
        if (nameForUid == null) {
            nameForUid = this.getContext().getPackageManager().getNameForUid(Binder.getCallingUid());
        }
        if (this.mPermissionManager.checkSlicePermission(uri, Binder.getCallingPid(), Binder.getCallingUid()) != 0) {
            final SliceProvider mProvider = this.mProvider;
            return SliceProvider.createPermissionSlice(this.getContext(), uri, nameForUid);
        }
        return this.onBindSliceStrict(uri, set);
    }
    
    private Collection<Uri> handleGetDescendants(final Uri uri) {
        this.mCallback = "onGetSliceDescendants";
        return this.mProvider.onGetSliceDescendants(uri);
    }
    
    private void handleSlicePinned(final Uri uri) {
        this.mCallback = "onSlicePinned";
        this.mHandler.postDelayed(this.mAnr, 2000L);
        try {
            this.mProvider.onSlicePinned(uri);
            this.mProvider.handleSlicePinned(uri);
        }
        finally {
            this.mHandler.removeCallbacks(this.mAnr);
        }
    }
    
    private void handleSliceUnpinned(final Uri uri) {
        this.mCallback = "onSliceUnpinned";
        this.mHandler.postDelayed(this.mAnr, 2000L);
        try {
            this.mProvider.onSliceUnpinned(uri);
            this.mProvider.handleSliceUnpinned(uri);
        }
        finally {
            this.mHandler.removeCallbacks(this.mAnr);
        }
    }
    
    private Slice onBindSliceStrict(final Uri uri, final Set<SliceSpec> specs) {
        final StrictMode$ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        this.mCallback = "onBindSlice";
        this.mHandler.postDelayed(this.mAnr, 2000L);
        try {
            StrictMode.setThreadPolicy(new StrictMode$ThreadPolicy$Builder().detectAll().penaltyDeath().build());
            SliceProvider.setSpecs(specs);
            try {
                return this.mProvider.onBindSlice(uri);
            }
            finally {
                SliceProvider.setSpecs(null);
                this.mHandler.removeCallbacks(this.mAnr);
            }
        }
        finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }
    
    public static void pinSlice(final Context context, final Uri uri, final Set<SliceSpec> set) {
        final ProviderHolder acquireClient = acquireClient(context.getContentResolver(), uri);
        if (acquireClient.mProvider != null) {
            try {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("slice_uri", (Parcelable)uri);
                bundle.putString("pkg", context.getPackageName());
                addSpecs(bundle, set);
                acquireClient.mProvider.call("pin_slice", "supports_versioned_parcelable", bundle);
            }
            catch (RemoteException ex) {
                Log.e("SliceProviderCompat", "Unable to pin slice", (Throwable)ex);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown URI ");
        sb.append(uri);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public static void unpinSlice(final Context context, final Uri uri, final Set<SliceSpec> set) {
        final ProviderHolder acquireClient = acquireClient(context.getContentResolver(), uri);
        if (acquireClient.mProvider != null) {
            try {
                final Bundle bundle = new Bundle();
                bundle.putParcelable("slice_uri", (Parcelable)uri);
                bundle.putString("pkg", context.getPackageName());
                addSpecs(bundle, set);
                acquireClient.mProvider.call("unpin_slice", "supports_versioned_parcelable", bundle);
            }
            catch (RemoteException ex) {
                Log.e("SliceProviderCompat", "Unable to unpin slice", (Throwable)ex);
            }
            return;
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("Unknown URI ");
        sb.append(uri);
        throw new IllegalArgumentException(sb.toString());
    }
    
    public Bundle call(final String s, String s2, Bundle bundle) {
        final boolean equals = s.equals("bind_slice");
        final Parcelable parcelable = null;
        final Parcelable parcelable2 = null;
        final Parcelable parcelable3 = null;
        final Parcelable parcelable4 = null;
        if (equals) {
            final Slice handleBindSlice = this.handleBindSlice((Uri)bundle.getParcelable("slice_uri"), getSpecs(bundle), this.getCallingPackage());
            bundle = new Bundle();
            if ("supports_versioned_parcelable".equals(s2)) {
                Parcelable parcelable5 = parcelable4;
                if (handleBindSlice != null) {
                    parcelable5 = ParcelUtils.toParcelable(handleBindSlice);
                }
                bundle.putParcelable("slice", parcelable5);
            }
            else {
                Object bundle2 = parcelable;
                if (handleBindSlice != null) {
                    bundle2 = handleBindSlice.toBundle();
                }
                bundle.putParcelable("slice", (Parcelable)bundle2);
            }
            return bundle;
        }
        if (s.equals("map_slice")) {
            final Uri onMapIntentToUri = this.mProvider.onMapIntentToUri((Intent)bundle.getParcelable("slice_intent"));
            final Bundle bundle3 = new Bundle();
            if (onMapIntentToUri != null) {
                final Slice handleBindSlice2 = this.handleBindSlice(onMapIntentToUri, getSpecs(bundle), this.getCallingPackage());
                if ("supports_versioned_parcelable".equals(s2)) {
                    Parcelable parcelable6 = parcelable2;
                    if (handleBindSlice2 != null) {
                        parcelable6 = ParcelUtils.toParcelable(handleBindSlice2);
                    }
                    bundle3.putParcelable("slice", parcelable6);
                }
                else {
                    Object bundle4 = parcelable3;
                    if (handleBindSlice2 != null) {
                        bundle4 = handleBindSlice2.toBundle();
                    }
                    bundle3.putParcelable("slice", (Parcelable)bundle4);
                }
            }
            else {
                bundle3.putParcelable("slice", (Parcelable)null);
            }
            return bundle3;
        }
        if (s.equals("map_only")) {
            final Uri onMapIntentToUri2 = this.mProvider.onMapIntentToUri((Intent)bundle.getParcelable("slice_intent"));
            final Bundle bundle5 = new Bundle();
            bundle5.putParcelable("slice", (Parcelable)onMapIntentToUri2);
            return bundle5;
        }
        if (s.equals("pin_slice")) {
            final Uri uri = (Uri)bundle.getParcelable("slice_uri");
            if (this.mPinnedList.addPin(uri, bundle.getString("pkg"), getSpecs(bundle))) {
                this.handleSlicePinned(uri);
            }
            return null;
        }
        if (s.equals("unpin_slice")) {
            final Uri uri2 = (Uri)bundle.getParcelable("slice_uri");
            s2 = bundle.getString("pkg");
            if (this.mPinnedList.removePin(uri2, s2)) {
                this.handleSliceUnpinned(uri2);
            }
            return null;
        }
        if (s.equals("get_specs")) {
            final Uri uri3 = (Uri)bundle.getParcelable("slice_uri");
            final Bundle bundle6 = new Bundle();
            addSpecs(bundle6, this.mPinnedList.getSpecs(uri3));
            return bundle6;
        }
        if (s.equals("get_descendants")) {
            final Uri uri4 = (Uri)bundle.getParcelable("slice_uri");
            final Bundle bundle7 = new Bundle();
            bundle7.putParcelableArrayList("slice_descendants", new ArrayList((Collection<? extends E>)this.handleGetDescendants(uri4)));
            return bundle7;
        }
        if (s.equals("check_perms")) {
            final Uri uri5 = (Uri)bundle.getParcelable("slice_uri");
            bundle.getString("pkg");
            final int int1 = bundle.getInt("pid");
            final int int2 = bundle.getInt("uid");
            final Bundle bundle8 = new Bundle();
            bundle8.putInt("result", this.mPermissionManager.checkSlicePermission(uri5, int1, int2));
            return bundle8;
        }
        if (s.equals("grant_perms")) {
            final Uri uri6 = (Uri)bundle.getParcelable("slice_uri");
            s2 = bundle.getString("pkg");
            if (Binder.getCallingUid() != Process.myUid()) {
                throw new SecurityException("Only the owning process can manage slice permissions");
            }
            this.mPermissionManager.grantSlicePermission(uri6, s2);
        }
        else if (s.equals("revoke_perms")) {
            final Uri uri7 = (Uri)bundle.getParcelable("slice_uri");
            s2 = bundle.getString("pkg");
            if (Binder.getCallingUid() != Process.myUid()) {
                throw new SecurityException("Only the owning process can manage slice permissions");
            }
            this.mPermissionManager.revokeSlicePermission(uri7, s2);
        }
        return null;
    }
    
    public String getCallingPackage() {
        return this.mProvider.getCallingPackage();
    }
    
    private static class ProviderHolder implements AutoCloseable
    {
        private final ContentProviderClient mProvider;
        
        ProviderHolder(final ContentProviderClient mProvider) {
            this.mProvider = mProvider;
        }
        
        @Override
        public void close() {
            if (this.mProvider == null) {
                return;
            }
            if (Build.VERSION.SDK_INT >= 24) {
                this.mProvider.close();
            }
            else {
                this.mProvider.release();
            }
        }
    }
}
