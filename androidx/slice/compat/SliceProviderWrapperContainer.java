package androidx.slice.compat;

import android.content.Intent;
import java.util.Collection;
import androidx.slice.SliceConvert;
import android.app.slice.Slice;
import android.app.slice.SliceSpec;
import java.util.Set;
import android.net.Uri;
import android.content.pm.ProviderInfo;
import android.content.Context;
import android.app.slice.SliceProvider;
import android.annotation.TargetApi;

@TargetApi(28)
public class SliceProviderWrapperContainer
{
    public static class SliceProviderWrapper extends SliceProvider
    {
        private androidx.slice.SliceProvider mSliceProvider;
        
        public SliceProviderWrapper(final androidx.slice.SliceProvider mSliceProvider, final String[] array) {
            super(array);
            this.mSliceProvider = mSliceProvider;
        }
        
        public void attachInfo(final Context context, final ProviderInfo providerInfo) {
            this.mSliceProvider.attachInfo(context, providerInfo);
            super.attachInfo(context, providerInfo);
        }
        
        public Slice onBindSlice(final Uri uri, final Set<SliceSpec> set) {
            androidx.slice.SliceProvider.setSpecs(SliceConvert.wrap(set));
            try {
                return SliceConvert.unwrap(this.mSliceProvider.onBindSlice(uri));
            }
            finally {
                androidx.slice.SliceProvider.setSpecs(null);
            }
        }
        
        public boolean onCreate() {
            return true;
        }
        
        public Collection<Uri> onGetSliceDescendants(final Uri uri) {
            return this.mSliceProvider.onGetSliceDescendants(uri);
        }
        
        public Uri onMapIntentToUri(final Intent intent) {
            return this.mSliceProvider.onMapIntentToUri(intent);
        }
        
        public void onSlicePinned(final Uri uri) {
            this.mSliceProvider.onSlicePinned(uri);
            this.mSliceProvider.handleSlicePinned(uri);
        }
        
        public void onSliceUnpinned(final Uri uri) {
            this.mSliceProvider.onSliceUnpinned(uri);
            this.mSliceProvider.handleSliceUnpinned(uri);
        }
    }
}
