package androidx.slice.core;

import android.text.TextUtils;
import java.util.Collection;
import androidx.slice.Slice;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import androidx.slice.SliceItem;

public class SliceQuery
{
    private static boolean checkFormat(final SliceItem sliceItem, final String s) {
        return s == null || s.equals(sliceItem.getFormat());
    }
    
    private static boolean checkSubtype(final SliceItem sliceItem, final String s) {
        return s == null || s.equals(sliceItem.getSubType());
    }
    
    private static <T> List<T> collect(final Iterator<T> iterator) {
        final ArrayList<T> list = new ArrayList<T>();
        while (iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }
    
    private static <T> Iterator<T> filter(final Iterator<T> iterator, final Filter<T> filter) {
        return new Iterator<T>() {
            T mNext = this.findNext();
            
            private T findNext() {
                while (iterator.hasNext()) {
                    final T next = iterator.next();
                    if (filter.filter(next)) {
                        return next;
                    }
                }
                return null;
            }
            
            @Override
            public boolean hasNext() {
                return this.mNext != null;
            }
            
            @Override
            public T next() {
                final T mNext = this.mNext;
                this.mNext = this.findNext();
                return mNext;
            }
        };
    }
    
    public static SliceItem find(final Slice slice, final String s) {
        return find(slice, s, null, (String[])null);
    }
    
    public static SliceItem find(final Slice slice, final String s, final String s2, final String s3) {
        return find(slice, s, new String[] { s2 }, new String[] { s3 });
    }
    
    public static SliceItem find(final Slice slice, final String s, final String[] array, final String[] array2) {
        return findFirst((Iterator<SliceItem>)filter((Iterator<T>)stream(slice), (Filter<T>)new Filter<SliceItem>() {
            public boolean filter(final SliceItem sliceItem) {
                return checkFormat(sliceItem, s) && SliceQuery.hasHints(sliceItem, array) && !SliceQuery.hasAnyHints(sliceItem, array2);
            }
        }), null);
    }
    
    public static SliceItem find(final SliceItem sliceItem, final String s) {
        return find(sliceItem, s, null, (String[])null);
    }
    
    public static SliceItem find(final SliceItem sliceItem, final String s, final String s2, final String s3) {
        return find(sliceItem, s, new String[] { s2 }, new String[] { s3 });
    }
    
    public static SliceItem find(final SliceItem sliceItem, final String s, final String[] array, final String[] array2) {
        return findFirst((Iterator<SliceItem>)filter((Iterator<T>)stream(sliceItem), (Filter<T>)new Filter<SliceItem>() {
            public boolean filter(final SliceItem sliceItem) {
                return checkFormat(sliceItem, s) && SliceQuery.hasHints(sliceItem, array) && !SliceQuery.hasAnyHints(sliceItem, array2);
            }
        }), null);
    }
    
    public static List<SliceItem> findAll(final SliceItem sliceItem, final String s) {
        return findAll(sliceItem, s, null, (String[])null);
    }
    
    public static List<SliceItem> findAll(final SliceItem sliceItem, final String s, final String s2, final String s3) {
        return findAll(sliceItem, s, new String[] { s2 }, new String[] { s3 });
    }
    
    public static List<SliceItem> findAll(final SliceItem sliceItem, final String s, final String[] array, final String[] array2) {
        return collect((Iterator<SliceItem>)filter((Iterator<T>)stream(sliceItem), (Filter<T>)new Filter<SliceItem>() {
            public boolean filter(final SliceItem sliceItem) {
                return checkFormat(sliceItem, s) && SliceQuery.hasHints(sliceItem, array) && !SliceQuery.hasAnyHints(sliceItem, array2);
            }
        }));
    }
    
    private static <T> T findFirst(final Iterator<T> iterator, final T t) {
        while (iterator.hasNext()) {
            final T next = iterator.next();
            if (next != null) {
                return next;
            }
        }
        return t;
    }
    
    public static SliceItem findSubtype(final Slice slice, final String s, final String s2) {
        return findFirst((Iterator<SliceItem>)filter((Iterator<T>)stream(slice), (Filter<T>)new Filter<SliceItem>() {
            public boolean filter(final SliceItem sliceItem) {
                return checkFormat(sliceItem, s) && checkSubtype(sliceItem, s2);
            }
        }), null);
    }
    
    public static SliceItem findSubtype(final SliceItem sliceItem, final String s, final String s2) {
        return findFirst((Iterator<SliceItem>)filter((Iterator<T>)stream(sliceItem), (Filter<T>)new Filter<SliceItem>() {
            public boolean filter(final SliceItem sliceItem) {
                return checkFormat(sliceItem, s) && checkSubtype(sliceItem, s2);
            }
        }), null);
    }
    
    public static SliceItem findTopLevelItem(final Slice slice, final String s, final String s2, final String[] array, final String[] array2) {
        final List<SliceItem> items = slice.getItems();
        for (int i = 0; i < items.size(); ++i) {
            final SliceItem sliceItem = items.get(i);
            if (checkFormat(sliceItem, s) && checkSubtype(sliceItem, s2) && hasHints(sliceItem, array) && !hasAnyHints(sliceItem, array2)) {
                return sliceItem;
            }
        }
        return null;
    }
    
    private static Iterator<SliceItem> getSliceItemStream(final ArrayList<SliceItem> list) {
        return new Iterator<SliceItem>() {
            @Override
            public boolean hasNext() {
                return list.size() != 0;
            }
            
            @Override
            public SliceItem next() {
                final SliceItem sliceItem = list.remove(0);
                if ("slice".equals(sliceItem.getFormat()) || "action".equals(sliceItem.getFormat())) {
                    list.addAll(sliceItem.getSlice().getItems());
                }
                return sliceItem;
            }
        };
    }
    
    public static boolean hasAnyHints(final SliceItem sliceItem, final String... array) {
        if (array == null) {
            return false;
        }
        final List<String> hints = sliceItem.getHints();
        for (int length = array.length, i = 0; i < length; ++i) {
            if (hints.contains(array[i])) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean hasHints(final SliceItem sliceItem, final String... array) {
        if (array == null) {
            return true;
        }
        final List<String> hints = sliceItem.getHints();
        for (final String s : array) {
            if (!TextUtils.isEmpty((CharSequence)s) && !hints.contains(s)) {
                return false;
            }
        }
        return true;
    }
    
    public static Iterator<SliceItem> stream(final Slice slice) {
        final ArrayList<SliceItem> list = new ArrayList<SliceItem>();
        if (slice != null) {
            list.addAll(slice.getItems());
        }
        return getSliceItemStream(list);
    }
    
    public static Iterator<SliceItem> stream(final SliceItem sliceItem) {
        final ArrayList<SliceItem> list = new ArrayList<SliceItem>();
        list.add(sliceItem);
        return getSliceItemStream(list);
    }
    
    private interface Filter<T>
    {
        boolean filter(final T p0);
    }
}
