package androidx.slice.core;

import androidx.slice.Slice;
import android.support.v4.graphics.drawable.IconCompat;
import androidx.slice.SliceItem;
import android.app.PendingIntent;

public class SliceActionImpl implements SliceAction
{
    private PendingIntent mAction;
    private SliceItem mActionItem;
    private CharSequence mContentDescription;
    private IconCompat mIcon;
    private int mImageMode;
    private boolean mIsChecked;
    private boolean mIsToggle;
    private int mPriority;
    private SliceItem mSliceItem;
    private CharSequence mTitle;
    
    public SliceActionImpl(final PendingIntent mAction, final IconCompat mIcon, final int mImageMode, final CharSequence mTitle) {
        this.mImageMode = 3;
        this.mPriority = -1;
        this.mAction = mAction;
        this.mIcon = mIcon;
        this.mTitle = mTitle;
        this.mImageMode = mImageMode;
    }
    
    public SliceActionImpl(final PendingIntent mAction, final CharSequence mTitle, final boolean mIsChecked) {
        this.mImageMode = 3;
        this.mPriority = -1;
        this.mAction = mAction;
        this.mTitle = mTitle;
        this.mIsToggle = true;
        this.mIsChecked = mIsChecked;
    }
    
    public SliceActionImpl(SliceItem sliceItem) {
        this.mImageMode = 3;
        final int n = -1;
        this.mPriority = -1;
        this.mSliceItem = sliceItem;
        sliceItem = SliceQuery.find(sliceItem, "action");
        if (sliceItem == null) {
            return;
        }
        this.mActionItem = sliceItem;
        final SliceItem find = SliceQuery.find(sliceItem.getSlice(), "image");
        if (find != null) {
            this.mIcon = find.getIcon();
            int mImageMode;
            if (find.hasHint("no_tint")) {
                if (find.hasHint("large")) {
                    mImageMode = 2;
                }
                else {
                    mImageMode = 1;
                }
            }
            else {
                mImageMode = 0;
            }
            this.mImageMode = mImageMode;
        }
        final SliceItem find2 = SliceQuery.find(sliceItem.getSlice(), "text", "title", null);
        if (find2 != null) {
            this.mTitle = find2.getText();
        }
        final SliceItem subtype = SliceQuery.findSubtype(sliceItem.getSlice(), "text", "content_description");
        if (subtype != null) {
            this.mContentDescription = subtype.getText();
        }
        this.mIsToggle = "toggle".equals(sliceItem.getSubType());
        if (this.mIsToggle) {
            this.mIsChecked = sliceItem.hasHint("selected");
        }
        sliceItem = SliceQuery.findSubtype(sliceItem.getSlice(), "int", "priority");
        int int1 = n;
        if (sliceItem != null) {
            int1 = sliceItem.getInt();
        }
        this.mPriority = int1;
    }
    
    public Slice buildSlice(final Slice.Builder builder) {
        final Slice.Builder builder2 = new Slice.Builder(builder);
        final IconCompat mIcon = this.mIcon;
        final String s = null;
        if (mIcon != null) {
            String[] array;
            if (this.mImageMode == 0) {
                array = new String[0];
            }
            else {
                array = new String[] { "no_tint" };
            }
            builder2.addIcon(this.mIcon, null, array);
        }
        if (this.mTitle != null) {
            builder2.addText(this.mTitle, null, "title");
        }
        if (this.mContentDescription != null) {
            builder2.addText(this.mContentDescription, "content_description", new String[0]);
        }
        if (this.mIsToggle && this.mIsChecked) {
            builder2.addHints("selected");
        }
        if (this.mPriority != -1) {
            builder2.addInt(this.mPriority, "priority", new String[0]);
        }
        String s2 = s;
        if (this.mIsToggle) {
            s2 = "toggle";
        }
        builder.addHints("shortcut");
        builder.addAction(this.mAction, builder2.build(), s2);
        return builder.build();
    }
    
    public PendingIntent getAction() {
        PendingIntent pendingIntent;
        if (this.mAction != null) {
            pendingIntent = this.mAction;
        }
        else {
            pendingIntent = this.mActionItem.getAction();
        }
        return pendingIntent;
    }
    
    public SliceItem getActionItem() {
        return this.mActionItem;
    }
    
    public CharSequence getContentDescription() {
        return this.mContentDescription;
    }
    
    @Override
    public IconCompat getIcon() {
        return this.mIcon;
    }
    
    @Override
    public int getImageMode() {
        return this.mImageMode;
    }
    
    public SliceItem getSliceItem() {
        return this.mSliceItem;
    }
    
    public CharSequence getTitle() {
        return this.mTitle;
    }
    
    public boolean isChecked() {
        return this.mIsChecked;
    }
    
    public boolean isDefaultToggle() {
        return this.mIsToggle && this.mIcon == null;
    }
    
    @Override
    public boolean isToggle() {
        return this.mIsToggle;
    }
}
