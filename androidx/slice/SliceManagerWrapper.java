package androidx.slice;

import android.net.Uri;
import java.util.List;
import android.content.Context;

class SliceManagerWrapper extends SliceManager
{
    private final Context mContext;
    private final android.app.slice.SliceManager mManager;
    
    SliceManagerWrapper(final Context context) {
        this(context, (android.app.slice.SliceManager)context.getSystemService((Class)android.app.slice.SliceManager.class));
    }
    
    SliceManagerWrapper(final Context mContext, final android.app.slice.SliceManager mManager) {
        this.mContext = mContext;
        this.mManager = mManager;
    }
    
    @Override
    public List<Uri> getPinnedSlices() {
        return (List<Uri>)this.mManager.getPinnedSlices();
    }
    
    @Override
    public void grantSlicePermission(final String s, final Uri uri) {
        this.mManager.grantSlicePermission(s, uri);
    }
}
