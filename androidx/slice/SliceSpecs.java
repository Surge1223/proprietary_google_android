package androidx.slice;

public class SliceSpecs
{
    public static final SliceSpec BASIC;
    public static final SliceSpec LIST;
    public static final SliceSpec MESSAGING;
    
    static {
        BASIC = new SliceSpec("androidx.slice.BASIC", 1);
        LIST = new SliceSpec("androidx.slice.LIST", 1);
        MESSAGING = new SliceSpec("androidx.slice.MESSAGING", 1);
    }
}
